#!/usr/bin/python
# 
# Copyright (c) 2011-2015, 
#  Etem Deniz    <etem.deniz@boun.edu.tr>, Bogazici University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
# (1) Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 
# (2) Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# 
# (3) The names of the contributors may not be used to endorse or promote
# products derived from this software without specific prior written
# permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
import os, errno, sys, stat
import shutil
import subprocess
import time
import signal

CWD = os.getcwd()
#DR_DIR = '' 									#not supported
#UMBRA_DIR = '' 								#not supported
BENCH_SYN_DIR = CWD + '/MBDT_Project'			# benchmark synthesis project path
MCA_DIR = CWD + '/MCA_Project'					# mca apis project path
SYN_TYPE = 'syn'			#syn (synthetic), sal (stand-alone), swm (stand-alone with performance metrics)
LIB_TYPE = 'posix' 			#posix, mrapi, mcapi
SIM_METRIC_TYPE = 'low' 	#low, high, all
SIM_THRESHOLD = '80' 		#[0-100]
IND_SIM_THRESHOLD = '70' 	#[0-100]
ITERATION_THRESHOLD = 50 	#[1++]
SYN_CODE_LEVEL = 'high' 	#high, low
CHAR_REPEAT = '3' 			#[1++]
GEN_SYN = 'true'			#true, false

#**********************************************************************************************************************************#
hz = os.sysconf(os.sysconf_names['SC_CLK_TCK'])
pid = os.getpid()

def getChildTime():
	with open("/proc/%d/stat" % (pid)) as fp:
		fields = fp.read().split(' ')[15:17]
		cutime, cstime = [ (float(f) / hz) for f in fields ]
		ctime = cutime + cstime
		return ctime
		# cutime + cstime will give you the total CPU used by child

def mkdirP(path):
    try:
        os.mkdir(path)
    except os.error, e:
		return

def cd(path):
	os.chdir(path)

def editPaths(argc, argv):
	CURR_DIR = os.getcwd()
	index = 0	
	retVal = []
	while (index < argc):
		if os.path.exists(argv[index]):
			if not os.path.isabs(argv[index]):
				retVal.append(CURR_DIR + '/' + argv[index])
			else:
				retVal.append(argv[index])
			index = index + 1
		else:
			print 'ERROR: Cannot find:', argv[index]
			sys.exit(4)
	return retVal

def findInSubdirectory(prefix, suffix, subdirectory=''):
	if subdirectory:
		path = subdirectory
	else:
		path = os.getcwd()
	for root, dirs, files in os.walk(path):
		for file in files:
			if file.startswith(prefix) and file.endswith(suffix):
				return os.path.join(root, file)
	return ''

def cleanFiles(synName):
	if os.path.exists(synName):
		os.remove(synName)
	# keep the following source code since we use it in test phase
	#if os.path.exists(synName + '.c'):
	#	os.remove(synName + '.c')

	path = os.getcwd()
	for root, dirs, files in os.walk(path):
		for file in files:
			#if (file.startswith('umbra.') and file.endswith('.proc.log')) or file.endswith('.perf.log'):
			if  file.endswith('.perf.log'):
				os.remove(file)

	if os.path.exists('origGenAlgParams.txt'):
		if os.path.exists('genAlgParams.txt'):
			os.remove('genAlgParams.txt')
		shutil.copy2('origGenAlgParams.txt', 'genAlgParams.txt')
	else:
		if os.path.exists('genAlgParams.txt'):
			shutil.copy2('genAlgParams.txt', 'origGenAlgParams.txt')
	
	if os.path.exists('graph.txt'):
		os.remove('graph.txt')

#**********************************************************************************************************************************#
print '**********************************************************************'
print '          MINIME: Multicore Benchmark Synthesizer v1.5           '
print '**********************************************************************'

if len(sys.argv) < 2:
	print 'Usage:', sys.argv[0], '-ep "<executable path> <parameters>"'
	sys.exit(1)

exeStr = ""
exe = []
isSourceExists = 0
count = 1
argc = len(sys.argv)

while (count < argc):
	if sys.argv[count] == '-ep':
		exeStr = sys.argv[count + 1]
	if sys.argv[count] == '-st':
		SYN_TYPE = sys.argv[count + 1]
	if sys.argv[count] == '-ss':
		SIM_THRESHOLD = sys.argv[count + 1]
	if sys.argv[count] == '-is':
		IND_SIM_THRESHOLD = sys.argv[count + 1]
	if sys.argv[count] == '-bl':
		LIB_TYPE = sys.argv[count + 1]
	if sys.argv[count] == '-ml':
		SIM_METRIC_TYPE = sys.argv[count + 1]
	if sys.argv[count] == '-ub':
		ITERATION_THRESHOLD = int(sys.argv[count + 1])
	if sys.argv[count] == '-co':
		exeStr = sys.argv[count + 1]
		isSourceExists = 1
	if sys.argv[count] == '-h' or sys.argv[count] == '--h' or sys.argv[count] == '-help':
		print '-st <synthesis type: syn (synthetic), sal (stand-alone), swm (stand-alone with performance metrics); deault is syn>'
		print '-ss <similarity score threshold; default is 80>'
		print '-is <indivudial similarity score threshold; default is 70>'
		print '-bl <type of library: mcapi, mrapi, posix; default is posix>'
		print '-ml <level of the metrics that will be similar: low (IPC, CMR, BMR), high (PP, TC, CCR), all; default is all>'
		print '-ub <number of iterations (upper bound); default is 20>'
		print '-ep <executable file path and arguments in double quote; for example: "/bin/ls -l">'
		print '-co <executable file path and arguments in double quote; for example: "/bin/ls -l">'
		print '-h, --h, -help <shows this help and exit>'
		sys.exit(2)
	count = count + 2

#Check arguments
if SYN_TYPE != 'syn' and SYN_TYPE != 'sal' and SYN_TYPE != 'swm':
	print 'ERROR: "-st ' + SYN_TYPE + '" not set correctly! st can be "syn", "sal", or "swn".'
	sys.exit(3)
if int(SIM_THRESHOLD) < 0 or int(SIM_THRESHOLD) > 100:
	print 'ERROR: "-ss ' + SIM_THRESHOLD + '"not set correctly! ss can be [0, 100].'
	sys.exit(3)
if int(IND_SIM_THRESHOLD) < 0 or int(IND_SIM_THRESHOLD) > 100 or IND_SIM_THRESHOLD > SIM_THRESHOLD:
	print 'ERROR: "-is ' + IND_SIM_THRESHOLD + '"not set correctly! is can be [0, 100] and should be less than ss.'
	sys.exit(3)
if LIB_TYPE != 'posix' and LIB_TYPE != 'mrapi' and LIB_TYPE != 'mcapi':
	print 'ERROR: "-bl ' + LIB_TYPE + '" not set correctly! bl can be "posix", "mrapi", or "mcapi".'
	sys.exit(3)
if SIM_METRIC_TYPE != 'low' and SIM_METRIC_TYPE != 'high' and SIM_METRIC_TYPE != 'all':
	print 'ERROR: "-ml ' + SIM_METRIC_TYPE + '" not set correctly! ml can be "low", "high", or "all".'
	sys.exit(3)

#**********************************************************************************************************************************#
#Global operations
exe = exeStr.split()
editPaths(1, exe)

# synthetic source code
if isSourceExists == 1:
	ITERATION_THRESHOLD = 1
	GEN_SYN = 'false'

if SYN_TYPE == 'syn':
	if not os.access(exe[0], os.X_OK):
		print 'ERROR: Cannot find or execute file!'
		sys.exit(2)
else:
	if not os.access(exe[0], os.R_OK):
		print 'ERROR: Cannot find or read file!'
		sys.exit(2)

appName = os.path.basename(os.path.splitext(exe[0])[0])
appDir = os.path.dirname(os.path.splitext(exe[0])[0])

mkdirP("temp")
cd("temp")
mkdirP(LIB_TYPE)
cd(LIB_TYPE)
mkdirP(appName + '_temp')
cd(appName + '_temp')

synName = appName + '_syn'
CURR_DIR = os.getcwd()
synExe = [CURR_DIR + '/' + appName + '_syn']
synCount = 1

cleanFiles(synName)

#**********************************************************************************************************************************#
if SYN_TYPE == 'syn':
	print '(Step 1) Running Original Application'

	tempCWD = os.getcwd()
	cd(appDir)

	originalCtimeBefore = getChildTime()
	subprocess.call(exe)
	originalCtimeAfter = getChildTime()
	print 'Original runtime:', originalCtimeAfter - originalCtimeBefore

	#print 'Original runtime:'
	#cmd = ['time']
	#index = 0
	#while index < count:
	#	cmd.append(exe[index])
	#	index = index + 1
	#subprocess.call(cmd)

#**********************************************************************************************************************************#
if SYN_TYPE == 'syn':
	print '(Step 2) Original Application Characterization'
	#install perf-tools in order to run the following code line
	cmd = ['perf', 'stat', '-r', CHAR_REPEAT, '-e', 'r1', '-e', 'r2', '-e', 'rc', '-e', 'rf']
	index = 0
	count = len(exe)
	while index < count:
		cmd.append(exe[index])
		index = index + 1
	p = subprocess.Popen(cmd, stdout = open(appName + '.perf.log', 'w'), stderr = open(appName + '.perf.log', 'w'))
	p.wait()

	cmd = ['perf', 'stat', '-r', CHAR_REPEAT, '-e', 'r6e', '-e', 'r6f']
	index = 0
	while index < count:
		cmd.append(exe[index])
		index = index + 1
	p = subprocess.Popen(cmd, stdout = open(appName + '.perf.log', 'a'), stderr = open(appName + '.perf.log', 'a'))
	p.wait()

	#origLogFile = findInSubdirectory('umbra.' + appName + '.', 'proc.log')
	#if origLogFile != '':
	#	os.remove(origLogFile)
	#
	#cmd = [DR_DIR + '/drrun', '-ops', '-thread_private -max_bb_instrs 512 -no_finite_bb_cache -no_finite_trace_cache', 
	#	'-client', UMBRA_DIR + '/build/bin/libaio.so', '0x0', '']
	#index = 0
	#while index < count:
	#	cmd.append(exe[index])
	#	index = index + 1
	#p = subprocess.Popen(cmd)
	#p.wait()

	#origLogFile = findInSubdirectory('umbra.' + appName + '.', 'proc.log')
	#if os.path.exists(origLogFile):
	#	shutil.move(origLogFile, tempCWD)
	if os.path.exists(appName + '.perf.log'):
		shutil.move(appName + '.perf.log', tempCWD)

	cd(tempCWD)

#**********************************************************************************************************************************#
if SYN_TYPE == 'syn':
	print '(Step 3) Pattern Recognition'

	#if synthetic benchmark source code not exists, then generate; else just use existing one
	if isSourceExists == 0:
		origLogFile = findInSubdirectory('umbra.' + appName + '.', 'proc.log')
		cmd = [BENCH_SYN_DIR + '/Debug/MBDT_Project', '-p', origLogFile, '-b', LIB_TYPE, '-g', GEN_SYN]
		subprocess.call(cmd)
else:
	#if synthetic benchmark source code not exists, then generate; else just use existing one
	if isSourceExists == 0:
		cmd = [BENCH_SYN_DIR + '/Debug/MBDT_Project', '-a', exe[0], '-b', LIB_TYPE, '-g', GEN_SYN]
		subprocess.call(cmd)

#**********************************************************************************************************************************#
#retcode = 3: similarity met; retcode = 2: similarity not met; retcode = else: error
iteration = 0
retcode = 2
while (retcode != 3) and (iteration < ITERATION_THRESHOLD):
	iteration = iteration + 1
	print 'Iteration:', iteration
	print '(Step 4) Running Synthetic Benchmark'

	#etem@etem-Dell-System-XPS-L502X:~/workspace/MCA_Project/Debug/src$ ar r libmca.a *.o
	
	if LIB_TYPE == 'posix':
		cmd = ['gcc', '-O0', CURR_DIR + '/' + appName + '_syn.c', '-o', synExe[0], '-lpthread']
	else:
		cmd = ['gcc', '-O0', CURR_DIR + '/' + appName + '_syn.c', '-o', synExe[0],
			'-I', MCA_DIR + '/include', '-L', MCA_DIR + '/Debug/src', '-lmca', '-lpthread']
	#p = subprocess.Popen(cmd)
	#p.wait()
	subprocess.call(cmd)
	
	if os.path.exists(synExe[0]):
		os.chmod(synExe[0], 0744)
	else:
		print 'ERROR:', synExe[0], 'compilation failed!'
		#cleanFiles(synName)
		sys.exit(3)

	synCtimeBefore = getChildTime()
	subprocess.call(synExe[0])
	synCtimeAfter = getChildTime()
	print 'Synthetic runtime:', synCtimeAfter - synCtimeBefore
	
	#print 'Synthetic runtime:'
	#cmd = ['time']
	#cmd.append('-p')
	#index = 0
	#while index < synCount:
	#	cmd.append(synExe[index])
	#	index = index + 1
	#subprocess.call(cmd)
	
#**********************************************************************************************************************************#
	if SYN_TYPE != 'sal':
		print '(Step 5) Synthetic Benchmark Characterization'

		cmd = ['perf', 'stat', '-r', CHAR_REPEAT, '-e', 'r1', '-e', 'r2', '-e', 'rc', '-e', 'rf']
		index = 0
		while index < synCount:
			cmd.append(synExe[index])
			index = index + 1
		p = subprocess.Popen(cmd, stdout = open(synName + '.perf.log', 'w'), stderr = open(synName + '.perf.log', 'w'))
		p.wait()

		cmd = ['perf', 'stat', '-r', CHAR_REPEAT, '-e', 'r6e', '-e', 'r6f']
		index = 0
		while index < synCount:
			cmd.append(synExe[index])
			index = index + 1
		p = subprocess.Popen(cmd, stdout = open(synName + '.perf.log', 'a'), stderr = open(synName + '.perf.log', 'a'))
		p.wait()
		
	#if SYN_TYPE == 'syn':
		#synLogFile = findInSubdirectory('umbra.' + synName + '.', 'proc.log')
		#if synLogFile != '':
		#	os.remove(synLogFile)
		#
		#cmd = [DR_DIR + '/drrun', '-ops', '-thread_private -max_bb_instrs 512 -no_finite_bb_cache -no_finite_trace_cache', 
		#	'-client', UMBRA_DIR + '/build/bin/libaio.so', '0x0', '']
		#index = 0
		#while index < synCount:
		#	cmd.append(synExe[index])
		#	index = index + 1
		#p = subprocess.Popen(cmd)
		#p.wait()

#**********************************************************************************************************************************#
	print '(Step 6) Benchmark Synthesis'
	
	if SYN_TYPE == 'syn':
		origLogFile = findInSubdirectory('umbra.' + appName + '.', 'proc.log')
		synLogFile = findInSubdirectory('umbra.' + synName + '.', 'proc.log')
		cmd = [BENCH_SYN_DIR + '/Debug/MBDT_Project', '-p', origLogFile, CURR_DIR + '/' + appName + '.perf.log', 
			'-c', synLogFile, CURR_DIR + '/' + synName + '.perf.log', '-s', SIM_THRESHOLD, '-i', IND_SIM_THRESHOLD, 
			'-b', LIB_TYPE, '-l', SYN_CODE_LEVEL, '-g', GEN_SYN, '-m', SIM_METRIC_TYPE]
		retcode = subprocess.call(cmd)
	else:
		if SYN_TYPE == 'swm':
			cmd = [BENCH_SYN_DIR + '/Debug/MBDT_Project', '-a', exe[0], 
				'-e', synName + '.perf.log', '-s', SIM_THRESHOLD, '-i', IND_SIM_THRESHOLD, 
				'-b', LIB_TYPE, '-l', SYN_CODE_LEVEL, '-g', GEN_SYN, '-m', SIM_METRIC_TYPE]
		else:
			cmd = [BENCH_SYN_DIR + '/Debug/MBDT_Project', '-a', exe[0], 
				'-s', SIM_THRESHOLD, '-i', IND_SIM_THRESHOLD, 
				'-b', LIB_TYPE, '-l', SYN_CODE_LEVEL, '-g', GEN_SYN, '-m', SIM_METRIC_TYPE]		
		retcode = subprocess.call(cmd)

if SYN_TYPE == 'syn':
	print 'Synthetic benchmark generation completed.'
	print 'Number of iterations:', iteration
else:
	if SYN_TYPE == 'swm':
		print 'Stand-alone benchmark with similarity metrics generation completed.'
		print 'Number of iterations:', iteration
	else:
		print 'Stand-alone benchmark generation completed.'

print '**********************************************************************'

#**********************************************************************************************************************************#
#cleanFiles(synName)

#**********************************************************************************************************************************#
