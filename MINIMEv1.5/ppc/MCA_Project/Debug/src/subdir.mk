################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/mca_utils.c \
../src/mcapi.c \
../src/mrapi.c \
../src/mrapi_impl_spec.c \
../src/mtapi.c \
../src/mthread_impl_posix.c \
../src/posix_utils.c \
../src/sysvr4.c \
../src/transport_sm.c \
../src/transport_sm_mrapi.c 

OBJS += \
./src/mca_utils.o \
./src/mcapi.o \
./src/mrapi.o \
./src/mrapi_impl_spec.o \
./src/mtapi.o \
./src/mthread_impl_posix.o \
./src/posix_utils.o \
./src/sysvr4.o \
./src/transport_sm.o \
./src/transport_sm_mrapi.o 

C_DEPS += \
./src/mca_utils.d \
./src/mcapi.d \
./src/mrapi.d \
./src/mrapi_impl_spec.d \
./src/mtapi.d \
./src/mthread_impl_posix.d \
./src/posix_utils.d \
./src/sysvr4.d \
./src/transport_sm.d \
./src/transport_sm_mrapi.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"../include" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


