/*
 * Copyright (c) 2011-2015,
 * Etem Deniz <etem.deniz@boun.edu.tr>, Bogazici University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * genAlg.c
 *
 *  Created on: Feb 10, 2012
 *      Author: Etem Deniz
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <limits.h>
#include "global.h"
#include "dataPrep.h"
#include "patRecog.h"
#include "benchPrep.h"
#include "benchGen.h"
#include "genAlg.h"

double dataSharingDisThreshold = 1;

paralPattern origPps[MAX_GROUP_NUMBER], candPps[MAX_GROUP_NUMBER];
int origPhaseCount, candPhaseCount;
int similarityThreshold = 80;
int indSimilarityThreshold = 0;
int similarities[MAX_THREADS][MAX_METRIC] = { { 0 } };
int metricCount = 0;
double mrapiDataSharingRatioInc = 0.2;
double mcapiDataSharingRatioInc = 0.002;
double posixDataSharingRatioInc = 0.4;
double dataSharingRatioInc = 0.02;
char *genAlgParFileName = "genAlgParams.txt";
char *resultFileName = "results.txt";
struct tMetric metrics[MAX_METRIC] =  { { "unknown", METRIC_UNKNOWN } };
/* metrics are IPC, CMR, BMR, CCR for cpu and IPC, WFr, WIt, Reg, LcM, CMR, GMR, LMR, PMR, MCl, HtR for gpu*/
struct tStepData oldStepData[MAX_THREADS][ITER_METRIC] = { { { 0, 0.0, 0, false, false } } };
struct tStepData newStepData[MAX_THREADS][ITER_METRIC] = { { { 0, 0.0, 0, false, false } } };
struct tStepData prevOldStepData[MAX_THREADS][ITER_METRIC] = { { { 0, 0.0, 0, false, false } } };
int stepDataId = -1;
boolean isSimScoreMets[MAX_THREADS] = { false };

void saveParameters() {

	FILE *fw;
	int i = 0;

	fw = fopen(genAlgParFileName, "a");
	if (fw == NULL) {
		printf("ERROR! Cannot generate %s.\n", genAlgParFileName);
		return;
	}

	fprintf(fw, "dataSharingRatio: %.3f\n", dataSharingRatio);
	fprintf(fw, "dataPrivateRatio: %.3f\n", dataPrivateRatio);
	fprintf(fw, "dataReadonlyRatio: %.3f\n", dataReadonlyRatio);
	if (isPerThread == true) {
		for (i = 2; i <= maxTid; i++) {
			fprintf(fw, "IPC: %d %d %.3f %u %u\n", newStepData[i][0].id, i, newStepData[i][0].val, newStepData[i][0].count, newStepData[i][0].isInc);
			fprintf(fw, "CMR: %d %d %.3f %u %u\n", newStepData[i][1].id, i, newStepData[i][1].val, newStepData[i][1].count, newStepData[i][1].isInc);
			fprintf(fw, "BMR: %d %d %.3f %u %u\n", newStepData[i][2].id, i, newStepData[i][2].val, newStepData[i][2].count, newStepData[i][2].isInc);
			fprintf(fw, "CCR: %d %d %.3f %u %u\n", newStepData[i][3].id, i, newStepData[i][3].val, newStepData[i][3].count, newStepData[i][3].isInc);
			fprintf(fw, "SIM: %d %u\n", i, isSimScoreMets[i]);
		}
	} else {
		fprintf(fw, "IPC: %d %d %.3f %u %u\n", newStepData[i][0].id, i, newStepData[i][0].val, newStepData[i][0].count, newStepData[i][0].isInc);
		fprintf(fw, "CMR: %d %d %.3f %u %u\n", newStepData[i][1].id, i, newStepData[i][1].val, newStepData[i][1].count, newStepData[i][1].isInc);
		fprintf(fw, "BMR: %d %d %.3f %u %u\n", newStepData[i][2].id, i, newStepData[i][2].val, newStepData[i][2].count, newStepData[i][2].isInc);
		fprintf(fw, "CCR: %d %d %.3f %u %u\n", newStepData[i][3].id, i, newStepData[i][3].val, newStepData[i][3].count, newStepData[i][3].isInc);
	}

	fclose(fw);

	return;
}

void saveParametersGpu() {

	FILE *fw;
	int i = 0;

	fw = fopen(genAlgParFileName, "a");
	if (fw == NULL) {
		printf("ERROR! Cannot generate %s.\n", genAlgParFileName);
		return;
	}

	fprintf(fw, "IPC: %d %d %.3f %u %u\n", newStepData[i][0].id, i, newStepData[i][0].val, newStepData[i][0].count, newStepData[i][0].isInc);
	fprintf(fw, "WFr: %d %d %.3f %u %u\n", newStepData[i][1].id, i, newStepData[i][1].val, newStepData[i][1].count, newStepData[i][1].isInc);
	fprintf(fw, "WIt: %d %d %.3f %u %u\n", newStepData[i][2].id, i, newStepData[i][2].val, newStepData[i][2].count, newStepData[i][2].isInc);
	fprintf(fw, "Reg: %d %d %.3f %u %u\n", newStepData[i][3].id, i, newStepData[i][3].val, newStepData[i][3].count, newStepData[i][3].isInc);
	fprintf(fw, "LcM: %d %d %.3f %u %u\n", newStepData[i][4].id, i, newStepData[i][4].val, newStepData[i][4].count, newStepData[i][4].isInc);
	fprintf(fw, "CMR: %d %d %.3f %u %u\n", newStepData[i][5].id, i, newStepData[i][5].val, newStepData[i][5].count, newStepData[i][5].isInc);
	fprintf(fw, "GMR: %d %d %.3f %u %u\n", newStepData[i][6].id, i, newStepData[i][6].val, newStepData[i][6].count, newStepData[i][6].isInc);
	fprintf(fw, "LMR: %d %d %.3f %u %u\n", newStepData[i][7].id, i, newStepData[i][7].val, newStepData[i][7].count, newStepData[i][7].isInc);
	fprintf(fw, "PMR: %d %d %.3f %u %u\n", newStepData[i][8].id, i, newStepData[i][8].val, newStepData[i][8].count, newStepData[i][8].isInc);
	fprintf(fw, "MCl: %d %d %.3f %u %u\n", newStepData[i][9].id, i, newStepData[i][9].val, newStepData[i][9].count, newStepData[i][9].isInc);
	fprintf(fw, "HtR: %d %d %.3f %u %u\n", newStepData[i][10].id, i, newStepData[i][10].val, newStepData[i][10].count, newStepData[i][10].isInc);

	fclose(fw);

	return;
}

double getParamDoubleValue(char *line) {

	double val;
	char *idBegin, cdata[STR_INT_MAX];

	while (*line != '\n') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cdata, idBegin, line - idBegin);
	cdata[line - idBegin] = '\0';

	val = atof(cdata);

	return val;
}

unsigned int getParamIntValue(char *line) {

	unsigned int val;
	char *idBegin, cdata[STR_INT_MAX];

	while (*line != '\n') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cdata, idBegin, line -	 idBegin);
	cdata[line - idBegin] = '\0';

	val = atoll(cdata);

	return val;
}

void getStepData(char *line, struct tStepData prevOldStepData[MAX_THREADS][ITER_METRIC],
		struct tStepData oldStepData[MAX_THREADS][ITER_METRIC], int index) {

	int i = 0;
	struct tStepData localStepData;

	sscanf(line, "%d %d %lf %u %u", &localStepData.id, &i, &localStepData.val, &localStepData.count, &localStepData.isInc);
	if (localStepData.count != 0) {
		localStepData.isValid = true;
	} else {
		localStepData.isValid = false;
	}

	if (oldStepData[i][index].count == 0) {
		localStepData.initVal = localStepData.val;
	} else {
		localStepData.initVal = oldStepData[i][index].initVal;
	}

	stepDataId = localStepData.id;

	// shifting
	memcpy(&prevOldStepData[i][index], &oldStepData[i][index], sizeof(struct tStepData));
	memcpy(&oldStepData[i][index], &localStepData, sizeof(struct tStepData));

	return;
}

void getSimData(char *line) {

	int i = 0;
	boolean isSimilar;

	sscanf(line, "%d %u", &i, &isSimilar);
	isSimScoreMets[i] = isSimilar;

	return;
}

void loadStepDataId() {

	char line[STR_LINE_MAX];
	FILE *fr;

	/* load values */
	fr = fopen(genAlgParFileName, "r+");
	if (fr != NULL) {
		while (!feof(fr)){
			fgets(line, STR_LINE_MAX, fr);
			if (strncmp(line, "IPC", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 0);
			}
		}
		fclose(fr);
	} else {
		//printf("ERROR! Cannot open %s.\n", genAlgParFileName);
		return;
	}

	return;
}

void loadParameters() {

	char line[STR_LINE_MAX];
	FILE *fr;

	/* load values */
	fr = fopen(genAlgParFileName, "r+");
	if (fr != NULL) {
		while (!feof(fr)){
			fgets(line, STR_LINE_MAX, fr);
			if (strncmp(line, "dataSharingRatio", 16) == 0) {
				dataSharingRatio = getParamDoubleValue(line);
			} else if (strncmp(line, "dataPrivateRatio", 16) == 0) {
				dataPrivateRatio = getParamDoubleValue(line);
			} else if (strncmp(line, "dataReadonlyRatio", 17) == 0) {
				dataReadonlyRatio = getParamDoubleValue(line);
			} else if (strncmp(line, "IPC", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 0);
			} else if (strncmp(line, "CMR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 1);
			} else if (strncmp(line, "BMR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 2);
			} else if (strncmp(line, "CCR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 3);
			} else if (strncmp(line, "SIM", 3) == 0) {
				getSimData(line + 5);
			}
		}
		fclose(fr);
	} else {
		//printf("ERROR! Cannot open %s.\n", genAlgParFileName);
		return;
	}

	return;
}

void loadParametersGpu() {

	char line[STR_LINE_MAX];
	FILE *fr;

	/* load values */
	fr = fopen(genAlgParFileName, "r+");
	if (fr != NULL) {
		while (!feof(fr)){
			fgets(line, STR_LINE_MAX, fr);
			if (strncmp(line, "IPC", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 0);
			} else if (strncmp(line, "WFr", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 1);
			} else if (strncmp(line, "WIt", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 2);
			} else if (strncmp(line, "Reg", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 3);
			} else if (strncmp(line, "LcM", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 4);
			} else if (strncmp(line, "CMR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 5);
			} else if (strncmp(line, "GMR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 6);
			} else if (strncmp(line, "LMR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 7);
			} else if (strncmp(line, "PMR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 8);
			} else if (strncmp(line, "MCl", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 9);
			} else if (strncmp(line, "HtR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 10);
			}
		}
		fclose(fr);
	} else {
		//printf("ERROR! Cannot open %s.\n", genAlgParFileName);
		return;
	}

	return;
}

int compareParallelPatterns() {

	int i, matchCount = 0;
	int sim = 0;

	if (origPhaseCount != candPhaseCount) {
		return sim;
	}

	for (i = 0; i < origPhaseCount; i++) {
		if (origPps[i] == candPps[i]) {
			matchCount++;
		}
	}

	sim = (matchCount / origPhaseCount) * SIMILARITY_MAX;

	return sim;
}

int compareComm() {

	int i, j, k;
	int numOfThreads, thresholdOrig, thresholdCand;
	struct tProcData *proc, *cand;
	struct tThreadData *origTd, *candTd;
	int sim = 0;
	int dd = 0, dn = 0, nd = 0, nn = 0, total = 0; /* d: dependent, n: not */
	boolean isOrigDep, isCandDep;

	for (i = 0; i < procCount; i++) {
		proc = procs[i];
		cand = cands[i];
		numOfThreads = proc->numOfThreads;

		for (j = 1; j < numOfThreads; j++) {
			origTd = proc->threads[j];
			candTd = cand->threads[j];
			if (origTd != NULL && candTd != NULL) {

				thresholdOrig = (int) (origTd->cacheInvalid * cahceMissRatio);
				thresholdCand = (int) (candTd->cacheInvalid * cahceMissRatio);
				for (k = 1; k <= numOfThreads; k++) {
					if (j != k) {
						if (origTd->groupId != -1 && proc->threads[k]->groupId != -1) {
							if (origTd->groupId != activeProc->threads[k]->groupId) {
								continue;
							}
						}
						//FIXME: origTd and candTd are in same group? Compare?!

						if (origTd->dependency[k] > thresholdOrig) {
							isOrigDep = true;
						} else {
							isOrigDep = false;
						}
						if (candTd->dependency[k] > thresholdCand) {
							isCandDep = true;
						} else {
							isCandDep = false;
						}

						if (isOrigDep && isCandDep) {
							dd++;
						} else if (isCandDep && !isCandDep) {
							dn++;
						} else if (!isOrigDep && isCandDep) {
							nd++;
						} else {
							nn++;
						}
						total++;
					}
				}

			}
		}

		if (numOfThreads == 1) {
			nn++;
		}

	}

	sim = (int) ((((double) (dd + nn)) / ((double) (dd + nn + dn + nd))) * SIMILARITY_MAX);

	return sim;
}

int compareDataSharing() {

	double dataSharing[MAX_DATA_SHR];
	double multiplier, dist;
	double max = MIN_DIST;
	int i, j, activeGroupId;
	int sim = 0, simCount = 0, totalCount = 0;

	dataSharingDisThreshold = scoreMax / 5; /* initialize */

	if (maxGroupId != maxCandGroupId) {
		return sim;
	}

	for (activeGroupId = 0; activeGroupId < maxGroupId; activeGroupId++) {

		dataSharing[0] = (double) activeProc->dataSharing[activeGroupId].readonly;
		dataSharing[1] = (double) activeProc->dataSharing[activeGroupId].migratory;
		dataSharing[2] = (double) activeProc->dataSharing[activeGroupId].prodCons;
		dataSharing[3] = (double) activeProc->dataSharing[activeGroupId].private;

		for (i = 0; i < MAX_DATA_SHR; i++) {
			if (dataSharing[i] > max) {
				max = dataSharing[i];
			}
		}

		multiplier = scoreMax / max;

		for (i = 0; i < MAX_DATA_SHR; i++) {
			dataSharing[i] *= multiplier;
		}

		for (j = 0; j < MAX_DATA_SHR; j++) {
			dist = dataSharing[j] - ppDataSharingPattern[j][origPps[activeGroupId]];
			if (dist > dataSharingDisThreshold) {
				simCount++;
			}
			totalCount++;
		}

	}

	return simCount / totalCount;
}

/*
 * error = (value - reference) / reference
 * if (error is larger than 1 then similarity score is 0)
 */

double calculateError(double value, double reference) {

	double retVal;

	if (reference == 0) {
		reference = MIN_DATA_VALUE;
		if (value == 0) {
			value = MIN_DATA_VALUE;
		}
	}

	retVal = fabs(value - reference) / reference;
	if (retVal > 1) {
		retVal = 1;
	}

	return retVal;
}

double calculateSimScore(double value, double reference) {

	return (1 - calculateError(value, reference)) * SIMILARITY_MAX;

}

int compareIpc(int indexId) {

	int i = 0, itemCount = 0, sim = 0;
	double vals = 0, val;

	if (isPerThread == true) {
		val = calculateError(cands[i]->threads[indexId]->ipcInfo.ipc, procs[i]->threads[indexId]->ipcInfo.ipc);
		vals += 1 - val;
		itemCount++;
	} else {
		val = calculateError(cands[indexId]->ipcInfo.ipc, procs[indexId]->ipcInfo.ipc);
		vals += 1 - val;
		itemCount++;
	}

	sim = (vals / (double) itemCount) * SIMILARITY_MAX;

	return sim;
}

int compareCacheMisses(int indexId) {

	int i = 0, itemCount = 0, sim = 0;
	double vals = 0, val;

	if (isPerThread == true) {
		val = calculateError(cands[i]->threads[indexId]->cacheInfo.cacheMissRatio, procs[i]->threads[indexId]->cacheInfo.cacheMissRatio);
		vals += 1 - val;
		itemCount++;
	} else {
		val = calculateError(cands[indexId]->cacheInfo.cacheMissRatio, procs[indexId]->cacheInfo.cacheMissRatio);
		vals += 1 - val;
		itemCount++;
	}

	sim = (vals / (double) itemCount) * SIMILARITY_MAX;

	return sim;
}

int compareBranchMisses(int indexId) {

	int i = 0, itemCount = 0, sim = 0;
	double vals = 0, val;

	if (isPerThread == true) {
		val = calculateError(cands[i]->threads[indexId]->branchInfo.branchMissRatio, procs[i]->threads[indexId]->branchInfo.branchMissRatio);
		vals += 1 - val;
		itemCount++;
	} else {
		val = calculateError(cands[indexId]->branchInfo.branchMissRatio, procs[indexId]->branchInfo.branchMissRatio);
		vals += 1 - val;
		itemCount++;
	}

	sim = (vals / (double) itemCount) * SIMILARITY_MAX;

	return sim;

}

int compareCommToComp() {

	int i = 0, itemCount = 0, sim = 0;
	double vals = 0, val = 0;

	val = calculateError(cands[i]->instructions.ccRatio, procs[i]->instructions.ccRatio);
	vals += 1 - val;
	itemCount++;

	sim = (vals / (double) itemCount) * SIMILARITY_MAX;

	return sim;
}

int measureOverallSim(int appCount, char *apps[MAX_PROCS], int candCount, char *cands[MAX_PROCS], int indexId) {

	int sim, totalSim = 0;
	int i, activeMetricCount = 0;

	metricCount = 0;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		similarities[indexId][metricCount] = compareParallelPatterns();
		activeMetricCount++;
	}
	metricCount++;

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		similarities[index][metricCount] = compareComm();
//		activeMetricCount++;
//	}
//	metricCount++;

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		similarities[index][metricCount] = compareDataSharing();
//		activeMetricCount++;
//	}
//	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareIpc(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareCacheMisses(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareBranchMisses(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		similarities[indexId][metricCount] = compareCommToComp();
		activeMetricCount++;
	}
	metricCount++;

	for (i = 0; i < metricCount; i++) {
		totalSim += similarities[indexId][i];
	}

	sim = totalSim / activeMetricCount;

	return sim;
}

int measureOverallSimSa(int appCount, char *apps[MAX_PROCS], int candCount, char *cands[MAX_PROCS], int indexId) {

	int sim, totalSim = 0;
	int i, activeMetricCount = 0;

	metricCount = 0;

	metricCount++; /* pp */
	metricCount++; /* comm */

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareIpc(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareCacheMisses(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareBranchMisses(indexId);
		activeMetricCount++;
	}
	metricCount++;

	metricCount++; /* comm to comp */

	for (i = 0; i < metricCount; i++) {
		totalSim += similarities[indexId][i];
	}

	sim = totalSim / activeMetricCount;
	return sim;
}

void dRoundAndFixRanges2(double *val) {

	if (*val < MIN_DATA_VALUE_GPU) {
		*val = MIN_DATA_VALUE_GPU;
	}

	*val = floorf(*val * 100 + 0.5) / 100;

}

void dRoundAndFixRanges3(double *val) {

	if (*val < MIN_DATA_VALUE_GPU) {
		*val = MIN_DATA_VALUE_GPU;
	}

	*val = floorf(*val * 1000 + 0.5) / 1000;

}

int measureOverallSimGpu(int indexId, boolean isVerbose) {

	int sim, totalSim = 0;
	int i = 0;
	double orgTotalComp, orgTotalComm;
	double orgGlobalMem, orgLocalMem, orgPrivateMem, orgTotalMem;
	double orgTotalAccesses, orgTotalCoalescedAccesses;
	double synTotalComp, synTotalComm;
	double synGlobalMem, synLocalMem, synPrivateMem, synTotalMem;
	double synTotalAccesses, synTotalCoalescedAccesses;
	double orgCMR, synCMR, orgGlobalMemRatio, orgCoalescedAccessRatio, synCoalescedAccessRatio;
	double synGlobalMemRatio, orgLocalMemRatio, synLocalMemRatio, orgPrivateMemRatio, synPrivateMemRatio;
	struct tProcData *currProc;

	// Original
	currProc = procs[indexId];
	orgTotalComp = (double) currProc->gpuExt.branchInstructions + (double) currProc->gpuExt.scalarALUInstructions +
			(double) currProc->gpuExt.vectorALUInstructions;
	orgTotalComm = (double) currProc->gpuExt.LDSInstructions + (double) currProc->gpuExt.scalarMemInstructions +
			(double) currProc->gpuExt.vectorMemInstructions;

	orgGlobalMem = (double) currProc->gpuExt.vectorMemInstructions + (double) currProc->gpuExt.scalarMemInstructions;
	orgLocalMem = (double) currProc->gpuExt.LDSInstructions;
	orgPrivateMem = 0;
	for (i = 0; i < MAX_COMPUTE_UNITS; i++) {
		orgPrivateMem += (double) currProc->gpuExt.computeUnitData[i].scalarRegReads
				+ (double) currProc->gpuExt.computeUnitData[i].scalarRegWrites
				+ (double) currProc->gpuExt.computeUnitData[i].vectorRegReads
				+ (double) currProc->gpuExt.computeUnitData[i].vectorRegWrites;
	}
	orgTotalMem = orgGlobalMem + orgLocalMem + orgPrivateMem;

	orgTotalAccesses = 0;
	orgTotalCoalescedAccesses = 0;
	for (i = 0; i < MAX_COMPUTE_UNITS; i++) {
		orgTotalAccesses += (double) currProc->gpuExt.computeUnitData[i].LDS.accesses;
		orgTotalCoalescedAccesses += (double) currProc->gpuExt.computeUnitData[i].LDS.coalescedReads +
				(double) currProc->gpuExt.computeUnitData[i].LDS.coalescedWrites;
	}

#ifdef DEBUG_ON
	printf("[DEBUG] Original #Instructions, IPC: %d, %f\n", currProc->gpuExt.instructions,
			(double) currProc->gpuExt.instructions / (double) currProc->gpuExt.cycles);
#endif

	orgCMR = orgTotalComp != 0 ? orgTotalComm / orgTotalComp : 0;
	orgGlobalMemRatio = orgTotalMem != 0 ? orgGlobalMem / orgTotalMem : 0;
	orgLocalMemRatio = orgTotalMem != 0 ? orgLocalMem / orgTotalMem : 0;
	orgPrivateMemRatio = orgTotalMem != 0 ? orgPrivateMem / orgTotalMem : 0;
	orgCoalescedAccessRatio = orgTotalAccesses != 0 ? orgTotalCoalescedAccesses / orgTotalAccesses : 0;

	if (isRangeAndRoundData == true) {
		dRoundAndFixRanges2(&orgCMR);
		dRoundAndFixRanges2(&orgGlobalMemRatio);
		dRoundAndFixRanges2(&orgLocalMemRatio);
		dRoundAndFixRanges2(&orgPrivateMemRatio);
		dRoundAndFixRanges2(&orgCoalescedAccessRatio);
	}

	// Synthetic
	currProc = cands[indexId];
	synTotalComp = (double) currProc->gpuExt.branchInstructions + (double) currProc->gpuExt.scalarALUInstructions +
			(double) currProc->gpuExt.vectorALUInstructions;
	synTotalComm = (double) currProc->gpuExt.LDSInstructions + (double) currProc->gpuExt.scalarMemInstructions +
			(double) currProc->gpuExt.vectorMemInstructions;

	synGlobalMem = (double) currProc->gpuExt.vectorMemInstructions + (double) currProc->gpuExt.scalarMemInstructions;
	synLocalMem = (double) currProc->gpuExt.LDSInstructions;
	synPrivateMem = 0;
	for (i = 0; i < MAX_COMPUTE_UNITS; i++) {
		synPrivateMem += (double) currProc->gpuExt.computeUnitData[i].scalarRegReads
				+ (double) currProc->gpuExt.computeUnitData[i].scalarRegWrites
				+ (double) currProc->gpuExt.computeUnitData[i].vectorRegReads
				+ (double) currProc->gpuExt.computeUnitData[i].vectorRegWrites;
	}
	synTotalMem = synGlobalMem + synLocalMem + synPrivateMem;

	synTotalAccesses = 0;
	synTotalCoalescedAccesses = 0;
	for (i = 0; i < MAX_COMPUTE_UNITS; i++) {
		synTotalAccesses += (double) currProc->gpuExt.computeUnitData[i].LDS.accesses;
		synTotalCoalescedAccesses += (double) currProc->gpuExt.computeUnitData[i].LDS.coalescedReads +
				(double) currProc->gpuExt.computeUnitData[i].LDS.coalescedWrites;
	}

	synCMR = synTotalComp != 0 ? synTotalComm / synTotalComp : 0;
	synGlobalMemRatio = synTotalMem != 0 ? synGlobalMem / synTotalMem : 0;
	synLocalMemRatio = synTotalMem != 0 ? synLocalMem / synTotalMem : 0;
	synPrivateMemRatio = synTotalMem != 0 ? synPrivateMem / synTotalMem : 0;
	synCoalescedAccessRatio = synTotalAccesses != 0 ? synTotalCoalescedAccesses / synTotalAccesses : 0;

#ifdef DEBUG_ON
	printf("[DEBUG] Synthetic #Instructions, IPC: %d, %f\n", currProc->gpuExt.instructions,
			(double) currProc->gpuExt.instructions / (double) currProc->gpuExt.cycles);

	printf("[DEBUG] Synthetic Global, Local, Private MR: %d, %d, %d; %f, %f, %f\n",
			(int) synGlobalMem, (int) synLocalMem, (int) synPrivateMem,
			synGlobalMemRatio, synLocalMemRatio, synPrivateMemRatio);
#endif

	if (isRangeAndRoundData == true) {
		dRoundAndFixRanges2(&synCMR);
		dRoundAndFixRanges2(&synGlobalMemRatio);
		dRoundAndFixRanges2(&synLocalMemRatio);
		dRoundAndFixRanges2(&synPrivateMemRatio);
		dRoundAndFixRanges2(&synCoalescedAccessRatio);
	}

	procs[indexId]->gpuExt.CMR = orgCMR;
	procs[indexId]->gpuExt.memory.globalMemCount = orgGlobalMem;
	procs[indexId]->gpuExt.memory.localMemCount = orgLocalMem;
	procs[indexId]->gpuExt.memory.privateMemCount = orgPrivateMem;
	procs[indexId]->gpuExt.memory.totalCount = orgTotalMem;
	procs[indexId]->gpuExt.globalMemRatio = orgGlobalMemRatio;
	procs[indexId]->gpuExt.localMemRatio = orgLocalMemRatio;
	procs[indexId]->gpuExt.privateMemRatio = orgPrivateMemRatio;
	procs[indexId]->gpuExt.coalescedAccessRatio = orgCoalescedAccessRatio;

	cands[indexId]->gpuExt.CMR = synCMR;
	cands[indexId]->gpuExt.memory.globalMemCount = synGlobalMem;
	cands[indexId]->gpuExt.memory.localMemCount = synLocalMem;
	cands[indexId]->gpuExt.memory.privateMemCount = synPrivateMem;
	cands[indexId]->gpuExt.memory.totalCount = synTotalMem;
	cands[indexId]->gpuExt.globalMemRatio = synGlobalMemRatio;
	cands[indexId]->gpuExt.localMemRatio = synLocalMemRatio;
	cands[indexId]->gpuExt.privateMemRatio = synPrivateMemRatio;
	cands[indexId]->gpuExt.coalescedAccessRatio = synCoalescedAccessRatio;

	// Calculation
	metricCount = 0;

	similarities[indexId][metricCount++] = calculateSimScore(cands[indexId]->gpuExt.instructionsPerCycle, procs[indexId]->gpuExt.instructionsPerCycle);
	similarities[indexId][metricCount++] = calculateSimScore(cands[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[1],
			procs[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[1]);
	similarities[indexId][metricCount++] = calculateSimScore(cands[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0],
			procs[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0]);
	similarities[indexId][metricCount++] = calculateSimScore(cands[indexId]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0],
			procs[indexId]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0]);
	similarities[indexId][metricCount++] = calculateSimScore(cands[indexId]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0],
			procs[indexId]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0]);
	similarities[indexId][metricCount++] = calculateSimScore(synCMR, orgCMR);
	similarities[indexId][metricCount++] = calculateSimScore(synGlobalMemRatio, orgGlobalMemRatio);
	similarities[indexId][metricCount++] = calculateSimScore(synLocalMemRatio, orgLocalMemRatio);
	similarities[indexId][metricCount++] = calculateSimScore(synPrivateMemRatio, orgPrivateMemRatio);
	similarities[indexId][metricCount++] = calculateSimScore(synCoalescedAccessRatio, orgCoalescedAccessRatio);
	similarities[indexId][metricCount++] = calculateSimScore(cands[indexId]->gpuExt.memory.hitRatio, procs[indexId]->gpuExt.memory.hitRatio);

	for (i = 0; i < metricCount; i++) {
		totalSim += similarities[indexId][i];
	}

	sim = totalSim / metricCount;

	// FIXME: not use 1, 2, 3, and 10 >>>>>>>>>>>>
	totalSim = similarities[indexId][0] + similarities[indexId][4] + similarities[indexId][5] +
	similarities[indexId][6] + similarities[indexId][7] + similarities[indexId][8] + similarities[indexId][9];
	sim = totalSim / 7;
	// <<<<<<<<<<<<

	return sim;
}

void setStepData(struct tStepData *newStepData, double val, unsigned int count, boolean isInc) {

	newStepData->id = stepDataId + 1;
	newStepData->val = val;
	newStepData->count = count;
	newStepData->isInc = isInc;

	return;
}

/*
 * numerator/denominator
 */
 
unsigned int calculateInitVal(long long unsigned int num, long long unsigned int den, double origRatio, double blockRatio) {

	unsigned int count;

#ifdef DEBUG_ON
	printf("[DEBUG] Calculating initial value\n");
#endif

	count = fabs(num - (origRatio * den)) / fabs(origRatio - blockRatio); // origRatio = (num + (count * blockRatio)) / (den + count)

	return count;

}

//FIXME: update calculateNewVal as calculateNewValGpu?
boolean calculateNewVal(int index, struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, double newCandVal, double origVal, unsigned int *newCount, boolean *newDir) {

	double diff;
	boolean retVal = true;
	int localCount;

#ifdef DEBUG_ON
	printf("[DEBUG] Calculating new value by using old one.\n");
#endif

	if (pOldStepData->id > 0) {
		if (pPrevOldStepData->count == pOldStepData->count) {
			retVal = false;
		}
	}

	if (retVal) {
		if (pOldStepData->initVal >= newCandVal) { /* from high to low */
			diff = pOldStepData->val - newCandVal;
			if (diff > 0) {
				localCount = (pOldStepData->count * (newCandVal - origVal)) / diff;
				if (localCount == 0) localCount = newCandVal - origVal > 0 ? 1 : -1;
				if (localCount >= 0) { /* We have a value between oldVal and origVal */
					*newCount = pOldStepData->count + localCount; /* incremental learning */
					*newDir = pOldStepData->isInc; /* should be 'false' */
				} else { /* We have a value smaller than origVal */
					*newCount = pOldStepData->count + localCount; /* localCount is negative */
					*newDir = pOldStepData->isInc;
				}
			} else {
//				retVal = false;
				*newCount = pOldStepData->count * 2;
				*newDir = pOldStepData->isInc;
			}
		} else { /* from low to high */
			diff = newCandVal - pOldStepData->val;
			if (diff > 0) {
				localCount = (pOldStepData->count * (origVal - newCandVal)) / diff;
				if (localCount == 0) localCount = origVal - newCandVal > 0 ? 1 : -1;
				if (localCount >= 0) { /* We have a value between oldVal and origVal */
					*newCount = pOldStepData->count + localCount; /* incremental learning */
					*newDir = pOldStepData->isInc; /* should be 'true' */
				} else { /* We have a value bigger than origVal */
					*newCount = pOldStepData->count + localCount; /* localCount is negative */
					*newDir = pOldStepData->isInc;
				}
			} else {
//				retVal = false;
				*newCount = pOldStepData->count * 2;
				*newDir = pOldStepData->isInc;
			}
		}
	}

	if (*newCount > MAX_LOOP_COUNT) {
		retVal = false;
	}

	return retVal;
}

boolean calculateNewValGpu(int index, struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, double newCandVal, double origVal, unsigned int *newCount, boolean *newDir) {

	double diff;
	boolean retVal = true;
	int localCount;

#ifdef DEBUG_ON
	printf("[DEBUG] Calculating new value by using old one.\n");
#endif

	if (pOldStepData->id > 0) {
		if (pPrevOldStepData->count == pOldStepData->count) {
			retVal = false;
		}
	}

	if (retVal) {
		if (pOldStepData->initVal >= newCandVal) { /* from high to low */
			diff = pOldStepData->val - newCandVal;
//			if (diff > 0) {
				localCount = (pOldStepData->count * (newCandVal - origVal)) / diff;
				if (localCount == 0) localCount = newCandVal - origVal > 0 ? 1 : -1;
				if (localCount >= 0) { /* We have a value between oldVal and origVal */
					*newCount = pOldStepData->count + localCount; /* incremental learning */
					*newDir = pOldStepData->isInc; /* should be 'false' */
				} else { /* We have a value smaller than origVal */
					*newCount = pOldStepData->count + localCount; /* localCount is negative */
					*newDir = pOldStepData->isInc;
				}
//			} else {
////				retVal = false;
//				*newCount = pOldStepData->count * 2;
//				*newDir = pOldStepData->isInc;
//			}
		} else { /* from low to high */
			diff = newCandVal - pOldStepData->val;
//			if (diff > 0) {
				localCount = (pOldStepData->count * (origVal - newCandVal)) / diff;
				if (localCount == 0) localCount = origVal - newCandVal > 0 ? 1 : -1;
				if (localCount >= 0) { /* We have a value between oldVal and origVal */
					*newCount = pOldStepData->count + localCount; /* incremental learning */
					*newDir = pOldStepData->isInc; /* should be 'true' */
				} else { /* We have a value bigger than origVal */
					*newCount = pOldStepData->count + localCount; /* localCount is negative */
					*newDir = pOldStepData->isInc;
				}
//			} else {
////				retVal = false;
//				*newCount = pOldStepData->count * 2;
//				*newDir = pOldStepData->isInc;
//			}
		}
	}

//	if (*newCount > MAX_LOOP_COUNT) {
//		retVal = false;
//	}

	return retVal;
}

int getIndexOfMinValue(int *arr, int arrSize) {

	int index = 0, minVal = INT_MAX;
	int i;

	for (i = 0; i < arrSize; i++) {
		if (arr[i] < minVal) {
			minVal = arr[i];
			index = i;
		}
	}

	return index;
}

void updateComm(int phaseIndex) {

	int i, targetIndex;
	double diff, maxDiff = 0;

	for (i = 0; i < MAX_COMM; i++) {
		diff = procs[0]->parPatternScores.tcSubScores[phaseIndex][i]
		        - cands[0]->parPatternScores.tcSubScores[phaseIndex][i];
		if (diff > maxDiff) {
			maxDiff = diff;
			targetIndex = i;
		}
	}

	//FIXME: do better operation for each
	if (targetIndex == 0) { /* none */
//		if (dataSharingRatio > dataSharingRatioInc) {
//			dataSharingRatio -= dataSharingRatioInc;
//		}
		dataSharingRatio += dataSharingRatioInc;
	} else if (targetIndex == 1) { /* few */
		dataSharingRatio += dataSharingRatioInc;
	} else { /* many */
		dataSharingRatio += dataSharingRatioInc;
	}

	return;
}

void updateDataSharing(int phaseIndex) {

	int i, targetIndex;
	double diff, maxDiff = 0;

	for (i = 0; i < MAX_DATA_SHR; i++) {
		diff = procs[0]->parPatternScores.dsSubScores[phaseIndex][i]
		        - cands[0]->parPatternScores.dsSubScores[phaseIndex][i];
		if (diff > maxDiff) {
			maxDiff = diff;
			targetIndex = i;
		}
	}

	//FIXME: do better operation for each
	if (targetIndex == 0) { /* read-only */
		dataReadonlyRatio += dataSharingRatioInc;
	} else if (targetIndex == 1) { /* migratory */
		dataSharingRatio += dataSharingRatioInc;
	} else if (targetIndex == 2) { /* prod/con */
		dataSharingRatio += dataSharingRatioInc;
	} else { /* private */
		dataPrivateRatio += dataSharingRatioInc;
	}

	return;
}

void updateGenThreading(int phaseIndex) {

	int i, targetIndex;
	double diff, maxDiff = 0;

	for (i = 0; i < MAX_GEN_THREADING; i++) {
		diff = procs[0]->parPatternScores.gtSubScores[phaseIndex][i]
		        - cands[0]->parPatternScores.gtSubScores[phaseIndex][i];
		if (diff > maxDiff) {
			maxDiff = diff;
			targetIndex = i;
		}
	}

	//FIXME: do better operation for each
	if (targetIndex == 0) { /* pc (unique) */
		/* this case should never occur */
	} else if (targetIndex == 1) { /* thread balanced */
		dataSharingRatio += dataSharingRatioInc;
	} else if (targetIndex == 2){ /* creator (same) */
		/* this case should never occur */
	} else if (targetIndex == 3) { /* lifetime */
		dataSharingRatio += dataSharingRatioInc;
	} else if (targetIndex == 4) { /* creation time */
		dataSharingRatio += dataSharingRatioInc;
	} else { /* exit time */
		dataSharingRatio += dataSharingRatioInc;
	}

	return;
}

void updateSyncCon(int phaseIndex) {

	//FIXME: not used, currently so noop

	return;
}

//FIXME: Library causes wrong data sharing values
void updateParPattern() {

	int i, minScore = SIMILARITY_MAX, minIndex = 0;

	//FIXME: reset all other counts
//	ipcCount = 0;
//	cacheMissCount = 0;
//	branchMissCount = 0;
//	commToCompCount = 0;

	if (origPhaseCount != candPhaseCount) {
		//TODO: add/remove phases to/from candidate benchmark
	}

	for (i = 0; i < origPhaseCount; i++) {
		if (origPps[i] != candPps[i]) {
			/* find */
			if (weightDS > 0 && cands[0]->parPatternScores.dsScores[i][origPps[i]] < minScore) {
				minIndex = 0;
				minScore = cands[0]->parPatternScores.dsScores[i][origPps[i]];
			}
			if (weightTC > 0 && cands[0]->parPatternScores.commScores[i][origPps[i]] < minScore) {
				minIndex = 1;
				minScore = cands[0]->parPatternScores.commScores[i][origPps[i]];
			}
			if (weightGT > 0 && cands[0]->parPatternScores.genThreadingScores[i][origPps[i]] < minScore) {
				minIndex = 2;
				minScore = cands[0]->parPatternScores.genThreadingScores[i][origPps[i]];
			}
			if (weightSC > 0 && cands[0]->parPatternScores.syncConScores[i][origPps[i]] < minScore) {
				minIndex = 3;
				minScore = cands[0]->parPatternScores.syncConScores[i][origPps[i]];
			}

			/* update */
			if (minIndex == 0) { /* ds */
				updateDataSharing(i);
			} else if (minIndex == 1) { /* comm */
				updateComm(i);
			} else if (minIndex == 2) { /* genThreading */
				updateGenThreading(i);
			} else { /* syncCon */
				updateSyncCon(i);
			}
		}
	}

	return;
}

/*
 * Reciprocal throughput:
 * The throughput is the maximum number of instructions of the same kind that can be executed per clock cycle
 * ADD SUB r,r/i 1 1 x x x 1 0.33 --> 	IPC = 1/0.33 = 3
 * IDIV r32 9 9 20-27 11-18 --> IPC = 1/10 = 0.1
 * */
 
void updateIpc(struct tIpcInfo *pOrigIpcInfo, struct tIpcInfo *pCandIpcInfo, struct tStepData *pPrevOldStepData,
		struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateData *pUpdateData) {

	double ipcInc = 2.5;
	double ipcDec = 0.2;
	double origIpc;

	//FIXME: I need to save per process parameters and then use them in this kind of functions

	origIpc = pOrigIpcInfo->ipc;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewVal(0, pPrevOldStepData, pOldStepData, pCandIpcInfo->ipc, origIpc, &pUpdateData->ipcCount, &pUpdateData->isIpcInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->ipcCount = 0;
			pUpdateData->isIpcInc = true;
		}
	} else {
		if (pCandIpcInfo->ipc >= origIpc) { // decrement
			pUpdateData->ipcCount = calculateInitVal(pCandIpcInfo->instructions, pCandIpcInfo->cycles, origIpc, ipcDec);
			pUpdateData->ipcCount = pUpdateData->ipcCount / 600;
			pUpdateData->isIpcInc = false;
		} else {
			pUpdateData->ipcCount = calculateInitVal(pCandIpcInfo->instructions, pCandIpcInfo->cycles, origIpc, ipcInc);
			pUpdateData->ipcCount = pUpdateData->ipcCount / 400;
			pUpdateData->isIpcInc = true;
		}
		if (isPerThread == true) {
			pUpdateData->ipcCount /= 4;
		}
	}

	setStepData(pNewStepData, pCandIpcInfo->ipc, pUpdateData->ipcCount, pUpdateData->isIpcInc);

#ifdef DEBUG_ON
	printf("[DEBUG] IPC: new count = %u, is increment = %s\n", pUpdateData->ipcCount, pUpdateData->isIpcInc == true ? "true" : "false");
#endif

	return;
}

/*
 * int i;
 * int blockSize = 1024*1024*4; //4MB
 * int loopCount = 1000;
 *
 * int *block = (int*) malloc(blockSize * sizeof(int));
 * for (i = 0; i < loopCount; i++) {
 * 	block[rand() % blockSize] = 0;
 * }
 * free(block);
 *
 * loopCount = 100, miss rate = 10%.
 * loopCount = 1000, miss rate = 18%.
 * loopCount = 10000, miss rate = 24%.
 * loopCount = 100000, miss rate = 36%.
 * loopCount = 1000000, miss rate = 55%.
 * loopCount = 10000000, miss rate = 60%.
 *
 * 	int a, lc, i, j, x[256][256];
 *	lc = 1;
 *
 *	for (a = 0; a < lc; a++) {
 *		for (j = 0; j < 256; j++) {
 *			for (i = 0; i < 256; i++) {
 *				x[i][j] = 2 * x[i][j];
 *			}
 *		}
 *	}
 *
 * lc = 1, miss rate = 1.8%
 * lc = 10, miss rate = 0.7%
 * lc = 100, miss rate = 0.07%
 * lc = 1000, miss rate = 0.005%
 * */

void updateCmr(struct tCacheInfo *pOrigCacheInfo, struct tCacheInfo *pCandCacheInfo, struct tStepData *pPrevOldStepData,
		struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateData *pUpdateData) {

	double cmrInc = 0.5;
	double cmrDec = 0.002;
	double origCmr;

	origCmr = pOrigCacheInfo->cacheMissRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewVal(1, pPrevOldStepData, pOldStepData, pCandCacheInfo->cacheMissRatio, origCmr, &pUpdateData->cacheMissCount, &pUpdateData->isCacheMissInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->cacheMissCount = 0;
			pUpdateData->isCacheMissInc = true;
		}
	} else {
		if (pCandCacheInfo->cacheMissRatio >= origCmr) { // decrement
			pUpdateData->cacheMissCount = calculateInitVal(pCandCacheInfo->cacheMisses, pCandCacheInfo->cacheReferences, origCmr, cmrDec);
			pUpdateData->cacheMissCount = pUpdateData->cacheMissCount / 500;
			pUpdateData->isCacheMissInc = false;
		} else {
			pUpdateData->cacheMissCount = calculateInitVal(pCandCacheInfo->cacheMisses, pCandCacheInfo->cacheReferences, origCmr, cmrInc);
			pUpdateData->isCacheMissInc = true;
		}
		if (isPerThread == true) {
			pUpdateData->cacheMissCount /= 8;
		}
	}

	setStepData(pNewStepData, pCandCacheInfo->cacheMissRatio, pUpdateData->cacheMissCount, pUpdateData->isCacheMissInc);

#ifdef DEBUG_ON
	printf("[DEBUG] CMR: new count = %u, is increment = %s\n", pUpdateData->cacheMissCount, pUpdateData->isCacheMissInc == true ? "true" : "false");
#endif

	return;
}

/*
 * I can use an array of function pointers
 * in order to increase branch-miss
 * funcPointers[10];
 * for (i = 0; i < 10; i++) {
 * 		funcPointers[i];
 * 	}
 */

void updateBmr(struct tBranchInfo *pOrigBranchInfo, struct tBranchInfo *pCandBranchInfo, struct tStepData *pPrevOldStepData,
		struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateData *pUpdateData) {

	double bmpInc = 10;
	double bmpDec = 0.3;
	double origBmp;

	origBmp = pOrigBranchInfo->branchMissRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewVal(2, pPrevOldStepData, pOldStepData, pCandBranchInfo->branchMissRatio, origBmp, &pUpdateData->branchMissCount, &pUpdateData->isBranchMissInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->branchMissCount = 0;
			pUpdateData->isBranchMissInc = true;
		}
	} else {
		if (pCandBranchInfo->branchMissRatio >= origBmp) { // decrement
			pUpdateData->branchMissCount = calculateInitVal(pCandBranchInfo->branchMisses, pCandBranchInfo->branches, origBmp, bmpDec);
			pUpdateData->branchMissCount = pUpdateData->branchMissCount / 100;
			pUpdateData->isBranchMissInc = false;
		} else {
			pUpdateData->branchMissCount = calculateInitVal(pCandBranchInfo->branchMisses, pCandBranchInfo->branches, origBmp, bmpInc);
			pUpdateData->branchMissCount = pUpdateData->branchMissCount / 3;
			pUpdateData->isBranchMissInc = true;
		}
		if (isPerThread == true) {
			pUpdateData->branchMissCount /= 2;
		}
	}

	setStepData(pNewStepData, pCandBranchInfo->branchMissRatio, pUpdateData->branchMissCount, pUpdateData->isBranchMissInc);

#ifdef DEBUG_ON
	printf("[DEBUG] BMR: new count = %u, is increment = %s\n", pUpdateData->branchMissCount, pUpdateData->isBranchMissInc == true ? "true" : "false");
#endif

	return;
}

void updateCcr(struct tInstructions *pOrigInstructions, struct tInstructions *pCandInstructions, struct tStepData *pPrevOldStepData,
		struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateData *pUpdateData) {

	long long unsigned int commInstCount, compInstCount;
	double origCcr;

	origCcr = pOrigInstructions->ccRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewVal(3, pPrevOldStepData, pOldStepData, pCandInstructions->ccRatio, origCcr, &pUpdateData->commToCompCount, &pUpdateData->isCommInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->commToCompCount = 0;
			pUpdateData->isCommInc = true;
		}
	} else {
		commInstCount = pCandInstructions->loadCount + pCandInstructions->storeCount;
		compInstCount = pCandInstructions->instCount - commInstCount;

		if (pCandInstructions->ccRatio > origCcr) { // decrement
			/* add computation */
			pUpdateData->commToCompCount = ((double) commInstCount / origCcr) - compInstCount;
			pUpdateData->commToCompCount /= 20;
			pUpdateData->isCommInc = false;
		} else {
			/* add communication */
			pUpdateData->commToCompCount = (compInstCount * origCcr) - commInstCount;
			pUpdateData->commToCompCount = pUpdateData->commToCompCount / 15;
			pUpdateData->isCommInc = true;
		}
		if (isPerThread == true) {
			pUpdateData->commToCompCount /= 2;
		}
	}

	setStepData(pNewStepData, pCandInstructions->ccRatio, pUpdateData->commToCompCount, pUpdateData->isCommInc);

#ifdef DEBUG_ON
	printf("[DEBUG] CCR: new count = %u, is increment = %s\n", pUpdateData->commToCompCount, pUpdateData->isCommInc == true ? "true" : "false");
#endif

	return;
}

int findMinScore(int size, int indexId) {

	int minIndex = 0;
	int i;
	int min = similarityThreshold;

	for (i = 0; i < size; i++) {
		if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
			if (similarities[indexId][i] < min) {
				minIndex = i;
				min = similarities[indexId][i];
			}
		}
	}

	return minIndex;

}

//case 0: /* IPC */
//case 1: /* WFr */
//case 2: /* WIt */
//case 3: /* Reg */
//case 4: /* LcM */
//case 5: /* CMR */
//case 6: /* GMR */
//case 7: /* LMR */
//case 8: /* PMR */
//case 9: /* MCl */
//case 10: /* HtR */
int findOrderedMinScoreGpu(int indexId) {

	int minIndex;
	int min = indSimilarityThreshold;

	if (similarities[indexId][1] < min) {
		minIndex = 1;
	} else if (similarities[indexId][2] < min) {
		minIndex = 2;
	} else if (similarities[indexId][3] < min) {
		minIndex = 4;
	} else if (similarities[indexId][5] < min) {
		minIndex = 5;
	} else if (similarities[indexId][6] < min) {
		minIndex = 6;
	} else if (similarities[indexId][7] < min) {
		minIndex = 7;
	} else if (similarities[indexId][8] < min) {
		minIndex = 8;
	} else if (similarities[indexId][9] < min) {
		minIndex = 9;
	} else if (similarities[indexId][0] < min) {
		minIndex = 0;
	} else if (similarities[indexId][3] < min) {
		minIndex = 3;
	} else if (similarities[indexId][10] < min) {
		minIndex = 10;
	}

	return minIndex;

}

void restoreOldStepData(int indexId) {

	if (isPerThread == true) {
		updateData[indexId].ipcCount = oldStepData[indexId][0].count;
		updateData[indexId].isIpcInc = oldStepData[indexId][0].isInc;
		setStepData(&newStepData[indexId][0], cands[0]->threads[indexId]->ipcInfo.ipc,
				updateData[indexId].ipcCount, updateData[indexId].isIpcInc);

		updateData[indexId].cacheMissCount = oldStepData[indexId][1].count;
		updateData[indexId].isCacheMissInc = oldStepData[indexId][1].isInc;
		setStepData(&newStepData[indexId][1], cands[0]->threads[indexId]->cacheInfo.cacheMissRatio,
				updateData[indexId].cacheMissCount, updateData[indexId].isCacheMissInc);

		updateData[indexId].branchMissCount = oldStepData[indexId][2].count;
		updateData[indexId].isBranchMissInc = oldStepData[indexId][2].isInc;
		setStepData(&newStepData[indexId][2], cands[0]->threads[indexId]->branchInfo.branchMissRatio,
				updateData[indexId].branchMissCount, updateData[indexId].isBranchMissInc);

		/* CCR not used with stand-alone; since findMinScore cannot find it because of its level, no problem*/
		updateData[indexId].commToCompCount = oldStepData[indexId][3].count;
		updateData[indexId].isCommInc = oldStepData[indexId][3].isInc;
		setStepData(&newStepData[indexId][3], cands[0]->instructions.ccRatio,
				updateData[indexId].commToCompCount, updateData[indexId].isCommInc);
	} else {
		updateData[indexId].ipcCount = oldStepData[indexId][0].count;
		updateData[indexId].isIpcInc = oldStepData[indexId][0].isInc;
		setStepData(&newStepData[indexId][0], cands[indexId]->ipcInfo.ipc,
				updateData[indexId].ipcCount, updateData[indexId].isIpcInc);

		updateData[indexId].cacheMissCount = oldStepData[indexId][1].count;
		updateData[indexId].isCacheMissInc = oldStepData[indexId][1].isInc;
		setStepData(&newStepData[indexId][1], cands[indexId]->cacheInfo.cacheMissRatio,
				updateData[indexId].cacheMissCount, updateData[indexId].isCacheMissInc);

		updateData[indexId].branchMissCount = oldStepData[indexId][2].count;
		updateData[indexId].isBranchMissInc = oldStepData[indexId][2].isInc;
		setStepData(&newStepData[indexId][2], cands[indexId]->branchInfo.branchMissRatio,
				updateData[indexId].branchMissCount, updateData[indexId].isBranchMissInc);

		/* CCR not used with stand-alone; since findMinScore cannot find it because of its level, no problem */
		updateData[indexId].commToCompCount = oldStepData[indexId][3].count;
		updateData[indexId].isCommInc = oldStepData[indexId][3].isInc;
		setStepData(&newStepData[indexId][3], cands[indexId]->instructions.ccRatio,
				updateData[indexId].commToCompCount, updateData[indexId].isCommInc);
	}

	return;
}

void restoreOldStepDataGpu(int indexId) {

	updateDataGpu[indexId].ipcCount = oldStepData[indexId][0].count;
	updateDataGpu[indexId].isIpcInc = oldStepData[indexId][0].isInc;
	setStepData(&newStepData[indexId][0], cands[indexId]->gpuExt.instructionsPerCycle,
			updateDataGpu[indexId].ipcCount, updateDataGpu[indexId].isIpcInc);

	updateDataGpu[indexId].wgrCount = oldStepData[indexId][1].count;
	updateDataGpu[indexId].isWgrInc = oldStepData[indexId][1].isInc;
	setStepData(&newStepData[indexId][1], cands[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[1],
			updateDataGpu[indexId].wgrCount, updateDataGpu[indexId].isWgrInc);

	updateDataGpu[indexId].witCount = oldStepData[indexId][2].count;
	updateDataGpu[indexId].isWitInc = oldStepData[indexId][2].isInc;
	setStepData(&newStepData[indexId][2], cands[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0],
			updateDataGpu[indexId].witCount, updateDataGpu[indexId].isWitInc);

	updateDataGpu[indexId].regCount = oldStepData[indexId][3].count;
	updateDataGpu[indexId].isRegInc = oldStepData[indexId][3].isInc;
	setStepData(&newStepData[indexId][3], cands[indexId]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0],
			updateDataGpu[indexId].regCount, updateDataGpu[indexId].isRegInc);

	updateDataGpu[indexId].lcmCount = oldStepData[indexId][4].count;
	updateDataGpu[indexId]. isLcmInc= oldStepData[indexId][4].isInc;
	setStepData(&newStepData[indexId][4], cands[indexId]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0],
			updateDataGpu[indexId].lcmCount, updateDataGpu[indexId].isLcmInc);

	updateDataGpu[indexId].cmrCount = oldStepData[indexId][5].count;
	updateDataGpu[indexId].isCmrInc = oldStepData[indexId][5].isInc;
	setStepData(&newStepData[indexId][5], cands[indexId]->gpuExt.CMR,
			updateDataGpu[indexId].cmrCount, updateDataGpu[indexId].isCmrInc);

	updateDataGpu[indexId].gmrCount = oldStepData[indexId][6].count;
	updateDataGpu[indexId].isGmrInc = oldStepData[indexId][6].isInc;
	setStepData(&newStepData[indexId][6], cands[indexId]->gpuExt.globalMemRatio,
			updateDataGpu[indexId].gmrCount, updateDataGpu[indexId].isGmrInc);

	updateDataGpu[indexId].lmrCount = oldStepData[indexId][7].count;
	updateDataGpu[indexId].isLmrInc = oldStepData[indexId][7].isInc;
	setStepData(&newStepData[indexId][7], cands[indexId]->gpuExt.localMemRatio,
			updateDataGpu[indexId].lmrCount, updateDataGpu[indexId].isLmrInc);

	updateDataGpu[indexId].pmrCount = oldStepData[indexId][8].count;
	updateDataGpu[indexId].isPmrInc = oldStepData[indexId][8].isInc;
	setStepData(&newStepData[indexId][8], cands[indexId]->gpuExt.privateMemRatio,
			updateDataGpu[indexId].pmrCount, updateDataGpu[indexId].isPmrInc);

	updateDataGpu[indexId].mclCount = oldStepData[indexId][9].count;
	updateDataGpu[indexId].isMclInc = oldStepData[indexId][9].isInc;
	setStepData(&newStepData[indexId][9], cands[indexId]->gpuExt.coalescedAccessRatio,
			updateDataGpu[indexId].mclCount, updateDataGpu[indexId].isMclInc);

	updateDataGpu[indexId].htrCount = oldStepData[indexId][10].count;
	updateDataGpu[indexId].isHtrInc = oldStepData[indexId][10].isInc;
	setStepData(&newStepData[indexId][10], cands[indexId]->gpuExt.memory.hitRatio,
			updateDataGpu[indexId].htrCount, updateDataGpu[indexId].isHtrInc);

	return;
}

void updateIpcWrapper(int indexId) {

	if (isPerThread == true) {
		updateIpc(&procs[0]->threads[indexId]->ipcInfo, &cands[0]->threads[indexId]->ipcInfo, &prevOldStepData[indexId][0],
				&oldStepData[indexId][0], &newStepData[indexId][0], &updateData[indexId]);
	} else {
		updateIpc(&procs[indexId]->ipcInfo, &cands[indexId]->ipcInfo, &prevOldStepData[indexId][0],
				&oldStepData[indexId][0], &newStepData[indexId][0], &updateData[indexId]);
	}

	return;
}

void updateCmrWrapper(int indexId) {

	if (isPerThread == true) {
		updateCmr(&procs[0]->threads[indexId]->cacheInfo, &cands[0]->threads[indexId]->cacheInfo, &prevOldStepData[indexId][1],
				&oldStepData[indexId][1], &newStepData[indexId][1], &updateData[indexId]);
	} else {
		updateCmr(&procs[indexId]->cacheInfo, &cands[indexId]->cacheInfo, &prevOldStepData[indexId][1],
				&oldStepData[indexId][1], &newStepData[indexId][1], &updateData[indexId]);
	}

	return;
}

void updateBmrWrapper(int indexId) {

	if (isPerThread == true) {
		updateBmr(&procs[0]->threads[indexId]->branchInfo, &cands[0]->threads[indexId]->branchInfo, &prevOldStepData[indexId][2],
				&oldStepData[indexId][2], &newStepData[indexId][2], &updateData[indexId]);
	} else {
		updateBmr(&procs[indexId]->branchInfo, &cands[indexId]->branchInfo, &prevOldStepData[indexId][2],
				&oldStepData[indexId][2], &newStepData[indexId][2], &updateData[indexId]);
	}

	return;
}

void updateCcrWrapper(int indexId) {

	static boolean isUpdateCcr = false;
	/* to prevent adding code blocks for multiple threads.
	 * note that ccr is process-based not thread-based */

	if (isPerThread == true) {
		if (isUpdateCcr == false) {
			updateCcr(&procs[0]->instructions, &cands[0]->instructions, &prevOldStepData[indexId][2],
					&oldStepData[indexId][3], &newStepData[indexId][3], &updateData[indexId]);
			isUpdateCcr = true;
		}
	} else {
		updateCcr(&procs[0]->instructions, &cands[0]->instructions,
				&prevOldStepData[indexId][3], &oldStepData[indexId][3], &newStepData[indexId][3], &updateData[indexId]);
	}

	return;
}

void updateIpcGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double origIpc, candIpc;
	int indexId = 0;
	struct tIpcInfo origIpcInfo, candIpcInfo;
	struct tIpcInfo *pOrigIpcInfo = &origIpcInfo, *pCandIpcInfo = &candIpcInfo;

	pOrigIpcInfo->ipc = procs[indexId]->gpuExt.instructionsPerCycle;
	pOrigIpcInfo->instructions = procs[indexId]->gpuExt.instructions;
	pOrigIpcInfo->cycles = procs[indexId]->gpuExt.cycles;

	pCandIpcInfo->ipc = cands[indexId]->gpuExt.instructionsPerCycle;
	pCandIpcInfo->instructions = cands[indexId]->gpuExt.instructions;
	pCandIpcInfo->cycles = cands[indexId]->gpuExt.cycles;

	origIpc = pOrigIpcInfo->ipc;
	candIpc = pCandIpcInfo->ipc;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candIpc, origIpc, &pUpdateData->ipcCount, &pUpdateData->isIpcInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->ipcCount = 0;
			pUpdateData->isIpcInc = true;
		} else {
			if (pUpdateData->isIpcInc == false) {
				if (pOldStepData->count < 100) {
					pUpdateData->ipcCount = pOldStepData->count + 1;
				} else {
					pUpdateData->ipcCount = 100;
				}
			}
		}
	} else {
		if (candIpc >= origIpc) { // decrement
			if (candIpc - origIpc < 9) {
				pUpdateData->ipcCount = 2;
			} else {
				pUpdateData->ipcCount = 1;
			}
			pUpdateData->isIpcInc = false;
		} else {
			pUpdateData->ipcCount = 1;
			pUpdateData->isIpcInc = true;
		}
	}

	setStepData(pNewStepData, candIpc, pUpdateData->ipcCount, pUpdateData->isIpcInc);

#ifdef DEBUG_ON
	printf("[DEBUG] IPC: new count = %u, is increment = %s\n", pUpdateData->ipcCount, pUpdateData->isIpcInc == true ? "true" : "false");
#endif

	return;
}

void updateRegGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double candReg = cands[0]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0];
	double origReg = procs[0]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0];

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candReg, origReg, &pUpdateData->regCount, &pUpdateData->isRegInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->regCount = 0;
			pUpdateData->isRegInc = true;
		}
	} else {
		if (candReg >= origReg) { // decrement
			pUpdateData->regCount = 1;
			pUpdateData->isRegInc = false;
		} else {
			pUpdateData->regCount = 1;
			pUpdateData->isRegInc = true;
		}
	}

	setStepData(pNewStepData, candReg, pUpdateData->regCount, pUpdateData->isRegInc);

#ifdef DEBUG_ON
	printf("[DEBUG] Reg: new count = %u, is increment = %s\n", pUpdateData->regCount, pUpdateData->isRegInc == true ? "true" : "false");
#endif

	return;
}

void updateLcmGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double candLcm = cands[0]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0];
	double origLcm = procs[0]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0];

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candLcm, origLcm, &pUpdateData->lcmCount, &pUpdateData->isLcmInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->lcmCount = 0;
			pUpdateData->isLcmInc = true;
		}
	} else {
		if (candLcm >= origLcm) { // decrement
			pUpdateData->lcmCount = procs[0]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0] / 4;
			pUpdateData->isLcmInc = false;
		} else {
			pUpdateData->lcmCount = procs[0]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0] / 4;
			pUpdateData->isLcmInc = true;
		}
	}

	setStepData(pNewStepData, candLcm, pUpdateData->lcmCount, pUpdateData->isLcmInc);

#ifdef DEBUG_ON
	printf("[DEBUG] LcM: new count = %u, is increment = %s\n", pUpdateData->lcmCount, pUpdateData->isLcmInc == true ? "true" : "false");
#endif

	return;
}

void updateCmrGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double candCmr = cands[0]->gpuExt.CMR;
	double origCmr = procs[0]->gpuExt.CMR;

	double cmrValues[7] = { 0.800, 0.444, 0.308, 0.235, 0.190, 0.160, 0.138 }; /* count 1 to 7 */
	double minValue, diff;
	int i;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candCmr, origCmr, &pUpdateData->cmrCount, &pUpdateData->isCmrInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->cmrCount = 0;
			pUpdateData->isCmrInc = true;
		} else { /* new for gpu*/
			if (pUpdateData->isCmrInc == true) {
				pUpdateData->cmrCount = pOldStepData->count - 1;
			} else {
				if (pUpdateData->cmrCount > 100) {
					pUpdateData->cmrCount = 5;
				}
			}
		}
	} else {
		if (candCmr >= origCmr) { // decrement
			pUpdateData->cmrCount = 1;
			pUpdateData->isCmrInc = false;
		} else {
//			pUpdateData->cmrCount = 1;
			minValue = 1.0;
			for (i = 0; i < 7; i++) {
				diff = fabs(cmrValues[i] - origCmr);
				if (diff < minValue) {
					minValue = diff;
					pUpdateData->cmrCount = i + 1;
				}
			}
			pUpdateData->isCmrInc = true;
		}
	}

	setStepData(pNewStepData, candCmr, pUpdateData->cmrCount, pUpdateData->isCmrInc);

#ifdef DEBUG_ON
	printf("[DEBUG] CMR: new count = %u, is increment = %s\n", pUpdateData->cmrCount, pUpdateData->isCmrInc == true ? "true" : "false");
#endif

	return;
}

void updateGmrGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double candGmr = cands[0]->gpuExt.globalMemRatio;
	double origGmr = procs[0]->gpuExt.globalMemRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candGmr, origGmr, &pUpdateData->gmrCount, &pUpdateData->isGmrInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->gmrCount = 0;
			pUpdateData->isGmrInc = true;
		} else { /* new for gpu*/
			if (pUpdateData->isGmrInc == true) {
				if (pOldStepData->count < 8) {
					pUpdateData->gmrCount = pOldStepData->count + 1;
				} else {
					pUpdateData->gmrCount = 8;
				}
			} else {
				pUpdateData->gmrCount = pOldStepData->count + 1;
			}
		}
	} else {
		if (candGmr >= origGmr) { // decrement
			pUpdateData->gmrCount = 3;
			pUpdateData->isGmrInc = false;
		} else {
			if (origGmr < 0.003) {
				pUpdateData->gmrCount = 2;
			} else {
				pUpdateData->gmrCount = 1;
			}
			pUpdateData->isGmrInc = true;
		}
	}

	setStepData(pNewStepData, candGmr, pUpdateData->gmrCount, pUpdateData->isGmrInc);

#ifdef DEBUG_ON
	printf("[DEBUG] GMR: new count = %u, is increment = %s\n", pUpdateData->gmrCount, pUpdateData->isGmrInc == true ? "true" : "false");
#endif

	return;
}

void updateLmrGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double candLmr = cands[0]->gpuExt.localMemRatio;
	double origLmr = procs[0]->gpuExt.localMemRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candLmr, origLmr, &pUpdateData->lmrCount, &pUpdateData->isLmrInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->lmrCount = 0;
			pUpdateData->isLmrInc = true;
		} else {
			pUpdateData->lmrCount = pOldStepData->count + 3;
		}
	} else {
		if (candLmr >= origLmr) { // decrement
			pUpdateData->lmrCount = 1;
			pUpdateData->isLmrInc = false;
		} else {
			pUpdateData->lmrCount = 1;
			pUpdateData->isLmrInc = true;
		}
	}

	setStepData(pNewStepData, candLmr, pUpdateData->lmrCount, pUpdateData->isLmrInc);

#ifdef DEBUG_ON
	printf("[DEBUG] LMR: new count = %u, is increment = %s\n", pUpdateData->lmrCount, pUpdateData->isLmrInc == true ? "true" : "false");
#endif

	return;
}

void updatePmrGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double candPmr = cands[0]->gpuExt.privateMemRatio;
	double origPmr = procs[0]->gpuExt.privateMemRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candPmr, origPmr, &pUpdateData->ipcCount, &pUpdateData->isIpcInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->pmrCount = 0;
			pUpdateData->isPmrInc = true;
		}
	} else {
		if (candPmr >= origPmr) { // decrement
			pUpdateData->pmrCount = 1;
			pUpdateData->isPmrInc = false;
		} else {
			if (procs[0]->gpuExt.instructions <= 500) {
				pUpdateData->pmrCount = 1;
			} else {
				pUpdateData->pmrCount = 2;
			}
			pUpdateData->isPmrInc = true;
		}
	}

	setStepData(pNewStepData, candPmr, pUpdateData->pmrCount, pUpdateData->isPmrInc);

#ifdef DEBUG_ON
	printf("[DEBUG] PMR: new count = %u, is increment = %s\n", pUpdateData->pmrCount, pUpdateData->isPmrInc == true ? "true" : "false");
#endif

	return;
}

void updateMclGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double candMcl = cands[0]->gpuExt.coalescedAccessRatio;
	double origMcl = procs[0]->gpuExt.coalescedAccessRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candMcl, origMcl, &pUpdateData->mclCount, &pUpdateData->isMclInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->mclCount = 0;
			pUpdateData->isMclInc = true;
		}
	} else {
		if (candMcl >= origMcl) { // decrement
			pUpdateData->mclCount = 1;
			pUpdateData->isMclInc = false;
		} else {
			if (origMcl > 0.7) {
				pUpdateData->mclCount = 1;
			} else if (origMcl > 0.4) {
				pUpdateData->mclCount = 2;
			}	else if (origMcl > 0.1) {
				pUpdateData->mclCount = 3;
			} else {
				pUpdateData->mclCount = 4;
			}
			pUpdateData->isMclInc = true;
		}
	}

	setStepData(pNewStepData, candMcl, pUpdateData->mclCount, pUpdateData->isMclInc);

#ifdef DEBUG_ON
	printf("[DEBUG] MCl: new count = %u, is increment = %s\n", pUpdateData->mclCount, pUpdateData->isMclInc == true ? "true" : "false");
#endif

	return;
}

void updateHtrGpu(struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateDataGpu *pUpdateData) {

	double candHtr = cands[0]->gpuExt.memory.hitRatio;
	double origHtr = procs[0]->gpuExt.memory.hitRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewValGpu(0, pPrevOldStepData, pOldStepData, candHtr, origHtr, &pUpdateData->htrCount, &pUpdateData->isHtrInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->htrCount = 0;
			pUpdateData->isHtrInc = true;
		}
	} else {
		if (candHtr >= origHtr) { // decrement
			pUpdateData->htrCount = 1;
			pUpdateData->isHtrInc = false;
		} else {
			pUpdateData->htrCount = 1;
			pUpdateData->isHtrInc = true;
		}
	}

	setStepData(pNewStepData, candHtr, pUpdateData->htrCount, pUpdateData->isHtrInc);

#ifdef DEBUG_ON
	printf("[DEBUG] HtR: new count = %u, is increment = %s\n", pUpdateData->htrCount, pUpdateData->isHtrInc == true ? "true" : "false");
#endif

	return;
}

void updateCandBenchOne(int indexId) {

	int index;
	static boolean isUpdateParPattern = false;

	restoreOldStepData(indexId);

	index = findMinScore(metricCount, indexId);

#ifdef DEBUG_ON
	printf("[DEBUG] Minimum score index = %d, type = %s\n", index, metrics[index].name);
#endif

	switch (index) {
	case 0:
		if (isUpdateParPattern == false) {
			updateParPattern();
			isUpdateParPattern = true;
		}
		break;
	case 1: /* IPC */
		updateIpcWrapper(indexId);
		break;
	case 2: /* CMR */
		updateCmrWrapper(indexId);
		break;
	case 3: /* BMR */
		updateBmrWrapper(indexId);
		break;
	case 4: /* CCR */
		updateCcrWrapper(indexId);
		break;
	default:
		break;
	}

	return;
}

void updateCandBenchOneGpu(int indexId) {

	int index;

	restoreOldStepDataGpu(indexId);

	index = findMinScore(metricCount, indexId);
//	index = findOrderedMinScoreGpu(indexId);

#ifdef DEBUG_ON
	printf("[DEBUG] Minimum score index = %d, type = %s\n", index, metrics[index].name);
#endif

	switch (index) {
	case 0: /* IPC */
		updateIpcGpu(&prevOldStepData[indexId][0], &oldStepData[indexId][0], &newStepData[indexId][0], &updateDataGpu[indexId]);
		break;
	case 1: /* WFr */
		/* no update required */
		break;
	case 2: /* WIt */
		/* no update required */
		break;
	case 3: /* Reg */
		updateRegGpu(&prevOldStepData[indexId][3], &oldStepData[indexId][3], &newStepData[indexId][3], &updateDataGpu[indexId]);
		break;
	case 4: /* LcM */
		updateLcmGpu(&prevOldStepData[indexId][4], &oldStepData[indexId][4], &newStepData[indexId][4], &updateDataGpu[indexId]);
		break;
	case 5: /* CMR */
		updateCmrGpu(&prevOldStepData[indexId][5], &oldStepData[indexId][5], &newStepData[indexId][5], &updateDataGpu[indexId]);
		break;
	case 6: /* GMR */
		updateGmrGpu(&prevOldStepData[indexId][6], &oldStepData[indexId][6], &newStepData[indexId][6], &updateDataGpu[indexId]);
		break;
	case 7: /* LMR */
		updateLmrGpu(&prevOldStepData[indexId][7], &oldStepData[indexId][7], &newStepData[indexId][7], &updateDataGpu[indexId]);
		break;
	case 8: /* PMR */
		updatePmrGpu(&prevOldStepData[indexId][8], &oldStepData[indexId][8], &newStepData[indexId][8], &updateDataGpu[indexId]);
		break;
	case 9: /* MCl */
		updateMclGpu(&prevOldStepData[indexId][9], &oldStepData[indexId][9], &newStepData[indexId][9], &updateDataGpu[indexId]);
		break;
	case 10: /* HtR */
		updateHtrGpu(&prevOldStepData[indexId][10], &oldStepData[indexId][10], &newStepData[indexId][10], &updateDataGpu[indexId]);
		break;
	default:
		break;
	}

	return;
}

/* order --> ipc, ccr, cmr, bmp */
void updateCandBenchOrdered(int indexId) {

	int index = 0;
//	static boolean isUpdateParPattern = false;

	restoreOldStepData(indexId);

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < similarityThreshold) {
//			if (isUpdateParPattern == false) {
//				updateParPattern();
//				isUpdateParPattern = true;
//			}
//			return;
//		}
//	}
//	index++;
//
//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < similarityThreshold) {
//			updateComm(0);
//			return;
//		}
//	}
//	index++;
//
//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < SIMILARITY_THRESHOLD) {
//			updateDataSharing(0);
//			return;
//		}
//	}
//	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateIpcWrapper(indexId);
			return;
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateCmrWrapper(indexId);
			return;
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateBmrWrapper(indexId);
			return;
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateCcrWrapper(indexId);
			return;
		}
	}
	index++;

	return;
}

void updateCandBenchAll(int indexId) {

	int index = 0;
	static boolean isUpdateParPattern = false;

	restoreOldStepData(indexId);

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		if (similarities[indexId][index] < similarityThreshold) {
			if (isUpdateParPattern == false) {
				updateParPattern();
				isUpdateParPattern = true;
			}
		}
	}
	index++;

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < similarityThreshold) {
//			updateComm(0);
//		}
//	}
//	index++;

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < SIMILARITY_THRESHOLD) {
//			updateDataSharing(0);
//		}
//	}
//	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateIpcWrapper(indexId);
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateCmrWrapper(indexId);
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateBmrWrapper(indexId);
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateCcrWrapper(indexId);
		}
	}
	index++;

	return;
}


void updateCandBenchAllGpu(int indexId) {

	restoreOldStepDataGpu(indexId);

	if (similarities[indexId][0] < indSimilarityThreshold)
		updateIpcGpu(&prevOldStepData[indexId][0], &oldStepData[indexId][0], &newStepData[indexId][0], &updateDataGpu[indexId]);
	if (similarities[indexId][3] < indSimilarityThreshold)
		updateRegGpu(&prevOldStepData[indexId][3], &oldStepData[indexId][3], &newStepData[indexId][3], &updateDataGpu[indexId]);
	if (similarities[indexId][4] < indSimilarityThreshold)
		updateLcmGpu(&prevOldStepData[indexId][4], &oldStepData[indexId][4], &newStepData[indexId][4], &updateDataGpu[indexId]);
	if (similarities[indexId][5] < indSimilarityThreshold)
		updateCmrGpu(&prevOldStepData[indexId][5], &oldStepData[indexId][5], &newStepData[indexId][5], &updateDataGpu[indexId]);
	if (similarities[indexId][6] < indSimilarityThreshold)
		updateGmrGpu(&prevOldStepData[indexId][6], &oldStepData[indexId][6], &newStepData[indexId][6], &updateDataGpu[indexId]);
	if (similarities[indexId][7] < indSimilarityThreshold)
		updateLmrGpu(&prevOldStepData[indexId][7], &oldStepData[indexId][7], &newStepData[indexId][7], &updateDataGpu[indexId]);
	if (similarities[indexId][8] < indSimilarityThreshold)
		updatePmrGpu(&prevOldStepData[indexId][8], &oldStepData[indexId][8], &newStepData[indexId][8], &updateDataGpu[indexId]);
	if (similarities[indexId][9] < indSimilarityThreshold)
		updateMclGpu(&prevOldStepData[indexId][9], &oldStepData[indexId][9], &newStepData[indexId][9], &updateDataGpu[indexId]);
	if (similarities[indexId][10] < indSimilarityThreshold)
		updateHtrGpu(&prevOldStepData[indexId][10], &oldStepData[indexId][10], &newStepData[indexId][10], &updateDataGpu[indexId]);

	return;
}


void manageSideEffects() {

	//TODO: do operations according to following knowledge

/*
 * 	Table: Side effects (+: inc, -: dec)
 *			IPC-inc	IPC-dec	CMR-inc	CMR-dec	BMR-inc	BMR-dec	CCR-inc	CCR-dec
 *	IPC		+		-
 *	CMR						+		-								+
 *	BMR										+		-
 *	CCR				+										+		-
 *
 */

	return;
}

void manageSideEffectsGpu() {

	//TODO: do operations according to the side effect information

	return;
}

/*
 * Program; Parallel Pattern(s); Ovl Sim; PP Sim; IPC Sim; CMR Sim; BMP Sim; CCR Sim
 * Program; oIPC; oCMR; oBMP; oCCR; sIPC; sCMR; sBMP; sCCR
 */

void writeSimScores(int similarityScores[MAX_THREADS]) {

	int i, index = 0, indexId = 0;
	FILE *fa;
	time_t now;

	struct tm *today;
	char date[32];

	fa = fopen(resultFileName, "a");
	if (fa == NULL) {
		printf("ERROR! Cannot generate %s.\n", resultFileName);
		return;
	}

	fprintf(fa, "%s;", procs[0]->name);

	loadStepDataId();
	fprintf(fa, "%d;", stepDataId + 2); // #iterations

	for (i = 0; i < maxGroupId; i++) {
		if (i == maxGroupId - 1) {
			fprintf(fa, "%s;", getPatternText(origPps[i]));
		} else {
			fprintf(fa, "%s + ", getPatternText(origPps[i]));
		}
	}

	if (isPerThread == true) {
		for (i = 2; i <= maxTid; i++) {
			fprintf(fa, "Thread%d:", i);
			fprintf(fa, "%d;", similarityScores[indexId]); // Overall

			fprintf(fa, "%d;", similarities[indexId][index++]); // PP
//			fprintf(fa, "%d;", similarities[indexId][index++]);
//			fprintf(fa, "%d;", similarities[indexId][index++]);
			fprintf(fa, "%d;", similarities[indexId][index++]); // IPC
			fprintf(fa, "%d;", similarities[indexId][index++]); // CMR
			fprintf(fa, "%d;", similarities[indexId][index++]); // BMR
			fprintf(fa, "%d;", similarities[indexId][index++]); // CCR

			fprintf(fa, "%.3f;", procs[indexId]->threads[i]->ipcInfo.ipc);
			fprintf(fa, "%.3f;", procs[indexId]->threads[i]->cacheInfo.cacheMissRatio);
			fprintf(fa, "%.3f;", procs[indexId]->threads[i]->branchInfo.branchMissRatio);
			fprintf(fa, "%.3f;", procs[indexId]->instructions.ccRatio);

			fprintf(fa, "%.3f;", cands[indexId]->threads[i]->ipcInfo.ipc);
			fprintf(fa, "%.3f;", cands[indexId]->threads[i]->cacheInfo.cacheMissRatio);
			fprintf(fa, "%.3f;", cands[indexId]->threads[i]->branchInfo.branchMissRatio);
			fprintf(fa, "%.3f;", cands[indexId]->instructions.ccRatio);
		}
	} else {
		fprintf(fa, "%d;", similarityScores[indexId]); // Overall

		fprintf(fa, "%d;", similarities[indexId][index++]); //PP
//		fprintf(fa, "%d;", similarities[indexId][index++]);
//		fprintf(fa, "%d;", similarities[indexId][index++]);
		fprintf(fa, "%d;", similarities[indexId][index++]); // IPC
		fprintf(fa, "%d;", similarities[indexId][index++]); // CMR
		fprintf(fa, "%d;", similarities[indexId][index++]); // BMR
		fprintf(fa, "%d;", similarities[indexId][index++]); // CCR

		fprintf(fa, "%.3f;", procs[indexId]->ipcInfo.ipc);
		fprintf(fa, "%.3f;", procs[indexId]->cacheInfo.cacheMissRatio);
		fprintf(fa, "%.3f;", procs[indexId]->branchInfo.branchMissRatio);
		fprintf(fa, "%.3f;", procs[indexId]->instructions.ccRatio);

		fprintf(fa, "%.3f;", cands[indexId]->ipcInfo.ipc);
		fprintf(fa, "%.3f;", cands[indexId]->cacheInfo.cacheMissRatio);
		fprintf(fa, "%.3f;", cands[indexId]->branchInfo.branchMissRatio);
		fprintf(fa, "%.3f;", cands[indexId]->instructions.ccRatio);
	}

	time(&now);
	today = localtime(&now);
	strftime(date, 32, "%d.%m.%Y-%H:%M:%S", today);

	fprintf(fa, "%s", date);

	fprintf(fa, "\n");

	fclose(fa);

	return;
}

void writeSimScoresGpu(int similarityScores[MAX_THREADS]) {

	int index = 0, indexId = 0;
	FILE *fa;
	time_t now;

	struct tm *today;
	char date[32];

	fa = fopen(resultFileName, "a");
	if (fa == NULL) {
		printf("ERROR! Cannot generate %s.\n", resultFileName);
		return;
	}

	fprintf(fa, "%s;", procs[0]->name); // process name

	loadStepDataId();
	fprintf(fa, "%d;", stepDataId + 2); // #iterations

	fprintf(fa, "%d;", similarityScores[indexId]); // Overall

	fprintf(fa, "%d;", similarities[indexId][index++]); // IPC
	fprintf(fa, "%d;", similarities[indexId][index++]); // WFr
	fprintf(fa, "%d;", similarities[indexId][index++]); // WIt
	fprintf(fa, "%d;", similarities[indexId][index++]); // Reg
	fprintf(fa, "%d;", similarities[indexId][index++]); // LcM
	fprintf(fa, "%d;", similarities[indexId][index++]); // CMR
	fprintf(fa, "%d;", similarities[indexId][index++]); // GMR
	fprintf(fa, "%d;", similarities[indexId][index++]); // LMR
	fprintf(fa, "%d;", similarities[indexId][index++]); // PMR
	fprintf(fa, "%d;", similarities[indexId][index++]); // MCl
	fprintf(fa, "%d;", similarities[indexId][index++]); // HtR

	fprintf(fa, "%d;", procs[indexId]->gpuExt.instructions);
	fprintf(fa, "%.3f;", procs[indexId]->gpuExt.instructionsPerCycle);
	fprintf(fa, "%d;", procs[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[1]);
	fprintf(fa, "%d;", procs[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0]);
	fprintf(fa, "%d;", procs[indexId]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0]);
	fprintf(fa, "%d;", procs[indexId]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0]);
	fprintf(fa, "%.3f;", procs[indexId]->gpuExt.CMR);
	fprintf(fa, "%.3f;", procs[indexId]->gpuExt.globalMemRatio);
	fprintf(fa, "%.3f;", procs[indexId]->gpuExt.localMemRatio);
	fprintf(fa, "%.3f;", procs[indexId]->gpuExt.privateMemRatio);
	fprintf(fa, "%.3f;", procs[indexId]->gpuExt.coalescedAccessRatio);
	fprintf(fa, "%.3f;", procs[indexId]->gpuExt.memory.hitRatio);

	fprintf(fa, "%d;", cands[indexId]->gpuExt.instructions);
	fprintf(fa, "%.3f;", cands[indexId]->gpuExt.instructionsPerCycle);
	fprintf(fa, "%d;", cands[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[1]);
	fprintf(fa, "%d;", cands[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0]);
	fprintf(fa, "%d;", cands[indexId]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0]);
	fprintf(fa, "%d;", cands[indexId]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0]);
	fprintf(fa, "%.3f;", cands[indexId]->gpuExt.CMR);
	fprintf(fa, "%.3f;", cands[indexId]->gpuExt.globalMemRatio);
	fprintf(fa, "%.3f;", cands[indexId]->gpuExt.localMemRatio);
	fprintf(fa, "%.3f;", cands[indexId]->gpuExt.privateMemRatio);
	fprintf(fa, "%.3f;", cands[indexId]->gpuExt.coalescedAccessRatio);
	fprintf(fa, "%.3f;", cands[indexId]->gpuExt.memory.hitRatio);

	time(&now);
	today = localtime(&now);
	strftime(date, 32, "%d.%m.%Y-%H:%M:%S", today);

	fprintf(fa, "%s", date);

	fprintf(fa, "\n");

	fclose(fa);

	return;
}

void printSimScores() {

	int index = 0, indexId = 0;

	printf(" Similarity Scores [Metric: Score (Original - Candidate)]\n");

	if (isPerThread == true) {
		for (indexId = 2; indexId <= maxTid; indexId++) {
			index = 0;
			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
				printf("  Thread%d - Parallel Pattern(s): %d\n", indexId, similarities[indexId][index]);
			}
			index++;

//			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//				printf("  Thread%d - Thread Communication: %d\n", indexId, similarities[indexId][index]);
//			}
//			index++;
//
//			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//				printf("  Thread%d - Data Sharing: %d\n", indexId, similarities[indexId][index]);
//			}
//			index++;

			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
				printf("  Thread%d - IPC: %d (%.3f-%.3f)\n", indexId,
						similarities[indexId][index], procs[0]->threads[indexId]->ipcInfo.ipc, cands[0]->threads[indexId]->ipcInfo.ipc);
			}
			index++;

			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
				printf("  Thread%d - CMR: %d (%.1f%s-%.1f%s)\n", indexId, similarities[indexId][index],
						procs[0]->threads[indexId]->cacheInfo.cacheMissRatio * 100, "%", cands[0]->threads[indexId]->cacheInfo.cacheMissRatio * 100, "%");
			}
			index++;

			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
				printf("  Thread%d - BMR: %d (%.1f%s-%.1f%s)\n", indexId, similarities[indexId][index],
						procs[0]->threads[indexId]->branchInfo.branchMissRatio * 100, "%", cands[0]->threads[indexId]->branchInfo.branchMissRatio * 100, "%");
			}
			index++;

			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
				printf("  Thread%d - CCR: %d (%.3f-%.3f)\n", indexId,
						similarities[indexId][index], procs[0]->instructions.ccRatio, cands[0]->instructions.ccRatio);
			}
			index++;
		}
	} else {
		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
			printf("  Parallel Pattern(s): %d\n", similarities[indexId][index]);
		}
		index++;

//		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//			printf("  Thread Communication: %d\n", similarities[indexId][index]);
//		}
//		index++;
//
//		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//			printf("  Data Sharing: %d\n", similarities[indexId][index]);
//		}
//		index++;

		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
			printf("  IPC: %d (%.3f-%.3f)\n", similarities[indexId][index], procs[0]->ipcInfo.ipc, cands[0]->ipcInfo.ipc);
		}
		index++;

		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
			printf("  CMR: %d (%.1f%s-%.1f%s)\n", similarities[indexId][index],
					procs[0]->cacheInfo.cacheMissRatio * 100, "%", cands[0]->cacheInfo.cacheMissRatio * 100, "%");
		}
		index++;

		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
			printf("  BMR: %d (%.1f%s-%.1f%s)\n", similarities[indexId][index],
					procs[0]->branchInfo.branchMissRatio * 100, "%", cands[0]->branchInfo.branchMissRatio * 100, "%");
		}
		index++;

		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
			printf("  CCR: %d (%.3f-%.3f)\n", similarities[indexId][index], procs[0]->instructions.ccRatio, cands[0]->instructions.ccRatio);
		}
		index++;
	}

	return;
}

void printSimScoresGpu() {

	int i = 0, indexId = 0;

	printf(" Similarity Scores [Metric: Score (Original - Candidate)]\n");
	printf("  IPC: %d (%.3f-%.3f)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.instructionsPerCycle, cands[indexId]->gpuExt.instructionsPerCycle);
	printf("  WFr: %d (%d-%d)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[1],
			cands[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[1]);
	printf("  WIt: %d (%d-%d)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0],
			cands[indexId]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0]);
	printf("  Reg: %d (%d-%d)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0],
			cands[indexId]->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0]);
	printf("  LcM: %d (%d-%d)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0],
			cands[indexId]->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0]);
	printf("  CMR: %d (%.3f-%.3f)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.CMR, cands[indexId]->gpuExt.CMR);
	printf("  GMR: %d (%.3f-%.3f)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.globalMemRatio, cands[indexId]->gpuExt.globalMemRatio);
	printf("  LMR: %d (%.3f-%.3f)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.localMemRatio, cands[indexId]->gpuExt.localMemRatio);
	printf("  PMR: %d (%.3f-%.3f)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.privateMemRatio, cands[indexId]->gpuExt.privateMemRatio);
	printf("  MCl: %d (%.3f-%.3f)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.coalescedAccessRatio, cands[indexId]->gpuExt.coalescedAccessRatio);
	printf("  HtR: %d (%.3f-%.3f)\n", similarities[indexId][i++],
			procs[indexId]->gpuExt.memory.hitRatio, cands[indexId]->gpuExt.memory.hitRatio);

	return;
}

void setGenAlgGlobalParams() {

	int dataCount = 0;

	dataSharingRatioInc = 0;

	if (hasThreadMrapiExt) {
		dataSharingRatioInc += mrapiDataSharingRatioInc;
		dataCount++;
	}
	if (hasThreadMcapiExt) {
		dataSharingRatioInc += mcapiDataSharingRatioInc;
		dataCount++;
	}
	if (hasThreadPosixExt) {
		dataSharingRatioInc += posixDataSharingRatioInc;
		dataCount++;
	}

	dataSharingRatioInc = dataSharingRatioInc / dataCount;

	return;
}

void setOrigTargetPatterns(int phaseCount, paralPattern pps[MAX_GROUP_NUMBER], paralPattern targetPatterns[MAX_GROUP_NUMBER]) {

	int i;

	for (i = 0; i < phaseCount; i++) {
		if (targetPatterns[i] != PP_NA) {
			printf("INFO: Setting parallel pattern of phase %d from %s to %s.\n",
					i + 1, getPatternText(pps[i]), getPatternText(targetPatterns[i]));
			pps[i] = targetPatterns[i];
		}
	}
}

void setMetricsAndTypes() {

	strcpy(metrics[0].name, "PP");
	metrics[0].level =	METRIC_HIGH;
	strcpy(metrics[1].name, "IPC");
	metrics[1].level =	METRIC_LOW;
	strcpy(metrics[2].name, "CMR");
	metrics[2].level =	METRIC_LOW;
	strcpy(metrics[3].name, "BMR");
	metrics[3].level =	METRIC_LOW;
	strcpy(metrics[4].name, "CCR");
	metrics[4].level =	METRIC_HIGH;

	return;
}

int geneticAlg(int appCount, char *appNames[MAX_PROCS * 2], int candCount, char *candNames[MAX_PROCS * 2],
		boolean isCandExists, paralPattern targetPatterns[MAX_GROUP_NUMBER], boolean isVerbose) {

	int similarityScores[MAX_THREADS], totalThreadScore, i, indexId = 0;
	int retVal = 2;
	boolean isSimScoreMet = true;
	int ret;

	setMetricsAndTypes();

	assert(indSimilarityThreshold <= similarityThreshold);

	setGenAlgGlobalParams();

	if (candCount > 0) {
		if ((retVal = prepData(appCount, appNames)) == 0) {
			origPhaseCount = recogPattern(origPps, false, false);
			setOrigTargetPatterns(origPhaseCount, origPps, targetPatterns);
			setRunningType(false);
			ret = prepData(candCount, candNames);

			if (ret >= 0) {

#ifndef DEBUG_ON
				candPhaseCount = recogPattern(candPps, false, false);
#else
				candPhaseCount = recogPattern(candPps, true, false);
#endif
				setRunningType(true);

				if (isPerThread == true) {
					if (isCandExists) {
						loadParameters();
					}
					for (indexId = 2; indexId <= maxTid; indexId++) {
						if (isSimScoreMets[indexId] == true) {
							cands[0]->threads[indexId]->ipcInfo.ipc = oldStepData[indexId][0].val;
							cands[0]->threads[indexId]->cacheInfo.cacheMissRatio = oldStepData[indexId][1].val;
							cands[0]->threads[indexId]->branchInfo.branchMissRatio = oldStepData[indexId][2].val;
							cands[0]->instructions.ccRatio = oldStepData[indexId][3].val;
						}
						isSimScoreMets[indexId] = true;
						similarityScores[indexId] = measureOverallSim(appCount, appNames, candCount, candNames, indexId);
						if (similarityScores[indexId] >= similarityThreshold) {
							for (i = 0; i < metricCount; i++) {
								if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
									if (similarities[indexId][i] < indSimilarityThreshold) {
										isSimScoreMet = false;
										isSimScoreMets[indexId] = false;
										break;
									}
								}
							}
						} else {
							isSimScoreMet = false;
							isSimScoreMets[indexId] = false;
						}
					}
				} else {
					similarityScores[indexId] = measureOverallSim(appCount, appNames, candCount, candNames, indexId);
					if (similarityScores[indexId] >= similarityThreshold) {
						for (i = 0; i < metricCount; i++) {
							if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
								if (similarities[indexId][i] < indSimilarityThreshold) {
									isSimScoreMet = false;
									break;
								}
							}
						}
					} else {
						isSimScoreMet = false;
					}
				}

				if (isVerbose) {
					printSimScores();
				}

				if (isSimScoreMet) {
					printf("  Target similarity score is met.\n");
					writeSimScores(similarityScores);
					retVal = 3;
				} else {
					if (isGenSyn) {
						//FIXME: decide updateCandBench type according to parallel pattern or metric values
						if (isPerThread == true) {
							for (indexId = 2; indexId <= maxTid; indexId++) {
								if (isSimScoreMets[indexId] == false) {
//									updateCandBenchAll(indexId);
									updateCandBenchOne(indexId);
//									updateCandBenchOrdered(indexId);
								} else {
									restoreOldStepData(indexId);
								}
							}
						} else {
							if (isCandExists) {
								loadParameters();
							}
//							updateCandBenchAll(indexId);
							updateCandBenchOne(indexId);
//							updateCandBenchOrdered(indexId);
						}
						manageSideEffects();
						prepBenchmark(origPhaseCount, origPps, false);
						genBenchmark(origPhaseCount, origPps);
						saveParameters();
					}

#ifdef DEBUG_ON
					if (isPerThread == true) {
						for (i = 2; i <= maxTid; i++) {
							printf("[DEBUG] Thread%d, Block iteration counts: IPC=%u, CMR=%u, BMR=%u, CCR=%u\n",
									i, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count);
						}
					} else {
						i = 0;
						printf("[DEBUG] Process%d, Block iteration counts: IPC=%u, CMR=%u, BMR=%u, CCR=%u\n",
								i + 1, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count);
					}
#endif
					printf("  Not similar enough!\n");
				}

				if (isPerThread == true) {
					for (indexId = 2; indexId <= maxTid; indexId++) {
						printf("   Thread%d Score: %d/%d, Threshold: %d, Individual: %d (%s)\n",
								indexId, similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold,
								isSimScoreMets[indexId] == false ? "Not similar" : "Similar");
					}
					for (indexId = 2, totalThreadScore = 0; indexId <= maxTid; indexId++) {
						totalThreadScore += similarityScores[indexId];
					}
					printf("  Average Thread Score: %d/%d, Threshold: %d, Individual: %d\n",
							(int) (totalThreadScore / (maxTid - 1)), SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
				} else {
					printf(" Score: %d/%d, Threshold: %d, Individual: %d\n",
							similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
				}

			}
		}
	}

	return retVal;
}

int geneticAlgSa(int appCount, char *appNames[MAX_PROCS * 2], int candCount, char *candNames[MAX_PROCS * 2],
		boolean isCandExists, boolean isVerbose) {

	int similarityScores[MAX_THREADS], i, indexId = 0;
	int phaseCount = 1;
	boolean isSimScoreMet = true;
	paralPattern pps[MAX_GROUP_NUMBER] = { PP_NA };
	int retVal = 2;

	setMetricsAndTypes();

	assert(indSimilarityThreshold <= similarityThreshold);

	setGenAlgGlobalParams();

	if (candCount > 0) {
		if ((retVal = prepDataSa(appCount, appNames)) == 0) {
			setRunningType(false);
			prepDataSa(candCount, candNames);
			setRunningType(true);
			maxGroupId = 1; /* can be done in pattern recognition */
			maxCandGroupId = 1; /* can be done in pattern recognition */

			if (isPerThread == true) {
				for (indexId = 2; indexId <= maxTid; indexId++) {
					similarityScores[indexId] = measureOverallSimSa(appCount, appNames, candCount, candNames, indexId);
					if (similarityScores[indexId] >= similarityThreshold) {
						for (i = 0; i < metricCount; i++) {
							if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
								if (similarities[indexId][i] < indSimilarityThreshold) {
									isSimScoreMet = false;
									break;
								}
							}
						}
					} else {
						isSimScoreMet = false;
					}
				}
			} else {
				similarityScores[indexId] = measureOverallSimSa(appCount, appNames, candCount, candNames, indexId);
				if (similarityScores[indexId] >= similarityThreshold) {
					for (i = 0; i < metricCount; i++) {
						if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
							if (similarities[indexId][i] < indSimilarityThreshold) {
								isSimScoreMet = false;
								break;
							}
						}
					}
				} else {
					isSimScoreMet = false;
				}
			}

			if (isVerbose) {
				printSimScores();
			}

			if (isSimScoreMet) {
				printf(" Target similarity score is met.");
				writeSimScores(similarityScores);
				retVal = 3;
			} else {
				if (isGenSyn) {
					if (isCandExists) {
						loadParameters();
					}
					//FIXME: decide updateCandBench type according to parallel pattern or metric values
					if (isPerThread == true) {
						for (indexId = 2; indexId <= maxTid; indexId++) {
//							updateCandBenchAll(indexId);
							updateCandBenchOne(indexId);
//							updateCandBenchOrdered(indexId);
						}
					} else {
//						updateCandBenchAll(indexId);
						updateCandBenchOne(indexId);
//						updateCandBenchOrdered(indexId);
					}
					manageSideEffects();
					prepBenchmarkSa(true);
					phaseCount = 1;
					pps[0] = PP_Hl;
					genBenchmarkSa(phaseCount, pps);
					saveParameters();
				}

#ifdef DEBUG_ON
				if (isPerThread == true) {
					for (i = 2; i <= maxTid; i++) {
						printf("[DEBUG] Block iteration counts of Thread%d: IPC=%u, CMR=%u, BMR=%u, CCR=%u\n",
								i, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count);
					}
				} else {
					i = 0;
					printf("[DEBUG] Block iteration counts of Process%d: IPC=%u, CMR=%u, BMR=%u, CCR=%u\n",
							i + 1, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count);
				}
#endif

				printf(" Not similar enough!");
			}

			if (isPerThread == true) {
				for (indexId = 2; indexId <= maxTid; indexId++) {
					printf(" Thread %d Score: %d/%d, Threshold: %d, Individual: %d\n",
							indexId, similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
				}
			} else {
				printf(" Score: %d/%d, Threshold: %d, Individual: %d\n",
						similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
			}

		}
	}

	return retVal;
}

void setMetricsAndTypesGpu() {

	strcpy(metrics[0].name, "IPC");
	metrics[0].level =	METRIC_HIGH;
	strcpy(metrics[1].name, "WFr");
	metrics[1].level =	METRIC_HIGH;
	strcpy(metrics[2].name, "WIt");
	metrics[2].level =	METRIC_HIGH;
	strcpy(metrics[3].name, "Reg");
	metrics[3].level =	METRIC_HIGH;
	strcpy(metrics[4].name, "LcM");
	metrics[4].level =	METRIC_HIGH;
	strcpy(metrics[5].name, "CMR");
	metrics[5].level =	METRIC_HIGH;
	strcpy(metrics[6].name, "GMR");
	metrics[6].level =	METRIC_HIGH;
	strcpy(metrics[7].name, "LMR");
	metrics[7].level =	METRIC_HIGH;
	strcpy(metrics[8].name, "PMR");
	metrics[8].level =	METRIC_HIGH;
	strcpy(metrics[9].name, "MCl");
	metrics[9].level =	METRIC_HIGH;
	strcpy(metrics[10].name, "HtR");
	metrics[10].level =	METRIC_HIGH;

	return;
}

int geneticAlgGpu(int appCount, char *appNames[MAX_PROCS * 2], int candCount, char *candNames[MAX_PROCS * 2],
		boolean isCandExists, paralPattern targetPatterns[MAX_GROUP_NUMBER], boolean isVerbose) {

	int similarityScores[MAX_THREADS], i, indexId = 0;
	boolean isSimScoreMet = true;
	int retVal = 2;

	setMetricsAndTypesGpu();

	assert(indSimilarityThreshold <= similarityThreshold);

	if (candCount > 0) {
		if ((retVal = prepDataGpu(appCount, appNames, false)) == 0) {
			setRunningType(false);
			prepDataGpu(candCount, candNames, false);
			setRunningType(true);

			similarityScores[indexId] = measureOverallSimGpu(indexId, isVerbose);
			if (similarityScores[indexId] >= similarityThreshold) {
				for (i = 0; i < metricCount; i++) {
					if (similarities[indexId][i] < indSimilarityThreshold) {
						isSimScoreMet = false;
						break;
					}
				}
			} else {
				isSimScoreMet = false;
			}

			if (isVerbose) {
				printSimScoresGpu();
			}

			if (isSimScoreMet) {
				printf(" Target similarity score is met.");
				writeSimScoresGpu(similarityScores);
				retVal = 3;
			} else {
//				writeSimScoresGpu(similarityScores);
				if (isGenSyn) {
					if (isCandExists) {
						loadParametersGpu();
					}
					updateCandBenchOneGpu(indexId);
//					updateCandBenchAllGpu(indexId);
					manageSideEffectsGpu();
					prepBenchmarkGpu(origPhaseCount, origPps);
					genBenchmarkGpu(origPhaseCount, origPps);
					saveParametersGpu();
				}

#ifdef DEBUG_ON
				i = 0;
				printf("[DEBUG] Block iteration counts of Process%d: IPC=%u, WFr=%u, WIt=%u, Reg=%u, LcM=%u, "
						"CMR=%u, GMR=%u, LMR=%u, PMR=%u, MCl=%u, HtR=%u\n",
						i + 1, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count,
						newStepData[i][4].count, newStepData[i][5].count, newStepData[i][6].count, newStepData[i][7].count,
						newStepData[i][8].count, newStepData[i][9].count, newStepData[i][10].count);
#endif

				printf(" Not similar enough!");
			}

			printf(" Score: %d/%d, Threshold: %d, Individual: %d\n",
					similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
			printf(" Speedup: %.2f\n", (double) (procs[0]->gpuExt.instructions) / (double) (cands[0]->gpuExt.instructions));

		}
	}

	return retVal;
}
