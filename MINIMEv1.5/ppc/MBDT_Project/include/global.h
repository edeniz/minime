/*
 * Copyright (c) 2011-2015,
 * Etem Deniz <etem.deniz@boun.edu.tr>, Bogazici University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * globah.h
 *
 *  Created on: Dec 5, 2011
 *      Author: Etem Deniz
 */

#ifndef GLOBAH_H_
#define GLOBAH_H_

#include <sys/ipc.h>

/* macros */
#define DEBUG_ON
//#define PRINT_CTRL_FLOW_IN_SYN

/* global */
#define MAX_PROCESSORS 16
#define MAX_PROCS 4
#define MAX_THREADS 128
#define MAX_PATH 128
#define MAX_INDENT 8
#define MAX_COMPUTE_UNITS 32
#define MAX_NDRANGE_COUNT 256
#define MAX_UNIQUE_NDRANGES 4

/* data preparation */
#define STR_NAME_MAX 64
#define STR_LINE_MAX 1024
#define STR_INT_MAX 16
#define STR_MAX 32
#define STR_LONG_MAX 32
#define STR_DOUBLE_MAX 16
#define MAX_PAR_DEP 16
#define MIN_DATA_VALUE 0.001
#define MIN_DATA_VALUE_GPU 0.01

/* pattern recognition */
#define MAX_GROUP_NUMBER 4
#define MAX_PAR_PAT 6
#define MAX_DATA_SHR 4
#define MAX_COMM 3
#define MAX_GEN_THREADING 6
#define MAX_SYNC_CON 3

/* benchmark preparation */
#define MAX_PACKET_SIZE 8
#define PROD_CONS_MIN 5000

/* benchmark generation */
#define MAX_GLOB_MEM 32
#define MAX_LOCAL_MEM 8
#define MAX_SHARED_MEM 16
#define MAX_SEM 64
#define MAX_MUTEX 32
#define MAX_BARRIER 8
#define MAX_ENDPOINT 64
#define MAX_CHANNEL 32
#define MAX_SYNC_OPS 16
#define MAX_MEM_OPS 32
#define MAX_MSG_OPS 64
#define MAX_WORK_PER_PHASE 3
#define MAX_WORK_OPS 8
#define MAX_WORK_LEN 1024
#define SIMILARITY_MAX 100

/* genetic algorithm */
#define MAX_LOOP_COUNT 500000000
#define MAX_METRIC 16
#define ITER_METRIC 11  /* if cpu use as 4; if gpu use as 11 */

typedef unsigned int boolean;

#define false 0
#define true (!false)

enum eParalPattern {
	PP_TP = 0,
	PP_DaC,
	PP_GD,
	PP_RD,
	PP_Pl,
	PP_EbC,
	PP_Hl,
	PP_ST,
	PP_NA,
	DW_DLA,
	DW_SLA,
	DW_SM,
	DW_NBM,
	DW_SG,
	DW_UG,
	DW_MR,
	DW_CL,
	DW_GT,
	DW_DP,
	DW_BBB,
	DW_GM,
	DW_FSM,
	DW_NA
};
typedef enum eParalPattern paralPattern;

/* data preparation */

enum eCharacterizerType {
	char_perf,
	char_papiex
};
typedef enum eCharacterizerType characterizerType;

enum eMetricType {
	metric_invalid,
	instructions,
	cycles,
	cache_misses,
	cache_references,
	branch_misses,
	branches
};
typedef enum eMetricType metricType;

/* pattern recognition */

enum eDataSharingType {
	private = 0,
	prodCons,
	migratory,
	readonly
};
enum eDataSharingType dataSharingType;

struct tTimeData {
	int hour;
	int minute;
	int second;
	int nanosec;
};

struct tParDependency {
	int dependency[MAX_THREADS];
	int globalIndex;
	struct tTimeData time;
};

struct tMsgData {
	int msgCount;
	int totalSize;
};

struct tDataSharing {
	int private;
	int prodCons;
	int migratory;
	int readonly;
	int total;
	int totalMem;
};

struct tSyncData {
	int locks;
	int barriers;
	int conditions;
};

struct tParPatternScores {
	double dsSubScores[MAX_GROUP_NUMBER][MAX_DATA_SHR];
	double tcSubScores[MAX_GROUP_NUMBER][MAX_COMM];
	double gtSubScores[MAX_GROUP_NUMBER][MAX_GEN_THREADING];
	double scSubScores[MAX_GROUP_NUMBER][MAX_SYNC_CON];
	int dsScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
	int commScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
	int genThreadingScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
	int syncConScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
	int totalScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
};

/* benchmark preparation and generation */

enum stRoutine {
	SR_NOT_INITIALIZED = 1000,
	SR_ASSIGNED,
	SR_CREATED
};

struct tThreadStartRoutine {
	int taskId;
	int input;
	int threadCount;
};

enum eEndpointType {
	EP_MESSAGE,
	EP_PACKET,
	EP_SCALAR
};
typedef enum eEndpointType endpointType;

struct tMcapiEndpoint {
	int nodeId;
	int portId;
	int printNodeId;
	int printPortId;
	endpointType type;
	boolean isSender;
	boolean isOwner;
	int phase;
};

struct tMcapiChannel {
	struct tMcapiEndpoint *pSenderEp;
	struct tMcapiEndpoint *pSenderGetEp;
	struct tMcapiEndpoint *pReceiverEp;
	struct tMcapiEndpoint *pReceiverGetEp;
	struct tThreadData *pSenderTd;
	struct tThreadData *pReceiverTd;
	int size; /* in bits */
	endpointType type;
};

struct tLocalMem {
	int size;
	int size2;
	char name[STR_NAME_MAX];
};

struct tSem {
	key_t key;
	boolean isOwner;
	boolean isLockAfterCreate;
	boolean isLocked;
};

struct tMutex {
	int id;
	key_t key;
	boolean isOwner;
	boolean isLocked;
};

struct tBarrier {
	int id;
	boolean isOwner;
	int numOfThreads;
	struct tMutex *pMutex;
};

struct tSharedMem {
	int size;
	key_t key;
	boolean isOwner;
	struct tMutex *pMutex;
};

enum eWorkOpType {
	WORK_OP_NOOP,
	WORK_OP_IPC_INC,
	WORK_OP_IPC_DEC,
	WORK_OP_CACHE_HIT,
	WORK_OP_CACHE_MISS,
	WORK_OP_BRNCH_MISS,
	WORK_OP_BRNCH_HIT,
	WORK_OP_COMP_INC,
	WORK_OP_COMM_INC
};
typedef enum eWorkOpType workOpType;

struct tWorkOp {
	workOpType type;
	unsigned int numOfIterations;
	int type1Percent;
	int time;
};

enum eMemOpType {
	MEM_OP_READ,
	MEM_OP_WRITE,
};
typedef enum eMemOpType memOpType;

enum eMemType {
	MEM_TYPE_LOCAL,
	MEM_TYPE_SHARED,
	MEM_TYPE_GLOBAL
};
typedef enum eMemType memType;

enum eSyncType {
	SYNC_TYPE_SEM,
	SYNC_TYPE_MTX,
	SYNC_TYPE_BAR
};
typedef enum eSyncType syncType;

struct tMemOp {
	int resId;
	int startIndex;
	int numOfIterations;
	int time;
	memOpType opType;
	memType type;
	int semIndex;
	int workSize;
};

struct tSyncOp {
	int resId;
	int time;
	syncType type;
};

enum eMsgOpType {
	MSG_OP_SEND,
	MSG_OP_RECV,
};
typedef enum eMsgOpType msgOpType;

enum eMsgType {
	MSG_TYPE_MSG,
	MSG_TYPE_PKT,
	MSG_TYPE_SCL
};
typedef enum eMsgType msgType;

struct tMsgOp {
	int resId;
	int resId2;
	int numOfIterations;
	int time;
	msgOpType opType;
	msgType type;
};

struct tInstructions {
	long long unsigned int instCount;
	long long unsigned int loadCount;
	long long unsigned int storeCount;
	int commRatio;
	int compRatio;
	double ccRatio;
};

struct tIpcInfo {
	double ipc;
	long long unsigned int cycles;
	long long unsigned int instructions;
};

struct tCacheInfo {
	double cacheMissRatio;
	long long unsigned int cacheMisses;
	long long unsigned int cacheReferences;
};

struct tBranchInfo {
	double branchMissRatio;
	long long unsigned int branchMisses;
	long long unsigned int branches;
};

struct tUpdateData {
	unsigned int ipcCount;
	boolean isIpcInc;
	unsigned int cacheMissCount;
	boolean isCacheMissInc;
	unsigned int branchMissCount;
	boolean isBranchMissInc;
	unsigned int commToCompCount;
	boolean isCommInc;
};

struct tUpdateDataGpu {
	unsigned int ipcCount;
	boolean isIpcInc;
	unsigned int wgrCount;
	boolean isWgrInc;
	unsigned int witCount;
	boolean isWitInc;
	unsigned int regCount;
	boolean isRegInc;
	unsigned int lcmCount;
	boolean isLcmInc;
	unsigned int cmrCount;
	boolean isCmrInc;
	unsigned int gmrCount;
	boolean isGmrInc;
	unsigned int lmrCount;
	boolean isLmrInc;
	unsigned int pmrCount;
	boolean isPmrInc;
	unsigned int mclCount;
	boolean isMclInc;
	unsigned int htrCount;
	boolean isHtrInc;
};

struct tMtapiThreadExt {
	int lifeTime;
	int loopCount;
	int numOfChildren[MAX_GROUP_NUMBER];
	struct tThreadStartRoutine startRoutine;
	int totalWork;
	int numOfWorks;
	struct tWorkOp workOps[MAX_WORK_OPS];
};

struct tLsThreadExt {
	boolean hasExt;
	int numOfLocalMems;
	struct tLocalMem localMems[MAX_LOCAL_MEM];
	int numOfMemOps;
	struct tMemOp memOps[MAX_MEM_OPS];
};

struct tMrapiThreadExt {
	boolean hasExt;
	int numOfSharedMems;
	struct tSharedMem sharedMems[MAX_SHARED_MEM];
	int numOfSemaphores;
	struct tSem sems[MAX_SEM];
	int numOfMutexes;
	struct tMutex mutexes[MAX_MUTEX + MAX_SHARED_MEM + MAX_BARRIER];
	int numOfBarriers;
	struct tBarrier barriers[MAX_BARRIER];
	int numOfMemOps;
	struct tMemOp memOps[MAX_MEM_OPS];
	int numOfSyncOps;
	struct tSyncOp syncOps[MAX_SYNC_OPS];
};

struct tMcapiThreadExt {
	boolean hasExt;
	int currPortId;
	int numOfEndpoints;
	struct tMcapiEndpoint endpoints[MAX_ENDPOINT + (4 * MAX_CHANNEL)];
	int numOfPcktChans;
	struct tMcapiChannel *pcktChans[MAX_CHANNEL];
	int numOfScalChans;
	struct tMcapiChannel *scalChans[MAX_CHANNEL];
	int numOfMsgOps;
	struct tMsgOp msgOps[MAX_MSG_OPS];
};

struct tPosixThreadExt {
	//boolean hasExt;
	int numOfSemaphores;
	struct tSem sems[MAX_SEM];
	int numOfMutexes;
	struct tMutex mutexes[MAX_MUTEX + MAX_SHARED_MEM + MAX_BARRIER];
};

struct tLsProcExt {
	//boolean hasExt;
	int numOfGlobMems;
	struct tLocalMem globMems[MAX_GLOB_MEM];
};

struct tMtapiProcExt {
	short isStartRoutine[MAX_THREADS];
	boolean hasUniqueThreads[MAX_THREADS];
	int totalLifeTimes[MAX_GROUP_NUMBER];
};

struct tMrapiProcExt {
	int barrierCount;
};

struct tMcapiProcExt {
	int scalChanCount;
	struct tMcapiChannel scalChans[MAX_CHANNEL * MAX_THREADS];
	int pcktChanCount;
	struct tMcapiChannel pcktChans[MAX_CHANNEL * MAX_THREADS];
};

struct tStandAloneExt {
	paralPattern parPattern;
	int phase;
	int stage;
	int loopCount;
	int localDataSize;
	int sharedDataSize;
	int workSize;
	int phasePatterns[MAX_GROUP_NUMBER];
};

/* genetic algorithm */

struct tStepData {
	int id;
	double val;
	double initVal;
	unsigned int count;
	boolean isInc;
	boolean isValid;
};

enum eMetricLevel {
	METRIC_LOW,
	METRIC_HIGH,
	METRIC_ALL,
	METRIC_UNKNOWN
};
typedef enum eMetricLevel metricLevel;

struct tMetric {
	char name[STR_NAME_MAX];
	metricLevel level;
};

/* global */

struct tOccupacy {
	int workItemsPerWorkGroup[2];
	int registersPerWorkItem[2];
	int localMemoryPerWorkGroup[2];
};

struct tMemory {
	int globalMemCount;
	int localMemCount;
	int privateMemCount;
	int totalCount;
	double hitRatio;
	int unitCount;
};

struct tLDS {
	int accesses;
	int reads;
	int effectiveReads;
	int coalescedReads;
	int writes;
	int effectiveWrites;
	int coalescedWrites;
};

struct tComputeUnitData {
	int nDRangeCount;
	int workGroupCount;
	int instructions;
	int scalarALUInstructions;
	int scalarMemInstructions;
	int branchInstructions;
	int vectorALUInstructions;
	int LDSInstructions;
	int vectorMemInstructions;
	int cycles;
	int instructionsPerCycle;
	int scalarRegReads;
	int scalarRegWrites;
	int vectorRegReads;
	int vectorRegWrites;
	struct tLDS LDS;
};

struct tNDRangeData {
	int workDim;
	int globalSizes[3];
	int localSizes[3];
	struct tOccupacy occupacy;
};

struct tDeviceData {
	int kernelCount;
	int NDRangeCount;
	int workGroupCount;
	int instructions;
	int scalarALUInstructions;
	int scalarMemInstructions;
	int branchInstructions;
	int vectorALUInstructions;
	int LDSInstructions;
	int vectorMemInstructions;
	int cycles;
	struct tNDRangeData NDRangeData[MAX_NDRANGE_COUNT];
	int maxWorkDim;
	struct tNDRangeData uniqueNDRanges[MAX_UNIQUE_NDRANGES];
	double instructionsPerCycle;
	struct tComputeUnitData computeUnitData[MAX_COMPUTE_UNITS];
	struct tMemory memory;
	double CMR; // computation-to-memory access ratio
	double globalMemRatio;
	double localMemRatio;
	double privateMemRatio;
	double coalescedAccessRatio;
};

struct tThreadData {
	int id;
	int tid;
	int pc;
	int creator;
	struct tTimeData creationTime;
	struct tTimeData exitTime;
	int instCount;
	int dependency[MAX_THREADS];
	int correlation[MAX_THREADS];
	int currParDep;
	int numOfParDeps;
	struct tParDependency parDeps[MAX_PAR_DEP];
	struct tMsgData msgData[MAX_PROCS];
	struct tMsgData imsgData[MAX_PROCS];
	int cacheMiss;
	int cacheInvalid;
	struct tMsgNode *msgList;
	int groupId;
	struct tIpcInfo ipcInfo;
	struct tCacheInfo cacheInfo;
	struct tBranchInfo branchInfo;
	struct tLsThreadExt lsExt;
	struct tMtapiThreadExt mtapiExt;
	struct tMrapiThreadExt mrapiExt;
	struct tMcapiThreadExt mcapiExt;
	struct tPosixThreadExt posixExt;
	struct tStandAloneExt standAloneExt;
};

struct tProcData {
	int id;
	char *name;
	int pid;
	int numOfThreads;
	struct tThreadData *threads[MAX_THREADS];
	struct tInstructions instructions;
	struct tDataSharing dataSharing[MAX_GROUP_NUMBER];
	int cacheMiss[MAX_GROUP_NUMBER];
	int cacheInvalid[MAX_GROUP_NUMBER];
	int numOfParDeps;
	struct tParDependency *parDeps[MAX_THREADS * MAX_PAR_DEP];
	struct tSyncData syncData[MAX_GROUP_NUMBER];
	struct tIpcInfo ipcInfo;
	struct tCacheInfo cacheInfo;
	struct tBranchInfo branchInfo;
	struct tParPatternScores parPatternScores;
	struct tLsProcExt lsExt;
	struct tMtapiProcExt mtapiExt;
	struct tMrapiProcExt mrapiExt;
	struct tMcapiProcExt mcapiExt;
	struct tDeviceData gpuExt;
};

/* global */
extern boolean isGenSyn;
characterizerType charType;
extern boolean isPerThread;
extern struct tProcData *procs[MAX_PROCS];
extern int procCount;
extern int maxGroupId;
extern struct tProcData *cands[MAX_PROCS];
extern int candCount;
extern int maxCandGroupId;
extern boolean isOriginal;
extern struct tProcData *buProcs[MAX_PROCS];
extern int buCount;
extern int buMaxGroupId;
extern struct tProcData *activeProc;

/* data preparation */
extern int maxTid;
extern boolean isRangeAndRoundData;

/* pattern recognition */
extern double cahceMissRatio;
extern double cahceMissRatioInterval;
extern double dsLowerBoundRatio;
extern double tcLowerBoundRatio;
extern double stdDevMult;
extern double cvUpperBound; /* if stdDev is higher than 20% of mean, then it is large. */

/* benchmark preparation */
extern boolean isStandalone; /* benchmark is synthesized from user given log file */
extern boolean isMultiThreadSingleTask; /* threads have unique functions or share same functions */
extern boolean hasThreadMrapiExt; /* threads have MRAPI extension */
extern boolean hasThreadMcapiExt; /* threads have MCAPI extension */
extern boolean hasThreadPosixExt; /* threads have POSIX extension */
extern boolean hasThreadOpenClExt; /* threads have OpenCL extension */
extern int codeLevel; /* tuning code blocks in low or high level */
extern metricLevel simMetricLevel; /* metric level that will be similar */
extern double dataMigTotalRatio; /* ratio of dependency value to migratory  */
extern double dataSharingRatio; /* the ratio of the data sharing */
extern double dataPrivateRatio; /* the ratio of the private data */
extern double dataReadonlyRatio; /* the ratio of the read-only data */
extern double migDataSharingRatio; /* the ratio of the migratory data sharing */
extern double ignoreRatio; /* the ratio of partial dependency to total dependency */
extern struct tUpdateData updateData[MAX_THREADS]; /* update data for the processes (cpu) */
extern struct tUpdateDataGpu updateDataGpu[MAX_THREADS]; /* update data for the processes (gpu) */

/* benchmark generation */
extern boolean isThreadStructData; /* thread data structure include tls, mrapi, mcapi data or not */
extern boolean isVariablesInSingleLine; /* variables having same type are generated in single line or not */
extern boolean isTlsVarArray; /* local variables are defined as arrays or not*/

/* genetic algorithm */
extern int similarityThreshold; /* similarity threshold */
extern int indSimilarityThreshold; /* individual similarity threshold */
extern double dataSharingDisThreshold; /* data sharing similarity threshold */

char *getPatternText(paralPattern pp);

void setRunningType(boolean isOrig);

char *getBaseName(char *path);

#endif /* GLOBAH_H_ */
