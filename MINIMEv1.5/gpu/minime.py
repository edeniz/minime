#!/usr/bin/python
# 
# Copyright (c) 2011-2015, 
#  Etem Deniz    <etem.deniz@boun.edu.tr>, Bogazici University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
# (1) Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 
# (2) Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# 
# (3) The names of the contributors may not be used to endorse or promote
# products derived from this software without specific prior written
# permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
import os, errno, sys, stat
import shutil
import subprocess
import time
import signal

CWD = os.getcwd()
PERF_CHAR_DIR = CWD + "/papiex/x86_64-Linux/"	# path that includes "papiex"
DYNAMORIO_DIR = CWD + '/dynamorio/build/bin64/'	# path that includes "drrun"
UMBRA_DIR = CWD + '/umbra/'						# path that includes "/build/bin/libaio.so"
BENCH_SYN_DIR = CWD + '/MBDT_Project/'			# benchmark synthesis project path
MCA_DIR = CWD + '/MCA_Project/'					# mca apis project path
M2S_EXE_DIR = '/usr/local/bin/'					# path including multi2sim executables
M2S_DIR = CWD + "/multi2sim-4.2/"				# path including multi2sim source
OCL_SDK_DIR = "/opt/AMDAPP/"					# AMD APP path
CHARACTERIZER_TYPE = 'papiex'					# perf, papiex
SYN_TYPE = 'gpu'								# syn (synthetic), sal (stand-alone), swm (stand-alone w perf. metrics), gpu (GPU opencl)
LIB_TYPE = 'opencl' 							# posix, mrapi, mcapi, opencl
SIM_METRIC_TYPE = 'all' 						# low, high, all (high or all for gpu)
SIM_THRESHOLD = '90' 							# [0-100]
IND_SIM_THRESHOLD = '60' 						# [0-100]
ITERATION_THRESHOLD = 20 						# [1++]
SYN_CODE_LEVEL = 'high' 						# high, low
CHAR_REPEAT = '1' 								# [1++]
GEN_SYN = 'true'								# true, false
PER_THREAD = 'true'								# true, false
IS_CHAR_ORG = 'true'							# true, false

#**********************************************************************************************************************************#
hz = os.sysconf(os.sysconf_names['SC_CLK_TCK'])
pid = os.getpid()

def getChildTime():
	with open("/proc/%d/stat" % (pid)) as fp:
		fields = fp.read().split(' ')[15:17]
		cutime, cstime = [ (float(f) / hz) for f in fields ]
		ctime = cutime + cstime
		return ctime
		# cutime + cstime will give you the total CPU used by child

def mkdirP(path):
    try:
        os.mkdir(path)
    except os.error, e:
		return

def cd(path):
	os.chdir(path)

def editPaths(argc, argv):
	CURR_DIR = os.getcwd()
	index = 0	
	retVal = []
	while (index < argc):
		if os.path.exists(argv[index]):
			if not os.path.isabs(argv[index]):
				retVal.append(CURR_DIR + '/' + argv[index])
			else:
				retVal.append(argv[index])
			index = index + 1
		else:
			print 'ERROR: Cannot find:', argv[index]
			sys.exit(4)
	return retVal

def findInSubdirectory(prefix, suffix, subdirectory=''):
	if subdirectory:
		path = subdirectory
	else:
		path = os.getcwd()
	for root, dirs, files in os.walk(path):
		for file in files:
			if file.startswith(prefix) and file.endswith(suffix):
				return os.path.join(root, file)
	return ''

def findDirInSubdirectory(prefix, subdirectory=''):
	if subdirectory:
		path = subdirectory
	else:
		path = os.getcwd()
	for root, dirs, files in os.walk(path):
		for dir in dirs:
			if dir.startswith(prefix):
				return os.path.join(root, dir)
	return ''

def cleanLogDirs(prefix, subdirectory=''):
	if subdirectory:
		path = subdirectory
	else:
		path = os.getcwd()
	for root, dirs, files in os.walk(path):
		for dir in dirs:
			if dir.startswith(prefix):
				shutil.rmtree(path + '/' + dir)
	return

def mergeAllTextFiles(dirLoc, outName):
	tempCWD = os.getcwd()
	if dirLoc == '':
		dirLoc = tempCWD
	else:
		outName = '../' + outName
	cd(dirLoc)
	files=os.listdir(dirLoc)
	for f in files:
		if f.endswith(".txt"):
			data=open(f, "r")
			out=open(outName, 'a')
			for line in data:
				out.write(line)
			data.close()
			out.close()
	cd(tempCWD)

def mergeTextFiles(inFile,  outName):
	data=open(inFile, "r")
	out=open(outName, 'a')
	for line in data:
		out.write(line)
	data.close()
	out.close()

def mergeAllTextFilesWithPrefix(dirLoc, prefix, outName):
	tempCWD = os.getcwd()
	if dirLoc == '':
		dirLoc = tempCWD
	else:
		outName = '../' + outName
	cd(dirLoc)
	files=os.listdir(dirLoc)
	for f in files:
		if f.startswith(prefix) and f.endswith(".txt"):
			data=open(f, "r")
			out=open(outName, 'a')
			for line in data:
				out.write(line)
			data.close()
			out.close()
	cd(tempCWD)

def mergePerfFiles(dirLoc, outName):
	tempCWD = os.getcwd()
	if dirLoc == '':
		dirLoc = tempCWD
	cd(dirLoc)
	files=os.listdir(dirLoc)
	for f in files:
		if f.endswith(".log1") or f.endswith(".log2"):
			data=open(f,"r")
			out=open(outName, 'a')
			for line in data:
				out.write(line)
			data.close()
			out.close()
	cd(tempCWD)

def cleanFiles(synName):
	if os.path.exists(synName):
		os.remove(synName)
	# keep the following source code since we use it in test phase
	#if os.path.exists(synName + '.c'):
	#	os.remove(synName + '.c')

	path = os.getcwd()
	for root, dirs, files in os.walk(path):
		for file in files:
			if (file.startswith('umbra.') and file.endswith('.proc.log')) or file.endswith('.perf.log'):
				os.remove(file)

	if os.path.exists('genAlgParams.txt'):
		os.remove('genAlgParams.txt')
	if os.path.exists('graph.txt'):
		os.remove('graph.txt')

#**********************************************************************************************************************************#
print '*******************************************************************************'
print '          MINIME: Multicore Benchmark Synthesizer v1.5           '
print '*******************************************************************************'

if len(sys.argv) < 2:
	print 'Usage:', sys.argv[0], '-ep "<executable path> <parameters>"'
	sys.exit(1)

exeStr = ""
exe = []
isSourceExists = 0
count = 1
argc = len(sys.argv)

while (count < argc):
	if sys.argv[count] == '-ep':
		exeStr = sys.argv[count + 1]
	if sys.argv[count] == '-st':
		SYN_TYPE = sys.argv[count + 1]
	if sys.argv[count] == '-ss':
		SIM_THRESHOLD = sys.argv[count + 1]
	if sys.argv[count] == '-is':
		IND_SIM_THRESHOLD = sys.argv[count + 1]
	if sys.argv[count] == '-bl':
		LIB_TYPE = sys.argv[count + 1]
	if sys.argv[count] == '-ml':
		SIM_METRIC_TYPE = sys.argv[count + 1]
	if sys.argv[count] == '-ub':
		ITERATION_THRESHOLD = sys.argv[count + 1]
	if sys.argv[count] == '-lt':
		PER_THREAD = sys.argv[count + 1]
	if sys.argv[count] == '-ct':
		CHARACTERIZER_TYPE = sys.argv[count + 1]
	if sys.argv[count] == '-co':
		exeStr = sys.argv[count + 1]
		isSourceExists = 1
	if sys.argv[count] == '-h' or sys.argv[count] == '--h' or sys.argv[count] == '-help':
		print '-st <synthesis type: syn (synthetic), sal (stand-alone), swm (stand-alone with performance metrics), gpu (GPU opencl); deault is gpu>'
		print '-ss <similarity score threshold; default is 90>'
		print '-is <indivudial similarity score threshold; default is 60>'
		print '-bl <type of library: mcapi, mrapi, posix, opencl; default is opencl>'
		print '-ml <level of the metrics that will be similar; for CPU can be low, high, or all and for GPU can be high or all: CPU-low (IPC, CMR, BMR), CPU-high (PP, TC, CCR), all; default is all>'
		print '-ub <number of iterations (upper bound); default is 20>'
		print '-lt <CPU per-thread characterization and synthesis: true, false; default is true>'
		print '-ct <CPU characterizer type: perf, papiex; default is papiex>'
		print '-ep <executable file path and arguments in double quote; for example: "/bin/ls -l">'
		print '-co <executable file path and arguments in double quote; for example: "/bin/ls -l">'
		print '-h, --h, -help <shows this help and exit>'
		sys.exit(2)
	count = count + 2

#Check arguments
if SYN_TYPE != 'syn' and SYN_TYPE != 'sal' and SYN_TYPE != 'swm' and SYN_TYPE != 'gpu':
	print 'ERROR: "-st ' + SYN_TYPE + '" not set correctly! st can be "syn", "sal", "swn", or "gpu".'
	sys.exit(3)
if int(SIM_THRESHOLD) < 0 or int(SIM_THRESHOLD) > 100:
	print 'ERROR: "-ss ' + SIM_THRESHOLD + '"not set correctly! ss can be [0, 100].'
	sys.exit(3)
if int(IND_SIM_THRESHOLD) < 0 or int(IND_SIM_THRESHOLD) > 100 or IND_SIM_THRESHOLD > SIM_THRESHOLD:
	print 'ERROR: "-is ' + IND_SIM_THRESHOLD + '"not set correctly! is can be [0, 100] and should be less than ss.'
	sys.exit(3)
if LIB_TYPE != 'posix' and LIB_TYPE != 'mrapi' and LIB_TYPE != 'mcapi' and LIB_TYPE != 'opencl':
	print 'ERROR: "-bl ' + LIB_TYPE + '" not set correctly! bl can be "posix", "mrapi", "mcapi", or "opencl".'
	sys.exit(3)
if SIM_METRIC_TYPE != 'low' and SIM_METRIC_TYPE != 'high' and SIM_METRIC_TYPE != 'all':
	print 'ERROR: "-ml ' + SIM_METRIC_TYPE + '" not set correctly! ml can be "low", "high", or "all".'
	sys.exit(3)
if PER_THREAD != 'true' and PER_THREAD != 'false':
	print 'ERROR: "-lt ' + PER_THREAD + '" not set correctly! lt can be "true" or "false".'
	sys.exit(3)
if CHARACTERIZER_TYPE != 'perf' and CHARACTERIZER_TYPE != 'papiex':
	print 'ERROR: "-ct ' + CHARACTERIZER_TYPE + '" not set correctly! ct can be "perf" or "papiex".'
	sys.exit(3)
if PER_THREAD == 'true' and CHARACTERIZER_TYPE != 'papiex':
	print 'ERROR: "-lt ' + PER_THREAD + 'and "-ct ' + CHARACTERIZER_TYPE + '" not set correctly! ct should be "papiex" when lt is "true".'
	sys.exit(3)

#**********************************************************************************************************************************#
#Global operations
exe = exeStr.split()
editPaths(1, exe)

# synthetic source code
if isSourceExists == 1:
	ITERATION_THRESHOLD = 1
	GEN_SYN = 'false'

if SYN_TYPE == 'syn':
	if not os.access(exe[0], os.X_OK):
		print 'ERROR: Cannot find or execute file!'
		sys.exit(2)
else:
	if not os.access(exe[0], os.R_OK):
		print 'ERROR: Cannot find or read file!'
		sys.exit(2)

appName = os.path.basename(os.path.splitext(exe[0])[0])
appDir = os.path.dirname(os.path.splitext(exe[0])[0])

mkdirP("temp")
cd("temp")
mkdirP(LIB_TYPE)
cd(LIB_TYPE)
mkdirP(appName + '_temp')
cd(appName + '_temp')

synName = appName + '_syn'
CURR_DIR = os.getcwd()
if SYN_TYPE != 'gpu':
	synExe = [CURR_DIR + '/' + synName]
	synCount = 1
else:
	synExe = [CURR_DIR + '/' + synName, '--load', synName + '_Kernels.bin', '-q']
	synCount = 4

if IS_CHAR_ORG == 'true':
	cleanFiles(synName)
else:
	if os.path.exists('genAlgParams.txt'):
		os.remove('genAlgParams.txt')

#**********************************************************************************************************************************#
if SYN_TYPE == 'syn':
	print '(Step 1) Running Original Application'

	tempCWD = os.getcwd()
	cd(appDir)

	originalCtimeBefore = getChildTime()
	subprocess.call(exe)
	originalCtimeAfter = getChildTime()
	print 'Original runtime:', originalCtimeAfter - originalCtimeBefore

#**********************************************************************************************************************************#
if SYN_TYPE != 'gpu':
	if SYN_TYPE == 'syn':
		print '(Step 2) Original Application Characterization'
	
		origLogFile = findInSubdirectory(appName + '.', '.perf.log')
		if origLogFile != '':
			os.remove(origLogFile)

		cleanLogDirs(appName + '.papiex.', tempCWD)

		if PER_THREAD == 'true' or CHARACTERIZER_TYPE == 'papiex':
			os.environ['LD_LIBRARY_PATH'] = "$LD_LIBRARY_PATH:/lib:/usr/lib:/usr/local/lib"
			cmd = [PERF_CHAR_DIR + 'papiex', '--no-scientific', '-m', '-f', tempCWD, 
			'-e', 'UNHALTED_CORE_CYCLES', '-e', 'INSTRUCTION_RETIRED', 
			'-e', 'LLC_REFERENCES', '-e', 'LLC_MISSES', 
			'-e', 'BRANCH_INSTRUCTIONS_RETIRED', '-e', 'MISPREDICTED_BRANCH_RETIRED', '--']

			index = 0
			count = len(exe)
			while index < count:
				cmd.append(exe[index])
				index = index + 1

			charRepeatCount = 0
			while charRepeatCount < int(CHAR_REPEAT):
				p = subprocess.Popen(cmd, stdout = open('out.tmp', 'w'), stderr = open('out.tmp', 'w'))
				p.wait()
				charRepeatCount = charRepeatCount + 1

			logDir = findDirInSubdirectory(appName + '.papiex.', tempCWD)
			if logDir != '':
				while logDir != '':
					mergeAllTextFiles(logDir, appName + '.perf.log')
					shutil.rmtree(logDir)
					logDir = findDirInSubdirectory(appName + '.papiex.', tempCWD)
			else:
				mergeAllTextFiles(logDir, appName + '.perf.log')

			if os.path.exists('out.tmp'):
				os.remove('out.tmp')
		
		else:
			cmd = ['perf', 'stat', '-r', CHAR_REPEAT, '-e', 'cycles', '-e', 'instructions', 
				'-e', 'cache-references', '-e', 'cache-misses']
			index = 0
			count = len(exe)
			while index < count:
				cmd.append(exe[index])
				index = index + 1
			p = subprocess.Popen(cmd, stdout = open(appName + '.perf.log', 'w'), stderr = open(appName + '.perf.log1', 'w'))
			p.wait()

			cmd = ['perf', 'stat', '-r', CHAR_REPEAT, '-e', 'branches', '-e', 'branch-misses']
			index = 0
			count = len(exe)
			while index < count:
				cmd.append(exe[index])
				index = index + 1
			p = subprocess.Popen(cmd, stdout = open(appName + '.perf.log', 'w'), stderr = open(appName + '.perf.log2', 'w'))
			p.wait()

			mergePerfFiles(os.getcwd(), appName + '.perf.log')
			if os.path.exists(appName + '.perf.log2'):
				os.remove(appName + '.perf.log2')
			if os.path.exists(appName + '.perf.log1'):
				os.remove(appName + '.perf.log1')

		origLogFile = findInSubdirectory('umbra.' + appName + '.',  '.proc.log')
		if origLogFile != '':
			os.remove(origLogFile)

		cmd = [DYNAMORIO_DIR + 'drrun', '-ops', 
			'-no_enable_reset -thread_private -max_bb_instrs 512 -no_finite_bb_cache -no_finite_trace_cache', 
			'-client', UMBRA_DIR + 'build/bin/libaio.so', '0x0', '']
		index = 0
		while index < count:
			cmd.append(exe[index])
			index = index + 1
		p = subprocess.Popen(cmd)
		p.wait()

		origLogFile = findInSubdirectory('umbra.' + appName + '.', '.proc.log')
		if os.path.exists(origLogFile):
			shutil.move(origLogFile, tempCWD)
		if os.path.exists(appName + '.perf.log'):
			shutil.move(appName + '.perf.log', tempCWD)

		cd(tempCWD)

else: #gpu
	print '(Step 2) Original Application Characterization'
	
	if IS_CHAR_ORG == 'true':
		origLogFile = findInSubdirectory(appName + '.', '.perf.log')
		if origLogFile != '':
			os.remove(origLogFile)

		# m2s --si-sim detailed --si-report report.txt --mem-report mem-report.txt --si-calc graph 
		# originalBenchmark --load SyntheticBenchmark_Kernels.bin
		cmd = [M2S_EXE_DIR + 'm2s', '--si-sim', 'detailed', 
			'--si-report', appName + '-report.txt', '--mem-report', appName + '-mem-report.txt', 
			'--si-calc', appName + '-graph']
		index = 0
		while index < count:
			cmd.append(exe[index])
			index = index + 1
		p = subprocess.Popen(cmd) #, stderr = open('console.txt', 'w')
		p.wait()

		mergeTextFiles(appName + '-report.txt',  appName + '.perf.log')
		mergeTextFiles(appName + '-mem-report.txt',  appName + '.perf.log')
		mergeAllTextFilesWithPrefix("", appName + '-graph', appName + '.perf.log')
		
#**********************************************************************************************************************************#
if SYN_TYPE != 'gpu':
	if SYN_TYPE == 'syn':
		print '(Step 3) Pattern Recognition / Initial Synthetic Benchmark Generation'
		#if synthetic benchmark source code not exists, then generate; else just use existing one
		if isSourceExists == 0:
			origLogFile = findInSubdirectory('umbra.' + appName + '.', '.proc.log')
			cmd = [BENCH_SYN_DIR + 'Debug/MBDT_Project', '-p', origLogFile, '-b', LIB_TYPE, '-g', GEN_SYN, 
				'-o', PER_THREAD, '-r', CHARACTERIZER_TYPE]
			subprocess.call(cmd)
	else:
		if isSourceExists == 0:
			cmd = [BENCH_SYN_DIR + 'Debug/MBDT_Project', '-a', exe[0], '-b', LIB_TYPE, '-g', GEN_SYN, 
				'-o', PER_THREAD, '-r', CHARACTERIZER_TYPE]
			subprocess.call(cmd)
else: #gpu
	print '(Step 3) Pattern Recognition / Initial Synthetic Benchmark Generation'
	if isSourceExists == 0:
		origLogFile = findInSubdirectory(appName + '.', '.perf.log')
		cmd = [BENCH_SYN_DIR + 'Debug/MBDT_Project', '-p', origLogFile, '-b', LIB_TYPE, '-g', GEN_SYN]
		subprocess.call(cmd)


#**********************************************************************************************************************************#
#retcode = 3: similarity met; retcode = 2: similarity not met; retcode = else: error
iteration = 0
retcode = 2
while (retcode != 3) and (iteration < ITERATION_THRESHOLD):
	iteration = iteration + 1
	print 'Iteration:', iteration
	print '(Step 4) Running Synthetic Benchmark'

	if SYN_TYPE != 'gpu':
		if LIB_TYPE == 'posix':
			cmd = ['gcc', '-O0', CURR_DIR + '/' + appName + '_syn.c', '-o', synExe[0], '-lpthread']
		else:
			#etem@etem-Dell-System-XPS-L502X:~/workspace/MCA_Project/Debug/src$ ar r libmca.a *.o
			cmd = ['gcc', '-O0', CURR_DIR + '/' + appName + '_syn.c', '-o', synExe[0],
				'-I', MCA_DIR + 'include', '-L', MCA_DIR + 'Debug/src', '-lmca', '-lpthread']
		subprocess.call(cmd)
	
		if os.path.exists(synExe[0]):
			os.chmod(synExe[0], 0744)
		else:
			print 'ERROR:', synExe[0], 'compilation failed!'
			#cleanFiles(synName)
			sys.exit(3)

		synCtimeBefore = getChildTime()
		subprocess.call(synExe[0])
		synCtimeAfter = getChildTime()
		print 'Synthetic runtime:', synCtimeAfter - synCtimeBefore

	else: #gpu
		cmd = ['gcc', '-m32', '-msse2', '-Wpointer-arith', '-Wfloat-equal', '-g3', '-O3', '-ffor-scope', 
			'-I', OCL_SDK_DIR + 'include/SDKUtil/', '-I', OCL_SDK_DIR + 'include/CL/', 
			'-I', M2S_DIR + 'runtime/include',  '-o', appName + '_syn.o', '-c', appName + '_syn.cpp']
		subprocess.call(cmd)

		cmd = ['gcc', '-o', synExe[0], appName + '_syn.o', '-O3', '-m32', 
			'-L', OCL_SDK_DIR + 'lib/x86/', '-lm2s-opencl', 
			'-lpthread', '-ldl', '-lstdc++', '-static', '-lm', '-lc', '-lgcc', '-lrt']
		subprocess.call(cmd)

		if os.path.exists(synExe[0]):
			os.chmod(synExe[0], 0744)
		else:
			print 'ERROR:', synExe[0], 'compilation failed!'
			#cleanFiles(synName)
			sys.exit(3)

		cmd = [M2S_EXE_DIR + 'm2c', '-O0', '--amd', '--amd-device', 'Tahiti', appName + '_syn_Kernels.cl']
		subprocess.call(cmd)
	
		if not os.path.exists(appName + '_syn_Kernels.bin'):
			print 'ERROR:', appName + '_syn_Kernels.cl', 'compilation failed!'
			sys.exit(3)

#**********************************************************************************************************************************#
	if SYN_TYPE != 'gpu':
		if SYN_TYPE != 'sal':

			synLogFile = findInSubdirectory(synName + '.', '.perf.log')
			if synLogFile != '':
				os.remove(synLogFile)
		
			cleanLogDirs(synName + '.papiex.', tempCWD)
		
			print '(Step 5) Synthetic Benchmark Characterization'
			if PER_THREAD == 'true' or CHARACTERIZER_TYPE == 'papiex':
				os.environ['LD_LIBRARY_PATH'] = "$LD_LIBRARY_PATH:/lib:/usr/lib:/usr/local/lib"
				cmd = [PERF_CHAR_DIR + 'papiex', '--no-scientific', '-m', '-f', tempCWD, 
					'-e', 'UNHALTED_CORE_CYCLES', '-e', 'INSTRUCTION_RETIRED', 
					'-e', 'LLC_REFERENCES', '-e', 'LLC_MISSES', 
					'-e', 'BRANCH_INSTRUCTIONS_RETIRED', '-e', 'MISPREDICTED_BRANCH_RETIRED', '--']

				index = 0
				count = len(exe)
				while index < synCount:
					cmd.append(synExe[index])
					index = index + 1

				charRepeatCount = 0
				while charRepeatCount < int(CHAR_REPEAT):
					p = subprocess.Popen(cmd, stdout = open('out.tmp', 'w'), stderr = open('out.tmp', 'w'))
					p.wait()
					charRepeatCount = charRepeatCount + 1

				logDir = findDirInSubdirectory(synName + '.papiex.', tempCWD)
				if logDir != '':
					while logDir != '':
						mergeAllTextFiles(logDir, synName + '.perf.log')
						shutil.rmtree(logDir)
						logDir = findDirInSubdirectory(synName + '.papiex.', tempCWD)
				else:
					mergeAllTextFiles(logDir, synName + '.perf.log')			

				if os.path.exists('out.tmp'):
					os.remove('out.tmp')
			
			else:
				cmd = ['perf', 'stat', '-r', CHAR_REPEAT, '-e', 'cycles', '-e', 'instructions', 
					'-e', 'cache-references', '-e', 'cache-misses']
				index = 0
				while index < synCount:
					cmd.append(synExe[index])
					index = index + 1
				p = subprocess.Popen(cmd, stdout = open(synName + '.perf.log', 'w'), stderr = open(synName + '.perf.log1', 'w'))
				p.wait()

				cmd = ['perf', 'stat', '-r', CHAR_REPEAT, '-e', 'branches', '-e', 'branch-misses']
				index = 0
				while index < synCount:
					cmd.append(synExe[index])
					index = index + 1
				p = subprocess.Popen(cmd, stdout = open(synName + '.perf.log', 'w'), stderr = open(synName + '.perf.log2', 'w'))
				p.wait()

				mergePerfFiles(os.getcwd(), synName + '.perf.log')
				if os.path.exists(synName + '.perf.log2'):
					os.remove(synName + '.perf.log2')
				if os.path.exists(synName + '.perf.log1'):
					os.remove(synName + '.perf.log1')

		if SYN_TYPE == 'syn':
			synLogFile = findInSubdirectory('umbra.' + synName + '.', '.proc.log')
			if synLogFile != '':
				os.remove(synLogFile)

			cmd = [DYNAMORIO_DIR + 'drrun', '-ops', 
				'-no_enable_reset -thread_private -max_bb_instrs 512 -no_finite_bb_cache -no_finite_trace_cache', 
				'-client', UMBRA_DIR + 'build/bin/libaio.so', '0x0', '']
			index = 0
			while index < synCount:
				cmd.append(synExe[index])
				index = index + 1
			p = subprocess.Popen(cmd)
			p.wait()

	else: #gpu
		synLogFile = findInSubdirectory(synName + '.', '.perf.log')
		if synLogFile != '':
			os.remove(synLogFile)

		# m2s --si-sim detailed --si-report report.txt --mem-report mem-report.txt --si-calc graph 
		# SyntheticBenchmark --load SyntheticBenchmark_Kernels.bin
		cmd = [M2S_EXE_DIR + 'm2s', '--si-sim', 'detailed', 
			'--si-report', synName + '-report.txt', '--mem-report', synName + '-mem-report.txt', 
			'--si-calc', synName + '-graph']
		index = 0
		while index < synCount:
			cmd.append(synExe[index])
			index = index + 1
		p = subprocess.Popen(cmd) #, stderr = open('console.txt', 'w')
		p.wait()

		mergeTextFiles(synName + '-report.txt',  synName + '.perf.log')
		mergeTextFiles(synName + '-mem-report.txt',  synName + '.perf.log')
		mergeAllTextFilesWithPrefix("", synName + '-graph', synName + '.perf.log')
		synLogFile = findInSubdirectory(synName + '.', '.perf.log')

#**********************************************************************************************************************************#
	print '(Step 6) Benchmark Synthesis'
	
	if SYN_TYPE != 'gpu':	
		if SYN_TYPE == 'syn':
			origLogFile = findInSubdirectory('umbra.' + appName + '.', '.proc.log')
			synLogFile = findInSubdirectory('umbra.' + synName + '.', '.proc.log')
			cmd = [BENCH_SYN_DIR + 'Debug/MBDT_Project', '-p', origLogFile, CURR_DIR + '/' + appName + '.perf.log', 
				'-c', synLogFile, CURR_DIR + '/' + synName + '.perf.log', '-s', SIM_THRESHOLD, '-i', IND_SIM_THRESHOLD, 
				'-b', LIB_TYPE, '-l', SYN_CODE_LEVEL, '-g', GEN_SYN, '-m', SIM_METRIC_TYPE, 
				'-o', PER_THREAD, '-r', CHARACTERIZER_TYPE]
			retcode = subprocess.call(cmd)
		else:
			if SYN_TYPE == 'swm':
				cmd = [BENCH_SYN_DIR + 'Debug/MBDT_Project', '-a', exe[0], 
					'-e', synName + '.perf.log', '-s', SIM_THRESHOLD, '-i', IND_SIM_THRESHOLD, 
					'-b', LIB_TYPE, '-l', SYN_CODE_LEVEL, '-g', GEN_SYN, 
					'-m', SIM_METRIC_TYPE, '-o', PER_THREAD, '-r', CHARACTERIZER_TYPE]
			else:
				cmd = [BENCH_SYN_DIR + 'Debug/MBDT_Project', '-a', exe[0], 
					'-s', SIM_THRESHOLD, '-i', IND_SIM_THRESHOLD, 
					'-b', LIB_TYPE, '-l', SYN_CODE_LEVEL, '-g', GEN_SYN, '-m', SIM_METRIC_TYPE]		
			retcode = subprocess.call(cmd)

	else: #gpu
		origLogFile = findInSubdirectory(appName + '.', 'perf.log')
		synLogFile = findInSubdirectory(synName + '.', '.perf.log')
		cmd = [BENCH_SYN_DIR + 'Debug/MBDT_Project', '-p', origLogFile, CURR_DIR + '/' + appName + '.perf.log', 
			'-c', synLogFile, CURR_DIR + '/' + synName + '.perf.log', '-s', SIM_THRESHOLD, '-i', IND_SIM_THRESHOLD, 
			'-b', LIB_TYPE, '-l', SYN_CODE_LEVEL, '-g', GEN_SYN, '-m', SIM_METRIC_TYPE, ]
		retcode = subprocess.call(cmd)


if SYN_TYPE != 'gpu':
	if SYN_TYPE == 'syn':
		print 'Synthetic benchmark generation completed.'
		print 'Number of iterations:', iteration
	else:
		if SYN_TYPE == 'swm':
			print 'Stand-alone benchmark with similarity metrics generation completed.'
			print 'Number of iterations:', iteration
		else:
			print 'Stand-alone benchmark generation completed.'
else: #gpu
	print 'Synthetic benchmark generation completed.'
	print 'Number of iterations:', iteration

print '*******************************************************************************'

#**********************************************************************************************************************************#
#cleanFiles(synName)

#**********************************************************************************************************************************#
