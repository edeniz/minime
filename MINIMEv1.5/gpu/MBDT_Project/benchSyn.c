/*
 * Copyright (c) 2011-2015,
 * Etem Deniz <etem.deniz@boun.edu.tr>, Bogazici University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * benchSyn.c
 *
 *  Created on: Jan 4, 2012
 *      Author: Etem Deniz
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "dataPrep.h"
#include "patRecog.h"
#include "benchPrep.h"
#include "benchGen.h"
#include "genAlg.h"

boolean isGenSyn = true;
characterizerType charType = char_perf;
boolean isPerThread = false;
boolean isOriginal = true;

void parseInput(int argc, char **argv, int *appCount, char *apps[MAX_PROCS * 2],
		int *candCount, char *cands[MAX_PROCS * 2], paralPattern targetPatterns[MAX_GROUP_NUMBER]) {

	int i, *count = 0, tarPatIndex = 0;
	char **progs = NULL;

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--h") == 0 || strcmp(argv[i], "-help") == 0) {
			printf("Multicore Benchmark Development Tool (MBDT) v1.5\n");
			printf("  -a <user defined stand-alone log file>\n");
			printf("  -e <stand-alone benchmark performance file>\n");
			printf("  -p <original log files>\n");
			printf("  -c <candidate log files>\n");
			printf("  -o <per-thread characterization and synthesis: true, false; default is true>\n");
			printf("  -s <similarity score threshold; default is 80/100>\n");
			printf("  -i <individual similarity score threshold; default is 0/100>\n");
			printf("  -b <type of library: mcapi, mrapi, posix; default is posix>\n");
			printf("  -t <phase index> <user defined parallel pattern: TP, DaC, GD, RD, Pl, or EbC>\n");
			printf("  -d <phase index> <user defined dwarf: SLA, SM, NBM, SG, UG, MR, CL, GT, DP, BBB, GM, or FSM>\n");
			printf("  -g <generate synthetic benchmark: true, false; default is true>\n");
			printf("  -l <level of the tuning code blocks: low, high; default is low>\n");
			printf("  -m <level of the metrics that will be similar: low, high, all; default is all>\n");
			printf("  -r <characterizer type: perf, papiex; default is perf>\n");
			printf("  -h, --h, -help <shows this help and exit>\n");
			exit(0);
		}
		if (strcmp(argv[i], "-p") == 0) {
			count = appCount;
			progs = (char **) apps;
		} else if (strcmp(argv[i], "-c") == 0) {
			count = candCount;
			progs = (char **) cands;
		} else if (strcmp(argv[i], "-a") == 0) {
			count = appCount;
			progs = (char **) apps;
			isStandalone = true;
		} else if (strcmp(argv[i], "-e") == 0) {
			count = candCount;
			progs = (char **) cands;
		} else if (strcmp(argv[i], "-s") == 0) {
			i++;
			similarityThreshold = atoi(argv[i]);
			/* default: similarityThreshold = 80 */
		} else if (strcmp(argv[i], "-i") == 0) {
			i++;
			indSimilarityThreshold = atoi(argv[i]);
			/* default: indSimilarityThreshold = 0 */
		} else if (strcmp(argv[i], "-b") == 0) {
			i++;
			if (strcmp(argv[i], "mrapi") == 0) {
				hasThreadMrapiExt = true;
			} else if (strcmp(argv[i], "mcapi") == 0) {
				hasThreadMcapiExt = true;
			} else if (strcmp(argv[i], "posix") == 0) {
				hasThreadPosixExt = true;
			} else if (strcmp(argv[i], "opencl") == 0) {
				hasThreadOpenClExt = true;
			} else { /* default = posix */
				hasThreadPosixExt = true;
			}
		} else if (strcmp(argv[i], "-t") == 0) {
			i++;
			tarPatIndex = atoi(argv[i]) - 1;
			i++;
			if (strcmp(argv[i], "TP") == 0) {
				targetPatterns[tarPatIndex] = PP_TP;
			} else if (strcmp(argv[i], "DaC") == 0) {
				targetPatterns[tarPatIndex] = PP_DaC;
			} else if (strcmp(argv[i], "GD") == 0) {
				targetPatterns[tarPatIndex] = PP_GD;
			} else if (strcmp(argv[i], "RD") == 0) {
				targetPatterns[tarPatIndex] = PP_RD;
			} else if (strcmp(argv[i], "Pl") == 0) {
				targetPatterns[tarPatIndex] = PP_Pl;
			} else if (strcmp(argv[i], "EbC") == 0) {
				targetPatterns[tarPatIndex] = PP_EbC;
			} else {
				targetPatterns[tarPatIndex++] = PP_NA;
			}
		} else if (strcmp(argv[i], "-d") == 0) {
			i++;
			tarPatIndex = atoi(argv[i]) - 1;
			i++;
			if (strcmp(argv[i], "DLA") == 0) {
				targetPatterns[tarPatIndex] = DW_DLA;
			} else if (strcmp(argv[i], "SLA") == 0) {
				targetPatterns[tarPatIndex] = DW_SLA;
			} else if (strcmp(argv[i], "SM") == 0) {
				targetPatterns[tarPatIndex] = DW_SM;
			} else if (strcmp(argv[i], "NBM") == 0) {
				targetPatterns[tarPatIndex] = DW_NBM;
			} else if (strcmp(argv[i], "SG") == 0) {
				targetPatterns[tarPatIndex] = DW_SG;
			} else if (strcmp(argv[i], "UG") == 0) {
				targetPatterns[tarPatIndex] = DW_UG;
			} else if (strcmp(argv[i], "MR") == 0) {
				targetPatterns[tarPatIndex] = DW_MR;
			} else if (strcmp(argv[i], "CL") == 0) {
				targetPatterns[tarPatIndex] = DW_CL;
			} else if (strcmp(argv[i], "GT") == 0) {
				targetPatterns[tarPatIndex] = DW_GT;
			} else if (strcmp(argv[i], "DP") == 0) {
				targetPatterns[tarPatIndex] = DW_DP;
			} else if (strcmp(argv[i], "BBB") == 0) {
				targetPatterns[tarPatIndex] = DW_BBB;
			} else if (strcmp(argv[i], "GM") == 0) {
				targetPatterns[tarPatIndex] = DW_GM;
			} else if (strcmp(argv[i], "FSM") == 0) {
				targetPatterns[tarPatIndex] = DW_FSM;
			} else {
				targetPatterns[tarPatIndex++] = DW_NA;
			}
		} else if (strcmp(argv[i], "-g") == 0) {
			i++;
			if (strcmp(argv[i], "false") == 0) {
				isGenSyn = false;
			}
		} else if (strcmp(argv[i], "-o") == 0) {
			i++;
			if (strcmp(argv[i], "true") == 0) {
				isPerThread = true;
			}
		} else if (strcmp(argv[i], "-l") == 0) {
			i++;
			if (strcmp(argv[i], "high") == 0) {
				codeLevel = 1;
			}
		} else if (strcmp(argv[i], "-m") == 0) {
			i++;
			if (strcmp(argv[i], "high") == 0) {
				simMetricLevel = METRIC_HIGH;
			} else if (strcmp(argv[i], "low") == 0) {
				simMetricLevel = METRIC_LOW;
			} else {
				simMetricLevel = METRIC_ALL;
			}
		} else if (strcmp(argv[i], "-r") == 0) {
			i++;
			if (strcmp(argv[i], "papiex") == 0) {
				charType = char_papiex;
			}
		} else {
			progs[(*count)++] = argv[i];
		}
	}

	/* default library is Posix */
	if (hasThreadMrapiExt == false && hasThreadMcapiExt == false && hasThreadPosixExt == false) {
		hasThreadPosixExt = true;
	}

	return;
}

void setRunningType(boolean isOrig) {

	if (isOrig != isOriginal) {

		if (!isOriginal && isOrig) { /* Candidate to Original */
			memcpy(cands, procs, sizeof(cands));
			candCount = procCount;
			maxCandGroupId = maxGroupId;
			memcpy(procs, buProcs, sizeof(procs));
			procCount = buCount;
			maxGroupId = buMaxGroupId;
		} else { /* Original to Candidate */
			memcpy(buProcs, procs, sizeof(buProcs));
			buCount = procCount;
			buMaxGroupId = maxGroupId;
			memcpy(procs, cands, sizeof(procs));
			procCount = candCount;
			maxGroupId = maxCandGroupId;
		}

		isOriginal = isOrig;
	}

	return;
}

void setTargetPatterns(int phaseCount, paralPattern pps[MAX_GROUP_NUMBER], paralPattern targetPatterns[MAX_GROUP_NUMBER]) {
	int i;

	for (i = 0; i < phaseCount; i++) {
		if (targetPatterns[i] != PP_NA) {
			printf("INFO: Setting parallel pattern of phase %d from %s to %s.\n",
					i + 1, getPatternText(pps[i]), getPatternText(targetPatterns[i]));
			pps[i] = targetPatterns[i];
		}
	}
}

//XXX: MBDT currently supports only single process.

/*
 * Input: ./MBDT_Project -p application.log -c candidate.log -t targetParallelPattern -s similarityThreshold
 * or
 * Input: ./MBDT_Project -a standAloneFile (-e candidate.log)
 * Output: Parallel pattern(s), Synthetic benchmark(s), Similarity Score(s)
 */
int main(int argc, char **argv) {

	int i, appCount = 0, candCount = 0; /* actually, counts show the input file counts */
	char *apps[MAX_PROCS * 2] = { NULL };
	char *cands[MAX_PROCS * 2] = { NULL };
	paralPattern pps[MAX_GROUP_NUMBER], targetPatterns[MAX_GROUP_NUMBER];
	int phaseCount = 0;
	int step = 0;
	int retVal = 0;

	if (argc > 1) {

		for (i = 0; i < MAX_GROUP_NUMBER; i++) {
			pps[i] = PP_NA;
			targetPatterns[i] = PP_NA;
		}

		parseInput(argc, argv, &appCount, apps, &candCount, cands, targetPatterns);

		if (appCount > 0) {

			initialize();

			printf("=============== Multicore Benchmark Development Tool (MBDT) v1.5 ==============\n");

			if (!isStandalone) { /* synthetic */
				if (!hasThreadOpenClExt) {
					if (candCount == 0) {
						printf("#%d. Data Preparation\n", ++step);
						retVal = prepData(appCount, apps);														/* (1) Data preparation */
						if (retVal < 0) return -1;
						printf("#%d. (Multiple Parallel) Pattern Detection\n", ++step);
						phaseCount = recogPattern(pps, true, true);												/* (2) Pattern recognition */
						setTargetPatterns(phaseCount, pps, targetPatterns);
						printf("#%d. Benchmark Data Preparation\n", ++step);
						prepBenchmark(phaseCount, pps, true);													/* (3) Benchmark preparation */
						printf("#%d. Benchmark Generation\n", ++step);
						genBenchmark(phaseCount, pps);															/* (4) Benchmark generation */
					} else {
						printf("#%d. Genetic Algorithm\n", ++step);
						retVal = geneticAlg(appCount, apps, candCount, cands, true, targetPatterns, true);		/* (5) Genetic algorithm */
					}
				} else { /* gpu: opencl */
					if (candCount == 0) {
						/* opencl application has only one dwarf */
						phaseCount = 1;
						setTargetPatterns(phaseCount, pps, targetPatterns);
						printf("#%d. Data Preparation\n", ++step);
						retVal = prepDataGpu(appCount, apps, false);											/* (1) Data preparation */
						if (retVal < 0) return -1;
						printf("#%d. Benchmark Data Preparation\n", ++step);
						prepBenchmarkGpu(phaseCount, pps);														/* (2) Benchmark preparation */
						printf("#%d. Benchmark Generation\n", ++step);
						genBenchmarkGpu(phaseCount, pps);														/* (3) Benchmark generation */
					} else {
						printf("#%d. Genetic Algorithm\n", ++step);
						retVal = geneticAlgGpu(appCount, apps, candCount, cands, true, targetPatterns, true);	/* (4) Genetic algorithm */
					}
				}
			} else { /* stand-alone */
				if (candCount == 0) { /* just generate; do not measure similarity */
					printf("#%d. Data Preparation\n", ++step);
					retVal = prepDataSa(appCount, apps);														/* (1) Data preparation */
					printf("#%d. Benchmark Data Preparation\n", ++step);
					prepBenchmarkSa(true);																		/* (2) Benchmark preparation */
					printf("#%d. Benchmark Generation\n", ++step);
					phaseCount = 1;
					pps[0] = PP_Hl;
					genBenchmarkSa(phaseCount, pps);															/* (3) Benchmark generation */
					retVal = 3; /* similarity score is met */
				} else {
					if (simMetricLevel != METRIC_LOW) {
						printf("INFO: Changing level of the metrics to low\n");
						simMetricLevel = METRIC_LOW;
					}
					printf("#%d. Genetic Algorithm\n", ++step);
					retVal = geneticAlgSa(appCount, apps, candCount, cands, true, true);						/* (4) Genetic algorithm */
				}
			}

			printf("===============================================================================\n");

			finalize();

		}

	} else {
		printf("ERROR! Usage: %s <proc_log_file> ...\n", argv[0]);
		retVal = -1;
	}

	return retVal;
}
