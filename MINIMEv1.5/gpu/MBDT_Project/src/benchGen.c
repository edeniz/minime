/*
 * Copyright (c) 2011-2015,
 * Etem Deniz <etem.deniz@boun.edu.tr>, Bogazici University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * benchGen.c
 *
 *  Created on: Jan 4, 2012
 *      Author: Etem Deniz
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include "benchGen.h"

const char* copyright =
		"/*\n"
		" * Copyright (c) 2015,\n"
		" * Etem Deniz <etem.deniz@boun.edu.tr>, Bogazici University\n"
		" * All rights reserved.\n"
		" * \n"
		" * Redistribution and use in source and binary forms, with or without\n"
		" * modification, are permitted provided that the following conditions are\n"
		" * met:\n"
		" * \n"
		" * (1) Redistributions of source code must retain the above copyright\n"
		" * notice, this list of conditions and the following disclaimer.\n"
		" * \n"
		" * (2) Redistributions in binary form must reproduce the above copyright\n"
		" * notice, this list of conditions and the following disclaimer in the\n"
		" * documentation and/or other materials provided with the distribution.\n"
		" * \n"
		" * (3) The names of the contributors may not be used to endorse or promote\n"
		" * products derived from this software without specific prior written\n"
		" * permission.\n"
		" * \n"
		" * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS\n"
		" * IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED\n"
		" * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A\n"
		" * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER\n"
		" * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,\n"
		" * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,\n"
		" * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR\n"
		" * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF\n"
		" * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING\n"
		" * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS\n"
		" * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n"
		" * \n"
		" */\n"
		"\n";

const char* headerInclude =
		"#include <stdio.h>\n"
		"#include <stdlib.h>\n"
		"#include <string.h>\n"
		"#include <pthread.h>\n"
		"#include <unistd.h>\n";

const char* mrapiHeaderInclude =
		"#include \"mrapi.h\"\n"
		"#include \"mrapi_ext.h\"\n";

const char* mcapiHeaderInclude =
		"#include \"mca_config.h\"\n"
		"#include \"mcapi.h\"\n";

const char* posixHeaderInclude =
		"#include <semaphore.h>\n"
		"#include <errno.h>\n"
		"#include <fcntl.h>\n";

const char* mtapieaderInclude =
		"#include \"mtapi.h\"\n";

const char* gpuHeaderInclude =
		"#include <stdio.h>\n"
		"#include <stdlib.h>\n"
		"#include <string.h>\n"
		"#include <assert.h>\n"
		"#include <CL/cl.h>\n"
		"#include \"CLUtil.hpp\"\n";

const char* mrapiDefines =
		"#ifndef MRAPI_DOMAIN\n"
		"#define MRAPI_DOMAIN 1\n"
		"#endif\n"
		"\n"
		"#define LOCK_LIMIT 1\n"
		"\n";

const char* mcapiDefines =
		"#ifndef MCAPI_DOMAIN\n"
		"#define MCAPI_DOMAIN 1\n"
		"#endif\n"
		"\n"
		"#define BUFF_SIZE 64\n"
		"#define MCAPI_TIMEOUT 1024\n"
		"#define MCAPI_ENDPOINT_INVALID (~0)\n"
		"\n";

const char* structures =
		"";

const char* mrapiGlobalVars =
		"char mrapiStatusBuff[MRAPI_MAX_STATUS_SIZE];\n"
		"\n";

const char* mcapiGlobalVars =
		"char mcapiStatusBuff[MCAPI_MAX_STATUS_SIZE];\n"
		"\n";

const char* wrongStr =
		"#define WRONG wrong(td->tid, status, __FILE__, __LINE__);\n"
		"void wrong(unsigned int tid, char *status, char *file, unsigned line) {\n"
		"\tfprintf(stderr,\"WRONG: tid=%d, status=%s, file=%s, line=%u\\n\", tid, status, file, line);\n"
		"\tfflush(stdout);\n"
		"\texit(1);\n"
		"}\n"
		"\n";

const char* mrapiWrongStr =
		"#define MRAPI_WRONG mrapiWrong(td->tid, mrapiStatus, __FILE__, __LINE__);\n"
		"void mrapiWrong(mca_node_t tid, mrapi_status_t status, char *file, unsigned line) {\n"
		"\tfprintf(stderr,\"WRONG: tid=%d, status=%s, file=%s, line=%u\\n\", tid, mrapi_display_status(status, mrapiStatusBuff, sizeof(mrapiStatusBuff)), file, line);\n"
		"\tfflush(stdout);\n"
		"\tmrapi_finalize(&status);\n"
		"\texit(1);\n"
		"}\n"
		"\n";

const char* mrapiCheckSuccessStr =
		"#define MRAPI_CHECK mrapiCheckSuccess(td->tid, mrapiStatus, __FILE__, __LINE__);\n"
		"void mrapiCheckSuccess(mca_node_t tid, mrapi_status_t status, char *file, unsigned line) {\n"
		"\tif (status != MRAPI_SUCCESS) {\n"
		"\t\tprintf(\"ERROR: tid=%d, status=%s, file=%s, line=%u\\n\", tid, mrapi_display_status(status, mrapiStatusBuff, sizeof(mrapiStatusBuff)), file, line);\n"
		"\t}\n"
		"}\n"
		"\n";

const char* mrapiFuncPars =
		"\tmrapi_parameters_t mrapiParms = 0;\n"
		"\tmrapi_info_t mrapiVersion;\n"
		"\tmrapi_status_t mrapiStatus;\n";

const char* mcapiWrongStr =
		"#define MCAPI_WRONG mcapiWrong(td->tid, mcapiStatus, __FILE__, __LINE__);\n"
		"void mcapiWrong(mca_node_t tid, mcapi_status_t status, char *file, unsigned line) {\n"
		"\tfprintf(stderr,\"WRONG: tid=%d, status=%s, file=%s, line=%u\\n\", tid, mcapi_display_status(status, mcapiStatusBuff, sizeof(mcapiStatusBuff)), file, line);\n"
		"\tfflush(stdout);\n"
		"\t/* mrapi_finalize(&mrapiStatus); // mcapi_finalize does */\n"
		"\tmcapi_finalize(&status);\n"
		"\texit(1);\n"
		"}\n"
		"\n";

const char* mcapiCheckSuccessStr =
		"#define MCAPI_CHECK mcapiCheckSuccess(td->tid, mcapiStatus, __FILE__, __LINE__);\n"
		"void mcapiCheckSuccess(mca_node_t tid, mcapi_status_t status, char *file, unsigned line) {\n"
		"\tif (status != MCAPI_SUCCESS) {\n"
		"\t\tprintf(\"ERROR: tid=%d, status=%s, file=%s, line=%u\\n\", tid, mcapi_display_status(status, mcapiStatusBuff, sizeof(mcapiStatusBuff)), file, line);\n"
		"\t}\n"
		"}\n"
		"\n";

const char* mcapiFuncPars =
		"\tmcapi_param_t mcapiParms = 0;\n"
		"\tmcapi_info_t mcapiVersion;\n"
		"\tmcapi_status_t mcapiStatus;\n";

const char* posixWrongStr =
		"#define POSIX_WRONG posixWrong(td->tid, retVal, __FILE__, __LINE__);\n"
		"void posixWrong(int tid, int retVal, char *file, unsigned line) {\n"
		"\tfprintf(stderr, \"WRONG: tid=%d, retval=%d, errno=%d, file=%s, line=%u\\n\", tid, retVal, errno, file, line);\n"
		"\tfflush(stdout);\n"
		"\texit(1);\n"
		"}\n"
		"\n";

const char* posixFuncPars =
		"\tint retVal = -1;\n";

boolean isThreadStructData = false;
boolean isVariablesInSingleLine = true;
boolean isTlsVarArray = true;

int semLock(FILE *fw, struct tThreadData *td, int index, char *tab,  boolean hasNewLine);
int posixSemLock(FILE *fw, struct tThreadData *td, int index, char *tab,  boolean hasNewLine);

char *getOperationTypeText(workOpType type) {

	char *opType;

	switch (type) {
	case WORK_OP_NOOP:
		opType = "code block for computation";
		break;
	case WORK_OP_IPC_INC:
		opType = "code block for IPC increment";
		break;
	case WORK_OP_IPC_DEC:
		opType = "code block for IPC decrement";
		break;
	case WORK_OP_CACHE_HIT:
		opType = "code block for CMR decrement";
		break;
	case WORK_OP_CACHE_MISS:
		opType = "code block for CMR increment";
		break;
	case WORK_OP_BRNCH_HIT:
		opType = "code block for BMR decrement";
		break;
	case WORK_OP_BRNCH_MISS:
		opType = "code block for BMR increment";
		break;
	case WORK_OP_COMP_INC:
		opType = "code block for CCR decrement";
		break;
	case WORK_OP_COMM_INC:
		opType = "code block for CCR increment";
		break;
	default:
		opType = "general";
		break;
	}

	return opType;

}

/***************************************************headers******************************************************/

void genHeader(FILE *fw, paralPattern pps[MAX_GROUP_NUMBER], boolean hasMrapiExt, boolean hasMcapiExt) {

	int i;

	fprintf(fw, "%s", copyright);
	fprintf(fw, "/*\n");
	fprintf(fw, " * Description: Synthetic benchmark of %s program.\n", activeProc->name);
	if (maxGroupId > 0) {
		fprintf(fw, " *              Parallel pattern of %s program is ", activeProc->name);
		for (i = 0; i < maxGroupId; i++) {
			if (i == maxGroupId - 1) {
				fprintf(fw, "%s.\n", getPatternText(pps[i]));
			} else {
				fprintf(fw, "%s", getPatternText(pps[i]));
				if (pps[i] != PP_Hl) {
					fprintf(fw, " + ");
				} else {
					break;
				}
			}
		}
	}
	fprintf(fw, " */\n\n");

	fprintf(fw, "%s", headerInclude);
	if (hasMrapiExt) {
		fprintf(fw, "%s", mrapiHeaderInclude);
	}
	if (hasMcapiExt) {
		fprintf(fw, "%s", mcapiHeaderInclude);
	}
	if (hasThreadPosixExt) {
		if (!hasMrapiExt && !hasMcapiExt) {
			fprintf(fw, "%s", posixHeaderInclude);
		}
	}
	fprintf(fw, "\n");

	return;
}

/***************************************************defines******************************************************/

void genDefines(FILE *fw, boolean hasMrapiExt, boolean hasMcapiExt) {

	if (hasMrapiExt) {
		fprintf(fw, "%s", mrapiDefines);
	}
	if (hasMcapiExt) {
		fprintf(fw, "%s", mcapiDefines);
	}

	return;
}

/***************************************************structures******************************************************/

void getMaxTlsCounts(int *lclCount) {

	int i, numOfThreads;
	struct tThreadData *td;

	*lclCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->lsExt.hasExt) {
			if (td->lsExt.numOfLocalMems > *lclCount) {
				*lclCount = td->lsExt.numOfLocalMems;
			}
		}
	}

	return;
}

void getMaxMrapiCounts(int *mtxCount, int *semCount, int *shmCount, int *barCount) {

	int i, numOfThreads;
	struct tThreadData *td;

	*mtxCount = 0;
	*semCount = 0;
	*shmCount = 0;
	*barCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mrapiExt.hasExt) {
			if (td->mrapiExt.numOfMutexes > *mtxCount) {
				*mtxCount = td->mrapiExt.numOfMutexes;
			}
			if (td->mrapiExt.numOfSemaphores > *semCount) {
				*semCount = td->mrapiExt.numOfSemaphores;
			}
			if (td->mrapiExt.numOfSharedMems > *shmCount) {
				*shmCount = td->mrapiExt.numOfSharedMems;
			}
			if (td->mrapiExt.numOfBarriers > *barCount) {
				*barCount = td->mrapiExt.numOfBarriers;
			}
		}
	}

	return;
}

void getMaxMcapiCounts(int *epCount, int *pktChanCount, int *sclChanCount) {

	int i, numOfThreads;
	struct tThreadData *td;

	*epCount = 0;
	*pktChanCount = 0;
	*sclChanCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mcapiExt.hasExt) {
			if (td->mcapiExt.numOfEndpoints > *epCount) {
				*epCount = td->mcapiExt.numOfEndpoints;
			}
			if (td->mcapiExt.numOfPcktChans > *pktChanCount) {
				*pktChanCount = td->mcapiExt.numOfPcktChans;
			}
			if (td->mcapiExt.numOfScalChans > *sclChanCount) {
				*sclChanCount = td->mcapiExt.numOfScalChans;
			}
		}
	}

	return;
}

void genStructures(FILE *fw, boolean hasMrapiExt, boolean hasMcapiExt) {

	int lclCount;
	int mtxCount, semCount, shmCount, barCount;
	int epCount, pktChanCount, sclChanCount;

	fprintf(fw, "%s", structures);

	if (isThreadStructData) {
		getMaxTlsCounts(&lclCount);
		getMaxMrapiCounts(&mtxCount, &semCount, &shmCount, &barCount);
		getMaxMcapiCounts(&epCount, &pktChanCount, &sclChanCount);
	}

	fprintf(fw, "typedef struct {\n");

	if (isThreadStructData) {
		if (lclCount > 0) {
			fprintf(fw, "\tchar *localAddr[%d]; /* local mem address */\n", lclCount);
		}
		if (mtxCount > 0) {
			fprintf(fw, "\tmrapi_mutex_hndl_t mutex[%d]; /* mrapi mutex handle */\n", mtxCount);
		}
		if (semCount > 0) {
			fprintf(fw, "\tmrapi_sem_hndl_t sem[%d]; /* mrapi sem handle */\n", semCount);
		}
		if (shmCount > 0) {
			fprintf(fw, "\tmrapi_shmem_hndl_t shmem[%d]; /* mrapi shmem handle */\n", shmCount);
		}
		if (shmCount > 0) {
			fprintf(fw, "\tchar *shmAddr[%d]; /* shared mem address */\n", shmCount);
		}
		if (epCount > 0) {
			fprintf(fw, "\tmcapi_endpoint_t ep[%d]; /* mcapi endpoint */\n", epCount);
		}
		if (pktChanCount > 0) {
			fprintf(fw, "\tmcapi_pktchan_send_hndl_t pktSendHandle[%d]; /* packet channel send handle */\n", pktChanCount);
			fprintf(fw, "\tmcapi_pktchan_recv_hndl_t pktRecvHandle[%d]; /* packet channel recv handle */\n", pktChanCount);
		}
		if (sclChanCount > 0) {
			fprintf(fw, "\tmcapi_sclchan_send_hndl_t sclSendHandle[%d]; /* scalar channel send handle */\n", sclChanCount);
			fprintf(fw, "\tmcapi_sclchan_recv_hndl_t sclRecvHandle[%d]; /* scalar channel recv handle */\n", sclChanCount);
		}
	}

	fprintf(fw, "\tunsigned int tid;\n");
	fprintf(fw, "} threadData;\n");
	fprintf(fw, "\n");

	return;
}

/***************************************************globals******************************************************/

void genGlobals(FILE *fw, boolean hasMrapiExt, boolean hasMcapiExt) {

	int i, j, numOfThreads;
	struct tThreadData *td;
	boolean isWritten;

	if (hasMrapiExt) {
		fprintf(fw, "%s", mrapiGlobalVars);
	}
	if (hasMcapiExt) {
		fprintf(fw, "%s", mcapiGlobalVars);
	}

	if (hasThreadPosixExt) {
		if (!hasMrapiExt && !hasMcapiExt) {
			fprintf(fw, "%s", wrongStr);
		}
	}

	if (hasMrapiExt) {
		fprintf(fw, "%s", mrapiWrongStr);
		fprintf(fw, "%s", mrapiCheckSuccessStr);
		fprintf(fw, "%s", mrapiGlobalVars);
	}
	if (hasMcapiExt) {
		fprintf(fw, "%s", mcapiWrongStr);
		fprintf(fw, "%s", mcapiCheckSuccessStr);
		fprintf(fw, "%s", mcapiGlobalVars);
	}

	if (hasThreadPosixExt) {
		if (!hasMrapiExt && !hasMcapiExt) {
			fprintf(fw, "%s", posixWrongStr);
		}
	}

	for (i = 0; i < activeProc->lsExt.numOfGlobMems; i++) {
		if (strcmp(activeProc->lsExt.globMems[i].name, "") == 0) {
			fprintf(fw, "int globAddr%d[%d];\n", i + 1, activeProc->lsExt.globMems[i].size);
		} else {
			fprintf(fw, "int %s[%d][%d];\n", activeProc->lsExt.globMems[i].name,
					activeProc->lsExt.globMems[i].size, activeProc->lsExt.globMems[i].size2);
		}
	}
	if (activeProc->lsExt.numOfGlobMems > 0) {
		fprintf(fw, "\n");
	}

	numOfThreads = activeProc->numOfThreads;

	isWritten = false;
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		for (j = 0; j < td->posixExt.numOfMutexes; j++) {
			if (td->posixExt.mutexes[j].isOwner) {
				fprintf(fw, "pthread_mutex_t mutexRes%d = PTHREAD_MUTEX_INITIALIZER;\n", j + 1);
				fprintf(fw, "pthread_mutex_t *mutex%d = &mutexRes%d;\n", j + 1, j + 1);
				isWritten = true;
			}
		}
	}
	if (isWritten) {
		fprintf(fw, "\n");
	}

	isWritten = false;
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mrapiExt.hasExt) {
			for (j = 0; j < td->mrapiExt.numOfBarriers; j++) {
				if (td->mrapiExt.barriers[j].isOwner) {
					fprintf(fw, "volatile int barrier%d = 0;\n", td->mrapiExt.barriers[j].id);
					isWritten = true;
				}
			}
		}
	}
	if (isWritten) {
		fprintf(fw, "\n");
	}

	return;
}

/***************************************************globals******************************************************/

void genFuncSignatures(FILE *fw, boolean hasMrapiExt, boolean hasMcapiExt) {

	int i, numOfThreads, maxTaskId = -1;
	struct tThreadData *td;

	numOfThreads = activeProc->numOfThreads;
	for (i = 2; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId > maxTaskId) {
			maxTaskId = td->mtapiExt.startRoutine.taskId;
		}
	}

	fprintf(fw, "/* Task signatures */\n");
	for (i = 0; i <= maxTaskId; i++) {
		fprintf(fw, "void *task%d(void *param);\n", i);
	}
	if (maxTaskId >= 0) {
		fprintf(fw, "\n");
	}

	return;
}

/***************************************************parameters******************************************************/

boolean genMtapiPars(FILE *fw, struct tThreadData *activeTd) {

	int i, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean hasExt = false;

	if (activeTd->mtapiExt.numOfWorks > 0) {
		if (activeTd->id > 1) {
			fprintf(fw, "\tint workCount; /* work counter */\n");
		} else {
			fprintf(fw, "\tregister unsigned int workCount; /* work counter */\n");
//			fprintf(fw, "\tunsigned int workCount; /* work counter */\n");
		}
	}

	numOfThreads = activeProc->numOfThreads;
	for (i = activeTd->id; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mtapiExt.loopCount > 0) {
				fprintf(fw, "\tint loopCount; /* loop counter */\n");
				hasExt = true;
				break;
			}
		}
	}

	return hasExt;
}

boolean getMaxTlsCountsInTask(int *lclCount, int threadId, int taskId) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasExt = false;

	*lclCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->lsExt.hasExt) {
				if (td->lsExt.numOfLocalMems > *lclCount) {
					*lclCount = td->lsExt.numOfLocalMems;
				}
				hasExt = true;
			}
		}
	}

	return hasExt;
}

int getMaxTlsSizeInTask(int threadId, int taskId, int tlsIndex) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean maxSize = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->lsExt.hasExt) {
				if (td->lsExt.numOfLocalMems > tlsIndex) {
					if (td->lsExt.localMems[tlsIndex].size > maxSize) {
						maxSize = td->lsExt.localMems[tlsIndex].size;
					}
				}
			}
		}
	}

	return maxSize;
}

boolean hasGlobMemOps(struct tThreadData *activeTd) {

	int i, j, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean retVal = false;

	numOfThreads = activeProc->numOfThreads;
	for (i = activeTd->id; i <= numOfThreads && retVal == false; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			for (j = 0; j < td->lsExt.numOfMemOps; j++) {
				if (td->lsExt.memOps[j].type == MEM_TYPE_GLOBAL) {
					retVal = true;
					break;
				}
			}
		}
	}

	return retVal;

}

boolean genTlsPars(FILE *fw, struct tThreadData *activeTd) {

	int i;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	boolean isTlsFuncPars = false;
	int lclCountInTask, maxSize;

	getMaxTlsCountsInTask(&lclCountInTask, activeTd->id, taskId);
	if (lclCountInTask > 0) {
		if (!isTlsVarArray) {
			fprintf(fw, "\tchar *status;\n");
		}
		fprintf(fw, "\tint localMemTemp; /* temp read/write data */\n");
		fprintf(fw, "\tint localMemIt; /* temp read/write data */\n");
		for (i = 0; i < lclCountInTask; i++) {
			maxSize = getMaxTlsSizeInTask(activeTd->id, taskId, i);
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					if (isTlsVarArray) {
						fprintf(fw, ", localAddr%d[%d]", i + 1, maxSize);
					} else {
						fprintf(fw, ", *localAddr%d", i + 1);
					}
				} else { /* first */
					if (isTlsVarArray) {
						fprintf(fw, "\tint localAddr%d[%d]", i + 1, maxSize);
					} else {
						fprintf(fw, "\tint *localAddr%d", i + 1);
					}
				}
				if (i == lclCountInTask - 1) { /* last */
					fprintf(fw, "; /* thread local mem */\n");
				}
			} else {
				if (isTlsVarArray) {
					fprintf(fw, "\tint localAddr%d[%d]; /* thread local mem */\n", i + 1, maxSize);
				} else {
					fprintf(fw, "\tint *localAddr%d; /* thread local mem */\n", i + 1);
				}
			}
			isTlsFuncPars = true;
		}
	}

	if (hasGlobMemOps(activeTd)) {
		fprintf(fw, "\tint globMemTemp;\n");
		fprintf(fw, "\tint globMemIt;\n");
	}

	return isTlsFuncPars;
}

boolean getMaxMrapiCountsInTask(int *mtxCount, int *semCount, int *shmCount, int *barCount, int threadId, int taskId) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasExt = false;

	*mtxCount = 0;
	*semCount = 0;
	*shmCount = 0;
	*barCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mrapiExt.hasExt) {
				if (td->mrapiExt.numOfMutexes > *mtxCount) {
					*mtxCount = td->mrapiExt.numOfMutexes;
				}
				if (td->mrapiExt.numOfSemaphores > *semCount) {
					*semCount = td->mrapiExt.numOfSemaphores;
				}
				if (td->mrapiExt.numOfSharedMems > *shmCount) {
					*shmCount = td->mrapiExt.numOfSharedMems;
				}
				if (td->mrapiExt.numOfBarriers > *barCount) {
					*barCount = td->mrapiExt.numOfBarriers;
				}
				hasExt = true;
			}
		}
	}

	return hasExt;
}

boolean genMrapiPars(FILE *fw, struct tThreadData *activeTd) {

	int i;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	boolean hasExt;
	int mtxCount, semCount, shmCount, barCount;

	hasExt = getMaxMrapiCountsInTask(&mtxCount, &semCount, &shmCount, &barCount, activeTd->id, taskId);

	if (hasExt) {
		fprintf(fw, "%s", mrapiFuncPars);
		if (mtxCount > 0) {
			fprintf(fw, "\tmrapi_key_t lockKey; /* mrapi mutex lock key */\n");
			for (i = 0; i < mtxCount; i++) {
				if (isVariablesInSingleLine) {
					if (i != 0) { /* middle */
						fprintf(fw, ", mutex%d", i + 1);
					} else { /* first */
						fprintf(fw, "\tmrapi_mutex_hndl_t mutex%d", i + 1);
					}
					if (i == mtxCount - 1) { /* last */
						fprintf(fw, "; /* mrapi mutex handle */\n");
					}
				} else {
					fprintf(fw, "\tmrapi_mutex_hndl_t mutex%d; /* mrapi mutex handle */\n", i + 1);
				}
			}
		}
		for (i = 0; i < semCount; i++) {
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					fprintf(fw, ", sem%d", i + 1);
				} else { /* first */
					fprintf(fw, "\tmrapi_sem_hndl_t sem%d", i + 1);
				}
				if (i == semCount - 1) { /* last */
					fprintf(fw, "; /* mrapi sem handle */\n");
				}
			} else {
				fprintf(fw, "\tmrapi_sem_hndl_t sem%d; /* mrapi sem handle */\n", i + 1);
			}
		}
		if (shmCount > 0) {
			fprintf(fw, "\tchar shmMemTemp; /* temp read/write data */\n");
			fprintf(fw, "\tint shmMemIt; /* temp read/write data */\n");
			for (i = 0; i < shmCount; i++) {
				if (isVariablesInSingleLine) {
					if (i != 0) { /* middle */
						fprintf(fw, ", shmem%d", i + 1);
					} else { /* first */
						fprintf(fw, "\tmrapi_shmem_hndl_t shmem%d", i + 1);
					}
					if (i == shmCount - 1) { /* last */
						fprintf(fw, "; /* mrapi shmem handle */\n");
					}
				} else {
					fprintf(fw, "\tmrapi_shmem_hndl_t shmem%d; /* mrapi shmem handle */\n", i + 1);
				}
			}
			for (i = 0; i < shmCount; i++) {
				if (isVariablesInSingleLine) {
					if (i != 0) { /* middle */
						fprintf(fw, ", *shmAddr%d", i + 1);
					} else { /* first */
						fprintf(fw, "\tchar *shmAddr%d", i + 1);
					}
					if (i == shmCount - 1) { /* last */
						fprintf(fw, "; /* shared mem */\n");
					}
				} else {
					fprintf(fw, "\tchar *shmAddr%d; /* shared mem */\n", i + 1);
				}
			}
		}
	}

	return hasExt;
}

boolean getMaxMcapiCountsInTask(int *epCount, int *pktChanCount, int *sclChanCount, int threadId, int taskId) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasExt = false;

	*epCount = 0;
	*pktChanCount = 0;
	*sclChanCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mcapiExt.hasExt) {
				if (td->mcapiExt.numOfEndpoints > *epCount) {
					*epCount = td->mcapiExt.numOfEndpoints;
				}
				if (td->mcapiExt.numOfPcktChans > *pktChanCount) {
					*pktChanCount = td->mcapiExt.numOfPcktChans;
				}
				if (td->mcapiExt.numOfScalChans > *sclChanCount) {
					*sclChanCount = td->mcapiExt.numOfScalChans;
				}
				hasExt = true;
			}
		}
	}

	return hasExt;
}

void getScalarTypeCountsInTask(int *scl8Count, int *scl16Count, int *scl32Count, int *scl64Count, int taskId) {

	int i, j, numOfThreads;
	struct tThreadData *td;

	*scl8Count = 0;
	*scl16Count = 0;
	*scl32Count = 0;
	*scl64Count = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mcapiExt.hasExt) {
				for (j = 0; j < td->mcapiExt.numOfScalChans; j++) {
					if (td->mcapiExt.scalChans[j]->size == 8) {
						(*scl8Count)++;
					}
					if (td->mcapiExt.scalChans[j]->size == 16) {
						(*scl16Count)++;
					}
					if (td->mcapiExt.scalChans[j]->size == 32) {
						(*scl32Count)++;
					}
					if (td->mcapiExt.scalChans[j]->size == 64) {
						(*scl64Count)++;
					}
				}
			}
		}
	}

	return;
}

boolean genMcapiPars(FILE *fw, struct tThreadData *activeTd) {

	int i, t, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean hasExt;
	struct tMcapiChannel *pMcapiChan;
	int epCount, msgChanCount, pktChanCount, sclChanCount;
	int scl8Count, scl16Count, scl32Count, scl64Count;

	hasExt = getMaxMcapiCountsInTask(&epCount, &pktChanCount, &sclChanCount, activeTd->id, taskId);
	msgChanCount = epCount - (2 * (pktChanCount + sclChanCount)); /* there exists msg exchange endpoints */

	if (hasExt) {
		fprintf(fw, "%s", mcapiFuncPars);
		if (msgChanCount > 0) {
			fprintf(fw, "\tchar *msg = NULL;\n");
		}
		if (msgChanCount > 0 || pktChanCount > 0) {
			fprintf(fw, "\tchar buffer[BUFF_SIZE];\n");
		}
		if (sclChanCount > 0) {
			getScalarTypeCountsInTask(&scl8Count, &scl16Count, &scl32Count, &scl64Count, taskId);
			if (scl8Count > 0) {
				fprintf(fw, "\tunsigned char data8;\n");
			}
			if (scl16Count > 0) {
				fprintf(fw, "\tunsigned short data16;\n");
			}
			if (scl32Count > 0) {
				fprintf(fw, "\tunsigned int data32;\n");
			}
			if (scl64Count > 0) {
				fprintf(fw, "\tunsigned long long data64;\n");
			}
		}

		fprintf(fw, "\tint i;\n");

		if (pktChanCount > 0 || sclChanCount > 0) {
			fprintf(fw, "\tsize_t size;\n");
			fprintf(fw, "\tmcapi_request_t mcapiRequest;\n");
		} else if (epCount > 2 * (pktChanCount + sclChanCount)) {
			fprintf(fw, "\tsize_t size;\n");
		}
		for (i = 0; i < epCount; i++) {
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					fprintf(fw, ", ep%d", i + 1);
				} else { /* first */
					fprintf(fw, "\tmcapi_endpoint_t ep%d", i + 1);
				}
				if (i == epCount - 1) { /* last */
					fprintf(fw, "; /* mcapi endpoint */\n");
				}
			} else {
				fprintf(fw, "\tmcapi_endpoint_t ep%d; /* mcapi endpoint */\n", i + 1);
			}
		}
	}

	numOfThreads = activeProc->numOfThreads;
	for (t = activeTd->id; t <= numOfThreads; t++) {
		td = activeProc->threads[t];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mcapiExt.hasExt) {
				for (i = 0; i < td->mcapiExt.numOfPcktChans; i++) {
					pMcapiChan = td->mcapiExt.pcktChans[i];
					if (td == pMcapiChan->pSenderTd) {
						fprintf(fw, "\tmcapi_pktchan_send_hndl_t pktSendHandle%d%d; /* psh = ep(%d)%d->ep(%d)%d */\n",
								td->id, i + 1, pMcapiChan->pSenderEp->nodeId, pMcapiChan->pSenderEp->portId,
								pMcapiChan->pReceiverEp->nodeId, pMcapiChan->pReceiverEp->portId);
					} else {
						fprintf(fw, "\tmcapi_pktchan_recv_hndl_t pktRecvHandle%d%d; /* prh = ep(%d)%d->ep(%d)%d */\n",
								td->id, i + 1, pMcapiChan->pSenderEp->nodeId, pMcapiChan->pSenderEp->portId,
								pMcapiChan->pReceiverEp->nodeId, pMcapiChan->pReceiverEp->portId);
					}
				}
				for (i = 0; i < td->mcapiExt.numOfScalChans; i++) {
					pMcapiChan = td->mcapiExt.scalChans[i];
					if (td == pMcapiChan->pSenderTd) {
						fprintf(fw, "\tmcapi_sclchan_send_hndl_t sclSendHandle%d%d; /* ssh = ep(%d)%d->ep(%d)%d */\n",
								td->id, i + 1, pMcapiChan->pSenderEp->nodeId, pMcapiChan->pSenderEp->portId,
								pMcapiChan->pReceiverEp->nodeId, pMcapiChan->pReceiverEp->portId);
					} else {
						fprintf(fw, "\tmcapi_sclchan_recv_hndl_t sclRecvHandle%d%d; /* srh = ep(%d)%d->ep(%d)%d */\n",
								td->id, i + 1, pMcapiChan->pSenderEp->nodeId, pMcapiChan->pSenderEp->portId,
								pMcapiChan->pReceiverEp->nodeId, pMcapiChan->pReceiverEp->portId);
					}
				}
			}
		}
	}

	return hasExt;
}

boolean getMaxPosixCountsInTask(int *semCount, int *mutexCount, int threadId, int taskId) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasExt = false;

	*semCount = 0;
	*mutexCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (hasThreadPosixExt) {
				if (!td->mrapiExt.hasExt && !td->mcapiExt.hasExt) {
					if (td->posixExt.numOfSemaphores > *semCount) {
						*semCount = td->posixExt.numOfSemaphores;
					}
					if (td->posixExt.numOfMutexes > *mutexCount) {
						*mutexCount = td->posixExt.numOfMutexes;
					}
					hasExt = true;
				}
			}
		}
	}

	return hasExt;
}

boolean genPosixPars(FILE *fw, struct tThreadData *activeTd) {

	int i, semCount, mutexCount;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	boolean hasExt = false;

	hasExt = getMaxPosixCountsInTask(&semCount, &mutexCount, activeTd->id, taskId);

	if (hasExt) {
		fprintf(fw, "%s", posixFuncPars);
		for (i = 0; i < semCount; i++) {
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					fprintf(fw, ", *sem%d", i + 1);
				} else { /* first */
					fprintf(fw, "\tsem_t *sem%d", i + 1);
				}
				if (i == semCount - 1) { /* last */
					fprintf(fw, "; /* posix sem */\n");
				}
			} else {
				fprintf(fw, "\tsem_t *sem%d; /* posix sem */\n", i + 1);
			}
		}
		for (i = 0; i < mutexCount; i++) {
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					fprintf(fw, ", mutex%d", i + 1);
				} else { /* first */
					fprintf(fw, "\tpthread_mutex_t mutex%d", i + 1);
				}
				if (i == semCount - 1) { /* last */
					fprintf(fw, "; /* posix mutex */\n");
				}
			} else {
				fprintf(fw, "\tpthread_mutex_t mutex%d; /* posix mutex */\n", i + 1);
			}
		}
	}

	return hasExt;
}

void genMxapiPars(FILE *fw, struct tThreadData *activeTd) {

	genMtapiPars(fw, activeTd);
	genTlsPars(fw, activeTd);
	genMrapiPars(fw, activeTd);
	genMcapiPars(fw, activeTd);
	genPosixPars(fw, activeTd);

	fprintf(fw, "\n");

	return;
}

/***************************************************initialization******************************************************/

void initMxapi(FILE *fw, struct tThreadData *activeTd) {

	int t, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean hasMrapiExt = false, hasMcapiExt = false;

	numOfThreads = activeProc->numOfThreads;
	for (t = activeTd->id; t <= numOfThreads; t++) {
		td = activeProc->threads[t];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mrapiExt.hasExt) {
				hasMrapiExt = true;
			}
			if (td->mcapiExt.hasExt) {
				hasMcapiExt = true;
			}
			if (hasMrapiExt && hasMrapiExt) {
				break;
			}
		}
	}

	if (hasMrapiExt) {
		fprintf(fw, "\tmrapi_initialize(MRAPI_DOMAIN, td->tid, mrapiParms, &mrapiVersion, &mrapiStatus);\n");
		fprintf(fw, "\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n");
		fprintf(fw, "\t\n");
	}

	if (hasMcapiExt) {
		fprintf(fw, "\tmcapi_initialize(MCAPI_DOMAIN, td->tid, NULL, &mcapiParms, &mcapiVersion, &mcapiStatus);\n");
		fprintf(fw, "\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n");
		fprintf(fw, "\t\n");
	}

	return;
}

/***************************************************creation******************************************************/

int createTls(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (!isTlsVarArray) {
		if (activeTd->lsExt.hasExt) {
			for (i = 0; i < activeTd->lsExt.numOfLocalMems; i++) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* (create the local memory */\n", tab);
				fprintf(fw, "%s\tlocalAddr%d = (int *) malloc(sizeof(int) * %d);\n", tab, i + 1, activeTd->lsExt.localMems[i].size);
				fprintf(fw, "%s\tif (localAddr%d == NULL) { status = \"ERR_NO_MEMORY\"; WRONG }\n", tab, i + 1);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int createMrapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (activeTd->mrapiExt.hasExt) {
		/* create semaphores */
		for (i = 0; i < activeTd->mrapiExt.numOfSemaphores; i++) {
			if (activeTd->mrapiExt.sems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* create the semaphore */\n", tab);
				fprintf(fw, "%s\tsem%d = mrapi_sem_create(0x%x, NULL /*attributes*/, LOCK_LIMIT, &mrapiStatus);\n",
						tab, i + 1, activeTd->mrapiExt.sems[i].key);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
			if (activeTd->mrapiExt.sems[i].isLockAfterCreate) {
				fprintf(fw, "%s\t\n", tab);
				semLock(fw, activeTd, i, tab, false);
			}
		}
		/* create mutexes */
		for (i = 0; i < activeTd->mrapiExt.numOfMutexes; i++) {
			if (activeTd->mrapiExt.mutexes[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* create the mutex */\n", tab);
				fprintf(fw, "%s\tmutex%d = mrapi_mutex_create(0x%x, NULL /*attributes*/, &mrapiStatus);\n",
						tab, i + 1, activeTd->mrapiExt.mutexes[i].key);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		/* create shared memory */
		for (i = 0; i < activeTd->mrapiExt.numOfSharedMems; i++) {
			if (activeTd->mrapiExt.sharedMems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* (create the shared memory and) attach to it */\n", tab);
				fprintf(fw, "%s\tshmem%d = mrapi_shmem_create(0x%x, %d /*size*/, NULL /* nodes list */, 0 /* nodes list size */, NULL /*attrs*/,"
						" 0 /*attrs size*/, &mrapiStatus);\n", tab, i + 1, activeTd->mrapiExt.sharedMems[i].key, activeTd->mrapiExt.sharedMems[i].size);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				fprintf(fw, "%s\t\n", tab);
				fprintf(fw, "%s\tshmAddr%d = mrapi_shmem_attach(shmem%d, &mrapiStatus);\n",
						tab, i + 1, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int getMrapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (activeTd->mrapiExt.hasExt) {
		/* get semaphores */
		for (i = 0; i < activeTd->mrapiExt.numOfSemaphores; i++) {
			if (!activeTd->mrapiExt.sems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* get the semaphore */\n", tab);
				fprintf(fw, "%s\tdo {\n", tab);
				fprintf(fw, "%s\t\tsem%d = mrapi_sem_get(0x%x, &mrapiStatus);\n",
						tab, i + 1, activeTd->mrapiExt.sems[i].key);
				fprintf(fw, "%s\t\tif (mrapiStatus == MRAPI_ERR_SEM_ID_INVALID) sched_yield();\n", tab);
				fprintf(fw, "%s\t} while (mrapiStatus == MRAPI_ERR_SEM_ID_INVALID);\n", tab);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		/* get mutexes */
		for (i = 0; i < activeTd->mrapiExt.numOfMutexes; i++) {
			if (!activeTd->mrapiExt.mutexes[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* get the mutex */\n", tab);
				fprintf(fw, "%s\tdo {\n", tab);
				fprintf(fw, "%s\t\tmutex%d = mrapi_mutex_get(0x%x, &mrapiStatus);\n",
						tab, i + 1, activeTd->mrapiExt.mutexes[i].key);
				fprintf(fw, "%s\t\tif (mrapiStatus == MRAPI_ERR_MUTEX_ID_INVALID) sched_yield();\n", tab);
				fprintf(fw, "%s\t} while (mrapiStatus == MRAPI_ERR_MUTEX_ID_INVALID);\n", tab);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		/* get shared memory */
		for (i = 0; i < activeTd->mrapiExt.numOfSharedMems; i++) {
			if (!activeTd->mrapiExt.sharedMems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* (get the shared memory and) attach to it */\n", tab);
				fprintf(fw, "%s\tdo {\n", tab);
				fprintf(fw, "%s\t\tshmem%d = mrapi_shmem_get(0x%x, &mrapiStatus);\n", tab, i + 1, activeTd->mrapiExt.sharedMems[i].key);
				fprintf(fw, "%s\t\tif (mrapiStatus == MRAPI_ERR_SHM_ID_INVALID) sched_yield();\n", tab);
				fprintf(fw, "%s\t} while (mrapiStatus == MRAPI_ERR_SHM_ID_INVALID);\n", tab);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				fprintf(fw, "%s\t\n", tab);
				fprintf(fw, "%s\tshmAddr%d = mrapi_shmem_attach(shmem%d, &mrapiStatus);\n",
						tab, i + 1, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

void openChan(FILE *fw, struct tThreadData *td, struct tMcapiChannel *pMcapiChan, int i, char *tab) {

	fprintf(fw, "%s\t\n", tab);
	fprintf(fw, "%s\t/* connect the channel */\n", tab);
	fprintf(fw, "%s\tdo {\n", tab);
	if (pMcapiChan->type == EP_PACKET) {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\tmcapi_pktchan_connect_i(ep%d, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, pMcapiChan->pSenderEp->portId, pMcapiChan->pSenderGetEp->printPortId);
		} else {
			fprintf(fw, "%s\t\tmcapi_pktchan_connect_i(ep%d, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, pMcapiChan->pReceiverGetEp->printPortId, pMcapiChan->pReceiverEp->portId);
		}
	} else {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_sclchan_connect_i(ep%d, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, pMcapiChan->pSenderEp->portId, pMcapiChan->pSenderGetEp->printPortId);
		} else {
			fprintf(fw, "%s\t\tmcapi_sclchan_connect_i(ep%d, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, pMcapiChan->pReceiverGetEp->printPortId, pMcapiChan->pReceiverEp->portId);
		}
	}
	fprintf(fw, "%s\t//retry if all request handles are in-use\n", tab);
	fprintf(fw, "%s\t} while (mcapiStatus == MCAPI_ERR_REQUEST_LIMIT);\n", tab);
	fprintf(fw, "%s\tmcapi_wait(&mcapiRequest, &size, MCA_INFINITE, &mcapiStatus);\n", tab);
	if (td == pMcapiChan->pSenderTd) {
		//fprintf(fw, "%s\tif (mcapiStatus != MCAPI_ERR_CHAN_CONNECTED && mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t/* If the status is MCAPI_ERR_ENDP_INVALID, we assume that there is problem with deletion */\n", tab);
				fprintf(fw, "%s\tif ((mcapiStatus != MCAPI_ERR_CHAN_CONNECTED && mcapiStatus != MCAPI_SUCCESS) && mcapiStatus != MCAPI_ERR_ENDP_INVALID) { MCAPI_WRONG }\n", tab);
	} else {
		fprintf(fw, "%s\t/* If the status is MCAPI_ERR_ENDP_INVALID, we assume that sender is done and sender endpoint is deleted */\n", tab);
		fprintf(fw, "%s\tif ((mcapiStatus != MCAPI_ERR_CHAN_CONNECTED && mcapiStatus != MCAPI_SUCCESS) && mcapiStatus != MCAPI_ERR_ENDP_INVALID) { MCAPI_WRONG }\n", tab);
	}

	fprintf(fw, "%s\t\n", tab);
	fprintf(fw, "%s\t/* open the endpoint handles */\n", tab);
	fprintf(fw, "%s\tdo {\n", tab);
	if (pMcapiChan->type == EP_PACKET) {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_pktchan_send_open_i(&pktSendHandle%d%d /*send_handle*/, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1, pMcapiChan->pSenderEp->portId);
		} else {
			fprintf(fw, "%s\t\tmcapi_pktchan_recv_open_i(&pktRecvHandle%d%d /*recv_handle*/, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1, pMcapiChan->pReceiverEp->portId);
		}
	} else {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_sclchan_send_open_i(&sclSendHandle%d%d /*send_handle*/, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1, pMcapiChan->pSenderEp->portId);
		} else {
			fprintf(fw, "%s\t\tmcapi_sclchan_recv_open_i(&sclRecvHandle%d%d /*recv_handle*/, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1, pMcapiChan->pReceiverEp->portId);
		}
	}
	fprintf(fw, "%s\t//retry if all request handles are in-use\n", tab);
	fprintf(fw, "%s\t} while (mcapiStatus == MCAPI_ERR_REQUEST_LIMIT);\n", tab);
	fprintf(fw, "%s\tmcapi_wait(&mcapiRequest, &size, MCA_INFINITE, &mcapiStatus);\n", tab);
	fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);

	return;
}

int createMcapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex, int phase) {

	int i;
	struct tMcapiEndpoint *mcapiEndpoint;

	if (activeTd->mcapiExt.hasExt) {
		/* endpoint create */
		for (i = 0; i < activeTd->mcapiExt.numOfEndpoints; i++) {
			mcapiEndpoint = &activeTd->mcapiExt.endpoints[i];
			if (mcapiEndpoint->isOwner && mcapiEndpoint->phase == phase) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* create an endpoint */\n", tab);
				fprintf(fw, "%s\tep%d = mcapi_endpoint_create(%d, &mcapiStatus);\n",
						tab, i + 1, mcapiEndpoint->portId);
				fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int getMcapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex, int phase) {

	int i;
	struct tMcapiEndpoint *mcapiEndpoint;
	struct tMcapiChannel *pMcapiChan;

	if (activeTd->mcapiExt.hasExt) {
		/* endpoint get */
		for (i = 0; i < activeTd->mcapiExt.numOfEndpoints; i++) {
			mcapiEndpoint = &activeTd->mcapiExt.endpoints[i];
			if (!mcapiEndpoint->isOwner && mcapiEndpoint->phase == phase) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				if (mcapiEndpoint->type == EP_MESSAGE || mcapiEndpoint->isSender) {
					fprintf(fw, "%s\t/* get the endpoint */\n", tab);
					fprintf(fw, "%s\tep%d = mcapi_endpoint_get(MCAPI_DOMAIN, %d, %d, MCA_INFINITE, &mcapiStatus);\n",
							tab, i + 1, mcapiEndpoint->nodeId, mcapiEndpoint->portId);
					fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
				} else {
					fprintf(fw, "%s\t/* get the endpoint if exists*/\n", tab);
					fprintf(fw, "%s\tep%d = MCAPI_ENDPOINT_INVALID;\n", tab, i + 1);
					fprintf(fw, "%s\tmcapi_endpoint_get_i(MCAPI_DOMAIN, %d, %d, &ep%d, &mcapiRequest, &mcapiStatus);\n",
							tab, mcapiEndpoint->nodeId, mcapiEndpoint->portId, i + 1);
					fprintf(fw, "%s\tmcapi_wait(&mcapiRequest, &size, MCAPI_TIMEOUT, &mcapiStatus);\n", tab);
				}
				currIndex++;
			}
		}
		for (i = 0; i < activeTd->mcapiExt.numOfPcktChans; i++) {
			pMcapiChan = activeTd->mcapiExt.pcktChans[i];
			openChan(fw, activeTd, pMcapiChan, i, tab);
		}
		for (i = 0; i < activeTd->mcapiExt.numOfScalChans; i++) {
			pMcapiChan = activeTd->mcapiExt.scalChans[i];
			openChan(fw, activeTd, pMcapiChan, i, tab);
		}
	}

	return currIndex;
}

int createPosix(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (!activeTd->mrapiExt.hasExt && !activeTd->mcapiExt.hasExt) {
		/* create semaphores */
		for (i = 0; i < activeTd->posixExt.numOfSemaphores; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			fprintf(fw, "%s\t/* create the semaphore */\n", tab);
			if (!activeTd->posixExt.sems[i].isLockAfterCreate) {
				fprintf(fw, "%s\tsem%d = sem_open(\"sem%x\", O_CREAT, 0644, 1);\n", tab, i + 1, activeTd->posixExt.sems[i].key);
			} else {
				fprintf(fw, "%s\tsem%d = sem_open(\"sem%x\", O_CREAT, 0644, 0);\n", tab, i + 1, activeTd->posixExt.sems[i].key);
			}
			fprintf(fw, "%s\tif (sem%d == SEM_FAILED) { POSIX_WRONG }\n", tab, i + 1);
			currIndex++;
//			if (activeTd->posixExt.sems[i].isLockAfterCreate) {
//				fprintf(fw, "%s\t\n", tab);
//				posixSemLock(fw, activeTd, i, tab, false);
//			}
		}

	}

	return currIndex;
}

int createMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex, int phase) {

	currIndex = createTls(fw, activeTd, tab, currIndex);
	currIndex = createMrapi(fw, activeTd, tab, currIndex);
	currIndex = createMcapi(fw, activeTd, tab, currIndex, phase);
	currIndex = createPosix(fw, activeTd, tab, currIndex);

	return currIndex;
}

int getMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex, int phase) {

	currIndex = getMrapi(fw, activeTd, tab, currIndex);
	currIndex = getMcapi(fw, activeTd, tab, currIndex, phase);

	return currIndex;
}

int createAndGetMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex, int phase) {

	currIndex = createTls(fw, activeTd, tab, currIndex);
	currIndex = createMrapi(fw, activeTd, tab, currIndex);
	currIndex = getMrapi(fw, activeTd, tab, currIndex);
	currIndex = createMcapi(fw, activeTd, tab, currIndex, phase);
	currIndex = getMcapi(fw, activeTd, tab, currIndex, phase);
	currIndex = createPosix(fw, activeTd, tab, currIndex);

	return currIndex;
}

/***************************************************operations******************************************************/

int globMemRead(FILE *fw, int index, int itStartIndex, int numOfIterations, char *tab) {

	if (index < activeProc->lsExt.numOfGlobMems) {
		if (numOfIterations <= activeProc->lsExt.globMems[index].size) {
			/* lock global mem */
			fprintf(fw, "%s\tfor (globMemIt = %d; globMemIt < %d; globMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\t\tglobMemTemp = globAddr%d[globMemIt]; /* read global mem */\n", tab, index + 1);
			fprintf(fw, "%s\t\t/* use globMemTemp */\n", tab);
			fprintf(fw, "%s\t}\n", tab);
		} else {
			printf("ERROR! Process %d, global mem (index = %d) size (%d) is smaller than %d!\n",
					activeProc->id, index, activeProc->lsExt.globMems[index].size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Process %d, no such global mem (index = %d)!\n", activeProc->id, index);
		return -1;
	}

	return 0;
}

int globMemWrite(FILE *fw, int index, int itStartIndex, int numOfIterations, char *tab) {

	if (index < activeProc->lsExt.numOfGlobMems) {
		if (numOfIterations <= activeProc->lsExt.globMems[index].size) {
			/* lock global mem */
			fprintf(fw, "%s\tfor (globMemIt = %d; globMemIt < %d; globMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\t\tglobMemTemp = globMemIt; /* assign globMemTemp */\n", tab);
			fprintf(fw, "%s\t\tglobAddr%d[globMemIt] = globMemTemp; /* write global mem */\n", tab, index + 1);
			fprintf(fw, "%s\t}\n", tab);
			/* unlock global mem */
		} else {
			printf("ERROR! Process %d, global mem (index = %d) size (%d) is smaller than %d!\n",
					activeProc->id, index, activeProc->lsExt.globMems[index].size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Process %d, no such global mem (index = %d)!\n", activeProc->id, index);
		return -1;
	}

	return 0;
}

int localMemRead(FILE *fw, struct tThreadData *td, int index, int itStartIndex, int numOfIterations, char *tab) {

	if (index < td->lsExt.numOfLocalMems) {
		if (itStartIndex + numOfIterations <= td->lsExt.localMems[index].size) {
			fprintf(fw, "%s\tfor (localMemIt = %d; localMemIt < %d; localMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\t\tlocalMemTemp = localAddr%d[localMemIt]; /* read local mem */\n", tab, index + 1);
			fprintf(fw, "%s\t\t/* use localMemTemp */\n", tab);
			fprintf(fw, "%s\t}\n", tab);
		} else {
			printf("ERROR! Thread %d, local mem (index = %d) size (%d) is smaller than %d!\n",
					td->id, index, td->lsExt.localMems[index].size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Thread %d, no such local mem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int localMemWrite(FILE *fw, struct tThreadData *td, int index, int itStartIndex, int numOfIterations, char *tab) {

	if (index < td->lsExt.numOfLocalMems) {
		if (itStartIndex + numOfIterations <= td->lsExt.localMems[index].size) {
			fprintf(fw, "%s\tfor (localMemIt = %d; localMemIt < %d; localMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\t\tlocalAddr%d[localMemIt] = localMemIt; /* write local mem */\n", tab, index + 1);
			fprintf(fw, "%s\t}\n", tab);
		} else {
			printf("ERROR! Thread %d, local mem (index = %d) size (%d) is smaller than %d!\n",
					td->id, index, td->lsExt.localMems[index].size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Thread %d, no such local mem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int mutexLock(FILE *fw, struct tThreadData *td, int index, char *tab, boolean hasNewLine) {

	if (index < td->mrapiExt.numOfMutexes) {
		fprintf(fw, "%s\tmrapi_mutex_lock(mutex%d, &lockKey, MRAPI_TIMEOUT_INFINITE /*timeout*/, &mrapiStatus);\n", tab, index + 1);
		fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such mutex (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int mutexUnlock(FILE *fw, struct tThreadData *td, int index, char *tab, boolean hasNewLine) {

	if (index < td->mrapiExt.numOfMutexes) {
		fprintf(fw, "%s\tmrapi_mutex_unlock(mutex%d, &lockKey, &mrapiStatus);\n", tab, index + 1);
		fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such mutex (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int semLock(FILE *fw, struct tThreadData *td, int index, char *tab,  boolean hasNewLine) {

	if (index < td->mrapiExt.numOfSemaphores) {
		fprintf(fw, "%s\tmrapi_sem_lock(sem%d, MRAPI_TIMEOUT_INFINITE /*timeout*/, &mrapiStatus);\n", tab, index + 1);
		fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such sem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int semUnlock(FILE *fw, struct tThreadData *td, int index, boolean isWaitTillLocked, boolean hasNewLine,  char *tab) {

	if (index < td->mrapiExt.numOfSemaphores) {
		fprintf(fw, "%s\tdo {\n", tab);
		fprintf(fw, "%s\t\tmrapi_sem_unlock(sem%d, &mrapiStatus);\n", tab, index + 1);
		fprintf(fw, "%s\t} while (mrapiStatus == MRAPI_ERR_SEM_NOTLOCKED);\n", tab);
		fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such sem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int barrierWait(FILE *fw, struct tThreadData *td, int index, char *tab) {

	struct tBarrier *pBarrier;

	if (index < td->mrapiExt.numOfBarriers) {
		pBarrier = &td->mrapiExt.barriers[index];
		fprintf(fw, "%s\tmrapi_barrier_wait(mutex%d, &barrier%d, %d /* num of threads */);\n",
				tab, pBarrier->pMutex->id, pBarrier->id, pBarrier->numOfThreads);
	} else {
		printf("ERROR! Thread %d, no such barrier (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int shmMemRead(FILE *fw, struct tThreadData *td, int index, int itStartIndex, int numOfIterations, char *tab) {

	struct tSharedMem *pShmMem;

	if (index < td->mrapiExt.numOfSharedMems) {
		pShmMem = &td->mrapiExt.sharedMems[index];
		if (itStartIndex + numOfIterations <= pShmMem->size) {
			mutexLock(fw, td, pShmMem->pMutex->id - 1, tab, true);
			fprintf(fw, "%s\tfor (shmMemIt = %d; shmMemIt < %d; shmMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\t\tshmMemTemp = shmAddr%d[shmMemIt]; /* read shared mem */\n", tab, index + 1);
			fprintf(fw, "%s\t\t/* use shmMemTemp */\n", tab);
			fprintf(fw, "%s\t}\n", tab);
			fprintf(fw, "%s\t\n", tab);
			mutexUnlock(fw, td, pShmMem->pMutex->id - 1, tab, false);
		} else {
			printf("ERROR! Thread %d, shared mem (index = %d) size (%d) is smaller than %d!\n",
					td->id, index, pShmMem->size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Thread %d, no such shared mem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int shmMemWrite(FILE *fw, struct tThreadData *td, int index, int itStartIndex, int numOfIterations, char *tab) {

	struct tSharedMem *pShmMem;

	if (index < td->mrapiExt.numOfSharedMems) {
		pShmMem = &td->mrapiExt.sharedMems[index];
		if (itStartIndex + numOfIterations <= pShmMem->size) {
			mutexLock(fw, td, pShmMem->pMutex->id - 1, tab, true);
			fprintf(fw, "%s\tfor (shmMemIt = %d; shmMemIt < %d; shmMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\t\tshmMemTemp = (char) shmMemIt; /* assign shmMemTemp */\n", tab);
			fprintf(fw, "%s\t\tshmAddr%d[shmMemIt] = shmMemTemp; /* write shared mem */\n", tab, index + 1);
			fprintf(fw, "%s\t}\n", tab);
			fprintf(fw, "%s\t\n", tab);
			mutexUnlock(fw, td, pShmMem->pMutex->id - 1, tab, false);
		} else {
			printf("ERROR! Thread %d, shared mem (index = %d) size (%d) is smaller than %d!\n",
					td->id, index, pShmMem->size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Thread %d, no such shared mem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int msgSend(FILE *fw, struct tThreadData *td, int sendIndex, int recvIndex, int count, char *tab) {

	if (sendIndex < td->mcapiExt.numOfEndpoints && recvIndex < td->mcapiExt.numOfEndpoints) {
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tmsg = \"mcapi_msg\";\n", tab);
		fprintf(fw, "%s\t\tsize = strlen(msg);\n", tab);
		fprintf(fw, "%s\t\tdo {\n", tab);
		fprintf(fw, "%s\t\t\tmcapi_msg_send(ep%d, ep%d, msg, size, 1 /*priority*/, &mcapiStatus);\n", tab, sendIndex + 1, recvIndex + 1);
		fprintf(fw, "%s\t\t} while (mcapiStatus == MCAPI_ERR_MEM_LIMIT);\n", tab);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such endpoint(s) (sendIndex = %d, recvIndex = %d)!\n", td->id, sendIndex, recvIndex);
		return -1;
	}

	return 0;
}

int msgRecv(FILE *fw, struct tThreadData *td, int recvIndex, int count, char *tab) {

	if (recvIndex < td->mcapiExt.numOfEndpoints) {
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tmcapi_msg_recv(ep%d, buffer, BUFF_SIZE, &size, &mcapiStatus);\n", tab, recvIndex + 1);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t\t/* use buffer */\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such endpoint (srecvIndex = %d)!\n", td->id, recvIndex);
		return -1;
	}

	return 0;
}

int pktSend(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	//struct tMcapiChannel *pChannel;

	if (index < td->mcapiExt.numOfPcktChans) {
		//pChannel = td->mcapiExt.pcktChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tsprintf(buffer, \"mcapi_packet_%s\", i);\n", tab, "%d");
		fprintf(fw, "%s\t\tsize = strlen(buffer);\n", tab);
		fprintf(fw, "%s\t\tdo {\n", tab);
		fprintf(fw, "%s\t\t\tmcapi_pktchan_send(pktSendHandle%d%d, buffer, size, &mcapiStatus);\n", tab, td->id, index + 1);
		fprintf(fw, "%s\t\t} while (mcapiStatus == MCAPI_ERR_MEM_LIMIT);\n", tab);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int pktRecv(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	//struct tMcapiChannel *pChannel;

	if (index < td->mcapiExt.numOfPcktChans) {
		//pChannel = td->mcapiExt.pcktChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tmcapi_pktchan_recv(pktRecvHandle%d%d, (void **) ((void*) &buffer), &size, &mcapiStatus);\n",
				tab, td->id, index + 1);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t\t/* use buffer */\n", tab);
		fprintf(fw, "%s\t\tmcapi_pktchan_release((void *) buffer, &mcapiStatus);\n", tab);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int sclSend(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	struct tMcapiChannel *pChannel;

	if (index < td->mcapiExt.numOfScalChans) {
		pChannel = td->mcapiExt.scalChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		if (pChannel->size == 8) {
			fprintf(fw, "%s\tdata8 = (unsigned char) i;\n", tab);
			fprintf(fw, "%s\t\tdo {\n", tab);
			fprintf(fw, "%s\t\t\tmcapi_sclchan_send_uint8(sclSendHandle%d%d, data8, &mcapiStatus);\n", tab, td->id, index + 1);
		} else if (pChannel->size == 16) {
			fprintf(fw, "%s\t\tdata16 = (unsigned short) i;\n", tab);
			fprintf(fw, "%s\t\tdo {\n", tab);
			fprintf(fw, "%s\t\t\tmcapi_sclchan_send_uint16(sclSendHandle%d%d, data16, &mcapiStatus);\n", tab, td->id, index + 1);
		} else if (pChannel->size == 32) {
			fprintf(fw, "%s\t\tdata32 = (unsigned int) i;\n", tab);
			fprintf(fw, "%s\t\tdo {\n", tab);
			fprintf(fw, "%s\t\t\tmcapi_sclchan_send_uint32(sclSendHandle%d%d, data32, &mcapiStatus);\n", tab, td->id, index + 1);
		} else {
			fprintf(fw, "%s\t\tdata64 = (unsigned long long) i;\n", tab);
			fprintf(fw, "%s\t\tdo {\n", tab);
			fprintf(fw, "%s\t\t\tmcapi_sclchan_send_uint64(sclSendHandle%d%d, data64, &mcapiStatus);\n", tab, td->id, index + 1);
		}
		fprintf(fw, "%s\t\t} while (mcapiStatus == MCAPI_ERR_MEM_LIMIT);\n", tab);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int sclRecv(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	struct tMcapiChannel *pChannel;

	if (index < td->mcapiExt.numOfScalChans) {
		pChannel = td->mcapiExt.scalChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		if (pChannel->size == 8) {
			fprintf(fw, "%s\t\tdata8 = mcapi_sclchan_recv_uint8(sclRecvHandle%d%d, &mcapiStatus);\n", tab, td->id, index + 1);
			fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
			fprintf(fw, "%s\t\t/* use data8 */\n", tab);
		} else if (pChannel->size == 16) {
			fprintf(fw, "%s\t\tdata16 = mcapi_sclchan_recv_uint16(sclRecvHandle%d%d, &mcapiStatus);\n", tab, td->id, index + 1);
			fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
			fprintf(fw, "%s\t\t/* use data16 */\n", tab);
		} else if (pChannel->size == 32) {
			fprintf(fw, "%s\t\tdata32 = mcapi_sclchan_recv_uint32(sclRecvHandle%d%d, &mcapiStatus);\n", tab, td->id, index + 1);
			fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
			fprintf(fw, "%s\t\t/* use data32 */\n", tab);
		} else {
			fprintf(fw, "%s\t\tdata64 = mcapi_sclchan_recv_uint64(sclRecvHandle%d%d, &mcapiStatus);\n", tab, td->id, index + 1);
			fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
			fprintf(fw, "%s\t\t/* use data64 */\n", tab);
		}
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int posixMutexLock(FILE *fw, struct tThreadData *td, int index, char *tab, boolean hasNewLine) {

	if (index < td->posixExt.numOfMutexes) {
		fprintf(fw, "%s\tretVal = pthread_mutex_lock(mutex%d);\n", tab, index + 1);
		fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such posix mutex (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int posixMutexUnlock(FILE *fw, struct tThreadData *td, int index, char *tab, boolean hasNewLine) {

	if (index < td->posixExt.numOfMutexes) {
		fprintf(fw, "%s\tpthread_mutex_unlock(mutex%d);\n", tab, index + 1);
		fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such posix mutex (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int posixSemLock(FILE *fw, struct tThreadData *td, int index, char *tab,  boolean hasNewLine) {

	if (index < td->posixExt.numOfSemaphores) {
		fprintf(fw, "%s\tretVal = sem_wait(sem%d);\n", tab, index + 1);
		fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such posix sem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int posixSemUnlock(FILE *fw, struct tThreadData *td, int index, boolean isWaitTillLocked, boolean hasNewLine,  char *tab) {

	if (index < td->posixExt.numOfSemaphores) {
		fprintf(fw, "%s\tretVal = sem_post(sem%d);\n", tab, index + 1);
		fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such posix sem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

//FIXME: do computation in CMR and BMR code blocks according to IPC values of original and synthetic

int doMtapiOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j, k, count1, count2;
	struct tWorkOp *pWorkOp;
	workOpType type;
	int tid = td->id;

	for (i = 0; i < td->mtapiExt.numOfWorks; i++) {
		pWorkOp = &td->mtapiExt.workOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pWorkOp->time == j) {
				if (pWorkOp->numOfIterations > 0) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					type = pWorkOp->type;
					fprintf(fw, "%s\t/*\n", tab);
					fprintf(fw, "%s\t * ==========================================================\n", tab);
					fprintf(fw, "%s\t * ====================== DO WORK-%d =========================\n", tab, i + 1);
					fprintf(fw, "%s\t * Description: %s\n", tab, getOperationTypeText(type));
					fprintf(fw, "%s\t * ==========================================================\n", tab);
					fprintf(fw, "%s\t */\n", tab);

					// variable decleration
					if (type == WORK_OP_IPC_INC) {
						if (codeLevel == 1) {
							fprintf(fw, "%s\tint i1_%d = 288, i2_%d = 3, ires_%d = 0;\n", tab, tid, tid, tid);
						}
					} else if (type == WORK_OP_IPC_DEC) {
						if (codeLevel == 1) {
							fprintf(fw, "%s\tdouble d1_%d = 288, d2_%d = 3, dres_%d = 0;\n", tab, tid, tid, tid);
						}
					} else if (type == WORK_OP_CACHE_HIT) {
						fprintf(fw, "%s\tint i_%d, j_%d;\n", tab, tid, tid);
					} else if (type == WORK_OP_CACHE_MISS) {
						fprintf(fw, "%s\tregister int wi_%d;\n", tab, tid);
						fprintf(fw, "%s\tint *wa_%d, wsize_%d = (1024 * 1024) * 4;\n", tab, tid, tid);
						fprintf(fw, "%s\twa_%d = (int *) malloc(wsize_%d * sizeof(int));\n", tab, tid, tid);
					} else if (type == WORK_OP_BRNCH_HIT) {
						if (codeLevel == 1) {
							fprintf(fw, "%s\tint b1_%d = 288, b2_%d = 3, bres_%d = 0;\n", tab, tid, tid, tid);
						}
					} else if (type == WORK_OP_BRNCH_MISS) {
						if (codeLevel == 1) {
							fprintf(fw, "%s\tint b1_%d = 288, b2_%d = 3, bres_%d = 0;\n", tab, tid, tid, tid);
						}
						fprintf(fw, "%s\tunsigned int randNum_%d, r3_%d, r4_%d, r8_%d;\n", tab, tid, tid, tid, tid);
					} else if (type == WORK_OP_COMM_INC) {
						fprintf(fw, "%s\tint c1_%d = 288, c2_%d = 3, cres_%d = 0;\n", tab, tid, tid, tid);
					} else if (type == WORK_OP_COMP_INC) {
						fprintf(fw, "%s\tint c1_%d = 288, c2_%d = 3, cres_%d = 0;\n", tab, tid, tid, tid);
					}

					// code blocks
					fprintf(fw, "%s\tfor (workCount = 0; workCount < %u; workCount++) {\n", tab, pWorkOp->numOfIterations);
					fprintf(fw, "%s\t\t/* do something */\n", tab);

					if (type == WORK_OP_IPC_INC) {
						if (codeLevel == 1) {
							count1 = 40;
							count2 = 0;
						} else {
							count1 = 0; //(int) ((double) (pWorkOp->type1Percent * 40) / 100);
							count2 = 40; //40 - count1;
						}
						for (k = 0; k < count1; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\tires_%d = i1_%d + i2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t__asm__ (\"add $0x1,%seax\");\n", tab, "%");
							}
						}
						for (k = 0; k < count2; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\tires_%d = i1_%d;\n", tab, tid, tid);
							} else {
								fprintf(fw, "%s\t\t__asm__ (\"mov $0x1,%seax\");\n", tab, "%");
							}
						}
					} else if (type == WORK_OP_IPC_DEC) {
						if (codeLevel == 0) {
							fprintf(fw, "%s\t\t__asm__ (\"movl $0x5,%seax\");\n", tab, "%");
							fprintf(fw, "%s\t\t__asm__ (\"movl %seax,%sedx\");\n", tab, "%", "%");
							fprintf(fw, "%s\t\t__asm__ (\"sar $0x1f,%sedx\");\n", tab, "%");
						}
						for (k = 0; k < 50; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\tdres_%d = d1_%d / d2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t__asm__ (\"idivl %seax\");\n", tab, "%");
							}
						}
					} else if (type == WORK_OP_CACHE_HIT) {
						fprintf(fw, "%s\t\tfor (i_%d = 0; i_%d < 256; i_%d += 1) {\n", tab, tid, tid, tid);
						fprintf(fw, "%s\t\t\tfor (j_%d = 0; j_%d < 256; j_%d += 1) {\n", tab, tid, tid, tid);
						fprintf(fw, "%s\t\t\t\twa[i_%d][j_%d] = 2 * wa[i_%d][j_%d];\n", tab, tid, tid, tid, tid);
						fprintf(fw, "%s\t\t\t}\n", tab);
						fprintf(fw, "%s\t\t}\n", tab);
					} else if (type == WORK_OP_CACHE_MISS) {
						fprintf(fw, "%s\t\twa_%d[rand() %s wsize_%d] = 0;\n", tab, tid, "%", tid);
					} else if (type == WORK_OP_BRNCH_HIT) {
						fprintf(fw, "%s\t\tif (workCount >= 0) {\n", tab);
						for (k = 0; k < 2; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\t\t\tbres_%d = b1_%d + b2_%d + (b1_%d / b2_%d);\n", tab, tid, tid, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t\t\t__asm__ (\"idivl %seax\");\n", tab, "%");
							}
						}
						fprintf(fw, "%s\t\t}\n", tab);
					} else if (type == WORK_OP_BRNCH_MISS) {
						fprintf(fw, "%s\t\trandNum_%d = rand();\n", tab, tid);
						fprintf(fw, "%s\t\tr3_%d = randNum_%d %s 3;\n", tab, tid, tid, "%");
						for (k = 0; k < 2; k++) {
							fprintf(fw, "%s\t\tif (r3_%d == %d) {\n", tab, tid, k);
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\t\tbres_%d = b1_%d + b2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
							}
							fprintf(fw, "%s\t\t}\n", tab);
						}
						fprintf(fw, "%s\t\tr4_%d = randNum_%d %s 4;\n", tab, tid, tid, "%");
						for (k = 0; k < 2; k++) {
							fprintf(fw, "%s\t\tif (r4_%d == %d) {\n", tab, tid, k);
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\t\tbres_%d = b1_%d + b2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
							}
							fprintf(fw, "%s\t\t}\n", tab);
						}
						fprintf(fw, "%s\t\tr8_%d = randNum_%d %s 8;\n", tab, tid, tid, "%");
						for (k = 0; k < 4; k++) {
							fprintf(fw, "%s\t\tif (r8_%d == %d) {\n", tab, tid, k);
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\t\tbres_%d = b1_%d + b2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
							}
							fprintf(fw, "%s\t\t}\n", tab);
						}
					} else if (type == WORK_OP_COMP_INC) {
						if (codeLevel == 0) {
							fprintf(fw, "%s\t\t__asm__ (\"movl $0x5,%seax\");\n", tab, "%");
							fprintf(fw, "%s\t\t__asm__ (\"movl %seax,%sedx\");\n", tab, "%", "%");
							fprintf(fw, "%s\t\t__asm__ (\"sar $0x1f,%sedx\");\n", tab, "%");
						}
						for (k = 0; k < 2; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\tcres_%d = ((2 * c1_%d) + (c2_%d / 2)) / ((3 * c1_%d) + (c2_%d / 3));\n",
										tab, tid, tid, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t__asm__ (\"idivl %seax\");\n", tab, "%");
							}
						}
					} else if (type == WORK_OP_COMM_INC) {
						for (k = 0; k < 5; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\tc1_%d = c2_%d;\n", tab, tid, tid);
								fprintf(fw, "%s\t\tcres_%d = c1_%d;\n", tab, tid, tid);
								fprintf(fw, "%s\t\tc2_%d = c1_%d;\n", tab, tid, tid);
								fprintf(fw, "%s\t\tcres_%d = c2_%d;\n", tab, tid, tid);
							} else {
								fprintf(fw, "%s\t\t__asm__ (\"mov -0x10(%srbp),%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t__asm__ (\"mov -0x14(%srbp),%sedx\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t__asm__ (\"mov %seax,-0x18(%srbp)\");\n", tab, "%", "%");
							}
						}
					}

					fprintf(fw, "%s\t}\n", tab);
					if (type == WORK_OP_CACHE_MISS) {
						fprintf(fw, "%s\tfree(wa_%d);\n", tab, tid);
					}

					currIndex++;
				}
			}
		}
	}

	return currIndex;
}

int doTlsOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j;
	struct tMemOp *pMemOp;

	for (i = 0; i < td->lsExt.numOfMemOps; i++) {
		pMemOp = &td->lsExt.memOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pMemOp->time == j) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}

				if (pMemOp->type == MEM_TYPE_LOCAL) {
					if (pMemOp->opType == MEM_OP_READ) {
						localMemRead(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					} else if (pMemOp->opType == MEM_OP_WRITE) {
						localMemWrite(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					}
				} else if (pMemOp->type == MEM_TYPE_GLOBAL) {
					if (pMemOp->opType == MEM_OP_READ) {
						if (pMemOp->semIndex != -1) { /* lock the semaphore */
							posixSemLock(fw, td, pMemOp->semIndex, tab, true);
#ifdef PRINT_CTRL_FLOW_IN_SYN
							fprintf(fw, "\t\t\tprintf(\"Thread %d, lock sem %d\\n\");\n", td->id, pMemOp->semIndex);
#endif
						}
						globMemRead(fw, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					} else if (pMemOp->opType == MEM_OP_WRITE) {
						globMemWrite(fw, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
						if (pMemOp->semIndex != -1) { /* unlock the semaphore */
							fprintf(fw, "%s\t\n", tab);
							posixSemUnlock(fw, td, pMemOp->semIndex, true, false, tab);
#ifdef PRINT_CTRL_FLOW_IN_SYN
							fprintf(fw, "\t\t\tprintf(\"Thread %d, unlock sem %d\\n\");\n", td->id, pMemOp->semIndex);
#endif
						}
					}
				}
				currIndex++;
			}
		}
	}

	return currIndex;
}

int doMrapiOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j;
	struct tMemOp *pMemOp;
	struct tSyncOp *pSyncOp;

	for (i = 0; i < td->mrapiExt.numOfMemOps; i++) {
		pMemOp = &td->mrapiExt.memOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pMemOp->time == j) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				if (pMemOp->opType == MEM_OP_READ) {
					if (pMemOp->semIndex != -1) { /* lock the semaphore */
						semLock(fw, td, pMemOp->semIndex, tab, true);
					}
					shmMemRead(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
				} else if (pMemOp->opType == MEM_OP_WRITE) {
					shmMemWrite(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					if (pMemOp->semIndex != -1) { /* unlock the semaphore */
						fprintf(fw, "%s\t\n", tab);
						semUnlock(fw, td, pMemOp->semIndex, true, false, tab);
					}
				}
				currIndex++;
			}
		}
	}

	for (i = 0; i < td->mrapiExt.numOfSyncOps; i++) {
		pSyncOp = &td->mrapiExt.syncOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pSyncOp->time == j) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				if (pSyncOp->type == SYNC_TYPE_BAR) {
					barrierWait(fw, td, pSyncOp->resId, tab);
				} else if (pSyncOp->type == SYNC_TYPE_MTX) {
					mutexLock(fw, td, pSyncOp->resId, tab, false);
				} else if (pSyncOp->type == SYNC_TYPE_SEM) {
					semLock(fw, td, pSyncOp->resId, tab, false);
				}
				currIndex++;
			}
		}
	}

	return currIndex;
}

int doMcapiOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j;
	struct tMsgOp *pMsgOp;

	for (i = 0; i < td->mcapiExt.numOfMsgOps; i++) {
		pMsgOp = &td->mcapiExt.msgOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pMsgOp->time == j) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				if (pMsgOp->type == MSG_TYPE_MSG) {
					if (pMsgOp->opType == MSG_OP_SEND) {
						msgSend(fw, td, pMsgOp->resId, pMsgOp->resId2, pMsgOp->numOfIterations, tab);
					} else if (pMsgOp->opType == MSG_OP_RECV) {
						msgRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					}
				} else if (pMsgOp->type == MSG_TYPE_PKT) {
					if (pMsgOp->opType == MSG_OP_SEND) {
						pktSend(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					} else if (pMsgOp->opType == MSG_OP_RECV) {
						pktRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					}
				} else if (pMsgOp->type == MSG_TYPE_SCL) {
					if (pMsgOp->opType == MSG_OP_SEND) {
						sclSend(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					} else if (pMsgOp->opType == MSG_OP_RECV) {
						sclRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					}
				}
				currIndex++;
			}
		}
	}

	return currIndex;
}

int doOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	if (td->lsExt.hasExt) {
		currIndex = doTlsOps(fw, td, minTime, maxTime, tab, currIndex);
	}

	if (td->mrapiExt.hasExt) {
		currIndex = doMrapiOps(fw, td, minTime, maxTime, tab, currIndex);
	}

	if (td->mcapiExt.hasExt) {
		currIndex = doMcapiOps(fw, td, minTime, maxTime, tab, currIndex);
	}

	currIndex = doMtapiOps(fw, td, minTime, maxTime, tab, currIndex);

	return currIndex;
}

/***************************************************deletion******************************************************/

int deleteTls(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (!isTlsVarArray) {
		if (activeTd->lsExt.hasExt) {
			for (i = 0; i < activeTd->lsExt.numOfLocalMems; i++) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* free the local memory */\n", tab);
				fprintf(fw, "%s\tfree(localAddr%d);\n", tab, i + 1);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int releaseMrapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (activeTd->mrapiExt.hasExt) {
		for (i = 0; i < activeTd->mrapiExt.numOfSemaphores; i++) {
			if (!activeTd->mrapiExt.sems[i].isOwner) {
				if (activeTd->mrapiExt.sems[i].isLocked) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					fprintf(fw, "%s\t/* unlock the semaphore so the owner can lock for deletion */\n", tab);
					fprintf(fw, "%s\tmrapi_sem_unlock(sem%d, &mrapiStatus);\n", tab, i + 1);
					fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
					currIndex++;
				}
			}
		}
		for (i = 0; i < activeTd->mrapiExt.numOfMutexes; i++) {
			if (!activeTd->mrapiExt.mutexes[i].isOwner) {
				if (activeTd->mrapiExt.mutexes[i].isLocked) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					fprintf(fw, "%s\t/* unlock the mutex so the owner can lock for deletion */\n", tab);
					fprintf(fw, "%s\tmrapi_mutex_unlock(mutex%d, &lockKey, &mrapiStatus);\n", tab, i + 1);
					fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
					currIndex++;
				}
			}
		}
	}

	return currIndex;
}

int deleteMrapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (activeTd->mrapiExt.hasExt) {
		for (i = 0; i < activeTd->mrapiExt.numOfSemaphores; i++) {
			if (activeTd->mrapiExt.sems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* lock the semaphore */\n", tab);
				fprintf(fw, "%s\tmrapi_sem_lock(sem%d, MRAPI_TIMEOUT_INFINITE /*timeout*/, &mrapiStatus);\n", tab, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				fprintf(fw, "%s\t\n", tab);
				fprintf(fw, "%s\t/* delete the semaphore */\n", tab);
				fprintf(fw, "%s\tmrapi_sem_delete(sem%d, &mrapiStatus);\n", tab, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		for (i = 0; i < activeTd->mrapiExt.numOfMutexes; i++) {
			if (activeTd->mrapiExt.mutexes[i].isOwner) {
				if (!activeTd->mrapiExt.mutexes[i].isLocked) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					fprintf(fw, "%s\t/* lock the mutex */\n", tab);
					fprintf(fw, "%s\tmrapi_mutex_lock(mutex%d, &lockKey, MRAPI_TIMEOUT_INFINITE /*timeout*/, &mrapiStatus);\n", tab, i + 1);
					fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* delete the mutex */\n", tab);
				fprintf(fw, "%s\tmrapi_mutex_delete(mutex%d, &mrapiStatus);\n", tab, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		for (i = 0; i < activeTd->mrapiExt.numOfSharedMems; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			fprintf(fw, "%s\t/* detach from the shared memory */\n", tab);
			fprintf(fw, "%s\tmrapi_shmem_detach(shmem%d, &mrapiStatus);\n", tab, i + 1);
			fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
			currIndex++;
		}
	}

	return currIndex;
}

void closeChan(FILE *fw, struct tThreadData *td, struct tMcapiChannel *pMcapiChan, int i, char *tab) {

	fprintf(fw, "%s\t/* close the endpoint handles */\n", tab);
	fprintf(fw, "%s\tdo {\n", tab);
	if (pMcapiChan->type == EP_PACKET) {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_pktchan_send_close_i(pktSendHandle%d%d /*send_handle*/, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1);
		} else {
			fprintf(fw, "%s\t\tmcapi_pktchan_recv_close_i(pktRecvHandle%d%d /*recv_handle*/, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1);
		}
	} else {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_sclchan_send_close_i(sclSendHandle%d%d /*send_handle*/, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1);
		} else {
			fprintf(fw, "%s\t\tmcapi_sclchan_recv_close_i(sclRecvHandle%d%d /*recv_handle*/, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1);
		}
	}
	fprintf(fw, "%s\t//retry if all request handles are in-use\n", tab);
	fprintf(fw, "%s\t} while (mcapiStatus == MCAPI_ERR_REQUEST_LIMIT);\n", tab);
	fprintf(fw, "%s\tmcapi_wait(&mcapiRequest, &size, MCA_INFINITE, &mcapiStatus);\n", tab);
	fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);

	return;
}

int deleteMcapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;
	struct tMcapiChannel *pMcapiChan;

	if (activeTd->mcapiExt.hasExt) {
		for (i = 0; i < activeTd->mcapiExt.numOfPcktChans; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			pMcapiChan = activeTd->mcapiExt.pcktChans[i];
			closeChan(fw, activeTd, pMcapiChan, i, tab);
			currIndex++;
		}
		for (i = 0; i < activeTd->mcapiExt.numOfScalChans; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			pMcapiChan = activeTd->mcapiExt.scalChans[i];
			closeChan(fw, activeTd, pMcapiChan, i, tab);
			currIndex++;
		}
		for (i = 0; i < activeTd->mcapiExt.numOfEndpoints; i++) {
			if (activeTd->mcapiExt.endpoints[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* delete the endpoint we've created */\n", tab);
				fprintf(fw, "%s\tmcapi_endpoint_delete(ep%d, &mcapiStatus);\n", tab, i + 1);
				fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int deletePosix(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (!activeTd->mrapiExt.hasExt && !activeTd->mcapiExt.hasExt) {
		for (i = 0; i < activeTd->posixExt.numOfSemaphores; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			//FIXME: when the owner closes the sem, it is removed. Is this correct behavior of POSIX standard?
//			if (activeTd->posixExt.sems[i].isOwner) {
//				fprintf(fw, "%s\t/* unlink the semaphore */\n", tab);
//				fprintf(fw, "%s\tretVal = sem_unlink(\"sem%x\");\n", tab, activeTd->posixExt.sems[i].key);
//				fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
//				fprintf(fw, "%s\t\n", tab);
//			}
			fprintf(fw, "%s\t/* remove reference of the semaphore */\n", tab);
			fprintf(fw, "%s\tretVal = sem_close(sem%d);\n", tab, i + 1);
			fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
			currIndex++;
		}
		for (i = 0; i < activeTd->posixExt.numOfMutexes; i++) {
			if (activeTd->posixExt.mutexes[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* destroy the mutex */\n", tab);
				fprintf(fw, "%s\tretVal = pthread_mutex_destroy(mutex%d);\n", tab, i + 1);
				fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int releaseMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	currIndex = releaseMrapi(fw, activeTd, tab, currIndex);

	return currIndex;
}

int deleteMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	currIndex = deleteTls(fw, activeTd, tab, currIndex);
	currIndex = deleteMrapi(fw, activeTd, tab, currIndex);
	currIndex = deleteMcapi(fw, activeTd, tab, currIndex);
	currIndex = deletePosix(fw, activeTd, tab, currIndex);

	return currIndex;
}

/***************************************************finalize******************************************************/

void finalizeMxApi(FILE *fw, struct tThreadData *activeTd) {

	int t, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean hasMrapiExt = false, hasMcapiExt = false;

	numOfThreads = activeProc->numOfThreads;
	for (t = activeTd->id; t <= numOfThreads; t++) {
		td = activeProc->threads[t];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mrapiExt.hasExt) {
				hasMrapiExt = true;
			}
			if (td->mcapiExt.hasExt) {
				hasMcapiExt = true;
			}
			if (hasMrapiExt && hasMrapiExt) {
				break;
			}
		}
	}

	if (hasMrapiExt && hasMcapiExt) {
		fprintf(fw, "\t\n");
		fprintf(fw, "\t/*\n");
		fprintf(fw, "\t* mrapi_finalize(&mrapiStatus); // mcapi_finalize does\n");
		fprintf(fw, "\t* MRAPI_CHECK\n");
		fprintf(fw, "\t* */\n");
	}

	if (hasMcapiExt) {
		fprintf(fw, "\t\n");
		fprintf(fw, "\tmcapi_finalize(&mcapiStatus);\n");
		fprintf(fw, "\tMCAPI_CHECK\n");
		fprintf(fw, "\t\n");
	}

	if (hasMrapiExt && !hasMcapiExt) {
		fprintf(fw, "\t\n");
		fprintf(fw, "\tmrapi_finalize(&mrapiStatus);\n");
		fprintf(fw, "\tMRAPI_CHECK\n");
		fprintf(fw, "\t\n");
	}

	return;
}

/***************************************************thread operations******************************************************/

void getTab(int tabCount, char *tab) {

	int i;

	for (i = 0; i < tabCount && i < MAX_INDENT - 1; i++) {
		tab[i] = '\t';
	}
	tab[tabCount] = '\0';

	return;
}

int getMaxGroupId(struct tThreadData *td) {

	int i, numOfThreads;
	int maxGrpId = 0;
	struct tThreadData *itTd;

	if (!isStandalone) {
		maxGrpId = maxGroupId;
	} else {
		numOfThreads = activeProc->numOfThreads;
		for (i = 2; i <= numOfThreads; i++) { /* except main */
			itTd = activeProc->threads[i];
			if (itTd->creator == td->id) {
				if (itTd->standAloneExt.phase > maxGrpId) {
					maxGrpId = itTd->standAloneExt.phase;
				}
			}
		}
	}

	return maxGrpId;

}

int getCreateJoin(FILE *fw, struct tThreadData *activeTd, int numOfThreads, int tabCount, char *tab, int currIndex) {

	int i, j, k, p, t;
	struct tThreadData *td, *tdIt;
	int numTasksInGroup;
	boolean isNextGroup, isNextThread;
	int createdPCs[MAX_THREADS];
	int numOfCreatedPCs = 0;

	maxGroupId = getMaxGroupId(activeTd);

	for (p = 0, t = 0, isNextGroup = false; p < maxGroupId; p++, isNextGroup = false) {

//		if (p == 0) {
			currIndex = createMxapi(fw, activeTd, tab, currIndex, p);
//		}

		currIndex = doOps(fw, activeTd, p * MAX_WORK_PER_PHASE, p * MAX_WORK_PER_PHASE, tab, currIndex); /* initial */

		/* clear information of the previous phase */
		for (k = 0; k < numOfCreatedPCs; k++) {
			createdPCs[k] = 0;
		}
		numOfCreatedPCs = 0;

		if (currIndex > 0) {
			fprintf(fw, "%s\t\n", tab);
		}
		fprintf(fw, "%s\t/* Create and run all the threads of phase %d */\n", tab, p + 1);
		if (p == 0) {
			fprintf(fw, "%s\tt = 0;\n", tab);
		}

		for (i = 2; i <= numOfThreads; i++) {
			td = activeProc->threads[i];
			if (td->creator == activeTd->id && td->groupId == p) {
				for (k = 0, isNextThread = false; k < numOfCreatedPCs; k++) {
					if (td->pc == createdPCs[k]) {
						isNextThread = true;
						break;
					}
				}
				if (isNextThread) {
					continue;
				}
				for (j = i + 1, numTasksInGroup = 1; j <= numOfThreads; j++) {
					tdIt = activeProc->threads[j];
					if ((tdIt->creator == activeTd->id && tdIt->groupId == p) && tdIt->mtapiExt.startRoutine.taskId == td->mtapiExt.startRoutine.taskId) {
						numTasksInGroup++;
					}
				}
				if (numTasksInGroup > 1) {
					createdPCs[numOfCreatedPCs++] = td->pc;
					fprintf(fw, "%s\tfor(n = 0; n < %d; n++, t++) {\n", tab, numTasksInGroup);
					fprintf(fw, "%s\t\trc = pthread_create(&threads[t], NULL, task%d, (void *) &tData[t]);\n",
							tab, td->mtapiExt.startRoutine.taskId);
					fprintf(fw, "%s\t\tif (rc) {\n", tab);
					fprintf(fw, "%s\t\t\tfprintf(stderr,\"ERROR; return code from pthread_create() is %s\\n\", rc);\n", tab, "%d");
					fprintf(fw, "%s\t\t\texit(-1);\n", tab);
					fprintf(fw, "%s\t\t}\n", tab);
					fprintf(fw, "%s\t}\n", tab);
					if (numTasksInGroup == activeTd->mtapiExt.numOfChildren[p]) {
						isNextGroup = true;
						currIndex++;
						break;
					}
				} else {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					fprintf(fw, "%s\trc = pthread_create(&threads[t], NULL, task%d, (void *) &tData[t]);\n",
							tab, td->mtapiExt.startRoutine.taskId);
					t++;
					fprintf(fw, "%s\tif (rc) {\n", tab);
					fprintf(fw, "%s\t\tfprintf(stderr,\"ERROR; return code from pthread_create() is %s\\n\", rc);\n", tab, "%d");
					fprintf(fw, "%s\t\texit(-1);\n", tab);
					fprintf(fw, "%s\t}\n", tab);
					fprintf(fw, "%s\tt++;\n", tab);
				}
			}
			if (isNextGroup) {
				break;
			}
			currIndex++;
		}

//		if (p == 0) {
			currIndex = getMxapi(fw, activeTd, tab, currIndex, p);
//		}

		if (activeTd->mtapiExt.loopCount > 0) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			fprintf(fw, "%s\tfor (loopCount = 0; loopCount < %d; loopCount++) {\n", tab, activeTd->mtapiExt.loopCount);
			tabCount++;
			getTab(tabCount, tab);
		}

		currIndex = doOps(fw, activeTd, (p * MAX_WORK_PER_PHASE) + 1, (p * MAX_WORK_PER_PHASE) + 1, tab, currIndex); /* internal - init */

		currIndex = doOps(fw, activeTd, (p * MAX_WORK_PER_PHASE) + 2, (p * MAX_WORK_PER_PHASE) + 2, tab, currIndex); /* internal - final */

		if (activeTd->mtapiExt.loopCount > 0) {
			fprintf(fw, "%s\t\n", tab);
			tabCount--;
			getTab(tabCount, tab);
			fprintf(fw, "%s\t}\n", tab);
		}

		fprintf(fw, "%s\t\n", tab);
		fprintf(fw, "%s\t/* Wait for all threads of phase %d*/\n", tab, p + 1);
		fprintf(fw, "%s\tfor (t = 0; t < %d; t++) {\n", tab, activeTd->mtapiExt.numOfChildren[p]);
		fprintf(fw, "%s\t\tpthread_join(threads[t], NULL);\n", tab);
		fprintf(fw, "%s\t}\n", tab);

		currIndex = doOps(fw, activeTd, (p * MAX_WORK_PER_PHASE) + 3, (p * MAX_WORK_PER_PHASE) + 3, tab, currIndex); /* final */

	}

	return currIndex;
}

int getMaxChildren(struct tThreadData *activeTd) {

	int i;
	int maxChildren = 0;

	maxGroupId = getMaxGroupId(activeTd);

	for (i = 0; i < maxGroupId; i++) {
		if (activeTd->mtapiExt.numOfChildren[i] > maxChildren) {
			maxChildren = activeTd->mtapiExt.numOfChildren[i];
		}
	}

	return maxChildren;
}

int getTotalChildren(struct tThreadData *activeTd) {

	int i;
	int totalChildren = 0;

	maxGroupId = getMaxGroupId(activeTd);

	for (i = 0; i < maxGroupId; i++) {
		totalChildren += activeTd->mtapiExt.numOfChildren[i];
	}

	return totalChildren;
}

void genTask(FILE *fw, struct tThreadData *activeTd, int numOfThreads) {

	int maxChildren, totalChildren, threadChildren;
	int i, t, tIdx, currThread, threadCountInTask = activeTd->mtapiExt.startRoutine.threadCount;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	int tabCount, currIndex = 0;
	char tab[MAX_INDENT] = { '\0' };

	maxChildren = getMaxChildren(activeTd);
	totalChildren = getTotalChildren(activeTd);

	/* variables */
	if (totalChildren > 0) {
		fprintf(fw, "\tthreadData tData[%d];\n", totalChildren);
	}

	if (maxChildren > 0) {
		fprintf(fw, "\tpthread_t threads[%d];\n", maxChildren);
		fprintf(fw, "\tint rc;\n");
		fprintf(fw, "\tint t;\n");
		fprintf(fw, "\tint n;\n");
	}

	if (activeTd->id > 1) {
		fprintf(fw, "\tthreadData* td = (threadData*) param;\n");
	}

	genMxapiPars(fw, activeTd);

	initMxapi(fw, activeTd);

	for (t = activeTd->id, currThread = 0, tabCount = 0; t <= numOfThreads; t++, tabCount = 0, currIndex = 0) {
		td = activeProc->threads[t];

		threadChildren = getTotalChildren(td);

		if (td->mtapiExt.startRoutine.taskId == taskId) {

			currThread++;

			getTab(tabCount, tab);
			if (!activeProc->mtapiExt.hasUniqueThreads[td->mtapiExt.startRoutine.taskId] && threadCountInTask > 1) {
				fprintf(fw, "\tif (td->tid == %d) {\n", td->id);

#ifdef PRINT_CTRL_FLOW_IN_SYN
				fprintf(fw, "\t\tprintf(\"Thread %d started\\n\");\n", td->id);
#endif

				tabCount++;
				getTab(tabCount, tab);
			}

			/* functions */
			if (threadChildren > 0) {
				for (i = 2, tIdx = 0; i <= numOfThreads; i++) {
					if (activeProc->threads[i]->creator == td->id) {
						fprintf(fw, "%s\ttData[%d].tid = %d;\n", tab, tIdx, activeProc->threads[i]->id);
						tIdx++;
					}
				}
				fprintf(fw, "\t\t\n");
			}

			if (threadChildren == 0) {
				currIndex = createAndGetMxapi(fw, td, tab, currIndex, td->groupId);

				currIndex = doOps(fw, td, 0, 0, tab, currIndex); /* initial */

				if (td->mtapiExt.loopCount > 0) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					fprintf(fw, "%s\tfor (loopCount = 0; loopCount < %d; loopCount++) {\n", tab, td->mtapiExt.loopCount);
					tabCount++;
					getTab(tabCount, tab);
					currIndex++;
				}

				currIndex = doOps(fw, td, 1, 1, tab, currIndex); /* internal - init */

				currIndex = doOps(fw, td, 2, 2, tab, currIndex); /* internal - final */

				if (td->mtapiExt.loopCount > 0) {
					fprintf(fw, "%s\t\n", tab);
					tabCount--;
					getTab(tabCount, tab);
					fprintf(fw, "%s\t}\n", tab);
				}

				currIndex = doOps(fw, td, 3, 3, tab, currIndex); /* final */

			} else {
				/* create child threads if any exists */
				currIndex = getCreateJoin(fw, td, numOfThreads, tabCount, tab, currIndex);
			}

			currIndex = releaseMxapi(fw, td, tab, currIndex);

			currIndex = deleteMxapi(fw, td, tab, currIndex);

#ifdef PRINT_CTRL_FLOW_IN_SYN
			fprintf(fw, "\t\tprintf(\"Thread %d exited\\n\");\n", td->id);
#endif

			if (!activeProc->mtapiExt.hasUniqueThreads[td->mtapiExt.startRoutine.taskId]) {
				if (threadCountInTask > 1) {
					fprintf(fw, "\t}\n");
				}
				if (currThread < threadCountInTask) {
					fprintf(fw, "\t\n");
				}
			} else {
				break; /* generating the code for the first process is enough */
			}

		}
	}

	finalizeMxApi(fw, activeTd);

	return;
}

void genMainThread(FILE *fw, struct tThreadData *mainTd, int numOfThreads) {

//	fprintf(fw, "int syn_main(int argc, char **argv) {\n");
	fprintf(fw, "int main(int argc, char **argv) {\n");

	/* variables */
	if ((mainTd->mrapiExt.hasExt || mainTd->mcapiExt.hasExt) || mainTd->lsExt.hasExt) {
		fprintf(fw, "\tthreadData maintData = { 1 };\n");
		fprintf(fw, "\tthreadData *td = &maintData;\n");
	}

	/* functions */

	genTask(fw, mainTd, numOfThreads);

	if (!mainTd->mrapiExt.hasExt && !mainTd->mcapiExt.hasExt) {
		fprintf(fw, "\t\n");
	}

	fprintf(fw, "\treturn 0;\n");
	fprintf(fw, "}\n");
	//fprintf(fw, "\n");

	return;
}

void genThread(FILE *fw, struct tThreadData *td, int numOfThreads) {

	int taskId = td->mtapiExt.startRoutine.taskId;

	if (taskId < MAX_THREADS && activeProc->mtapiExt.isStartRoutine[taskId] != SR_CREATED) {

		fprintf(fw, "void *task%d(void *param) {\n", taskId);

		genTask(fw, td, numOfThreads);

		if (!td->mrapiExt.hasExt && !td->mcapiExt.hasExt) {
			fprintf(fw, "\t\n");
		}

		fprintf(fw, "\treturn NULL;\n");
		fprintf(fw, "}\n");
		fprintf(fw, "\n");

		activeProc->mtapiExt.isStartRoutine[taskId] = SR_CREATED; /* this start routine is create */
	}

	return;
}

void genThreads(FILE *fw) {

	int i;
	int numOfThreads;
	struct tThreadData *td;

	numOfThreads = activeProc->numOfThreads;
	for (i = 2; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		genThread(fw, td, numOfThreads);
	}

	genMainThread(fw, activeProc->threads[1], numOfThreads);

	return;
}

/***************************************************main******************************************************/

int genBenchmark(int phaseCount, paralPattern pp[MAX_GROUP_NUMBER]) {

	FILE *fw;
	char *temp, name[MAX_PATH] = { '\0' };
	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasMrapiExt = false, hasMcapiExt = false;

	activeProc = procs[0];
	temp = getBaseName(activeProc->name);
	strcpy(name, temp);
	strcat(name, "_syn.c");

//	fw = stdout;
	fw = fopen(name, "w");
	if (fw == NULL) {
		printf("ERROR! Cannot generate %s.\n", name);
		return -1;
	}

	numOfThreads = activeProc->numOfThreads;
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mrapiExt.hasExt) {
			hasMrapiExt = true;
		}
		if (td->mcapiExt.hasExt) {
			hasMcapiExt = true;
		}
		if (hasMrapiExt && hasMrapiExt) {
			break;
		}
	}

	genHeader(fw, pp, hasMrapiExt, hasMcapiExt);
	genDefines(fw, hasMrapiExt, hasMcapiExt);
	genStructures(fw, hasMrapiExt, hasMcapiExt);
	genGlobals(fw, hasMrapiExt, hasMcapiExt);
	genFuncSignatures(fw, hasMrapiExt, hasMcapiExt);
	genThreads(fw);

	fclose(fw);

	return 0;
}

int genBenchmarkSa(int phaseCount, paralPattern pp[MAX_GROUP_NUMBER]) {

	FILE *fw;
	char *temp, name[MAX_PATH] = { '\0' };
	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasMrapiExt = false, hasMcapiExt = false;

	activeProc = procs[0];
	temp = getBaseName(activeProc->name);
	strcpy(name, temp);
	strcat(name, "_syn.c");

//	fw = stdout;
	fw = fopen(name, "w");
	if (fw == NULL) {
		printf("ERROR! Cannot generate %s.\n", name);
		return -1;
	}

	numOfThreads = activeProc->numOfThreads;
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mrapiExt.hasExt) {
			hasMrapiExt = true;
		}
		if (td->mcapiExt.hasExt) {
			hasMcapiExt = true;
		}
		if (hasMrapiExt && hasMrapiExt) {
			break;
		}
	}

	genHeader(fw, pp, hasMrapiExt, hasMcapiExt);
	genDefines(fw, hasMrapiExt, hasMcapiExt);
	genStructures(fw, hasMrapiExt, hasMcapiExt);
	genGlobals(fw, hasMrapiExt, hasMcapiExt);
	genFuncSignatures(fw, hasMrapiExt, hasMcapiExt);
	genThreads(fw);

	fclose(fw);

	return 0;
}

/***************************************************gpu operations******************************************************/

void convertToUpperCase(char *sPtr) {
	while(*sPtr != '\0') {
		*sPtr = toupper((unsigned char) *sPtr);
		++sPtr;
	}
	return;
}

void genGpuHeaderClassData(FILE *fw) {

	int i;

	fprintf(fw, "\tcl_uint                  seed;                  /**< Seed value for random number generation */\n");
	fprintf(fw, "\tcl_double           setupTime;                  /**< Time for setting up OpenCL */\n");
	fprintf(fw, "\tcl_double             appTime;                  /**< Time for transfer + kernel execution */\n");
	fprintf(fw, "\tcl_double          kernelTime;                  /**< Time for kernel execution */\n");
	fprintf(fw, "\tcl_float              *input0;                  /**< Input array */\n");
	fprintf(fw, "\tcl_int                 width0;                  /**< width of input Array */\n");
	fprintf(fw, "\tcl_int                height0;                  /**< height of input Array */\n");
	fprintf(fw, "\tcl_float              *input1;                  /**< Input array */\n");
	fprintf(fw, "\tcl_int                 width1;                  /**< width of Input Array */\n");
	fprintf(fw, "\tcl_int                height1;                  /**< height of Input Array */\n");
	fprintf(fw, "\tcl_float              *output;                  /**< Output Array */\n");
	fprintf(fw, "\tcl_float  *verificationOutput;                  /**< Output array for reference implementation */\n");
	fprintf(fw, "\tcl_uint             blockSize;                  /**< Size of the block used for shared memory */\n");
	fprintf(fw, "\tcl_context            context;                  /**< CL context */\n");
	fprintf(fw, "\tcl_device_id         *devices;                  /**< CL device list */\n");
	fprintf(fw, "\tcl_mem           inputBuffer0;                  /**< CL memory buffer  for matrix input0 */\n");
	fprintf(fw, "\tcl_mem           inputBuffer1;                  /**< CL memory buffer  for matrix input1 */\n");
	fprintf(fw, "\tcl_mem           outputBuffer;                  /**< CL memory buffer  for storing the output */\n");
	fprintf(fw, "\tcl_int                      w0;                  /**< w0 width of matrix A */\n");
	fprintf(fw, "\tcl_int                      h0;                  /**< h0 height of matrix A */\n");
	fprintf(fw, "\tcl_int                      w1;                  /**< w1 width of matrix B */\n");
	fprintf(fw, "\tcl_int                      h1;                  /**< h1 height of matrix B */\n");
	fprintf(fw, "\tcl_command_queue commandQueue;                  /**< CL command queue */\n");
	fprintf(fw, "\tcl_program            program;                  /**< CL program */\n");
	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
		fprintf(fw, "\tcl_kernel              kernel%d;                  /**< CL kernel */\n", i);
		fprintf(fw, "\tbool                      lds%d;                  /**< Local data store availability */\n", i);
		fprintf(fw, "\tsize_t       globalThreads%d[%d];                  /**< global NDRange */\n", i, procs[0]->gpuExt.uniqueNDRanges[i].workDim);
		fprintf(fw, "\tsize_t        localThreads%d[%d];                  /**< local Threads */\n",i,  procs[0]->gpuExt.uniqueNDRanges[i].workDim);
		fprintf(fw, "\tKernelWorkGroupInfo kernelInfo%d;                 /**< Structure to store kernel related info */\n", i);
	}
	fprintf(fw, "\tcl_ulong availableLocalMemory;                  /**< Total Local Memory available for the kernel */\n");
	fprintf(fw, "\tcl_ulong    neededLocalMemory;                  /**< Total Local Memory needed by the kernel */\n");
	fprintf(fw, "\tint                 iterations;                 /**< Number of iterations for kernel execution */\n");
	fprintf(fw, "\tSDKDeviceInfo       deviceInfo;                 /**< Structure to store device information */\n");
	fprintf(fw, "\tbool                eAppGFLOPS;\n");

	fprintf(fw, "\tSDKTimer          *sampleTimer;                 /**< SDKTimer object */\n");

	return;
}

void genGpuHeaderPublicData(FILE *fw, char *className) {

	int i;

	fprintf(fw, "\t\tCLCommandArgs   *sampleArgs;               /**< CLCommand argument class */\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Constructor\n");
	fprintf(fw, "\t\t * Initialize member variables\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\t%s()\n", className);
	fprintf(fw, "\t\t{\n");
	fprintf(fw, "\t\t\tsampleArgs = new CLCommandArgs();\n");
	fprintf(fw, "\t\t\tsampleTimer = new SDKTimer();\n");
	fprintf(fw, "\t\t\tsampleArgs->sampleVerStr = \"AMD-APP-SDK-v2.9.214.1\";\n");
	fprintf(fw, "\t\t\tseed   = 123;\n");
	fprintf(fw, "\t\t\tinput0 = NULL;\n");
	fprintf(fw, "\t\t\tinput1 = NULL;\n");
	fprintf(fw, "\t\t\toutput = NULL;\n");
	fprintf(fw, "\t\t\tverificationOutput = NULL;\n");

	fprintf(fw, "\t\t\tw0 = 256;\n");
	if (procs[0]->gpuExt.maxWorkDim == 1) {
		fprintf(fw, "\t\t\th0 = 1;\n");
	} else {
		fprintf(fw, "\t\t\th0 = 64;\n");
	}
	fprintf(fw, "\t\t\tw1 = 256;\n");
	if (procs[0]->gpuExt.maxWorkDim == 1) {
		fprintf(fw, "\t\t\th1 = 1;\n");
	} else {
		fprintf(fw, "\t\t\th1 = 64;\n");
	}
//	if (updateDataGpu[0].mclCount <= 1) {
//		fprintf(fw, "\t\t\tblockSize = %d;\n",
//				(int) sqrt((double) procs[0]->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0]));
//	} else {
		fprintf(fw, "\t\t\tblockSize = %d;\n", 32);
//	}
	fprintf(fw, "\t\t\tsetupTime = 0;\n");
	fprintf(fw, "\t\t\tappTime = 0;\n");
	fprintf(fw, "\t\t\titerations = 1;\n");

	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
		if (updateDataGpu[i].mclCount == 0 && updateDataGpu[i].lmrCount == 0) {
			fprintf(fw, "\t\t\tlds%d = 0;\n", i);
		} else {
			fprintf(fw, "\t\t\tlds%d = 1;\n", i);
		}
	}
	fprintf(fw, "\t\t\teAppGFLOPS = false;\n");

	fprintf(fw, "\t\t}\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Allocate and initialize host memory array with random values\n");
	fprintf(fw, "\t\t * @return SDK_SUCCESS on success and SDK_FAILURE on failure\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\tint setup%s();\n", className);
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Function to compute local WorkGroup Size based on inputs\n");
	fprintf(fw, "\t\t * and device properties\n");
	fprintf(fw, "\t\t */\n");
	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
		fprintf(fw, "\t\tint setWorkGroupSize%d();\n", i);
	}
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Override from SDKSample, Generate binary image of given kernel\n");
	fprintf(fw, "\t\t * and exit application\n");
	fprintf(fw, "\t\t * @return SDK_SUCCESS on success and SDK_FAILURE on failure\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\tint genBinaryImage();\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * OpenCL related initialisations.\n");
	fprintf(fw, "\t\t * Set up Context, Device list, Command Queue, Memory buffers\n");
	fprintf(fw, "\t\t * Build CL kernel program executable\n");
	fprintf(fw, "\t\t * @return SDK_SUCCESS on success and SDK_FAILURE on failure\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\tint setupCL();\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Set values for kernels' arguments, enqueue calls to the kernels\n");
	fprintf(fw, "\t\t * on to the command queue, wait till end of kernel execution.\n");
	fprintf(fw, "\t\t * Get kernel start and end time if timing is enabled\n");
	fprintf(fw, "\t\t * @return SDK_SUCCESS on success and SDK_FAILURE on failure\n");
	fprintf(fw, "\t\t */\n");
	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
		fprintf(fw, "\t\tint runCLKernels%d();\n", i);
	}
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Override from SDKSample. Print sample stats.\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\tvoid printStats();\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Override from SDKSample. Initialize\n");
	fprintf(fw, "\t\t * command line parser, add custom options\n");
	fprintf(fw, "\t\t * @return SDK_SUCCESS on success and SDK_FAILURE on failure\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\tint initialize();\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Override from SDKSample, adjust width and height\n");
	fprintf(fw, "\t\t * of execution domain, perform all sample setup\n");
	fprintf(fw, "\t\t * @return SDK_SUCCESS on success and SDK_FAILURE on failure\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\tint setup();\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Override from SDKSample\n");
	fprintf(fw, "\t\t * Run OpenCL Synthetic Benchmark\n");
	fprintf(fw, "\t\t * @return SDK_SUCCESS on success and SDK_FAILURE on failure\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\tint run();\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t/**\n");
	fprintf(fw, "\t\t * Override from SDKSample\n");
	fprintf(fw, "\t\t * Cleanup memory allocations\n");
	fprintf(fw, "\t\t * @return SDK_SUCCESS on success and SDK_FAILURE on failure\n");
	fprintf(fw, "\t\t */\n");
	fprintf(fw, "\t\tint cleanup();\n");

	return ;
}

int genGpuHeaderFile(FILE *fw, char *className) {

	char headerDefine[MAX_PATH] = { '\0' };
	strcpy(headerDefine, className);
	convertToUpperCase(headerDefine);
	strcat(headerDefine, "_H_");

	fprintf(fw, "%s", copyright);
	fprintf(fw, "#ifndef %s\n", headerDefine);
	fprintf(fw, "#define %s\n", headerDefine);
	fprintf(fw, "\n");
	fprintf(fw, "%s", gpuHeaderInclude);

	fprintf(fw, "\n");
	fprintf(fw, "using namespace appsdk;\n");
	fprintf(fw, "\n");

	fprintf(fw, "class %s\n", className);
	fprintf(fw, "{\n");

	genGpuHeaderClassData(fw);
	fprintf(fw, "\n");

	fprintf(fw, "\tpublic:\n");
	fprintf(fw, "\n");
	genGpuHeaderPublicData(fw, className);

	fprintf(fw, "};\n");

	fprintf(fw, "\n");
	fprintf(fw, "#endif\n");

	return 0;
}

void genSetupClassFunction(FILE *fw, char *className) {

	fprintf(fw, "int\n");
	fprintf(fw, "%s::setup%s()\n", className, className);
	fprintf(fw, "{\n");
	//FIXME: do the followings in a loop and create a new function that works on only one input >>>>>>
	fprintf(fw, "\t// allocate and init memory used by host  input0[width0][height0]\n");
	fprintf(fw, "\tcl_uint inputSizeBytes0 = width0 * height0 * sizeof(cl_float);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tinput0 = (cl_float *) malloc(inputSizeBytes0);\n");
	fprintf(fw, "\tCHECK_ALLOCATION(input0, \"Failed to allocate host memory. (input0)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// random initialisation of input\n");
	fprintf(fw, "\tfillRandom<cl_float>(input0, width0, height0, 0, 10);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// allocate and init memory used by host input1[width1][height1]\n");
	fprintf(fw, "\tcl_uint inputSizeBytes1 = width1 * height1 * sizeof(cl_float);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tinput1 = (cl_float *) malloc(inputSizeBytes1);\n");
	fprintf(fw, "\tCHECK_ALLOCATION(input1, \"Failed to allocate host memory. (input1)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// random initialisation of input\n");
	fprintf(fw, "\tfillRandom<cl_float>(input1, width1, height1, 0, 10);\n");
	fprintf(fw, "\t\n");
	//FIXME: <<<<<<<
	fprintf(fw, "\t// allocate memory for output[width1][height0]\n");
	fprintf(fw, "\tcl_uint outputSizeBytes = height0 * width1 * sizeof(cl_float);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\toutput = (cl_float *) malloc(outputSizeBytes);\n");
	fprintf(fw, "\tCHECK_ALLOCATION(output, \"Failed to allocate host memory. (output)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// allocate memory for output[width1][height0] of reference implementation\n");
	fprintf(fw, "\tif(sampleArgs->verify)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tverificationOutput = (cl_float *) malloc(outputSizeBytes);\n");
	fprintf(fw, "\tCHECK_ALLOCATION(verificationOutput,\n");
	fprintf(fw, "\t\t\t\t\"Failed to allocate host memory. (verificationOutput)\");\n");
	fprintf(fw, "\t\tmemset(verificationOutput, 0, outputSizeBytes);\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Unless quiet mode has been enabled, print the INPUT arrays\n");
	fprintf(fw, "\tif(!sampleArgs->quiet)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tprintArray<cl_float>(\n");
	fprintf(fw, "\t\t\t\"Input0\",\n");
	fprintf(fw, "\t\t\tinput0,\n");
	fprintf(fw, "\t\t\twidth0,\n");
	fprintf(fw, "\t\t\t1);\n");
	fprintf(fw, "\t\tprintArray<cl_float>(\n");
	fprintf(fw, "\t\t\t\"Input1\",\n");
	fprintf(fw, "\t\t\tinput1,\n");
	fprintf(fw, "\t\t\twidth1,\n");
	fprintf(fw, "\t\t\t1);\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genGenBinaryImageFunction(FILE *fw, char *className) {

	char kernelName[MAX_PATH] = { '\0' };

	strcpy(kernelName, className);
	strcat(kernelName, "_Kernels.cl");

	fprintf(fw, "/**\n");
	fprintf(fw, " * genBinary Image function is used to when we want to create the binary\n");
	fprintf(fw, " * for the kernel and run it at a later time. This is useful where offline\n");
	fprintf(fw, " * compilation is the preferred mechanism.\n");
	fprintf(fw, " */\n");

	fprintf(fw, "int\n");
	fprintf(fw, "%s::genBinaryImage()\n", className);
	fprintf(fw, "{\n");
	fprintf(fw, "\tbifData binaryData;\n");
	fprintf(fw, "\tbinaryData.kernelName = std::string(\"%s\");\n", kernelName);
	fprintf(fw, "\tbinaryData.flagsStr = std::string(\"\");\n");
	fprintf(fw, "\tif(sampleArgs->isComplierFlagsSpecified())\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tbinaryData.flagsFileName = std::string(sampleArgs->flags.c_str());\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tbinaryData.binaryName = std::string(sampleArgs->dumpBinary.c_str());\n");
	fprintf(fw, "\tint status = generateBinaryImage(binaryData);\n");
	fprintf(fw, "\treturn status;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genSetupCLFunction(FILE *fw, char *className) {

	int i;

	fprintf(fw, "int\n");
	fprintf(fw, "%s::setupCL(void)\n", className);
	fprintf(fw, "{\n");
	fprintf(fw, "\tcl_int status = 0;\n");
	fprintf(fw, "\tcl_device_type dType;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(sampleArgs->deviceType.compare(\"cpu\") == 0)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tdType = CL_DEVICE_TYPE_CPU;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\telse //deviceType = \"gpu\"\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tdType = CL_DEVICE_TYPE_GPU;\n");
	fprintf(fw, "\t\tif(sampleArgs->isThereGPU() == false)\n");
	fprintf(fw, "\t\t{\n");
	fprintf(fw, "\t\t\tstd::cout << \"GPU not found. Falling back to CPU device\" << std::endl;\n");
	fprintf(fw, "\t\t\tdType = CL_DEVICE_TYPE_CPU;\n");
	fprintf(fw, "\t\t}\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t/*\n");
	fprintf(fw, "\t * Have a look at the available platforms and pick either\n");
	fprintf(fw, "\t * the AMD one if available or a reasonable default.\n");
	fprintf(fw, "\t */\n");
	fprintf(fw, "\tcl_platform_id platform = NULL;\n");
	fprintf(fw, "\tint retValue = getPlatform(platform, sampleArgs->platformId,\n");
	fprintf(fw, "\t\t\t\t\t\t\t\tsampleArgs->isPlatformEnabled());\n");
	fprintf(fw, "\tCHECK_ERROR(retValue, SDK_SUCCESS, \"getPlatform() failed\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Display available devices.\n");
	fprintf(fw, "\tretValue = displayDevices(platform, dType);\n");
	fprintf(fw, "\tCHECK_ERROR(retValue, SDK_SUCCESS, \"displayDevices() failed\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// creating context\n");
	fprintf(fw, "\tcl_context_properties cps[3] =\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tCL_CONTEXT_PLATFORM,\n");
	fprintf(fw, "\t\t(cl_context_properties)platform,\n");
	fprintf(fw, "\t\t0\n");
	fprintf(fw, "\t};\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tcontext = clCreateContextFromType(\n");
	fprintf(fw, "\t\t\t\t\tcps,\n");
	fprintf(fw, "\t\t\t\t\tdType,\n");
	fprintf(fw, "\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\t&status);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clCreateContextFromType failed.\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// getting device on which to run the sample\n");
	fprintf(fw, "\tstatus = getDevices(context, &devices, sampleArgs->deviceId,\n");
	fprintf(fw, "\t\t\t\t\t\tsampleArgs->isDeviceIdEnabled());\n");
	fprintf(fw, "\tCHECK_ERROR(status, SDK_SUCCESS, \"getDevices() failed\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t//Set device info of given cl_device_id\n");
	fprintf(fw, "\tretValue = deviceInfo.setDeviceInfo(devices[sampleArgs->deviceId]);\n");
	fprintf(fw, "\tCHECK_ERROR(retValue, SDK_SUCCESS, \"SDKDeviceInfo::setDeviceInfo() failed\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\t// The block is to move the declaration of prop closer to its use\n");
	fprintf(fw, "\t\tcl_command_queue_properties prop = 0;\n");
	fprintf(fw, "\t\tif(!eAppGFLOPS)\n");
	fprintf(fw, "\t\t{\n");
	fprintf(fw, "\t\t\tprop |= CL_QUEUE_PROFILING_ENABLE;\n");
	fprintf(fw, "\t\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t\tcommandQueue = clCreateCommandQueue(\n");
	fprintf(fw, "\t\t\t\t\t\t\tcontext,\n");
	fprintf(fw, "\t\t\t\t\t\t\tdevices[sampleArgs->deviceId],\n");
	fprintf(fw, "\t\t\t\t\t\t\tprop,\n");
	fprintf(fw, "\t\t\t\t\t\t\t&status);\n");
	fprintf(fw, "\t\tCHECK_OPENCL_ERROR(status, \"clCreateCommandQueue failed.\");\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Set Presistent memory only for AMD platform\n");
	fprintf(fw, "\tcl_mem_flags inMemFlags = CL_MEM_READ_ONLY;\n");
	fprintf(fw, "\tif(sampleArgs->isAmdPlatform())\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tinMemFlags |= CL_MEM_USE_PERSISTENT_MEM_AMD;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	//FIXME: do the followings in a loop and create a new function that works on only one input >>>>>>
	fprintf(fw, "\t// Create buffer for matrix A\n");
	fprintf(fw, "\tinputBuffer0 = clCreateBuffer(\n");
	fprintf(fw, "\t\t\t\t\t\tcontext,\n");
	fprintf(fw, "\t\t\t\t\t\tinMemFlags,\n");
	fprintf(fw, "\t\t\t\t\t\tsizeof(cl_float) * width0 * height0,\n");
	fprintf(fw, "\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\t&status);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clCreateBuffer failed. (inputBuffer0)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Create buffer for matrix B\n");
	fprintf(fw, "\tinputBuffer1 = clCreateBuffer(\n");
	fprintf(fw, "\t\t\t\t\t\tcontext,\n");
	fprintf(fw, "\t\t\t\t\t\tinMemFlags,\n");
	fprintf(fw, "\t\t\t\t\t\tsizeof(cl_float) * width1 * height1,\n");
	fprintf(fw, "\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\t&status);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clCreateBuffer failed. (inputBuffer1)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\toutputBuffer = clCreateBuffer(\n");
	fprintf(fw, "\t\t\t\t\t\tcontext,\n");
	fprintf(fw, "\t\t\t\t\t\tCL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR,\n");
	fprintf(fw, "\t\t\t\t\t\tsizeof(cl_float) * height0 * width1,\n");
	fprintf(fw, "\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\t&status);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clCreateBuffer failed. (outputBuffer)\");\n");
	fprintf(fw, "\t\n");
	//FIXME: <<<<<<
	fprintf(fw, "\t// create a CL program using the kernel source\n");
	fprintf(fw, "\tbuildProgramData buildData;\n");
	fprintf(fw, "\tbuildData.kernelName = std::string(\"%s_Kernels.cl\");\n", className);
	fprintf(fw, "\tbuildData.devices = devices;\n");
	fprintf(fw, "\tbuildData.deviceId = sampleArgs->deviceId;\n");
	fprintf(fw, "\tbuildData.flagsStr = std::string(\"\");\n");
	fprintf(fw, "\tif(sampleArgs->isLoadBinaryEnabled())\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tbuildData.binaryName = std::string(sampleArgs->loadBinary.c_str());\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(sampleArgs->isComplierFlagsSpecified())\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tbuildData.flagsFileName = std::string(sampleArgs->flags.c_str());\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tretValue = buildOpenCLProgram(program, context, buildData);\n");
	fprintf(fw, "\tCHECK_ERROR(retValue, SDK_SUCCESS, \"buildOpenCLProgram() failed\");\n");
	fprintf(fw, "\t\n");
	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
		fprintf(fw, "\t// If local memory is present then use the specific kernel\n");
		fprintf(fw, "\tif(lds%d)\n", i);
		fprintf(fw, "\t{\n");
		fprintf(fw, "\t\tkernel%d = clCreateKernel(program, \"syntheticKernel_local%d\", &status);\n", i, i);
		fprintf(fw, "\t}\n");
		fprintf(fw, "\telse\n");
		fprintf(fw, "\t{\n");
		fprintf(fw, "\t\tkernel%d = clCreateKernel(program, \"syntheticKernel%d\", &status);\n", i, i);
		fprintf(fw, "\t}\n");
		fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clCreateKernel failed.\");\n");
	}
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genSetWorkGroupSizeFunction(FILE *fw, char *className, int kernelId) {

	fprintf(fw, "int\n");
	fprintf(fw, "%s::setWorkGroupSize%d()\n", className, kernelId);
	fprintf(fw, "{\n");
	fprintf(fw, "\t/*\n");
	fprintf(fw, "\t * Kernel runs over complete output matrix with blocks of blockSize x blockSize\n");
	fprintf(fw, "\t * running concurrently\n");
	fprintf(fw, "\t */\n");
	fprintf(fw, "\tcl_int status = 0;\n");
	if (procs[0]->gpuExt.NDRangeData[kernelId].workDim == 2) {
		fprintf(fw, "\tglobalThreads%d[0] = %d;\n", kernelId, procs[0]->gpuExt.NDRangeData[kernelId].globalSizes[0]);
		fprintf(fw, "\tglobalThreads%d[1] = %d;\n", kernelId, procs[0]->gpuExt.NDRangeData[kernelId].globalSizes[1]);
		fprintf(fw, "\tlocalThreads%d[0] = %d;\n", kernelId, procs[0]->gpuExt.NDRangeData[kernelId].localSizes[0]);
		fprintf(fw, "\tlocalThreads%d[1] = %d;\n", kernelId, procs[0]->gpuExt.NDRangeData[kernelId].localSizes[1]);
	} else {
		fprintf(fw, "\tglobalThreads%d[0] = %d;\n", kernelId, procs[0]->gpuExt.NDRangeData[kernelId].globalSizes[0]);
		fprintf(fw, "\tlocalThreads%d[0] = %d;\n", kernelId, procs[0]->gpuExt.NDRangeData[kernelId].localSizes[0]);
	}
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Setting the KernelWorkGroupInfo values\n");
	fprintf(fw, "\tstatus = kernelInfo%d.setKernelWorkGroupInfo(kernel%d,\n", kernelId, kernelId);
	fprintf(fw, "\t\t\t\tdevices[sampleArgs->deviceId]);\n");
	fprintf(fw, "\tCHECK_ERROR(status,0, \"setKernelWrkGroupInfo failed\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tavailableLocalMemory = deviceInfo.localMemSize - kernelInfo%d.localMemoryUsed;\n", kernelId);
	fprintf(fw, "\tneededLocalMemory    = 2 * blockSize * blockSize * sizeof(cl_float);\n");
	fprintf(fw, "\tif(neededLocalMemory > availableLocalMemory)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tstd::cout << \"Unsupported: Insufficient local memory on device.\" << std::endl;\n");
	fprintf(fw, "\t\treturn SDK_SUCCESS;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	if (procs[0]->gpuExt.NDRangeData[kernelId].workDim == 2) {
		fprintf(fw, "\tif((cl_uint)(localThreads%d[0] * localThreads%d[1]) >\n", kernelId, kernelId);
		fprintf(fw, "\t\t\tkernelInfo%d.kernelWorkGroupSize)\n", kernelId);
		fprintf(fw, "\t{\n");
		fprintf(fw, "\t\tif(kernelInfo%d.kernelWorkGroupSize >= 64)\n", kernelId);
		fprintf(fw, "\t\t{\n");
		fprintf(fw, "\t\t\tblockSize = 8;\n");
		fprintf(fw, "\t\t\tlocalThreads%d[0] = blockSize;\n", kernelId);
		fprintf(fw, "\t\t\tlocalThreads%d[1] = blockSize;\n", kernelId);
		fprintf(fw, "\t\t}\n");
		fprintf(fw, "\t\telse if(kernelInfo%d.kernelWorkGroupSize >= 32)\n", kernelId);
		fprintf(fw, "\t\t{\n");
		fprintf(fw, "\t\t\tblockSize = 4;\n");
		fprintf(fw, "\t\t\tlocalThreads%d[0] = blockSize;\n", kernelId);
		fprintf(fw, "\t\t\tlocalThreads%d[1] = blockSize;\n", kernelId);
		fprintf(fw, "\t\t}\n");
		fprintf(fw, "\t\telse\n");
		fprintf(fw, "\t\t{\n");
		fprintf(fw, "\t\t\tstd::cout << \"Out of Resources!\" << std::endl;\n");
		fprintf(fw, "\t\t\tstd::cout << \"Group Size specified : \" << localThreads%d[0] * localThreads%d[1] <<\n", kernelId, kernelId);
		fprintf(fw, "\t\t\t\t\t\tstd::endl;\n");
		fprintf(fw, "\t\t\tstd::cout << \"Max Group Size supported on the kernel : \"\n");
		fprintf(fw, "\t\t\t\t\t\t<< kernelInfo%d.kernelWorkGroupSize << std::endl;\n", kernelId);
		fprintf(fw, "\t\t\treturn SDK_FAILURE;\n");
		fprintf(fw, "\t\t}\n");
		fprintf(fw, "\t}\n");
		fprintf(fw, "\t\n");
		fprintf(fw, "\tif(localThreads%d[0] > deviceInfo.maxWorkItemSizes[0] ||\n", kernelId);
		fprintf(fw, "\t\t\tlocalThreads%d[1] > deviceInfo.maxWorkItemSizes[1] ||\n", kernelId);
		fprintf(fw, "\t\t\tlocalThreads%d[0] * localThreads%d[1] > deviceInfo.maxWorkGroupSize)\n", kernelId, kernelId);
		fprintf(fw, "\t{\n");
		fprintf(fw, "\t\tstd::cout <<\n");
		fprintf(fw, "\t\t\t\t\t\"Unsupported: Device does not support requested number of work items.\" <<\n");
		fprintf(fw, "\t\t\t\t\tstd::endl;\n");
		fprintf(fw, "\t\treturn SDK_FAILURE;\n");
		fprintf(fw, "\t}\n");
	}
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genRunCLKernelsFunction(FILE *fw, char *className, int kernelId) {

	fprintf(fw, "int\n");
	fprintf(fw, "%s::runCLKernels%d(void)\n", className, kernelId);
	fprintf(fw, "{\n");
	fprintf(fw, "\tcl_int   status;\n");
	fprintf(fw, "\tstatus = setWorkGroupSize%d();\n",kernelId);
	fprintf(fw, "\tCHECK_ERROR(status, SDK_SUCCESS, \"getWorkGroupSize%d() failed\");\n", kernelId);
	fprintf(fw, "\t\n");
	fprintf(fw, "\tcl_event ndrEvt;\n");
	fprintf(fw, "\tcl_int eventStatus = CL_QUEUED;\n");
	fprintf(fw, "\t\n");
	//FIXME: update the following codes according to inputs
	fprintf(fw, "\t// Set input data to matrix A and matrix B\n");
	fprintf(fw, "\tcl_event inMapEvt1, inMapEvt2, inUnmapEvt1, inUnmapEvt2, outMapEvt, outUnmapEvt;\n");
	fprintf(fw, "\tvoid* mapPtr1 = clEnqueueMapBuffer(\n");
	fprintf(fw, "\t\t\t\t\t\tcommandQueue,\n");
	fprintf(fw, "\t\t\t\t\t\tinputBuffer0,\n");
	fprintf(fw, "\t\t\t\t\t\tCL_FALSE,\n");
	fprintf(fw, "\t\t\t\t\t\tCL_MAP_WRITE,\n");
	fprintf(fw, "\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\twidth0 * height0 * sizeof(cl_float),\n");
	fprintf(fw, "\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\t\t&inMapEvt1,\n");
	fprintf(fw, "\t\t\t\t\t\t&status);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clEnqueueMapBuffer failed. (inputBuffer0)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tvoid* mapPtr2 = clEnqueueMapBuffer(\n");
	fprintf(fw, "\t\t\t\t\t\tcommandQueue,\n");
	fprintf(fw, "\t\t\t\t\t\tinputBuffer1,\n");
	fprintf(fw, "\t\t\t\t\t\tCL_FALSE,\n");
	fprintf(fw, "\t\t\t\t\t\tCL_MAP_WRITE,\n");
	fprintf(fw, "\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\twidth1 * height1 * sizeof(cl_float),\n");
	fprintf(fw, "\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\t\t&inMapEvt2,\n");
	fprintf(fw, "\t\t\t\t\t\t&status);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clEnqueueMapBuffer failed. (inputBuffer1)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clFlush(commandQueue);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clFlush failed.\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = waitForEventAndRelease(&inMapEvt1);\n");
	fprintf(fw, "\tCHECK_ERROR(status,SDK_SUCCESS, \"WaitForEventAndRelease(inMapEvt1) Failed\");\n");
	fprintf(fw, "\tmemcpy(mapPtr1, input0, sizeof(cl_float) * width0  * height0);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = waitForEventAndRelease(&inMapEvt2);\n");
	fprintf(fw, "\tCHECK_ERROR(status,SDK_SUCCESS, \"WaitForEventAndRelease(inMapEvt2) Failed\");\n");
	fprintf(fw, "\tmemcpy(mapPtr2, input1, sizeof(cl_float) * width1  * height1);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clEnqueueUnmapMemObject(\n");
	fprintf(fw, "\t\t\t\t\tcommandQueue,\n");
	fprintf(fw, "\t\t\t\t\tinputBuffer0,\n");
	fprintf(fw, "\t\t\t\t\tmapPtr1,\n");
	fprintf(fw, "\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\t&inUnmapEvt1);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clEnqueueUnmapMemObject failed. (inputBuffer0)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clEnqueueUnmapMemObject(\n");
	fprintf(fw, "\t\t\t\t\tcommandQueue,\n");
	fprintf(fw, "\t\t\t\t\tinputBuffer1,\n");
	fprintf(fw, "\t\t\t\t\tmapPtr2,\n");
	fprintf(fw, "\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\t&inUnmapEvt2);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clEnqueueUnmapMemObject failed. (inputBuffer1)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clFlush(commandQueue);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clFlush failed.\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = waitForEventAndRelease(&inUnmapEvt1);\n");
	fprintf(fw, "\tCHECK_ERROR(status, SDK_SUCCESS, \"waitForEventAndRelease(inUnmapEvt1) failed\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = waitForEventAndRelease(&inUnmapEvt2);\n");
	fprintf(fw, "\tCHECK_ERROR(status,SDK_SUCCESS, \"waitForEventAndRelease(inUnmapEvt2) failed\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Set appropriate arguments to the kernel\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// output array as the 1st argument : stores product of input0 and input1\n");
	fprintf(fw, "\tstatus = clSetKernelArg(\n");
	fprintf(fw, "\t\t\t\t\tkernel%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\tsizeof(cl_mem),\n");
	fprintf(fw, "\t\t\t\t\t(void *)&inputBuffer0);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clSetKernelArg failed. (inputBuffer0)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// the input matrix  as 2nd argument - input0\n");
	fprintf(fw, "\tstatus = clSetKernelArg(\n");
	fprintf(fw, "\t\t\t\t\tkernel%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\t1,\n");
	fprintf(fw, "\t\t\t\t\tsizeof(cl_mem),\n");
	fprintf(fw, "\t\t\t\t\t(void *)&inputBuffer1);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clSetKernelArg failed. (inputBuffer1)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// the input matrix as 3rd argument - input1\n");
	fprintf(fw, "\tstatus = clSetKernelArg(\n");
	fprintf(fw, "\t\t\t\t\tkernel%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\t2,\n");
	fprintf(fw, "\t\t\t\t\tsizeof(cl_mem),\n");
	fprintf(fw, "\t\t\t\t\t(void *)&outputBuffer);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clSetKernelArg failed. (outputBuffer)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// width0 of the input0 matrix as 4th argument - width0\n");
	fprintf(fw, "\tstatus = clSetKernelArg(\n");
	fprintf(fw, "\t\t\t\t\tkernel%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\t3,\n");
	fprintf(fw, "\t\t\t\t\tsizeof(cl_int),\n");
	fprintf(fw, "\t\t\t\t\t(void*)&width0);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clSetKernelArg failed. (width0)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// width1 of the input1 matrix as 5th argument - width1\n");
	fprintf(fw, "\tstatus = clSetKernelArg(\n");
	fprintf(fw, "\t\t\t\t\tkernel%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\t4,\n");
	fprintf(fw, "\t\t\t\t\tsizeof(cl_int),\n");
	fprintf(fw, "\t\t\t\t\t(void*)&width1);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clSetKernelArg failed. (width1)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Set local memory argument if Scratchpad is available\n");
	fprintf(fw, "\tif(lds%d)\n", kernelId);
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tstatus = clSetKernelArg(\n");
	fprintf(fw, "\t\t\t\t\t\tkernel%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\t\t5,\n");
	fprintf(fw, "\t\t\t\t\t\t(blockSize * 4) * (blockSize * 4) * sizeof(cl_float),\n");
	fprintf(fw, "\t\t\t\t\t\tNULL);\n");
	fprintf(fw, "\t\tCHECK_OPENCL_ERROR(status, \"clSetKernelArg failed. (local memory)\");\n");
	fprintf(fw, "\t}\n");
//	fprintf(fw, "\telse\n");
//	fprintf(fw, "\t{\n");
//	fprintf(fw, "\t\tstatus = clSetKernelArg(kernel, 4, sizeof(cl_int), &width1);\n");
//	fprintf(fw, "\t\tCHECK_OPENCL_ERROR(status, \"clSetKernelArg failed. (width1)\");\n");
//	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Enqueue a kernel run call\n");
	fprintf(fw, "\tstatus = clEnqueueNDRangeKernel(\n");
	fprintf(fw, "\t\t\t\t\tcommandQueue,\n");
	fprintf(fw, "\t\t\t\t\tkernel%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\t%d,\n", procs[0]->gpuExt.NDRangeData[kernelId].workDim);
	fprintf(fw, "\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\tglobalThreads%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\tlocalThreads%d,\n", kernelId);
	fprintf(fw, "\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\t&ndrEvt);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clEnqueueNDRangeKernel failed.\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clFlush(commandQueue);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clFlush failed.\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// wait for the kernel call to finish execution\n");
	fprintf(fw, "\teventStatus = CL_QUEUED;\n");
	fprintf(fw, "\twhile(eventStatus != CL_COMPLETE)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tstatus = clGetEventInfo(\n");
	fprintf(fw, "\t\t\t\t\t\tndrEvt,\n");
	fprintf(fw, "\t\t\t\t\t\tCL_EVENT_COMMAND_EXECUTION_STATUS,\n");
	fprintf(fw, "\t\t\t\t\t\tsizeof(cl_int),\n");
	fprintf(fw, "\t\t\t\t\t\t&eventStatus,\n");
	fprintf(fw, "\t\t\t\t\t\tNULL);\n");
	fprintf(fw, "\t\tCHECK_OPENCL_ERROR(status, \"clGetEventInfo failed.\");\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(!eAppGFLOPS)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\t// Calculate performance\n");
	fprintf(fw, "\t\tcl_ulong startTime;\n");
	fprintf(fw, "\t\tcl_ulong endTime;\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t// Get kernel profiling info\n");
	fprintf(fw, "\t\tstatus = clGetEventProfilingInfo(ndrEvt,\n");
	fprintf(fw, "\t\t\t\t\t\t\t\t\t\t\tCL_PROFILING_COMMAND_START,\n");
	fprintf(fw, "\t\t\t\t\t\t\t\t\t\t\tsizeof(cl_ulong),\n");
	fprintf(fw, "\t\t\t\t\t\t\t\t\t\t\t&startTime,\n");
	fprintf(fw, "\t\t\t\t\t\t\t\t\t\t\t0);\n");
	fprintf(fw, "\t\tCHECK_OPENCL_ERROR(status, \"clGetEventProfilingInfo failed.(startTime)\");\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\tstatus = clGetEventProfilingInfo(ndrEvt,\n");
	fprintf(fw, "\t\t\t\t\t\t\t\t\t\t\tCL_PROFILING_COMMAND_END,\n");
	fprintf(fw, "\t\t\t\t\t\t\t\t\t\t\tsizeof(cl_ulong),\n");
	fprintf(fw, "\t\t\t\t\t\t\t\t\t\t\t&endTime,\n");
	fprintf(fw, "\t\t\t\t\t\t\t\t\t\t\t0);\n");
	fprintf(fw, "\t\tCHECK_OPENCL_ERROR(status, \"clGetEventProfilingInfo failed.(endTime)\");\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t// Print performance numbers\n");
	fprintf(fw, "\t\tdouble sec = 1e-9 * (endTime - startTime);\n");
	fprintf(fw, "\t\tkernelTime += sec;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clReleaseEvent(ndrEvt);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clReleaseEvent failed. (ndrEvt)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tvoid* outMapPtr = clEnqueueMapBuffer(\n");
	fprintf(fw, "\t\t\t\t\t\t\tcommandQueue,\n");
	fprintf(fw, "\t\t\t\t\t\t\toutputBuffer,\n");
	fprintf(fw, "\t\t\t\t\t\t\tCL_FALSE,\n");
	fprintf(fw, "\t\t\t\t\t\t\tCL_MAP_READ,\n");
	fprintf(fw, "\t\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\t\twidth1 * height0 * sizeof(cl_float),\n");
	fprintf(fw, "\t\t\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\t\t\t&outMapEvt,\n");
	fprintf(fw, "\t\t\t\t\t\t\t&status);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clEnqueueMapBuffer failed. (outputBuffer)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clFlush(commandQueue);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clFlush failed.\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = waitForEventAndRelease(&outMapEvt);\n");
	fprintf(fw, "\tCHECK_ERROR(status,0, \"waitForEventAndRelease(outMapEvt) failed\");\n");
	fprintf(fw, "\tmemcpy(output, outMapPtr, sizeof(cl_float) * width1  * height0);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clEnqueueUnmapMemObject(\n");
	fprintf(fw, "\t\t\t\t\tcommandQueue,\n");
	fprintf(fw, "\t\t\t\t\toutputBuffer,\n");
	fprintf(fw, "\t\t\t\t\toutMapPtr,\n");
	fprintf(fw, "\t\t\t\t\t0,\n");
	fprintf(fw, "\t\t\t\t\tNULL,\n");
	fprintf(fw, "\t\t\t\t\t&outUnmapEvt);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clEnqueueUnmapMemObject failed. (outputBuffer)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clFlush(commandQueue);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clFlush failed.\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = waitForEventAndRelease(&outUnmapEvt);\n");
	fprintf(fw, "\tCHECK_ERROR(status,0, \"waitForEventAndRelease(outUnmapEvt) failed\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genInitializeFunction(FILE *fw, char *className) {

	fprintf(fw, "int\n");
	fprintf(fw, "%s::initialize()\n", className);
	fprintf(fw, "{\n");
	fprintf(fw, "\t// Call base class Initialize to get default configuration\n");
	fprintf(fw, "\tif(sampleArgs->initialize() != SDK_SUCCESS)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// add an option for getting blockSize from commandline\n");
	fprintf(fw, "\tOption* xParam = new Option;\n");
	fprintf(fw, "\tCHECK_ALLOCATION(xParam, \"Memory Allocation error.\\n\");\n");
	fprintf(fw, "\txParam->_sVersion = \"x\";\n");
	fprintf(fw, "\txParam->_lVersion = \"width0\";\n");
	fprintf(fw, "\txParam->_description = \"width of matrix A\";\n");
	fprintf(fw, "\txParam->_type     = CA_ARG_INT;\n");
	fprintf(fw, "\txParam->_value    = &w0;\n");
	fprintf(fw, "\tsampleArgs->AddOption(xParam);\n");
	fprintf(fw, "\tdelete xParam;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tOption* yParam = new Option;\n");
	fprintf(fw, "\tCHECK_ALLOCATION(yParam, \"Memory Allocation error.\\n\");\n");
	fprintf(fw, "\tyParam->_sVersion = \"y\";\n");
	fprintf(fw, "\tyParam->_lVersion = \"height0\";\n");
	fprintf(fw, "\tyParam->_description = \"height of matrix A \";\n");
	fprintf(fw, "\tyParam->_type     = CA_ARG_INT;\n");
	fprintf(fw, "\tyParam->_value    = &h0;\n");
	fprintf(fw, "\tsampleArgs->AddOption(yParam);\n");
	fprintf(fw, "\tdelete yParam;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tOption* zParam = new Option;\n");
	fprintf(fw, "\tCHECK_ALLOCATION(zParam, \"Memory Allocation error.\\n\");\n");
	fprintf(fw, "\tzParam->_sVersion = \"z\";\n");
	fprintf(fw, "\tzParam->_lVersion = \"width1\";\n");
	fprintf(fw, "\tzParam->_description = \"width of matrix B\";\n");
	fprintf(fw, "\tzParam->_type     = CA_ARG_INT;\n");
	fprintf(fw, "\tzParam->_value    = &w1;\n");
	fprintf(fw, "\tsampleArgs->AddOption(zParam);\n");
	fprintf(fw, "\tdelete zParam;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tOption* wParam = new Option;\n");
	fprintf(fw, "\tCHECK_ALLOCATION(wParam, \"Memory Allocation error.\\n\");\n");
	fprintf(fw, "\twParam->_sVersion = \"w\";\n");
	fprintf(fw, "\twParam->_lVersion = \"height1\";\n");
	fprintf(fw, "\twParam->_description = \"height of matrix B\";\n");
	fprintf(fw, "\twParam->_type     = CA_ARG_INT;\n");
	fprintf(fw, "\twParam->_value    = &h1;\n");
	fprintf(fw, "\tsampleArgs->AddOption(wParam);\n");
	fprintf(fw, "\tdelete wParam;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tOption* blockSizeParam = new Option;\n");
	fprintf(fw, "\tCHECK_ALLOCATION(blockSizeParam, \"Memory Allocation error.\\n\");\n");
	fprintf(fw, "\tblockSizeParam->_sVersion = \"b\";\n");
	fprintf(fw, "\tblockSizeParam->_lVersion = \"blockSize\";\n");
	fprintf(fw, "\tblockSizeParam->_description =\n");
	fprintf(fw, "\t\t\"Use local memory of dimensions blockSize x blockSize\";\n");
	fprintf(fw, "\tblockSizeParam->_type     = CA_ARG_INT;\n");
	fprintf(fw, "\tblockSizeParam->_value    = &blockSize;\n");
	fprintf(fw, "\tsampleArgs->AddOption(blockSizeParam);\n");
	fprintf(fw, "\tdelete blockSizeParam;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tOption* num_iterations = new Option;\n");
	fprintf(fw, "\tCHECK_ALLOCATION(num_iterations, \"Memory Allocation error.\\n\");\n");
	fprintf(fw, "\tnum_iterations->_sVersion = \"i\";\n");
	fprintf(fw, "\tnum_iterations->_lVersion = \"iterations\";\n");
	fprintf(fw, "\tnum_iterations->_description = \"Number of iterations for kernel execution\";\n");
	fprintf(fw, "\tnum_iterations->_type = CA_ARG_INT;\n");
	fprintf(fw, "\tnum_iterations->_value = &iterations;\n");
	fprintf(fw, "\tsampleArgs->AddOption(num_iterations);\n");
	fprintf(fw, "\tdelete num_iterations;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tOption* appGflops_option = new Option;\n");
	fprintf(fw, "\tCHECK_ALLOCATION(appGflops_option, \"Memory Allocation error.\\n\");\n");
	fprintf(fw, "\tappGflops_option->_sVersion = \"\";\n");
	fprintf(fw, "\tappGflops_option->_lVersion = \"eAppGflops\";\n");
	fprintf(fw, "\tappGflops_option->_description =\n");
	fprintf(fw, "\t\t\"Prints GFLOPS calculated from transfer + kernel time\";\n");
	fprintf(fw, "\tappGflops_option->_type = CA_NO_ARGUMENT;\n");
	fprintf(fw, "\tappGflops_option->_value = &eAppGFLOPS;\n");
	fprintf(fw, "\tsampleArgs->AddOption(appGflops_option);\n");
	fprintf(fw, "\tdelete appGflops_option;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genSetupFunction(FILE *fw, char *className) {

	fprintf(fw, "int\n");
	fprintf(fw, "%s::setup()\n", className);
	fprintf(fw, "{\n");
	fprintf(fw, "\t// Validation of input values\n");
	fprintf(fw, "\tif((w0 == 0) || (h0 == 0) || (w1 == 0) || (h1 == 0))\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tstd::cout << \"Error: Matrix dimensions can not be 0\" << std::endl;\n");
	fprintf(fw, "\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// Make sure the dimensions are multiples of blockSize\n");
	fprintf(fw, "\tconst int vectorSize = 4;\n");
	fprintf(fw, "\tif(w0 %s (blockSize * vectorSize) != 0)\n", "%");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tw0 = (w0 / (blockSize * vectorSize) + 1) * (blockSize * vectorSize);\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(h0 %s (blockSize * vectorSize) != 0)\n", "%");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\th0 = (h0 / (blockSize * vectorSize) + 1) * (blockSize * vectorSize);\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(w1 %s (blockSize * vectorSize) != 0)\n", "%");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tw1 = (w1 / (blockSize * vectorSize) + 1) * (blockSize * vectorSize);\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(h1 %s (blockSize * vectorSize) != 0)\n", "%");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\th1 = (h1 / (blockSize * vectorSize) + 1) * (blockSize * vectorSize);\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	//FIXME: update the following block according to inputs
	fprintf(fw, "\twidth0  = w0;\n");
	fprintf(fw, "\theight0 = h0;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\twidth1  = w1;\n");
	fprintf(fw, "\theight1 = h1;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(setup%s()!=SDK_SUCCESS)\n", className);
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tint timer = sampleTimer->createTimer();\n");
	fprintf(fw, "\tsampleTimer->resetTimer(timer);\n");
	fprintf(fw, "\tsampleTimer->startTimer(timer);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(setupCL()!=SDK_SUCCESS)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tsampleTimer->stopTimer(timer);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tsetupTime = (cl_double)sampleTimer->readTimer(timer);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genRunFunction(FILE *fw, char *className) {

	int i;

	fprintf(fw, "int\n");
	fprintf(fw, "%s::run()\n", className);
	fprintf(fw, "{\n");
//	fprintf(fw, "\t// Warm up\n");
//	fprintf(fw, "\tfor(int i = 0; i < 2 && iterations != 1; i++)\n");
//	fprintf(fw, "\t{\n");
//	fprintf(fw, "\t\t// Arguments are set and execution call is enqueued on command buffer\n");
//	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
//		fprintf(fw, "\t\tif(runCLKernels%d() != SDK_SUCCESS)\n", i);
//		fprintf(fw, "\t\t{\n");
//		fprintf(fw, "\t\t\treturn SDK_FAILURE;\n");
//		fprintf(fw, "\t\t}\n");
//	}
//	fprintf(fw, "\t}\n");
//	fprintf(fw, "\t\n");
	fprintf(fw, "\tint timer = sampleTimer->createTimer();\n");
	fprintf(fw, "\tsampleTimer->resetTimer(timer);\n");
	fprintf(fw, "\tsampleTimer->startTimer(timer);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstd::cout << \"Executing kernel for \" << iterations << \" iterations\" <<\n");
	fprintf(fw, "\t\t\t\tstd::endl;\n");
	fprintf(fw, "\tstd::cout << \"-------------------------------------------\" << std::endl;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tkernelTime = 0;\n");
	fprintf(fw, "\tfor(int i = 0; i < iterations; i++)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\t// Arguments are set and execution call is enqueued on command buffer\n");
	fprintf(fw, "\t\tint kernelRun;\n");
	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
		fprintf(fw, "\t\tkernelRun = runCLKernels%d();\n", i);
		fprintf(fw, "\t\tif(kernelRun != SDK_SUCCESS)\n");
		fprintf(fw, "\t\t{\n");
		fprintf(fw, "\t\t\treturn kernelRun;\n");
		fprintf(fw, "\t\t}\n");
	}
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tsampleTimer->stopTimer(timer);\n");
	fprintf(fw, "\tappTime = (double)(sampleTimer->readTimer(timer)) / iterations;\n");
	fprintf(fw, "\tkernelTime = kernelTime / iterations;\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(!sampleArgs->quiet)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tprintArray<cl_float>(\"Output\", output, width1, 1);\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genPrintStatsFunction(FILE *fw, char *className) {

	fprintf(fw, "void\n");
	fprintf(fw, "%s::printStats()\n", className);
	fprintf(fw, "{\n");
	fprintf(fw, "\tif(sampleArgs->timing)\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\tif(eAppGFLOPS)\n");
	fprintf(fw, "\t\t{\n");
	//FIXME: update the following codes according to inputs
	fprintf(fw, "\t\t\tstd::string strArray[4] = {\"MatrixA\", \"MatrixB\", \"Time(sec)\", \"[Transfer+kernel]Time(sec)\"};\n");
	fprintf(fw, "\t\t\tstd::string stats[4];\n");
	fprintf(fw, "\t\t\t\n");
	fprintf(fw, "\t\t\tdouble flops = 2 * width0 * width1;\n");
	fprintf(fw, "\t\t\tdouble perf = (flops / appTime) * height0 * 1e-9;\n");
	fprintf(fw, "\t\t\tif(sampleArgs->timing)\n");
	fprintf(fw, "\t\t\t{\n");
	fprintf(fw, "\t\t\t\tstd::cout << \"GFlops achieved : \" << perf << std::endl << std::endl;\n");
	fprintf(fw, "\t\t\t}\n");
	fprintf(fw, "\t\t\t\n");
	fprintf(fw, "\t\t\tsampleTimer->totalTime = setupTime + appTime;\n");
	fprintf(fw, "\t\t\t\n");
	fprintf(fw, "\t\t\tstats[0]  = toString(height0, std::dec)\n");
	fprintf(fw, "\t\t\t\t\t\t+\"x\"+toString(width0, std::dec);\n");
	fprintf(fw, "\t\t\tstats[1]  = toString(height1, std::dec)\n");
	fprintf(fw, "\t\t\t\t\t\t+\"x\"+toString(width1, std::dec);\n");
	fprintf(fw, "\t\t\tstats[2]  = toString(sampleTimer->totalTime, std::dec);\n");
	fprintf(fw, "\t\t\tstats[3]  = toString(appTime, std::dec);\n");
	fprintf(fw, "\t\t\t\n");
	fprintf(fw, "\t\t\tprintStatistics(strArray, stats, 4);\n");
	fprintf(fw, "\t\t}\n");
	fprintf(fw, "\t\telse\n");
	fprintf(fw, "\t\t{\n");
	fprintf(fw, "\t\t\tstd::string strArray[4] = {\"MatrixA\", \"MatrixB\", \"Time(sec)\", \"kernelTime(sec)\"};\n");
	fprintf(fw, "\t\t\tstd::string stats[4];\n");
	fprintf(fw, "\t\t\t\n");
	fprintf(fw, "\t\t\tdouble flops = 2 * width0 * width1;\n");
	fprintf(fw, "\t\t\tdouble perf = (flops / kernelTime) * height0 * 1e-9;\n");
	fprintf(fw, "\t\t\tif(sampleArgs->timing)\n");
	fprintf(fw, "\t\t\t{\n");
	fprintf(fw, "\t\t\t\tstd::cout << \"GFlops achieved : \" << perf << std::endl << std::endl;\n");
	fprintf(fw, "\t\t\t}\n");
	fprintf(fw, "\t\t\t\n");
	fprintf(fw, "\t\t\tsampleTimer->totalTime = setupTime + kernelTime;\n");
	fprintf(fw, "\t\t\t\n");
	fprintf(fw, "\t\t\tstats[0]  = toString(height0, std::dec)\n");
	fprintf(fw, "\t\t\t\t\t\t+\"x\"+toString(width0, std::dec);\n");
	fprintf(fw, "\t\t\tstats[1]  = toString(height1, std::dec)\n");
	fprintf(fw, "\t\t\t\t\t\t+\"x\"+toString(width1, std::dec);\n");
	fprintf(fw, "\t\t\tstats[2]  = toString(sampleTimer->totalTime, std::dec);\n");
	fprintf(fw, "\t\t\tstats[3]  = toString(kernelTime, std::dec);\n");
	fprintf(fw, "\t\t\t\n");
	fprintf(fw, "\t\t\tprintStatistics(strArray, stats, 4);\n");
	fprintf(fw, "\t\t}\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genCleanupFunction(FILE *fw, char *className) {

	int i;

	fprintf(fw, "int\n");
	fprintf(fw, "%s::cleanup()\n", className);
	fprintf(fw, "{\n");
	fprintf(fw, "\t// Releases OpenCL resources (Context, Memory etc.)\n");
	fprintf(fw, "\tcl_int status;\n");
	fprintf(fw, "\t\n");
	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
		fprintf(fw, "\tstatus = clReleaseKernel(kernel%d);\n", i);
		fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clReleaseKernel failed.(kernel%d)\");\n", i);
		fprintf(fw, "\t\n");
	}
	fprintf(fw, "\tstatus = clReleaseProgram(program);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clReleaseProgram failed.(program)\");\n");
	fprintf(fw, "\t\n");
	//FIXME: update the following codes according to inputs
	fprintf(fw, "\tstatus = clReleaseMemObject(inputBuffer0);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clReleaseMemObject failed.(inputBuffer0)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clReleaseMemObject(inputBuffer1);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clReleaseMemObject failed.(inputBuffer1)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clReleaseMemObject(outputBuffer);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clReleaseMemObject failed.(outputBuffer)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clReleaseCommandQueue(commandQueue);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clReleaseCommandQueue failed.(commandQueue)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tstatus = clReleaseContext(context);\n");
	fprintf(fw, "\tCHECK_OPENCL_ERROR(status, \"clReleaseContext failed.(context)\");\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\t// release program resources (input memory etc.)\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tFREE(input0);\n");
	fprintf(fw, "\tFREE(input1);\n");
	fprintf(fw, "\tFREE(output);\n");
	fprintf(fw, "\tFREE(verificationOutput);\n");
	fprintf(fw, "\tFREE(devices);\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");
	fprintf(fw, "\n");

	return;
}

void genMainFunction(FILE *fw, char *className) {

	fprintf(fw, "int\n");
	fprintf(fw, "main(int argc, char * argv[])\n");
	fprintf(fw, "{\n");
	fprintf(fw, "\t%s cl%s;\n", className, className);
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(cl%s.initialize() != SDK_SUCCESS)\n", className);
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(cl%s.sampleArgs->parseCommandLine(argc, argv) != SDK_SUCCESS)\n", className);
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\tif(cl%s.sampleArgs->isDumpBinaryEnabled() != SDK_SUCCESS)\n", className);
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\treturn cl%s.genBinaryImage();\n", className);
	fprintf(fw, "\t}\n");
	fprintf(fw, "\telse\n");
	fprintf(fw, "\t{\n");
	fprintf(fw, "\t\t// Setup\n");
	fprintf(fw, "\t\tif(cl%s.setup() != SDK_SUCCESS)\n", className);
	fprintf(fw, "\t\t{\n");
	fprintf(fw, "\t\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t\t}\n");
	fprintf(fw, "\t\t// Run\n");
	fprintf(fw, "\t\tif(cl%s.run() != SDK_SUCCESS)\n", className);
	fprintf(fw, "\t\t{\n");
	fprintf(fw, "\t\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t\t}\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\t// Cleanup\n");
	fprintf(fw, "\t\tif(cl%s.cleanup() != SDK_SUCCESS)\n", className);
	fprintf(fw, "\t\t{\n");
	fprintf(fw, "\t\t\treturn SDK_FAILURE;\n");
	fprintf(fw, "\t\t}\n");
	fprintf(fw, "\t\t\n");
	fprintf(fw, "\t\tcl%s.printStats();\n", className);
	fprintf(fw, "\t}\n");
	fprintf(fw, "\t\n");
	fprintf(fw, "\treturn SDK_SUCCESS;\n");
	fprintf(fw, "}\n");

	return;
}

int genGpuSourceFile(FILE *fw, char *className, char* headerName) {

	int i;

	fprintf(fw, "%s", copyright);
	fprintf(fw, "#include \"%s\"\n", headerName);
	fprintf(fw, "\n");

	genSetupClassFunction(fw, className);
	genGenBinaryImageFunction(fw, className);
	genSetupCLFunction(fw, className);

	for (i = 0; i < procs[0]->gpuExt.kernelCount; i++) {
		genSetWorkGroupSizeFunction(fw, className, i);
		genRunCLKernelsFunction(fw, className, i);
	}

	genInitializeFunction(fw, className);
	genSetupFunction(fw, className);
	genRunFunction(fw, className);
	genPrintStatsFunction(fw, className);
	genCleanupFunction(fw, className);
	genMainFunction(fw, className);

	return 0;
}

int genGpuKernelFile(FILE *fw) {

	int i, kernelId;

	fprintf(fw, "%s", copyright);

	for (kernelId = 0; kernelId < procs[0]->gpuExt.kernelCount; kernelId++) {

		if (updateDataGpu[kernelId].mclCount == 0 && updateDataGpu[kernelId].lmrCount == 0) {
			fprintf(fw, "__kernel void syntheticKernel%d(__global float4 *matrixA,\n", kernelId);
		} else {
			fprintf(fw, "__kernel void syntheticKernel_local%d(__global float4 *matrixA,\n", kernelId);
		}
		fprintf(fw, "\t\t\t\t\t\t\t\t__global float4 *matrixB,\n");
		fprintf(fw, "\t\t\t\t\t\t\t\t__global float4 *matrixC,\n");
		if (updateDataGpu[kernelId].mclCount == 0 && updateDataGpu[kernelId].lmrCount == 0) {
			fprintf(fw, "\t\t\t\t\t\t\t\tuint widthA, uint widthB)\n");
		} else {
			fprintf(fw, "\t\t\t\t\t\t\t\tuint widthA, uint widthB,\n");
			fprintf(fw, "\t\t\t\t\t\t\t\t__local float4 *blockA)\n");
		}
		fprintf(fw, "{\n");

		/* IPC */
		if (updateDataGpu[0].ipcCount > 0) {
			if (updateDataGpu[kernelId].isIpcInc) {
				fprintf(fw, "\t\t/* Code block to increment IPC */\n");
				fprintf(fw, "\t\tint ipcI;\n");
				fprintf(fw, "\t\tfor (ipcI = 0; ipcI < 1; ipcI++)\n");
				fprintf(fw, "\t\t\tmatrixC[0] = 1.2;\n");
				fprintf(fw, "\t\t\n");
			} else {
				fprintf(fw, "\t\t/* Code block to decrement IPC */\n");
				if (updateDataGpu[kernelId].ipcCount == 1) {
					fprintf(fw, "\t\tint xValue = get_global_id(0);\n");
					fprintf(fw, "\t\tint yValue = get_global_id(1);\n");
					fprintf(fw, "\t\tfloat4 ipc1 = matrixA[yValue * widthA + xValue];\n");
					fprintf(fw, "\t\tmatrixA[0] = ipc1;\n");
				} else {
					fprintf(fw, "\t\tfloat ipc1 = 1, ipc2 = 2;\n");
					for (i = 0; i < updateDataGpu[kernelId].ipcCount; i++) {
						fprintf(fw, "\t\tmatrixA[%d] = ipc1 + ipc2;\n", i * 4 + 1);
						fprintf(fw, "\t\tmem_fence(CLK_GLOBAL_MEM_FENCE);\n");
					}
				}
				fprintf(fw, "\t\t\n");
			}
		}

		/* Reg */
		if (updateDataGpu[kernelId].regCount > 0) {
			if (updateDataGpu[kernelId].isRegInc) {
				//TODO: ??
				fprintf(fw, "\t\t/* Code block to increment Reg */\n");
				fprintf(fw, "\t\t/* NOT IMPLEMENTED! */\n");
				fprintf(fw, "\t\t\n");
			} else {
				//TODO: ??
				fprintf(fw, "\t\t/* Code block to decrement Reg */\n");
				fprintf(fw, "\t\t/* NOT IMPLEMENTED! */\n");
				fprintf(fw, "\t\t\n");
			}
		}

		/* LcM */
		if (updateDataGpu[kernelId].lcmCount > 0) {
			if (updateDataGpu[kernelId].isLcmInc) {
				fprintf(fw, "\t\t/* Code block to increment LcM */\n");
				fprintf(fw, "\t\t__local float localds[%d];\n", updateDataGpu[kernelId].lcmCount);
				fprintf(fw, "\t\t\n");
			} else {
				fprintf(fw, "\t\t/* Code block to decrement LcM */\n");
				fprintf(fw, "\t\t__local float localds[%d];\n", updateDataGpu[kernelId].lcmCount);
				fprintf(fw, "\t\t\n");
			}
		}

		/* CMR */
		if (updateDataGpu[kernelId].cmrCount > 0) {
			if (updateDataGpu[kernelId].isCmrInc) {
				// add communication operation: LDSInstructions, scalarMemInstructions, vectorMemInstructions
				fprintf(fw, "\t\t/* Code block to increment CMR */\n");
				fprintf(fw, "\t\tfloat4 cmr1 = matrixA[0];\n");
				fprintf(fw, "\t\tmatrixC[0] = cmr1");
				for (i = 1; i < updateDataGpu[kernelId].cmrCount; i++) {
					fprintf(fw, " + cmr1");
				}
				fprintf(fw, ";\n");
				fprintf(fw, "\t\t\n");
			} else {
				// add computation operation: branchInstructions, scalarALUInstructions, vectorALUInstructions
				if (updateDataGpu[kernelId].cmrCount == 1) {
					fprintf(fw, "\t\t/* Code block to decrement CMR */\n");
					fprintf(fw, "\t\tfloat4 cmr1, cmr2;\n");
					fprintf(fw, "\t\tint cmri;\n");
					fprintf(fw, "\t\tfor (cmri = 0; cmri < %u; cmri++) {\n", updateDataGpu[kernelId].cmrCount);
					fprintf(fw, "\t\t\tcmr1 = cmr2;\n");
					fprintf(fw, "\t\t}\n");
					fprintf(fw, "\t\tmatrixC[0] = cmr1;\n");
					fprintf(fw, "\t\t\n");
				} else {
					fprintf(fw, "\t\t/* Code block to decrement CMR */\n");
					fprintf(fw, "\t\tfloat4 cmr1, cmr2;\n");
					fprintf(fw, "\t\tint cmri;\n");
					fprintf(fw, "\t\tfor (cmri = 0; cmri < %u; cmri++) {\n", updateDataGpu[kernelId].cmrCount);
					fprintf(fw, "\t\t\tcmr1 = cmr1 + cmr2;\n");
					fprintf(fw, "\t\t}\n");
					fprintf(fw, "\t\tmatrixC[0] = cmr1;\n");
					fprintf(fw, "\t\t\n");
				}
			}
		}

		/* GMR */
		if (updateDataGpu[kernelId].gmrCount > 0) {
			if (updateDataGpu[kernelId].isGmrInc) {
				fprintf(fw, "\t\t/* Code block to increment GMR */\n");
				for (i = 0; i < updateDataGpu[kernelId].gmrCount; i++) {
					fprintf(fw, "\t\tmatrixC[%d] = matrixA[%d] + matrixB[%d];\n", i, i, i);
				}
				fprintf(fw, "\t\t\n");
			} else {
				fprintf(fw, "\t\t/* Code block to decrement GMR */\n");
				fprintf(fw, "\t\tfloat4 gmr1 = matrixA[0];\n");
				for (i = 0; i < updateDataGpu[kernelId].gmrCount; i++) {
					fprintf(fw, "\t\tfloat4 gmr%d = gmr%d + 2;\n", i + 2, i + 1);
				}
				fprintf(fw, "\t\tmatrixC[1] = gmr%d;\n", i + 1);
				fprintf(fw, "\t\t\n");
			}
		}

		/* LMR */
		if (updateDataGpu[kernelId].lmrCount > 0) {
			if (updateDataGpu[kernelId].isLmrInc) {
				//TODO: ??
				fprintf(fw, "\t\t/* Code block to increment LMR */\n");
				fprintf(fw, "\t\tint lmri;\n");
				fprintf(fw, "\t\tfor (lmri = 0; lmri < %u; lmri++) {\n", updateDataGpu[kernelId].lmrCount);
				fprintf(fw, "\t\t\tblockA[10] += blockA[11];\n");
				fprintf(fw, "\t\t}\n");
				fprintf(fw, "\t\t\n");
			} else {
				//TODO: ??
				fprintf(fw, "\t\t/* Code block to decrement LMR */\n");
				fprintf(fw, "\t\t/* NOT IMPLEMENTED! */\n");
				fprintf(fw, "\t\t\n");
			}
		}

		/* PMR */
		if (updateDataGpu[kernelId].pmrCount > 0) {
			if (updateDataGpu[kernelId].isPmrInc) {
				fprintf(fw, "\t\t/* Code block to increment PMR */\n");
				if (updateDataGpu[kernelId].pmrCount == 1) {
					fprintf(fw, "\t\tfloat4 pmr1, pmr2;\n");
					fprintf(fw, "\t\tmatrixC[0] = pmr1 + pmr2;\n");
				} else {
					fprintf(fw, "\t\tfloat4 pmr1 = matrixA[0];\n");
					fprintf(fw, "\t\tfloat4 pmr2 = matrixB[0];\n");
					fprintf(fw, "\t\tmatrixC[0] = pmr1 + pmr2;\n");
				}
				fprintf(fw, "\t\t\n");
			} else {
				//TODO: ??
				fprintf(fw, "\t\t/* Code block to decrement PMR */\n");
				fprintf(fw, "\t\t/* NOT IMPLEMENTED! */\n");
				fprintf(fw, "\t\t\n");
			}
		}

		/* MCl */
		if (updateDataGpu[kernelId].mclCount > 0) {
			if (updateDataGpu[kernelId].isMclInc) {
				fprintf(fw, "\t\t/* Code block to increment MCl */\n");
				if (updateDataGpu[kernelId].mclCount == 1) {
//					fprintf(fw, "\t\tblockA[0] = 1.2;\n");
					fprintf(fw, "\t\tmatrixC[0] = blockA[0];\n");
				} else if (updateDataGpu[kernelId].mclCount == 2) {
					fprintf(fw, "\t\tsize_t tid = 2 * get_global_id(0);\n");
					if (procs[0]->gpuExt.NDRangeData[kernelId].globalSizes[0] *
							procs[0]->gpuExt.NDRangeData[kernelId].globalSizes[1] < 256) {
						fprintf(fw, "\t\tblockA[tid] = 1.2;\n");
						fprintf(fw, "\t\tmatrixC[0] = blockA[tid];\n");
					} else {
						fprintf(fw, "\t\tif (tid < widthA) {\n");
						fprintf(fw, "\t\t\tblockA[tid] = 1.2;\n");
						fprintf(fw, "\t\t\tmatrixC[0] = blockA[tid];\n");
						fprintf(fw, "\t\t}\n");
					}
				} else if (updateDataGpu[kernelId].mclCount == 3) {
					fprintf(fw, "\t\tsize_t tid = 3 * get_global_id(0);\n");
					fprintf(fw, "\t\tif (tid < 32) {\n");
					fprintf(fw, "\t\t\tblockA[tid] = 1.2;\n");
					fprintf(fw, "\t\t\tmatrixC[0] = blockA[tid];\n");
					fprintf(fw, "\t\t}\n");
				} else {
					if (updateDataGpu[kernelId].lcmCount > 0) {
						fprintf(fw, "\t\tsize_t tid = get_local_id(0);\n");
						fprintf(fw, "\t\tint mcl1;\n");
						fprintf(fw, "\t\tfloat mcl2;\n");
						fprintf(fw, "\t\tfor (mcl1 = 8; mcl1 >= 0; mcl1--) {\n");
						fprintf(fw, "\t\t\tlocalds[16 * tid + mcl1] = 0;\n");
						fprintf(fw, "\t\t\tlocalds[16 * tid] = 0;\n");
						fprintf(fw, "\t\t}\n");
						fprintf(fw, "\t\tmcl2 = localds[16 * tid];\n");
						fprintf(fw, "\t\tmcl1 = mcl2 / 16;\n");
						fprintf(fw, "\t\tmatrixC[0] = localds[16 * tid + mcl1];\n");
						fprintf(fw, "\t\tmatrixC[1] = 1;\n");
						fprintf(fw, "\t\tmatrixC[2] = 2;\n");
					} else {
						fprintf(fw, "\t\tsize_t tid = 4 * get_global_id(0);\n");
						fprintf(fw, "\t\tif (tid < 32) {\n");
						fprintf(fw, "\t\t\tblockA[tid] = 1.2;\n");
						fprintf(fw, "\t\t\tmatrixC[0] = blockA[tid];\n");
						fprintf(fw, "\t\t}\n");
					}
				}
				fprintf(fw, "\t\t\n");
			} else {
				//TODO: ??
				fprintf(fw, "\t\t/* Code block to decrement MCl */\n");
				fprintf(fw, "\t\t/* NOT IMPLEMENTED! */\n");
				fprintf(fw, "\t\t\n");
			}
		}

		/* HtR */
		if (updateDataGpu[kernelId].htrCount > 0) {
			if (updateDataGpu[kernelId].isHtrInc) {
				//TODO: ??
				fprintf(fw, "\t\t/* Code block to increment HtR */\n");
				fprintf(fw, "\t\t/* NOT IMPLEMENTED! */\n");
				fprintf(fw, "\t\t\n");
			} else {
				//TODO: ??
				fprintf(fw, "\t\t/* Code block to decrement HtR */\n");
				fprintf(fw, "\t\t/* NOT IMPLEMENTED! */\n");
				fprintf(fw, "\t\t\n");
			}
		}

		fprintf(fw, "}\n\n");

	}

	return 0;
}

int genBenchmarkGpu(int phaseCount, paralPattern pp[MAX_GROUP_NUMBER]) {

	FILE *fwHeader, *fwSource, *fwKernel;
	char name[MAX_PATH] = { '\0' }, fileName[MAX_PATH];
	char className[MAX_PATH] = { '\0' };
	char headerName[MAX_PATH] = { '\0' };

	activeProc = procs[0];

	strcpy(name, getBaseName(activeProc->name));
	strcpy(className, name);
	strcat(className, "_syn");
	strcpy(headerName, name);
	strcat(headerName, "_syn.hpp");

	// header file
	memset(fileName, 0, MAX_PATH);
	strcpy(fileName, name);
	strcat(fileName, "_syn.hpp");

	fwHeader = fopen(fileName, "w");
	if (fwHeader == NULL) {
		printf("ERROR! Cannot generate %s.\n", name);
		return -1;
	}

	// source file
	memset(fileName, 0, MAX_PATH);
	strcpy(fileName, name);
	strcat(fileName, "_syn.cpp");

	fwSource = fopen(fileName, "w");
	if (fwSource == NULL) {
		printf("ERROR! Cannot generate %s.\n", name);
		return -1;
	}

	// kernel file
	memset(fileName, 0, MAX_PATH);
	strcpy(fileName, name);
	strcat(fileName, "_syn_Kernels.cl");

	fwKernel = fopen(fileName, "w");
	if (fwKernel == NULL) {
		printf("ERROR! Cannot generate %s.\n", name);
		return -1;
	}

	genGpuHeaderFile(fwHeader, className);
	genGpuSourceFile(fwSource, className, headerName);
	genGpuKernelFile(fwKernel);

	fclose(fwHeader);
	fclose(fwSource);
	fclose(fwKernel);

	return 0;
}
