/*
 * Copyright (c) 2011-2015,
 * Etem Deniz <etem.deniz@boun.edu.tr>, Bogazici University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * benchPrep.c
 *
 *  Created on: Jan 29, 2012
 *      Author: Etem Deniz
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include "benchGen.h"

boolean isStandalone = false;
boolean isMultiThreadSingleTask = true;
boolean hasThreadMrapiExt = false;
boolean hasThreadMcapiExt = false;
boolean hasThreadPosixExt = false;
boolean hasThreadOpenClExt = false;
int codeLevel = 1;	/*0: low, 1: high */
metricLevel simMetricLevel = METRIC_ALL;
boolean isGlobalParamsInit = false;
double dataMigTotalRatio = 0.1;
double mrapiDataSharingRatio = 0.2;
double mcapiDataSharingRatio = 0.004;
double posixDataSharingRatio = 0.5;
double dataSharingRatio = 0.01;
double dataPrivateRatio = 0.01;
double dataReadonlyRatio = 0.01;
double migDataSharingRatio = 0.00001;
double ignoreRatio = 0.1;
struct tUpdateData updateData[MAX_THREADS] = { { 0, false, 0, false, 0, false, 0, false } };
struct tUpdateDataGpu updateDataGpu[MAX_THREADS] = { { 0, false, 0, false, 0, false, 0, false,
		0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false } };
int minDataSizes[MAX_GROUP_NUMBER];
int maxDataSizes[MAX_GROUP_NUMBER];

key_t semKey;
key_t mtxKey;
key_t shmKey;

key_t nextSemKey() {
	key_t key = semKey;
	semKey += 100;
	return key;
}

key_t nextMtxKey() {
	key_t key = mtxKey;
	mtxKey += 100;
	return key;
}

key_t nextShmKey() {
	key_t key = shmKey;
	shmKey += 100;
	return key;
}

int addGlobalMem(int size) {

	char *name = "";
	int count = activeProc->lsExt.numOfGlobMems;

	assert(count < MAX_GLOB_MEM);

	activeProc->lsExt.globMems[count].size = size;
	strcpy(activeProc->lsExt.globMems[count].name, name);
	activeProc->lsExt.numOfGlobMems++;

	return count;
}

int addGlobalMemWithName(int size, int size2, char *name) {

	int i;
	boolean isExists = false;
	int count = activeProc->lsExt.numOfGlobMems;

	assert(count < MAX_GLOB_MEM);

	for (i = 0; i < count; i++) {
		if (strcmp(name, activeProc->lsExt.globMems[i].name) == 0) {
			isExists = true;
			break;
		}
	}

	if (isExists == false) {
		activeProc->lsExt.globMems[count].size = size;
		activeProc->lsExt.globMems[count].size2 = size2;
		strcpy(activeProc->lsExt.globMems[count].name, name);
		activeProc->lsExt.numOfGlobMems++;
	} else {
		count = i;
	}

	return count;
}

int addTlsLocalMem(struct tThreadData *td, int size) {

	int count = td->lsExt.numOfLocalMems;

	assert(count < MAX_LOCAL_MEM);

	td->lsExt.localMems[count].size = size;
	td->lsExt.numOfLocalMems++;

	return count;
}

struct tSem *addMrapiSem(struct tThreadData *td, boolean isOwner, key_t key, boolean isLockAfterCreate) {

	int count = td->mrapiExt.numOfSemaphores;

	assert(count < MAX_SEM);

	td->mrapiExt.sems[count].isOwner = isOwner;
	td->mrapiExt.sems[count].key = key;
	td->mrapiExt.sems[count].isLockAfterCreate = isLockAfterCreate;
	td->mrapiExt.sems[count].isLocked = false;
	td->mrapiExt.numOfSemaphores++;

	return &td->mrapiExt.sems[count];
}

void addMrapiSemBetweenTwo(struct tThreadData *ownerTd, struct tThreadData *td, key_t key, boolean isLockAfterCreate) {

	addMrapiSem(ownerTd, true, key, isLockAfterCreate);
	addMrapiSem(td, false, key, false);

	return;
}

struct tMutex *addMrapiMtx(struct tThreadData *td, boolean isOwner, key_t key) {

	int count = td->mrapiExt.numOfMutexes;

	assert(count < MAX_MUTEX);

	td->mrapiExt.mutexes[count].id = count + 1;
	td->mrapiExt.mutexes[count].isOwner = isOwner;
	td->mrapiExt.mutexes[count].key = key;
	td->mrapiExt.mutexes[count].isLocked = false;
	td->mrapiExt.numOfMutexes++;

	return &td->mrapiExt.mutexes[count];
}

int addMrapiBarrier(struct tThreadData *td, boolean isOwner, key_t mtxKey, int threadCount) {

	int i;
	struct tThreadData *itTd;
	int numOfThreads;
	int count = td->mrapiExt.numOfBarriers;

	assert(count < MAX_BARRIER);

	if (isOwner) {
		activeProc->mrapiExt.barrierCount++;
		threadCount = 1;
		numOfThreads = activeProc->numOfThreads;
		for (i = 2; i <= numOfThreads; i++) {
			itTd = activeProc->threads[i];
			itTd->mrapiExt.hasExt = true;
			if ((itTd != td && itTd->mrapiExt.hasExt) && td->mtapiExt.startRoutine.taskId == itTd->mtapiExt.startRoutine.taskId) {
				threadCount++;
			}
		}
		for (i = 2; i <= numOfThreads; i++) {
			itTd = activeProc->threads[i];
			if ((itTd != td && itTd->mrapiExt.hasExt) && td->mtapiExt.startRoutine.taskId == itTd->mtapiExt.startRoutine.taskId) {
				addMrapiBarrier(itTd, false, mtxKey, threadCount);
			}
		}
	}

	td->mrapiExt.barriers[count].id = activeProc->mrapiExt.barrierCount;
	td->mrapiExt.barriers[count].isOwner = isOwner;
	td->mrapiExt.barriers[count].pMutex = addMrapiMtx(td, isOwner, mtxKey);
	td->mrapiExt.barriers[count].numOfThreads = threadCount;

	return activeProc->mrapiExt.barrierCount;
}

int addMrapiBarrierBetweenTwo(struct tThreadData *itd, struct tThreadData *jtd, key_t mtxKey) {

	int count;
	int id = activeProc->mrapiExt.barrierCount;

	count = itd->mrapiExt.numOfBarriers;
	assert(count < MAX_BARRIER);
	itd->mrapiExt.barriers[count].id = id + 1;
	itd->mrapiExt.barriers[count].isOwner = true;
	itd->mrapiExt.barriers[count].pMutex = addMrapiMtx(itd, true, mtxKey);
	itd->mrapiExt.barriers[count].numOfThreads = 2;
	itd->mrapiExt.numOfBarriers++;

	count = jtd->mrapiExt.numOfBarriers;
	assert(count < MAX_BARRIER);
	jtd->mrapiExt.barriers[count].id = id + 1;
	jtd->mrapiExt.barriers[count].isOwner = false;
	jtd->mrapiExt.barriers[count].pMutex = addMrapiMtx(jtd, false, mtxKey);
	jtd->mrapiExt.barriers[count].numOfThreads = 2;
	jtd->mrapiExt.numOfBarriers++;

	activeProc->mrapiExt.barrierCount++;

	return id;
}

int addMrapiShm(struct tThreadData *td, int size, boolean isOwner, key_t shmKey, key_t mtxKey) {

	int count = td->mrapiExt.numOfSharedMems;

	assert(count < MAX_SHARED_MEM);

	td->mrapiExt.sharedMems[count].size = size;
	td->mrapiExt.sharedMems[count].isOwner = isOwner;
	td->mrapiExt.sharedMems[count].key = shmKey;
	td->mrapiExt.sharedMems[count].pMutex = addMrapiMtx(td, isOwner, mtxKey);
	td->mrapiExt.numOfSharedMems++;

	return count;
}

void addMrapiShmBetweenTwo(struct tThreadData *writerTd, struct tThreadData *readerTd, int size, key_t shmKey, key_t mtxKey) {

	addMrapiShm(writerTd, size, true, shmKey, mtxKey);
	addMrapiShm(readerTd, size, false, shmKey, mtxKey);

	return;
}

int addMrapiShmBetweenAll(struct tThreadData *td, int size, boolean isOwner, key_t shmKey, key_t mtxKey) {

	int i, count = 0;
	struct tThreadData *itTd;
	int numOfThreads;

	addMrapiShm(td, size, true, shmKey, mtxKey);

	if (isOwner) {
		numOfThreads = activeProc->numOfThreads;
		for (i = 1; i <= numOfThreads; i++) {
			itTd = activeProc->threads[i];
			if (itTd != td && (td->groupId == -1 || itTd->groupId == td->groupId)) {
				itTd->mrapiExt.hasExt = true;
				count = addMrapiShm(itTd, size, false, shmKey, mtxKey);
			}
		}
	}

	return count;
}

int addMrapiShmBetweenPhase(struct tThreadData *td, int phase, int size, boolean isOwner, key_t shmKey, key_t mtxKey) {

	int i, count = 0;
	struct tThreadData *itTd;
	int numOfThreads;

	addMrapiShm(td, size, true, shmKey, mtxKey);

	if (isOwner) {
		numOfThreads = activeProc->numOfThreads;
		for (i = 2; i <= numOfThreads; i++) {
			itTd = activeProc->threads[i];
			if (itTd->creator == td->id && itTd->standAloneExt.phase == phase) {
				itTd->mrapiExt.hasExt = true;
				count = addMrapiShm(itTd, size, false, shmKey, mtxKey);
			}
		}
	}

	return count;
}

struct tMcapiEndpoint *addMcapiEndpoint(struct tThreadData *td, int nodeId, int portId, boolean isSender,
		endpointType type, boolean isOwner, int phase) {

	int count = td->mcapiExt.numOfEndpoints;

	assert(count < MAX_ENDPOINT);

	td->mcapiExt.endpoints[count].nodeId = nodeId;
	td->mcapiExt.endpoints[count].portId = portId;
	td->mcapiExt.endpoints[count].printNodeId = td->id;
	td->mcapiExt.endpoints[count].printPortId = td->mcapiExt.numOfEndpoints + 1;
	td->mcapiExt.endpoints[count].isSender = isSender;
	td->mcapiExt.endpoints[count].type = type;
	td->mcapiExt.endpoints[count].isOwner = isOwner;
	td->mcapiExt.endpoints[count].phase = phase;

	td->mcapiExt.numOfEndpoints++;

	return &td->mcapiExt.endpoints[count];
}

//FIXME: If channel type is MSG then do not add reverse channel

int addMcapiChannel(struct tThreadData *sendrTd, struct tThreadData *recvrTd, int size, endpointType type, int senderPhase, int recvrPhase) {

	int count = 0;
	struct tMcapiEndpoint *pSenderEp;
	struct tMcapiEndpoint *pSenderGetEp;
	struct tMcapiEndpoint *pReceiverEp;
	struct tMcapiEndpoint *pReceiverGetEp = NULL;
	int phase;

	/* phase starts from 0 but */
	if (senderPhase > recvrPhase) phase = senderPhase - 1;
	else phase = recvrPhase - 1;

	pSenderEp = addMcapiEndpoint(sendrTd, sendrTd->id, ++sendrTd->mcapiExt.currPortId, true, type, true, phase);
	pSenderGetEp = addMcapiEndpoint(sendrTd, recvrTd->id, ++recvrTd->mcapiExt.currPortId, false, type, false, phase);
	pReceiverEp = addMcapiEndpoint(recvrTd, recvrTd->id, recvrTd->mcapiExt.currPortId, false, type, true, phase);
	if (type != EP_MESSAGE) {
		recvrTd->mcapiExt.currPortId++;
		pReceiverGetEp = addMcapiEndpoint(recvrTd, sendrTd->id, sendrTd->mcapiExt.currPortId++, true, type, false, phase);
	}

	/* if (type == EP_MESSAGE) do nothing except adding endpoints */

	if (type == EP_PACKET) {
		count = activeProc->mcapiExt.pcktChanCount;
		assert(count < MAX_CHANNEL);
		activeProc->mcapiExt.pcktChans[count].pSenderEp = pSenderEp;
		activeProc->mcapiExt.pcktChans[count].pSenderGetEp = pSenderGetEp;
		activeProc->mcapiExt.pcktChans[count].pReceiverEp= pReceiverEp;
		activeProc->mcapiExt.pcktChans[count].pReceiverGetEp = pReceiverGetEp;
		activeProc->mcapiExt.pcktChans[count].pSenderTd = sendrTd;
		activeProc->mcapiExt.pcktChans[count].pReceiverTd = recvrTd;
		activeProc->mcapiExt.pcktChans[count].size = 0;
		activeProc->mcapiExt.pcktChans[count].type = type;
		activeProc->mcapiExt.pcktChanCount++;
		sendrTd->mcapiExt.pcktChans[sendrTd->mcapiExt.numOfPcktChans++] = &activeProc->mcapiExt.pcktChans[count];
		recvrTd->mcapiExt.pcktChans[recvrTd->mcapiExt.numOfPcktChans++] = &activeProc->mcapiExt.pcktChans[count];
	} else if (type == EP_SCALAR) {

		/* round packet size */
		if (size  < 12) size = 8;
		else if (size < 24) size = 16;
		else if (size < 48) size = 32;
		else size = 64;

		count = activeProc->mcapiExt.scalChanCount;
		assert(count < MAX_CHANNEL);
		activeProc->mcapiExt.scalChans[count].pSenderEp = pSenderEp;
		activeProc->mcapiExt.scalChans[count].pSenderGetEp = pSenderGetEp;
		activeProc->mcapiExt.scalChans[count].pReceiverEp= pReceiverEp;
		activeProc->mcapiExt.scalChans[count].pReceiverGetEp = pReceiverGetEp;
		activeProc->mcapiExt.scalChans[count].pSenderTd = sendrTd;
		activeProc->mcapiExt.scalChans[count].pReceiverTd = recvrTd;
		activeProc->mcapiExt.scalChans[count].size = size;
		activeProc->mcapiExt.scalChans[count].type = type;
		activeProc->mcapiExt.scalChanCount++;
		sendrTd->mcapiExt.scalChans[sendrTd->mcapiExt.numOfScalChans++] = &activeProc->mcapiExt.scalChans[count];
		recvrTd->mcapiExt.scalChans[recvrTd->mcapiExt.numOfScalChans++] = &activeProc->mcapiExt.scalChans[count];
	}

	return count;
}

int addMtapiWorkOp(struct tThreadData *td, workOpType type, unsigned int numOfOperations, int time) {

	int count = td->mtapiExt.numOfWorks;

	assert(count < MAX_WORK_OPS);

	td->mtapiExt.workOps[count].type = type;
	td->mtapiExt.workOps[count].numOfIterations = numOfOperations;
	td->mtapiExt.workOps[count].time = time;

	td->mtapiExt.numOfWorks++;

	return count;
}

int addMtapiWorkOpWithRatio(struct tThreadData *td, workOpType type, unsigned int numOfOperations, int type1Percent, int time) {

	int count = td->mtapiExt.numOfWorks;

	assert(count < MAX_WORK_OPS);

	td->mtapiExt.workOps[count].type = type;
	td->mtapiExt.workOps[count].numOfIterations = numOfOperations;
	td->mtapiExt.workOps[count].type1Percent = type1Percent;
	td->mtapiExt.workOps[count].time = time;

	td->mtapiExt.numOfWorks++;

	return count;
}

int addTlsMemOp(struct tThreadData *td, int resId, int startIndex, int numOfIterations, int time, memOpType opType, memType type) {

	int count = td->lsExt.numOfMemOps;

	assert(count < MAX_MEM_OPS);

	if (numOfIterations > 0) {
		td->lsExt.memOps[count].resId = resId;
		td->lsExt.memOps[count].startIndex = startIndex;
		td->lsExt.memOps[count].numOfIterations = numOfIterations;
		td->lsExt.memOps[count].time = time;
		td->lsExt.memOps[count].opType = opType;
		td->lsExt.memOps[count].type = type;
		td->lsExt.memOps[count].semIndex = -1;

		td->lsExt.numOfMemOps++;
	} else {
		count = -1;
	}

	return count;
}

int addTlsOrderedMemOp(struct tThreadData *td, int resId, int startIndex, int numOfIterations, int time, memOpType opType, memType type, int semIndex) {

	int count = td->lsExt.numOfMemOps;

	assert(count < MAX_MEM_OPS);

	if (numOfIterations > 0) {
		td->lsExt.memOps[count].resId = resId;
		td->lsExt.memOps[count].startIndex = startIndex;
		td->lsExt.memOps[count].numOfIterations = numOfIterations;
		td->lsExt.memOps[count].time = time;
		td->lsExt.memOps[count].opType = opType;
		td->lsExt.memOps[count].type = type;
		td->lsExt.memOps[count].semIndex = semIndex;
		td->posixExt.sems[semIndex].isLocked = true;
		td->lsExt.memOps[count].workSize = -1;

		td->lsExt.numOfMemOps++;
	} else {
		count = -1;
	}

	return count;
}

int addMrapiSyncOp(struct tThreadData *td, int resId, int time, syncType type) {

	int count = td->mrapiExt.numOfSyncOps;

	assert(count < MAX_SYNC_OPS);

	td->mrapiExt.syncOps[count].resId = resId;
	td->mrapiExt.syncOps[count].time = time;
	td->mrapiExt.syncOps[count].type = type;

	td->mrapiExt.numOfSyncOps++;

	return count;
}

/* Assuming that all threads have same barrier resource id */
int addMrapiSyncOpInTask(struct tThreadData *td, int resId, int time, syncType type) {

	int count = 0;
	int i;
	struct tThreadData *itTd;
	int numOfThreads;

	numOfThreads = activeProc->numOfThreads;
	for (i = 2; i <= numOfThreads; i++) {
		itTd = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == itTd->mtapiExt.startRoutine.taskId) {
			count = itTd->mrapiExt.numOfSyncOps;
			assert(count < MAX_SYNC_OPS);
			itTd->mrapiExt.syncOps[count].resId = resId;
			itTd->mrapiExt.syncOps[count].time = time;
			itTd->mrapiExt.syncOps[count].type = type;
			itTd->mrapiExt.numOfSyncOps++;
		}
	}

	return count;
}

int addMrapiMemOp(struct tThreadData *td, int resId, int startIndex, int numOfIterations, int time, memOpType opType, memType type) {

	int count = td->mrapiExt.numOfMemOps;

	assert(count < MAX_MEM_OPS);

	td->mrapiExt.memOps[count].resId = resId;
	td->mrapiExt.memOps[count].startIndex = startIndex;
	td->mrapiExt.memOps[count].numOfIterations = numOfIterations;
	td->mrapiExt.memOps[count].time = time;
	td->mrapiExt.memOps[count].opType = opType;
	td->mrapiExt.memOps[count].type = type;
	td->mrapiExt.memOps[count].semIndex = -1;
	td->mrapiExt.memOps[count].workSize = -1;

	td->mrapiExt.numOfMemOps++;

	return count;
}

int addMrapiOrderedMemOp(struct tThreadData *td, int resId, int startIndex, int numOfIterations, int time, memOpType opType, memType type, int semIndex) {

	int count = td->mrapiExt.numOfMemOps;

	assert(count < MAX_MEM_OPS);

	td->mrapiExt.memOps[count].resId = resId;
	td->mrapiExt.memOps[count].startIndex = startIndex;
	td->mrapiExt.memOps[count].numOfIterations = numOfIterations;
	td->mrapiExt.memOps[count].time = time;
	td->mrapiExt.memOps[count].opType = opType;
	td->mrapiExt.memOps[count].type = type;
	td->mrapiExt.memOps[count].semIndex = semIndex;
	td->mrapiExt.sems[semIndex].isLocked = true;
	td->mrapiExt.memOps[count].workSize = -1;

	td->mrapiExt.numOfMemOps++;

	return count;
}

int addMrapiOrderedMemOpWithWork(struct tThreadData *td, int resId, int startIndex, int numOfIterations, int time, memOpType opType, memType type, int semIndex, int workSize) {

	int count = td->mrapiExt.numOfMemOps;

	assert(count < MAX_MEM_OPS);

	td->mrapiExt.memOps[count].resId = resId;
	td->mrapiExt.memOps[count].startIndex = startIndex;
	td->mrapiExt.memOps[count].numOfIterations = numOfIterations;
	td->mrapiExt.memOps[count].time = time;
	td->mrapiExt.memOps[count].opType = opType;
	td->mrapiExt.memOps[count].type = type;
	td->mrapiExt.memOps[count].semIndex = semIndex;
	td->mrapiExt.memOps[count].workSize = workSize;

	td->mrapiExt.numOfMemOps++;

	return count;
}

int addMcapiMsgOp(struct tThreadData *td, int resId, int resId2, int numOfIterations, int time, msgOpType opType, msgType type) {

	int count = td->mcapiExt.numOfMsgOps;

	assert(count < MAX_MSG_OPS);

	td->mcapiExt.msgOps[count].resId = resId;
	td->mcapiExt.msgOps[count].resId2 = resId2;
	td->mcapiExt.msgOps[count].numOfIterations = numOfIterations;
	td->mcapiExt.msgOps[count].time = time;
	td->mcapiExt.msgOps[count].opType = opType;
	td->mcapiExt.msgOps[count].type = type;

	td->mcapiExt.numOfMsgOps++;

	return count;
}

struct tSem *addPosixSem(struct tThreadData *td, boolean isOwner, key_t key, boolean isLockAfterCreate) {

	int count = td->posixExt.numOfSemaphores;

	assert(count < MAX_SEM);

	td->posixExt.sems[count].isOwner = isOwner;
	td->posixExt.sems[count].key = key;
	td->posixExt.sems[count].isLockAfterCreate = isLockAfterCreate;
	td->posixExt.sems[count].isLocked = false;
	td->posixExt.numOfSemaphores++;

	return &td->posixExt.sems[count];
}

void addPosixSemBetweenTwo(struct tThreadData *ownerTd, struct tThreadData *td, key_t key, boolean isLockAfterCreate) {

	addPosixSem(ownerTd, true, key, isLockAfterCreate);
	addPosixSem(td, false, key, false);

	return;
}

void addPosixSemBetweenAll(struct tThreadData *ownerTd, key_t key, boolean isLockAfterCreate) {

	int i;
	struct tThreadData *itTd;
	int numOfThreads;

	addPosixSem(ownerTd, true, key, isLockAfterCreate);

	numOfThreads = activeProc->numOfThreads;
	for (i = 1; i <= numOfThreads; i++) {
		itTd = activeProc->threads[i];
		if (itTd != ownerTd && (ownerTd->groupId == -1 || itTd->groupId == itTd->groupId)) {
			itTd->lsExt.hasExt = true;
			addPosixSem(itTd, false, key, false);
		}
	}

	return;
}

/*
 * add posix mutexes to only main thread
 */
struct tMutex *addPosixMtx(struct tThreadData *td, boolean isOwner, key_t key) {

	int count = td->posixExt.numOfMutexes;

	assert(count < MAX_MUTEX);

	td->posixExt.mutexes[count].id = count + 1;
	td->posixExt.mutexes[count].isOwner = isOwner;
	td->posixExt.mutexes[count].key = key;
	td->posixExt.mutexes[count].isLocked = false;
	td->posixExt.numOfMutexes++;

	return &td->posixExt.mutexes[count];
}

int subTimes(struct tTimeData *lhs, struct tTimeData *rhs) {

	int retVal;

	retVal = lhs->nanosec - rhs->nanosec;
	retVal += (lhs->second - rhs->second) * 1000;
	retVal += (lhs->minute - rhs->minute) * (1000 * 60);
	retVal += (lhs->hour - rhs->hour) * ((1000 * 60) * 60);

	return retVal;
}

void assignTasks(paralPattern pps[MAX_GROUP_NUMBER]) {

	int i, j, numOfThreads;
	struct tThreadData *td;
	int taskId = 0, threadCount;

	numOfThreads = activeProc->numOfThreads;

	for (i = 2; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		activeProc->threads[td->creator]->mtapiExt.numOfChildren[td->groupId]++;
		if (td->mtapiExt.startRoutine.taskId == -1) { /* not assigned */
			if (isMultiThreadSingleTask) {
				for (j = i, threadCount = 0; j <= numOfThreads; j++) {
					if (td->pc == activeProc->threads[j]->pc && td->groupId == activeProc->threads[j]->groupId) {
						activeProc->threads[j]->mtapiExt.startRoutine.taskId = taskId;
						threadCount++;
					}
				}
				for (j = i; j <= numOfThreads; j++) {
					if (td->pc == activeProc->threads[j]->pc && td->groupId == activeProc->threads[j]->groupId) {
						activeProc->threads[j]->mtapiExt.startRoutine.threadCount = threadCount;
					}
				}
				//FIXME: Unique tasks for TP or not?
//				if (pps[td->groupId] == PP_TP) {
//					activeProc->mtapiExt.hasUniqueThreads[taskId] = true;
//				} else {
					activeProc->mtapiExt.hasUniqueThreads[taskId] = false;
//				}
			} else {
				td->mtapiExt.startRoutine.taskId = taskId;
			}
			activeProc->mtapiExt.isStartRoutine[taskId] = SR_ASSIGNED;
			taskId++;
		}
	}


	return;
}

void assignTasksSa() {

	int i, j, numOfThreads;
	struct tThreadData *td;
	int taskId = 0, threadCount;

	numOfThreads = activeProc->numOfThreads;

	for (i = 2; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		td->groupId = td->standAloneExt.phase - 1;
		activeProc->threads[td->creator]->mtapiExt.numOfChildren[td->standAloneExt.phase - 1]++;
		activeProc->threads[td->creator]->standAloneExt.phasePatterns[td->standAloneExt.phase - 1] = td->standAloneExt.parPattern;
		if (td->mtapiExt.startRoutine.taskId == -1) { /* not assigned */
			if (isMultiThreadSingleTask) {
				for (j = i, threadCount = 0; j <= numOfThreads; j++) {
					if (td->pc == activeProc->threads[j]->pc && td->standAloneExt.phase == activeProc->threads[j]->standAloneExt.phase) {
						activeProc->threads[j]->mtapiExt.startRoutine.taskId = taskId;
						threadCount++;
					}
				}
				for (j = i; j <= numOfThreads; j++) {
					if (td->pc == activeProc->threads[j]->pc && td->standAloneExt.phase == activeProc->threads[j]->standAloneExt.phase) {
						activeProc->threads[j]->mtapiExt.startRoutine.threadCount = threadCount;
					}
				}
				if (td->standAloneExt.parPattern == PP_TP) {
					activeProc->mtapiExt.hasUniqueThreads[taskId] = true;
				} else {
					activeProc->mtapiExt.hasUniqueThreads[taskId] = false;
				}
			} else {
				td->mtapiExt.startRoutine.taskId = taskId;
			}
			activeProc->mtapiExt.isStartRoutine[taskId] = SR_ASSIGNED;
			taskId++;
		}
	}

	return;
}

void findMinMaxDataSize(int groupId, int *minDataSize, int *maxDataSize) {

	int i, j, numOfThreads, threshold;
	struct tThreadData *td;

	*minDataSize = INT_MAX;
	*maxDataSize = 0;

	numOfThreads = activeProc->numOfThreads;

	for (i = 2; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->groupId == groupId) {
			threshold = (int) (td->cacheInvalid * cahceMissRatio);
			for (j = 2; j <= numOfThreads; j++) {
				if (i != j) {
					if (activeProc->threads[j]->groupId == groupId) {
						if (td->dependency[j] > threshold) {
							/* thread i depends on thread j */
							if (td->dependency[j] < *minDataSize) {
								*minDataSize = td->dependency[j];
							}
							if (td->dependency[j] > *maxDataSize) {
								*maxDataSize = td->dependency[j];
							}
						}
					}
				}
			}
		}
	}

	return;
}

int getScalarSize(int size, int maxSize) {

	int scalarSize;

	scalarSize = 64 * ((double) size / (double) maxSize);

	if (scalarSize / 48 > 1) {
		return 64;
	} else if (scalarSize / 24 > 1) {
		return 32;
	} else if (scalarSize / 12 > 1) {
		return 16;
	}

	return 8;
}

int getMaxGroupId2(struct tThreadData *td) {

	int i, numOfThreads;
	int maxGrpId = 0;
	struct tThreadData *itTd;

	if (!isStandalone) {
		maxGrpId = maxGroupId;
	} else {
		numOfThreads = activeProc->numOfThreads;
		for (i = 2; i <= numOfThreads; i++) { /* except main */
			itTd = activeProc->threads[i];
			if (itTd->creator == td->id) {
				if (itTd->standAloneExt.phase > maxGrpId) {
					maxGrpId = itTd->standAloneExt.phase;
				}
			}
		}
	}

	return maxGrpId;

}

int getTotalSharedDataSize(struct tThreadData *td, int phase) {

	int i, numOfThreads;
	int totalSize = 0;
	struct tThreadData *itTd;

	if (!isStandalone) {
		totalSize = activeProc->dataSharing[td->groupId].prodCons;
	} else {
		numOfThreads = activeProc->numOfThreads;
		for (i = 2; i <= numOfThreads; i++) { /* except main */
			itTd = activeProc->threads[i];
			if (itTd->creator == td->id && itTd->standAloneExt.phase == phase) {
				totalSize += itTd->standAloneExt.sharedDataSize;
			}
		}
	}

	return totalSize;

}

struct tThreadData* getCreatorOfMaxThreads()
{
	int i, numOfThreads, maxValue = 0, maxIndex = 1;
	struct tThreadData *creator;
	int creators[MAX_THREADS] = { 0 };

	numOfThreads = activeProc->numOfThreads;

	for (i = 1; i <= numOfThreads; i++) {
		creators[activeProc->threads[i]->creator]++;
	}

	for (i = 1; i <= numOfThreads; i++) {
		if (creators[i] > maxValue) {
			maxValue = creators[i];
			maxIndex = i;
		}
	}

	creator = activeProc->threads[maxIndex];

	return creator;
}

void initMxapiData() {

	int i, numOfThreads;
	struct tThreadData *td;

	numOfThreads = activeProc->numOfThreads;

	/* initialize process */
	for (i = 1; i <= MAX_THREADS; i++) {
		activeProc->mtapiExt.isStartRoutine[i] = SR_NOT_INITIALIZED;
	}
	for (i = 1; i < MAX_GROUP_NUMBER; i++) {
		activeProc->mtapiExt.totalLifeTimes[i] = 0;
	}
	activeProc->mrapiExt.barrierCount = 0;
	activeProc->mcapiExt.pcktChanCount = 0;
	activeProc->mcapiExt.scalChanCount = 0;

	/* initialize threads*/
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		/* mtapi */
		td->mtapiExt.lifeTime = 0;
		memset(td->mtapiExt.workOps, -1, MAX_WORK_OPS * sizeof(struct tWorkOp));
		td->mtapiExt.loopCount = 0;
		memset(td->mtapiExt.numOfChildren, 0, MAX_GROUP_NUMBER * sizeof(int));
		td->mtapiExt.numOfWorks = 0;
		/* ls */
		td->lsExt.hasExt = false;
		td->lsExt.numOfLocalMems = 0;
		td->lsExt.numOfMemOps = 0;
		/* mrapi */
		td->mrapiExt.hasExt = false;
		td->mrapiExt.numOfSemaphores = 0;
		td->mrapiExt.numOfMutexes = 0;
		td->mrapiExt.numOfBarriers = 0;
		td->mrapiExt.numOfSharedMems = 0;
		td->mrapiExt.numOfMemOps = 0;
		td->mrapiExt.numOfSyncOps = 0;
		/* mcapi */
		td->mcapiExt.hasExt = false;
		td->mcapiExt.currPortId = 0;
		td->mcapiExt.numOfEndpoints = 0;
		td->mcapiExt.numOfPcktChans = 0;
		td->mcapiExt.numOfScalChans = 0;
		td->mcapiExt.numOfMsgOps = 0;
	}

	return;
}

void addCodeBlocks(struct tThreadData *td, int index, struct tThreadData *mainTd) {

	if (updateData[index].ipcCount > 0) {
		if (updateData[index].isIpcInc) {
			addMtapiWorkOpWithRatio(td, WORK_OP_IPC_INC, updateData[index].ipcCount, activeProc->instructions.compRatio, ((maxGroupId - 1) * MAX_WORK_PER_PHASE) + 3);
		} else {
			addMtapiWorkOpWithRatio(td, WORK_OP_IPC_DEC, updateData[index].ipcCount, activeProc->instructions.compRatio, ((maxGroupId - 1) * MAX_WORK_PER_PHASE) + 3);
		}
	}

	if (updateData[index].cacheMissCount > 0) {
		if (updateData[index].isCacheMissInc) {
			addMtapiWorkOp(td, WORK_OP_CACHE_MISS, updateData[index].cacheMissCount, ((maxGroupId - 1) * MAX_WORK_PER_PHASE) + 3);
		} else {
			addGlobalMemWithName(256, 256, "wa");
			addMtapiWorkOp(td, WORK_OP_CACHE_HIT, updateData[index].cacheMissCount, ((maxGroupId - 1) * MAX_WORK_PER_PHASE) + 3);
		}
	}

	if (updateData[index].branchMissCount > 0) {
		if (updateData[index].isBranchMissInc) {
			addMtapiWorkOp(td, WORK_OP_BRNCH_MISS, updateData[index].branchMissCount, ((maxGroupId - 1) * MAX_WORK_PER_PHASE) + 3);
		} else {
			addMtapiWorkOp(td, WORK_OP_BRNCH_HIT, updateData[index].branchMissCount, ((maxGroupId - 1) * MAX_WORK_PER_PHASE) + 3);
		}
	}

	if (updateData[index].commToCompCount > 0) {
		if (updateData[index].isCommInc) {
			addMtapiWorkOp(mainTd, WORK_OP_COMM_INC, updateData[index].commToCompCount, ((maxGroupId - 1) * MAX_WORK_PER_PHASE) + 3);
		} else {
			addMtapiWorkOp(mainTd, WORK_OP_COMP_INC, updateData[index].commToCompCount, ((maxGroupId - 1) * MAX_WORK_PER_PHASE) + 3);
		}
	}

	return;
}

void prepareMtapi(paralPattern pps[MAX_GROUP_NUMBER]) {

	int i, numOfThreads;
	struct tThreadData *td;
	int lifeTime, maxLifeTime = 0;
	paralPattern parPat;

	numOfThreads = activeProc->numOfThreads;

	/* assign task identifier for each thread */
	assignTasks(pps);

	/* calculate life times of the threads */
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		td->mtapiExt.lifeTime = subTimes(&td->exitTime, &td->creationTime);
		if (td->mtapiExt.lifeTime > maxLifeTime) {
			maxLifeTime = td->mtapiExt.lifeTime;
		}
		activeProc->mtapiExt.totalLifeTimes[td->groupId] += td->mtapiExt.lifeTime;
	}

	/* calculate minimum data flow sizes */
	for (i = 0; i < maxGroupId; i++) {
		if (pps[i] == PP_Pl || pps[i] == PP_EbC) {
			 findMinMaxDataSize(i, &minDataSizes[i], &maxDataSizes[i]);
		}
	}

	td = activeProc->threads[1]; /* main */
	lifeTime = (((double) td->mtapiExt.lifeTime / (double) maxLifeTime) * (double) MAX_WORK_LEN);
	td->mtapiExt.totalWork = lifeTime;
	addMtapiWorkOp(td, WORK_OP_NOOP, lifeTime, 0);

	if (isPerThread == true) {
		for (i = 2; i <= numOfThreads; i++) {
			addCodeBlocks(activeProc->threads[i], i, td);
		}
	} else {
		addCodeBlocks(td, 0, td);
	}

	for (i = 2; i <= numOfThreads; i++) { /* except main */
		td = activeProc->threads[i];
		lifeTime = (((double) td->mtapiExt.lifeTime / (double) maxLifeTime) * (double) MAX_WORK_LEN);
		td->mtapiExt.totalWork = lifeTime;
		parPat = pps[td->groupId];
		switch (parPat) {
		case PP_TP:
			addMtapiWorkOp(td, WORK_OP_NOOP, lifeTime, 0);
			break;
		case PP_DaC:
			addMtapiWorkOp(td, WORK_OP_NOOP, (int) ((double) lifeTime) / 2.0, 0);
			addMtapiWorkOp(td, WORK_OP_NOOP, (int) ((double) lifeTime) / 2.0, 2);
			break;
		case PP_GD:
			addMtapiWorkOp(td, WORK_OP_NOOP, lifeTime, 0);
			break;
		case PP_RD:
			addMtapiWorkOp(td, WORK_OP_NOOP, lifeTime, 0);
			break;
		case PP_Pl:
			td->mtapiExt.loopCount = minDataSizes[td->groupId];
			addMtapiWorkOp(td, WORK_OP_NOOP, (int) ((double) lifeTime / (double) td->mtapiExt.loopCount), 0);
			break;
		case PP_EbC:
			addMtapiWorkOp(td, WORK_OP_NOOP, lifeTime, 0);
			break;
		default:
			break;
		}
	}

	return;
}

void prepareMtapiSa() {

	int i, numOfThreads;
	struct tThreadData *td;
	int lifeTime;
	paralPattern parPat;

	numOfThreads = activeProc->numOfThreads;

	/* assign task identifier for each thread */
	assignTasksSa();

	td = activeProc->threads[1]; /* main */

	if (isPerThread == true) {
		for (i = 2; i <= numOfThreads; i++) {
			addCodeBlocks(activeProc->threads[i], i, td);
		}
	} else {
		addCodeBlocks(td, 0, td);
	}

	for (i = 2; i <= numOfThreads; i++) { /* except main */
		td = activeProc->threads[i];
		lifeTime = td->standAloneExt.workSize;
		td->mtapiExt.totalWork = lifeTime;
		parPat = td->standAloneExt.parPattern;
		switch (parPat) {
		case PP_TP:
			addMtapiWorkOp(td, WORK_OP_NOOP, lifeTime, 0);
			break;
		case PP_DaC:
			addMtapiWorkOp(td, WORK_OP_NOOP, (int) ((double) lifeTime) / 2.0, 0);
			addMtapiWorkOp(td, WORK_OP_NOOP, (int) ((double) lifeTime) / 2.0, 2);
			break;
		case PP_GD:
			addMtapiWorkOp(td, WORK_OP_NOOP, lifeTime, 0);
			break;
		case PP_RD:
			addMtapiWorkOp(td, WORK_OP_NOOP, lifeTime, 0);
			break;
		case PP_Pl:
			td->mtapiExt.loopCount = td->standAloneExt.loopCount;
			addMtapiWorkOp(td, WORK_OP_NOOP, (int) ((double) lifeTime / (double) td->mtapiExt.loopCount), 1);
			break;
		case PP_EbC:
			td->mtapiExt.loopCount = td->standAloneExt.loopCount;
			addMtapiWorkOp(td, WORK_OP_NOOP, (int) ((double) lifeTime / (double) td->mtapiExt.loopCount), 1);
			break;
		default:
			break;
		}
	}

	return;
}

void prepareTls(paralPattern pps[MAX_GROUP_NUMBER]) {

	int i, numOfThreads;
	struct tThreadData *td;
	paralPattern parPat;
	int readOnlyMemSize = 0, readOnlyMemResId = 0;
	int privateMemSize, resId;
	boolean isGlobAdded[MAX_GROUP_NUMBER] = { false };

	numOfThreads = activeProc->numOfThreads;

	td = activeProc->threads[1]; /* main */
	for (i = 0; i < maxGroupId; i++) {
		readOnlyMemSize += activeProc->dataSharing[i].readonly;
		//FIXME: Main thread uses private memory or not?
		//FIXME: handle multi-phases
		if (pps[i] != PP_Pl) {
			td->lsExt.hasExt = true;
			readOnlyMemSize = (int) (((double) readOnlyMemSize / (double) numOfThreads) * dataReadonlyRatio) + 1;
			readOnlyMemResId = addGlobalMem(readOnlyMemSize);
			addTlsMemOp(td, readOnlyMemResId, 0, readOnlyMemSize, 0, MEM_OP_READ, MEM_TYPE_GLOBAL);
		}
	}

	for (i = 2; i <= numOfThreads; i++) { /* except main */
		td = activeProc->threads[i];
		td->lsExt.hasExt = true;
		privateMemSize = (int) ((double) (activeProc->dataSharing[td->groupId].private * dataPrivateRatio) / (double) (numOfThreads - 1));
		parPat = pps[td->groupId];
		switch (parPat) {
		case PP_TP:
			privateMemSize = (double) privateMemSize / 2.0 + 1; /* since we access each memory two times */
			resId = addTlsLocalMem(td, privateMemSize);
			addTlsMemOp(td, resId, 0, privateMemSize, 0, MEM_OP_WRITE, MEM_TYPE_LOCAL);
			addTlsMemOp(td, resId, 0, privateMemSize, 3, MEM_OP_READ, MEM_TYPE_LOCAL);
			break;
		case PP_DaC:
			privateMemSize = ((double) privateMemSize / 3.0) + 1; /* since we access each memory three times */
			resId = addTlsLocalMem(td, privateMemSize);
			addTlsMemOp(td, 0, resId, privateMemSize, 0, MEM_OP_WRITE, MEM_TYPE_LOCAL);
			addTlsMemOp(td, 0, resId, privateMemSize, 2, MEM_OP_READ, MEM_TYPE_LOCAL);
			addTlsMemOp(td, 0, resId, privateMemSize, 3, MEM_OP_WRITE, MEM_TYPE_LOCAL);
			break;
		case PP_GD:
			/* no operation */
			break;
		case PP_RD:
			/* no operation */
			break;
		case PP_Pl:
			if (!isGlobAdded[td->groupId]) {
				readOnlyMemSize = (int) (((double) readOnlyMemSize / (double) numOfThreads) * dataReadonlyRatio);
				readOnlyMemSize = (readOnlyMemSize / td->mtapiExt.loopCount) + 1;
				readOnlyMemResId = addGlobalMem(readOnlyMemSize);
				isGlobAdded[td->groupId] = true;
			}
			addTlsMemOp(td, readOnlyMemResId, 0, readOnlyMemSize, 0, MEM_OP_READ, MEM_TYPE_GLOBAL);
			privateMemSize = ((double) privateMemSize / (2.0 * (td->mtapiExt.loopCount))) + 1; /* since we access each memory two times */
			resId = addTlsLocalMem(td, privateMemSize);
			addTlsMemOp(td, 0, resId, privateMemSize, 1, MEM_OP_WRITE, MEM_TYPE_LOCAL);
			addTlsMemOp(td, 0, resId, privateMemSize, 2, MEM_OP_READ, MEM_TYPE_LOCAL);
			break;
		case PP_EbC:
			privateMemSize = ((double) privateMemSize / 2.0) + 1; /* since we access each memory two times */
			resId = addTlsLocalMem(td, privateMemSize);
			addTlsMemOp(td, 0, resId, privateMemSize, 1, MEM_OP_WRITE, MEM_TYPE_LOCAL);
			addTlsMemOp(td, 0, resId, privateMemSize, 2, MEM_OP_READ, MEM_TYPE_LOCAL);
			break;
		default:
			break;
		}
	}

	return;
}

void prepareTlsSa() {

	int i, numOfThreads;
	struct tThreadData *td;
	int privateMemSize, resId;

	numOfThreads = activeProc->numOfThreads;

	//FIXME: add read-only data?

	for (i = 2; i <= numOfThreads; i++) { /* except main */
		td = activeProc->threads[i];
		privateMemSize = td->standAloneExt.localDataSize;
		if (privateMemSize > 0) {
			td->lsExt.hasExt = true;
			switch (td->standAloneExt.parPattern) {
			case PP_TP:
				resId = addTlsLocalMem(td, privateMemSize);
				addTlsMemOp(td, resId, 0, privateMemSize, 0, MEM_OP_WRITE, MEM_TYPE_LOCAL);
				addTlsMemOp(td, resId, 0, privateMemSize, 3, MEM_OP_READ, MEM_TYPE_LOCAL);
				break;
			case PP_DaC:
				resId = addTlsLocalMem(td, privateMemSize);
				addTlsMemOp(td, 0, resId, privateMemSize, 0, MEM_OP_WRITE, MEM_TYPE_LOCAL);
				addTlsMemOp(td, 0, resId, privateMemSize, 2, MEM_OP_READ, MEM_TYPE_LOCAL);
				addTlsMemOp(td, 0, resId, privateMemSize, 3, MEM_OP_WRITE, MEM_TYPE_LOCAL);
				break;
			case PP_GD:
				/* no operation */
				break;
			case PP_RD:
				/* no operation */
				break;
			case PP_Pl:
				resId = addTlsLocalMem(td, privateMemSize);
				addTlsMemOp(td, 0, resId, privateMemSize, 1, MEM_OP_WRITE, MEM_TYPE_LOCAL);
				addTlsMemOp(td, 0, resId, privateMemSize, 2, MEM_OP_READ, MEM_TYPE_LOCAL);
				break;
			case PP_EbC:
				resId = addTlsLocalMem(td, privateMemSize);
				addTlsMemOp(td, 0, resId, privateMemSize, 1, MEM_OP_WRITE, MEM_TYPE_LOCAL);
				addTlsMemOp(td, 0, resId, privateMemSize, 2, MEM_OP_READ, MEM_TYPE_LOCAL);
				break;
			default:
				break;
			}
		}
	}

	return;
}

//FIXME: Partial dependencies:
/*
 * Currently, ignore changes in dependencies between other threads, just focus on thread i and j.
 * In future, if the dependency between any other two threads is higher than threshold then add operation.
 * Do the same for MCAPI preparation
 */

void prepareMrapi(paralPattern pps[MAX_GROUP_NUMBER]) {

	int i, j, numOfThreads, threshold;
	//int k, unitWork, workSize, memSize;
	struct tThreadData *td;
	paralPattern parPat;
	int resId, unitSize, numOfWorkers, totalDepCount = 0, totalDepVolume = 0;
	int prodConsSizes[MAX_GROUP_NUMBER] = { 0 };
	int startIndexes[MAX_GROUP_NUMBER] = { 0 };
//	boolean isBarrierAdded[MAX_THREADS] = { false };
	//FIXME: barrier: key_t shmKey, mtxKey;

	td = getCreatorOfMaxThreads();

	/* find the data flow */
	for (i = 0; i < maxGroupId; i++) {
		if (pps[i] == PP_GD || pps[i] == PP_RD) {
			td->mrapiExt.hasExt = true;
			prodConsSizes[i] = (activeProc->dataSharing[i].prodCons * dataSharingRatio) + PROD_CONS_MIN;
			resId = addMrapiShmBetweenAll(td, prodConsSizes[i], true, nextShmKey(), nextMtxKey());
			addMrapiMemOp(td, resId, 0, prodConsSizes[i], (i * MAX_WORK_PER_PHASE) + 0, MEM_OP_WRITE, MEM_TYPE_SHARED);
			addMrapiMemOp(td, resId, 0, prodConsSizes[i], (i * MAX_WORK_PER_PHASE) + 3, MEM_OP_READ, MEM_TYPE_SHARED);
		}
	}

	numOfThreads = activeProc->numOfThreads;

	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		threshold = (int) (td->cacheInvalid * cahceMissRatio);
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					totalDepCount++;
					totalDepVolume += td->dependency[j];
				}
			}
		}
	}

	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		threshold = (int) (td->cacheInvalid * cahceMissRatio);
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					parPat = pps[td->groupId];
					switch (parPat) {
					case PP_TP:
						/* no need to shared memory */
						break;
					case PP_DaC:
						 /* The higher one is accepted as dependency and the other one is ignored */
						if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
							td->mrapiExt.hasExt = true;
							activeProc->threads[j]->mrapiExt.hasExt = true;
							unitSize =  td->dependency[j] * dataSharingRatio;
							addMrapiShmBetweenTwo(activeProc->threads[j], td, unitSize, nextShmKey(), nextMtxKey());
							addMrapiMemOp(activeProc->threads[j], activeProc->threads[j]->mrapiExt.numOfSharedMems - 1, 0, unitSize, 2, MEM_OP_WRITE, MEM_TYPE_SHARED);
							addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, 0, unitSize, 1, MEM_OP_READ, MEM_TYPE_SHARED);
						}
						break;
					case PP_GD:
						td->mrapiExt.hasExt = true;
						activeProc->threads[j]->mrapiExt.hasExt = true;
						/* memory addition already done */
						numOfWorkers = totalDepCount; // FIXME: I can change this in the future (numOfWorkers = activeProc->threads[1]->mtapiExt.numOfChildren[td->groupId];)
						unitSize = (int) ((double) (prodConsSizes[td->groupId]) / (double) numOfWorkers);
						//FIXME: for #reads>#writes, add the following line or remove the write operation
						//addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, startIndexes[td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_SHARED);
						addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, startIndexes[td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_SHARED);
							addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, startIndexes[td->groupId], unitSize, 3, MEM_OP_WRITE, MEM_TYPE_SHARED);
						startIndexes[td->groupId] += unitSize;
						/*if (!isBarrierAdded[td->mtapiExt.startRoutine.taskId]) {
							addMrapiBarrier(td, true, nextMtxKey(), 1);
							isBarrierAdded[td->mtapiExt.startRoutine.taskId] = true;
							addMrapiSyncOpInTask(td, td->mrapiExt.numOfBarriers - 1, 3, SYNC_TYPE_BAR);
						}*/
						break;
					case PP_RD:
						td->mrapiExt.hasExt = true;
						activeProc->threads[j]->mrapiExt.hasExt = true;
						/* memory addition already done */
						numOfWorkers = activeProc->threads[td->creator]->mtapiExt.numOfChildren[td->groupId];
						unitSize = (int) (prodConsSizes[td->groupId] * ((double) td->dependency[j] / (double) totalDepVolume));
						addMrapiMemOp(activeProc->threads[j], td->mrapiExt.numOfSharedMems - 1, startIndexes[td->groupId], unitSize, 3, MEM_OP_WRITE, MEM_TYPE_SHARED);
						addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, startIndexes[td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_SHARED);
						startIndexes[td->groupId] += unitSize;
//						if (!isBarrierAdded[td->mtapiExt.startRoutine.taskId]) {
//							addMrapiBarrier(td, true, nextMtxKey(), 1);
//							isBarrierAdded[td->mtapiExt.startRoutine.taskId] = true;
//							addMrapiSyncOpInTask(td, td->mrapiExt.numOfBarriers - 1, 3, SYNC_TYPE_BAR);
//						}
						break;
					case PP_Pl:
						if (j != 1) { /* except main */
							/* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								td->mrapiExt.hasExt = true;
								activeProc->threads[j]->mrapiExt.hasExt = true;
								unitSize = td->dependency[j] > minDataSizes[td->groupId] ? (int) ((double) td->dependency[j] / (double) minDataSizes[td->groupId]) : 1;
								addMrapiShmBetweenTwo(activeProc->threads[j], td, unitSize, nextShmKey(), nextMtxKey());
								addMrapiSemBetweenTwo(activeProc->threads[j], td, nextSemKey(), true); /* create and get the semaphore (initially locked) */
								addMrapiOrderedMemOp(activeProc->threads[j], activeProc->threads[j]->mrapiExt.numOfSharedMems - 1, 0, unitSize, 2,
										MEM_OP_WRITE, MEM_TYPE_SHARED, activeProc->threads[j]->mrapiExt.numOfSemaphores - 1); /* unlock after write */
								addMrapiOrderedMemOp(td, td->mrapiExt.numOfSharedMems - 1, 0, unitSize, 1,
										MEM_OP_READ, MEM_TYPE_SHARED, td->mrapiExt.numOfSemaphores - 1); /* lock before read */
								/* add barrier */
								addMrapiBarrierBetweenTwo(td, activeProc->threads[j], nextMtxKey());
								addMrapiSyncOp(td, td->mrapiExt.numOfBarriers - 1, 3, SYNC_TYPE_BAR);
								addMrapiSyncOp(activeProc->threads[j], activeProc->threads[j]->mrapiExt.numOfBarriers - 1, 3, SYNC_TYPE_BAR);
							}
						} else {
							//TODO: do similar to preparePosix
						}
						break;
					case PP_EbC:
						if (j != 1) { /* except main */
							 /* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								td->mrapiExt.hasExt = true;
								activeProc->threads[j]->mrapiExt.hasExt = true;
								unitSize =  td->dependency[j] > minDataSizes[td->groupId] ? (int) ((double) td->dependency[j] / (double) minDataSizes[td->groupId]) : 1;
								addMrapiShmBetweenTwo(activeProc->threads[j], td, unitSize, nextShmKey(), nextMtxKey());
								addMrapiMemOp(activeProc->threads[j], activeProc->threads[j]->mrapiExt.numOfSharedMems - 1, 0, unitSize, 2, MEM_OP_WRITE, MEM_TYPE_SHARED);
								addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, 0, unitSize, 1, MEM_OP_READ, MEM_TYPE_SHARED);
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
	}

	return;
}

void prepareMrapiSa() {

	int i, j, numOfThreads, threshold = 0;
	struct tThreadData *td;
	paralPattern parPat;
	int resId, totalSize, unitSize;
	int startIndexes[MAX_THREADS][MAX_GROUP_NUMBER] = { { 0 } };

	numOfThreads = activeProc->numOfThreads;

	/* find the data flow */
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		maxGroupId = getMaxGroupId2(td);
		for (j = 0; j < maxGroupId; j++) {
			if (td->mtapiExt.numOfChildren[j] > 0) {
				if (td->standAloneExt.phasePatterns[j] == PP_GD || td->standAloneExt.phasePatterns[j] == PP_RD) {
					td->mrapiExt.hasExt = true;
					totalSize = getTotalSharedDataSize(td, j + 1);
					resId = addMrapiShmBetweenPhase(td, j+ 1, totalSize, true, nextShmKey(), nextMtxKey());
					addMrapiMemOp(td, resId, 0, totalSize, (j * MAX_WORK_PER_PHASE) + 1, MEM_OP_WRITE, MEM_TYPE_SHARED);
					addMrapiMemOp(td, resId, 0, totalSize, (j * MAX_WORK_PER_PHASE) + 3, MEM_OP_READ, MEM_TYPE_SHARED);
				}
			}
		}
	}

	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					parPat = td->standAloneExt.parPattern;
					switch (parPat) {
					case PP_TP:
						/* no need to shared memory */
						break;
					case PP_DaC:
						/* no need to shared memory */
						break;
					case PP_GD:
						/* memory addition already done */
						td->mrapiExt.hasExt = true;
						activeProc->threads[j]->mrapiExt.hasExt = true;
						unitSize = td->standAloneExt.sharedDataSize;
						//FIXME: for #reads>#writes, add the following line or remove the write operation
						//addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, startIndexes[td->creator][td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_SHARED);
						addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, startIndexes[td->creator][td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_SHARED);
						addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, startIndexes[td->creator][td->groupId], unitSize, 3, MEM_OP_WRITE, MEM_TYPE_SHARED);
						startIndexes[td->creator][td->groupId] += unitSize;
						break;
					case PP_RD:
						/* memory addition already done */
						td->mrapiExt.hasExt = true;
						activeProc->threads[j]->mrapiExt.hasExt = true;
						unitSize = td->standAloneExt.sharedDataSize;
						addMrapiMemOp(activeProc->threads[j], td->mrapiExt.numOfSharedMems - 1, startIndexes[td->creator][td->groupId], unitSize, 3, MEM_OP_WRITE, MEM_TYPE_SHARED);
						addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, startIndexes[td->creator][td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_SHARED);
						startIndexes[td->creator][td->groupId] += unitSize;
						break;
					case PP_Pl:
						if (j != 1) { /* except main */
							/* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								td->mrapiExt.hasExt = true;
								activeProc->threads[j]->mrapiExt.hasExt = true;
								unitSize = td->standAloneExt.sharedDataSize / td->standAloneExt.loopCount;
								addMrapiShmBetweenTwo(activeProc->threads[j], td, unitSize, nextShmKey(), nextMtxKey());
								addMrapiSemBetweenTwo(activeProc->threads[j], td, nextSemKey(), true); /* create and get the semaphore (initially locked) */
								addMrapiOrderedMemOp(activeProc->threads[j], activeProc->threads[j]->mrapiExt.numOfSharedMems - 1, 0, unitSize, 2,
										MEM_OP_WRITE, MEM_TYPE_SHARED, activeProc->threads[j]->mrapiExt.numOfSemaphores - 1); /* unlock after write */
								addMrapiOrderedMemOp(td, td->mrapiExt.numOfSharedMems - 1, 0, unitSize, 1,
										MEM_OP_READ, MEM_TYPE_SHARED, td->mrapiExt.numOfSemaphores - 1); /* lock before read */
								/* add barrier */
								addMrapiBarrierBetweenTwo(td, activeProc->threads[j], nextMtxKey());
								addMrapiSyncOp(td, td->mrapiExt.numOfBarriers - 1, 3, SYNC_TYPE_BAR);
								addMrapiSyncOp(activeProc->threads[j], activeProc->threads[j]->mrapiExt.numOfBarriers - 1, 3, SYNC_TYPE_BAR);
							}
						}
						break;
					case PP_EbC:
						if (j != 1) { /* except main */
							 /* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								td->mrapiExt.hasExt = true;
								activeProc->threads[j]->mrapiExt.hasExt = true;
								unitSize = td->standAloneExt.sharedDataSize;
								addMrapiShmBetweenTwo(activeProc->threads[j], td, unitSize, nextShmKey(), nextMtxKey());
								addMrapiMemOp(activeProc->threads[j], activeProc->threads[j]->mrapiExt.numOfSharedMems - 1, 0, unitSize, 2, MEM_OP_WRITE, MEM_TYPE_SHARED);
								addMrapiMemOp(td, td->mrapiExt.numOfSharedMems - 1, 0, unitSize, 1, MEM_OP_READ, MEM_TYPE_SHARED);
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
	}

	return;
}

void prepareMcapi(paralPattern pps[MAX_GROUP_NUMBER]) {

	int i, j, numOfThreads, threshold;
	struct tThreadData *td;
	int unitSize, numOfWorkers;
	int totalDepCount = 0, totalDepVolume = 0;
	endpointType channelTypes[MAX_GROUP_NUMBER];
	paralPattern parPat;

	//FIXME: think about not adding reverse direction message channel between threads

	/* calculate minimum data flow sizes */
	for (i = 0; i < maxGroupId; i++) {
		if (pps[i] == PP_Pl || pps[i] == PP_EbC) {
			if (((minDataSizes[i] + maxDataSizes[i]) / 2) * (dataMigTotalRatio < activeProc->dataSharing[i].migratory * dataSharingRatio)) {
				channelTypes[i] = EP_SCALAR;
			} else {
				channelTypes[i] = EP_PACKET;
			}
		}
	}

	numOfThreads = activeProc->numOfThreads;

	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		threshold = (int) (td->cacheInvalid * cahceMissRatio);
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					totalDepCount++;
					totalDepVolume += td->dependency[j];
				}
			}
		}
	}

	/* find the data flow */
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		threshold = (int) (td->cacheInvalid * cahceMissRatio);
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					parPat = pps[td->groupId];
					switch (parPat) {
					case PP_TP:
						/* no need to message exchange */
						break;
					case PP_DaC:
						 /* The higher one is accepted as dependency and the other one is ignored */
						if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
							unitSize = td->dependency[j] * dataSharingRatio;
							addMcapiChannel(activeProc->threads[j], td, unitSize, EP_PACKET, 0, 0);
							addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfPcktChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_PKT);
							addMcapiMsgOp(td, td->mcapiExt.numOfPcktChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_PKT);
						}
						break;
					case PP_GD:
						td->mcapiExt.hasExt = true;
						activeProc->threads[j]->mcapiExt.hasExt = true;
						numOfWorkers = activeProc->threads[1]->mtapiExt.numOfChildren[td->groupId];
						unitSize = (int) ((double) (activeProc->dataSharing[td->groupId].prodCons * dataSharingRatio) / (double) numOfWorkers);
						addMcapiChannel(activeProc->threads[j], td, unitSize, EP_MESSAGE, 0, 0);
						if (j < i) { /* for preventing deadlock */
							addMcapiMsgOp(td, td->mcapiExt.numOfEndpoints - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_MSG);
						} else {
							addMcapiMsgOp(td, td->mcapiExt.numOfEndpoints - 1, 0, unitSize, 2, MSG_OP_RECV, MSG_TYPE_MSG);
						}
						if (j != 1) { /* not depends on main thread */
							if (i < j) { /* for preventing deadlock */
								addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, 1, MSG_OP_SEND, MSG_TYPE_MSG);
							} else {
								addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, 2, MSG_OP_SEND, MSG_TYPE_MSG);
							}
						} else { /* depends on main thread */
							addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, (td->groupId * MAX_WORK_PER_PHASE) + 1, MSG_OP_SEND, MSG_TYPE_MSG);
						}
						break;
					case PP_RD:
						activeProc->threads[j]->mcapiExt.hasExt = true;
						numOfWorkers = activeProc->threads[td->creator]->mtapiExt.numOfChildren[td->groupId];
						unitSize = (int) (activeProc->dataSharing[td->groupId].prodCons * ((double) td->dependency[j] / (double) totalDepVolume));
						addMcapiChannel(activeProc->threads[j], td, unitSize, EP_MESSAGE, 0, 0);
						addMcapiMsgOp(td, td->mcapiExt.numOfEndpoints - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_MSG);
						if (j != 1) {
							addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, 2, MSG_OP_SEND, MSG_TYPE_MSG);
						} else {
							addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, (td->groupId * MAX_WORK_PER_PHASE) + 2, MSG_OP_SEND, MSG_TYPE_MSG);
						}
						break;
					case PP_Pl:
						td->mcapiExt.hasExt = true;
						activeProc->threads[j]->mcapiExt.hasExt = true;
						if (j != 1) { /* except main */
							 /* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								if (channelTypes[td->groupId] == EP_PACKET) {
									unitSize = td->dependency[j] > minDataSizes[td->groupId] ? (int) ((double) td->dependency[j] / (double) minDataSizes[td->groupId]) : 1;
									addMcapiChannel(activeProc->threads[j], td, unitSize, EP_PACKET, 0, 0);
									addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfPcktChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_PKT);
									addMcapiMsgOp(td, td->mcapiExt.numOfPcktChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_PKT);
								} else {
									unitSize = getScalarSize(td->dependency[j], maxDataSizes[td->groupId]);
									addMcapiChannel(activeProc->threads[j], td, unitSize, EP_SCALAR, 0, 0);
									addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfScalChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_SCL);
									addMcapiMsgOp(td, td->mcapiExt.numOfScalChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_SCL);
								}
							}
						} else {
							//TODO: do similar to preparePosix
						}
						break;
					case PP_EbC:
						td->mcapiExt.hasExt = true;
						activeProc->threads[j]->mcapiExt.hasExt = true;
						if (j != 1) { /* except main */
							 /* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								if (channelTypes[td->groupId] == EP_PACKET) {
									unitSize = td->dependency[j] > minDataSizes[td->groupId] ? (int) ((double) td->dependency[j] / (double) minDataSizes[td->groupId]) : 1;
									addMcapiChannel(activeProc->threads[j], td, unitSize, EP_PACKET, 0, 0);
									addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfPcktChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_PKT);
									addMcapiMsgOp(td, td->mcapiExt.numOfPcktChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_PKT);
								} else {
									unitSize = getScalarSize(td->dependency[j], maxDataSizes[td->groupId]);
									addMcapiChannel(activeProc->threads[j], td, unitSize, EP_SCALAR, 0, 0);
									addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfScalChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_SCL);
									addMcapiMsgOp(td, td->mcapiExt.numOfScalChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_SCL);
								}
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
	}

	/* find the data flow for main thread */
	td = activeProc->threads[1]; /* main */
	for (i = 0; i < maxGroupId; i++) {
		if (pps[i] == PP_GD || pps[i] == PP_RD) {
			threshold = (int) (td->cacheInvalid * cahceMissRatio);
			for (j = 2; j <= numOfThreads; j++) {
				/* thread main depends on thread j */
				td->mcapiExt.hasExt = true;
				activeProc->threads[j]->mcapiExt.hasExt = true;
				numOfWorkers = activeProc->threads[1]->mtapiExt.numOfChildren[i];
				unitSize = (int) ((double) (activeProc->dataSharing[i].prodCons * dataSharingRatio) / (double) numOfWorkers);
				addMcapiChannel(activeProc->threads[j], td, unitSize, EP_MESSAGE, 0, 0);
				addMcapiMsgOp(td, td->mcapiExt.numOfEndpoints - 1, 0, unitSize, (i * MAX_WORK_PER_PHASE) + 2, MSG_OP_RECV, MSG_TYPE_MSG);
				addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
						activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, 2, MSG_OP_SEND, MSG_TYPE_MSG);
			}
		}
	}

	return;
}

void prepareMcapiSa() {

	int i, j, numOfThreads, threshold = 0;
	struct tThreadData *td;
	int unitSize;
	paralPattern parPat;

	//FIXME: think about not adding reverse direction message channel between threads

	numOfThreads = activeProc->numOfThreads;

	/* find the data flow */
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					parPat = td->standAloneExt.parPattern;
					switch (parPat) {
					case PP_TP:
						/* no need to message exchange */
						break;
					case PP_DaC:
						/* no need to message exchange */
						break;
					case PP_GD:
						td->mcapiExt.hasExt = true;
						activeProc->threads[j]->mcapiExt.hasExt = true;
						unitSize = td->standAloneExt.sharedDataSize;
						addMcapiChannel(activeProc->threads[j], td, unitSize, EP_MESSAGE, activeProc->threads[j]->standAloneExt.phase, td->standAloneExt.phase);
						if (j < i) { /* for preventing deadlock */
							addMcapiMsgOp(td, td->mcapiExt.numOfEndpoints - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_MSG);
						} else {
							addMcapiMsgOp(td, td->mcapiExt.numOfEndpoints - 1, 0, unitSize, 2, MSG_OP_RECV, MSG_TYPE_MSG);
						}
						if (j != 1) {
							if (i < j) { /* for preventing deadlock */
								addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, 1, MSG_OP_SEND, MSG_TYPE_MSG);
							} else {
								addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, 2, MSG_OP_SEND, MSG_TYPE_MSG);
							}
						} else {
							addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, (td->groupId * MAX_WORK_PER_PHASE) + 1, MSG_OP_SEND, MSG_TYPE_MSG);
						}
						break;
					case PP_RD:
						activeProc->threads[j]->mcapiExt.hasExt = true;
						td->mcapiExt.hasExt = true;
						unitSize = td->standAloneExt.sharedDataSize;
						addMcapiChannel(activeProc->threads[j], td, unitSize, EP_MESSAGE, activeProc->threads[j]->standAloneExt.phase, td->standAloneExt.phase);
						addMcapiMsgOp(td, td->mcapiExt.numOfEndpoints - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_MSG);
						if (j != 1) {
							addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, 2, MSG_OP_SEND, MSG_TYPE_MSG);
						} else {
							addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfEndpoints - 2,
									activeProc->threads[j]->mcapiExt.numOfEndpoints - 1, unitSize, (td->groupId * MAX_WORK_PER_PHASE) + 2, MSG_OP_SEND, MSG_TYPE_MSG);
						}
						break;
					case PP_Pl:
						td->mcapiExt.hasExt = true;
						activeProc->threads[j]->mcapiExt.hasExt = true;
						if (j != 1) { /* except main */
							 /* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								unitSize = td->standAloneExt.sharedDataSize / td->standAloneExt.loopCount;
								if (unitSize <= MAX_PACKET_SIZE) {
									addMcapiChannel(activeProc->threads[j], td, unitSize, EP_PACKET, activeProc->threads[j]->standAloneExt.phase, td->standAloneExt.phase);
									addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfPcktChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_PKT);
									addMcapiMsgOp(td, td->mcapiExt.numOfPcktChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_PKT);
								} else {
									addMcapiChannel(activeProc->threads[j], td, unitSize, EP_SCALAR, activeProc->threads[j]->standAloneExt.phase, td->standAloneExt.phase);
									addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfScalChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_SCL);
									addMcapiMsgOp(td, td->mcapiExt.numOfScalChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_SCL);
								}
							}
						}
						break;
					case PP_EbC:
						td->mcapiExt.hasExt = true;
						activeProc->threads[j]->mcapiExt.hasExt = true;
						if (j != 1) { /* except main */
							 /* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								unitSize = td->standAloneExt.sharedDataSize;
								if (unitSize <= MAX_PACKET_SIZE) {
									addMcapiChannel(activeProc->threads[j], td, unitSize, EP_PACKET, activeProc->threads[j]->standAloneExt.phase, td->standAloneExt.phase);
									addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfPcktChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_PKT);
									addMcapiMsgOp(td, td->mcapiExt.numOfPcktChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_PKT);
								} else {
									addMcapiChannel(activeProc->threads[j], td, unitSize, EP_SCALAR, activeProc->threads[j]->standAloneExt.phase, td->standAloneExt.phase);
									addMcapiMsgOp(activeProc->threads[j], activeProc->threads[j]->mcapiExt.numOfScalChans - 1, 0, unitSize, 2, MSG_OP_SEND, MSG_TYPE_SCL);
									addMcapiMsgOp(td, td->mcapiExt.numOfScalChans - 1, 0, unitSize, 1, MSG_OP_RECV, MSG_TYPE_SCL);
								}
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
	}

	return;
}

void preparePosix(paralPattern pps[MAX_GROUP_NUMBER]) {

	int i, j, numOfThreads, threshold;
	//int k, unitWork, workSize, memSize;
	struct tThreadData *td;
	paralPattern parPat;
	int resId, unitSize, numOfWorkers, totalDepCount = 0, mainDepCount = 0;
	int totalDepVolume = 0;
	int prodConsSizes[MAX_GROUP_NUMBER] = { 0 };
	int migSizes[MAX_GROUP_NUMBER] = { 0 };
	int startIndexes[MAX_GROUP_NUMBER] = { 0 };

	td = getCreatorOfMaxThreads();

	/* find the data flow */
	for (i = 0; i < maxGroupId; i++) {
		if (pps[i] == PP_GD || pps[i] == PP_RD) {
			td->lsExt.hasExt = true;
			prodConsSizes[i] = (activeProc->dataSharing[i].prodCons * dataSharingRatio) + PROD_CONS_MIN;
			resId = addGlobalMem(prodConsSizes[i]);
//			addPosixSemBetweenAll(td, nextSemKey(), false);
			addTlsMemOp(td, resId, 0, prodConsSizes[i], (i * MAX_WORK_PER_PHASE) + 0, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
			addTlsMemOp(td, resId, 0, prodConsSizes[i], (i * MAX_WORK_PER_PHASE) + 3, MEM_OP_READ, MEM_TYPE_GLOBAL);
		} else if (pps[i] == PP_Pl) {
			migSizes[i] = activeProc->dataSharing[i].migratory * dataSharingRatio;
		}
	}

	numOfThreads = activeProc->numOfThreads;

	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		threshold = (int) (td->cacheInvalid * cahceMissRatio);
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					totalDepCount++;
					totalDepVolume += td->dependency[j];
				}
			}
		}
	}

	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		threshold = (int) (td->cacheInvalid * cahceMissRatio);
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					parPat = pps[td->groupId];
					switch (parPat) {
					case PP_TP:
						/* no need to shared memory */
						break;
					case PP_DaC:
						 /* The higher one is accepted as dependency and the other one is ignored */
						if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
							td->lsExt.hasExt = true;
							activeProc->threads[j]->lsExt.hasExt = true;
							unitSize =  td->dependency[j] * dataSharingRatio;
							resId = addGlobalMem(unitSize);
							addTlsMemOp(activeProc->threads[j], resId, 0, unitSize, 2, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
							addTlsMemOp(td, resId, 0, unitSize, 1, MEM_OP_READ, MEM_TYPE_GLOBAL);
						}
						/* no need to shared memory */
						break;
					case PP_GD:
						td->lsExt.hasExt = true;
						activeProc->threads[j]->lsExt.hasExt = true;
						/* memory addition already done */
						numOfWorkers = totalDepCount; // FIXME: I can change this in the future (numOfWorkers = activeProc->threads[1]->mtapiExt.numOfChildren[td->groupId];)
						unitSize = (int) ((double) (prodConsSizes[td->groupId]) / (double) numOfWorkers);
						if (startIndexes[td->groupId] + unitSize < prodConsSizes[td->groupId]) {
							unitSize++;
						}
						addTlsMemOp(td, activeProc->lsExt.numOfGlobMems - 1, startIndexes[td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_GLOBAL);
						//FIXME: for #reads>#writes, remove the write operation
//						addTlsMemOp(td, activeProc->lsExt.numOfGlobMems - 1, startIndexes[td->groupId], unitSize, 3, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
						startIndexes[td->groupId] += unitSize;
						break;
					case PP_RD:
						td->lsExt.hasExt = true;
						activeProc->threads[j]->lsExt.hasExt = true;
						/* memory addition already done */
						numOfWorkers = activeProc->threads[td->creator]->mtapiExt.numOfChildren[td->groupId];
						unitSize = (int) (prodConsSizes[td->groupId] * ((double) td->dependency[j] / (double) totalDepVolume));
						//FIXME: for #reads>#writes, remove the write operation
//						addTlsMemOp(activeProc->threads[j], activeProc->lsExt.numOfGlobMems - 1, startIndexes[td->groupId], unitSize, 3, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
						addTlsMemOp(td, activeProc->lsExt.numOfGlobMems - 1, startIndexes[td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_GLOBAL);
						startIndexes[td->groupId] += unitSize;
						break;
					case PP_Pl:
						//TODO: check cycle that causes deadlock
						if (j != 1) { /* except main */
							/* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								td->lsExt.hasExt = true;
								activeProc->threads[j]->lsExt.hasExt = true;
								unitSize = td->dependency[j] > minDataSizes[td->groupId] ? (int) ((double) td->dependency[j] / (double) minDataSizes[td->groupId]) : 1;
								unitSize = unitSize * (migSizes[td->groupId] * migDataSharingRatio);
								if (unitSize > 1) {
									resId = addGlobalMem(unitSize);
//									addPosixSemBetweenTwo(activeProc->threads[j], td, nextSemKey(), true); /* create and get the semaphore (initially locked) */
//									addTlsOrderedMemOp(activeProc->threads[j], resId, 0, unitSize, 2,
//											MEM_OP_WRITE, MEM_TYPE_GLOBAL, activeProc->threads[j]->posixExt.numOfSemaphores - 1); /* unlock after write */
//									addTlsOrderedMemOp(td, resId, 0, unitSize, 1,
//											MEM_OP_READ, MEM_TYPE_GLOBAL, td->posixExt.numOfSemaphores - 1); /* lock before read */
									addTlsMemOp(activeProc->threads[j], resId, 0, unitSize, 2, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
									addTlsMemOp(td, resId, 0, unitSize, 1, MEM_OP_READ, MEM_TYPE_GLOBAL);
								}
							}
						} else {
							if (mainDepCount < totalDepCount / 5) {
								td->lsExt.hasExt = true;
								activeProc->threads[j]->lsExt.hasExt = true;
								unitSize = td->dependency[j] > minDataSizes[td->groupId] ? (int) ((double) td->dependency[j] / (double) minDataSizes[td->groupId]) : 1;
								unitSize = unitSize * (migSizes[td->groupId] * migDataSharingRatio);
								if (unitSize > 1) {
									resId = addGlobalMem(unitSize);
//									addPosixSemBetweenTwo(activeProc->threads[j], td, nextSemKey(), true); /* create and get the semaphore (initially locked) */
//									addTlsOrderedMemOp(activeProc->threads[j], resId, 0, unitSize, 2,
//											MEM_OP_WRITE, MEM_TYPE_GLOBAL, activeProc->threads[j]->posixExt.numOfSemaphores - 1); /* unlock after write */
//									addTlsOrderedMemOp(td, resId, 0, unitSize, 1,
//											MEM_OP_READ, MEM_TYPE_GLOBAL, td->posixExt.numOfSemaphores - 1); /* lock before read */
									addTlsMemOp(activeProc->threads[j], resId, 0, unitSize, 2, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
									addTlsMemOp(td, resId, 0, unitSize, 1, MEM_OP_READ, MEM_TYPE_GLOBAL);
									mainDepCount++;
								}
							}
						}
						break;
					case PP_EbC:
						if (j != 1) { /* except main */
							 /* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								td->lsExt.hasExt = true;
								activeProc->threads[j]->lsExt.hasExt = true;
								unitSize =  td->dependency[j] > minDataSizes[td->groupId] ? (int) ((double) td->dependency[j] / (double) minDataSizes[td->groupId]) : 1;
								resId = addGlobalMem(unitSize);
//								addPosixSemBetweenTwo(activeProc->threads[j], td, nextSemKey(), true); /* create and get the semaphore (initially locked) */
//								addTlsOrderedMemOp(activeProc->threads[j], resId, 0, unitSize, 2,
//										MEM_OP_WRITE, MEM_TYPE_GLOBAL, activeProc->threads[j]->posixExt.numOfSemaphores - 1); /* unlock after write */
//								addTlsOrderedMemOp(td, resId, 0, unitSize, 1,
//										MEM_OP_READ, MEM_TYPE_GLOBAL, td->posixExt.numOfSemaphores - 1); /* lock before read */
								addTlsMemOp(activeProc->threads[j], resId, 0, unitSize, 2, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
								addTlsMemOp(td, resId, 0, unitSize, 1, MEM_OP_READ, MEM_TYPE_GLOBAL);

							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
	}

	return;
}

void preparePosixSa() {

	int i, j, numOfThreads, threshold = 0;
	struct tThreadData *td;
	paralPattern parPat;
	int resId, totalSize, unitSize;
	int startIndexes[MAX_THREADS][MAX_GROUP_NUMBER] = { { 0 } };

	numOfThreads = activeProc->numOfThreads;

	/* find the data flow */
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		maxGroupId = getMaxGroupId2(td);
		for (j = 0; j < maxGroupId; j++) {
			if (td->mtapiExt.numOfChildren[j] > 0) {
				if (td->standAloneExt.phasePatterns[j] == PP_GD || td->standAloneExt.phasePatterns[j] == PP_RD) {
					td->lsExt.hasExt = true;
					totalSize = getTotalSharedDataSize(td, j + 1);
					resId = addGlobalMem(totalSize);
					addTlsMemOp(td, resId, 0, totalSize, (j * MAX_WORK_PER_PHASE) + 0, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
					addTlsMemOp(td, resId, 0, totalSize, (j * MAX_WORK_PER_PHASE) + 3, MEM_OP_READ, MEM_TYPE_GLOBAL);
				}
			}
		}
	}

	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId || td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold && i != 1) { /* except main */
					/* thread i depends on thread j */
					parPat = td->standAloneExt.parPattern;
					switch (parPat) {
					case PP_TP:
						/* no need to shared memory */
						break;
					case PP_DaC:
						/* no need to shared memory */
						break;
					case PP_GD:
						/* memory addition already done */
						td->lsExt.hasExt = true;
						activeProc->threads[j]->lsExt.hasExt = true;
						unitSize = td->standAloneExt.sharedDataSize;
						addTlsMemOp(td, activeProc->lsExt.numOfGlobMems - 1, startIndexes[td->creator][td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_GLOBAL);
						//FIXME: for #reads>#writes, remove the write operation
//						addTlsMemOp(td, activeProc->lsExt.numOfGlobMems - 1, startIndexes[td->creator][td->groupId], unitSize, 3, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
						startIndexes[td->creator][td->groupId] += unitSize;
						break;
					case PP_RD:
						/* memory addition already done */
						td->lsExt.hasExt = true;
						activeProc->threads[j]->lsExt.hasExt = true;
						unitSize = td->standAloneExt.sharedDataSize;
						//FIXME: for #reads>#writes, remove the write operation
//						addTlsMemOp(activeProc->threads[j], activeProc->lsExt.numOfGlobMems - 1, startIndexes[td->creator][td->groupId], unitSize, 3, MEM_OP_WRITE, MEM_TYPE_GLOBAL);
						addTlsMemOp(td, activeProc->lsExt.numOfGlobMems - 1, startIndexes[td->creator][td->groupId], unitSize, 0, MEM_OP_READ, MEM_TYPE_GLOBAL);
						startIndexes[td->creator][td->groupId] += unitSize;
						break;
					case PP_Pl:
						if (j != 1) { /* except main */
							/* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								td->lsExt.hasExt = true;
								activeProc->threads[j]->lsExt.hasExt = true;
								unitSize = td->standAloneExt.sharedDataSize / td->standAloneExt.loopCount;
								resId = addGlobalMem(unitSize);
								addPosixSemBetweenTwo(activeProc->threads[j], td, nextSemKey(), true); /* create and get the semaphore (initially locked) */
								addTlsOrderedMemOp(activeProc->threads[j], resId, 0, unitSize, 2,
										MEM_OP_WRITE, MEM_TYPE_GLOBAL, activeProc->threads[j]->posixExt.numOfSemaphores - 1); /* unlock after write */
								addTlsOrderedMemOp(td, resId, 0, unitSize, 1,
										MEM_OP_READ, MEM_TYPE_GLOBAL, td->posixExt.numOfSemaphores - 1); /* lock before read */
							}
						}
						break;
					case PP_EbC:
						if (j != 1) { /* except main */
							 /* The higher one is accepted as dependency and the other one is ignored */
							if (td->dependency[j] > activeProc->threads[j]->dependency[i]) {
								td->lsExt.hasExt = true;
								activeProc->threads[j]->lsExt.hasExt = true;
								unitSize = td->standAloneExt.sharedDataSize;
								resId = addGlobalMem(unitSize);
								addPosixSemBetweenTwo(activeProc->threads[j], td, nextSemKey(), true); /* create and get the semaphore (initially locked) */
								addTlsOrderedMemOp(activeProc->threads[j], resId, 0, unitSize, 2,
										MEM_OP_WRITE, MEM_TYPE_GLOBAL, activeProc->threads[j]->posixExt.numOfSemaphores - 1); /* unlock after write */
								addTlsOrderedMemOp(td, resId, 0, unitSize, 1,
										MEM_OP_READ, MEM_TYPE_GLOBAL, td->posixExt.numOfSemaphores - 1); /* lock before read */
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
	}

	return;
}

/*
 * We decide the followings for the synthetic benchmark according to parallel pattern(s) and thread information
 * A- Data Sharing (private, producer/consumer, migratory, read-only)
 * B- Thread Communication (dependency)
 * C- General Threading (lifetime, insCount)
 * D- Synchronization/Concurrency
 */

//FIXME: think about multiple phases in the threads. Will worker threads have multiple phases?
		// TLS, MTAPI, MRAPI, MCAPI.

void setBenchPrepGlobalParams() {

	int dataCount = 0;

	dataSharingRatio = 0;

	if (hasThreadMrapiExt) {
		dataSharingRatio += mrapiDataSharingRatio;
		dataCount++;
	}
	if (hasThreadMcapiExt) {
		dataSharingRatio += mcapiDataSharingRatio;
		dataCount++;
	}
	if (hasThreadPosixExt) {
		dataSharingRatio += posixDataSharingRatio;
		dataCount++;
	}

	dataSharingRatio = dataSharingRatio / dataCount;

	/* set default values from sharing ratio */
	dataPrivateRatio = dataSharingRatio;
	dataReadonlyRatio = dataSharingRatio;

	return;
}

int prepBenchmark(int phaseCount, paralPattern pps[MAX_GROUP_NUMBER], boolean isGlobalParamsInit) {

	semKey = ftok("/dev/null",'s');
	mtxKey = ftok("/dev/null",'m');
	shmKey = ftok("/dev/null",'h');

	activeProc = procs[0];

	if (isGlobalParamsInit) {
		setBenchPrepGlobalParams();
	}

	initMxapiData();

	prepareMtapi(pps);

	prepareTls(pps);

	if (hasThreadMrapiExt) {
		prepareMrapi(pps);
	}

	if(hasThreadMcapiExt) {
		prepareMcapi(pps);
	}

	if (hasThreadPosixExt) {
		preparePosix(pps);
	}

	return 0;
}

void convertSaToSyn() {

	int i, j, numOfThreads;
	struct tThreadData *td, *itTd;
	int pc = 100, targetStage;
	paralPattern pp;

	numOfThreads = activeProc->numOfThreads;

	for (i = 2; i <= numOfThreads; i++) { /* except main */
		td = activeProc->threads[i];
		/* assign PC and set dependencies */
		if (td->pc == -1) {
			pp = td->standAloneExt.parPattern;
			td->pc = pc++;
			if (pp == PP_GD || pp == PP_RD) {
				for (j = i; j <= numOfThreads; j++) {
					itTd = activeProc->threads[j];
					if (td->creator == itTd->creator && td->standAloneExt.phase == itTd->standAloneExt.phase) {
						itTd->pc = pc;
						itTd->dependency[itTd->creator] = 1;
						activeProc->threads[itTd->creator]->dependency[i] = 1;
					}
				}
			} else if (pp == PP_Pl || pp == PP_EbC) {
				if (td->standAloneExt.stage > 1) {
					targetStage = td->standAloneExt.stage - 1;
					for (j = 2; j <= numOfThreads; j++) {
						itTd = activeProc->threads[j];
						if (td->creator == itTd->creator && td->standAloneExt.phase == itTd->standAloneExt.phase) {
							if (itTd->standAloneExt.stage == targetStage) {
								td->dependency[j] = 1;
							}
						}
					}
				}
			}
		}
//		/* assign max group id */
//		if (td->standAloneExt.phase > maxGroupId) {
//			maxGroupId = td->standAloneExt.phase;
//		}
	}

	return;
}

int prepBenchmarkSa(boolean isGlobalParamsInit) {

	semKey = ftok("/dev/null",'s');
	mtxKey = ftok("/dev/null",'m');
	shmKey = ftok("/dev/null",'h');

	activeProc = procs[0];

	if (isGlobalParamsInit) {
		setBenchPrepGlobalParams();
	}

	initMxapiData();

	convertSaToSyn();

	prepareMtapiSa();

	prepareTlsSa();

	if (hasThreadMrapiExt) {
		prepareMrapiSa();
	}

	if(hasThreadMcapiExt) {
		prepareMcapiSa();
	}

	if (hasThreadPosixExt) {
		preparePosixSa();
	}

	return 0;
}

int prepBenchmarkGpu(int phaseCount, paralPattern pps[MAX_GROUP_NUMBER]) {

	activeProc = procs[0];

	//TODO: prepare gpu benchmark based on dwarf

	switch (pps[0]) {
	case DW_DLA:

		break;
	case DW_SLA:

		break;
	case DW_SM:

		break;
	case DW_NBM:

		break;
	case DW_SG:

		break;
	case DW_UG:

		break;
	case DW_MR:

		break;
	case DW_CL:

		break;
	case DW_GT:

		break;
	case DW_DP:

		break;
	case DW_BBB:

		break;
	case DW_GM:

		break;
	case DW_FSM:

		break;
	default:
		/* no operations */
		break;
	}

	return 0;
}
