/*********************************************************************
 * Copyright (c) 2011-2013 Etem Deniz <etem.deniz@boun.edu.tr>       *
 *                                                                   *
 * Permission is hereby granted, free of charge, to any person       *
 * obtaining a copy of this software and associated documentation    *
 * files (the "Software"), to deal in the Software without           *
 * restriction, including without limitation the rights to use,      *
 * copy, modify, merge, publish, distribute, sublicense, and/or sell *
 * copies of the Software, and to permit persons to whom the         *
 * Software is furnished to do so, subject to the following          *
 * conditions:                                                       *
 *                                                                   *
 * The above copyright notice and this permission notice shall be    *
 * included in all copies or substantial portions of the Software.   *
 *                                                                   *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,   *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     *
 * OTHER DEALINGS IN THE SOFTWARE.                                   *
 *********************************************************************/
/*
 * Module Name:
 *     client-aio.c
 *
 * Description:
 *     Umbra client to gather characteristics.
 *
 * Author: 
 *     Etem Deniz
 * 
 * Date:
 *     11/01/2011
 * 
 * Note:
 *     11/01/2011: It currently only works in Linux x86_64.
 *                 It currently only works in thread private code cache
 */

#ifdef UMBRA_CLIENT_AIO

#include <stddef.h>    /* offsetof */
#include <string.h>    /* memset */
#include <unistd.h>    /* sleep */
#include "dr_api.h"
#include "dr_tools.h"
#include "../core/global.h"
#include "../core/table.h"
#include "../core/utils.h"

/* CACHE LINE GRANULARITY */
#define SHARE_UNIT_BITS 6
#define CACHE_LINE_SIZE 64
#define CACHE_LINE_BITS 6
#define CACHE_MISS_IDX  0
#define CACHE_INVD_IDX  1
#define FALSE_SHARE_IDX 2
#define LOCATION_READ_BIT  (0x1 << 20)
#define LOCATION_WRITE_BIT (0x1 << 21)
#define NUM_OF_REGS 16
#define CLIENT_CODE_CACHE_SIZE (PAGE_SIZE << 4)
#define MSG_LEN 128
#define CACHE_DUMP_GRANULARITY 1024

#define MAX_NUM_THREADS 32

#define NULL_TERMINATE(buf) buf[(sizeof(buf)/sizeof(buf[0])) - 1] = '\0'

typedef struct _client_proc_data_t {
	int num_threads;
	int num_active_threads;
	int group_id;
	void *lock;
	reg_t total_mem;
	reg_t total_private;
	reg_t total_prod_cons;
	reg_t total_migratory;
	reg_t total_readonly;
	reg_t stats[4];
	reg_t ins_count;
	int num_locks;
	int num_barriers;
	int num_conditions;
	unsigned int thread_id;
	reg_t load_counter;
	reg_t store_counter;
}client_proc_data_t;
client_proc_data_t client_proc_data;

typedef struct _msg_data_t {
	unsigned int msg_count;
	unsigned int total_size;
}msg_data_t;

typedef struct _client_tls_data_t {
	unsigned int tid_map;
	unsigned int tid_map_write;
	unsigned int tid_map_read;
	unsigned int myid;
	reg_t stats[4];
	reg_t cor_counter[MAX_NUM_THREADS];
	reg_t dep_counter[MAX_NUM_THREADS];
//	msg_data_t msgsend_data[MAX_NUM_THREADS];
//	msg_data_t imsgsend_data[MAX_NUM_THREADS];
	uint64 ins_count;
	dr_time_t creation_time;
	dr_time_t exit_time;
}client_tls_data_t;

typedef struct _shadow_data_t {
	unsigned int tid_map;
	unsigned int tid_map_write;
	unsigned int tid_map_read;
	unsigned int thd;
}shadow_data_t;

int bit_count[16] = {0, 1, 1, 2,
	1, 2, 2, 3,
	1, 2, 2, 3,
	2, 3, 3, 4};
int thd_count[256];

//static void dump_dependency(client_tls_data_t *tls_data, uint64 dep_counter) {
//	int i = 0;
//	dr_time_t time;
//
//	if (dep_counter % CACHE_DUMP_GRANULARITY == 0) {
//		dr_mutex_lock(client_proc_data.lock);
//		dr_get_time(&time);
//		if (tls_data->dep_counter[i] != 0) {
//			dr_fprintf(proc_info.log, "Partial dependency of Thread %d, %d.%d.%d.%d: ", tls_data->myid,
//				time.hour, time.minute, time.second, time.milliseconds);
//			dr_fprintf(proc_info.log, "[%d] %llu", i, tls_data->dep_counter[i]);
//		}
//		for (i = 1; i < MAX_NUM_THREADS; i++) {
//			if (tls_data->dep_counter[i] != 0)
//			dr_fprintf(proc_info.log, ", [%d] %llu", i, tls_data->dep_counter[i]);
//		}
//		dr_fprintf(proc_info.log, "\n");
//		dr_mutex_unlock(client_proc_data.lock);
//	}
//	return;
//}

/*************************client-share-anal*****************************/

static void
instrument_memory_read_sa(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	instr_t *instr, *label;
	opnd_t opnd1, opnd2;
	reg_id_t reg = umbra_info->steal_regs[0];
	client_tls_data_t *tls_data;

	tls_data = umbra_info->client_tls_data;

	label = INSTR_CREATE_label(drcontext);
	instrlist_meta_preinsert(ilist, where, label);

	opnd1 = OPND_CREATE_ABSMEM(&client_proc_data.load_counter, OPSZ_8);
	instr = INSTR_CREATE_inc(drcontext, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);

	/* test [%reg].tid_map_read, tid_map_read*/
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, tid_map_read),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32(tls_data->tid_map_read);
	instr = INSTR_CREATE_test(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	instr_set_ok_to_mangle(instr, true);
	instr_set_translation(instr, ref->pc);
	instr_set_meta_may_fault(instr, true);

	/* jnz label */
	opnd1 = opnd_create_instr(label);
	instr = INSTR_CREATE_jcc(drcontext, OP_jnz, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);
	/* racy or */
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, tid_map_read),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32(tls_data->tid_map_read);
	instr = INSTR_CREATE_or(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	instr_set_ok_to_mangle(instr, true);
	instr_set_translation(instr, ref->pc);
	instr_set_meta_may_fault(instr, true);
}

static void
instrument_memory_write_sa(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	instr_t *instr, *label;
	opnd_t opnd1, opnd2;
	reg_id_t reg = umbra_info->steal_regs[0];
	client_tls_data_t *tls_data;

	tls_data = umbra_info->client_tls_data;

	label = INSTR_CREATE_label(drcontext);
	instrlist_meta_preinsert(ilist, where, label);

	opnd1 = OPND_CREATE_ABSMEM(&client_proc_data.store_counter, OPSZ_8);
	instr = INSTR_CREATE_inc(drcontext, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);

	/* test [%reg].tid_map_write, tid_map_write*/
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, tid_map_write),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32(tls_data->tid_map_write);
	instr = INSTR_CREATE_test(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	instr_set_ok_to_mangle(instr, true);
	instr_set_translation(instr, ref->pc);
	instr_set_meta_may_fault(instr, true);

	/* jnz label */
	opnd1 = opnd_create_instr(label);
	instr = INSTR_CREATE_jcc(drcontext, OP_jnz, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);
	/* racy or */
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, tid_map_write),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32(tls_data->tid_map_write);
	instr = INSTR_CREATE_or(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	instr_set_ok_to_mangle(instr, true);
	instr_set_translation(instr, ref->pc);
	instr_set_meta_may_fault(instr, true);
}

static void
instrument_memory_modify_sa(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	instrument_memory_write_sa(drcontext,
			umbra_info,
			ref,
			ilist,
			where);
}

static void
instrument_update_sa(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	if (ref->type == MemRead)
	instrument_memory_read_sa(drcontext,
			umbra_info,
			ref,
			ilist,
			where);
	else if (ref->type == MemWrite)
	instrument_memory_write_sa(drcontext,
			umbra_info,
			ref,
			ilist,
			where);
	else if (ref->type == MemModify)
	instrument_memory_modify_sa(drcontext,
			umbra_info,
			ref,
			ilist,
			where);
}

static void
umbra_client_thread_init_sa(void *drcontext, umbra_info_t *umbra_info)
{
	client_tls_data_t *tls_data = ((client_tls_data_t *) umbra_info->client_tls_data);

	/* bit 0 is always set as for total memory referenced */
	tls_data->tid_map_read = (1 << ((client_proc_data.num_threads) % MAX_NUM_THREADS));
	tls_data->tid_map_write = (1 << ((client_proc_data.num_threads) % MAX_NUM_THREADS));
}

static void
umbra_client_exit_sa()
{
	dr_mutex_lock(client_proc_data.lock);
	dr_fprintf(proc_info.log,
			"total private of group %d: %llu\n"
			"total producer/consumer of group %d: %llu\n"
			"total migratory of group %d: %llu\n"
			"total readonly of group %d: %llu\n"
			"total cacheline of group %d: %llu\n"
			"total mem of group %d: %llu\n",
			client_proc_data.group_id, client_proc_data.total_private,
			client_proc_data.group_id, client_proc_data.total_prod_cons,
			client_proc_data.group_id, client_proc_data.total_migratory,
			client_proc_data.group_id, client_proc_data.total_readonly,
			client_proc_data.group_id,			
			client_proc_data.total_private +
			client_proc_data.total_prod_cons +
			client_proc_data.total_migratory + 
			client_proc_data.total_readonly,
			client_proc_data.group_id, client_proc_data.total_mem);

	dr_fprintf(proc_info.log, "Total Load: %llu\n", client_proc_data.load_counter);
	dr_fprintf(proc_info.log, "Total Store: %llu\n", client_proc_data.store_counter);

	dr_mutex_unlock(client_proc_data.lock);
}

static int
num_of_threads(uint tid_map)
{
	byte *map = (byte *)&tid_map;
	return (thd_count[map[0]] +
			thd_count[map[1]] +
			thd_count[map[2]] +
			thd_count[map[3]]);
}

static void
shadow_memory_create_sa(memory_map_t *map)
{
	/*dr_time_t time;
	
	dr_mutex_lock(client_proc_data.lock);
	dr_get_time(&time);
	dr_fprintf(proc_info.log,
	"In map create: %p - %p; %d.%d.%d.%d\n",
	map->app_base, map->app_end, time.hour, time.minute, 
	time.second, time.milliseconds);
	dr_mutex_unlock(client_proc_data.lock);*/
}

static void
shadow_memory_remove_sa(memory_map_t *map)
{
	int private, prod_cons, migratory, readonly;
	app_pc addr;
	int num_of_reads = 0, num_of_writes = 0;
/*	dr_time_t time;*/
	
	private = 0;
	prod_cons = 0;
	migratory = 0;
	readonly = 0;
	for (addr = (app_pc)map->shd_base[0];
			addr < (app_pc)map->shd_end[0];
			addr += (1 << SHARE_UNIT_BITS)) {
		shadow_data_t *ptr = (shadow_data_t *)addr;
		if (ptr->tid_map_read != 0 || ptr->tid_map_write != 0) {
			num_of_reads = num_of_threads(ptr->tid_map_read);
			num_of_writes = num_of_threads(ptr->tid_map_write);
			if ((num_of_reads == 1 && num_of_writes == 1) && (ptr->tid_map_read == ptr->tid_map_write)) {
				// if ((ptr->tid_map_read & 1) != 1) { /* except main */
				private++;
				// }
			} else if (num_of_writes == 0) {
				readonly++;
			} else if (num_of_reads > num_of_writes) {
				prod_cons++;
			} else {
				migratory++;
			}
		}

	}
	dr_mutex_lock(client_proc_data.lock);
/*	dr_get_time(&time);
	if (num_of_reads != 0 || num_of_writes != 0) {
		dr_fprintf(proc_info.log,
		"In map remove: %p - %p; %d.%d.%d.%d; "
		"private: %d, producer/consumer: %d, migratory: %d, readonly: %d\n",
		map->app_base, map->app_end, time.hour, time.minute, 
		time.second, time.milliseconds,
		private, prod_cons, migratory, readonly);
	}*/
	client_proc_data.total_private += private;
	client_proc_data.total_prod_cons += prod_cons;
	client_proc_data.total_migratory += migratory;
	client_proc_data.total_readonly += readonly;
	client_proc_data.total_mem += ((reg_t)map->app_end -
			(reg_t)map->app_base);
	dr_mutex_unlock(client_proc_data.lock);
}

/*****************************client-tcd*****************************/

static void
instrument_memory_read_tcd(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	instr_t *instr, *label;
	opnd_t opnd1, opnd2;
	reg_id_t reg = umbra_info->steal_regs[0];
	client_tls_data_t *tls_data;

	tls_data = umbra_info->client_tls_data;

	label = INSTR_CREATE_label(drcontext);
	instrlist_meta_preinsert(ilist, where, label);

	/* check if my bit is in it */
	/* test [%reg].tid_map, tid_map */
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, tid_map),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32((int)tls_data->tid_map);
	instr = INSTR_CREATE_test(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	instr_set_ok_to_mangle(instr, true);
	instr_set_translation(instr, ref->pc);
	instr_set_meta_may_fault(instr, true);

	/* if in, do nothing jnz label */
	opnd1 = opnd_create_instr(label);
	instr = INSTR_CREATE_jcc(drcontext, OP_jnz, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);
	/* it is a miss */
	/* add myself into the bitmap in shadow memory, or */
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, tid_map),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32((int)tls_data->tid_map);
	instr = INSTR_CREATE_or(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	instr_set_ok_to_mangle(instr, true);
	instr_set_translation(instr, ref->pc);
	instr_set_meta_may_fault(instr, true);

	/* update the thread correlation counter */
	/* get the array index */
	opnd1 = opnd_create_reg(reg_64_to_32(reg));
	opnd2 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, thd),
			OPSZ_4);
	instr = INSTR_CREATE_mov_ld(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);

	/* inc correlation counter */
	opnd1 = opnd_create_base_disp(DR_REG_NULL, reg, 8,
			(reg_t)tls_data->cor_counter,
			OPSZ_8);
	instr = INSTR_CREATE_inc(drcontext, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);
	
	/* inc dependency counter */
	opnd1 = opnd_create_base_disp(DR_REG_NULL, reg, 8,
			(reg_t)tls_data->dep_counter,
			OPSZ_8);
	instr = INSTR_CREATE_inc(drcontext, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);
	
	/* increment the cache miss counter */
	opnd1 = OPND_CREATE_ABSMEM(&ref->note[CACHE_MISS_IDX], OPSZ_4);
	instr = INSTR_CREATE_inc(drcontext, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);

	/* call dump dependency */
//	dr_insert_clean_call(drcontext, ilist, instr,
//		(void *)dump_dependency, false /* save fpstate */, 2,
//		OPND_CREATE_INTPTR(tls_data),
//		opnd_create_base_disp(DR_REG_NULL, reg, 8,
//			(reg_t)tls_data->dep_counter,
//			OPSZ_8));
}

static void
instrument_memory_write_tcd(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	instr_t *instr, *label;
	opnd_t opnd1, opnd2;
	reg_id_t reg = umbra_info->steal_regs[0];
	client_tls_data_t *tls_data;

	tls_data = umbra_info->client_tls_data;

	label = INSTR_CREATE_label(drcontext);
	instrlist_meta_preinsert(ilist, where, label);
	/* check if I am the exclusive owner: cmp [%reg].tid_map, tid_map*/
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, tid_map),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32((int)tls_data->tid_map);
	instr = INSTR_CREATE_cmp(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	instr_set_ok_to_mangle(instr, true);
	instr_set_translation(instr, ref->pc);
	instr_set_meta_may_fault(instr, true);

	/* if yes, do nothing je label */
	opnd1 = opnd_create_instr(label);
	instr = INSTR_CREATE_jcc(drcontext, OP_je, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);
	/* else, set me as */
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, tid_map),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32((int)tls_data->tid_map);
	instr = INSTR_CREATE_mov_st(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	instr_set_ok_to_mangle(instr, true);
	instr_set_translation(instr, ref->pc);
	instr_set_meta_may_fault(instr, true);

	reg_id_t r2 = umbra_info->steal_regs[1];
	/* update the thread correlation counter */
	opnd1 = opnd_create_reg(reg_64_to_32(r2));
	opnd2 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, thd),
			OPSZ_4);
	instr = INSTR_CREATE_mov_ld(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);

	/* inc */
	opnd1 = opnd_create_base_disp(DR_REG_NULL, r2, 8,
			(reg_t)tls_data->cor_counter,
			OPSZ_8);
	instr = INSTR_CREATE_inc(drcontext, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);
	/* update with my own id */
	opnd1 = opnd_create_base_disp(reg, DR_REG_NULL, 0,
			offsetof(shadow_data_t, thd),
			OPSZ_4);
	opnd2 = OPND_CREATE_INT32((int)tls_data->myid);
	instr = INSTR_CREATE_mov_st(drcontext, opnd1, opnd2);
	instrlist_meta_preinsert(ilist, label, instr);
	
	/* increment the cache invd counter */
	opnd1 = OPND_CREATE_ABSMEM(&ref->note[CACHE_INVD_IDX], OPSZ_4);
	instr = INSTR_CREATE_inc(drcontext, opnd1);
	instrlist_meta_preinsert(ilist, label, instr);
}

static void
instrument_memory_modify_tcd(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	instrument_memory_write_tcd(drcontext,
			umbra_info,
			ref,
			ilist,
			where);
}

static void
instrument_update_tcd(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	if (ref->type == MemRead)
	instrument_memory_read_tcd(drcontext,
			umbra_info,
			ref,
			ilist,
			where);
	else if (ref->type == MemWrite)
	instrument_memory_write_tcd(drcontext,
			umbra_info,
			ref,
			ilist,
			where);
	else if (ref->type == MemModify)
	instrument_memory_modify_tcd(drcontext,
			umbra_info,
			ref,
			ilist,
			where);
}

static void
umbra_client_thread_init_tcd(void *drcontext, umbra_info_t *umbra_info)
{
	client_tls_data_t *tls_data;

	tls_data = (client_tls_data_t *)umbra_info->client_tls_data;

	/* bit 0 is always set as for total memory referenced */
	tls_data->tid_map = (1 << ((client_proc_data.num_threads) % MAX_NUM_THREADS));

	tls_data->myid = client_proc_data.num_threads + 1;
}

static void
umbra_client_thread_exit_tcd(void *drcontext, umbra_info_t *umbra_info)
{
	int i = 0;
	mem_ref_t *ref;
	client_tls_data_t *tls_data;

	tls_data = (client_tls_data_t *)umbra_info->client_tls_data;

	for (i = 0; i < umbra_info->table.num_refs; i++) {
		if ((i % INIT_REF_TABLE_SIZE) != (INIT_REF_TABLE_SIZE - 1)) {
			ref = table_get_ref(umbra_info, i);
			tls_data->stats[CACHE_MISS_IDX] += ref->note[CACHE_MISS_IDX];
			tls_data->stats[CACHE_INVD_IDX] += ref->note[CACHE_INVD_IDX];
		}
	}

	dr_fprintf(proc_info.log, "Correlation of Thread %d: ", tls_data->myid);
	i = 0;
	if (tls_data->cor_counter[i] != 0)
		dr_fprintf(proc_info.log, "[%d] %llu", i, tls_data->cor_counter[i]);
	for (i = 1; i < MAX_NUM_THREADS; i++) {
		if (tls_data->cor_counter[i] != 0)
		dr_fprintf(proc_info.log, ", [%d] %llu", i, tls_data->cor_counter[i]);
	}
	dr_fprintf(proc_info.log, "\n");

	dr_fprintf(proc_info.log, "Cache miss of Thread %d: %llu\n",
			tls_data->myid, tls_data->stats[CACHE_MISS_IDX]);
	dr_fprintf(proc_info.log, "Cache invalidation of Thread %d: %llu\n",
			tls_data->myid, tls_data->stats[CACHE_INVD_IDX]);

	client_proc_data.stats[CACHE_MISS_IDX] += tls_data->stats[CACHE_MISS_IDX];
	client_proc_data.stats[CACHE_INVD_IDX] += tls_data->stats[CACHE_INVD_IDX];

	return;
}

static void
umbra_client_exit_tcd()
{
	dr_mutex_lock(client_proc_data.lock);
	dr_fprintf(proc_info.log, "Total cache miss of group %d: %llu\n",
			client_proc_data.group_id, client_proc_data.stats[CACHE_MISS_IDX]);
	dr_fprintf(proc_info.log, "Total cache invalidation of group %d: %llu\n",
			client_proc_data.group_id, client_proc_data.stats[CACHE_INVD_IDX]);
	dr_mutex_unlock(client_proc_data.lock);
}

/*****************************client-dep-anal*****************************/

static void
umbra_client_thread_exit_da(void *drcontext, umbra_info_t *umbra_info)
{
	int i = 0;
	client_tls_data_t *tls_data;

	tls_data = (client_tls_data_t *)umbra_info->client_tls_data;
	dr_fprintf(proc_info.log, "Dependency of Thread %d: ", tls_data->myid);
	if (tls_data->dep_counter[i] != 0)
		dr_fprintf(proc_info.log, "[%d] %llu", i, tls_data->dep_counter[i]);
	for (i = 1; i < MAX_NUM_THREADS; i++) {
		if (tls_data->dep_counter[i] != 0)
		dr_fprintf(proc_info.log, ", [%d] %llu", i, tls_data->dep_counter[i]);
	}
	dr_fprintf(proc_info.log, "\n");
	return;
}

/*****************************client-mpi-anal*****************************/

/* int MPI_Send(void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm) */
/* int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status *status) */
/* int MPI_Isend(void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request) */
/* int MPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request *request) */

//static void
//wrap_pre_send()
//{
//	umbra_info_t *info = umbra_get_info();
//	client_tls_data_t *tls_data;
//	dr_mcontext_t mc = {sizeof(mc), DR_MC_ALL, };
//	int second_arg, third_arg, forth_arg;
//	int size = 0;
//
//	tls_data = (client_tls_data_t *)info->client_tls_data;
//
//	dr_get_mcontext(info->drcontext, &mc);
//	second_arg = (int) mc.rsi; /* count */
//	third_arg = (int) mc.rdx; /* datatype */
//	forth_arg = (int) mc.rcx; /* dest */
//
//	//FIXME: MPI_Type_size(third_arg, &size);
//	//XXX: MAX_PROC_NUMBER for MPI is MAX_NUM_THREADS
//
//	if (forth_arg < MAX_NUM_THREADS) {
//		tls_data->msgsend_data[forth_arg].msg_count++;
//		tls_data->msgsend_data[forth_arg].total_size += second_arg * size;
//	}
//}
//
//static void
//wrap_post_send()
//{
//}
//
//static void
//wrap_pre_recv()
//{
//	umbra_info_t *info = umbra_get_info();
//	dr_mcontext_t mc = { sizeof(mc), DR_MC_ALL, };
//	int second_arg, third_arg, forth_arg;
//
//	dr_get_mcontext(info->drcontext, &mc);
//	second_arg = (int) mc.rsi; /* count */
//	third_arg = (int) mc.rdx; /* datatype */
//	forth_arg = (int) mc.rcx; /* source */
//
//	//TODO: do I need this?
//}
//
//static void
//wrap_post_recv()
//{
//}
//
//static void
//wrap_pre_isend()
//{
//	umbra_info_t *info = umbra_get_info();
//	client_tls_data_t *tls_data;
//	dr_mcontext_t mc = {sizeof(mc), DR_MC_ALL, };
//	int second_arg, third_arg, forth_arg;
//	int size = 0;
//
//	tls_data = (client_tls_data_t *)info->client_tls_data;
//
//	dr_get_mcontext(info->drcontext, &mc);
//	second_arg = (int) mc.rsi; /* count */
//	third_arg = (int) mc.rdx; /* datatype */
//	forth_arg = (int) mc.rcx; /* dest */
//
//	//FIXME: MPI_Type_size(third_arg, &size);
//	//XXX: MAX_PROC_NUMBER for MPI is MAX_NUM_THREADS
//
//	if (forth_arg < MAX_NUM_THREADS) {
//		tls_data->imsgsend_data[forth_arg].msg_count++;
//		tls_data->imsgsend_data[forth_arg].total_size += second_arg * size;
//	}
//}
//
//static void
//wrap_post_isend()
//{
//}
//
//static void
//wrap_pre_irecv()
//{
//	umbra_info_t *info = umbra_get_info();
//	dr_mcontext_t mc = { sizeof(mc), DR_MC_ALL, };
//	int second_arg, third_arg, forth_arg;
//
//	dr_get_mcontext(info->drcontext, &mc);
//	second_arg = (int) mc.rsi; /* count */
//	third_arg = (int) mc.rdx; /* datatype */
//	forth_arg = (int) mc.rcx; /* source */
//
//	//TODO: do I need this?
//}
//
//static void
//wrap_post_irecv()
//{
//}
//
//static void
//umbra_client_thread_exit_ma(void *drcontext, umbra_info_t *umbra_info)
//{
//	int i;
//	client_tls_data_t *tls_data;
//
//	tls_data = (client_tls_data_t *)umbra_info->client_tls_data;
//	i = 0;
//	dr_fprintf(proc_info.log, "Sent Messages of Thread %d: ", tls_data->myid);
//	if (tls_data->msgsend_data[i].msg_count != 0)
//		dr_fprintf(proc_info.log, "[%d] <%d-%d>", i, tls_data->msgsend_data[i].msg_count, tls_data->msgsend_data[i].total_size);
//	for (i = 1; i < MAX_NUM_THREADS; i++) {
//		if (tls_data->msgsend_data[i].msg_count != 0)
//		dr_fprintf(proc_info.log, ", [%d] <%d-%d>", i, tls_data->msgsend_data[i].msg_count, tls_data->msgsend_data[i].total_size);
//	}
//	dr_fprintf(proc_info.log, "\n");
//	i = 0;
//	dr_fprintf(proc_info.log, "Isent Messages of Thread %d: ", tls_data->myid);
//	if (tls_data->imsgsend_data[i].msg_count != 0)
//		dr_fprintf(proc_info.log, "[%d] <%d-%d>", i, tls_data->imsgsend_data[i].msg_count, tls_data->imsgsend_data[i].total_size);
//	for (i = 1; i < MAX_NUM_THREADS; i++) {
//		if (tls_data->imsgsend_data[i].msg_count != 0)
//		dr_fprintf(proc_info.log, ", [%d] <%d-%d>", i, tls_data->imsgsend_data[i].msg_count, tls_data->imsgsend_data[i].total_size);
//	}
//	dr_fprintf(proc_info.log, "\n");
//	return;
//}

/*****************************client-mcapi-anal*****************************/

//TODO:
	//	wrap endpoint_create
	//	wrap msg_send operations, store the number of messages and sizes
	//	wrap msg_recv operations

/*************************client-general-threading*****************************/

static void
add(uint64 *ins_count, uint64 num_instrs)
{
	*ins_count += num_instrs;
}

static void
wrap_pre_tcreate()
{
	umbra_info_t *info = umbra_get_info();
	dr_mcontext_t mc = { sizeof(mc), DR_MC_ALL, };
	app_pc third_arg;
	client_tls_data_t *tls_data = (client_tls_data_t *) info->client_tls_data;

	dr_get_mcontext(info->drcontext, &mc);
	third_arg = (app_pc) mc.rdx;
	dr_mutex_lock(client_proc_data.lock);
	dr_fprintf(proc_info.log, "PC of Thread %d: %llu\n"
			"Creator of Thread %d: %d\n",
			client_proc_data.thread_id + 2, third_arg,
			client_proc_data.thread_id + 2, tls_data->myid);
	client_proc_data.thread_id++;
	dr_mutex_unlock(client_proc_data.lock);

}

static void
wrap_post_tcreate()
{
}

static void
umbra_client_thread_init_gt(void *drcontext, umbra_info_t *umbra_info)
{
	dr_get_time(&((client_tls_data_t *) umbra_info->client_tls_data)->creation_time);
}

static void
umbra_client_thread_exit_gt(void *drcontext, umbra_info_t *umbra_info)
{
	client_tls_data_t *tls_data = (client_tls_data_t *) umbra_info->client_tls_data;
	char msg[MSG_LEN];
	int len;
	dr_time_t *ct, *et;

	len = dr_snprintf(msg, sizeof(msg)/sizeof(msg[0]),
			"Instruction Count of Thread %d: %llu\n",
			tls_data->myid, tls_data->ins_count);
	DR_ASSERT(len > 0);
	NULL_TERMINATE(msg);

	dr_fprintf(proc_info.log, msg);

	dr_get_time(&tls_data->exit_time);

	ct = &tls_data->creation_time;
	et = &tls_data->exit_time;
	dr_fprintf(proc_info.log, "Time of Thread %d: %d.%d.%d.%d - %d.%d.%d.%d\n", tls_data->myid,
			ct->hour, ct->minute, ct->second, ct->milliseconds,
			et->hour, et->minute, et->second, et->milliseconds);
}

static void
umbra_client_exit_gt()
{
	char msg[MSG_LEN];
	int len;
	len = dr_snprintf(msg, sizeof(msg)/sizeof(msg[0]),
			"Total Instruction Count of Process: %llu\n",
			client_proc_data.ins_count);
	DR_ASSERT(len > 0);
	NULL_TERMINATE(msg);
	dr_mutex_lock(client_proc_data.lock);
	dr_fprintf(proc_info.log, msg);
	dr_mutex_unlock(client_proc_data.lock);
}

static void
pre_umbra_instrument_bb_gt(void *drcontext,
		umbra_info_t *umbra_info,
		void *tag,
		instrlist_t *ilist,
		bool for_trace)
{
	client_tls_data_t *tls_data = (client_tls_data_t *) umbra_info->client_tls_data;
	instr_t *instr;
	uint64 num_instrs;

	for (instr = instrlist_first(ilist), num_instrs = 0;
			instr != NULL; instr = instr_get_next(instr)) {
		num_instrs++;
	}

	dr_insert_clean_call(drcontext, ilist, instrlist_first(ilist),
			(void *)add, false /* save fpstate */, 2,
			OPND_CREATE_INT64(&client_proc_data.ins_count),
			OPND_CREATE_INT64(num_instrs));

	dr_insert_clean_call(drcontext, ilist, instrlist_first(ilist),
			(void *)add, false /* save fpstate */, 2,
			OPND_CREATE_INT64(&tls_data->ins_count),
			OPND_CREATE_INT64(num_instrs));

}

/*****************************sync_con-detect*****************************/

//static void
//wrap_pre_lock()
//{
//	//client_proc_data.num_locks++;
//}
//
//static void
//wrap_post_lock()
//{
//}
//
//static void
//wrap_pre_barrier()
//{
//	//client_proc_data.num_barriers++;
//}
//
//static void
//wrap_post_barrier()
//{
//}
//
//static void
//wrap_pre_condition()
//{
//	//client_proc_data.num_conditions++;
//}
//
//static void
//wrap_post_condition()
//{
//}

static void
umbra_client_exit_syd()
{
	dr_mutex_lock(client_proc_data.lock);
	dr_fprintf(proc_info.log, "#locks of group %d: %d\n"
			"#barriers of group %d: %d\n"
			"#conditions of group %d: %d\n",
			client_proc_data.group_id, client_proc_data.num_locks,
			client_proc_data.group_id, client_proc_data.num_barriers,
			client_proc_data.group_id, client_proc_data.num_conditions);
	dr_mutex_unlock(client_proc_data.lock);
}

/******************************************************************/

#define TEST_AND_UPDATE
static void
instrument_update(void *drcontext,
		umbra_info_t *umbra_info,
		mem_ref_t *ref,
		instrlist_t *ilist,
		instr_t *where)
{
	instrument_update_sa(drcontext, umbra_info, ref, ilist, where);
	instrument_update_tcd(drcontext, umbra_info, ref, ilist, where);
}

static bool
ref_is_interested(umbra_info_t *info, mem_ref_t *ref)
{
	if (opnd_is_far_base_disp(ref->opnd))
	return false;

	if ((opnd_is_rel_addr(ref->opnd) || opnd_is_abs_addr(ref->opnd))) {
		void *addr = opnd_get_addr(ref->opnd);
		if ((void *) 0xffffffffff5ff0ff >= addr && (void *) 0xffffffffff5ff000 <= addr)
		return false;
	}

	return true;
}

static bool
bb_is_interested(umbra_info_t *info, basic_block_t *bb)
{
	int i;
	if (bb->num_refs == 0)
	return false;
	/* check ref one by one */
	for (i = 0; i < bb->num_refs; i++) {
		if (ref_is_interested(info, &bb->refs[i]))
		return true;
	}
	/* if reach here, no memory reference is interested */
	return false;
}

static void
umbra_client_thread_init(void *drcontext, umbra_info_t *umbra_info)
{
	client_tls_data_t *tls_data;

	/* allocate client tls data */
	tls_data = dr_thread_alloc(drcontext, sizeof(client_tls_data_t));
	memset(tls_data, 0, sizeof(client_tls_data_t));
	umbra_info->client_tls_data = tls_data;

	dr_mutex_lock(client_proc_data.lock);

	umbra_client_thread_init_sa(drcontext, umbra_info);
	umbra_client_thread_init_tcd(drcontext, umbra_info);
	umbra_client_thread_init_gt(drcontext, umbra_info);

	client_proc_data.num_threads++;
	client_proc_data.num_active_threads++;
	
	if (client_proc_data.num_active_threads == 2 && client_proc_data.group_id > 0) { /* I have only main */
		/* umbra_client_exit_sa(); */
		dr_fprintf(proc_info.log,
				"total private of group %d: %llu\n"
				"total producer/consumer of group %d: %llu\n"
				"total migratory of group %d: %llu\n"
				"total readonly of group %d: %llu\n"
				"total cacheline of group %d: %llu\n"
				"total mem of group %d: %llu\n",
				client_proc_data.group_id, client_proc_data.total_private,
				client_proc_data.group_id, client_proc_data.total_prod_cons,
				client_proc_data.group_id, client_proc_data.total_migratory,
				client_proc_data.group_id, client_proc_data.total_readonly,
				client_proc_data.group_id,			
				client_proc_data.total_private +
				client_proc_data.total_prod_cons +
				client_proc_data.total_migratory + 
				client_proc_data.total_readonly,
				client_proc_data.group_id, client_proc_data.total_mem);
		client_proc_data.total_private = 0;
		client_proc_data.total_prod_cons = 0;
		client_proc_data.total_migratory = 0;
		client_proc_data.total_readonly = 0;
		client_proc_data.total_mem = 0;

		/* umbra_client_exit_tcd(); */
		dr_fprintf(proc_info.log, "Total cache miss of group %d: %llu\n",
				client_proc_data.group_id, client_proc_data.stats[CACHE_MISS_IDX]);
		dr_fprintf(proc_info.log, "Total cache invalidation of group %d: %llu\n",
				client_proc_data.group_id, client_proc_data.stats[CACHE_INVD_IDX]);
		client_proc_data.stats[CACHE_MISS_IDX] = 0;
		client_proc_data.stats[CACHE_INVD_IDX] = 0; 

		/* umbra_client_exit_syd(); */
		dr_fprintf(proc_info.log, "#locks of group %d: %d\n"
				"#barriers of group %d: %d\n"
				"#conditions of group %d: %d\n",
				client_proc_data.group_id, client_proc_data.num_locks,
				client_proc_data.group_id, client_proc_data.num_barriers,
				client_proc_data.group_id, client_proc_data.num_conditions);

		client_proc_data.num_locks = 0;
		client_proc_data.num_barriers = 0;
		client_proc_data.num_conditions = 0;
	}
	
	if (client_proc_data.num_active_threads == 2) {
		client_proc_data.group_id++;
	}

	dr_mutex_unlock(client_proc_data.lock);
}

static void
umbra_client_thread_exit(void *drcontext, umbra_info_t *umbra_info)
{
	dr_mutex_lock(client_proc_data.lock);

	umbra_client_thread_exit_tcd(drcontext, umbra_info);
	umbra_client_thread_exit_da(drcontext, umbra_info);
//	umbra_client_thread_exit_ma(drcontext, umbra_info);
	umbra_client_thread_exit_gt(drcontext, umbra_info);

	client_proc_data.num_active_threads--;
	dr_mutex_unlock(client_proc_data.lock);

	dr_thread_free(drcontext, umbra_info->client_tls_data,
			sizeof(client_tls_data_t));
}

static void
shadow_memory_create(memory_map_t *map)
{
	shadow_memory_create_sa(map);
}

static void
shadow_memory_remove(memory_map_t *map)
{
	shadow_memory_remove_sa(map);
}

static void
pre_umbra_instrument_bb(void *drcontext,
		umbra_info_t *umbra_info,
		void *tag,
		instrlist_t *ilist,
		bool for_trace)
{
	pre_umbra_instrument_bb_gt(drcontext, umbra_info, tag, ilist, for_trace);
}

static void
umbra_client_exit()
{
	umbra_client_exit_sa();
	umbra_client_exit_tcd();
	umbra_client_exit_syd();
	umbra_client_exit_gt();
	dr_mutex_destroy(client_proc_data.lock);
}

void
umbra_client_init()
{
	umbra_client_t *client;
	int i, j;

	memset(&client_proc_data, 0, sizeof(client_proc_data_t));
	client_proc_data.lock = dr_mutex_create();
	client = &proc_info.client;
	memset(client, 0, sizeof(umbra_client_t));
	client->thread_init = umbra_client_thread_init;
	client->thread_exit = umbra_client_thread_exit;
	client->client_exit = umbra_client_exit;
	client->bb_is_interested = bb_is_interested;
	client->ref_is_interested = ref_is_interested;
	client->app_unit_bits[0] = CACHE_LINE_BITS;
	client->shd_unit_bits[0] = CACHE_LINE_BITS; /* 64-byte-2-64-byte mapping */
	client->orig_addr = false;
	client->num_steal_regs = 2;
	client->instrument_update = instrument_update;
	client->shadow_memory_module_destroy = shadow_memory_remove;
	client->pre_umbra_instrument_bb = pre_umbra_instrument_bb;
	client->shadow_memory_module_create = shadow_memory_create;

	wrap_funcs = NULL;

	/* share-anal */
	for (i = 0; i < 16; i++) {
		for (j = 0; j < 16; j++) {
			thd_count[i * 16 + j] = bit_count[i] + bit_count[j];
		}
	}

	/* mpi-anal */	
//	umbra_wrap_func(NULL, "MPI_send", (app_pc) wrap_pre_send, (app_pc) wrap_post_send);
//	umbra_wrap_func(NULL, "MPI_recv", (app_pc) wrap_pre_recv, (app_pc) wrap_post_recv);
//	umbra_wrap_func(NULL, "MPI_Isend", (app_pc) wrap_pre_isend, (app_pc) wrap_post_isend);
//	umbra_wrap_func(NULL, "MPI_Irecv", (app_pc) wrap_pre_irecv, (app_pc) wrap_post_irecv);

	/* sync-detect */
//	umbra_wrap_func(NULL, "sem_init", (app_pc) wrap_pre_lock, (app_pc) wrap_post_lock);
//	umbra_wrap_func(NULL, "pthread_mutex_init", (app_pc) wrap_pre_lock, (app_pc) wrap_post_lock);
//	umbra_wrap_func(NULL, "pthread_barrier_init", (app_pc) wrap_pre_barrier, (app_pc) wrap_post_barrier);
//	umbra_wrap_func(NULL, "pthread_cond_init", (app_pc) wrap_pre_condition, (app_pc) wrap_post_condition);

	/* general-threading */
	umbra_wrap_func(NULL, "pthread_create", (app_pc) wrap_pre_tcreate, (app_pc) wrap_post_tcreate); /* C, C++ */
	/*umbra_wrap_func(NULL, "Thread", (app_pc) wrap_pre_tcreate, (app_pc) wrap_post_tcreate);*/ /* Java */
}

#endif /* UMBRA_CLIENT_AIO */

