/*********************************************************************
 * Copyright (c) 2011-2013 Etem Deniz <etem.deniz@boun.edu.tr>       *
 *                                                                   *
 * Permission is hereby granted, free of charge, to any person       *
 * obtaining a copy of this software and associated documentation    *
 * files (the "Software"), to deal in the Software without           *
 * restriction, including without limitation the rights to use,      *
 * copy, modify, merge, publish, distribute, sublicense, and/or sell *
 * copies of the Software, and to permit persons to whom the         *
 * Software is furnished to do so, subject to the following          *
 * conditions:                                                       *
 *                                                                   *
 * The above copyright notice and this permission notice shall be    *
 * included in all copies or substantial portions of the Software.   *
 *                                                                   *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,   *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     *
 * OTHER DEALINGS IN THE SOFTWARE.                                   *
 *********************************************************************/
/*
 * Module Name:
 *     client-general-threading.c
 *
 * Description:
 *     the number of active threads over time,
 *     dynamic instruction counts per thread,
 *     PC per thread.
 *
 * Author: 
 *     Etem Deniz
 * 
 * Date:
 *     11/01/2011
 * 
 * Note:
 *     11/01/2011: It currently only works in Linux x86_64.
 *                 It currently only works in thread private code cache
 */

#ifdef UMBRA_CLIENT_GENERAL_THREADING

#include <stddef.h>    /* offsetof */
#include <string.h>    /* memset */
#include "dr_api.h"
#include <dr_tools.h> 
#include "../core/global.h"

/* CACHE LINE GRANULARITY */
#define CACHE_LINE_BITS 6

#define MAX_NUM_THREADS 32

#define NULL_TERMINATE(buf) buf[(sizeof(buf)/sizeof(buf[0])) - 1] = '\0'

typedef struct _client_proc_data_t {
	int num_threads;
	unsigned int thread_id;
	void *lock;
	uint64 ins_count;
}client_proc_data_t;
client_proc_data_t client_proc_data;

typedef struct _client_tls_data_t {
	unsigned int myid;
	uint64 ins_count;
	dr_time_t creation_time;
	dr_time_t exit_time;
}client_tls_data_t;

static void inscount(uint64 *ins_count, uint64 num_instrs) {*ins_count += num_instrs;}

static bool
ref_is_interested(umbra_info_t *info, mem_ref_t *ref)
{
	if (opnd_is_far_base_disp(ref->opnd))
	return false;
	return true;
}

static bool
bb_is_interested(umbra_info_t *info, basic_block_t *bb)
{
	int i;
	if (bb->num_refs == 0)
	return false;
	/* check ref one by one */
	for (i = 0; i < bb->num_refs; i++) {
		if (ref_is_interested(info, &bb->refs[i]))
		return true;
	}
	/* if reach here, no memory reference is interested */
	return false;
}

static void
wrap_pre_tcreate()
{
	umbra_info_t *info = umbra_get_info();
	dr_mcontext_t mc = { sizeof(mc), DR_MC_ALL, };
	app_pc third_arg;
	client_tls_data_t *tls_data = (client_tls_data_t *) info->client_tls_data;

	dr_get_mcontext(info->drcontext, &mc);
	third_arg = (app_pc) mc.rdx;
	dr_fprintf(proc_info.log, "PC of Thread %d: %p\n"
			"Creator of Thread %d: %d\n",
			client_proc_data.thread_id, third_arg,
			client_proc_data.thread_id, tls_data->myid);

	client_proc_data.thread_id++;
}

static void
wrap_post_tcreate()
{
}

static void
umbra_client_thread_init(void *drcontext, umbra_info_t *umbra_info)
{
	client_tls_data_t *tls_data;

	/* allocate client tls data */
	tls_data = dr_thread_alloc(drcontext, sizeof(client_tls_data_t));
	memset(tls_data, 0, sizeof(client_tls_data_t));
	umbra_info->client_tls_data = tls_data;

	dr_mutex_lock(client_proc_data.lock);
	tls_data->myid = ++client_proc_data.num_threads;
	dr_mutex_unlock(client_proc_data.lock);

	dr_get_time(&tls_data->creation_time);
}

static void
umbra_client_thread_exit(void *drcontext, umbra_info_t *umbra_info)
{
	client_tls_data_t *tls_data;
	char msg[512];
	int len;
	dr_time_t *ct, *et;

	tls_data = (client_tls_data_t *) umbra_info->client_tls_data;

	len = dr_snprintf(msg, sizeof(msg)/sizeof(msg[0]),
			"Instruction Count of Thread %d: %llu\n",
			tls_data->myid, tls_data->ins_count);
	DR_ASSERT(len > 0);
	NULL_TERMINATE(msg);

	dr_mutex_lock(client_proc_data.lock);
	dr_fprintf(proc_info.log, msg);

	dr_get_time(&tls_data->exit_time);

	ct = &tls_data->creation_time;
	et = &tls_data->exit_time;
	dr_fprintf(proc_info.log, "Time of Thread %d: %d.%d.%d.%d - %d.%d.%d.%d\n", tls_data->myid,
			ct->hour, ct->minute, ct->second, ct->milliseconds,
			et->hour, et->minute, et->second, et->milliseconds);

	dr_mutex_unlock(client_proc_data.lock);
	dr_thread_free(drcontext, umbra_info->client_tls_data,
			sizeof(client_tls_data_t));
}

static void
umbra_client_exit()
{
	char msg[512];
	int len;
	len = dr_snprintf(msg, sizeof(msg)/sizeof(msg[0]),
			"Instruction Count of Process: %llu\n",
			client_proc_data.ins_count);
	DR_ASSERT(len > 0);
	NULL_TERMINATE(msg);
	dr_fprintf(proc_info.log, msg);
	dr_mutex_destroy(client_proc_data.lock);
}

static void
pre_umbra_instrument_bb(void *drcontext,
		umbra_info_t *umbra_info,
		void *tag,
		instrlist_t *ilist,
		bool for_trace)
{
	client_tls_data_t *tls_data = (client_tls_data_t *) umbra_info->client_tls_data;
	instr_t *instr;
	uint64 num_instrs;

	for (instr = instrlist_first(ilist), num_instrs = 0; instr != NULL; instr = instr_get_next(instr)) {
		num_instrs++;
	}

	dr_insert_clean_call(drcontext, ilist, instrlist_first(ilist),
			(void *)inscount, false /* save fpstate */, 2,
			OPND_CREATE_INT64(&client_proc_data.ins_count),
			OPND_CREATE_INT64(num_instrs));

	dr_insert_clean_call(drcontext, ilist, instrlist_first(ilist),
			(void *)inscount, false /* save fpstate */, 2,
			OPND_CREATE_INT64(&tls_data->ins_count),
			OPND_CREATE_INT64(num_instrs));

}

void
umbra_client_init()
{
	umbra_client_t *client;

	memset(&client_proc_data, 0, sizeof(client_proc_data_t));
	client_proc_data.lock = dr_mutex_create();
	client_proc_data.thread_id = 1;
	client = &proc_info.client;
	memset(client, 0, sizeof(umbra_client_t));
	client->thread_init = umbra_client_thread_init;
	client->thread_exit = umbra_client_thread_exit;
	client->client_exit = umbra_client_exit;
	client->bb_is_interested = bb_is_interested;
	client->ref_is_interested = ref_is_interested;
	client->pre_umbra_instrument_bb = pre_umbra_instrument_bb;
	client->app_unit_bits[0] = CACHE_LINE_BITS;
	client->shd_unit_bits[0] = CACHE_LINE_BITS; /* 64-byte-2-64-byte mapping */
	client->orig_addr = false;
	client->num_steal_regs = 2;
	client->instrument_update = NULL;
	client->shadow_memory_module_destroy = NULL;
	client->shadow_memory_module_create = NULL;

	wrap_funcs = NULL;
	/* int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine)(void*), void *arg); */
	umbra_wrap_func(NULL, "pthread_create", (app_pc) wrap_pre_tcreate, (app_pc) wrap_post_tcreate);
}

#endif /* UMBRA_CLIENT_GENERAL_THREADING */
