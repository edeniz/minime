/*********************************************************************
 * Copyright (c) 2011-2013 Etem Deniz <etem.deniz@boun.edu.tr>       *
 *                                                                   *
 * Permission is hereby granted, free of charge, to any person       *
 * obtaining a copy of this software and associated documentation    *
 * files (the "Software"), to deal in the Software without           *
 * restriction, including without limitation the rights to use,      *
 * copy, modify, merge, publish, distribute, sublicense, and/or sell *
 * copies of the Software, and to permit persons to whom the         *
 * Software is furnished to do so, subject to the following          *
 * conditions:                                                       *
 *                                                                   *
 * The above copyright notice and this permission notice shall be    *
 * included in all copies or substantial portions of the Software.   *
 *                                                                   *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,   *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     *
 * OTHER DEALINGS IN THE SOFTWARE.                                   *
 *********************************************************************/
/*
 * Module Name:
 *     client-sync-detect.c
 *
 * Description:
 *     counts synchronization objects(Locks, Barriers, Conditions)
 *
 * Author: 
 *     Etem Deniz
 * 
 * Date:
 *     11/01/2011
 * 
 * Note:
 *     11/01/2011: It currently only works in Linux x86_64.
 *                 It currently only works in thread private code cache
 */

#ifdef UMBRA_CLIENT_SYNC_DETECT

#include <stddef.h>    /* offsetof */
#include <string.h>    /* memset */
#include "dr_api.h"
#include "../core/global.h"

/* CACHE LINE GRANULARITY */
#define CACHE_LINE_BITS 6

#define MAX_NUM_THREADS 32

typedef struct _client_proc_data_t {
	int num_locks;
	int num_barriers;
	int num_conditions;
	void *lock;
}client_proc_data_t;
client_proc_data_t client_proc_data;

static bool
ref_is_interested(umbra_info_t *info, mem_ref_t *ref)
{
	if (opnd_is_far_base_disp(ref->opnd))
	return false;
	return true;
}

static bool
bb_is_interested(umbra_info_t *info, basic_block_t *bb)
{
	int i;
	if (bb->num_refs == 0)
	return false;
	/* check ref one by one */
	for (i = 0; i < bb->num_refs; i++) {
		if (ref_is_interested(info, &bb->refs[i]))
		return true;
	}
	/* if reach here, no memory reference is interested */
	return false;
}

static void
wrap_pre_lock()
{
	client_proc_data.num_locks++;

}

static void
wrap_post_lock()
{
}

static void
wrap_pre_barrier()
{
	client_proc_data.num_barriers++;
}

static void
wrap_post_barrier()
{
}

static void
wrap_pre_condition()
{
	client_proc_data.num_conditions++;
}

static void
wrap_post_condition()
{
}

static void
umbra_client_exit()
{
	dr_fprintf(proc_info.log, "Number of locks: %d\n"
			"Number of barriers: %d\n"
			"Number of conditions: %d\n",
			client_proc_data.num_locks,
			client_proc_data.num_barriers,
			client_proc_data.num_conditions);

	dr_mutex_destroy(client_proc_data.lock);
}

void
umbra_client_init()
{
	umbra_client_t *client;

	memset(&client_proc_data, 0, sizeof(client_proc_data_t));
	client_proc_data.lock = dr_mutex_create();
	client = &proc_info.client;
	memset(client, 0, sizeof(umbra_client_t));
	client->thread_init = NULL;
	client->thread_exit = NULL;
	client->client_exit = umbra_client_exit;
	client->bb_is_interested = bb_is_interested;
	client->ref_is_interested = ref_is_interested;
	client->app_unit_bits[0] = CACHE_LINE_BITS;
	client->shd_unit_bits[0] = CACHE_LINE_BITS; /* 64-byte-2-64-byte mapping */
	client->orig_addr = false;
	client->num_steal_regs = 2;
	client->instrument_update = NULL;
	client->shadow_memory_module_destroy = NULL;
	client->shadow_memory_module_create = NULL;

	wrap_funcs = NULL;
	umbra_wrap_func(NULL, "sem_init", (app_pc) wrap_pre_lock, (app_pc) wrap_post_lock);
	umbra_wrap_func(NULL, "pthread_mutex_init", (app_pc) wrap_pre_lock, (app_pc) wrap_post_lock);
	umbra_wrap_func(NULL, "pthread_barrier_init", (app_pc) wrap_pre_barrier, (app_pc) wrap_post_barrier);
	umbra_wrap_func(NULL, "pthread_cond_init", (app_pc) wrap_pre_condition, (app_pc) wrap_post_condition);
}

#endif /* UMBRA_CLIENT_SYNC_DETECT */
