/*********************************************************************
 * Copyright (c) 2011-2013 Etem Deniz <etem.deniz@boun.edu.tr>       *
 *                                                                   *
 * Permission is hereby granted, free of charge, to any person       *
 * obtaining a copy of this software and associated documentation    *
 * files (the "Software"), to deal in the Software without           *
 * restriction, including without limitation the rights to use,      *
 * copy, modify, merge, publish, distribute, sublicense, and/or sell *
 * copies of the Software, and to permit persons to whom the         *
 * Software is furnished to do so, subject to the following          *
 * conditions:                                                       *
 *                                                                   *
 * The above copyright notice and this permission notice shall be    *
 * included in all copies or substantial portions of the Software.   *
 *                                                                   *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,   *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     *
 * OTHER DEALINGS IN THE SOFTWARE.                                   *
 *********************************************************************/
/*
 * Module Name:
 *     client-mpi-anal.c
 *
 * Description:
 *     logs MPI message exchange functions
 *
 * Author: 
 *     Etem Deniz
 * 
 * Date:
 *     11/01/2011
 * 
 * Note:
 *     11/01/2011: It currently only works in Linux x86_64.
 *                 It currently only works in thread private code cache
 *
 *     The calling convention in Linux x64, i.e. which register is used for which parameter:
 *       rdi, rsi, rdx, rcx, r8, r9, xmm0-7 
 *         (the first parameter in rdi, the second one in rsi and so on.)
 */

#ifdef UMBRA_CLIENT_MPI_ANALYSIS

#include <stddef.h>    /* offsetof */
#include <string.h>    /* memset */
#include "dr_api.h"
#include "../core/global.h"

/* CACHE LINE GRANULARITY */
#define CACHE_LINE_BITS 6

#define MAX_NUM_THREADS 32

typedef struct _client_proc_data_t {
	int num_msgs;
	void *lock;
}client_proc_data_t;
client_proc_data_t client_proc_data;

static bool
ref_is_interested(umbra_info_t *info, mem_ref_t *ref)
{
	if (opnd_is_far_base_disp(ref->opnd))
	return false;
	return true;
}

static bool
bb_is_interested(umbra_info_t *info, basic_block_t *bb)
{
	int i;
	if (bb->num_refs == 0)
	return false;
	/* check ref one by one */
	for (i = 0; i < bb->num_refs; i++) {
		if (ref_is_interested(info, &bb->refs[i]))
		return true;
	}
	/* if reach here, no memory reference is interested */
	return false;
}

static void
wrap_pre_send()
{
	umbra_info_t *info = umbra_get_info();
	dr_mcontext_t mc = {sizeof(mc), DR_MC_ALL, };
	int second_arg, third_arg, forth_arg;

	dr_get_mcontext(info->drcontext, &mc);
	second_arg = (int) mc.rsi;
	third_arg = (int) mc.rdx;
	forth_arg = (int) mc.rcx;
	dr_fprintf(proc_info.log, "MPI_send; arg2: %d, arg3: %d, arg4: %d\n",
			second_arg, third_arg, forth_arg);

	client_proc_data.num_msgs++;
}

static void
wrap_post_send()
{
}

static void
wrap_pre_recv()
{
	umbra_info_t *info = umbra_get_info();
	dr_mcontext_t mc = { sizeof(mc), DR_MC_ALL, };
	int second_arg, third_arg, forth_arg;

	dr_get_mcontext(info->drcontext, &mc);
	second_arg = (int) mc.rsi;
	third_arg = (int) mc.rdx;
	forth_arg = (int) mc.rcx;
	dr_fprintf(proc_info.log, "MPI_recv; arg2: %d, arg3: %d, arg4: %d\n",
			second_arg, third_arg, forth_arg);
}

static void
wrap_post_recv()
{
}

static void
umbra_client_exit()
{
	dr_fprintf(proc_info.log, "Number of messages: %d\n",
			client_proc_data.num_msgs);

	dr_mutex_destroy(client_proc_data.lock);
}

void
umbra_client_init()
{
	umbra_client_t *client;

	memset(&client_proc_data, 0, sizeof(client_proc_data_t));
	client_proc_data.lock = dr_mutex_create();
	client = &proc_info.client;
	memset(client, 0, sizeof(umbra_client_t));
	client->thread_init = NULL;
	client->thread_exit = NULL;
	client->client_exit = umbra_client_exit;
	client->bb_is_interested = bb_is_interested;
	client->ref_is_interested = ref_is_interested;
	client->app_unit_bits[0] = CACHE_LINE_BITS;
	client->shd_unit_bits[0] = CACHE_LINE_BITS; /* 64-byte-2-64-byte mapping */
	client->orig_addr = false;
	client->num_steal_regs = 2;
	client->instrument_update = NULL;
	client->shadow_memory_module_destroy = NULL;
	client->shadow_memory_module_create = NULL;

	wrap_funcs = NULL;

	/* int MPI_Send(void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm) */
	umbra_wrap_func(NULL, "MPI_send", (app_pc) wrap_pre_send, (app_pc) wrap_post_send);

	/* int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status *status) */
	umbra_wrap_func(NULL, "MPI_recv", (app_pc) wrap_pre_recv, (app_pc) wrap_post_recv);
}

#endif /* UMBRA_CLIENT_MPI_ANALYSIS */
