#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

void *Thread(void *arg)
{
  int i, n = *(int *)arg;
  volatile double a = 0.1, b = 1.1, c = 2.1;

  if (n == 0)
    {
      n = 10000000;
      printf("Main Thread 0x%lx: loop of %d iterations\n",(unsigned long)pthread_self(),n);
      for (i=0;i<n;i++)
	{
	  a += b * c;
	}
    }
  else
    {
#if defined(CANCEL)
      int oldtype;
      pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldtype);
#if defined(CANCEL_ASYNC)
      pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
#endif
#endif
      n = 100000;
      printf("PThread 0x%lx: inf. loop of %d iterations\n",(unsigned long)pthread_self(),n);
      while(1)
	{
	  for (i=0;i<n;i++)
	    {
	      a += b * c;
	    }
#if defined(CANCEL) && !defined(CANCEL_ASYNC)
	  pthread_testcancel();
#endif
	}
    }
}

int main(int argc, char **argv)
{
   pthread_t e_th;
   pthread_t f_th;
   int rc, flops1, flops2, flops3;

   flops1 = 1;
   rc = pthread_create(&e_th, NULL, Thread, (void *) &flops1);
   flops2 = 1;
   rc = pthread_create(&f_th, NULL, Thread, (void *) &flops2);
   flops3 = 0;
   Thread(&flops3);

#if defined(CANCEL)
   rc = pthread_cancel(e_th);
   rc = pthread_cancel(f_th);
   rc = pthread_join(e_th, NULL);
   rc = pthread_join(f_th, NULL);
#endif
}
