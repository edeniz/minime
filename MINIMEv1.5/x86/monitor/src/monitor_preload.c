/* $Id: monitor_preload.c,v 1.32 2010/08/10 03:57:07 tmohan Exp $ */

/*
    This file is part of Monitor.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*/

/* This code has been stripped out of HPCToolkit and heavily modified by Philip Mucci,
   University of Tennessee and SiCortex. This is OPEN SOURCE. */

/* -*-Mode: C;-*- */

/****************************************************************************
//
// File: 
//    $Source: /cvs/homes/ospat/monitor/src/monitor_preload.c,v $
//
// Purpose:
//    Implements monitoring that is launched with a preloaded library,
//    something that only works for dynamically linked applications.
//    The library intercepts the beginning execution point of the
//    process and then starts monitoring. When the process exits,
//    control will be transferred back to this library where profiling
//    is finalized.
//
// Description:
//    [The set of functions, macros, etc. defined in the file]
//
// Author:
//    Original source by John Mellor-Crummey, Nathan Tallent, Rice University.
//    SiCortex version by Philip Mucci and Mark Krentel
//
*****************************************************************************/

/**************************** User Include Files ****************************/

#include "monitor_preload.h"
#include "monitor.h"

/* Some Linuxes don't define LIST_FIRST and LIST_NEXT !?
 * How brain-damaged is that !?
 */
#define MONITOR_THREAD_LOCK    (*real_pthread_mutex_lock)(&monitor_thread_mutex)
#define MONITOR_THREAD_UNLOCK  (*real_pthread_mutex_unlock)(&monitor_thread_mutex)
#define MONITOR_EXIT_CLEANUP_SIGNAL  SIGUSR2
#define MONITOR_POLL_USLEEP_TIME  100000
#define MONITOR_POLL_MAX 10
#define MONITOR_MAX_ARG_STRING_SIZE ARG_MAX

/**************************** Forward Declarations **************************/

static int  monitor_libc_start_main PARAMS_START_MAIN;
static void monitor_libc_start_main_fini(); 
static int  monitor_execv  PARAMS_EXECV;
static int  monitor_execvp PARAMS_EXECVP;
static int  monitor_execve PARAMS_EXECVE;
static pid_t monitor_fork();
static void monitor_init_sighandlers();
static int monitor_pthread_create PARAMS_PTHREAD_CREATE;
static void* monitor_pthread_create_start_routine(void* arg);
static void monitor_pthread_cleanup_routine(void* arg);

/*************************** Variable Declarations **************************/

/* Intercepted libc and libpthread routines: set when the library is initialized */
/* preserved across fork */

static libc_start_main_fptr_t      real_start_main = NULL;
static libc_start_main_fini_fptr_t real_start_main_fini = NULL;
static execv_fptr_t                real_execv = NULL;
static execvp_fptr_t               real_execvp = NULL;
static execve_fptr_t               real_execve = NULL;
static fork_fptr_t                 real_fork = NULL;
static _exit_fptr_t                real__exit = NULL;
static exit_fptr_t                 real_exit = NULL;
static pthread_create_fptr_t       real_pthread_create = NULL;
static pthread_self_fptr_t         real_pthread_self = NULL;
static dlopen_fptr_t               real_dlopen = NULL;
static pthread_mutex_lock_fptr_t   real_pthread_mutex_lock = NULL;
static pthread_mutex_lock_fptr_t   real_pthread_mutex_unlock = NULL;
static pthread_kill_fptr_t	   real_pthread_kill = NULL;

/* Library globals, preserved across on fork */

int monitor_opt_debug = 0;
int monitor_opt_error = 0;

/* Local globals, preserved across fork */

static int monitor_early_init_done = 0;
static int monitor_normal_init_done = 0;
static char *monitor_cmd = NULL;

/* Library private globals, reset on fork */

__thread struct monitor_thread_node *monitor_my_tnode = NULL;
int monitor_argc = 0;
char **monitor_argv = NULL;

/* Information about the process, reset on fork */

static int monitor_root_pid = 0;
static int monitor_library_fini_done = 0; 
static pthread_mutex_t monitor_thread_mutex = PTHREAD_MUTEX_INITIALIZER;
static int monitor_thread_num = 0;
static int monitor_init_process_done = 0;
static int monitor_thread_support_done = 0;
static int monitor_thread_support_deferred = 0;
static int monitor_thread_init_done = 0;
static int monitor_in_exit_cleanup = 0;
static int monitor_exit_cleanup_done = 0;
static LIST_HEAD(, monitor_thread_node) monitor_thread_list;

static void nuke_global_variables(void) {
  monitor_my_tnode = NULL;
  monitor_root_pid = getpid();
  monitor_library_fini_done = 0;
//  monitor_thread_mutex = PTHREAD_MUTEX_INITIALIZER;
  monitor_thread_num = 0;
  monitor_init_process_done = 0;
  monitor_thread_support_done = 0;
  monitor_thread_support_deferred = 0;
  monitor_thread_init_done = 0;
  monitor_in_exit_cleanup = 0;
  monitor_exit_cleanup_done = 0;
  while (1) {
    struct monitor_thread_node *n1 = LIST_FIRST(&monitor_thread_list);
    if (n1 == NULL)
      break;
    LIST_REMOVE(n1, tn_links);
    memset(n1, 0, sizeof(struct monitor_thread_node));
    free(n1);
  }
  LIST_INIT(&monitor_thread_list);
  /* MPI */
#ifdef HAVE_MPI
  monitor_mpi_size = -1;
  monitor_mpi_thread_support_level = -1;
  monitor_mpi_rank = -1;
  monitor_mpi_is_mpied = 0;
#endif
}

/*
 * It seems these weak symbols are giving problems on new gcc (4.4.1)/binutils/ld (2.20).
 * If we define them, the user function is not able to override the default handler.
 * By undefining the default handlers, everything seems to work.
 * I've guarded invocations to these functions in libmonitor itself using dlsym checks.
 */
#if 0
#ifdef __GNUC__
void __attribute__ ((weak))  monitor_init_library(void) {
    MONITOR_DEBUG("(default callback)\n");
}
void __attribute__ ((weak))  monitor_fini_library(void) {
    MONITOR_DEBUG("(default callback)\n");
}
void __attribute__ ((weak))  monitor_init_process(char *process, int *argc, char **argv, unsigned pid) {
    MONITOR_DEBUG("(%s, %u) (default callback)\n", process, pid);
}
void __attribute__ ((weak))  monitor_fini_process(void) {
    MONITOR_DEBUG("(default callback)\n");
}
void __attribute__ ((weak))  monitor_init_thread_support(void) {
    MONITOR_DEBUG("(default callback)\n");
}
void * __attribute__ ((weak))  monitor_init_thread(unsigned num) {
    MONITOR_DEBUG("(%u) (default callback)\n", num);
    return (NULL);
}
void __attribute__ ((weak))  monitor_fini_thread(void *init_thread_data) {
    MONITOR_DEBUG("(%p) (default callback)\n", init_thread_data);
}
void __attribute__ ((weak))  monitor_dlopen(const char *library) {
    MONITOR_DEBUG("(%p) (default callback)\n", library);
}
#endif
#endif

typedef void (*_monitor_init_library_fptr_t) (void);
static _monitor_init_library_fptr_t real_monitor_init_library = NULL;
typedef void (*_monitor_fini_library_fptr_t) (void);
static _monitor_fini_library_fptr_t real_monitor_fini_library = NULL;
typedef void (*_monitor_init_process_fptr_t) (char*, int*, char**, unsigned int);
static _monitor_init_process_fptr_t real_monitor_init_process = NULL;
typedef void (*_monitor_fini_process_fptr_t) (void);
static _monitor_fini_process_fptr_t real_monitor_fini_process = NULL;
typedef void (*_monitor_init_thread_support_fptr_t) (void);
static _monitor_init_thread_support_fptr_t real_monitor_init_thread_support = NULL;
typedef void* (*_monitor_init_thread_fptr_t) (unsigned);
static _monitor_init_thread_fptr_t real_monitor_init_thread = NULL;
typedef void (*_monitor_fini_thread_fptr_t) (void*);
static _monitor_fini_thread_fptr_t real_monitor_fini_thread = NULL;
typedef void (*_monitor_dlopen_fptr_t) (const char*);
static _monitor_dlopen_fptr_t real_monitor_dlopen = NULL;
typedef void (*_monitor_init_mpi_fptr_t) (int, int, int);
_monitor_init_mpi_fptr_t real_monitor_init_mpi = NULL;
typedef void (*_monitor_fini_mpi_fptr_t) (void);
_monitor_fini_mpi_fptr_t real_monitor_fini_mpi = NULL;

static void* dlproc_handle = NULL;

static inline void* get_fn_handle(const char* fn) {
  void *handle = NULL;
  if (!dlproc_handle) 
    dlproc_handle = monitor_real_dlopen(NULL, RTLD_LAZY); 
  if (dlproc_handle) {
    dlerror();
    handle = (void *) dlsym(dlproc_handle, fn);
    if (dlerror() != NULL) handle = NULL;
  }
  return handle;
}

#define MONITOR_FINI_PROCESS  monitor_fini_process_function()

#define MONITOR_FINI_LIBRARY  do {					\
        if (real_monitor_fini_library) {                             \
          MONITOR_DEBUG("calling monitor_fini_library() ...\n");	\
	  real_monitor_fini_library();					\
        }                                                               \
	monitor_library_fini_done = 1;					\
} while (0)

unsigned long
monitor_gettid()
{
  if (monitor_my_tnode == NULL ||
      monitor_my_tnode->tn_magic != MONITOR_TN_MAGIC)
    return (0);
  else
    return (monitor_my_tnode->tn_thread_num);
}


static void
monitor_exit_signal_handler(int signum)
{
    struct monitor_thread_node *tn = monitor_my_tnode;

    if (tn == NULL || tn->tn_magic != MONITOR_TN_MAGIC) {
	MONITOR_WARN("missing thread specific data -- "
		     "unable to call monitor_fini_thread\n");
	return;
    }
    if (tn->tn_appl_started && !tn->tn_fini_started) {
	MONITOR_DEBUG("calling monitor_fini_thread(data = %p, num = %d) (if defined)...\n",
		      tn->tn_user_data, tn->tn_thread_num);
	tn->tn_fini_started = 1;
	if (real_monitor_fini_thread) real_monitor_fini_thread(tn->tn_user_data);
	tn->tn_fini_done = 1;
    }
}

static struct monitor_thread_node *
monitor_make_thread_node(void)
{
    struct monitor_thread_node *tn;

    tn = malloc(sizeof(struct monitor_thread_node));
    if (tn == NULL) {
	MONITOR_ERROR("unable to malloc thread node\n");
    }
    memset(tn, 0, sizeof(struct monitor_thread_node));
    tn->tn_magic = MONITOR_TN_MAGIC;

    return (tn);
}

/* This runs in the new thread.
 * 
 * Returns: 0 on success, or 1 if at exit cleanup and thus we don't
 * allow any new threads.
 */ 
static int
monitor_link_thread_node(struct monitor_thread_node *tn)
{
    MONITOR_THREAD_LOCK;
    if (monitor_in_exit_cleanup) {
	MONITOR_THREAD_UNLOCK;
	return (1);
    }

    tn->tn_thread_num = ++monitor_thread_num;
    tn->tn_self= (*real_pthread_self)();
    monitor_my_tnode = tn;
    LIST_INSERT_HEAD(&monitor_thread_list, tn, tn_links);

    MONITOR_THREAD_UNLOCK;
    return (0);
}

static void
monitor_unlink_thread_node(struct monitor_thread_node *tn)
{
    MONITOR_THREAD_LOCK;

    /* Don't delete the thread node if in exit cleanup, just mark the
     * node as finished.
     */
    if (monitor_in_exit_cleanup)
	tn->tn_fini_done = 1;
    else {
	LIST_REMOVE(tn, tn_links);
	memset(tn, 0, sizeof(struct monitor_thread_node));
	free(tn);
    }

    MONITOR_THREAD_UNLOCK;
}

static void
monitor_cleanup_leftover_threads(void)
{
    struct monitor_thread_node *tn, *my_tn;
    struct sigaction my_action;
    sigset_t empty_set;
    pthread_t self;
    int iter, num_unfinished;

    MONITOR_DEBUG("\n");

    /*
     * Install the signal handler for MONITOR_EXIT_CLEANUP_SIGNAL.
     * Note: the signal handler is process-wide.
     */
    sigemptyset(&empty_set);
    my_action.sa_handler = monitor_exit_signal_handler;
    my_action.sa_mask = empty_set;
    my_action.sa_flags = 0;
    if (sigaction(MONITOR_EXIT_CLEANUP_SIGNAL, &my_action, NULL) != 0) {
	MONITOR_ERROR("sigaction failed\n");
    }

    /*
     * Walk through the list of unfinished threads, send a signal to
     * force them into their fini_thread functions, and wait until
     * they all finish.  But don't signal ourself.
     *
     * Note: we may want to add a timeout here.
     */

    iter = 0;
 again:
    self = (*real_pthread_self)();
    my_tn = NULL;
    num_unfinished = 0;

    for (tn = LIST_FIRST(&monitor_thread_list);
	 tn != NULL;
	 tn = LIST_NEXT(tn, tn_links)) {
      
      if (my_tn == NULL) {
	if (pthread_equal(self, tn->tn_self)) {
	  MONITOR_DEBUG("Found calling thread 0x%lx (%d)\n", tn->tn_self, tn->tn_thread_num);
	  my_tn = tn;
	  continue;
	}
      }
      
      if (tn->tn_appl_started && !tn->tn_fini_started) {
	MONITOR_DEBUG("Killing thread 0x%lx (%d)\n", tn->tn_self, tn->tn_thread_num);
	(*real_pthread_kill)(tn->tn_self, MONITOR_EXIT_CLEANUP_SIGNAL);
	usleep(MONITOR_POLL_USLEEP_TIME);
      }

      if (! tn->tn_fini_done) {
	MONITOR_DEBUG("Thread 0x%lx (%d) still not finished\n", tn->tn_self, tn->tn_thread_num);
	num_unfinished++;
      }
    }

    if (num_unfinished) {
      MONITOR_DEBUG("waiting on %d threads to finish\n", num_unfinished);
      usleep(num_unfinished*MONITOR_POLL_USLEEP_TIME);
    }
    
    if (++iter < MONITOR_POLL_MAX)
      goto again;

    /* See if we need to run fini_thread from this thread.
     */
    if (my_tn != NULL && !my_tn->tn_fini_started) {
	MONITOR_DEBUG("calling monitor_fini_thread(data = %p, num = %d) ...\n",
		      my_tn->tn_user_data, my_tn->tn_thread_num);
	my_tn->tn_fini_started = 1;
	if (real_monitor_fini_thread) real_monitor_fini_thread(my_tn->tn_user_data);
	my_tn->tn_fini_done = 1;
    }
}

static void
monitor_fini_process_function(void)
{
    int first;

    MONITOR_DEBUG("\n");

    /* Avoid using pthread functions (mutex) if the application has
     * never called pthread_create().
     */
    if (monitor_thread_init_done) {
	MONITOR_THREAD_LOCK;
	first = !monitor_in_exit_cleanup;
	monitor_in_exit_cleanup = 1;
	MONITOR_THREAD_UNLOCK;
    } else {
	first = !monitor_in_exit_cleanup;
	monitor_in_exit_cleanup = 1;
    }

    if (first && monitor_thread_init_done)
	monitor_cleanup_leftover_threads();

    /* Don't let any thread advance until all fini_thread and
     * fini_process functions have returned.
     */
    if (first) {
	MONITOR_DEBUG("calling monitor_fini_process() if defined ...\n");
#if defined(HAVE_MPI)
	if (monitor_mpi_is_mpied) {
	  MONITOR_WARN("MPI_Finalize never called before exit!\n");
	}
#endif
	if (real_monitor_fini_process) real_monitor_fini_process();
	monitor_exit_cleanup_done = 1;
    } else {
	while (! monitor_exit_cleanup_done)
	    usleep(MONITOR_POLL_USLEEP_TIME);
    }
}

/* Force the running of fini_thread for any outstanding threads, and
 * then run fini_process.
 */
void
monitor_force_fini_process(void)
{
    monitor_fini_process_function();
}

/****************************************************************************
 * Library initialization and finalization
 ****************************************************************************/

/* Var and name can't be expressions here.
 */
#define MONITOR_TRY_SYMBOL(var, name)  do {		\
    if (var == NULL) {					\
	const char *err_str;				\
	dlerror();					\
	var = dlsym(RTLD_NEXT, (name));			\
	err_str = dlerror();				\
	if (err_str != NULL) {				\
	    MONITOR_WARN("dlsym(%s) failed: %s\n",	\
			 (name), err_str);		\
	}						\
	MONITOR_DEBUG("%s() = %p\n", (name), var);	\
    }							\
} while (0)

#define MONITOR_REQUIRE_SYMBOL(var, name)  do {		\
    if (var == NULL) {					\
	const char *err_str;				\
	dlerror();					\
	var = dlsym(RTLD_NEXT, (name));			\
	err_str = dlerror();				\
	if (var == NULL) {				\
	    MONITOR_ERROR("dlsym(%s) failed: %s\n",	\
			 (name), err_str);		\
	}						\
	MONITOR_DEBUG("%s() = %p\n", (name), var);	\
    }							\
} while (0)

/*
 *  Normally run as part of monitor_normal_init(), but may be run
 *  earlier if someone calls pthread_create() before our library init
 *  function.
 */
static void
monitor_early_init(void)
{
    char *options;

    if (monitor_early_init_done)
	return;

    nuke_global_variables();
#if defined(HAVE_MPI)
    monitor_mpi_opt_debug = 0; /* Debug messages */
#endif
    monitor_opt_error = 0;
    monitor_opt_debug = 0;
    options = getenv("MONITOR_OPTIONS");
    if (options != NULL && strstr(options, "SIGABRT"))
	monitor_opt_error |= MONITOR_SIGABRT;
    if (options != NULL && strstr(options, "SIGINT"))
	monitor_opt_error |= MONITOR_SIGINT;
    if (options != NULL && strstr(options, "NONZERO_EXIT"))
	monitor_opt_error |= MONITOR_NONZERO_EXIT;
    if ((options != NULL && strstr(options, "DEBUG") != NULL)
	|| getenv("MONITOR_DEBUG") != NULL) {
      monitor_opt_debug = 1;
#if defined(HAVE_MPI)
      monitor_mpi_opt_debug = 1;
#endif
    }

    MONITOR_DEBUG("debug = %d, error= %d\n",
		  monitor_opt_debug, monitor_opt_error);

    monitor_early_init_done = 1;
}

/*
 *  Run on the first call to pthread_create().
 */
static void
monitor_thread_init(void)
{
    if (monitor_thread_init_done)
	return;

    LIST_INIT(&monitor_thread_list);
    MONITOR_REQUIRE_SYMBOL(real_pthread_create, "pthread_create");
    MONITOR_REQUIRE_SYMBOL(real_pthread_kill,   "pthread_kill");
    MONITOR_REQUIRE_SYMBOL(real_pthread_mutex_lock,   "pthread_mutex_lock");
    MONITOR_REQUIRE_SYMBOL(real_pthread_mutex_unlock, "pthread_mutex_unlock");
    MONITOR_REQUIRE_SYMBOL(real_pthread_self,  "pthread_self");

    monitor_thread_init_done = 1;
}

/*
 *  Run at library init time.
 */
static void
monitor_normal_init(void)
{
    monitor_early_init();
    if (monitor_normal_init_done)
	return;

    MONITOR_TRY_SYMBOL(real_start_main, "__libc_start_main");
    MONITOR_REQUIRE_SYMBOL(real_start_main, "__BP___libc_start_main");
    MONITOR_TRY_SYMBOL(real_dlopen, "dlopen");
    MONITOR_TRY_SYMBOL(real_execv,  "execv");
    MONITOR_TRY_SYMBOL(real_execvp, "execvp");
    MONITOR_TRY_SYMBOL(real_execve, "execve");
    MONITOR_TRY_SYMBOL(real_fork,   "fork");
    MONITOR_TRY_SYMBOL(real_exit,   "exit");
    MONITOR_TRY_SYMBOL(real__exit,  "_exit");
    MONITOR_TRY_SYMBOL(real_pthread_create, "pthread_create");

       monitor_normal_init_done = 1;

    /* check which of the callback functions are defined by the user */
    real_monitor_init_library = (_monitor_init_library_fptr_t) get_fn_handle("monitor_init_library");
    real_monitor_fini_library = (_monitor_fini_library_fptr_t) get_fn_handle("monitor_fini_library");
    real_monitor_init_process = (_monitor_init_process_fptr_t) get_fn_handle("monitor_init_process");
    real_monitor_fini_process= (_monitor_fini_process_fptr_t) get_fn_handle("monitor_fini_process");
    real_monitor_init_thread_support= (_monitor_init_thread_support_fptr_t) get_fn_handle("monitor_init_thread_support");
    real_monitor_init_thread= (_monitor_init_thread_fptr_t) get_fn_handle("monitor_init_thread");
    real_monitor_fini_thread= (_monitor_fini_thread_fptr_t) get_fn_handle("monitor_fini_thread");
    real_monitor_dlopen= (_monitor_dlopen_fptr_t) get_fn_handle("monitor_dlopen");
    real_monitor_init_mpi= (_monitor_init_mpi_fptr_t) get_fn_handle("monitor_init_mpi");
    real_monitor_fini_mpi= (_monitor_fini_mpi_fptr_t) get_fn_handle("monitor_fini_mpi");
    MONITOR_DEBUG("The following callbacks are registered:\n");
    if (real_monitor_init_library) MONITOR_DEBUG("  monitor_init_library\n");
    if (real_monitor_fini_library) MONITOR_DEBUG("  monitor_fini_library\n");
    if (real_monitor_init_process) MONITOR_DEBUG("  monitor_init_process\n");
    if (real_monitor_fini_process) MONITOR_DEBUG("  monitor_fini_process\n");
    if (real_monitor_init_thread_support) MONITOR_DEBUG("  monitor_init_thread_support\n");
    if (real_monitor_init_thread) MONITOR_DEBUG("  monitor_init_thread\n");
    if (real_monitor_fini_thread) MONITOR_DEBUG("  monitor_fini_thread\n");
    if (real_monitor_dlopen) MONITOR_DEBUG("  monitor_dlopen\n");
    if (real_monitor_init_mpi) MONITOR_DEBUG("  monitor_init_mpi\n");
    if (real_monitor_fini_mpi) MONITOR_DEBUG("  monitor_fini_mpi\n");

    MONITOR_DEBUG("monitor id: $Id: monitor_preload.c,v 1.32 2010/08/10 03:57:07 tmohan Exp $\n");
    if (real_monitor_init_library) {
      MONITOR_DEBUG("calling monitor_init_library() ...\n");
      real_monitor_init_library();
    }
}

/*
 *  Library constructor/deconstructor
 */
#ifdef __GNUC__
extern void __attribute__ ((constructor))
_monitor_init(void)
#else
extern void
_init(void)
#endif
{
    MONITOR_DEBUG("\n");
    monitor_normal_init();
}

#ifdef __GNUC__
extern void
_monitor_fini() __attribute__((destructor));
extern void
_monitor_fini()
#else
extern void _fini()
#endif
{
    MONITOR_DEBUG("\n");
    MONITOR_FINI_LIBRARY;
}

static void getProcCmdLine (int *ac, char ***av)
{
  int i = 0, pid, infile;
  char inbuf[MONITOR_MAX_ARG_STRING_SIZE], file[PATH_MAX];
  char *arg_ptr, **tav;

  *ac = 0;
  memset(inbuf,0,sizeof(inbuf));

  pid = getpid ();
  sprintf (file, "/proc/%d/cmdline", pid);
  infile = syscall(SYS_open,file,O_RDONLY);
  if (infile == -1)
    MONITOR_ERROR("open of %s to get argv",file);
  else if (syscall(SYS_read,infile,inbuf,MONITOR_MAX_ARG_STRING_SIZE) == -1)
    MONITOR_ERROR("read of %d bytes to hold argv: %s", MONITOR_MAX_ARG_STRING_SIZE, strerror(errno));
  else {
    syscall(SYS_close,infile);
    /* Count */
    arg_ptr = inbuf;
    while (*arg_ptr != '\0')
      {
	MONITOR_DEBUG("arg[%d] is %s, %d bytes\n",i,arg_ptr,(int)strlen(arg_ptr));
	arg_ptr += strlen (arg_ptr) + 1;
	i++;
      }
    tav = (char **)malloc(i*sizeof(char *));
    if (tav == NULL) {
      MONITOR_ERROR("Malloc of %d bytes for argv vector",(int)(i*sizeof(char)));
      return;
    }
    /* Copy */
    i = 0;
    arg_ptr = inbuf;
    while (*arg_ptr != '\0')
      {
	tav[i] = strdup (arg_ptr);
	if (tav[i] == NULL) {
	  MONITOR_ERROR("Malloc of %d bytes for argv[%d]",(int)strlen(arg_ptr),i);
	  return;
	}
	arg_ptr += strlen (tav[i]) + 1;
	i++;
      }
    *ac = i;
    *av = tav;
  }
}

static int 
monitor_libc_start_main PARAMS_START_MAIN
{
  char tmp[PATH_MAX], tmp2[PATH_MAX];
  int ret = 0;

  MONITOR_DEBUG("\n");

  monitor_argc = 0;
  monitor_argv = NULL;
  monitor_cmd = NULL;

  getProcCmdLine(&monitor_argc,&monitor_argv);

  if (monitor_argc == 0) {
    /* Get the real exe */
    memset(tmp,0x0,sizeof(tmp));
    memset(tmp2,0x0,sizeof(tmp2));
    sprintf(tmp,"/proc/%d/exe",getpid());
    if (readlink(tmp,tmp2,PATH_MAX) == -1)
      {
	MONITOR_WARN("readlink(%s,%p,%d) failed! (%s)\n",tmp,tmp2,PATH_MAX,strerror(errno)); 
	monitor_cmd = strdup("Unknown");
      }
    else 
      {
	monitor_cmd = strdup(tmp2);
      }
  } else {
    monitor_cmd = strdup(monitor_argv[0]);
  }

  /* If no args, try for something */
  if (ret == 0)
    monitor_argv[0] = strdup(monitor_cmd);

  real_start_main_fini = fini;

  /* Keep track of base pid */
  monitor_root_pid = getpid();

  /* initialize signal handlers */
  monitor_init_sighandlers();
  
  if (atexit(monitor_force_fini_process) == -1)
    {
      MONITOR_ERROR("atexit(%p) failed! (%s)\n",
		    monitor_force_fini_process, strerror(errno));
      monitor_real_exit(1);
    }

  /* EXTERNAL HOOK */
  if (real_monitor_init_process) {
    MONITOR_DEBUG("calling monitor_init_process(cmd = %s, monitor_argc = %p(%d), monitor_argv = %p, pid = %d) ...\n",
		monitor_cmd, &monitor_argc, monitor_argc, monitor_argv, monitor_root_pid);
    real_monitor_init_process(monitor_cmd, &monitor_argc, monitor_argv, monitor_root_pid);
  }
  monitor_init_process_done = 1;

  /* If we've deferred thread_support because pthread_create was run
   * early, then do it now.  This also releases any waiting threads.
   */
  if (monitor_thread_support_deferred) {
      MONITOR_DEBUG("calling monitor_init_thread_support() deferred (if defined) ...\n"); 
      if (real_monitor_init_thread_support) real_monitor_init_thread_support();
      monitor_thread_support_done = 1;
  }

  /* launch the process */
  MONITOR_DEBUG("starting program: %s\n", monitor_cmd);
  (*real_start_main)(main, argc, ubp_av, init,
		     monitor_libc_start_main_fini, rtld_fini, stack_end);

  /* never reached */
  return (0);
}

static void 
monitor_libc_start_main_fini()
{
    MONITOR_DEBUG("\n");

    if (real_start_main_fini) {
	(*real_start_main_fini)();
    }
    MONITOR_FINI_PROCESS;

    if (monitor_cmd) {
	free(monitor_cmd);
	monitor_cmd = NULL;
    }
    if (monitor_argv) {
      int i;
      for (i=0;i<monitor_argc;i++) {
	if (monitor_argv[i]) {
	  free(monitor_argv[i]);
	  monitor_argv[i] = NULL;
	}
      }
      monitor_argv = NULL;
    }
    monitor_argc = 0;
}

extern void 
monitor_parse_execl(const char*** argv, const char* const** envp,
		    const char* arg, va_list arglist)
{
  /* argv & envp are pointers to arrays of char* */
  /* va_start has already been called */

  const char* argp;
  int argvSz = 32, argc = 1;
  
  *argv = malloc((argvSz+1) * sizeof(const char*));
  if (!*argv) { MONITOR_ERROR("malloc() failed!\n"); }
  
  (*argv)[0] = arg;
  while ((argp = va_arg(arglist, const char*)) != NULL) { 
    if (argc > argvSz) {
      argvSz *= 2;
      *argv = realloc(*argv, (argvSz+1) * sizeof(const char*));
      if (!*argv) { MONITOR_ERROR("realloc() failed!\n"); }
    }
    (*argv)[argc] = argp;
    argc++;
  }
  (*argv)[argc] = NULL;
  
  if (envp != NULL) {
    *envp = va_arg(arglist, const char* const*);
  }

  { 
    int i;
    for (i = 0; i < argc; ++i) {
      MONITOR_DEBUG("arg %d: %s\n", i, (*argv)[i]);
    }
  }
  
  /* user calls va_end */
}

static int
monitor_execv PARAMS_EXECV
{
    MONITOR_DEBUG("execv(path = %s, ...)\n", path);

    /* Sanity check to defer turning off library as long as possible.
     */
    if (access(path, X_OK) != 0) {
	MONITOR_DEBUG("execv path not executable\n");
	return (-1);
    }

    MONITOR_FINI_PROCESS;
    MONITOR_FINI_LIBRARY;

    MONITOR_REQUIRE_SYMBOL(real_execv, "execv");
    return (*real_execv)(path, argv);
}

static int
monitor_execvp PARAMS_EXECVP
{
    MONITOR_DEBUG("execvp(file = %s, ...)\n", file);

    /* We better hope this succeeds because we will shut down the
     * library on this event.
     */
    MONITOR_FINI_PROCESS;
    MONITOR_FINI_LIBRARY;

    MONITOR_REQUIRE_SYMBOL(real_execvp, "execvp");

    return (*real_execvp)(file, argv);
}

static int
monitor_execve PARAMS_EXECVE
{
    MONITOR_DEBUG("execve(path = %s, ...)\n", path);

    /* Sanity check to defer turning off library as soon as possible.
     */
    if (access(path, X_OK) != 0) {
	MONITOR_DEBUG("execve path not executable\n");
	return (-1);
    }

    MONITOR_FINI_PROCESS;
    MONITOR_FINI_LIBRARY;

    return monitor_real_execve(path, argv, envp);
}

static pid_t 
monitor_fork()
{
  pid_t pid;

  MONITOR_DEBUG("\n"); 
  
  pid = monitor_real_fork();

  if (pid == 0) { 
    nuke_global_variables();

    /* EXTERNAL HOOK */
    if (real_monitor_init_process) {
      MONITOR_DEBUG("fork -- monitor_init_process(%s,%d,%p,%d)\n",
  		  monitor_cmd,monitor_argc,monitor_argv,monitor_root_pid);
      real_monitor_init_process(monitor_cmd, &monitor_argc, monitor_argv, monitor_root_pid);
    }
    monitor_init_process_done = 1;
  } 
  /* Nothing to do for parent process */
  
  return pid;
}

static void 
monitor__exit(int status)
{
    MONITOR_DEBUG("_exit(%d)\n", status);

    MONITOR_REQUIRE_SYMBOL(real__exit, "_exit");

    (*real__exit)(status);
}

/****************************************************************************
 * Intercepted signals
 ****************************************************************************/

/* We allow the user to kill profiling by intercepting the certain
   signals.  This can be very useful on long-running or misbehaving
   applications. */

static void
monitor_sighandler(int sig)
{
  MONITOR_DEBUG("catching signal %d\n", sig); 
  
  signal(sig, SIG_DFL); /* return to default action */
    
  MONITOR_FINI_PROCESS;
  MONITOR_FINI_LIBRARY;
  
  if (sig == SIGABRT)
    abort();
  else if (sig == SIGINT)
    raise(SIGINT); /* This violates Unix standards...what do they think abort does? */
}

static void 
monitor_init_sighandler(int sig)
{
  typedef void (*sighandler_t)(int);
  sighandler_t dummy;

  dummy = signal(sig, SIG_IGN);
  
  if (dummy != SIG_IGN) 
    signal(sig, monitor_sighandler);
  else 
    {
      MONITOR_WARN("signal %d already has a handler.\n", sig);
      signal(sig, dummy);
    }
}

static void 
monitor_init_sighandlers()
{
  if (monitor_opt_error & MONITOR_SIGINT) 
    monitor_init_sighandler(SIGINT);   /* Ctrl-C */
  if (monitor_opt_error & MONITOR_SIGABRT)
    monitor_init_sighandler(SIGABRT);  /* abort() */
}

/* End stolen code */

static int
monitor_pthread_create PARAMS_PTHREAD_CREATE
{
    struct monitor_thread_node *tn;
    int first, ret;

    MONITOR_DEBUG("\n");

    /* It's possible to get here before _monitor_init().
     */
    monitor_early_init();

    /* There is no race condition to get here first because until now,
     * there is only one thread.
     */
    first = !monitor_thread_init_done;
    if (first)
	monitor_thread_init();

    /* Always launch the thread.  If we're in pthread_create too
     * early, then the new thread will spin-wait until init_process
     * and thread_support are done.
     */
    tn = monitor_make_thread_node();
    tn->tn_start_routine = start_routine;
    tn->tn_arg = arg;

    ret = (*real_pthread_create)
	(thread, attr, monitor_pthread_create_start_routine, (void *)tn);

    if (first) {
	/*
	 * Normally, we run thread_support here, on the first call to
	 * pthread_create.  But if we're here early, before
	 * libc_start_main, then defer thread_support until after
	 * init_process in libc_start_main.
	 */
	if (monitor_init_process_done) {
	    MONITOR_DEBUG("calling monitor_init_thread_support() (if defined) ...\n");
	    if (real_monitor_init_thread_support) real_monitor_init_thread_support();
	    monitor_thread_support_done = 1;
	} else {
	    MONITOR_DEBUG("deferring thread support\n");
	    monitor_thread_support_deferred = 1;
	}
    }

    return (ret);
}

/* This is called from real_pthread_create(), it's where the
 * newly-created thread begins.
 */
static void *
monitor_pthread_create_start_routine(void *arg)
{
    struct monitor_thread_node *tn = arg;
    void *rval;

    /* Wait for monitor_init_thread_support() to finish in the main
     * thread before this thread runs.
     *
     * Note: if this thread is created before libc_start_main (OpenMP
     * does this), then this will wait for both init_process and
     * thread_support to finish from libc_start_main.  And that has
     * the potential to deadlock if the application waits for this
     * thread to accomplish something before it finishes its library
     * init.  (An evil thing for it to do, but it's possible.)
     */
    while (! monitor_thread_support_done)
	usleep(MONITOR_POLL_USLEEP_TIME);

    MONITOR_DEBUG("(start of new thread)\n");

    /* Don't create any new threads after someone has called exit().
     */
    if (monitor_link_thread_node(tn) != 0) {
	MONITOR_WARN("trying to create new thread during exit cleanup -- "
		     "thread not started\n");
	return (NULL);
    }
    MONITOR_DEBUG("calling monitor_init_thread(num = %u) if defined ...\n",
		  tn->tn_thread_num);
    tn->tn_user_data = NULL;
    if (real_monitor_init_thread)
      tn->tn_user_data = real_monitor_init_thread(tn->tn_thread_num);

#if defined(USE_PTHREAD_CLEANUP_PUSH_POP)
    pthread_cleanup_push(monitor_pthread_cleanup_routine, tn);
#endif

    tn->tn_appl_started = 1;
    rval = (tn->tn_start_routine)(tn->tn_arg);

#if defined(USE_PTHREAD_CLEANUP_PUSH_POP)
    pthread_cleanup_pop(1);  
#else
    monitor_pthread_cleanup_routine(tn);
#endif

    return (rval);
}

/* We get here when the application thread has finished, either by
 * returning, pthread_exit() or being canceled.
 */
static void
monitor_pthread_cleanup_routine(void *arg)
{
    struct monitor_thread_node *tn = arg;

    MONITOR_DEBUG("(pthread cleanup)\n");

    if (tn == NULL)
	tn = monitor_my_tnode;
    if (tn == NULL || tn->tn_magic != MONITOR_TN_MAGIC) {
	MONITOR_WARN("missing thread-specific data\n");
	MONITOR_DEBUG("calling monitor_fini_thread(NULL) if defined\n");
	if (real_monitor_fini_thread) real_monitor_fini_thread(NULL);
	return;
    }

    if (! tn->tn_fini_started) {
	MONITOR_DEBUG("calling monitor_fini_thread(data = %p), num = %d (if defined)\n",
		      tn->tn_user_data, tn->tn_thread_num);
	tn->tn_fini_started = 1;
	if (real_monitor_fini_thread) real_monitor_fini_thread(tn->tn_user_data);
	tn->tn_fini_done = 1;
    }
    monitor_unlink_thread_node(tn);
}

/****************************************************************************
 * Provided routines
 * Wrappers to be used by tool code
 ****************************************************************************/

void 
monitor_real_exit(int status)
{
    MONITOR_DEBUG("monitor_real_exit(%d)\n", status);

    MONITOR_TRY_SYMBOL(real_exit, "exit");
    if (real_exit == NULL) {
#if defined(SYS_exit)
        syscall(SYS_exit, status);
#elif defined(__NR_exit)
        syscall(__NR_exit, status);
#else
        MONITOR_ERROR("unable to find 'exit' or equivalent syscall\n");
#endif
    }

    (*real_exit)(status);
}

pid_t
monitor_real_fork(void)
{
    MONITOR_DEBUG("\n");

    MONITOR_TRY_SYMBOL(real_fork, "fork");
    if (real_fork == NULL) {
#if defined(SYS_fork)
	return syscall(SYS_fork);
#elif defined(__NR_fork)
	return syscall(__NR_fork);
#else
	MONITOR_ERROR("unable to find 'fork' or equivalent syscall\n");
#endif
    }

    return (*real_fork)();
}

int
monitor_real_execve PARAMS_EXECVE
{
    MONITOR_DEBUG("monitor_real_execve(%s, ...)\n", path);

    MONITOR_TRY_SYMBOL(real_execve, "execve");
    if (real_execve == NULL) {
#if defined(SYS_execve)
	return syscall(SYS_execve, path, argv, envp);
#elif defined(__NR_execve)
	return syscall(__NR_execve, path, argv, envp);
#else
	MONITOR_ERROR("unable to find 'execve' or equivalent syscall\n");
#endif
    }

    return (*real_execve)(path, argv, envp);
}

void *
monitor_real_dlopen(const char *filename, int flags)
{
    MONITOR_DEBUG("monitor_real_dlopen(%s, %d)\n", filename, flags);

    MONITOR_REQUIRE_SYMBOL(real_dlopen, "dlopen");

    return (*real_dlopen)(filename, flags);
}

/****************************************************************************
 * Intercepted routines
 ****************************************************************************/

/*
 *  Intercept the start routine: this is from glibc and can be one of
 *  two different names depending on how the macro BP_SYM is defined.
 *    glibc-x/sysdeps/generic/libc-start.c
 */

extern int
__libc_start_main PARAMS_START_MAIN
{
  monitor_libc_start_main(main, argc, ubp_av, init, fini, rtld_fini, stack_end);
  return 0; /* never reached */
}


extern int
__BP___libc_start_main PARAMS_START_MAIN
{
  monitor_libc_start_main(main, argc, ubp_av, init, fini, rtld_fini, stack_end);
  return 0; /* never reached */
}

/*
 *  Intercept creation of exec() family of routines because profiling
 *  *must* be off when the exec takes place or it interferes with
 *  ld.so (and the latter will not preload our monitoring library).
 *  Moreover, this allows us to safely write any profile data to disk.
 *
 *  execl, execlp, execle / execv, execvp, execve  
 *
 *  Note: We cannot simply intercept libc'c __execve (which implements
 *  all other routines on Linux) because it is a libc-local symbol.
 *    glibc-x/sysdeps/generic/execve.c
 * 
 *  Note: Stupid C has no way of passing along the input vararg list
 *  intact.
 */

extern int 
execl(const char *path, const char *arg, ...)
{
  const char** argv = NULL;
  va_list arglist;

  va_start(arglist, arg);
  monitor_parse_execl(&argv, NULL, arg, arglist);
  va_end(arglist);
  
  return monitor_execv(path, (char* const*)argv);
}

extern int 
execlp(const char *file, const char *arg, ...)
{
  const char** argv = NULL;
  va_list arglist;
  
  va_start(arglist, arg);
  monitor_parse_execl(&argv, NULL, arg, arglist);
  va_end(arglist);
  
  return monitor_execvp(file, (char* const*)argv);
}

extern int 
execle(const char *path, const char *arg, ...)
{
  const char** argv = NULL;
  const char* const* envp = NULL;
  va_list arglist;

  va_start(arglist, arg);
  monitor_parse_execl(&argv, &envp, arg, arglist);
  va_end(arglist);
  
  return monitor_execve(path, (char* const*)argv, (char* const*)envp);
}

extern int 
execv PARAMS_EXECV
{
  return monitor_execv(path, argv);
}

extern int 
execvp PARAMS_EXECVP
{
  return monitor_execvp(file, argv);
}

extern int 
execve PARAMS_EXECVE
{
  return monitor_execve(path, argv, envp);
}

/*
 *  Intercept creation of new processes via fork(), vfork()
 */

extern pid_t 
fork()
{
  return monitor_fork();
}


extern pid_t 
vfork()
{
  return monitor_fork();
}

/*
 *  Intercept premature exits that disregard registered atexit()
 *  handlers.
 *
 *    'normal' termination  : _exit, _Exit
 *    'abnormal' termination: abort
 *
 *  Note: _exit() implements _Exit().
 *  Note: We catch abort by catching SIGABRT
 */

extern void *dlopen(const char *filename, int flag)
{
    void *retval;

    MONITOR_DEBUG("dlopen(%s,%d)\n",filename,flag);

    retval = monitor_real_dlopen(filename,flag);
    /* Callback */

    if (retval && filename && real_monitor_dlopen)
	real_monitor_dlopen(filename);

    return(retval);
}

extern void 
    _exit(int status)
{
    MONITOR_DEBUG("_exit(status = %d)\n", status);

    if ((monitor_opt_error & MONITOR_NONZERO_EXIT) || (status == 0))
	monitor_force_fini_process();
    MONITOR_FINI_LIBRARY;

    monitor__exit(status);
}

extern void
_Exit(int status)
{
    MONITOR_DEBUG("_Exit(status = %d)\n", status);

    _exit(status);
}

extern void
exit(int status)
{
    MONITOR_DEBUG("exit(status = %d)\n", status);

    if (!(monitor_opt_error & MONITOR_NONZERO_EXIT) && (status != 0))
    {
	/* This prevents callbacks from occuring inside of _fini */
	monitor_root_pid = 0;
    }
    monitor_real_exit(status);
}

/*
 *  Intercept creation of new theads via pthread_create()
 */
extern int
pthread_create PARAMS_PTHREAD_CREATE
{
    MONITOR_DEBUG("\n");

    return monitor_pthread_create(thread, attr, start_routine, arg);
}
