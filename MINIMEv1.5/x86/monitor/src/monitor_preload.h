/* This code has been stripped out and modified slightly by Philip Mucci,
University of Tennessee. This is OPEN SOURCE. */

/* -*-Mode: C;-*- */
/* $Id: monitor_preload.h,v 1.14 2010/08/10 03:57:07 tmohan Exp $ */

/****************************************************************************
//
// File:
//    $Source: /cvs/homes/ospat/monitor/src/monitor_preload.h,v $
//
// Purpose:
//    General header.
//
// Description:
//    Shared declarations, etc for monitoring.
//    
//    *** N.B. *** 
//    There is an automake install hook that will hide these external
//    symbols.
//
// Author:
//    Written by John Mellor-Crummey and Nathan Tallent, Rice University.
//
*****************************************************************************/

#ifndef monitor_h
#define monitor_h

/************************** System Include Files ****************************/

#ifndef __USE_GNU
# define __USE_GNU /* must define on Linux to get RTLD_NEXT from <dlfcn.h> */
# define SELF_DEFINED__USE_GNU
#endif

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>      /* getpid() */
#include <inttypes.h>
#include <memory.h>
#include <errno.h>
#ifdef linux
#  include <linux/limits.h>
#endif
#include <limits.h>
#include <signal.h>
#include <syscall.h>
#include <sys/queue.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dlfcn.h>

#ifdef HAVE_MPI
#include "mpi.h"
#ifdef OMPI_MAJOR_VERSION
/* Weak symbols for OpenMPI's dumb ABI */
extern struct ompi_datatype_t __attribute__((weak)) ompi_mpi_char;
extern struct ompi_communicator_t __attribute__((weak)) ompi_mpi_comm_world;
#endif
#endif

#ifndef LIST_FIRST
#define LIST_FIRST(head)         ((head)->lh_first)
#endif
#ifndef LIST_NEXT
#define LIST_NEXT(elm, field)    ((elm)->field.le_next)
#endif

#define MONITOR_TN_MAGIC  0x6d746e00

#define MONITOR_DEBUG(...)  do {				\
    if (monitor_opt_debug) {					\
	fprintf(stderr, "monitor debug>> %s,%lu,%lu: ", __func__, (long unsigned int)getpid(), (long unsigned int)monitor_gettid()); \
	fprintf(stderr, __VA_ARGS__ );				\
    }								\
} while (0)
#define MONITOR_WARN(...)  do {					\
    fprintf(stderr, "monitor warning>> %s: ", __func__);	\
    fprintf(stderr, __VA_ARGS__ );				\
} while (0)
#define MONITOR_ERROR(...)  do {				\
    fprintf(stderr, "monitor error>> %s: ", __func__);		\
    fprintf(stderr, __VA_ARGS__ );				\
} while (0)
#ifdef HAVE_MPI
#define MONITOR_MPI_DEBUG(...)  do {				\
    if (monitor_mpi_opt_debug) {					\
	fprintf(stderr, "monitor mpi debug>> %s,%d: ", __func__, monitor_mpi_comm_rank());	\
	fprintf(stderr, __VA_ARGS__ );				\
    }								\
} while (0)
#define MONITOR_MPI_ERROR(...)  do {				\
    fprintf(stderr, "monitor mpi error>> %s: ", __func__);		\
    fprintf(stderr, __VA_ARGS__ );				\
} while (0)
#endif
/**************************** Forward Declarations **************************/

#ifdef MAX
# undef MAX
#endif
#ifdef MIN
# undef MIN
#endif
#define MAX(a,b)	((a>=b)?a:b)
#define MIN(a,b)	((a<=b)?a:b)

/**************************** Forward Declarations **************************/

/* 'intercepted' libc routines */
#define PARAMS_START_MAIN (int (*main) (int, char **, char **),              \
			   int argc,                                         \
			   char *__unbounded *__unbounded ubp_av,            \
			   void (*init) (void),                              \
                           void (*fini) (void),                              \
			   void (*rtld_fini) (void),                         \
			   void *__unbounded stack_end)
typedef int (*libc_start_main_fptr_t) PARAMS_START_MAIN;
typedef void (*libc_start_main_fini_fptr_t) (void);

#define PARAMS_EXECV  (const char *path, char *const argv[])
#define PARAMS_EXECVP (const char *file, char *const argv[])
#define PARAMS_EXECVE (const char *path, char *const argv[],                 \
                       char *const envp[])
#define PARAMS_DLOPEN (const char *filename, int flag)
#define PARAMS_PTHREAD_CREATE (pthread_t* thread,                            \
			       const pthread_attr_t* attr,                   \
			       void *(*start_routine)(void*),                \
			       void* arg)

typedef void * (*dlopen_fptr_t) PARAMS_DLOPEN;
typedef int (*execv_fptr_t)  PARAMS_EXECV;
typedef int (*execvp_fptr_t) PARAMS_EXECVP;
typedef int (*execve_fptr_t) PARAMS_EXECVE;
typedef int (*pthread_create_fptr_t) PARAMS_PTHREAD_CREATE;
typedef pthread_t (*pthread_self_fptr_t) (void);
typedef void (*pthread_exit_fptr_t) (void *);
typedef pid_t (*fork_fptr_t) (void);
typedef void (*_exit_fptr_t) (int);
typedef void (*exit_fptr_t) (int);
typedef int (*pthread_mutex_lock_fptr_t)(pthread_mutex_t *mutex);
typedef int (*pthread_kill_fptr_t)(pthread_t thread, int signum);
typedef void *(pthread_func_t)(void *);

struct monitor_thread_node {
    LIST_ENTRY(monitor_thread_node) tn_links;
    int    tn_magic;
    int    tn_thread_num;
    pthread_t  tn_self;
    pthread_func_t *tn_start_routine;
    void  *tn_arg;
    void  *tn_user_data;
    int   tn_appl_started;
    int   tn_fini_started;
    int   tn_fini_done;
#ifdef HAVE_MPI
    int   tn_mpi_rank;
    int   tn_mpi_started;
#endif
};

/* These are added to support the braindead implementation of OMP threads in GCC */

#define PARAMS_GOMP_PARALLEL_START (void (*fn) (void *), void *data, unsigned num_threads)
typedef void (*gomp_parallel_start_fptr_t) PARAMS_GOMP_PARALLEL_START;
typedef void (*gomp_parallel_end_fptr_t) (void);
typedef int (*omp_get_max_threads_fptr_t) (void);

/* These are added to support the braindead implementation of OMP threads in Pathscale */

typedef int (*__ompc_fork_fptr_t) (unsigned unknown1, void (*fn) (void *), void *stack);

/* 'intercepted' libpthread routines */

typedef struct {
  void* (*start_routine)(void*);    /* from pthread_create() */
  void* arg;                        /* from pthread_create() */
  void* hook; /* profiling info */
} monitor_pthread_create_args_t;

#if defined(HAVE_MPI)
extern int monitor_mpi_opt_debug;
extern int monitor_mpi_is_mpied;
extern int monitor_mpi_rank;    /* Our global rank */
extern int monitor_mpi_size;    /* Our global size */
extern int monitor_mpi_thread_support_level; /* MPI thread support level */
#endif

extern __thread struct monitor_thread_node *monitor_my_tnode;
extern int monitor_argc;
extern char **monitor_argv;
#endif
