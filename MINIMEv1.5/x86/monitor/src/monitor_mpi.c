#include "monitor_preload.h"
#include "monitor.h"

#define TS2STR(a) (a == MPI_THREAD_SINGLE ? "MPI_THREAD_SINGLE": a == MPI_THREAD_FUNNELED ? "MPI_THREAD_FUNNELED" : a == MPI_THREAD_SERIALIZED ? "MPI_THREAD_SERIALIZED" : a == MPI_THREAD_MULTIPLE ? "MPI_THREAD_MULTIPLE" : "MPI_THREAD_SINGLE")

typedef int (*_MPI_Init_fptr_t) (int*, char***);
typedef int (*_MPI_Init_thread_fptr_t) (int*, char***, int required, int *provided);
typedef int (*_MPI_Finalize_fptr_t) (void);

typedef void (*_monitor_init_mpi_fptr_t) (int, int, int);
extern _monitor_init_mpi_fptr_t real_monitor_init_mpi;
typedef void (*_monitor_fini_mpi_fptr_t) (void);
extern _monitor_fini_mpi_fptr_t real_monitor_fini_mpi;


/* Visible globals, reset on fork */

int monitor_mpi_size = -1;    /* Our global size */
int monitor_mpi_thread_support_level = -1; /* MPI thread support level */
int monitor_mpi_rank = -1;    /* Our global rank, contained in thread if multithreaded MPI */
int monitor_mpi_is_mpied = 0; /* Whether MPI is now active, contained in thread if multithreaded MPI */

int monitor_mpi_opt_debug = 0; /* Debug messages */

#pragma weak MPI_Comm_rank
#pragma weak MPI_Comm_size
#pragma weak MPI_Query_thread

static void _monitor_mpi_query_thread(int *ptr) {
    MONITOR_DEBUG("(%p) (default callback)\n", ptr);
}

int MPI_Query_thread(int *p) __attribute__ ((weak, alias ("_monitor_mpi_query_thread")));

extern int MPI_Init(int *pargc, char*** pargv) {
  MONITOR_MPI_DEBUG("\n");
  dlerror();
  _MPI_Init_fptr_t real_MPI_Init =  (_MPI_Init_fptr_t)dlsym(RTLD_NEXT, "MPI_Init");
  char *err;
  if ((err = dlerror()) != NULL) {
    MONITOR_MPI_ERROR("dlsym(RTLD_NEXT,MPI_Init) failed. %s", err);
    return MPI_ERR_OTHER;
  }
  int retval = real_MPI_Init(pargc, pargv);
  if (retval == MPI_SUCCESS) {
    MPI_Query_thread(&monitor_mpi_thread_support_level);
    if (monitor_mpi_size == -1)
      MPI_Comm_size(MPI_COMM_WORLD, &monitor_mpi_size);
    if ((monitor_mpi_thread_support_level == MPI_THREAD_SINGLE) || (monitor_my_tnode == NULL)) {
      MPI_Comm_rank(MPI_COMM_WORLD, &monitor_mpi_rank);
    } else {
      MPI_Comm_rank(MPI_COMM_WORLD, &monitor_my_tnode->tn_mpi_rank);
      monitor_my_tnode->tn_mpi_started = 1;
    }
    monitor_mpi_is_mpied = 1;
    MONITOR_MPI_DEBUG("%d of %d MPI tasks, %s thread support\n",monitor_mpi_rank,monitor_mpi_size,TS2STR(monitor_mpi_thread_support_level));
    if (real_monitor_init_mpi) {
      MONITOR_MPI_DEBUG("Calling monitor_init_mpi..\n");
      real_monitor_init_mpi(monitor_mpi_rank,monitor_mpi_size,monitor_mpi_thread_support_level);
    }
    else {
      MONITOR_MPI_DEBUG("monitor_init_mpi not defined. Skipping calling it.\n");
    }
  }
  return (retval);
}

extern int MPI_Init_thread(int *pargc, char*** pargv, int required, int *provided) 
{
  MONITOR_MPI_DEBUG("\n");
  char *err;
  dlerror();
  _MPI_Init_thread_fptr_t real_MPI_Init_thread = (_MPI_Init_thread_fptr_t)dlsym(RTLD_NEXT, "MPI_Init_thread");
  if ((err = dlerror()) != NULL) {
    MONITOR_MPI_DEBUG("dlsym(RTLD_NEXT,MPI_Init_thread) failed. %s", err);
  }
  int retval = real_MPI_Init_thread(pargc, pargv, required, provided);
  if (retval == MPI_SUCCESS) {
    monitor_mpi_thread_support_level = *provided;
    if (monitor_mpi_size == -1)
      MPI_Comm_size(MPI_COMM_WORLD, &monitor_mpi_size);
    if ((monitor_mpi_thread_support_level == MPI_THREAD_SINGLE) || (monitor_my_tnode == NULL)) {
      MPI_Comm_rank(MPI_COMM_WORLD, &monitor_mpi_rank);
    } else {
      MPI_Comm_rank(MPI_COMM_WORLD, &monitor_my_tnode->tn_mpi_rank);
      monitor_my_tnode->tn_mpi_started = 1;
    }
    monitor_mpi_is_mpied = 1;
    MONITOR_MPI_DEBUG("%d of %d, %s thread support\n",monitor_mpi_rank,monitor_mpi_size,TS2STR(monitor_mpi_thread_support_level));
    if (real_monitor_init_mpi) {
      MONITOR_MPI_DEBUG("Calling monitor_init_mpi..\n");
      real_monitor_init_mpi(monitor_mpi_rank, monitor_mpi_size, monitor_mpi_thread_support_level);
    }
    else {
      MONITOR_MPI_DEBUG("monitor_init_mpi not defined. Skipping calling it.\n");
    }
  }
  return(retval);
}

extern int MPI_Finalize(void) 
{
  MONITOR_MPI_DEBUG("%d of %d\n",monitor_mpi_rank,monitor_mpi_size); 
  char *err;
  if (monitor_mpi_is_mpied) {
    if ((monitor_mpi_thread_support_level == MPI_THREAD_SINGLE) || (monitor_my_tnode == NULL))
      monitor_mpi_is_mpied = 0;
    else {
      monitor_my_tnode->tn_mpi_started = 0;
    }
    if (real_monitor_fini_mpi) {
      MONITOR_MPI_DEBUG("Calling monitor_fini_mpi..\n");
      real_monitor_fini_mpi();
    }
    else {
      MONITOR_MPI_DEBUG("monitor_fini_mpi not defined. Skipping calling it.\n");
    }
    if ((monitor_mpi_thread_support_level == MPI_THREAD_SINGLE) || (monitor_my_tnode == NULL)) {
      monitor_mpi_rank = -1;
      monitor_mpi_size = 0;
    } else
      monitor_my_tnode->tn_mpi_rank = -1;
    dlerror();
  }
  _MPI_Finalize_fptr_t real_MPI_Finalize = (_MPI_Finalize_fptr_t)dlsym(RTLD_NEXT, "MPI_Finalize");
  if ((err = dlerror()) != NULL) {
    MONITOR_MPI_ERROR("dlsym(RTLD_NEXT,MPI_Finalize) failed. %s", err);
    return MPI_ERR_OTHER;
  }
  return(real_MPI_Finalize());
}

/* Fortran */

extern void mpi_init__(int *retval) 
{
  *retval = MPI_Init(&monitor_argc, &monitor_argv);
}
extern void mpi_init_thread__(int *required, int *provided, int *retval) 
{
  *retval = MPI_Init_thread(&monitor_argc, &monitor_argv, *required, provided);
}
extern void mpi_finalize__(void) 
{
  MPI_Finalize();
}

#pragma weak mpi_finalize = mpi_finalize__
#pragma weak mpi_finalize_ = mpi_finalize__
#pragma weak MPI_FINALIZE = mpi_finalize__
#pragma weak MPI_FINALIZE_ = mpi_finalize__
#pragma weak MPI_FINALIZE__ = mpi_finalize__
#pragma weak mpi_init = mpi_init__
#pragma weak mpi_init_ = mpi_init__
#pragma weak MPI_INIT = mpi_init__
#pragma weak MPI_INIT_ = mpi_init__
#pragma weak MPI_INIT__ = mpi_init__
#pragma weak mpi_init_thread = mpi_init_thread__
#pragma weak mpi_init_thread_ = mpi_init_thread__
#pragma weak MPI_INIT_THREAD = mpi_init_thread__
#pragma weak MPI_INIT_THREAD_ = mpi_init_thread__
#pragma weak MPI_INIT_THREAD__ = mpi_init_thread__

/* API calls */

int monitor_mpi_comm_size(void)
{
    return (monitor_mpi_size);
}

int monitor_mpi_thread_support(void)
{
    return (monitor_mpi_thread_support_level);
}

int monitor_mpi_comm_rank(void)
{
  if ((monitor_my_tnode == NULL) ||
      (monitor_my_tnode->tn_magic != MONITOR_TN_MAGIC) ||
      (monitor_mpi_thread_support_level == MPI_THREAD_SINGLE))
    return (monitor_mpi_rank);
  else
    return (monitor_my_tnode->tn_mpi_rank);
    
}
