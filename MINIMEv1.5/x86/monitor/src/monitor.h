/* $Id: monitor.h,v 1.17 2009/03/28 17:25:49 mucci Exp $ */

#ifndef libmonitor_h
#define libmonitor_h

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Defined by user code */

extern void monitor_init_library();
extern void monitor_fini_library();
extern void monitor_init_process(char *process, int *argc, char **argv, unsigned tid);
extern void monitor_fini_process();
extern void monitor_init_mpi(int rank, int size, int thread_support_level);
extern void monitor_fini_mpi();
extern void monitor_init_thread_support();
extern void *monitor_init_thread(unsigned tid);
extern void monitor_fini_thread(void *init_thread_data);
extern void monitor_dlopen(const char *library);

/* Defined by libmonitor, they can be weak for clients */

extern unsigned long __attribute__((weak)) monitor_gettid();
extern void __attribute__((weak)) monitor_force_fini_process();
extern void __attribute__((weak)) monitor_real_exit(int);
extern int __attribute__((weak)) monitor_real_execve(const char *filename, char *const argv [], char *const envp[]);
extern pid_t __attribute__((weak)) monitor_real_fork(void);
extern void * __attribute__((weak)) monitor_real_dlopen(const char *, int);
extern int __attribute__((weak)) monitor_opt_debug;
extern int __attribute__((weak)) monitor_opt_error;
extern int __attribute__((weak)) monitor_mpi_comm_size(void);
extern int __attribute__((weak)) monitor_mpi_thread_support(void);
extern int __attribute__((weak)) monitor_mpi_comm_rank(void);

// path functions exported by monitor
extern void __attribute__((weak)) monitor_set_base_output_path(char* app, char *tool, char* prefix, char* user_spec_file, char* user_spec_suffix, int _is_mpied, int job_size);
extern char* __attribute__((weak)) monitor_get_base_output_path(void);
extern char* __attribute__((weak)) monitor_get_proc_output_path(void);
extern char* __attribute__((weak)) monitor_get_thread_output_path(int thread_id);
extern void __attribute__((weak)) monitor_make_output_paths(int is_threaded);

/* Flags for monitor_opt_error that say whether monitor should handle
   abnormal termination. */

#define MONITOR_NONZERO_EXIT 	0x01
#define MONITOR_SIGINT 		0x02
#define MONITOR_SIGABRT 	0x04
#define MONITOR_SIGNALS 	(MONITOR_SIGINT|MONITOR_SIGABRT)

#ifdef __cplusplus
}
#endif

#endif
