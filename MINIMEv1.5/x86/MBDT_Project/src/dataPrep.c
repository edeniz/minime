/*
 * Copyright (c) 2011-2015,
 * Etem Deniz <etem.deniz@boun.edu.tr>, Bogazici University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * dataPrep.c
 *
 *  Created on: Dec 5, 2011
 *      Author: Etem Deniz
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <math.h>
#include "dataPrep.h"

struct tProcData *procs[MAX_PROCS];
int procCount;
int maxGroupId;
struct tProcData *cands[MAX_PROCS];
int candCount;
int maxCandGroupId;
struct tProcData *buProcs[MAX_PROCS];
int buCount;
int buMaxGroupId;
struct tProcData *activeProc;
int maxTid = 0, currentTid = 0;
boolean isRangeAndRoundData = true;

struct tThreadData *createThread(int id);
void destroyThread(struct tThreadData *td);
struct tProcData *createProc(char *procName, int pid);
void destroyProc(struct tProcData * pd);

void initialize() {
	int i;

	for (i = 0; i < MAX_PROCS; i++) {
		procs[i] = NULL;
		cands[i] = NULL;
	}

	procCount = 0;
	candCount = 0;
	maxGroupId = -1;
	maxCandGroupId = -1;
}

void finalize() {
	int i;

	for (i = 0; i < procCount; i++) {
		if (procs[i] != NULL) {
			destroyProc(procs[i]);
		}
	}
	procCount = 0;
	maxGroupId = -1;

	for (i = 0; i < candCount; i++) {
		if (cands[i] != NULL) {
			destroyProc(cands[i]);
		}
	}
	candCount = 0;
	maxCandGroupId = -1;
}

struct tThreadData *createThread(int id) {

	int i;
	struct tThreadData *td;

	assert(id < MAX_THREADS);

	td = (struct tThreadData *) malloc(sizeof (struct tThreadData));

	memset(td->correlation, 0, MAX_THREADS);
	memset(td->dependency, 0, MAX_THREADS);

	td->numOfParDeps = 0;
	td->currParDep = 0;

	td->groupId = maxGroupId;
	for (i = 0; i < MAX_GROUP_NUMBER; i++) {
		td->mtapiExt.numOfChildren[i] = 0;
	}

	td->pc = -1;
	td->mtapiExt.startRoutine.taskId = -1;
	td->id = id;
	td->tid = 0;

	td->msgList = NULL;

	memset(&td->ipcInfo, 0, sizeof(struct tIpcInfo));
	memset(&td->cacheInfo, 0, sizeof(struct tCacheInfo));
	memset(&td->branchInfo, 0, sizeof(struct tBranchInfo));

	activeProc->threads[id] = td;

	return td;
}

struct tThreadData *getThread(int id) {
	return (activeProc->threads[id]);
}

void destroyThread(struct tThreadData *td) {

	free(td);

	return;
}

struct tProcData *createProc(char *procName, int pid) {

	int i, len;
	struct tProcData *pd;
	struct tThreadData * td;

	assert(procCount < MAX_PROCS);

	pd = (struct tProcData *) malloc(sizeof(struct tProcData));
	memset(pd, 0, sizeof(struct tProcData));

	procs[procCount] = pd;
	pd->id = procCount + 1;
	pd->pid = pid;
	if (procName == NULL) {
		procName = "unknown";
	}
	len = strlen(procName);
	pd->name = (char *) malloc(len * sizeof(char) + 1);
	strncpy(pd->name, procName, len);
	pd->name[len] = '\0';
	pd->numOfThreads = 0;
	pd->ipcInfo.ipc = 0;
	pd->cacheInfo.cacheMissRatio = 0;
	pd->branchInfo.branchMissRatio = 0;
	pd->instructions.ccRatio = 0;
	pd->gpuExt.memory.hitRatio = 0;
	pd->gpuExt.memory.unitCount = 0;

	for (i = 0; i < MAX_THREADS; i++) {
		pd->threads[i] = NULL;
	}

	activeProc = pd;
	td = createThread(1);
	td->creator = 0;
	activeProc->numOfThreads++;
	maxGroupId++;

	procCount++;

	return pd;
}

void destroyProc(struct tProcData * pd) {

	int i;

	for (i = 0; i <= pd->numOfThreads; i++) {
		if (pd->threads[i] != NULL) {
			destroyThread(pd->threads[i]);
		}
	}
	free(pd->name);

	procs[pd->id] = NULL;
	procCount -= 1;
	free(pd);
}

/*
 * ---------------------PATTERNS------------------
 * PC of Thread %d: %d
 * Creator of Thread %d: %d
 * Time of Thread %d: %d.%d.%d.%d - %d.%d.%d.%d
 * Instruction Count of Thread %d: %d
 * Correlation of Thread %d: [%d] %d, ...
 * Dependency of Thread %d: [%d] %d, ...
 * Send Messages of Thread %d: [%d] <%d-%d>, ...
 * ISend Messages of Thread %d: [%d] <%d-%d>, ...
 *
 * total private: %d
 * total producer/consumer: %d
 * total migratory: %d
 * total readonly: %d
 * total cachline: %d
 * total mem: %d
 * Total Instruction Count of Process: %d
 * #locks: %d
 * #barriers: %d
 * #conditions: %d
 * ------------------------------------------------
 */

int getId(char *line) {

	int id = 0;
	char *idBegin, cid[STR_INT_MAX];

	while (*line != ':') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cid, idBegin, line - idBegin);
	cid[line - idBegin] = '\0';

	id = atoi(cid);

	return id;
}

int getIdWithSepComma(char *line) {

	int id = 0;
	char *idBegin, cid[STR_INT_MAX];

	while (*line != ',') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cid, idBegin, line - idBegin);
	cid[line - idBegin] = '\0';

	id = atoi(cid);

	return id;
}

long long unsigned int getLongLongIntData(char *line) {

	long long unsigned int data;
	char *idBegin, cdata[STR_LONG_MAX];

	while (*line != '\n') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cdata, idBegin, line - idBegin);
	cdata[line - idBegin] = '\0';

	data = atoll(cdata);

	return data;
}

int getIntData(char *line) {

	int data;
	char *idBegin, cdata[STR_INT_MAX];

	while (*line != '\n') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cdata, idBegin, line - idBegin);
	cdata[line - idBegin] = '\0';

	data = atoi(cdata);

	return data;
}

void fillRelation(int id, char *line, boolean isCorrelation) {

	int cid;
	char *dBegin, cdata[STR_INT_MAX];
	struct tThreadData * td = activeProc->threads[id];

	while (*line != ':') line++;
	line++;
	for (;;) {
		while (*line != '[' && *line != '\n') line++;
		if (*line == '[') {
			dBegin = ++line;
			while (*line != ']') line++;
			strncpy(cdata, dBegin, line - dBegin);
			cdata[line - dBegin] = '\0';
			cid = atoi(cdata);
			line += 2;
			dBegin = line;
			while (*line != ',' && *line != '\n') line++;
			strncpy(cdata, dBegin, line - dBegin);
			cdata[line - dBegin] = '\0';
			if (isCorrelation) {
				td->correlation[cid] = atoi(cdata);
			} else {
				td->dependency[cid] = atoi(cdata);
			}
		} else {
			break;
		}
	}

	return;
}

void setParTime(struct tThreadData *td, int currDepIndex, char *line) {

	char *tBegin, cdata[STR_INT_MAX];

	while (*line != ',') line++;
	line += 2;

	tBegin = line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->parDeps[currDepIndex].time.hour = atoi(cdata);

	tBegin = ++line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->parDeps[currDepIndex].time.minute = atoi(cdata);

	tBegin = ++line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->parDeps[currDepIndex].time.second = atoi(cdata);

	tBegin = ++line;
	while (*line != ':') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->parDeps[currDepIndex].time.nanosec = atoi(cdata);

	return;
}

void fillParRelation(int id, char *line) {

	int cid, currDepIndex;
	char *dBegin, cdata[STR_INT_MAX];
	struct tThreadData * td = activeProc->threads[id];

	currDepIndex = td->numOfParDeps;
	assert(currDepIndex < MAX_PAR_DEP);

	memset(td->parDeps[currDepIndex].dependency, 0, MAX_THREADS);

	setParTime(td, currDepIndex, line);

	while (*line != ':') line++;
	line++;
	for (;;) {
		while (*line != '[' && *line != '\n') line++;
		if (*line == '[') {
			dBegin = ++line;
			while (*line != ']') line++;
			strncpy(cdata, dBegin, line - dBegin);
			cdata[line - dBegin] = '\0';
			cid = atoi(cdata);
			line += 2;
			dBegin = line;
			while (*line != ',' && *line != '\n') line++;
			strncpy(cdata, dBegin, line - dBegin);
			cdata[line - dBegin] = '\0';
			td->parDeps[currDepIndex].dependency[cid] = atoi(cdata);
		} else {
			break;
		}
	}
	td->numOfParDeps++;
	td->parDeps[currDepIndex].globalIndex = activeProc->numOfParDeps;

	activeProc->parDeps[activeProc->numOfParDeps] = &td->parDeps[currDepIndex];
	activeProc->numOfParDeps++;

	return;
}

void fillMessages(int id, char *line, boolean isBlocking) {

	int cid;
	char *dBegin, cdata1[STR_INT_MAX], cdata2[STR_INT_MAX];
	struct tThreadData * td = activeProc->threads[id];

	while (*line != ':') line++;
	line++;
	for (;;) {
		while (*line != '[' && *line != '\n') line++;
		if (*line == '[') {
			dBegin = ++line;
			while (*line != ']') line++;
			strncpy(cdata1, dBegin, line - dBegin);
			cdata1[line - dBegin] = '\0';
			cid = atoi(cdata1);
			line += 3;
			dBegin = line;
			while (*line != '-') line++;
			strncpy(cdata1, dBegin, line - dBegin);
			cdata1[line - dBegin] = '\0';
			line += 1;
			dBegin = line;
			while (*line != '>') line++;
			strncpy(cdata2, dBegin, line - dBegin);
			cdata2[line - dBegin] = '\0';
			if (isBlocking) {
				td->msgData[cid].msgCount = atoi(cdata1);
				td->msgData[cid].totalSize = atoi(cdata2);
			} else {
				td->imsgData[cid].msgCount = atoi(cdata1);
				td->imsgData[cid].totalSize = atoi(cdata2);
			}
		} else {
			break;
		}
	}

	return;
}

void setTimes(int id, char *line) {

	char *tBegin, cdata[STR_INT_MAX];
	struct tThreadData *td = activeProc->threads[id];

	while (*line != ':') line++;
	line += 2;

	tBegin = line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->creationTime.hour = atoi(cdata);

	tBegin = ++line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->creationTime.minute = atoi(cdata);

	tBegin = ++line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->creationTime.second = atoi(cdata);

	tBegin = ++line;
	while (*line != ' ') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->creationTime.nanosec = atoi(cdata);

	line += 2;

	tBegin = ++line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->exitTime.hour = atoi(cdata);

	tBegin = ++line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->exitTime.minute = atoi(cdata);

	tBegin = ++line;
	while (*line != '.') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->exitTime.second = atoi(cdata);

	tBegin = ++line;
	while (*line != '\n') line++;
	strncpy(cdata, tBegin, line - tBegin);
	cdata[line - tBegin] = '\0';
	td->exitTime.nanosec = atoi(cdata);

	return;
}

void processLog1Line(char *line) {

	char *temp;
	int id, data = 0;
	double commRatio = 0, compRatio = 0;

	if (strncmp(line, "Par", 3) == 0) {
		id = getIdWithSepComma(line);
		fillParRelation(id, line);
	} else if (strncmp(line, "PC", 2) == 0) {
		id = getId(line);
		data = getIntData(line);
		if (activeProc->threads[id] == NULL) {
			createThread(id);
			activeProc->numOfThreads++;
		}
		activeProc->threads[id]->pc = data;
	} else if (strncmp(line, "Cr", 2) == 0) {
		id = getId(line);
		data = getIntData(line);
		activeProc->threads[id]->creator = data;
	} else if (strncmp(line, "Ti", 2) == 0) {
		id = getId(line);
		setTimes(id, line);
	} else if (strncmp(line, "Ins", 3) == 0) {
		id = getId(line);
		data = getIntData(line);
		activeProc->threads[id]->instCount = data;
	} else if (strncmp(line, "Co", 2) == 0) {
		id = getId(line);
		fillRelation(id, line, true);
	} else if (strncmp(line, "De", 2) == 0) {
		id = getId(line);
		fillRelation(id, line, false);
	} else if (strncmp(line, "Ca", 2) == 0) {
		id = getId(line);
		data = getIntData(line);
		temp = line +6;
		if (strncmp(temp, "m", 1) == 0) {
			activeProc->threads[id]->cacheMiss = data;
		} else if (strncmp(temp, "i", 1) == 0) {
			activeProc->threads[id]->cacheInvalid = data;
		}
	} else if (strncmp(line, "to", 2) == 0) {
		temp = line +6;
		id = getId(line) - 1;
		data = getIntData(line);
		if (strncmp(temp, "pri", 3) == 0) {
			activeProc->dataSharing[id].private = data;
			maxGroupId++;
		} else if (strncmp(temp, "pro", 3) == 0) {
			activeProc->dataSharing[id].prodCons = data;
		} else if (strncmp(temp, "mi", 2) == 0) {
			activeProc->dataSharing[id].migratory = data;
		} else if (strncmp(temp, "r", 1) == 0) {
			activeProc->dataSharing[id].readonly = data;
		} else if (strncmp(temp, "c", 1) == 0) {
			activeProc->dataSharing[id].total = data;
		} else if (strncmp(temp, "me", 2) == 0) {
			activeProc->dataSharing[id].totalMem = data;
		}
	} else if (strncmp(line, "To", 2) == 0) {
		temp = line + 6;
		id = getId(line) - 1;
		if (strncmp(temp, "I", 1) == 0) {
			activeProc->instructions.instCount = getLongLongIntData(line);
			commRatio = (double) (activeProc->instructions.loadCount + activeProc->instructions.storeCount) / (double) activeProc->instructions.instCount;
			compRatio = 1.0 - commRatio;
			activeProc->instructions.commRatio = (int) (commRatio * 100);
			activeProc->instructions.compRatio = (int) (compRatio * 100);
			activeProc->instructions.ccRatio = (double) activeProc->instructions.commRatio / (double) activeProc->instructions.compRatio;
		} else if (strncmp(temp, "L", 1) == 0) {
			activeProc->instructions.loadCount = getLongLongIntData(line);
		} else if (strncmp(temp, "S", 1) == 0) {
			activeProc->instructions.storeCount = getLongLongIntData(line);
		} else {
			data = getIntData(line);
			temp = line + 12;
			if (strncmp(temp, "m", 1) == 0) {
				activeProc->cacheMiss[id] = data;
			} else if (strncmp(temp, "i", 1) == 0) {
				activeProc->cacheInvalid[id] = data;
			}
		}
	} else if (strncmp(line, "#", 1) == 0) {
		temp = line + 1;
		id = getId(line) - 1;
		data = getIntData(line);
		if (strncmp(temp, "l", 1) == 0) {
			activeProc->syncData[id].locks = data;
		} else if (strncmp(temp, "b", 1) == 0) {
			activeProc->syncData[id].barriers = data;
		} else if (strncmp(temp, "c", 1) == 0) {
			activeProc->syncData[id].conditions = data;
		}
	} else if (strncmp(line, "Se", 2) == 0) {
		id = getId(line);
		fillMessages(id, line, true);
	} else if (strncmp(line, "IS", 2) == 0) {
		id = getId(line);
		fillMessages(id, line, false);
	}

	return;
}

int getSaParamIntValue(char *line) {

	int value;
	char *idBegin, cdata[STR_INT_MAX];

	while (*line != ',' && *line != '>') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cdata, idBegin, line - idBegin);
	cdata[line - idBegin] = '\0';

	value = atoi(cdata);

	return value;
}

double getSaParamDoubleValue(char *line) {

	double value;
	char *idBegin, cdata[STR_DOUBLE_MAX];

	while (*line != ',' && *line != '>') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cdata, idBegin, line - idBegin);
	cdata[line - idBegin] = '\0';

	value = atof(cdata);

	return value;
}

char *getSaParamStrValue(char *line, char *value) {

	char *idBegin;

	while (*line != ',' && *line != '>') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(value, idBegin, line - idBegin);
	value[line - idBegin] = '\0';

	return value;
}

/*
 * RULES:
 * - thread_id's should begin from 2 and should be sequential
 * - The thread_id of main thread is 1
 * - work_size for Pipeline parallel pattern is the total work size.
 * 		Exp: work_size=300, loop_count=10; work per loop is 30 unit
 * - phase's are defined per thread and begin from 1. There are also sequential
 */

int processSaLine(char *line) {

	int i, len;
	int id, value = 0;
	double dValue;
	char cdata[STR_MAX];
	struct tThreadData *activeThread;
	boolean isThread;
	int retVal = 0;

	len = strlen(line);

	for (i = 0; i < len; i++, line++) {

		if (strncmp(line, "thread_id", 9) == 0) {
			//FIXME: assign id by hand
			id = getSaParamIntValue(line);
			if (activeProc->threads[id] == NULL) {
				activeThread = createThread(id);
				activeProc->numOfThreads++;
			}
			isThread = true;
		} else if (strncmp(line, "par_pattern", 11) == 0) {
			getSaParamStrValue(line, cdata);
			if (strcmp(cdata, "TP") == 0) {
				activeProc->threads[id]->standAloneExt.parPattern = PP_TP;
			} else if (strcmp(cdata, "DaC") == 0) {
				activeProc->threads[id]->standAloneExt.parPattern = PP_DaC;
			} else if (strcmp(cdata, "GD") == 0) {
				activeProc->threads[id]->standAloneExt.parPattern = PP_GD;
			} else if (strcmp(cdata, "RD") == 0) {
				activeProc->threads[id]->standAloneExt.parPattern = PP_RD;
			} else if (strcmp(cdata, "Pl") == 0) {
				activeProc->threads[id]->standAloneExt.parPattern = PP_Pl;
			} else if (strcmp(cdata, "EbC") == 0) {
				activeProc->threads[id]->standAloneExt.parPattern = PP_EbC;
			}
		} else if (strncmp(line, "creator_id", 10) == 0) {
			value = getSaParamIntValue(line);
			activeProc->threads[id]->creator = value;
		} else if (strncmp(line, "local_data_size", 15) == 0) {
			value = getSaParamIntValue(line);
			activeProc->threads[id]->standAloneExt.localDataSize = value;
		} else if (strncmp(line, "shared_data_size", 16) == 0) {
			value = getSaParamIntValue(line);
			activeProc->threads[id]->standAloneExt.sharedDataSize = value;
		} else if (strncmp(line, "stage", 5) == 0) {
			value = getSaParamIntValue(line);
			activeProc->threads[id]->standAloneExt.stage = value;
		} else if (strncmp(line, "phase", 5) == 0) {
			value = getSaParamIntValue(line);
			activeProc->threads[id]->standAloneExt.phase = value;
			activeProc->threads[id]->groupId = value;
		} else if (strncmp(line, "work_size", 9) == 0) {
			value = getSaParamIntValue(line);
			activeProc->threads[id]->standAloneExt.workSize = value;
		} else if (strncmp(line, "loop_count", 10) == 0) {
			value = getSaParamIntValue(line);
			activeProc->threads[id]->standAloneExt.loopCount = value;
		} else if (strncmp(line, "application_id", 14) == 0) {
			value = getSaParamIntValue(line);
			activeProc->pid = value;
			isThread = false;
		} else if (strncmp(line, "ipc", 3) == 0) {
			dValue = getSaParamDoubleValue(line);
			if (isThread == true) {
				activeThread->ipcInfo.ipc = dValue;
			} else {
				activeProc->ipcInfo.ipc = dValue;
			}
		} else if (strncmp(line, "cmr", 3) == 0) {
			dValue = getSaParamDoubleValue(line);
			if (isThread == true) {
				activeThread->cacheInfo.cacheMissRatio = dValue;
			} else {
				activeProc->cacheInfo.cacheMissRatio = dValue;
			}
		} else if (strncmp(line, "bmr", 3) == 0) {
			dValue = getSaParamDoubleValue(line);
			if (isThread == true) {
				activeThread->branchInfo.branchMissRatio = dValue;
			} else {
				activeProc->branchInfo.branchMissRatio = dValue;
			}
		} else {
			retVal = -1;
		}

	}

	return retVal;
}

//Metrics:
//Instruction throughput: InstructionsPerCycle
//Warp occupancy: Work-Group Size (, Number of Registers per Work-Item, Local Memory Used per Work-Group)
//Computation-to-memory access ratio: Branch, LDS, ScalarALU, ScalarMem, VectorALU, VectorMem
//Memory instruction mix: VectorMemInstructions + ScalarMemInstructions (Global memory), LDSInstructions (Local memory), ScalarRegReads + ScalarRegWrites + VectorRegReads + VectorRegWrites ([private] Register file)
//Memory efficiency: LDS (Accesses, Reads, EffectiveReads, CoalescedReads, Writes, EffectiveWrites, CoalescedWrites + HitRatio

//Example:
	//NDRange: 0
	//Work dimension: 2
	//Global sizes: 256 256 1
	//Local sizes: 16 16 1
	//Work-items per work-group: 256 8
	//Registers per work-item: 62 8
	//Local memory per work-group: 0 8
	//8 is "Wavefronts per SIMD"

int processGpuLine(char *line) {

	int i, len, ival;
	int retVal = 0;
	static int computeUnitId = -1;
	static int NDRangeDataId = -1;
	double dValue;
	int iValue;

	static boolean isUsed = false;

	len = strlen(line);

	for (i = 0; i < len; i++, line++) {

		if (strncmp(line, "[ ComputeUnit ", 14) == 0) {
			sscanf(line + 14, "%d", &computeUnitId);
			assert(computeUnitId < MAX_COMPUTE_UNITS);
			break;
		} else if (strncmp(line, "[ Device ]", 10) == 0) {
			computeUnitId = -1;
			break;
		} else if (strncmp(line, "NDRangeCount", 12) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 14, "%d", &activeProc->gpuExt.NDRangeCount);
			else
				sscanf(line + 14, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].nDRangeCount);
			break;
		} else if (strncmp(line, "WorkGroupCount", 14) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 16, "%d", &activeProc->gpuExt.workGroupCount);
			else
				sscanf(line + 16, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].workGroupCount);
			break;
		} else if (strncmp(line, "Instructions ", 13) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 15, "%d", &activeProc->gpuExt.instructions);
			else
				sscanf(line + 15, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].instructions);
			break;
		} else if (strncmp(line, "ScalarALUInstructions", 21) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 23, "%d", &activeProc->gpuExt.scalarALUInstructions);
			else
				sscanf(line + 23, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].scalarALUInstructions);
			break;
		} else if (strncmp(line, "ScalarMemInstructions", 21) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 23, "%d", &activeProc->gpuExt.scalarMemInstructions);
			else
				sscanf(line + 23, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].scalarMemInstructions);
			break;
		} else if (strncmp(line, "BranchInstructions", 18) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 20, "%d", &activeProc->gpuExt.branchInstructions);
			else
				sscanf(line + 20, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].branchInstructions);
			break;
		} else if (strncmp(line, "VectorALUInstructions", 21) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 23, "%d", &activeProc->gpuExt.vectorALUInstructions);
			else
				sscanf(line + 23, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].vectorALUInstructions);
			break;
		} else if (strncmp(line, "LDSInstructions", 15) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 17, "%d", &activeProc->gpuExt.LDSInstructions);
			else
				sscanf(line + 17, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].LDSInstructions);
			break;
		} else if (strncmp(line, "VectorMemInstructions", 21) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 23, "%d", &activeProc->gpuExt.vectorMemInstructions);
			else
				sscanf(line + 23, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].vectorMemInstructions);
			break;
		} else if (strncmp(line, "Cycles", 6) == 0) {
			if (computeUnitId == -1)
				sscanf(line + 8, "%d", &activeProc->gpuExt.cycles);
			else
				sscanf(line + 8, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].cycles);
			break;
		} else if (strncmp(line, "InstructionsPerCycle", 20) == 0) {
			if (computeUnitId == -1) {
				sscanf(line + 22, "%d", &ival);
				activeProc->gpuExt.instructionsPerCycle = ival;
			} else {
				sscanf(line + 22, "%d", &ival);
				activeProc->gpuExt.computeUnitData[computeUnitId].instructionsPerCycle = ival;
			}
			break;
		} else if (strncmp(line, "ScalarRegReads", 14) == 0) {
			sscanf(line + 15, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].scalarRegReads);
			break;
		} else if (strncmp(line, "ScalarRegWrites", 15) == 0) {
			sscanf(line + 16, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].scalarRegWrites);
			break;
		} else if (strncmp(line, "VectorRegReads", 14) == 0) {
			sscanf(line + 15, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].vectorRegReads);
			break;
		} else if (strncmp(line, "VectorRegWrites", 15) == 0) {
			sscanf(line + 16, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].vectorRegWrites);
			break;
		} else if (strncmp(line, "LDS.Accesses", 12) == 0) {
			sscanf(line + 15, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].LDS.accesses);
			break;
		} else if (strncmp(line, "LDS.Reads", 9) == 0) {
			sscanf(line + 11, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].LDS.reads);
			break;
		} else if (strncmp(line, "LDS.EffectiveReads", 18) == 0) {
			sscanf(line + 20, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].LDS.effectiveReads);
			break;
		} else if (strncmp(line, "LDS.CoalescedReads", 18) == 0) {
			sscanf(line + 20, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].LDS.coalescedReads);
			break;
		} else if (strncmp(line, "LDS.Writes", 10) == 0) {
			sscanf(line + 12, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].LDS.writes);
			break;
		} else if (strncmp(line, "LDS.EffectiveWrites", 19) == 0) {
			sscanf(line + 21, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].LDS.effectiveWrites);
			break;
		} else if (strncmp(line, "LDS.CoalescedWrites", 19) == 0) {
			sscanf(line + 21, "%d", &activeProc->gpuExt.computeUnitData[computeUnitId].LDS.coalescedWrites);
			break;
		} else if (strncmp(line, "Accesses", 8) == 0) {
			sscanf(line + 10, "%d", &iValue);
			if (iValue > 0) {
				isUsed = true;
			} else {
				isUsed = false;
			}
			break;
		} else if (strncmp(line, "HitRatio", 8) == 0) {
			// Calculate avg. hit ratio of only the used ones
			if (isUsed == true) {
				sscanf(line + 10, "%lf", &dValue);
				activeProc->gpuExt.memory.hitRatio += dValue;
				activeProc->gpuExt.memory.unitCount++;
			}
			break;
		} else if (strncmp(line, "NDRange:", 8) == 0) {
			sscanf(line + 9, "%d", &NDRangeDataId);
			assert(NDRangeDataId < MAX_NDRANGE_COUNT);
			break;
		} else if (strncmp(line, "Work dimension:", 15) == 0) {
			sscanf(line + 16, "%d", &activeProc->gpuExt.NDRangeData[NDRangeDataId].workDim);
			break;
		} else if (strncmp(line, "Global sizes:", 13) == 0) {
			sscanf(line + 14, "%d %d %d", &activeProc->gpuExt.NDRangeData[NDRangeDataId].globalSizes[0],
					&activeProc->gpuExt.NDRangeData[NDRangeDataId].globalSizes[1],
					&activeProc->gpuExt.NDRangeData[NDRangeDataId].globalSizes[2]);
			break;
		} else if (strncmp(line, "Local sizes:", 12) == 0) {
			sscanf(line + 13, "%d %d %d", &activeProc->gpuExt.NDRangeData[NDRangeDataId].localSizes[0],
					&activeProc->gpuExt.NDRangeData[NDRangeDataId].localSizes[1],
					&activeProc->gpuExt.NDRangeData[NDRangeDataId].localSizes[2]);
			break;
		} else if (strncmp(line, "Work-items per work-group:", 26) == 0) {
			sscanf(line + 27, "%d %d", &activeProc->gpuExt.NDRangeData[NDRangeDataId].occupacy.workItemsPerWorkGroup[0],
					&activeProc->gpuExt.NDRangeData[NDRangeDataId].occupacy.workItemsPerWorkGroup[1]);
			break;
		} else if (strncmp(line, "Registers per work-item:", 24) == 0) {
			sscanf(line + 25, "%d %d", &activeProc->gpuExt.NDRangeData[NDRangeDataId].occupacy.registersPerWorkItem[0],
					&activeProc->gpuExt.NDRangeData[NDRangeDataId].occupacy.registersPerWorkItem[1]);
			break;
		} else if (strncmp(line, "Local memory per work-group:", 28) == 0) {
			sscanf(line + 29, "%d %d", &activeProc->gpuExt.NDRangeData[NDRangeDataId].occupacy.localMemoryPerWorkGroup[0],
					&activeProc->gpuExt.NDRangeData[NDRangeDataId].occupacy.localMemoryPerWorkGroup[1]);
			break;
		}

	}

	return retVal;
}

// line =         3,090,470 instructions              #    0.56  insns per cycle
double getValue(char *line) {

	double value;
	char *temp1, *temp2, val[STR_DOUBLE_MAX];

	temp1 = line;
	while (*temp1 != '#' && *temp1 != '\n') temp1++;

	if (*temp1 == '\n') {
		printf("ERROR! Input format error detected in '.perf.log' file.\n");
		printf("INFO: line = %s", line);
		printf("\t\tPlease, check your original and synthetic 'perf.log' files.\n");
	}
	assert (*temp1 != '\n');

	temp1++;
	while (*temp1 == ' ') temp1++;
	temp2 = temp1 + 1;
	while (*temp2 != ' ') temp2++;

	strncpy(val, temp1, temp2 - temp1);
	val[temp2 - temp1] = '\0';

	value = atof(val);

	return value;
}

long long unsigned int getCount(char *line) {

	long long unsigned int value;
	char *temp1, *temp2, valTemp[STR_LONG_MAX], val[STR_LONG_MAX];
	int i, j;

	// line =         3,090,470 instructions              #    0.56  insns per cycle
	temp1 = line;
	while (*temp1 == ' ') temp1++;
	temp2 = temp1 + 1;
	while (*temp2 != ' ') temp2++;

	strncpy(valTemp, temp1, temp2 - temp1);
	valTemp[temp2 - temp1] = '\0';

	for (i = 0, j = 0; i < temp2 - temp1; i++) {
		if (valTemp[i] != ',') {
			val[j++] = valTemp[i];
		}
	}

	value = atoll(val);

	return value;
}

int getValAfterColon(char *line) {

	int val = 0;

	// line = Thread id                     : 2
	while (*line != ':') line++;
	line += 2;

	sscanf(line, "%d", &val);

	return val;
}

long long unsigned int getCountAfterDots(char *line) {

	long long unsigned int val = 0;

	// line = UNHALTED_CORE_CYCLES .........................            64564
	while (*line != ' ') line++;
	line++;
	while (*line == '.') line++;
	while (*line == ' ') line++;

	sscanf(line, "%lld", &val);

	return val;
}

void processLog2Line(char *line) {

	if (isPerThread == true || charType == char_papiex) {
		/* general */
		if (strstr(line, "Thread id") != 0) {
			currentTid = getValAfterColon(line) + 1;
			if (activeProc->threads[currentTid] == NULL) {
				createThread(currentTid);
				activeProc->numOfThreads++;
			}
		} else if (strstr(line, "Num. of threads") != 0) {
			currentTid = 0;
		} else if (strstr(line, "UNHALTED_CORE_CYCLES .") != 0) {
			if (currentTid > 0) {
				activeProc->threads[currentTid]->ipcInfo.cycles += getCountAfterDots(line);
			} else {
				activeProc->ipcInfo.cycles += getCountAfterDots(line);
			}
		} else if (strstr(line, "INSTRUCTION_RETIRED .") != 0) {
			if (currentTid > 0) {
				activeProc->threads[currentTid]->ipcInfo.instructions += getCountAfterDots(line);
				activeProc->threads[currentTid]->ipcInfo.ipc =
						(double) activeProc->threads[currentTid]->ipcInfo.instructions / (double) activeProc->threads[currentTid]->ipcInfo.cycles;
			} else {
				activeProc->ipcInfo.instructions += getCountAfterDots(line);
				activeProc->ipcInfo.ipc = (double) activeProc->ipcInfo.instructions / (double) activeProc->ipcInfo.cycles;
			}
		} else if (strstr(line, "LLC_REFERENCES .") != 0) {
			if (currentTid > 0) {
				activeProc->threads[currentTid]->cacheInfo.cacheReferences += getCountAfterDots(line);
			} else {
				activeProc->cacheInfo.cacheReferences += getCountAfterDots(line);
			}
		} else if (strstr(line, "LLC_MISSES .") != 0) {
			if (currentTid > 0) {
				activeProc->threads[currentTid]->cacheInfo.cacheMisses += getCountAfterDots(line);
				activeProc->threads[currentTid]->cacheInfo.cacheMissRatio =
						(double) activeProc->threads[currentTid]->cacheInfo.cacheMisses / (double) activeProc->threads[currentTid]->cacheInfo.cacheReferences;
			} else {
				activeProc->cacheInfo.cacheMisses += getCountAfterDots(line);
				activeProc->cacheInfo.cacheMissRatio = (double) activeProc->cacheInfo.cacheMisses / (double) activeProc->cacheInfo.cacheReferences;
			}
		} else if (strstr(line, "BRANCH_INSTRUCTIONS_RETIRED .") != 0) {
			if (currentTid > 0) {
				activeProc->threads[currentTid]->branchInfo.branches += getCountAfterDots(line);
			} else {
				activeProc->branchInfo.branches += getCountAfterDots(line);
			}
		} else if (strstr(line, "MISPREDICTED_BRANCH_RETIRED .") != 0) {
			if (currentTid > 0) {
				activeProc->threads[currentTid]->branchInfo.branchMisses += getCountAfterDots(line);
				activeProc->threads[currentTid]->branchInfo.branchMissRatio =
						(double) activeProc->threads[currentTid]->branchInfo.branchMisses / (double) activeProc->threads[currentTid]->branchInfo.branches;
			} else {
				activeProc->branchInfo.branchMisses += getCountAfterDots(line);
				activeProc->branchInfo.branchMissRatio = (double) activeProc->branchInfo.branchMisses / (double) activeProc->branchInfo.branches;
			}
		}

		/* fsl p4080 */
		// will be implemented later
	} else {
		/* general */
		if (strstr(line, "cycles") != 0) {
			activeProc->ipcInfo.cycles = getCount(line);
		} else if (strstr(line, "instructions") != 0) {
			activeProc->ipcInfo.instructions = getCount(line);
			activeProc->ipcInfo.ipc = getValue(line);
		} else if (strstr(line, "cache-references") != 0) {
			activeProc->cacheInfo.cacheReferences = getCount(line);
		} else if (strstr(line, "cache-misses") != 0) {
			activeProc->cacheInfo.cacheMisses = getCount(line);
			activeProc->cacheInfo.cacheMissRatio = 0.01 * getValue(line); /* % of all references, now not */
		} else if ((strstr(line, "branches") != 0) && (strstr(line, "all branches") == 0)) {
			activeProc->branchInfo.branches = getCount(line);
		} else if (strstr(line, "branch-misses") != 0) {
			activeProc->branchInfo.branchMisses = getCount(line);
			activeProc->branchInfo.branchMissRatio = 0.01 * getValue(line);
		}
		/* fsl p4080 */
		else if (strstr(line, "r1") != 0) {
			activeProc->ipcInfo.cycles = getCount(line);
		} else if (strstr(line, "r2") != 0) {
			activeProc->ipcInfo.instructions = getCount(line);
			assert(activeProc->ipcInfo.cycles > 0);
			activeProc->ipcInfo.ipc = (double) activeProc->ipcInfo.instructions / (double) activeProc->ipcInfo.cycles;
		} else if (strstr(line, "r6e") != 0) {
			activeProc->cacheInfo.cacheReferences = getCount(line);
		} else if (strstr(line, "r6f") != 0) {
			activeProc->cacheInfo.cacheMisses = activeProc->cacheInfo.cacheReferences - getCount(line);
			assert(activeProc->cacheInfo.cacheReferences > 0);
			activeProc->cacheInfo.cacheMissRatio = (double) activeProc->cacheInfo.cacheMisses / (double) activeProc->cacheInfo.cacheReferences;
		} else if ((strstr(line, "rc") != 0)) {
			activeProc->branchInfo.branches = getCount(line);
		} else if (strstr(line, "rf") != 0 && strstr(line, "erfo") == 0) {
			activeProc->branchInfo.branchMisses = getCount(line);
			assert(activeProc->branchInfo.branches > 0);
			activeProc->branchInfo.branchMissRatio = (double) activeProc->branchInfo.branchMisses / (double) activeProc->branchInfo.branches;
		}
	}

	return;
}



int prepProcData(char *fileName1, char *fileName2, char *procName, int pid) {

	char line[STR_LINE_MAX];
	FILE *fr1, *fr2;

	fr1 = fopen(fileName1, "r+");
	if (fr1 != NULL) {

		if (fileName2 != NULL) {
			fr2 = fopen(fileName2, "r+");
			if (fr2 != NULL) {

				createProc(procName, pid);

				while (!feof(fr1)) {
					fgets(line, STR_LINE_MAX, fr1);
					processLog1Line(line);
					line[0] = '\0';
				}

				while (!feof(fr2)) {
					fgets(line, STR_LINE_MAX, fr2);
					processLog2Line(line);
					line[0] = '\0';
				}
				maxTid = activeProc->numOfThreads;

				fclose(fr1);
				fclose(fr2);

			} else {
				fclose(fr1);
				printf("ERROR! Cannot open %s.\n", fileName2);
				return -3;
			}
		} else {
			createProc(procName, pid);
			while (!feof(fr1)) {
				fgets(line, STR_LINE_MAX, fr1);
				processLog1Line(line);
			}
			fclose(fr1);
		}
	} else {
		printf("ERROR! Cannot open %s.\n", fileName1);
		return -3;
	}

	return 0;
}

int prepProcDataSa(char *fileName, char *procName, int pid) {

	char line[STR_LINE_MAX];
	FILE *fr1;

	fr1 = fopen(fileName, "r+");
	if (fr1 != NULL) {

			createProc(procName, pid);

			while (!feof(fr1)) {
				fgets(line, STR_LINE_MAX, fr1);
				if (processSaLine(line) != 0) {
					processLog2Line(line);
					line[0] = '\0';
				}
			}
			maxTid = activeProc->numOfThreads;

			fclose(fr1);

	} else {
		printf("ERROR! Cannot open %s.\n", fileName);
		return -2;
	}

	return 0;
}

int prepProcDataGpu(char *fileName, char *procName, int pid) {

	char line[STR_LINE_MAX];
	FILE *fr1;

	fr1 = fopen(fileName, "r+");
	if (fr1 != NULL) {

			createProc(procName, pid);

			while (!feof(fr1)) {
				fgets(line, STR_LINE_MAX, fr1);
				if (processGpuLine(line) != 0) {
					line[0] = '\0';
				}
			}
			maxTid = activeProc->numOfThreads;

			fclose(fr1);

	} else {
		printf("ERROR! Cannot open %s.\n", fileName);
		return -2;
	}

	return 0;
}

char *getBaseName(char *path) {
	char *name;
	int i, len;

	len = strlen(path);
	for (i = len - 1; i >= 0; i--) {
		if (path[i] == '/' || path[i] == '\\') {
			break;
		}
	}
	if (i > 0) {
		name = &path[i + 1];
	} else {
		name = path;
	}
	return name;
}

int checkProcsData() {

	int i, j;
	struct tProcData *itProc;
	struct tThreadData *itThread;

	if (isPerThread == true) {
		for (i = 0; i < procCount; i++) {
			itProc = procs[i];
			// except main thread
			for (j = 2; j <= itProc->numOfThreads; j++) {
				itThread = itProc->threads[j];
				if (itThread->branchInfo.branches <= 0) {
					printf("ERROR! Total branch count (=%lld) is not set correctly for %s application, tid=%d.\n",
							itThread->branchInfo.branches, itProc->name, itThread->tid);
					return -3;
				}
				if (itThread->cacheInfo.cacheReferences <= 0) {
					printf("ERROR! Total cache count (=%lld) is not set correctly for %s application, tid=%d.\n",
							itThread->cacheInfo.cacheReferences, itProc->name, itThread->tid);
					return -3;
				}
			}
		}
	} else {
		for (i = 0; i < procCount; i++) {
			itProc = procs[i];
			if (itProc->branchInfo.branches <= 0) {
				printf("ERROR! Total branch count (=%lld) is not set correctly for %s application.\n",
						itProc->branchInfo.branches, itProc->name);
				return -3;
			}
			if (itProc->cacheInfo.cacheReferences <= 0) {
				printf("ERROR! Total cache count (=%lld) is not set correctly for %s application.\n",
						itProc->cacheInfo.cacheReferences, itProc->name);
				return -3;
			}
			if (itProc->instructions.loadCount <= 0) {
				printf("ERROR! Load instruction count (=%lld) is not set correctly for %s application.\n",
						itProc->instructions.loadCount, itProc->name);
				return -3;
			}
			if (itProc->instructions.storeCount <= 0) {
				printf("ERROR! Store instruction count (=%lld) is not set correctly for %s application.\n",
						itProc->instructions.storeCount, itProc->name);
				return -3;
			}
			if (itProc->instructions.instCount <= 0) {
				printf("ERROR! Total instruction count (=%lld) is not set correctly for %s application.\n",
						itProc->instructions.instCount, itProc->name);
				return -3;
			}
		}
	}

	return 0;
}

void checkAndFixDataRanges() {

	int i, j;
	struct tProcData *itProc;
	struct tThreadData *itThread;

	if (isPerThread == true) {
		for (i = 0; i < procCount; i++) {
			itProc = procs[i];
			// except main thread
			for (j = 2; j <= itProc->numOfThreads; j++) {
				itThread = itProc->threads[j];
				if (itThread->ipcInfo.ipc < MIN_DATA_VALUE) {
					itThread->ipcInfo.ipc = MIN_DATA_VALUE;
				}
				if (itThread->branchInfo.branchMissRatio < MIN_DATA_VALUE) {
					itThread->branchInfo.branchMissRatio = MIN_DATA_VALUE;
				}
				if (itThread->cacheInfo.cacheMissRatio < MIN_DATA_VALUE) {
					itThread->cacheInfo.cacheMissRatio = MIN_DATA_VALUE;
				}
			}
			if (itProc->instructions.ccRatio < MIN_DATA_VALUE) {
				itProc->instructions.ccRatio = MIN_DATA_VALUE;
			}
		}
	} else {
		for (i = 0; i < procCount; i++) {
			itProc = procs[i];
			if (itProc->ipcInfo.ipc < MIN_DATA_VALUE) {
				itProc->ipcInfo.ipc = MIN_DATA_VALUE;
			}
			if (itProc->branchInfo.branchMissRatio < MIN_DATA_VALUE) {
				itProc->branchInfo.branchMissRatio = MIN_DATA_VALUE;
			}
			if (itProc->cacheInfo.cacheMissRatio < MIN_DATA_VALUE) {
				itProc->cacheInfo.cacheMissRatio = MIN_DATA_VALUE;
			}
			if (itProc->instructions.ccRatio < MIN_DATA_VALUE) {
				itProc->instructions.ccRatio = MIN_DATA_VALUE;
			}
		}
	}
	
	return;
}

double dRoundNearest(double val) {

	double nearest = floorf(val * 1000 + 0.5) / 1000;
	return nearest;

}

void roundData() {

	int i, j;
	struct tProcData *itProc;
	struct tThreadData *itThread;

	if (isPerThread == true) {
		for (i = 0; i < procCount; i++) {
			itProc = procs[i];
			// except main thread
			for (j = 2; j <= itProc->numOfThreads; j++) {
				itThread = itProc->threads[j];
				itThread->ipcInfo.ipc = dRoundNearest(itThread->ipcInfo.ipc);
				itThread->branchInfo.branchMissRatio = dRoundNearest(itThread->branchInfo.branchMissRatio);
				itThread->cacheInfo.cacheMissRatio = dRoundNearest(itThread->cacheInfo.cacheMissRatio);
			}
			itProc->instructions.ccRatio = dRoundNearest(itProc->instructions.ccRatio);
		}
	} else {
		for (i = 0; i < procCount; i++) {
			itProc = procs[i];
			itProc->ipcInfo.ipc = dRoundNearest(itProc->ipcInfo.ipc);
			itProc->branchInfo.branchMissRatio = dRoundNearest(itProc->branchInfo.branchMissRatio);
			itProc->cacheInfo.cacheMissRatio = dRoundNearest(itProc->cacheInfo.cacheMissRatio);
			itProc->instructions.ccRatio = dRoundNearest(itProc->instructions.ccRatio);
		}
	}

	return;
}

int prepData(int fileCount, char *apps[MAX_PROCS * 2]) {

	int i, pid, isSuccess;
	char *fName1, *fName2, *temp1, *temp2;
	char procName[STR_NAME_MAX], procId[STR_INT_MAX];
	int retVal;

	for (i = 0; i < fileCount; i++) {
		maxGroupId = -1;
		fName1 = apps[i];
		temp1 = getBaseName(fName1);
		while(*temp1 != '.' && *temp1 != '\0') temp1++;
		if (*temp1 == '\0') return -2;
		temp1++;
		temp2 = temp1;
		while(*temp2 != '.' && temp2 != '\0') temp2++;
		if (*temp2 == '\0') return -2;
		strncpy(procName, temp1, temp2 - temp1);
		procName[temp2 - temp1] = '\0';
		temp1 = ++temp2;
		while(*temp2 != '.' && temp2 != '\0') temp2++;
		if (*temp2 == '\0') return -2;
		strncpy(procId, temp1, temp2 - temp1);
		procId[temp2 - temp1] = '\0';
		pid = atoi(procId);
		i++;
		if (i < fileCount) {
			fName2 = apps[i];
		} else {
			fName2 = NULL;
		}

		isSuccess = prepProcData(fName1, fName2, procName, pid); /* cpu log file */
		if (isSuccess != 0) {
			return -1;
		}

		if (fileCount > 1) {
			retVal = checkProcsData();
			if (retVal < 0) {
				return retVal;
			}
		}

		if (isRangeAndRoundData == true) {
			roundData();
			checkAndFixDataRanges();
		}
	}

	return 0;
}

int prepDataSa(int fileCount, char *apps[MAX_PROCS * 2]) {

	int i, isSuccess;

	for (i = 0; i < fileCount; i++) {
		isSuccess = prepProcDataSa(apps[i], apps[i], i); /* stand-alone application file */
		if (isSuccess != 0) {
			return -1;
		}
	}

	if (isRangeAndRoundData == true) {
		roundData();
		checkAndFixDataRanges();
	}

	return 0;
}

//Metrics:
//+ Instruction throughput: InstructionsPerCycle
//+ Warp occupancy: Work-Group Size (, Number of Registers per Work-Item, Local Memory Used per Work-Group)
//+ Computation-to-memory access ratio: Branch, LDS, ScalarALU, ScalarMem, VectorALU, VectorMem
//+ Memory instruction mix: VectorMemInstructions + ScalarMemInstructions (Global memory), LDSInstructions (Local memory), ScalarRegReads + ScalarRegWrites + VectorRegReads + VectorRegWrites ([private] Register file)
//+ Memory efficiency: LDS (Accesses, Reads, EffectiveReads, CoalescedReads, Writes, EffectiveWrites, CoalescedWrites + HitRatio

void dumpGpuData(char *procName) {

	int i;
	double totalComp, totalComm;
	double globalMem, localMem, privateMem, totalMem;
	double totalAccesses, totalCoalescedAccesses;
	char chars[STR_LINE_MAX] = { '\0' };
	char fileName[STR_NAME_MAX] = { '\0' };

	totalComp = (double) activeProc->gpuExt.branchInstructions + (double) activeProc->gpuExt.scalarALUInstructions +
			(double) activeProc->gpuExt.vectorALUInstructions;
	totalComm = (double) activeProc->gpuExt.LDSInstructions + (double) activeProc->gpuExt.scalarMemInstructions +
			(double) activeProc->gpuExt.vectorMemInstructions;

	globalMem = (double) activeProc->gpuExt.vectorMemInstructions + (double) activeProc->gpuExt.scalarMemInstructions;
	localMem = (double) activeProc->gpuExt.LDSInstructions;
	privateMem = 0;
	for (i = 0; i < MAX_COMPUTE_UNITS; i++) {
		privateMem += (double) activeProc->gpuExt.computeUnitData[i].scalarRegReads
				+ (double) activeProc->gpuExt.computeUnitData[i].scalarRegWrites
				+ (double) activeProc->gpuExt.computeUnitData[i].vectorRegReads
				+ (double) activeProc->gpuExt.computeUnitData[i].vectorRegWrites;
	}
	totalMem = globalMem + localMem + privateMem;

	totalAccesses = 0;
	totalCoalescedAccesses = 0;
	for (i = 0; i < MAX_COMPUTE_UNITS; i++) {
		totalAccesses += (double) activeProc->gpuExt.computeUnitData[i].LDS.accesses;
		totalCoalescedAccesses += (double) activeProc->gpuExt.computeUnitData[i].LDS.coalescedReads
				+ (double) activeProc->gpuExt.computeUnitData[i].LDS.coalescedWrites;
	}

	sprintf(chars, "%lf; ", activeProc->gpuExt.instructionsPerCycle);
	sprintf(chars + strlen(chars), "%d; %d; ", activeProc->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[0],
			activeProc->gpuExt.NDRangeData[0].occupacy.workItemsPerWorkGroup[1]);
	sprintf(chars + strlen(chars), "%d; %d; ", activeProc->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0],
			activeProc->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[1]);
	sprintf(chars + strlen(chars), "%d; %d; ", activeProc->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[0],
			activeProc->gpuExt.NDRangeData[0].occupacy.localMemoryPerWorkGroup[1]);
	sprintf(chars + strlen(chars), "%lf; ", totalComm / totalComp);
	sprintf(chars + strlen(chars), "%lf; %lf; %lf; ", globalMem / totalMem,
			localMem / totalMem, privateMem / totalMem);
	if (totalAccesses > 0)
		sprintf(chars + strlen(chars), "%lf; ", totalCoalescedAccesses / totalAccesses);
	else
		sprintf(chars + strlen(chars), "%lf; ", (double) 0);
	sprintf(chars + strlen(chars), "%lf\n", activeProc->gpuExt.memory.hitRatio);

	printf("%s", chars);

	strcpy(fileName, procName);
	strcat(fileName, "_chars.txt");
	FILE *cFile = fopen(fileName, "w+");
	fprintf(cFile, "%s", chars);
	fclose(cFile);

	return;
}

void calculateKernelCount(struct tDeviceData *pGpuExt) {

	int i, j;
	boolean isExist;

	pGpuExt->kernelCount = 1;
	memcpy(&(pGpuExt->uniqueNDRanges[0]), &(pGpuExt->NDRangeData[0]), sizeof (struct tNDRangeData));

			for (i = 1; i < pGpuExt->NDRangeCount; i++) {
				isExist = false;
				for (j = 0; j < pGpuExt->kernelCount; j++) {
					if (pGpuExt->NDRangeData[i].workDim == pGpuExt->uniqueNDRanges[j].workDim &&
							pGpuExt->NDRangeData[i].globalSizes[0] == pGpuExt->uniqueNDRanges[j].globalSizes[0] &&
							pGpuExt->NDRangeData[i].globalSizes[1] == pGpuExt->uniqueNDRanges[j].globalSizes[1] &&
							pGpuExt->NDRangeData[i].globalSizes[2] == pGpuExt->uniqueNDRanges[j].globalSizes[2] &&
							pGpuExt->NDRangeData[i].localSizes[0] == pGpuExt->uniqueNDRanges[j].localSizes[0] &&
							pGpuExt->NDRangeData[i].localSizes[1] == pGpuExt->uniqueNDRanges[j].localSizes[1] &&
							pGpuExt->NDRangeData[i].localSizes[2] == pGpuExt->uniqueNDRanges[j].localSizes[2]) {

						//FIXME: check also Work-items per work-group, Registers per work-item, and
						//Local memory per work-group to calculate kernel count?

						isExist = true;
						break;
					}
				}
				if (isExist == false) {
					assert(pGpuExt->kernelCount < MAX_UNIQUE_NDRANGES);
					memcpy(&(pGpuExt->uniqueNDRanges[pGpuExt->kernelCount]),
							&(pGpuExt->NDRangeData[i]), sizeof (struct tNDRangeData));
					pGpuExt->kernelCount++;
				}
			}

	return;
}

void calculateMaxWorkDim(struct tDeviceData *pGpuExt) {

	int i, maxWorkDim = 1;

	for (i = 0; i < pGpuExt->kernelCount; i++) {
		if (pGpuExt->uniqueNDRanges[i].workDim > maxWorkDim) {
			maxWorkDim = pGpuExt->uniqueNDRanges[i].workDim;
		}
	}

	pGpuExt->maxWorkDim = maxWorkDim;

	return;
}

int prepDataGpu(int fileCount, char *apps[MAX_PROCS * 2], boolean isDumpData) {

	int i, isSuccess;
	char *temp1, *temp2;
	char procName[STR_NAME_MAX];

	for (i = 0; i < fileCount; i++) {
		temp1 = getBaseName(apps[i]);
		temp2 = temp1;
		while(*temp2 != '.' && temp2 != '\0') temp2++;
		if (*temp2 == '\0') return -2;
		strncpy(procName, temp1, temp2 - temp1);
		procName[temp2 - temp1] = '\0';

		isSuccess = prepProcDataGpu(apps[i], procName, i); /* gpu log file */
		if (isSuccess != 0) {
			return -1;
		}

		calculateKernelCount(&activeProc->gpuExt);

		calculateMaxWorkDim(&activeProc->gpuExt);

		if (activeProc->gpuExt.memory.unitCount > 0) {
			activeProc->gpuExt.memory.hitRatio = activeProc->gpuExt.memory.hitRatio / activeProc->gpuExt.memory.unitCount;
		}

		// Since the IPC in the log file is integer value, re-calculate it as double value
//		activeProc->gpuExt.instructionsPerCycle = (double) activeProc->gpuExt.instructions / (double) activeProc->gpuExt.cycles;

		//TODO: remove the following code after the bug fix of the m2s
#ifdef DEBUG_ON
		printf("[DEBUG] Registers per Work Item: %d\n", activeProc->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0]);
#endif
		activeProc->gpuExt.NDRangeData[0].occupacy.registersPerWorkItem[0] = 0;

#ifdef DEBUG_ON
		printf("[DEBUG] Hit Ratio: %.3f\n", activeProc->gpuExt.memory.hitRatio);
#endif
		activeProc->gpuExt.memory.hitRatio = 0;

		if (isDumpData) {
			dumpGpuData(procName);
		}
	}

	return 0;
}
