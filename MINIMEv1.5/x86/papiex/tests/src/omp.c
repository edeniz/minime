#ifdef _OPENMP
#include <omp.h>
#else
#error "This compiler does not understand OPENMP"
#endif
#include <stdlib.h>
#include <stdio.h>

volatile double a = 0.1, b = 1.1, c = 2.1;

void Thread(int n)
{
  int i;
  printf("OpenMP Thread %d: %d iteratons\n",omp_get_thread_num(),n);
  for (i=0;i<n;i++)
    a += b * c;
}

int main(int argc, char **argv)
{
   int maxthr, retval;
   
#pragma omp parallel
   {
      Thread(1000000 * (omp_get_thread_num()+1));
   }
#pragma omp barrier

   exit(0);
}
