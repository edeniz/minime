#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
  char pwd[PATH_MAX];
  getcwd(pwd,PATH_MAX);
  strcat(pwd,"/basic");
  printf("system(%s)\n",pwd);
  exit(system(pwd));
}
