#include <assert.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <fenv.h>
#ifdef HAVE_MPI
#include <mpi.h>
#ifdef HAVE_MONITOR
#else
#error HAVE_MPI requires HAVE_MONITOR presently
#endif
#endif
#include "papiex_internal.h"

#ifdef HAVE_SCPERFUTILS
#  include "scperfutils.c"
#endif

#define STRINGIFY(X) #X
 
#define ADD_DERIVED_DESC(metric, text) \
  if (!not_first_pass) { \
    sprintf(desc, "%-30s: %s\n", metric, text); \
    strncat(derived_events_desc, desc, sizeof(derived_events_desc)-1); \
  }


static const char* SCOPE_STRING[] = {"THREAD_SCOPE", "PROCESS_SCOPE", "JOB_SCOPE"};
static const int PAD_LENGTH = 46;

static const int TYPE_FLOAT=0;
static const int TYPE_LONG=1;
static const int TYPE_STRING=2;

static int eventcnt = 0;
static char *eventnames[PAPIEX_MAX_COUNTERS];
static int eventcodes[PAPIEX_MAX_COUNTERS];
static const PAPI_exe_info_t *exeinfo;
static const PAPI_hw_info_t *hwinfo;
static int multiplex;
static int papi_broken_read_ts = 0;
static int papi_broken_stop =0 ;
static int mpx_interval= PAPIEX_DEFAULT_MPX_HZ; // in Hertz
int _papiex_threaded;
/* To satisfy eventinfo.c used in libpapiex.so */
char *tool = "papiex";
char *build_date = __DATE__;
char *build_time = __TIME__;
char *svn_revision = "svn $Id: papiex.c,v 1.57 2010/07/28 16:27:15 mucci Exp $";

static int nthreads = 1;
static char *file_output;  // this is set for prefix and file output
static char *output_file_name = NULL;
static int file_prefix;
static int domain;
static int rusage;
static int memory;
static int quiet;
static int no_follow_fork;
static char *process_name = NULL;
static char process_args[PATH_MAX] = "";
static const char *fullname;
static const char *processor;
static double clockrate = 1.0;
int _papiex_debug;
static void *thread_id_handle;
static char hostname[PATH_MAX] = "";
static int no_write = 0;   /* no file output */
static int write_only = 0; /* no output to console */
static int no_summary_stats = 0; /* don't generate summary statistics */
static int no_mpi_gather = 0; /* Don't use MPI to gather */
static int no_scientific = 0;

static void *dlproc_handle = NULL;
static char top_level_path[PATH_MAX];
static char proc_stats_path[PATH_MAX];
static char user_path_prefix[PATH_MAX];
static PAPI_all_thr_spec_t all_threads;
static int all_threads_size = PAPIEX_INIT_THREADS;

/* instead of using the global proc_init_time, we should
 * ideally add fields for start and end time in the
 * PAPI_all_thread structure
 */
static struct timeval proc_init_time ;
static struct timeval proc_fini_time ;

/* Note: is_mpied is set ONLY when the MPI library is _used_ 
 * We do not set it merely on linking with the MPI library.
 * (since mpicc links with the library even if the program
 * doesn't use MPI calls in its run). We actually intercept 
 * the call to MPI_Finalize and set this variable there. 
 */
int uses_mpi = 0;  /* set if program uses MPI (regardless whether we intercept calls */
int is_mpied = 0;  /* set once we intercept MPI_Init or MPI_Init_thread */

#ifdef HAVE_MPI
#  ifndef USE_MONITOR_MPI
typedef int (*_MPI_Init_fptr_t) (int*, char***);
typedef int (*_MPI_Init_thread_fptr_t) (int*, char***, int required, int *provided);
typedef int (*_MPI_Finalize_fptr_t) (void);
static _MPI_Finalize_fptr_t real_MPI_Finalize = NULL; /*valid if is_mpied set */
static _MPI_Init_fptr_t real_MPI_Init = NULL; /*valid if is_mpied set */
static _MPI_Init_thread_fptr_t real_MPI_Init_thread = NULL; /*valid if is_mpied set */

/* For mpich we can pick all the functions dyamically and don't need to link
 * with the MPICH library / or use mpicc */
typedef int (*_MPI_Comm_size_fptr_t) (MPI_Comm, int*);
typedef int (*_MPI_Comm_rank_fptr_t) (MPI_Comm, int*);
static _MPI_Comm_size_fptr_t fptr_MPI_Comm_size = NULL;
static _MPI_Comm_rank_fptr_t fptr_MPI_Comm_rank = NULL;
#  endif
typedef int (*_MPI_Gather_fptr_t) (void *, int, MPI_Datatype, void*, int, MPI_Datatype, int, MPI_Comm);
typedef int (*_MPI_Bcast_fptr_t) (void *, int, MPI_Datatype, int, MPI_Comm);
static _MPI_Gather_fptr_t fptr_MPI_Gather = NULL;
static _MPI_Bcast_fptr_t fptr_MPI_Bcast = NULL;
#endif

static char *stringify_domain(int domain);
static void print_global_stats(papiex_perthread_data_t *_task_data, FILE *output);
static void print_thread_stats(FILE *output, papiex_perthread_data_t *thread, unsigned long tid, 
                               int scope, float process_walltime);

static void print_process_stats(PAPI_all_thr_spec_t *process, 
                                papiex_perthread_data_t *process_data_sums,
                                FILE *output) ;

static papiex_perthread_data_t*  papiex_mpi_gather(const papiex_perthread_data_t*);

#ifdef HAVE_SCPERFUTILS
static char *suffix = ".txt";
#else
static void set_top_level_path(void);
static void make_paths(void);
static void make_proc_stats_path(void);
static void make_top_level_path(void);
#endif

static inline int get_event_index(const char * name, const char *eventnames[], int count);
static inline int get_rank(void);
static inline int get_num_tasks(void);
static void print_banner(FILE*);

static inline double sqdiff_ll(long long arg1, long long arg2)
{
  double ret, a, b;

  //printf("%d %lld %lld\n",get_rank(),arg1,arg2);
  a = (double)arg1;
  b = (double)arg2;
  
  ret = a - b;
  return (ret*ret);
}


/* This compares label and sub_label. If they are both non-empty, then they
 * must match or an error (1) is returned. If the first is a string and the 
 * second is null, then an error (1) is returned. If the first is an empty
 * string then the second is copied into the first
 */
static inline int match_caliper_label(char *label, const char* sub_label) {
  assert(label);
  assert(sub_label);

  // common case first
  if ((label[0] != '\0') && (sub_label[0] != '\0')) return (strcmp(label, sub_label)); 

  if ((sub_label[0]=='\0') && (label[0] =='\0')) return 0;
  if ((sub_label[0]=='\0') && (label[0] !='\0')) return 1;
  
  if (label[0] =='\0') strcpy(label, sub_label); return 0;

}

#ifndef HAVE_SCPERFUTILS
// This function checks the existence of path,
// if it doesn't exist, 0 is returned, else, the
// next available generation is returned
static int get_next_gen(const char *path) {
  struct stat buf;
  char tmp_path[PATH_MAX];
  strcpy(tmp_path, path);
  int rc = stat(tmp_path, &buf);
  if ((rc<0) && (errno==ENOENT)) return 0;
  int inst = 0;
  do {
    inst++;
    strcpy(tmp_path, path);
    sprintf(tmp_path, "%s.%d", path, inst);
    rc = stat(tmp_path, &buf);
  } while (!((rc<0) && (errno==ENOENT))); 
  return inst;
}
#endif

/* This is a function with overloaded arguments. Use with care! 
 * desc - String printed before the dots 
 * count - Number (can be float or long). type should be TYPE_FLOAT or TYPE_LONG, else this count is ignored (TYPE_STRING)
 * type - One of TYPE_{SRING|LONG_FLOAT}
 * make_indented - Leave blanks at the begining of the output line
 * no_newline - No newline is printed after the output line if this is set.
 * str - Printed when TYPE_STRING is used.
 */
static inline void pretty_print(FILE* output, const char* desc, double count, int type, int make_indented, int no_newline, const char* str) {
  int pad_len = PAD_LENGTH;
  char ndesc[PATH_MAX];
  strcpy(ndesc, desc);
  ndesc[PAD_LENGTH-2] = '\0'; // truncate ndesc to the requisite length. strncpy behaves weird.

  if (make_indented) {
    pad_len-=4;
    fprintf(output, "    ");
  }
  //if (strchr(desc, '%'))
  //  pad_len++;
  if (quiet) {
    if (type==TYPE_LONG)
      fprintf(output, "%-16lld %s", (long long) count, ndesc);
    else if (type==TYPE_FLOAT)
      fprintf(output, "%-16.2f %s", (float)count, ndesc);
  }
  else {
    /* not quiet */
#ifdef NO_PRETTY_PRINT
    if (type==TYPE_LONG) {
      if (no_scientific)
        fprintf(output, "%-30s: %16lld", ndesc, (long long) count);
      else
        fprintf(output, "%-30s: %16g", ndesc, (double) count);
    }
    else if (type==TYPE_FLOAT){
      fprintf(output, "%-30s: %16.2f", ndesc,  (float)count);
    }
#else
    /* The old dot dot pretty print */
    fprintf(output, "%s ", ndesc);
    int len;
    for (len=strlen(ndesc)+1; len<pad_len; len++)
      fprintf(output, ".");
    if (type==TYPE_LONG) {
      if (no_scientific)
        fprintf(output, " %16lld", (long long)count);
      else
        fprintf(output, " %16g", (double)count);
    }
    else if (type==TYPE_FLOAT){
      fprintf(output, " %16.2f", (float)count);
    }
    else if (type==TYPE_STRING){
      fprintf(output, " %16s", str);
    }
#endif 
  }
  if (!no_newline) fprintf(output, "\n");
  return;
}

/* Pretty printing for floats */
inline void pretty_printf(FILE* output, const char* desc, int make_indented, float count) {
  if (isnan(count) || (count < 0))
    count = 0.0;
  pretty_print(output, desc, (double) count, TYPE_FLOAT, make_indented, 0, NULL);
}

/* Pretty printing for longs */
inline void pretty_printl(FILE* output, const char* desc, int make_indented, long long count) {
  pretty_print(output, desc, (double) count, TYPE_LONG, make_indented, 0, NULL);
}

/* Pretty printing for strings */
inline void pretty_prints(FILE* output, const char* desc, int make_indented, const char *str) {
  pretty_print(output, desc, 0, TYPE_STRING, make_indented, 0, str);
}

static inline long long get_event_count(const char* event_name, const papiex_perthread_data_t *thread, unsigned int index) {
  long long retval = -1;
  int idx = get_event_index(event_name,(const char**) eventnames, eventcnt);
  if (idx >= 0) 
    retval = thread->data[index].counters[idx];
  return retval; 
}

// Failsafe function to get cycles. Will first try to return the hardware event count.
/// Failing that, it will give the next best value
static inline long long get_cpu_cycles(const papiex_perthread_data_t* thread, int index) {
  long long retval = -1 ;
  retval = get_event_count("PAPI_TOT_CYC", thread, index);
  if (retval < 0) {
#ifdef FULL_CALIPER_DATA
    retval = thread->data[index].virt_cyc;
#else
    /* For calipers we may not have virt_cyc available */
    if (index) 
      retval = thread->data[index].real_cyc;
    else
      retval = thread->data[index].virt_cyc;
#endif
  }
  
  return retval;
}


/* Returns either the stored value of real_usec, or it calculates
 * using real_cyc (for calipers when FULL_CALIPER_DATA is unset.
 */
static inline float get_real_usec(papiex_perthread_data_t* thread, int index) {
  float real_usec = 0;
#ifndef FULL_CALIPER_DATA
  if (index==0)
#endif
  {
    real_usec = (float)thread->data[index].real_usec;
    return real_usec;
  }
  /* caliper (index >0) && FULL_CALIPER_DATA unset */
  if (hwinfo->mhz > 0) {
    long long real_cyc = thread->data[index].real_cyc;
    real_usec = (float)real_cyc /(float) hwinfo->mhz;
  }
  return real_usec;
}

static int print_event_count(FILE *output, const char* desc, const char* event, 
                                     const papiex_perthread_data_t *thread, unsigned int index) {
  int  idx = get_event_index(event, (const char**) eventnames, eventcnt); 
  int pad_len = PAD_LENGTH;
  if (index > 0)    /*index calipers*/
    pad_len-=4;
  int len;
  if (idx >= 0) {
      if (quiet) {
        if (index > 0)                 /* indent the caliper */
          fprintf(output, "    ");
        fprintf(output, "%-16lld %s", thread->data[index].counters[idx], desc);
      }
      else {
        /* !quiet */
        pretty_print(output, desc, thread->data[index].counters[idx], 1, index, 1, NULL);
        // print % of thread for calipers
        if (index > 0)
          fprintf(output, "\t[%5.1f%%]", 100.0*(float)thread->data[index].counters[idx]/(float)thread->data[0].counters[idx]);
      }
      fprintf(output, "\n");
  }     
  else if (_papiex_debug) {
      if (index > 0)                 /* index calipers */
        fprintf(output, "    ");
      fprintf(output, "%s ", desc);
      for (len=strlen(desc)+1; len<pad_len; len++)
        fprintf(output, ".");
      fprintf(output, "       Not Available\n");
  }
  return idx;
}

static inline long long min(long long a, long long b) {
  if (a <= b) return a;
  else return b;
}

static inline long long max(long long a, long long b) {
  if (a >= b) return a;
  else return b;
}

static void print_executable_info(FILE *output) {
  char tool_version[PATH_MAX];
  sprintf(tool_version, "%s version", tool);
  fprintf(output,"%-30s: %s\n", tool_version, PAPIEX_VERSION);
  fprintf(output,"%-30s: %s\n", "Executable", fullname);
  fprintf(output,"%-30s: %s\n", "Arguments", process_args);
  fprintf(output,"%-30s: %s\n", "Processor", processor);
  fprintf(output,"%-30s: %f\n", "Clockrate", clockrate);
  fprintf(output,"%-30s: %s\n", "Hostname", hostname);
  fprintf(output,"%-30s: %s\n", "Options", getenv(PAPIEX_ENV));
  fprintf(output,"%-30s: %s\n", "Domain", stringify_domain(domain));
  fprintf(output,"%-30s: %d\n", "Parent process id", (int)getppid());
  fprintf(output,"%-30s: %d\n", "Process id", (int)getpid());
  return;
}

static void print_rusage_stats(FILE *output) {
  struct rusage ru;
  getrusage(RUSAGE_SELF,&ru);
  pretty_printl(output, "Max resident", 0, ru.ru_maxrss);
  pretty_printl(output, "Shared", 0, ru.ru_ixrss);
  pretty_printl(output, "Data", 0, ru.ru_idrss);
  pretty_printl(output, "Stack", 0, ru.ru_isrss);
  pretty_printl(output, "Min faults", 0, ru.ru_minflt);
  pretty_printl(output, "Page faults", 0, ru.ru_majflt);
  pretty_printl(output, "Swaps", 0, ru.ru_nswap);
  pretty_printl(output, "Block dev inputs", 0, ru.ru_inblock);
  pretty_printl(output, "Block dev outputs", 0, ru.ru_oublock);
  pretty_printl(output, "Msgs sent", 0, ru.ru_msgsnd);
  pretty_printl(output, "Msgs received", 0, ru.ru_msgrcv);
  pretty_printl(output, "Signals", 0, ru.ru_nsignals);
  pretty_printl(output, "Voluntary yields", 0, ru.ru_nvcsw);
  pretty_printl(output, "Preemptions", 0, ru.ru_nivcsw);
  return;
}

static void print_pretty_stats(FILE* output, papiex_perthread_data_t *thread, unsigned int index, 
                               const int scope, float process_walltime_usecs) {
    LIBPAPIEX_DEBUG("Printing pretty stats in %s\n", SCOPE_STRING[scope]);
   
    /* index 0 is special -- it has counts for the thread */
    LIBPAPIEX_DEBUG("Printing pretty stats for caliper %d of thread\n", index);
    if (index == 0) {
      if (multiplex && (thread->data[0].virt_usec <= ((1000000 * PAPIEX_MPX_MIN_SAMPLES)/mpx_interval))) {            //  so we get at least MIN_SAMPLES
        fprintf(output, "*** WARNING: Run was too short to gather performance data ***\n"
"This thread ran for only %4.2f CPU seconds. This is less than the time\n"
"(%4.2f secs) needed to get the minimum %d samples. Hence, the numbers\n"
"below are suspect. You can increase the multiplexing frequency from the\n"
"current %d by specifying -m<interval> to get more samples.\n\n",
                ((float)thread->data[0].virt_usec/1000000), ((float)PAPIEX_MPX_MIN_SAMPLES/mpx_interval), PAPIEX_MPX_MIN_SAMPLES, mpx_interval);
      }
    }
    else {
        if (thread->data[index].label && (strlen(thread->data[index].label) != 0))
	  fprintf(output,"    %s\n",thread->data[index].label);
	else
	  fprintf(output,"    Caliper %d\n",index);
    }

    if (multiplex) {
      if (index && !quiet) fprintf(output, "    ");
      if (!quiet) fprintf(output, "Multiplex Counts:\n");
    }

   /* This is using PAPI presets */
   print_event_count(output, "Cycles", "PAPI_TOT_CYC", thread, index);
   print_event_count(output, "Total Resource Stalls","PAPI_RES_STL",thread, index);
   print_event_count(output, "Memory Stall Cycles","PAPI_MEM_SCY",thread, index);
   print_event_count(output, "FP Stall Cycles","PAPI_FP_STAL",thread, index);
   print_event_count(output, "FP Operations","PAPI_FP_OPS",thread, index);
   print_event_count(output,"Instructions Completed", "PAPI_TOT_INS", thread, index);
   print_event_count(output, "FP Instructions","PAPI_FP_INS", thread, index);
   print_event_count(output, "Integer Instructions","PAPI_INT_INS",thread, index);
   print_event_count(output, "FMA Instructions","PAPI_FMA_INS",thread, index);
   print_event_count(output, "Vector Instructions","PAPI_VEC_INS",thread, index);
   print_event_count(output, "Load-Store Instructions","PAPI_LST_INS",thread, index);
   print_event_count(output, "Load Instructions","PAPI_LD_INS",thread, index);
   print_event_count(output, "Store Instructions","PAPI_SR_INS",thread, index);
   print_event_count(output, "Store Conditionals Issued","PAPI_CSR_TOT",thread, index);
   print_event_count(output, "Store Conditionals Failed","PAPI_CSR_FAL",thread, index);
   print_event_count(output, "Branch Instructions","PAPI_BR_INS",thread, index);
   print_event_count(output, "Conditional Branch Instructions","PAPI_BR_CN",thread, index);
   print_event_count(output, "Mispredicted Branches","PAPI_BR_MSP",thread, index);
   print_event_count(output, "L1 Data Cache Accesses","PAPI_L1_DCA",thread, index);
   print_event_count(output, "L1 Data Cache Misses","PAPI_L1_DCM",thread, index);
   print_event_count(output, "L1 Instruction Cache Misses","PAPI_L1_ICM",thread, index);
   print_event_count(output, "L2 Data Cache Accesses","PAPI_L2_DCA",thread, index);
   print_event_count(output, "L2 Data Cache Misses","PAPI_L2_DCM",thread, index);
   print_event_count(output, "L2 Instruction Cache Accesses","PAPI_L2_ICA",thread, index);
   print_event_count(output, "L2 Instruction Cache Misses","PAPI_L2_ICM",thread, index);
   print_event_count(output, "Cache Line Invalidation Requests","PAPI_CA_INV",thread, index);
   print_event_count(output, "Data TLB Misses","PAPI_TLB_DM",thread, index);
   print_event_count(output, "Instruction TLB Misses","PAPI_TLB_IM",thread, index);
   print_event_count(output,"FPU Idle Cycles","PAPI_FPU_IDL",thread, index);
   print_event_count(output,"LSU Idle Cycles","PAPI_LSU_IDL",thread, index);
   print_event_count(output,"Cycles with No Issue","PAPI_STL_ICY",thread, index);
   print_event_count(output,"Cycles with Full Issue","PAPI_FUL_ICY",thread, index);

   fprintf(output, "\n");
   return;
}

/* Print pretty stats for the thread and all the caliper points */
static inline void print_all_pretty_stats(FILE* output, papiex_perthread_data_t *thread, 
                                          const int scope, float process_walltime_usecs) {
  int j;
  print_pretty_stats(output, thread, 0, scope, process_walltime_usecs);
  for (j=1;j<thread->max_caliper_entries;j++)
    if (thread->data[j].used >0)
      print_pretty_stats(output, thread, j, scope, process_walltime_usecs);
}


static void print_counters(FILE *output, papiex_perthread_data_t *thread) {
  int i,j;
  if (quiet) {
    fprintf(output,"%-16lld Real usecs\n",thread->data[0].real_usec);
    fprintf(output,"%-16lld Real cycles\n",thread->data[0].real_cyc);
    fprintf(output,"%-16lld Virtual usecs\n",thread->data[0].virt_usec);
    fprintf(output,"%-16lld Virtual cycles\n",thread->data[0].virt_cyc);
    for (i=0;i<eventcnt;i++)
      fprintf(output,"%-16lld %s\n",thread->data[0].counters[i],eventnames[i]);
      for (j=1;j<thread->max_caliper_entries;j++) {
        if (thread->data[j].used >0) {
          fprintf(output,"%-16lld %s\n",(long long)j,thread->data[j].label);
          fprintf(output,"%-16lld Measurements\n",(long long)thread->data[j].used);
          fprintf(output,"%-16lld Real cycles\n",thread->data[j].real_cyc);
#ifdef FULL_CALIPER_DATA
          fprintf(output,"%-16lld Real usecs\n",thread->data[j].real_usec);
          fprintf(output,"%-16lld Virtual usecs\n",thread->data[j].virt_usec);
          fprintf(output,"%-16lld Virtual cycles\n",thread->data[j].virt_cyc);
#endif
          for (i=0;i<eventcnt;i++)
            fprintf(output,"%-16lld %s\n",thread->data[j].counters[i],eventnames[i]);
	}
      }
  } /* quiet */

  else {
    /* not quiet */
    pretty_printl(output,"Real usecs",0,thread->data[0].real_usec);
    pretty_printl(output,"Real cycles",0,thread->data[0].real_cyc);
    pretty_printl(output,"Virtual usecs",0,thread->data[0].virt_usec);
    pretty_printl(output,"Virtual cycles",0,thread->data[0].virt_cyc);
    for (i=0;i<eventcnt;i++) {
      pretty_printl(output, eventnames[i], 0, thread->data[0].counters[i]);
    }

    for (j=1;j<thread->max_caliper_entries;j++) {
      if (thread->data[j].used >0) {
        fprintf(output, "\n");
        if (strlen(thread->data[j].label) == 0)
	  fprintf(output,"    Caliper %d\n",j);
	else
	  fprintf(output,"    %s\n",thread->data[j].label);
        pretty_printl(output,"Executions",1, (long long)thread->data[j].used);
        pretty_printl(output,"Real cycles",1, thread->data[j].real_cyc);
#ifdef FULL_CALIPER_DATA
        pretty_printl(output,"Real usecs",1, thread->data[j].real_usec);
        pretty_printl(output,"Virtual usecs",1, thread->data[j].virt_usec);
        pretty_printl(output,"Virtual cycles",1, thread->data[j].virt_cyc);
#endif
        for (i=0;i<eventcnt;i++) {
          pretty_print(output, eventnames[i],thread->data[j].counters[i], 1, 1, 1, NULL);
	  /* Derived check here */
          fprintf(output, " [%5.1f%%]\n", 100*(float)thread->data[j].counters[i]/(float)thread->data[0].counters[i]);
        }
      }
    }

    fprintf(output,"\n");
    fprintf(output,"Event descriptions:\n");
    for (i=0;i<eventcnt;i++)
      _papiex_dump_event_info(output,eventcodes[i],0);

  } /* not quiet */
  return;
}


static void print_min_max_mean_cv(FILE *output, 
                                         papiex_perthread_data_t *min_data,
                                         papiex_perthread_data_t *max_data,
                                         papiex_perthread_data_t *mean_data,
                                         papiex_perthread_data_t *stddev_data) {
  int j,k;
  fprintf(output, "\n");
  if (quiet) {
    fprintf(output,"%16lld\t%16lld\t%16lld\t%16.2f\tReal cycles\n",
         min_data->data[0].real_cyc, max_data->data[0].real_cyc, 
         mean_data->data[0].real_cyc, (float)stddev_data->data[0].real_cyc/mean_data->data[0].real_cyc);
    fprintf(output,"%16lld\t%16lld\t%16lld\t%16.2f\tReal usecs\n",
       min_data->data[0].real_usec, max_data->data[0].real_usec, 
       mean_data->data[0].real_usec, (float)stddev_data->data[0].real_usec/mean_data->data[0].real_usec);
    fprintf(output,"%16lld\t%16lld\t%16lld\t%16.2f\tVirtual cycles\n",
       min_data->data[0].virt_cyc, max_data->data[0].virt_cyc, 
       mean_data->data[0].virt_cyc, (float)stddev_data->data[0].virt_cyc/mean_data->data[0].virt_cyc);
    fprintf(output,"%16lld\t%16lld\t%16lld\t%16.2f\tVirtual usecs\n",
       min_data->data[0].virt_usec, max_data->data[0].virt_usec, 
       mean_data->data[0].virt_usec, (float)stddev_data->data[0].virt_usec/mean_data->data[0].virt_usec);
    for (k=0;k<eventcnt;k++)
      fprintf(output,"%16lld\t%16lld\t%16lld\t%16.2f\t%s\n", 
         min_data->data[0].counters[k], max_data->data[0].counters[k],
         mean_data->data[0].counters[k], (float)stddev_data->data[0].counters[k]/mean_data->data[0].counters[k],
         eventnames[k]);
    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (mean_data->data[j].used >0) {
        fprintf(output, "\nCaliper %d, %s:\n", j, (mean_data->data[j].label == NULL ? "" : mean_data->data[j].label));
        //fprintf(output,"%16lld\t%16lld\t%16lld\t%16lld\tMeasurements\n",
        //   (long long) min_data->data[j].used, (long long) max_data->data[j].used, 
        //   (long long)mean_data->data[j].used, (long long) stddev_data->data[j].used);
#ifdef FULL_CALIPER_DATA
        fprintf(output,"%16lld\t%16lld\t%16lld\t%16.2f\tReal usecs\n",
           (long long) min_data->data[j].real_usec, (long long) max_data->data[j].real_usec, 
           (long long)mean_data->data[j].real_usec, (float) stddev_data->data[j].real_usec/mean_data->data[j].real_usec);
        fprintf(output,"%16lld\t%16lld\t%16lld\t%16.2f\tVirtual usecs\n",
           (long long) min_data->data[j].virt_usec, (long long) max_data->data[j].virt_usec, 
           (long long)mean_data->data[j].virt_usec, (float) stddev_data->data[j].virt_usec/mean_data->data[j].virt_usec);
        fprintf(output,"%16lld\t%16lld\t%16lld\t%16.2f\tVirtual cycles\n",
           (long long) min_data->data[j].virt_cyc, (long long) max_data->data[j].virt_cyc, 
           (long long)mean_data->data[j].virt_cyc, (float) stddev_data->data[j].virt_cyc/mean_data->data[j].virt_cyc);
#endif
        for (k=0;k<eventcnt;k++)
          fprintf(output,"%s:\t%16lld\t%16lld\t%16lld\t%16.2f\n", eventnames[k], 
             min_data->data[0].counters[k], max_data->data[0].counters[k],
             mean_data->data[0].counters[k], (float)stddev_data->data[0].counters[k]/mean_data->data[0].counters[k]);
      }
    } /* calipers */
  }
  else {
      fprintf(output,"\nProgram Statistics:\n");
      fprintf(output,"%-30s: %12s\t%12s\t%12s\t%8s\n","Event","Min","Max","Mean","COV");
    /* print the global stats */
      fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n","Real usecs",
       (double)min_data->data[0].real_usec, (double)max_data->data[0].real_usec, 
       (double)mean_data->data[0].real_usec, (float)stddev_data->data[0].real_usec/mean_data->data[0].real_usec);

      fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n","Real cycles",
         (double)min_data->data[0].real_cyc, (double)max_data->data[0].real_cyc, 
         (double)mean_data->data[0].real_cyc, (float)stddev_data->data[0].real_cyc/mean_data->data[0].real_cyc);
      fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n","Virtual usecs",
       (double)min_data->data[0].virt_usec, (double)max_data->data[0].virt_usec, 
       (double)mean_data->data[0].virt_usec, (float)stddev_data->data[0].virt_usec/mean_data->data[0].virt_usec);
      fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n","Virtual cycles",
       (double)min_data->data[0].virt_cyc, (double)max_data->data[0].virt_cyc, 
       (double)mean_data->data[0].virt_cyc, (float)stddev_data->data[0].virt_cyc/mean_data->data[0].virt_cyc);
    for (k=0;k<eventcnt;k++)
      fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n", eventnames[k], 
         (double)min_data->data[0].counters[k], (double)max_data->data[0].counters[k],
         (double)mean_data->data[0].counters[k], (mean_data->data[0].counters[k] > 0 ? (float)stddev_data->data[0].counters[k]/mean_data->data[0].counters[k] :0));
    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (mean_data->data[j].used >0) {
        fprintf(output, "\nCaliper %d: %s\n", j, (mean_data->data[j].label == NULL ? "" : mean_data->data[j].label));
        //fprintf(output,"%16lld\t%16lld\t%16lld\t%16lld\tMeasurements\n",
        //   (long long) min_data->data[j].used, (long long) max_data->data[j].used, 
        //   (long long)mean_data->data[j].used, (long long) stddev_data->data[j].used);
#ifdef FULL_CALIPER_DATA
        fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n","Real usecs",
           (double)min_data->data[j].real_usec, (double)max_data->data[j].real_usec, 
           (double)mean_data->data[j].real_usec, (mean_data->data[j].real_usec > 0 ? (float)stddev_data->data[j].real_usec/mean_data->data[j].real_usec :0));
        fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n","Virtual usec:"
           (double)min_data->data[j].virt_usec, (double)max_data->data[j].virt_usec, 
           (double)mean_data->data[j].virt_usec, (mean_data->data[j].virt_usec > 0 ? (float)stddev_data->data[j].virt_usec/mean_data->data[j].virt_usec : 0));
        fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n","Virtual cycles",
           (double)min_data->data[j].virt_cyc, (double)max_data->data[j].virt_cyc, 
           (double)mean_data->data[j].virt_cyc, (mean_data->data[j].virt_cyc > 0 ? (float)stddev_data->data[j].virt_cyc/mean_data->data[j].virt_cyc : 0));
#endif
        for (k=0;k<eventcnt;k++)
          fprintf(output,"%-30s: %12g\t%12g\t%12g\t%8.2f\n", eventnames[k], 
             (double)min_data->data[0].counters[k], (double)max_data->data[0].counters[k],
             (double)mean_data->data[0].counters[k], (mean_data->data[0].counters[k] > 0 ? (float)stddev_data->data[0].counters[k]/mean_data->data[0].counters[k] : 0));
      }
    } /* calipers */

    fprintf(output,"\n");
    //fprintf(output,"\nEvent descriptions:\n");
    //for (k=0;k<eventcnt;k++)
    //  _papiex_dump_event_info(output,eventcodes[k],0);

  } /* not quiet */

  return;
}

/* Returns the index of 'name' in eventnames array.
 * Returns -1 if the name is not found
 */
static inline int get_event_index(const char * name, const char *eventnames[], int count) {
  int i;
  assert(name);
  for (i=0; i<count; i++) {
    if (strcmp(eventnames[i], name)==0)
      return i;
  }

  // if we reach here, the event doesn't exist in the array
  return -1;
}

static char *stringify_domain(int domain)
{
  static char output[256];
  output[0] = '\0';
  if (domain & PAPI_DOM_USER)
    strcat(output,"User");
  if (domain & PAPI_DOM_KERNEL)
    {
      if (strlen(output))
	strcat(output,",");
      strcat(output,"Kernel");
    }
  if (domain & PAPI_DOM_OTHER)
    {
      if (strlen(output))
	strcat(output,",");
      strcat(output,"Other");
    }
  if (domain & PAPI_DOM_SUPERVISOR)
    {
      if (strlen(output))
	strcat(output,",");
      strcat(output,"Supervisor");
    }
  return(output);
}

char *stringify_all_domains(int domains)
{
  static char buf[PAPI_HUGE_STR_LEN];
  int i, did = 0;
  buf[0] = '\0';

  for (i=PAPI_DOM_MIN;i<=PAPI_DOM_MAX;i=i<<1)
    if (domains&i)
      {
        if (did)
          strcpy(buf+strlen(buf),"|");
        strcpy(buf+strlen(buf),stringify_domain(domains&i));
        did++;
      }
  return(buf);
}

char *stringify_granularity(int granularity)
{
   switch (granularity) {
   case PAPI_GRN_THR:
      return ("PAPI_GRN_THR");
   case PAPI_GRN_PROC:
      return ("PAPI_GRN_PROC");
   case PAPI_GRN_PROCG:
      return ("PAPI_GRN_PROCG");
   case PAPI_GRN_SYS_CPU:
      return ("PAPI_GRN_SYS_CPU");
   case PAPI_GRN_SYS:
      return ("PAPI_GRN_SYS");
   default:
     return("Unrecognized granularity!");
   }
   return (NULL);
}

char *stringify_all_granularities(int granularities)
{
  static char buf[PAPI_HUGE_STR_LEN];
  int i, did = 0;

  buf[0] = '\0';
  for (i=PAPI_GRN_MIN;i<=PAPI_GRN_MAX;i=i<<1)
    if (granularities&i)
      {
        if (did)
          strcpy(buf+strlen(buf),"|");
        strcpy(buf+strlen(buf),stringify_granularity(granularities&i));
        did++;
      }

  return(buf);
}


void papiex_stop(int point)
{
  papiex_perthread_data_t *thread = NULL;
  void *tmp = NULL;
  int i, retval;
  long long counters[PAPIEX_MAX_COUNTERS], real_cyc;

  /* This is here for people that have instrumented with -lpapiex but not run under papiex -PJM */
  if (eventcnt <= 0)
    return; 

  LIBPAPIEX_DEBUG("STOP POINT %d\n",point);

  retval = PAPI_get_thr_specific(PAPI_TLS_USER_LEVEL1, &tmp);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_get_thr_specific",retval);
      return;
    }
  thread = (papiex_perthread_data_t *)tmp;
  if (thread == NULL)
    {
      LIBPAPIEX_ERROR("PAPI_get_thr_specific() returned NULL.");
      return;
    }
  /* PAPI_read_ts is broken for multiplexing */
  if (!papi_broken_read_ts) {
    retval = PAPI_read_ts(thread->eventset,counters,&real_cyc);
  }
  else {
    real_cyc = PAPI_get_real_cyc();
    retval = PAPI_read(thread->eventset,counters);
  }
  if (retval != PAPI_OK)
    LIBPAPIEX_PAPI_WARN("PAPI_read_ts",retval);

  if (point < 0) 
    {
      LIBPAPIEX_ERROR("Caliper point %d is out of range",point);
      return;
    }
  if (point >= thread->max_caliper_entries)
    {
      LIBPAPIEX_DEBUG("Caliper point %d is out of range, max %d",point,thread->max_caliper_entries);
      return;
    }
  if (thread->data[point].depth == 0)
    {
      LIBPAPIEX_ERROR("Caliper point %d is not in use",point);
      return;
    }
  thread->data[point].depth--;
  thread->data[point].used++;
  thread->data[point].real_cyc += real_cyc - thread->data[point].tmp_real_cyc;

  if (retval == PAPI_OK)
    {
      for (i=0;i<eventcnt;i++)
	thread->data[point].counters[i] += counters[i] - thread->data[point].tmp_counters[i];
#ifndef FULL_CALIPER_DATA
      if (point == 0)
#endif
	{
	  thread->data[point].real_usec += PAPI_get_real_usec() - thread->data[point].tmp_real_usec;
	  thread->data[point].virt_usec += PAPI_get_virt_usec() - thread->data[point].tmp_virt_usec;
	  thread->data[point].virt_cyc += PAPI_get_virt_cyc() - thread->data[point].tmp_virt_cyc;
	}
    }
  LIBPAPIEX_DEBUG("STOP POINT %d USED %d DEPTH %d\n",point,(int)thread->data[point].used,thread->data[point].depth);
}

void papiex_stop__(int *point)
{
  papiex_stop(*point);
}
void papiex_stop_(int *point)
{
  papiex_stop(*point);
}
void PAPIEX_STOP__(int *point)
{
  papiex_stop(*point);
}
void PAPIEX_STOP_(int *point)
{
  papiex_stop(*point);
}

void papiex_accum(int point)
{
  papiex_perthread_data_t *thread = NULL;
  void *tmp;
  int i, retval;
  long long counters[PAPIEX_MAX_COUNTERS], real_cyc;

  /* This is here for people that have instrumented with -lpapiex but not run under papiex -PJM */
  if (eventcnt <= 0)
    return; 

  LIBPAPIEX_DEBUG("ACCUM POINT %d\n",point);
  
  retval = PAPI_get_thr_specific(PAPI_TLS_USER_LEVEL1, &tmp);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_get_thr_specific",retval);
      return;
    }
  thread = (papiex_perthread_data_t *)tmp;
  if (thread == NULL)
    {
      LIBPAPIEX_ERROR("PAPI_get_thr_specific() returned NULL.");
      return;
    }
  
  /* PAPI_read_ts is broken for multiplexing */
  if (!papi_broken_read_ts) {
    retval = PAPI_read_ts(thread->eventset,counters,&real_cyc);
  }
  else {
    real_cyc = PAPI_get_real_cyc();
    retval = PAPI_read(thread->eventset,counters);
  }
  if (retval != PAPI_OK)
    LIBPAPIEX_PAPI_WARN("PAPI_read_ts",retval);
    
  /* Accum can't be called on point 0 */
  if (point <= 0) 
    {
      LIBPAPIEX_ERROR("Caliper point %d is out of range",point);
      return;
    }
  if (point >= thread->max_caliper_entries)
    {
      LIBPAPIEX_DEBUG("Caliper point %d is out of range, max %d",point,thread->max_caliper_entries);
      return;
    }
  if (thread->data[point].depth == 0)
    {
      LIBPAPIEX_ERROR("Caliper point %d is not in use",point);
      return;
    }
  thread->data[point].used++;

  thread->data[point].real_cyc += real_cyc - thread->data[point].tmp_real_cyc;
  thread->data[point].tmp_real_cyc = real_cyc;

  if (retval == PAPI_OK)
    {
      for (i=0;i<eventcnt;i++)
	{
	  thread->data[point].counters[i] += counters[i] - thread->data[point].tmp_counters[i];
	  thread->data[point].tmp_counters[i] = counters[i];
	}
#ifndef FULL_CALIPER_DATA
      if (point == 0)
#endif
	{
	  long long tmp = PAPI_get_real_usec();
	  thread->data[point].real_usec += tmp - thread->data[point].tmp_real_usec;
	  thread->data[point].tmp_real_usec = tmp;
	  tmp = PAPI_get_virt_usec();
	  thread->data[point].virt_usec += tmp - thread->data[point].tmp_virt_usec;
	  thread->data[point].tmp_virt_usec = tmp;
	  tmp = PAPI_get_virt_cyc();
	  thread->data[point].virt_cyc += tmp - thread->data[point].tmp_virt_cyc;
	  thread->data[point].tmp_virt_cyc = tmp;
	}
    }
  LIBPAPIEX_DEBUG("POINT %d USED %d DEPTH %d\n",point,(int) thread->data[point].used,thread->data[point].depth);
}

void papiex_accum__(int *point)
{
  papiex_accum(*point);
}
void papiex_accum_(int *point)
{
  papiex_accum(*point);
}
void PAPIEX_ACCUM__(int *point)
{
  papiex_accum(*point);
}
void PAPIEX_ACCUM_(int *point)
{
  papiex_accum(*point);
}

static void gather_papi_thread_data_ptrs(PAPI_all_thr_spec_t *all_threads)
{

//  all_threads->num = 4096; // Fix for PAPI 3.5.0 and earlier
//  all_threads->id = (unsigned long *)malloc(all_threads->num * sizeof(unsigned long));
//  all_threads->data = (void **)malloc(all_threads->num * sizeof(void *));

}

static void papiex_thread_shutdown_routine(void)
{
  void *tmp;
  papiex_perthread_data_t *thread = NULL;
  int retval;

  LIBPAPIEX_DEBUG("THREAD SHUTDOWN START\n");

  papiex_stop(0);

  retval = PAPI_get_thr_specific(PAPI_TLS_USER_LEVEL1, &tmp);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_get_thr_specific",retval);
      return;
    }
  thread = (papiex_perthread_data_t *)tmp;
  if (thread == NULL)
    {
      LIBPAPIEX_ERROR("PAPI_get_thr_specific() returned NULL.");
      return;
    }
  if (papi_broken_stop && multiplex && _papiex_threaded && (PAPI_thread_id() == 0)) {
    /* skip calling PAPI_stop, as it dies */
  }
  else {
    PAPI_stop(thread->eventset,NULL);
  }
  time(&thread->finish);

  retval = PAPI_cleanup_eventset(thread->eventset);
  if (retval != PAPI_OK) 
    LIBPAPIEX_PAPI_WARN("PAPI_cleanup_eventset",retval);

  retval = PAPI_destroy_eventset(&thread->eventset);
  if (retval != PAPI_OK) 
    LIBPAPIEX_PAPI_WARN("PAPI_destroy_eventset",retval);

  retval = PAPI_unregister_thread();
  if (retval != PAPI_OK) 
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_unregister_thread",retval);
       return;
    }

  LIBPAPIEX_DEBUG("THREAD SHUTDOWN END\n");
  return;
}

#if defined(__GNUC__) && !defined(HAVE_MONITOR)
static void papiex_process_shutdown_routine() __attribute__((destructor));
#elif defined(sun) 
#pragma fini (papiex_process_shutdown_routine)
#endif

/* Only the main process thread comes to this routine, no other thread */
/* This routine gets called either from MPI_Finalize or from the process
   cleanup handler */

static void papiex_process_shutdown_routine(void)
{
  papiex_perthread_data_t process_data_sums;

  LIBPAPIEX_DEBUG("PROCESS SHUTDOWN START\n");

  /* Set process finish time if it's not set */
  if (proc_fini_time.tv_sec == 0)
    gettimeofday(&proc_fini_time, NULL);

  /* Only invoke the thread shutdown routine for non-MPI programs 
     unless we have disabled using MPI to gather data to rank 0. */

  papiex_thread_shutdown_routine();
      
  gather_papi_thread_data_ptrs(&all_threads);
  LIBPAPIEX_DEBUG("Num. of threads = %d\n", all_threads.num);
    
  /* Create the needed directories */ 
#ifdef HAVE_SCPERFUTILS
  scperfutils_set_base_output_path(process_name, tool, user_path_prefix, output_file_name, suffix, is_mpied, 0);
  if (!no_write) scperfutils_make_output_paths(_papiex_threaded);
#else
  make_paths();
#endif

  // printf("START print_process_stats rank %d\n",get_rank()); fflush(stdout);
  print_process_stats(&all_threads, &process_data_sums, stderr);
  // printf("FINISHED print_process_stats rank %d\n",get_rank()); fflush(stdout);

  if (is_mpied && !no_mpi_gather) {
    // printf("START papiex_mpi_gather rank %d\n",get_rank()); fflush(stdout);
    papiex_perthread_data_t *task_data =  papiex_mpi_gather(&process_data_sums);
    // printf("FINISHED papiex_mpi_gather rank %d\n",get_rank()); fflush(stdout);
    if (get_rank()==0) /* master */
      {
	// printf("START print_global_stats rank %d\n",get_rank()); fflush(stdout);
	print_global_stats(task_data, stderr);
	// printf("FINISHED print_global_stats rank %d\n",get_rank()); fflush(stdout);
      }
  }

  LIBPAPIEX_DEBUG("PROCESS SHUTDOWN END\n");
}

void papiex_start(int point, char *label)
{
  int retval;
  papiex_perthread_data_t *thread = NULL;
  void *tmp;

  /* This is here for people that have instrumented with -lpapiex but not run under papiex -PJM */
  if (eventcnt <= 0)
    return; 

  LIBPAPIEX_DEBUG("START POINT %d LABEL %s\n",point,label);

  retval = PAPI_get_thr_specific(PAPI_TLS_USER_LEVEL1, &tmp);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_get_thr_specific",retval);
      return;
    }
  thread = (papiex_perthread_data_t *)tmp;
  if (thread == NULL)
    {
      LIBPAPIEX_ERROR("PAPI_get_thr_specific() returned NULL.");
      return;
    }
  if (point < 0)
    {
      LIBPAPIEX_ERROR("Caliper point %d is out of range",point);
      return;
    }
  if (point >= thread->max_caliper_entries)
    {
      LIBPAPIEX_DEBUG("Caliper point %d is out of range, max %d",point,thread->max_caliper_entries);
      return;
    }
  if (thread->data[point].depth)
    {
      LIBPAPIEX_ERROR("Caliper point %d is already in use",point);
      return;
    }
  thread->data[point].depth++;

  if (label && point && (thread->data[point].label[0] == '\0'))
    {
      if (strlen(label) > MAX_LABEL_SIZE-1) {
	PAPIEX_WARN("Max. label size is %d bytes, ignoring some characters of %s\n", MAX_LABEL_SIZE-1, label);
      }
      strncpy(thread->data[point].label, label, MAX_LABEL_SIZE-1);
      thread->data[point].label[MAX_LABEL_SIZE-1] = '\0';
    }

  /* Update the max caliper point used */
  if (thread->max_caliper_used <= point)
    thread->max_caliper_used = point;

#ifndef FULL_CALIPER_DATA
  if (point == 0)
#endif
    {
      thread->data[point].tmp_virt_usec = PAPI_get_virt_usec();
      thread->data[point].tmp_real_usec = PAPI_get_real_usec();
      thread->data[point].tmp_virt_cyc = PAPI_get_virt_cyc();
    }
#ifdef PAPI_READ_TS_OK
  retval = PAPI_read_ts(thread->eventset,thread->data[point].tmp_counters,&thread->data[point].tmp_real_cyc);
#else
  thread->data[point].tmp_real_cyc = PAPI_get_real_cyc();
  retval = PAPI_read(thread->eventset,thread->data[point].tmp_counters);
#endif
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_read_ts",retval);
      return;
    }

  LIBPAPIEX_DEBUG("START POINT %d USED %d DEPTH %d\n",point,(int) thread->data[point].used,thread->data[point].depth);
}

void papiex_start__(int *point, char *label)
{
  papiex_start(*point, label);
}
void papiex_start_(int *point, char *label)
{
  papiex_start(*point, label);
}
void PAPIEX_START__(int *point, char *label)
{
  papiex_start(*point, label);
}
void PAPIEX_START_(int *point, char *label)
{
  papiex_start(*point, label);
}

static void papiex_thread_init_routine(void)
{
  int retval, eventset;
  papiex_perthread_data_t *thread;

  LIBPAPIEX_DEBUG("INIT START malloc(%lu)\n",sizeof(papiex_perthread_data_t));
  thread = malloc(sizeof(papiex_perthread_data_t));
  if (thread == NULL)
    {
      LIBPAPIEX_ERROR("malloc(%d) for per thread data failed.",(int)sizeof(papiex_perthread_data_t));
      monitor_real_exit(1);
    }
  /* The thread data structure is initialized to zero */
  memset(thread,0x0,sizeof(papiex_perthread_data_t));
  thread->max_caliper_entries = PAPIEX_MAX_CALIPERS;

  eventset = PAPI_NULL;
  retval = PAPI_create_eventset(&eventset);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_create_eventset",retval);
      return;
    }

  if (multiplex)
    {
#if (PAPI_VERSION_MAJOR(PAPI_VERSION) > 3)
       retval = PAPI_assign_eventset_component(eventset, 0);
       if (retval != PAPI_OK) {
         LIBPAPIEX_PAPI_ERROR("PAPI_assign_eventset_component",retval);
         return;
       }
#endif
  	 
       if (mpx_interval > 0) {
         LIBPAPIEX_DEBUG("PAPI multiplex interval set to: %d Hz\n", mpx_interval);
         PAPI_multiplex_option_t popt;
         memset(&popt,0,sizeof(popt));
 #ifdef PAPI_DEF_ITIMER_NS
         popt.ns = (int)(1000000000 / mpx_interval);
         retval = PAPI_set_opt(PAPI_DEF_ITIMER_NS, (PAPI_option_t *)&popt);
 #else
         popt.us = (int)(1000000 / mpx_interval);
         retval = PAPI_set_opt(PAPI_DEF_MPX_USEC, (PAPI_option_t *)&popt);
 #endif
         if (retval != PAPI_OK) {
           LIBPAPIEX_PAPI_ERROR("PAPI_multiplex_init",retval);
           return;
         }
       } // mpx_interval

      retval = PAPI_set_multiplex(eventset);
      if (retval != PAPI_OK)
      	{
	  LIBPAPIEX_PAPI_ERROR("PAPI_set_multiplex",retval);
	  return;
	}
    }

  if (eventcnt)
    {
      retval = PAPI_add_events(eventset, eventcodes, eventcnt);
      if (retval < PAPI_OK)
	{
	  LIBPAPIEX_PAPI_ERROR("PAPI_add_events",retval);
	  return;
	}
    }
  else
    {
      eventcodes[0] = PAPI_TOT_CYC;
      eventnames[0] = strdup("PAPI_TOT_CYC");
      if (PAPI_query_event(PAPI_FP_OPS) == PAPI_OK)
	{
	  eventcodes[1] = PAPI_FP_OPS;
	  eventnames[1] = strdup("PAPI_FP_OPS");
	}
      else if (PAPI_query_event(PAPI_FP_INS) == PAPI_OK)
	{
	  eventcodes[1] = PAPI_FP_INS;
	  eventnames[1] = strdup("PAPI_FP_INS");
	}
      else
	{
	  eventcodes[1] = PAPI_TOT_INS;
	  eventnames[1] = strdup("PAPI_TOT_INS");
	}
      eventcnt = 2;
      retval = PAPI_add_events(eventset, eventcodes, eventcnt);
      if (retval < PAPI_OK)
	{
	  LIBPAPIEX_PAPI_ERROR("PAPI_add_events",retval);
	  return;
	}
    }
  
  thread->eventset = eventset;
  retval = PAPI_set_thr_specific(PAPI_TLS_USER_LEVEL1,thread);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_set_thr_specific",retval);
      return;
    }

  retval = PAPI_start(thread->eventset);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_start",retval);
      return;
    }

  time(&thread->stamp);

  PAPI_lock(PAPI_LOCK_USR2);
  if (all_threads.num+1 >= all_threads_size)
    {
      all_threads.id = (PAPI_thread_id_t *)realloc(all_threads.id,(all_threads_size*2*sizeof(PAPI_thread_id_t)));
      all_threads.data = (void **)realloc(all_threads.data,(all_threads_size*2*sizeof(void *)));
      if ((all_threads.data == NULL) || (all_threads.id == NULL)) {
	LIBPAPIEX_ERROR("malloc() for %d thread entries failed.",all_threads_size*2);
	monitor_real_exit(1);
      }
      all_threads_size = all_threads_size*2;
    }
      
  all_threads.id[all_threads.num] = monitor_gettid();
  all_threads.data[all_threads.num] = thread;
  all_threads.num++;

  PAPI_unlock(PAPI_LOCK_USR2);

  papiex_start(0,"");

  LIBPAPIEX_DEBUG("INIT END\n");
}

#if defined(__GNUC__) && !defined(HAVE_MONITOR)
static void papiex_process_init_routine() __attribute__((constructor));
#elif defined(sun)
#pragma init (papiex_process_init_routine)
#endif

static void papiex_process_init_routine(void)
{
  int retval, i;
  int version;

  /* after a fork, this better be initialized to 0 in the child
   * Otherwise, we will end up intercepting I/O calls and calling PAPI_get_real_usec before
   * PAPI_library_init
   */
  if (getenv("PAPIEX_BROKEN_PAPI_READ_TS")) {
    papi_broken_read_ts = 1;
    LIBPAPIEX_DEBUG("Broken PAPI_read_ts for multiplexing. Will work around it.\n");
  }
  if (getenv("PAPIEX_BROKEN_PAPI_STOP")) {
    papi_broken_stop = 1;
    LIBPAPIEX_DEBUG("Broken PAPI_stop for multiplex+pthreads. Will work around it.\n");
  }

  version = PAPI_library_init(PAPI_VER_CURRENT);
  if (version != PAPI_VER_CURRENT)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_library_init",version);
      return;
    }

  if ((hwinfo = PAPI_get_hardware_info()) == NULL) 
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_get_hardware_info returned NULL",0);
      return;
    }
  processor = hwinfo->model_string;
  clockrate = hwinfo->mhz;

  if ((exeinfo = PAPI_get_executable_info()) == NULL)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_get_executable_info",0);
      return;
    }
  process_name = (char *)exeinfo->address_info.name;
  fullname = exeinfo->fullname;

  for (i = 0;i<eventcnt;i++)
    {
      int eventcode = 0;
      LIBPAPIEX_DEBUG("Checking event %s for existance.\n",eventnames[i]);
      retval = PAPI_event_name_to_code(eventnames[i],&eventcode);
      if (retval != PAPI_OK)
	{
	  LIBPAPIEX_PAPI_ERROR("PAPI_event_name_to_code",retval);
	  return;
	}
      eventcodes[i] = eventcode;
    }

  if (multiplex)
    {
      retval = PAPI_multiplex_init();
      if (retval != PAPI_OK)
	{
	  LIBPAPIEX_PAPI_ERROR("PAPI_multiplex_init",retval);
	  return;
	}
    }

  if (domain == 0)
    domain = PAPI_DOM_USER;

  retval = PAPI_set_domain(domain);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_set_domain",retval);
      return;
    }

  all_threads.id = (PAPI_thread_id_t *)realloc(all_threads.id,(all_threads_size*sizeof(PAPI_thread_id_t)));
  all_threads.data = (void **)realloc(all_threads.data,(all_threads_size*sizeof(void *)));
  if ((all_threads.data == NULL) || (all_threads.id == NULL)) {
    LIBPAPIEX_ERROR("malloc() for %d thread entries failed.",all_threads_size);
    monitor_real_exit(1);
  }

  if ((!uses_mpi) && (!quiet)) print_banner(stderr);

  gettimeofday(&proc_init_time, NULL);

  papiex_thread_init_routine();
  LIBPAPIEX_DEBUG("INIT END\n");
}

static inline int get_rank(void) {
  static int myrank = 0;
#ifdef HAVE_MPI
  if (is_mpied && (myrank==0)) {
#ifdef USE_MONITOR_MPI
    myrank = monitor_mpi_comm_rank();
#else
    if (!fptr_MPI_Comm_rank)
      LIBPAPIEX_ERROR("Cannot get a handle to MPI_Comm_rank");
    fptr_MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
#endif
  }
  
#endif /* HAVE_MPI */
  return myrank;
}

static inline int get_num_tasks(void) {
  static int num_tasks = 1;
#ifdef HAVE_MPI
  if (is_mpied && (num_tasks==1)) {
#ifdef USE_MONITOR_MPI
    num_tasks = monitor_mpi_comm_size();
#else
    if (!fptr_MPI_Comm_size)
      LIBPAPIEX_ERROR("Cannot get a handle to MPI_Comm_size");
    fptr_MPI_Comm_size(MPI_COMM_WORLD, &num_tasks);
#endif
  }
#endif /* HAVE_MPI */
  return num_tasks;
}

static papiex_perthread_data_t* papiex_mpi_gather(const papiex_perthread_data_t* process_sums)
{
  LIBPAPIEX_DEBUG("Starting papiex_mpi_gather\n");
  papiex_perthread_data_t* _task_data = NULL;
#ifdef HAVE_MPI
  int myrank = get_rank();
  int ntasks = get_num_tasks();

// #ifdef MPICH
//   papiex_debug("This is an MPICH program\n");
  LIBPAPIEX_DEBUG("Task %d in papiex_mpi_gather now\n", myrank);
// #else
//   papiex_debug("This is NOT an MPICH program\n");
//   MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
//   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
// #endif

  if (myrank == 0) {
    /* master */
    /* Let's get the numbers from all the slaves */

    /* First we allocate space for the array of task data structures */
    _task_data = (papiex_perthread_data_t *)(malloc(ntasks * sizeof(papiex_perthread_data_t)));
    if (_task_data == NULL) {
      LIBPAPIEX_ERROR("malloc(%u) for all tasks thread data failed.",(unsigned int)(ntasks * sizeof(papiex_perthread_data_t)));
      monitor_real_exit(1);
    }

    if (!fptr_MPI_Gather)
      LIBPAPIEX_ERROR("fptr_MPI_Gather == NULL. Did you ever get a handle to MPI_Gather?");

    /* Now we do the MPI Gather */
// #ifdef MPICH
    fptr_MPI_Gather((char*)process_sums, sizeof(papiex_perthread_data_t), MPI_CHAR, &_task_data[0], sizeof(papiex_perthread_data_t), MPI_CHAR, 0, MPI_COMM_WORLD);
// #else
//    MPI_Gather((char*)thread_data, sizeof(papiex_perthread_data_t), MPI_CHAR, &_task_data[0], sizeof(papiex_perthread_data_t), MPI_CHAR, 0, MPI_COMM_WORLD);
// #endif
    LIBPAPIEX_DEBUG("Master finished aggregating data from all tasks\n");

  }
  else {
    /* Slave */
// #ifdef MPICH
    fptr_MPI_Gather((char*)process_sums, sizeof(papiex_perthread_data_t), MPI_CHAR, &_task_data[0], sizeof(papiex_perthread_data_t), MPI_CHAR, 0, MPI_COMM_WORLD);
// #else
//    MPI_Gather((char*)thread_data, sizeof(papiex_perthread_data_t), MPI_CHAR, &_task_data[0], sizeof(papiex_perthread_data_t), MPI_CHAR, 0, MPI_COMM_WORLD);
// #endif
    LIBPAPIEX_DEBUG("Task %d finished sending data to task 0\n", myrank);
  }
#endif
  LIBPAPIEX_DEBUG("Finished papiex_mpi_gather\n");
  return _task_data;
}

#ifdef HAVE_MPI
#  ifdef USE_MONITOR_MPI
extern void monitor_init_mpi(int rank, int size, int thread_support_level) {
  LIBPAPIEX_DEBUG("monitor_init_mpi callback invoked\n");
  is_mpied = 1;
  if ((rank == 0) && (!quiet)) print_banner(stderr);
  return;
}

extern void monitor_fini_mpi(void) {
  LIBPAPIEX_DEBUG("monitor_fini_mpi callback invoked\n");
  /* Force monitor to call the fini_process callback, monitor will guarantee 
     us that this only happens once. Only do this if we need to use MPI
     to gather the data. */
  if (!no_mpi_gather)
    monitor_force_fini_process();
}

#  else /* don't use USE_MONITOR_MPI */
extern int MPI_Init(int *pargc, char*** pargv) {
  LIBPAPIEX_DEBUG("Intercepted MPI_Init\n");
  is_mpied = 1;
  int rc = real_MPI_Init(pargc, pargv);
  if (rc == MPI_SUCCESS) {
    int rank = get_rank();
    if ((rank == 0) && (!quiet)) print_banner(stderr);
  }
  return (rc);
}
extern int MPI_Init_thread(int *pargc, char*** pargv, int required, int *provided) 
{
  LIBPAPIEX_DEBUG("Intercepted MPI_Init_thread\n");
  is_mpied = 1;
  return(real_MPI_Init_thread(pargc, pargv, required, provided));
}
extern int MPI_Finalize(void) {
  LIBPAPIEX_DEBUG("Intercepted MPI_Finalize rank %d\n",get_rank()); 

  /* Force monitor to call the fini_process callback, monitor will guarantee 
     us that this only happens once. Only do this if we need to use MPI
     to gather the data. */

  if (!no_mpi_gather)
    monitor_force_fini_process();

  LIBPAPIEX_DEBUG("Calling real MPI_Finalize, rank %d\n",get_rank()); 
  return(real_MPI_Finalize());
}

#define MPIP_MAX_ARG_STRING_SIZE 4096
void
getProcCmdLine (int *ac, char **av)
{
  int i = 0, pid;
  char *inbuf = NULL, file[256];
  FILE *infile;
  char *arg_ptr;

  *ac = 0;
  *av = NULL;

  pid = getpid ();
  snprintf (file, 256, "/proc/%d/cmdline", pid);
  infile = fopen (file, "r");

  if (infile != NULL)
    {
      while (!feof (infile))
	{
	  inbuf = malloc (MPIP_MAX_ARG_STRING_SIZE);
	  if (inbuf == NULL)
	    LIBPAPIEX_ERROR("malloc of %d bytes to copy arguments for MPI_Init", MPIP_MAX_ARG_STRING_SIZE);
	  if (fread (inbuf, 1, MPIP_MAX_ARG_STRING_SIZE, infile) > 0)
	    {
	      arg_ptr = inbuf;
	      while (*arg_ptr != '\0')
		{
		  av[i] = strdup (arg_ptr);
		  arg_ptr += strlen (av[i]) + 1;
		  i++;
		}
	    }
	}
      *ac = i;

      free (inbuf);
      fclose (infile);
    }
}
/* Fortran */
extern void mpi_init__(int *retval) 
{
  char **tmp_argv;
  char *pargv[32];
  int pargc;
  getProcCmdLine(&pargc,pargv);
  tmp_argv = pargv;
  *retval = MPI_Init(&pargc, (char ***)&tmp_argv);
}
extern void mpi_init_thread__(int *required, int *provided, int *retval) 
{
  char **tmp_argv;
  char *pargv[32];
  int pargc;
  getProcCmdLine(&pargc,pargv);
  tmp_argv = pargv;
  *retval = MPI_Init_thread(&pargc, (char ***)&tmp_argv, *required, provided);
}
extern void mpi_finalize__(void) 
{
  MPI_Finalize();
}
#pragma weak mpi_finalize = mpi_finalize__
#pragma weak mpi_finalize_ = mpi_finalize__
#pragma weak MPI_FINALIZE = mpi_finalize__
#pragma weak MPI_FINALIZE_ = mpi_finalize__
#pragma weak MPI_FINALIZE__ = mpi_finalize__
#pragma weak mpi_init = mpi_init__
#pragma weak mpi_init_ = mpi_init__
#pragma weak MPI_INIT = mpi_init__
#pragma weak MPI_INIT_ = mpi_init__
#pragma weak MPI_INIT__ = mpi_init__
#pragma weak mpi_init_thread = mpi_init_thread__
#pragma weak mpi_init_thread_ = mpi_init_thread__
#pragma weak MPI_INIT_THREAD = mpi_init_thread__
#pragma weak MPI_INIT_THREAD_ = mpi_init_thread__
#pragma weak MPI_INIT_THREAD__ = mpi_init_thread__
#  endif /* USE_MONITOR_MPI */
#endif /* HAVE_MPI */

static void print_global_stats(papiex_perthread_data_t *task_data,
                               FILE *output)
{
  int rank, i, j, k;

  assert(task_data);
  int ntasks = get_num_tasks();

  // This is here to make the top-level not have to calls with different arguments
  // not ideal, I agree
  if ((output == stderr) && write_only) output = NULL;

#ifdef HAVE_SCPERFUTILS
  strcpy(top_level_path, scperfutils_get_base_output_path());
#endif

  if (!no_write && !quiet && (output!=NULL))
    fprintf(output, "\n%s:\n%s: Storing output in [%s]\n%s:\n\n", 
                     tool, tool, top_level_path, tool);
  if (no_summary_stats) return;

  /* Finished calculating mean and stddev */
  if (proc_fini_time.tv_sec == 0)
    gettimeofday(&proc_fini_time, NULL);

  LIBPAPIEX_DEBUG("Master will now compute global stats, ntasks=%d\n", ntasks);

  papiex_perthread_data_t mean_d, min_d, max_d, stddev_d;
  papiex_perthread_data_t* mean_data = &mean_d;
  papiex_perthread_data_t* min_data = &min_d;
  papiex_perthread_data_t* max_data = &max_d;
  papiex_perthread_data_t* stddev_data = &stddev_d;
  papiex_perthread_data_t global_data_sums;
  papiex_double_perthread_data_t global_squared_sums;

  memset(mean_data, 0, sizeof(papiex_perthread_data_t));
  memset(stddev_data, 0, sizeof(papiex_perthread_data_t));
  memset(min_data, 0, sizeof(papiex_perthread_data_t));
  memset(max_data, 0, sizeof(papiex_perthread_data_t));
  memset(&global_data_sums, 0, sizeof(papiex_perthread_data_t));
  memset(&global_squared_sums, 0, sizeof(papiex_double_perthread_data_t));

  /* stddev computation and stddev structure population */

  /* compute mean over all the tasks */
  LIBPAPIEX_DEBUG("\tComputing the global means\n");

  for (rank=0; rank<ntasks; rank++) {
    LIBPAPIEX_DEBUG("\t\tProcessing totals for task %d\n", rank);
    global_data_sums.data[0].used += task_data[rank].data[0].used;
    global_data_sums.data[0].real_usec += task_data[rank].data[0].real_usec;
    global_data_sums.data[0].real_cyc += task_data[rank].data[0].real_cyc;
    global_data_sums.data[0].virt_usec += task_data[rank].data[0].virt_usec;
    global_data_sums.data[0].virt_cyc += task_data[rank].data[0].virt_cyc;
    for (i=0;i<eventcnt;i++)
      global_data_sums.data[0].counters[i] += task_data[rank].data[0].counters[i];
    global_data_sums.max_caliper_entries = max (global_data_sums.max_caliper_entries, task_data[rank].max_caliper_entries);
    global_data_sums.max_caliper_used = max (global_data_sums.max_caliper_used, task_data[rank].max_caliper_used);

    /* I think this can be improved a lot. 

    * We should only process the calipers we've used
    * We should not use a hash, we should just make sure the mapping is identical for the calipers, 
      otherwise the hash has to be done everywhere, not just here. */

    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (task_data[rank].data[j].used >0) {

        // Punt if the caliper label doesn't match
        if (match_caliper_label(global_data_sums.data[j].label, task_data[rank].data[j].label) != 0) {
	  LIBPAPIEX_WARN("Label for caliper %d is %s in the global sum AND %s in task %d\nSkipping the latter in the summation.\n"
	                 "Please treat all data (min,max,dev) for these calipers as suspect\n",
	      j, global_data_sums.data[j].label, task_data[rank].data[j].label, rank);
          continue;
	}
        global_data_sums.data[j].used += task_data[rank].data[j].used;
        global_data_sums.data[j].real_cyc += task_data[rank].data[j].real_cyc;
#ifdef FULL_CALIPER_DATA
        global_data_sums.data[j].real_usec += task_data[rank].data[j].real_usec;
        global_data_sums.data[j].virt_usec += task_data[rank].data[j].virt_usec;
        global_data_sums.data[j].virt_cyc += task_data[rank].data[j].virt_cyc;
#endif
	for (i=0;i<eventcnt;i++)
	  global_data_sums.data[j].counters[i] += task_data[rank].data[j].counters[i];
      }
    } /* calipers */
  } /* ranks */

  mean_data->data[0].used = global_data_sums.data[0].used / ntasks ; 
  //printf("START mpi mean rank %d, ntasks %d\n",get_rank(),ntasks); fflush(stdout);
  /* divide aggregate data by ntasks to get mean */
  mean_data->data[0].real_usec = global_data_sums.data[0].real_usec / ntasks ; 
  mean_data->data[0].real_cyc = global_data_sums.data[0].real_cyc / ntasks ; 
  mean_data->data[0].virt_usec = global_data_sums.data[0].virt_usec / ntasks ; 
  mean_data->data[0].virt_cyc = global_data_sums.data[0].virt_cyc / ntasks ; 
  for (i=0;i<eventcnt;i++)
    mean_data->data[0].counters[i] = global_data_sums.data[0].counters[i] / ntasks;
  for (j=1;j<PAPIEX_MAX_CALIPERS; j++) {
    if (global_data_sums.data[j].used >0) {
//#warning "This line makes no sense to me, shouldn't this really be the mean?"
      mean_data->data[j].used = global_data_sums.data[j].used / ntasks ; 
      if (strlen(global_data_sums.data[j].label) == 0)
        strcpy(mean_data->data[j].label, global_data_sums.data[j].label);
      mean_data->data[j].real_cyc = global_data_sums.data[j].real_cyc / ntasks ; 
#ifdef FULL_CALIPER_DATA
      mean_data->data[j].real_usec = global_data_sums.data[j].real_usec / ntasks ; 
      mean_data->data[j].virt_usec = global_data_sums.data[j].virt_usec / ntasks ; 
      mean_data->data[j].virt_cyc = global_data_sums.data[j].virt_cyc / ntasks ; 
#endif
      for (i=0;i<eventcnt;i++)
        mean_data->data[j].counters[i] = global_data_sums.data[j].counters[i] / ntasks;
    }
  } /* calipers */

  /* min computing */
  //printf("START mpi min rank %d, tasks %d\n",get_rank(),ntasks); fflush(stdout);
  LIBPAPIEX_DEBUG("\tComputing mpi min stats for rank %d\n", rank);
  min_data->data[0].real_usec = task_data[0].data[0].real_usec;
  min_data->data[0].real_cyc = task_data[0].data[0].real_cyc;
  min_data->data[0].virt_usec = task_data[0].data[0].virt_usec;
  min_data->data[0].virt_cyc = task_data[0].data[0].virt_cyc;
  for (i=0;i<eventcnt;i++)
    min_data->data[0].counters[i] = task_data[0].data[0].counters[i];
  for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
    if (task_data[0].data[j].used >0) {
      min_data->data[j].used = task_data[0].data[j].used;
      min_data->data[j].real_cyc = task_data[0].data[j].real_cyc;
#ifdef FULL_CALIPER_DATA
      min_data->data[j].real_usec = task_data[0].data[j].real_usec;
      min_data->data[j].virt_usec = task_data[0].data[j].virt_usec;
      min_data->data[j].virt_cyc = task_data[0].data[j].virt_cyc;
#endif
      for (k=0;k<eventcnt;k++)
        min_data->data[j].counters[k] = task_data[0].data[j].counters[k];
    }
  } /* calipers */

  // printf("START mpi min rank %d, tasks %d\n",get_rank(),ntasks); fflush(stdout);
  for (rank=1; rank<ntasks; rank++) {
    LIBPAPIEX_DEBUG("\t\tProcessing rank %d\n", rank);
    min_data->data[0].real_usec = min(min_data->data[0].real_usec, task_data[rank].data[0].real_usec);
    min_data->data[0].real_cyc = min(min_data->data[0].real_cyc, task_data[rank].data[0].real_cyc);
    min_data->data[0].virt_usec = min(min_data->data[0].virt_usec, task_data[rank].data[0].virt_usec);
    min_data->data[0].virt_cyc = min(min_data->data[0].virt_cyc, task_data[rank].data[0].virt_cyc);
    for (i=0;i<eventcnt;i++)
      min_data->data[0].counters[i] = min(min_data->data[0].counters[i], task_data[rank].data[0].counters[i]);
    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (task_data[rank].data[j].used >0) {
        min_data->data[j].used = (float) min(min_data->data[j].used, task_data[rank].data[j].used);
        min_data->data[j].real_cyc = min(min_data->data[j].real_cyc, task_data[rank].data[j].real_cyc);
#ifdef FULL_CALIPER_DATA
        min_data->data[j].real_usec = min(min_data->data[j].real_usec, task_data[rank].data[j].real_usec);
        min_data->data[j].virt_usec = min(min_data->data[j].virt_usec, task_data[rank].data[j].virt_usec);
        min_data->data[j].virt_cyc = min(min_data->data[j].virt_cyc, task_data[rank].data[j].virt_cyc);
#endif
        for (k=0;k<eventcnt;k++)
          min_data->data[j].counters[k] = min(min_data->data[j].counters[k], task_data[rank].data[j].counters[k]);
      }
    } /* calipers */
  } /* ranks */

  /* max computing */
  //printf("START mpi max rank %d, tasks %d\n",get_rank(),ntasks); fflush(stdout);
  LIBPAPIEX_DEBUG("\tComputing global max values\n");
  max_data->data[0].real_usec = task_data[0].data[0].real_usec;
  max_data->data[0].real_cyc = task_data[0].data[0].real_cyc;
  max_data->data[0].virt_usec = task_data[0].data[0].virt_usec;
  max_data->data[0].virt_cyc = task_data[0].data[0].virt_cyc;
  for (i=0;i<eventcnt;i++)
    max_data->data[0].counters[i] = task_data[0].data[0].counters[i];
  for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
    if (task_data[0].data[j].used >0) {
      max_data->data[j].used =  task_data[0].data[j].used;
      max_data->data[j].real_cyc = task_data[0].data[j].real_cyc;
#ifdef FULL_CALIPER_DATA
      max_data->data[j].real_usec = task_data[0].data[j].real_usec;
      max_data->data[j].virt_usec = task_data[0].data[j].virt_usec;
      max_data->data[j].virt_cyc = task_data[0].data[j].virt_cyc;
#endif
      for (k=0;k<eventcnt;k++)
        max_data->data[j].counters[k] = task_data[0].data[j].counters[k];
    }
  } /* calipers */

  //printf("START mpi max rank %d, tasks %d\n",get_rank(),ntasks); fflush(stdout);
  for (rank=1; rank<ntasks; rank++) {
    LIBPAPIEX_DEBUG("\t\tProcessing totals from rank %d\n", rank);
    max_data->data[0].real_usec = max(max_data->data[0].real_usec, task_data[rank].data[0].real_usec);
    max_data->data[0].real_cyc = max(max_data->data[0].real_cyc, task_data[rank].data[0].real_cyc);
    max_data->data[0].virt_usec = max(max_data->data[0].virt_usec, task_data[rank].data[0].virt_usec);
    max_data->data[0].virt_cyc = max(max_data->data[0].virt_cyc, task_data[rank].data[0].virt_cyc);
    for (i=0;i<eventcnt;i++)
      max_data->data[0].counters[i] = max(max_data->data[0].counters[i], task_data[rank].data[0].counters[i]);
    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (task_data[rank].data[j].used >0) {
        max_data->data[j].used = (float) max(max_data->data[j].used, task_data[rank].data[j].used);
        max_data->data[j].real_cyc = max(max_data->data[j].real_cyc, task_data[rank].data[j].real_cyc);
#ifdef FULL_CALIPER_DATA
        max_data->data[j].real_usec = max(max_data->data[j].real_usec, task_data[rank].data[j].real_usec);
        max_data->data[j].virt_usec = max(max_data->data[j].virt_usec, task_data[rank].data[j].virt_usec);
        max_data->data[j].virt_cyc = max(max_data->data[j].virt_cyc, task_data[rank].data[j].virt_cyc);
#endif
        for (k=0;k<eventcnt;k++)
          max_data->data[j].counters[k] = max(max_data->data[j].counters[k], task_data[rank].data[j].counters[k]);
      }
    } /* calipers */
  } /* ranks */

  /* stddev computation and stddev structure population */
  LIBPAPIEX_DEBUG("\tNow computing standard deviation for rank %d\n", rank);
  //printf("START mpi squared_sums rank %d, ntasks %d\n",get_rank(),ntasks); fflush(stdout);

  for (rank=0; rank<ntasks; rank++) {
    LIBPAPIEX_DEBUG("\tProcessing totals from task %d\n", rank);

    /* aggregate into the  global sums data structure */
    //printf("START global_squared_sums mean_real_usec %lld task_real_usec %lld\n",mean_data->data[0].real_usec,task_data[rank].data[0].real_usec); fflush(stdout);
    global_squared_sums.data[0].real_usec += sqdiff_ll(mean_data->data[0].real_usec,task_data[rank].data[0].real_usec);
    //printf("START global_squared_sums real_usec %f\n",global_squared_sums.data[0].real_usec);
    global_squared_sums.data[0].real_cyc += sqdiff_ll(mean_data->data[0].real_cyc,task_data[rank].data[0].real_cyc);
    //printf("START global_squared_sums real_cyc %f\n",global_squared_sums.data[0].real_cyc);
    global_squared_sums.data[0].virt_usec += sqdiff_ll(mean_data->data[0].virt_usec,task_data[rank].data[0].virt_usec);
    //printf("START global_squared_sums virt_usec %f\n",global_squared_sums.data[0].virt_usec);
    global_squared_sums.data[0].virt_cyc += sqdiff_ll(mean_data->data[0].virt_cyc,task_data[rank].data[0].virt_cyc);
    //printf("START global_squared_sums virt_cyc %f\n",global_squared_sums.data[0].virt_cyc);
    for (k=0;k<eventcnt;k++)
      global_squared_sums.data[0].counters[k] += sqdiff_ll(mean_data->data[0].counters[k],task_data[rank].data[0].counters[k]);
    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (task_data[rank].data[j].used >0) {
        global_squared_sums.data[j].used += sqdiff_ll(mean_data->data[j].used,task_data[rank].data[j].used);
        global_squared_sums.data[j].real_cyc += sqdiff_ll(mean_data->data[j].real_cyc,task_data[rank].data[j].real_cyc);
#ifdef FULL_CALIPER_DATA
        global_squared_sums.data[j].real_usec += sqdiff_ll(mean_data->data[j].real_usec,task_data[rank].data[j].real_usec);
        global_squared_sums.data[j].virt_usec += sqdiff_ll(mean_data->data[j].virt_usec,task_data[rank].data[j].virt_usec);
        global_squared_sums.data[j].virt_cyc += sqdiff_ll(mean_data->data[j].virt_cyc,task_data[rank].data[j].virt_cyc);
#endif
	for (k=0;k<eventcnt;k++)
          global_squared_sums.data[j].counters[k] += sqdiff_ll(mean_data->data[j].counters[k],task_data[rank].data[j].counters[k]);
      }
    } /* calipers */
  }

  /* divide by aggregates by ntasks to get stddev */
  //printf("START mpi stddev rank %d, ntasks %d\n",get_rank(),ntasks); fflush(stdout);
  //printf("START mpi stddev rank %d, ntasks %d, real_usec %f\n",get_rank(),ntasks,global_squared_sums.data[0].real_usec); fflush(stdout);
  stddev_data->data[0].real_usec = (long long) sqrt(global_squared_sums.data[0].real_usec / (double)ntasks); 
  //printf("START mpi stddev rank %d, ntasks %d, real_cyc %f\n",get_rank(),ntasks,global_squared_sums.data[0].real_cyc); fflush(stdout);
  stddev_data->data[0].real_cyc = (long long) sqrt(global_squared_sums.data[0].real_cyc / (double)ntasks); 
  //printf("START mpi stddev rank %d, ntasks %d, virt_usec %f\n",get_rank(),ntasks,global_squared_sums.data[0].virt_usec); fflush(stdout);
  stddev_data->data[0].virt_usec = (long long) sqrt(global_squared_sums.data[0].virt_usec / (double)ntasks) ; 
  //printf("START mpi stddev rank %d, ntasks %d, virt_cyc %f\n",get_rank(),ntasks,global_squared_sums.data[0].virt_cyc); fflush(stdout);
  stddev_data->data[0].virt_cyc = (long long) sqrt(global_squared_sums.data[0].virt_cyc / (double)ntasks) ; 
  for (k=0;k<eventcnt;k++) {
    //printf("START mpi stddev rank %d, ntasks %d, virt_cyc %f\n",get_rank(),ntasks,global_squared_sums.data[0].counters[k]); fflush(stdout);
    stddev_data->data[0].counters[k] = (long long) sqrt(global_squared_sums.data[0].counters[k] / (double)ntasks);
  }
  for (j=1;j<PAPIEX_MAX_CALIPERS; j++) {
    if (global_squared_sums.data[j].used >0) {
      //printf("USED mpi rank %d, tasks %d, caliper %d, used %d, real %lld\n",get_rank(),ntasks,j,global_squared_sums.data[j].used,global_squared_sums.data[j].real_cyc); fflush(stdout);
//#warning "Why is this not divided by ntasks?"
      stddev_data->data[j].used = sqrt(global_squared_sums.data[j].used / ntasks) ; 
      //stddev_data->data[j].used = (long long) global_squared_sums.data[j].used;
      stddev_data->data[j].real_cyc = (long long) sqrt(global_squared_sums.data[j].real_cyc / (double)ntasks) ; 
#ifdef FULL_CALIPER_DATA
      stddev_data->data[j].real_usec = (long long) sqrt(global_squared_sums.data[j].real_usec / (double)ntasks) ; 
      stddev_data->data[j].virt_usec = (long long) sqrt(global_squared_sums.data[j].virt_usec / (double)ntasks) ; 
      stddev_data->data[j].virt_cyc = (long long) sqrt(global_squared_sums.data[j].virt_cyc / (double)ntasks) ; 
#endif
      for (k=0;k<eventcnt;k++) {
	//printf("USED mpi rank %d, tasks %d, caliper %d, counter %lld\n",get_rank(),ntasks,j,global_squared_sums.data[j].counters[k]); fflush(stdout);
        stddev_data->data[j].counters[k] = (long long) sqrt(global_squared_sums.data[j].counters[k] / (double)ntasks);
      }
    }
  }
  //printf("FINISHED mpi stddev rank %d\n",get_rank()); fflush(stdout);

  /* Finished calculating mean and stddev */

  LIBPAPIEX_DEBUG("Master will now print global stats\n");

  if (output != NULL) {
    if (!quiet) {
      /* Print executable information */
      print_executable_info(output);
      fprintf(output, "%-30s: %d\n", "Num. of MPI tasks", ntasks);
      fprintf(output, "%-30s: %s", "Start", ctime(&proc_init_time.tv_sec));
      fprintf(output, "%-30s: %s", "Finish", ctime(&proc_fini_time.tv_sec));
    }
    fprintf(output,"\n");
    print_all_pretty_stats(output, &global_data_sums, JOB_SCOPE, 0);  // last arg ignored
    print_min_max_mean_cv(output, min_data, max_data, mean_data, stddev_data);
    fprintf(output, "Cumulative Program Counts:\n");
    print_counters(output, &global_data_sums);
  } /* output != NULL */

  /* also write the above output to file */
  if (!no_write) {
    char global_file[PATH_MAX];
    sprintf(global_file, "%s/job_summary.txt", top_level_path);
    unlink(global_file);
    LIBPAPIEX_DEBUG("Now printing global stats to file (%s)\n", global_file);
    FILE *global_out = fopen(global_file, "w");
    if (!global_out) {
      LIBPAPIEX_ERROR("fopen(%s) failed. %s",
              global_file, strerror(errno));
      monitor_real_exit(1);
    }
    if (!quiet) {
      /* Print executable information */
      print_executable_info(global_out);
      fprintf(global_out, "%-30s: %d\n", "Num. of MPI tasks", ntasks);
      fprintf(global_out, "%-30s: %s", "Start", ctime(&proc_init_time.tv_sec));
      fprintf(global_out, "%-30s: %s", "Finish", ctime(&proc_fini_time.tv_sec));
    }
    fprintf(global_out,"\n");
    print_all_pretty_stats(global_out, &global_data_sums, JOB_SCOPE, 0); // last arg ignored
    print_min_max_mean_cv(global_out, min_data, max_data, mean_data, stddev_data);
    fprintf(global_out, "Cumulative Program Counts:\n");
    print_counters(global_out, &global_data_sums);
    fclose(global_out);
  }
  LIBPAPIEX_DEBUG("Master has finished printing global stats\n");

  return;

}

#ifndef HAVE_SCPERFUTILS

/* This function can be harmlessly called repeatedly */
static void set_top_level_path(void) {
  if (strlen(top_level_path)==0) {
     sprintf(top_level_path, "%s%s.%s.%s.%d", user_path_prefix, process_name, tool, hostname, (int)getpid());

     // if the user has specified an output file name, that overrides the setting above
     if (file_output && !file_prefix)
       strcpy(top_level_path, file_output);

     int gen = get_next_gen(top_level_path);
     if (gen) {
       char tmp_str[25];
       sprintf(tmp_str, ".%d", gen);
       strcat(top_level_path, tmp_str);
     }

    if (is_mpied && !no_mpi_gather) {
#ifdef HAVE_MPI
      LIBPAPIEX_DEBUG("Setting top-level-path: Using MPI_Bcast to share the master's path\n");
      fptr_MPI_Bcast(top_level_path, PATH_MAX, MPI_CHAR, 0, MPI_COMM_WORLD);
      LIBPAPIEX_DEBUG("Received master's path : %s\n", top_level_path);
#endif
    }
    LIBPAPIEX_DEBUG("Set top-level-path to %s\n", top_level_path);
  }
}

/* This function can be harmlessly called repeatedly */
static void make_top_level_path(void) {
  set_top_level_path(); 
  LIBPAPIEX_DEBUG("Making top-level path (%s)\n", top_level_path);
  if(mkdir(top_level_path, 0755)) {
    if (errno != EEXIST) {
      LIBPAPIEX_ERROR("mkdir(%s) failed. %s", top_level_path, strerror(errno));
      monitor_real_exit(1);
    }
  }
  return;
}

/* This function can be harmlessly called repeatedly */
/* Note, this function sets the global proc_stats_path variable */
static void make_proc_stats_path() {
  make_top_level_path();
  /* for MPI programs, the output path is suffixed with the rank */
  if (is_mpied)
    sprintf(proc_stats_path, "%s/task_%d", top_level_path, get_rank());
  else
    strcpy(proc_stats_path, top_level_path);

  LIBPAPIEX_DEBUG("Process %d output path set to %s\n", get_rank(), proc_stats_path);
  /* If it's a threaded program then the proc_stats_path is a directory
   * we create. If it's an unthreaded program, then proc_stats_path
   * becomes the name of the file itself
   */
  if (_papiex_threaded) {
    LIBPAPIEX_DEBUG("Making process path for rank %d (%s)\n", get_rank(), proc_stats_path);
    if(mkdir(proc_stats_path, 0755))
      if (errno != EEXIST) {
        LIBPAPIEX_ERROR("mkdir(%s) failed. %s", proc_stats_path, strerror(errno));
        monitor_real_exit(1);
      }
  }
  return; 
}

/* This function can be harmlessly called repeatedly */
static void make_paths(void) {
  set_top_level_path();
  /* create needed directories */
  if (!no_write && (_papiex_threaded || is_mpied))  {
    LIBPAPIEX_DEBUG("Making necessary paths\n");
    make_proc_stats_path();
  }
  else  // for unthreaded, non-mpi the top-level path is the file name
    strcpy(proc_stats_path, top_level_path); 
  LIBPAPIEX_DEBUG("Process %d output path set to %s\n", get_rank(), proc_stats_path);
}

#endif /* ifndef HAVE_SCPERFUTILS */

static void print_process_stats(PAPI_all_thr_spec_t *process, 
                                papiex_perthread_data_t *process_data_sums,
                                FILE *output) 
{
  int i, j, k;
  unsigned long tid;
  papiex_perthread_data_t *thr_data;
  papiex_perthread_data_t mean_d, min_d, max_d, stddev_d;
  papiex_perthread_data_t* mean_data = &mean_d;
  papiex_perthread_data_t* min_data = &min_d;
  papiex_perthread_data_t* max_data = &max_d;
  papiex_perthread_data_t* stddev_data = &stddev_d;
  papiex_double_perthread_data_t squared_sums;
  int rank = get_rank();

  assert(process);
  assert(process_data_sums);

  memset(mean_data, 0, sizeof(papiex_perthread_data_t));
  memset(stddev_data, 0, sizeof(papiex_perthread_data_t));
  memset(min_data, 0, sizeof(papiex_perthread_data_t));
  memset(max_data, 0, sizeof(papiex_perthread_data_t));
  memset(&squared_sums, 0, sizeof(papiex_double_perthread_data_t));
  memset(process_data_sums, 0, sizeof(papiex_perthread_data_t));

  // This is here to make the top-level not have to calls with different arguments
  // not ideal, I agree
  if ((output == stderr) && write_only) output = NULL;

  nthreads = process->num;
  LIBPAPIEX_DEBUG("Computing stats for process rank=%d, %d threads, process_ptr=%p\n",
                   rank, nthreads, process);

#ifdef HAVE_SCPERFUTILS
  strcpy(proc_stats_path, scperfutils_get_proc_output_path());
  if (! _papiex_threaded) strcat(proc_stats_path, suffix);
#endif

  if (!quiet && !no_write &&  (output != NULL) && !is_mpied)
    fprintf(output, "\n%s:\n%s: Storing output in [%s]\n%s:\n\n", 
                     tool, tool, proc_stats_path, tool);

  float process_walltime_usec = (1000000 * proc_fini_time.tv_sec + proc_fini_time.tv_usec) -
                                 (1000000 * proc_init_time.tv_sec + proc_init_time.tv_usec);

  int scope = (_papiex_threaded ? THR_SCOPE:PROC_SCOPE);
  for (i=0;i<nthreads;i++) {
    tid = process->id[i];
    thr_data = process->data[i];

    /* do we need to write, per-thread-data?*/
    if (!no_write) {
      char thr_file[PATH_MAX];

      /* If it's a threaded program then we use the proc_stats_path
       * to derive the file name, by appending the thread id.
       * If it's an unthreaded program, then we just use 
       * set the thread file name to the proc_stats_path
       */
      if (_papiex_threaded)
#ifdef  HAVE_SCPERFUTILS
        strcpy(thr_file, scperfutils_get_thread_output_path(i));
#else
        sprintf(thr_file, "%s/thread_%d", proc_stats_path, i);
#endif
      else
        strcpy(thr_file, proc_stats_path);

      unlink(thr_file);
      FILE *thr_out = fopen(thr_file, "w");
      if (!thr_out) {
        LIBPAPIEX_ERROR("fopen(%s) failed. %s",
                thr_file, strerror(errno));
        monitor_real_exit(1);
      }
      /* one hack: */
      /* For the main thread in an MPI threaded program, the
       * finish time is not set. We set it here
       */
      if (thr_data->finish == 0)
        thr_data->finish = proc_fini_time.tv_sec;

      print_thread_stats(thr_out, thr_data, i, scope, process_walltime_usec);
      fclose(thr_out);
      LIBPAPIEX_DEBUG("Finished printing stats for thread %d\n", i);
    }
  }
  /* finished writing thread output into thr-specific file */

//#warning "Why is this here? This should be done just like every other program, not modal. Can't we do this in the standard path?"

  /* If this is an unthreaded (non-MPI) program, then we additionally print the stats
   * to output and copy the thread structure to the sums */

  /* These lines may be the source of bug 3880 */

  if (!_papiex_threaded) {
    if ((output != NULL) && !is_mpied)
      print_thread_stats(output, process->data[0], 0, PROC_SCOPE,  process_walltime_usec);
    memcpy(process_data_sums, process->data[0], sizeof(papiex_perthread_data_t));
    return;
  }

  /* Return now, if the user doesn't want to do any processing */
  if (no_summary_stats) return;
  
  LIBPAPIEX_DEBUG("Computing summary stats for rank %d\n", rank);

  /* Compute mean and stddev over the threads */
  LIBPAPIEX_DEBUG("\tNow computing process sums stats for rank %d\n", rank);
  //printf("START sum rank %d, threads %d\n",get_rank(),nthreads); fflush(stdout);
  for (i=0;i<nthreads;i++) {
    tid = process->id[i];
    thr_data = process->data[i];
    LIBPAPIEX_DEBUG("\t\tProcessing totals for thread %d\n", i);
 
    /* aggregate into the global sums data structure */
    process_data_sums->data[0].used += thr_data->data[0].used;
    process_data_sums->data[0].real_usec += thr_data->data[0].real_usec;
    process_data_sums->data[0].real_cyc += thr_data->data[0].real_cyc;
    process_data_sums->data[0].virt_usec += thr_data->data[0].virt_usec;
    process_data_sums->data[0].virt_cyc += thr_data->data[0].virt_cyc;
    for (k=0;k<eventcnt;k++)
      process_data_sums->data[0].counters[k] += thr_data->data[0].counters[k];
    process_data_sums->max_caliper_entries = max (process_data_sums->max_caliper_entries, thr_data->max_caliper_entries);
    process_data_sums->max_caliper_used = max (process_data_sums->max_caliper_used, thr_data->max_caliper_used);

    /* I think this can be improved a lot. 

    * We should only process the calipers we've used
    * We should not use a hash, we should just make sure the mapping is identical for the calipers, 
      otherwise the hash has to be done everywhere, not just here. */

    for (j=1;j<thr_data->max_caliper_entries;j++) {
      if (thr_data->data[j].used >0) {

        // punt if the caliper labels don't match
        if (match_caliper_label(process_data_sums->data[j].label, thr_data->data[j].label) != 0) {
	   LIBPAPIEX_WARN("Label for caliper %d is %s in the sum AND %s in thread %d\nSkipping the latter in the summation.\n"
	                  "Please treat all data (min,max,dev) for these calipers as suspect\n",
	     j, process_data_sums->data[j].label, thr_data->data[j].label, i);
           continue;
	}

        LIBPAPIEX_DEBUG("\t\t\tProcessing caliper %d (%s)\n", j, process_data_sums->data[j].label);
        process_data_sums->data[j].used += thr_data->data[j].used;
	process_data_sums->data[j].real_cyc += thr_data->data[j].real_cyc;
#ifdef FULL_CALIPER_DATA
	process_data_sums->data[j].virt_usec += thr_data->data[j].virt_usec;
	process_data_sums->data[j].virt_cyc += thr_data->data[j].virt_cyc;
        process_data_sums->data[j].real_usec += thr_data->data[j].real_usec;
#endif
	for (k=0;k<eventcnt;k++)
          process_data_sums->data[j].counters[k] += thr_data->data[j].counters[k];
      }
    } /* calipers */
  } /* threads */

//  printf("START mean rank %d, threads %d\n",get_rank(),nthreads); fflush(stdout);
  /* divide aggregate data by nthreads to get mean */
  mean_data->data[0].used = process_data_sums->data[0].used / nthreads ; 
  mean_data->data[0].real_usec = process_data_sums->data[0].real_usec / nthreads ; 
  mean_data->data[0].real_cyc = process_data_sums->data[0].real_cyc / nthreads ; 
  mean_data->data[0].virt_usec = process_data_sums->data[0].virt_usec / nthreads ; 
  mean_data->data[0].virt_cyc = process_data_sums->data[0].virt_cyc / nthreads ; 
  for (k=0;k<eventcnt;k++)
    mean_data->data[0].counters[k] = process_data_sums->data[0].counters[k] / nthreads;
  for (j=1;j<PAPIEX_MAX_CALIPERS; j++) {
    if (process_data_sums->data[j].used >0) {
//#warning "This line makes no sense to me, shouldn't this really be the mean?"
      mean_data->data[j].used = process_data_sums->data[j].used / nthreads ; 
//      mean_data->data[j].used = process_data_sums->data[j].used;
      if (strlen(process_data_sums->data[j].label) == 0)
        strcpy(mean_data->data[j].label, process_data_sums->data[j].label);
     // printf("Mean valued of used for caliper %s is %g\n", mean_data->data[j].label, mean_data->data[j].used);
      mean_data->data[j].real_cyc = process_data_sums->data[j].real_cyc / nthreads ; 
#ifdef FULL_CALIPER_DATA
      mean_data->data[j].real_usec = process_data_sums->data[j].real_usec / nthreads ; 
      mean_data->data[j].virt_usec = process_data_sums->data[j].virt_usec / nthreads ; 
      mean_data->data[j].virt_cyc = process_data_sums->data[j].virt_cyc / nthreads ; 
#endif
      for (k=0;k<eventcnt;k++)
        mean_data->data[j].counters[k] = process_data_sums->data[j].counters[k] / nthreads;
    }
  } /* calipers */

  //printf("START min rank %d, threads %d\n",get_rank(),nthreads); fflush(stdout);
  /* min computing */
  LIBPAPIEX_DEBUG("\tComputing thread min stats for rank %d\n", rank);
  thr_data = process->data[0];
  min_data->data[0].real_usec = thr_data->data[0].real_usec;
  min_data->data[0].real_cyc = thr_data->data[0].real_cyc;
  min_data->data[0].virt_usec = thr_data->data[0].virt_usec;
  min_data->data[0].virt_cyc = thr_data->data[0].virt_cyc;
  for (i=0;i<eventcnt;i++)
    min_data->data[0].counters[i] = thr_data->data[0].counters[i];
  for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
    if (thr_data->data[j].used >0) {
      min_data->data[j].used = thr_data->data[j].used;
      min_data->data[j].real_cyc = thr_data->data[j].real_cyc;
#ifdef FULL_CALIPER_DATA
      min_data->data[j].real_usec = thr_data->data[j].real_usec;
      min_data->data[j].virt_usec = thr_data->data[j].virt_usec;
      min_data->data[j].virt_cyc = thr_data->data[j].virt_cyc;
#endif
      for (k=0;k<eventcnt;k++)
        min_data->data[j].counters[k] = thr_data->data[j].counters[k];
    }
  } /* calipers */

  //printf("START min rank %d, threads %d\n",get_rank(),nthreads); fflush(stdout);
  for (i=1; i<nthreads; i++) {
    thr_data = process->data[i];
    LIBPAPIEX_DEBUG("\t\tProcessing thread %d for rank %d\n", i, rank);
    min_data->data[0].real_usec = min(min_data->data[0].real_usec, thr_data->data[0].real_usec);
    min_data->data[0].real_cyc = min(min_data->data[0].real_cyc, thr_data->data[0].real_cyc);
    min_data->data[0].virt_usec = min(min_data->data[0].virt_usec, thr_data->data[0].virt_usec);
    min_data->data[0].virt_cyc = min(min_data->data[0].virt_cyc, thr_data->data[0].virt_cyc);
    for (k=0;k<eventcnt;k++)
      min_data->data[0].counters[k] = min(min_data->data[0].counters[k], thr_data->data[0].counters[k]);
    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (thr_data->data[j].used >0) {
        min_data->data[j].used = min(min_data->data[j].used, thr_data->data[j].used);
        min_data->data[j].real_cyc = min(min_data->data[j].real_cyc, thr_data->data[j].real_cyc);
#ifdef FULL_CALIPER_DATA
        min_data->data[j].real_usec = min(min_data->data[j].real_usec, thr_data->data[j].real_usec);
        min_data->data[j].virt_usec = min(min_data->data[j].virt_usec, thr_data->data[j].virt_usec);
        min_data->data[j].virt_cyc = min(min_data->data[j].virt_cyc, thr_data->data[j].virt_cyc);
#endif
        for (k=0;k<eventcnt;k++)
          min_data->data[j].counters[k] = min(min_data->data[j].counters[k], thr_data->data[j].counters[k]);
      }
    } /* calipers */
  } /* nthreads */

  //printf("START max rank %d, threads %d\n",get_rank(),nthreads); fflush(stdout);
  /* max computing */
  LIBPAPIEX_DEBUG("\tComputing max. stats for rank %d\n", rank);
  thr_data = process->data[0];
  max_data->data[0].real_usec = thr_data->data[0].real_usec;
  max_data->data[0].real_cyc = thr_data->data[0].real_cyc;
  max_data->data[0].virt_usec = thr_data->data[0].virt_usec;
  max_data->data[0].virt_cyc = thr_data->data[0].virt_cyc;
  for (k=0;k<eventcnt;k++)
    max_data->data[0].counters[k] = thr_data->data[0].counters[k];
  for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
    if (thr_data->data[j].used >0) {
      max_data->data[j].used = thr_data->data[j].used;
      max_data->data[j].real_cyc = thr_data->data[j].real_cyc;
#ifdef FULL_CALIPER_DATA
      max_data->data[j].real_usec = thr_data->data[j].real_usec;
      max_data->data[j].virt_usec = thr_data->data[j].virt_usec;
      max_data->data[j].virt_cyc = thr_data->data[j].virt_cyc;
#endif
      for (k=0;k<eventcnt;k++)
        max_data->data[j].counters[k] = thr_data->data[j].counters[k];
    }
  } /* calipers */

  //printf("START max rank %d, threads %d\n",get_rank(),nthreads); fflush(stdout);
  for (i=1; i<nthreads; i++) {
    thr_data = process->data[i];
    LIBPAPIEX_DEBUG("\t\tProcessing thread %d for rank %d\n", i, rank);
    max_data->data[0].real_usec = max(max_data->data[0].real_usec, thr_data->data[0].real_usec);
    max_data->data[0].real_cyc = max(max_data->data[0].real_cyc, thr_data->data[0].real_cyc);
    max_data->data[0].virt_usec = max(max_data->data[0].virt_usec, thr_data->data[0].virt_usec);
    max_data->data[0].virt_cyc = max(max_data->data[0].virt_cyc, thr_data->data[0].virt_cyc);
    for (k=0;k<eventcnt;k++)
      max_data->data[0].counters[k] = max(max_data->data[0].counters[k], thr_data->data[0].counters[k]);
    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (thr_data->data[j].used >0) {
        max_data->data[j].used = max(max_data->data[j].used, thr_data->data[j].used);
        max_data->data[j].real_cyc = max(max_data->data[j].real_cyc, thr_data->data[j].real_cyc);
#ifdef FULL_CALIPER_DATA
        max_data->data[j].real_usec = max(max_data->data[j].real_usec, thr_data->data[j].real_usec);
        max_data->data[j].virt_usec = max(max_data->data[j].virt_usec, thr_data->data[j].virt_usec);
        max_data->data[j].virt_cyc = max(max_data->data[j].virt_cyc, thr_data->data[j].virt_cyc);
#endif
        for (k=0;k<eventcnt;k++)
          max_data->data[j].counters[k] = max(max_data->data[j].counters[k], thr_data->data[j].counters[k]);
      }
    } /* calipers */
  } /* nthreads */

  /* stddev computation and stddev structure population */
  LIBPAPIEX_DEBUG("\tNow computing standard deviation for rank %d\n", rank);
  //printf("START squared_sums rank %d, threads %d\n",get_rank(),nthreads); fflush(stdout);

  for (i=0; i<nthreads; i++) {
    thr_data = process->data[i];
    LIBPAPIEX_DEBUG("\t\tProcessing thread %d for rank %d\n", i, rank);

    /* aggregate into the  global sums data structure */
    squared_sums.data[0].real_cyc += sqdiff_ll(mean_data->data[0].real_cyc,thr_data->data[0].real_cyc);
    squared_sums.data[0].real_usec += sqdiff_ll(mean_data->data[0].real_usec,thr_data->data[0].real_usec);
    squared_sums.data[0].virt_usec += sqdiff_ll(mean_data->data[0].virt_usec,thr_data->data[0].virt_usec);
    squared_sums.data[0].virt_cyc += sqdiff_ll(mean_data->data[0].virt_cyc,thr_data->data[0].virt_cyc);
    for (k=0;k<eventcnt;k++)
      squared_sums.data[0].counters[k] += sqdiff_ll(mean_data->data[0].counters[k],thr_data->data[0].counters[k]);
    for (j=1;j<PAPIEX_MAX_CALIPERS;j++) {
      if (thr_data->data[j].used >0) {
	// printf("rank %d Caliper\n",get_rank());
        squared_sums.data[j].used += sqdiff_ll(mean_data->data[j].used,thr_data->data[j].used);
        squared_sums.data[j].real_cyc += sqdiff_ll(mean_data->data[j].real_cyc,thr_data->data[j].real_cyc);
#ifdef FULL_CALIPER_DATA
        squared_sums.data[j].real_usec += sqdiff_ll(mean_data->data[j].real_usec,thr_data->data[j].real_usec);
        squared_sums.data[j].virt_usec += sqdiff_ll(mean_data->data[j].virt_usec,thr_data->data[j].virt_usec);
        squared_sums.data[j].virt_cyc += sqdiff_ll(mean_data->data[j].virt_cyc,thr_data->data[j].virt_cyc);
#endif
	for (k=0;k<eventcnt;k++)
          squared_sums.data[j].counters[k] += sqdiff_ll(mean_data->data[j].counters[k],thr_data->data[j].counters[k]);
      }
    } /* calipers */
  }

  /* divide by aggregates by nthreads to get std dev. */
  //printf("START stddev rank %d, threads %d\n",get_rank(),nthreads); fflush(stdout);
  stddev_data->data[0].real_usec = (long long) sqrt(squared_sums.data[0].real_usec / (double)nthreads) ; 
  stddev_data->data[0].real_cyc = (long long) sqrt(squared_sums.data[0].real_cyc / (double)nthreads) ; 
  stddev_data->data[0].virt_usec = (long long) sqrt(squared_sums.data[0].virt_usec / (double)nthreads) ; 
  stddev_data->data[0].virt_cyc = (long long) sqrt(squared_sums.data[0].virt_cyc / (double)nthreads) ; 
  // Why are these two not divided? 
//#warning "It makes no sense to me that these values are not divided by threads"
//#warning "it is different in global sums"
  for (k=0;k<eventcnt;k++)
    stddev_data->data[0].counters[k] = (long long) sqrt(squared_sums.data[0].counters[k] / (double)nthreads);
  for (j=1;j<PAPIEX_MAX_CALIPERS; j++) {
    if (squared_sums.data[j].used >0) {
      //printf("USED rank %d, threads %d, caliper %d, used %d, real %lld\n",get_rank(),nthreads,j,squared_sums.data[j].used,squared_sums.data[j].real_cyc); fflush(stdout);
//#warning "Why is this not divided by nthreads?"
      stddev_data->data[j].used = sqrt(squared_sums.data[j].used/(double)nthreads);
      stddev_data->data[j].real_cyc = (long long) sqrt(squared_sums.data[j].real_cyc / (double)nthreads) ; 
#ifdef FULL_CALIPER_DATA
      stddev_data->data[j].real_usec = (long long) sqrt(squared_sums.data[j].real_usec / (double)nthreads) ; 
      stddev_data->data[j].virt_usec = (long long) sqrt(squared_sums.data[j].virt_usec / (double)nthreads) ; 
      stddev_data->data[j].virt_cyc = (long long) sqrt(squared_sums.data[j].virt_cyc / (double)nthreads) ; 
#endif
      for (k=0;k<eventcnt;k++) {
	//printf("USED rank %d, threads %d, caliper %d, counter %lld\n",get_rank(),nthreads,j,squared_sums.data[j].counters[k]); fflush(stdout);
        stddev_data->data[j].counters[k] = (long long) sqrt(squared_sums.data[j].counters[k] / (double)nthreads);
      }
    }
  }
  //printf("FINISHED stddev rank %d\n",get_rank()); fflush(stdout);

  /* Finished calculating mean and stddev */

  LIBPAPIEX_DEBUG("\tNow printing statistics for rank %d\n", rank);
  // We don't print to stderr for MPI programs, only to a file
  if (output != NULL && !((output==stderr) && is_mpied)) {
    if (!quiet) {
      print_executable_info(output);
      fprintf(output,"%-30s: %d\n", "Num. of threads", nthreads);
      fprintf(output,"%-30s: %s", "Start",ctime(&proc_init_time.tv_sec));
      fprintf(output,"%-30s: %s", "Finish",ctime(&proc_fini_time.tv_sec));
    }
    fprintf(output, "\n");
    print_all_pretty_stats(output, process_data_sums, PROC_SCOPE, 0);  // last arg is ignored
    print_min_max_mean_cv(output, min_data, max_data, mean_data, stddev_data);
    fprintf(output, "Cumulative Process Counts:\n");
    print_counters(output, process_data_sums);
  } /* output != NULL */

  /* also write the above output to file */
  if (!no_write) {
    char proc_file[PATH_MAX];
    sprintf(proc_file, "%s/process_summary.txt", proc_stats_path);
    unlink(proc_file);
    FILE *proc_out = fopen(proc_file, "w");
    if (!proc_out) {
      LIBPAPIEX_ERROR("fopen(%s) failed. %s",
              proc_file, strerror(errno));
      monitor_real_exit(1);
    }
    if (!quiet) {
      print_executable_info(proc_out);
      fprintf(proc_out,"%-30s: %d\n", "Num. of threads", nthreads);
      fprintf(proc_out,"%-30s: %s", "Start",ctime(&proc_init_time.tv_sec));
      fprintf(proc_out,"%-30s: %s", "Finish",ctime(&proc_fini_time.tv_sec));
    }
    fprintf(proc_out, "\n");
    print_all_pretty_stats(proc_out, process_data_sums, PROC_SCOPE, 0); // last arg is ignored
    print_min_max_mean_cv(proc_out, min_data, max_data, mean_data, stddev_data);
    fprintf(proc_out, "Cumulative Process Counts:\n");
    print_counters(proc_out, process_data_sums);
    fclose(proc_out);
  }

  return;
}

/* This function should not probably be used to print
 * rusage and memory info, as that may not be thread-specific
 */
static void print_thread_stats(FILE *output, papiex_perthread_data_t *thread,
                               unsigned long tid, int scope, float process_walltime_usec)
{
  if (output == NULL) return;

  if (!quiet) { 
    print_executable_info(output);
    if (_papiex_threaded) fprintf(output,"%-30s: %lu\n", "Thread id", tid);
#ifdef HAVE_PAPI
    fprintf(output, "%-30s: %s", "Start",ctime(&thread->stamp));
    fprintf(output, "%-30s: %s", "Finish",ctime(&thread->finish));
#endif
    fprintf(output, "\n");
  }

  print_all_pretty_stats(output, thread, scope, process_walltime_usec);

  /* check if we should be printing rusage and memory stuff here */
  if (rusage) {
    print_rusage_stats(output);
  }
  if (memory) {
    _papiex_dump_memory_info(output);
  }

  print_counters(output, thread);
  return;
}


void monitor_init_library(void) {
  char *err;
  int retval;
  char *opts = NULL, *tmp = NULL;

  /* Init all globals */

  memset(eventnames,0x0,sizeof(char *)*PAPIEX_MAX_COUNTERS);
  memset(eventcodes,0x0,sizeof(int)*PAPIEX_MAX_COUNTERS);
  memset(&all_threads,0x0,sizeof(all_threads));
  eventcnt = 0;
  exeinfo = NULL;
  hwinfo = NULL;
  multiplex = 0;
  _papiex_threaded = 0;
  file_output = NULL;
  file_prefix = 0;
  domain = 0;
  rusage = 0;
  memory = 0;
  quiet = 0;
  no_follow_fork = 0;
  process_name = NULL;
  process_args[0] = '\0';
  fullname = NULL;
  processor = NULL;
  clockrate = 0.0;
  _papiex_debug = 0;
  thread_id_handle = NULL;
  hostname[0] = '\0';
  all_threads_size = PAPIEX_INIT_THREADS;
  
  opts = getenv(PAPIEX_ENV);
  if (opts == NULL)
    {
      LIBPAPIEX_ERROR("Environment variable %s is not defined.",PAPIEX_ENV);
      monitor_real_exit(1);
    }
  opts = strdup(opts);

  tmp = strtok(opts,", ");
  if (tmp) 
    {
      do {
	if (strcmp(tmp,"QUIET") == 0)
	  quiet = 1;
	else if (strcmp(tmp,"DEBUG") == 0)
	  _papiex_debug++;
	else if (strcmp(tmp,"NO_SUMMARY_STATS") == 0)
	  no_summary_stats = 1;
	else if (strcmp(tmp,"MULTIPLEX") == 0)
	  multiplex = 1;
        else if (strncmp(tmp,"MPX_INTERVAL=", 13)==0)
          mpx_interval = atoi(&tmp[13]);
	else if (strcmp(tmp,"NOFORK") == 0)
	  no_follow_fork = 1;
	else if (strcmp(tmp,"RUSAGE") == 0)
	  rusage = 1;
	else if (strcmp(tmp,"MEMORY") == 0)
	  memory = 1;
	else if (strcmp(tmp,"NO_WRITE") == 0)
	  no_write = 1;
	else if (strcmp(tmp,"NO_MPI_GATHER") == 0)
	  no_mpi_gather = 1;
	else if (strcmp(tmp,"NO_SCIENTIFIC") == 0) {
          no_scientific = 1;
        }
	else if (strcmp(tmp,"WRITE_ONLY") == 0)
	  write_only = 1;
	else if (strcmp(tmp,"PREFIX") == 0)
	  {
	    char *t = getenv(PAPIEX_OUTPUT_ENV);

	    if ((t != NULL) && (strlen(t) > 0))
	      {
		file_output = strdup(t);
		file_prefix = 1;
                strcpy(user_path_prefix, t);
	      }
	  }
	else if (strcmp(tmp,"DIR") == 0)
	  {
	    char *t = getenv(PAPIEX_OUTPUT_ENV);

	    if ((t != NULL) && (strlen(t) > 0))
	      {
		sprintf(user_path_prefix, "%s/", t);
                if(mkdir(user_path_prefix, 0755))
                  if (errno != EEXIST) {
                    LIBPAPIEX_ERROR("mkdir(%s) failed. %s", user_path_prefix, strerror(errno));
                    monitor_real_exit(1);
                  }
		file_output = strdup(user_path_prefix);
		file_prefix = 1;
	      }
	  }
	else if (strcmp(tmp,"FILE") == 0)
	  {
	    char *t = getenv(PAPIEX_OUTPUT_ENV);

	    if ((t != NULL) && (strlen(t) > 0))
	      {
		file_output = strdup(t);
                output_file_name = file_output; 
		file_prefix = 0;
#ifdef HAVE_SCPERFUTILS
                suffix = "";  //no automatic prefix
#endif
	      }
	  }
	else if (strcmp(tmp,"USER") == 0)
	  domain |= PAPI_DOM_USER;
	else if (strcmp(tmp,"KERNEL") == 0)
	  domain |= PAPI_DOM_KERNEL;
	else if (strcmp(tmp,"OTHER") == 0)
	  domain |= PAPI_DOM_OTHER;
	else if (strcmp(tmp,"SUPERVISOR") == 0)
	  domain |= PAPI_DOM_SUPERVISOR;
	else
	  {
	    eventnames[eventcnt] = tmp;
	    eventcnt++;
	  }
      } while ((tmp = strtok(NULL,",")) != NULL);
    }

  gethostname(hostname,256);

  if (_papiex_debug)
    {
      retval = PAPI_set_debug(PAPI_VERB_ECONT);
      if (retval != PAPI_OK)
	{
	  LIBPAPIEX_PAPI_ERROR("PAPI_set_debug",retval);
	  return;
	}
    }

  if (_papiex_debug > 4)
    putenv("PAPI_DEBUG=ALL");
  else if (_papiex_debug > 3)
    putenv("PAPI_DEBUG=API:THREADS:INTERNAL:SUBSTRATE");
  else if (_papiex_debug > 2)
    putenv("PAPI_DEBUG=API:SUBSTRATE");
  else if (_papiex_debug > 1)
    putenv("PAPI_DEBUG=API");
  
  if (dlproc_handle == NULL)
    dlproc_handle = monitor_real_dlopen(NULL, RTLD_LAZY);
  if (dlproc_handle == NULL) {
    LIBPAPIEX_ERROR("dlopen(NULL, RTLD_LAZY) failed. %s",  strerror(errno));
    monitor_real_exit(1);
  }

  dlerror();  /* clear existing errors */
  if (dlsym(RTLD_NEXT, "MPI_Comm_rank")) {
    /* This is an MPI program */
    uses_mpi = 1;
#ifdef HAVE_MPI
#ifndef USE_MONITOR_MPI
    /* Let's get a handle on some useful MPI functions */
    dlerror();
    real_MPI_Init = (_MPI_Init_fptr_t)dlsym(RTLD_NEXT, "MPI_Init");
    if ((err = dlerror()) != NULL) {
      LIBPAPIEX_ERROR("dlsym(RTLD_NEXT,MPI_Init) failed. %s", err);
      monitor_real_exit(1);
    }
    /* This function may not be defined by MPI */
    dlerror();
    real_MPI_Init_thread = (_MPI_Init_thread_fptr_t)dlsym(RTLD_NEXT, "MPI_Init_thread");
    if ((err = dlerror()) != NULL) {
      LIBPAPIEX_ERROR("dlsym(RTLD_NEXT,MPI_Init_thread) failed. %s", err);
      monitor_real_exit(1);
    }
    dlerror();  /* clear existing errors */
    real_MPI_Finalize = (_MPI_Finalize_fptr_t)dlsym(RTLD_NEXT, "MPI_Finalize");
    if ((err = dlerror()) != NULL) {
      LIBPAPIEX_ERROR("dlsym(RTLD_NEXT,MPI_Finalize) failed. %s", err);
      monitor_real_exit(1);
    }
// #ifdef MPICH
    dlerror();  /* clear existing errors */
    fptr_MPI_Comm_rank = (_MPI_Comm_rank_fptr_t)dlsym(RTLD_NEXT, "MPI_Comm_rank");
    if ((err = dlerror()) != NULL) {
      LIBPAPIEX_ERROR("dlsym(RTLD_NEXT,MPI_Comm_rank) failed. %s", err);
      monitor_real_exit(1);
    }
    dlerror();  /* clear existing errors */
    fptr_MPI_Comm_size = (_MPI_Comm_size_fptr_t)dlsym(RTLD_NEXT, "MPI_Comm_size");
    if ((err = dlerror()) != NULL) {
      LIBPAPIEX_ERROR("dlsym(RTLD_NEXT,MPI_Comm_size) failed. %s", err);
      monitor_real_exit(1);
    }
#endif /* USE_MONITOR_MPI */

    dlerror();  /* clear existing errors */
    fptr_MPI_Gather = (_MPI_Gather_fptr_t)dlsym(RTLD_NEXT, "MPI_Gather");
    if ((err = dlerror()) != NULL) {
      LIBPAPIEX_ERROR("dlsym(RTLD_NEXT,MPI_Gather) failed. %s", err);
      monitor_real_exit(1);
    }
    dlerror();  /* clear existing errors */
    fptr_MPI_Bcast = (_MPI_Bcast_fptr_t)dlsym(RTLD_NEXT, "MPI_Bcast");
    if ((err = dlerror()) != NULL) {
      LIBPAPIEX_ERROR("dlsym(RTLD_NEXT,MPI_Bcast) failed. %s", err);
      monitor_real_exit(1);
    }
#endif /* HAVE_MPI */
// #endif
  } /* mpi program */

}

void monitor_init_process(char *name, int *argc, char **argv, unsigned pid)
{
  int i;
  LIBPAPIEX_DEBUG("monitor_init_process callback occurred\n");
  for (i=1;i<*argc;i++)
    {
      if (i > 1)
	strcat(process_args," ");
      strcat(process_args,argv[i]);
    }

  papiex_process_init_routine();
}

void monitor_fini_process(void)
{
  LIBPAPIEX_DEBUG("monitor_fini_process callback occurred\n");
  papiex_process_shutdown_routine();
}

void monitor_init_thread_support(void)
{
  int retval;
  LIBPAPIEX_DEBUG("monitor_init_thread_support callback occurred\n");
  retval = PAPI_thread_init((unsigned long (*)(void))monitor_gettid);
  if (retval != PAPI_OK)
    {
      LIBPAPIEX_PAPI_ERROR("PAPI_thread_init",retval);
      return;
    }
  _papiex_threaded = 1;
}

void *monitor_init_thread(unsigned tid)
{
  LIBPAPIEX_DEBUG("monitor_init_thread callback occurred\n");
  papiex_thread_init_routine();
  return(NULL);
}

void monitor_fini_thread(void *data)
{
  LIBPAPIEX_DEBUG("monitor_fini_thread callback occurred\n");
  papiex_thread_shutdown_routine();
}

static void print_banner(FILE *stream) {
  assert(stream);
  assert(tool);
  assert(PAPIEX_VERSION);
  assert(build_date);
  assert(build_time);

  fprintf(stream, "%s:\n%s %s (Build %s/%s, %s)\n%s:\n\n",
                   tool, tool, PAPIEX_VERSION, build_date, 
		   build_time, svn_revision, tool);
  fflush(stream);
}
