#include "papiex_internal.h"

/* Sanity checks */
#ifndef PAPIEX_SHARED_LIBS
#define PAPIEX_SHARED_LIBS "libpapiex.so libmonitor.so libpapi.so"
#endif

/* This is an ordered list with most important to less important from left-to-right
 * In case there's a compatibility issue, events are added from left first */
static const char papi_useful_presets[] ="PAPI_TOT_INS,PAPI_FP_INS,PAPI_LST_INS,PAPI_BR_INS,PAPI_VEC_INS,PAPI_INT_INS,PAPI_FMA_INS,PAPI_LD_INS,PAPI_SR_INS,PAPI_TOT_CYC,PAPI_RES_STL,PAPI_MEM_SCY,PAPI_FP_STAL,PAPI_L1_DCM,PAPI_L1_ICM,PAPI_TLB_DM,PAPI_TLB_IM,PAPI_L2_DCM,PAPI_L2_ICM,PAPI_CA_INV,PAPI_CSR_FAL,PAPI_CSR_TOT,PAPI_LSU_IDL,PAPI_FPU_IDL,PAPI_STL_ICY,PAPI_FUL_ICY,PAPI_BR_CN,PAPI_BR_MSP,PAPI_FP_OPS,PAPI_L1_DCA,PAPI_L2_DCA,PAPI_L2_ICA";

static const char ENV_PAPIEX_LD_LIBPATH[] = "PAPIEX_LD_LIBRARY_PATH";

static int debug = 0;
static int shell_dump = 0;
static int is_papiex = 1;
static int no_ld_library_path = 0; /* should we modify LD_LIBRARY_PATH ? */
static int no_summary_stats = 0;
static int no_mpi_gather = 0;
static int no_scientific = 0; 
static int multiplex = 0;
static int auto_stats = 0;
static int new_ld_paths_added = 0;
const PAPI_hw_info_t *hwinfo = NULL;
/* Global used by error messages */
char *tool = "papiex";

/* PAPIEX-specific */
static const struct option long_options[] = {
         {"no-ld-path", 0, &no_ld_library_path, 1},
         {"no-summary", 0, &no_summary_stats, 1},
         {"no-gather", 0, &no_mpi_gather, 1},
         {"no-scientific", 0, &no_scientific, 1},
	 {"help", 0, NULL, 'h'},
         {0, 0, 0, 0}
};

static void dump_substrate_info (FILE *);
static void get_useful_events (char *events, int maxlen);
static int test_papi_events(const char* events);
static int make_compatible_eventset(char* events);

static inline void append_option(char *opt_str, const char* option) {
  if (strlen(opt_str))
    strcat(opt_str, ",");
  strcat(opt_str, option);
  return;
}

/* COMMON PAPI OPTION HANDLING */

void do_i_option(const PAPI_hw_info_t *hwi)
{
  printf ("%-30s: %s (%d)\n","Vendor string and code",
	  hwi->vendor_string, hwi->vendor);
  printf ("%-30s: %s (%d)\n","Model string and code",
	  hwi->model_string, hwi->model);
  printf ("%-30s: %f\n","CPU Megahertz", hwi->mhz);
  printf ("%-30s: %d\n","Clock Megahertz", hwi->clock_mhz);
  printf ("%-30s: %d\n","Total # of CPU's",
	  hwi->totalcpus);
  printf ("%-30s: %d\n","Number Hardware Counters",
	  PAPI_get_opt (PAPI_MAX_HWCTRS, NULL));
  printf ("%-30s: %d\n","Max Multiplex Counters",
	  PAPIEX_MAX_COUNTERS);
  dump_substrate_info (stdout);
}

/* If NULL, list-em-all */

void do_L_option(const PAPI_hw_info_t *hwi, const char *event)
{
  int eventcode, retval;
  
  if (event)
    {
      retval = PAPI_event_name_to_code ((char *)event, &eventcode);
      if (retval != PAPI_OK)
	PAPIEX_PAPI_ERROR ("PAPI_event_name_to_code", retval);
      
       _papiex_dump_event_info (stdout, eventcode, 1);
    }
  else
    {
      int i;
      PAPI_event_info_t info;
      printf ("Preset events:\n\n");
      printf ("Preset                         Description\n");

      i = 0 | PAPI_PRESET_MASK;
      PAPI_enum_event(&i, PAPI_ENUM_FIRST);
      do
	{
	  if (PAPI_get_event_info (i, &info) == PAPI_OK)
	    {
	      if (info.count)
		printf ("%-30s %s\n", info.symbol,
			info.long_descr);
	    }
	}
      while (PAPI_enum_event (&i, PAPI_PRESET_ENUM_AVAIL) == PAPI_OK);

      printf ("\nNative events:\n\n");
      printf ("Native                         Description\n");

      i = 0 | PAPI_NATIVE_MASK;
      PAPI_enum_event(&i, PAPI_ENUM_FIRST);
      do {
	      if (PAPI_get_event_info (i, &info) == PAPI_OK)
		{
		  printf ("%-30s %s\n", info.symbol,
			  info.long_descr);
		}
	    }
	  while (PAPI_enum_event (&i, PAPI_ENUM_EVENTS) == PAPI_OK);
    }
}

void do_V_option(const char *name, int version)
{
  printf
    ("%s version %s, PAPI library version %u.%u.%u, PAPI header version %u.%u.%u\n",name,
     PAPIEX_VERSION, PAPI_VERSION_MAJOR (version),
     PAPI_VERSION_MINOR (version),
     PAPI_VERSION_REVISION (version),
     PAPI_VERSION_MAJOR (PAPI_VERSION),
     PAPI_VERSION_MINOR (PAPI_VERSION),
     PAPI_VERSION_REVISION (PAPI_VERSION));
}

void do_s_option(void)
{
  shell_dump = 1;
}

static int handle_common_opts(char c, const char* optarg, char **my_argv, int version) {
  int found = 1;
  switch(c) {
    case ':':
    case '?':
      {
        PAPIEX_ERROR ("Try '%s -h' for more information.\n",tool);
        exit (1);
        break;
      }

    case 'i':
      {
        do_i_option(hwinfo);
        exit (0);
        break;
      }

    case 'V':
      {
        do_V_option(my_argv[0],version);
        exit (0);
        break;
      }

    case 's':
      {
        do_s_option();
        break;
      }

    case 'L':
      {
        do_L_option(hwinfo,optarg);
        exit (0);
        break;
      }

    case 'l':
      {
        do_L_option(hwinfo,NULL);
        exit (0);
        break;
      }
    default:
      found = 0;
  }
  return found;
}


void print_common_opts(void)
{
  printf (" -d\t\tEnable debugging output, use repeatedly for more output.\n");
  printf (" -h\t\tPrint this message.\n");
  printf (" -i\t\tPrint information about the host machine.\n");
  printf (" -s\t\tDump the shell environment variables and exit.\n");
  printf (" -V\t\tPrint version information.\n");
}

void print_common_output_opts(void)
{
  printf (" -q\t\tQuiet mode; Suitable for parsing with other tools.\n");
  printf (" -n\t\tDo not write to files, write to stderr. Opposite of -w\n");
  printf (" -w\t\tDo not write to stderr, write to files. Opposite of -n\n");
  printf ("\n\t\t*** The options -n and -w are mutually exclusive. ***\n\n");
  printf (" -f <dir>\tSend output to <dir>/<cmd>.%s.<host>.<pid>.<generation>\n", tool);
  printf (" -p <prefix>\tSend output to <prefix><cmd>.%s.<host>.<pid>.<generation>.\n", tool);
  printf (" -o <file>\tSend output to <file>.  If <file> exists, a suffix will\n"
  	  "\t\tbe appended, and the old file will not be overwritten.\n"
          "\t\tIt is recommended that -o is NOT used for runs where multiple\n"
	  "\t\tfiles are created (use -f instead in such situations).\n");
  printf ("\n\t\t*** The options -n, -p, -f and -o are mutually exclusive. ***\n\n");
}

void print_common_papi_opts(void)
{
  printf (" -e event\tMonitor this event. May be repeated.\n");
  printf (" -l\t\tPrint a terse listing of all the available events to count.\n");
  printf (" -L event\tPrint a full description of this event.\n");
  printf (" -m<interval>\tEnable counter multiplexing. Silently ignored if unavailable.\n"
          "\t\tThe 'interval' is in Hz, and is optional (defaults to 10).\n");
  printf (" -U\t\tMonitor user mode events (default).\n");
  printf (" -K\t\tMonitor kernel mode events\n");
  printf (" -I\t\tMonitor transient (interrupt) mode events.\n");
  printf (" -S\t\tMonitor supervisor mode events.\n");
}

void print_copyright(void)
{
  printf ("\n"
          "This is OPEN SOURCE SOFTWARE written by Philip J. Mucci and Tushar Mohan\n"
          "on behalf of The University of Tennessee, Innovative Computing Laboratory\nand Samara Technology Group LLC. See http://icl.cs.utk.edu and http://www.samaratechnologygroup.com.\n"
          "For license and copying information see the file COPYING in the source.\n"
          "\nPlease send all bug reports to: ospat-devel@cs.utk.edu\n");
}

/* DO NOT SET ENVIRONMENT VARIABLES IN THIS ROUTINE, THAT HAPPENS IN MAIN */

static char *
parse_args (int my_argc, char **my_argv, char **cmd, char ***cmd_argv,
	    char *preload_env, char *preload_sep, char *ldpath_env, char *ldpath_sep, char *outstr)
{
  int c; 
  char events[PATH_MAX] = "";
  static char option_string[PATH_MAX] = "";
  int version = 0;
  PAPI_option_t pl;

  tool = my_argv[0];

  PAPI_set_debug(PAPI_VERB_ECONT);
  version = PAPI_library_init (PAPI_VER_CURRENT);
  if (version != PAPI_VER_CURRENT)
    PAPIEX_PAPI_ERROR ("PAPI_library_init", version);
  PAPI_set_debug(PAPI_QUIET);

  PAPI_get_opt (PAPI_PRELOAD, &pl);
  strcpy (preload_env, pl.preload.lib_preload_env);
  *preload_sep = pl.preload.lib_preload_sep;
  strcpy (ldpath_env, pl.preload.lib_dir_env);
  *ldpath_sep = pl.preload.lib_dir_sep;

  if ((hwinfo = PAPI_get_hardware_info ()) == NULL)
    PAPIEX_PAPI_ERROR ("PAPI_query_all_events_verbose", 0);

  if (my_argc)
    {
      while (1)
	{
	  /* (is_papiex) */
	  c = getopt_long (my_argc, my_argv, "+dhisVqnwf:p:o:e:lL:m::UKISarx", // M::, 
                             long_options, NULL);

	  if (c == -1)
	    break;

	  if (handle_common_opts(c, optarg, my_argv, version))
	    continue; // if we found a matching option continue

	  if (is_papiex)
	    {
	      switch (c)
		{
		case 'd':
		  append_option (option_string, "DEBUG");
		  PAPI_set_debug(PAPI_VERB_ECONT);
		  debug++;
		  break;
		case 'h':
		  printf
		    ("Usage:\n"
		     "papiex [-dhisVqnwlUKISarx] [-f output-dir] [-p prefix] [-o output-file]\n"
                     "       [-e papi_event] [-m<interval>] [-L papi_event]\n"
		     "       [--no-summary] [--no-scientific] [--no-ld-path] [--no-gather]\n"
                     "       -- <cmd> <cmd options>\n\n");
		  print_common_opts();
		  print_common_output_opts();
		  print_common_papi_opts();
		  printf (" -a\t\tMonitor 'useful' events automatically (-m and -x are implicit).\n");
		  printf (" -r\t\tReport getrusage() information.\n");
		  printf (" -x\t\tReport memory usage information.\n");
		  printf (" --no-summary\tDo not generate any summary statistics (threads/MPI).\n");
		  printf (" --no-scientific Do not print output numbers in scientific notation.\n");
		  printf (" --no-ld-path\tDo not modify the LD_LIBRARY_PATH variable at all.\n");
		  printf (" --no-gather\tDo not use MPI to gather the data to the front end.\n");
		  printf("\nThe runtime loader MUST find %s.\n"
                           "Set LD_LIBRARY_PATH appropriately if necessary.\n", PAPIEX_SHARED_LIBS);
		  print_copyright();
		  exit (0);
		  break;
                case 0: 
                  /* do nothing; long option */
                  break;
		case 'q':
		  if (strstr (option_string, "RUSAGE"))
		    {
		      PAPIEX_ERROR ("-r and -q are not compatible.\n");
		      exit (1);
		    }
		  append_option (option_string, "QUIET");
		  break;
		case 'm':
		  append_option (option_string, "MULTIPLEX");
		  multiplex = 1;
                  if (optarg != NULL) {
                    int val=strtol(optarg, (char **)NULL, 10);
                    if ((errno == ERANGE) || (val <= 0)) {
                      PAPIEX_ERROR("The argument to '-m' must be a positive integer\n");
                      exit(1);
                    }
                    char interval[25];
                    sprintf(interval, "MPX_INTERVAL=%d", val);
                    append_option(option_string, interval);
                  }
		  break;
		case 'r':
		  if (strstr (option_string, "QUIET"))
		    {
		      PAPIEX_ERROR ("-r and -q are not compatible.\n");
		      exit (1);
		    }
		  append_option (option_string, "RUSAGE");
		  break;
		case 'x':
		  if (strstr (option_string, "QUIET"))
		    {
		      PAPIEX_ERROR ("-x and -q are not compatible.\n");
		      exit (1);
		    }
		  append_option (option_string, "MEMORY");
		  break;
		case 'n':
		    if (strstr (option_string, "DIR") || strstr(option_string, "FILE") || strstr(option_string, "PREFIX") ||
                        strstr (option_string, "WRITE_ONLY"))
		      {
			PAPIEX_ERROR ("-n is not compatible with -f, -o, -p or -w.\n");
			exit (1);
		      }
		  append_option (option_string, "NO_WRITE");
		  break;
		case 'w':
		  if (strstr (option_string, "NO_WRITE"))
		    {
		      PAPIEX_ERROR ("-w and -n are not compatible.\n");
		      exit (1);
		    }
		  append_option (option_string, "WRITE_ONLY");
		  break;
		case 'U':
		  append_option (option_string, "USER");
		  break;
		case 'K':
		  append_option (option_string, "KERNEL");
		  break;
		case 'I':
		  append_option (option_string, "OTHER");
		  break;
		case 'S':
		  append_option (option_string, "SUPERVISOR");
		  break;
		case 'p':
		  {
		    if (strstr (option_string, "DIR") || strstr(option_string, "FILE") || strstr(option_string, "NO_WRITE"))
		      {
			PAPIEX_ERROR ("-p is not compatible with -f, -o and -n.\n");
			exit (1);
		      }
		    if (optarg == NULL)
		      {
			append_option (option_string, "PREFIX");
		      }
		    else
		      {
			append_option (option_string, "PREFIX");
			strncpy (outstr, optarg, PATH_MAX);
		      }
		  }
		  break;
		case 'f':
		  {
		    if (strstr (option_string, "PREFIX") || strstr(option_string, "FILE") || strstr(option_string, "NO_WRITE"))
		      {
			PAPIEX_ERROR ("-f is not compatible with -n, -p and -o.\n");
			exit (1);
		      }
		    append_option (option_string, "DIR");
		    strncpy (outstr, optarg, PATH_MAX);
		  }
		  break;
		case 'o':
		  {
		    if (strstr (option_string, "PREFIX") || strstr(option_string, "DIR") || strstr(option_string, "NO_WRITE"))
		      {
			PAPIEX_ERROR ("-o is not compatible with -n, -f and -p.\n");
			exit (1);
		      }
		    append_option (option_string, "FILE");
		    strncpy (outstr, optarg, PATH_MAX);
		  }
		  break;
		case 'a':
		  {
                    append_option (option_string, "MULTIPLEX");	/* multiplexing is implicit with -a */
                    append_option (option_string, "MEMORY");	/* memory is implicit with -a */
                    multiplex = 1;
                    auto_stats = 1;
		    char useful_events[PATH_MAX];
		    useful_events[0] = '\0';

		    get_useful_events (useful_events, PATH_MAX);
		    if (strlen (useful_events))
		      {
			append_option(events, useful_events);
		     }
		    break;
		  }
		case 'e':
		  {
		    append_option(events,optarg);
		    break;
		  }
		default:
		  error:
		    PAPIEX_ERROR ("Try `%s -h' for more information.\n", my_argv[0]);
		    exit (1);
		  }
		}
	    }			/*papiex */
	}

  /* Test PAPI events for sanity and availability */
  /* Add the fixed up events to the list of options */
  if (strlen(events)) {
    int rc = test_papi_events(events);
    if (auto_stats && (rc != PAPI_OK))
      make_compatible_eventset(events);
    
  append_option (option_string, events);
  }

  /* Process the long options */
  
  if (no_summary_stats) {
    append_option(option_string, "NO_SUMMARY_STATS");
  }

#ifndef HAVE_MPI
  no_mpi_gather = 1;
#endif

  if (no_mpi_gather) {
    append_option(option_string, "NO_MPI_GATHER");
  }

  if (no_scientific) {
    append_option(option_string, "NO_SCIENTIFIC");
  }

  /* Construct argv for the fork/exec pair later on. */

  if (shell_dump == 0)
    {
      char **tmp_my_argv;
      int i;

      if (my_argv[optind] == NULL)
	{
	  PAPIEX_WARN ("no command given\n");
	  goto error;
	}

      tmp_my_argv =
	(char **) malloc ((my_argc - optind + 1) * sizeof (char *));
      for (i = optind; i < my_argc; i++)
	tmp_my_argv[i - optind] = strdup (my_argv[i]);
      tmp_my_argv[i - optind] = NULL;
      *cmd_argv = tmp_my_argv;
      *cmd = strdup (tmp_my_argv[0]);
    }

  PAPI_shutdown ();

  return (option_string);
}

static char *stringify_domain(int domain)
{
  static char output[256];
  output[0] = '\0';
  if (domain & PAPI_DOM_USER)
    strcat(output,"User");
  if (domain & PAPI_DOM_KERNEL)
    {
      append_option(output,"Kernel");
    }
  if (domain & PAPI_DOM_OTHER)
    {
      append_option(output,"Other");
    }
  if (domain & PAPI_DOM_SUPERVISOR)
    {
      append_option(output,"Supervisor");
    }
  return(output);
}

static char *stringify_all_domains(int domains)
{
  static char buf[PAPI_HUGE_STR_LEN];
  int i, did = 0;
  buf[0] = '\0';

  for (i=PAPI_DOM_MIN;i<=PAPI_DOM_MAX;i=i<<1)
    if (domains&i)
      {
        if (did)
          strcpy(buf+strlen(buf),"|");
        strcpy(buf+strlen(buf),stringify_domain(domains&i));
        did++;
      }
  return(buf);
}

static char *stringify_granularity(int granularity)
{
   switch (granularity) {
   case PAPI_GRN_THR:
      return ("PAPI_GRN_THR");
   case PAPI_GRN_PROC:
      return ("PAPI_GRN_PROC");
   case PAPI_GRN_PROCG:
      return ("PAPI_GRN_PROCG");
   case PAPI_GRN_SYS_CPU:
      return ("PAPI_GRN_SYS_CPU");
   case PAPI_GRN_SYS:
      return ("PAPI_GRN_SYS");
   default:
     return("Unrecognized granularity!");
   }
   return (NULL);
}

static char *stringify_all_granularities(int granularities)
{
  static char buf[PAPI_HUGE_STR_LEN];
  int i, did = 0;

  buf[0] = '\0';
  for (i=PAPI_GRN_MIN;i<=PAPI_GRN_MAX;i=i<<1)
    if (granularities&i)
      {
        if (did)
          strcpy(buf+strlen(buf),"|");
        strcpy(buf+strlen(buf),stringify_granularity(granularities&i));
        did++;
      }

  return(buf);
}

/* This function presently adds available, PAPI preset events
 * to 'events'. For "known" architectures we could do better
 * and add native events in the future
 * 'events' can point to some pre-existing events, so please append
 * to it! Also note that the length of events string is maxlen.
 */
static void get_useful_events(char *events, int maxlen) {
  int len = strlen(events);

  /* Default, for unknown architectures */
  if (strlen(papi_useful_presets)+len >= maxlen-1) {
    PAPIEX_ERROR("Cannot fit all the useful events in the buffer\n"
                 "Please allocate a bigger buffer!\n");
    exit(2);
  } 
  append_option(events, papi_useful_presets);
  return;
}

static void dump_substrate_info(FILE *output) 
{
#if (PAPI_VERSION_MAJOR(PAPI_VERSION) == 3)
  const PAPI_substrate_info_t *ss_info;
  ss_info = PAPI_get_substrate_info();
#else
  const PAPI_component_info_t *ss_info;
  ss_info = PAPI_get_component_info(0);
#endif
  if (ss_info != NULL) {
    if (ss_info->name != NULL && strlen(ss_info->name))
      fprintf(output, "%-30s: %s\n", "Substrate name",ss_info->name);

    if (ss_info->version != NULL && strlen(ss_info->version))
      fprintf(output, "%-30s: %s\n", "Substrate version",ss_info->version);

    if (ss_info->support_version != NULL && strlen(ss_info->support_version))
      fprintf(output, "%-30s: %s\n", "Support lib version",ss_info->support_version);

    if (ss_info->kernel_version != NULL && strlen(ss_info->kernel_version))
      fprintf(output, "%-30s: %s\n", "Kernel driver version",ss_info->kernel_version);
    fprintf(output, "%-30s: %d\n","Num. counters",ss_info->num_cntrs);
    fprintf(output, "%-30s: %d\n","Num. preset events",ss_info->num_preset_events);
    fprintf(output, "%-30s: 0x%x (%s)\n","Default domain",ss_info->default_domain,stringify_all_domains(ss_info->default_domain)); 
    fprintf(output, "%-30s: 0x%x (%s)\n","Available domains",ss_info->available_domains,stringify_all_domains(ss_info->available_domains));
    fprintf(output, "%-30s: 0x%x (%s)\n","Default granularity",ss_info->default_granularity,stringify_granularity(ss_info->default_granularity));
    fprintf(output, "%-30s: 0x%x (%s)\n","Available granularities",ss_info->available_granularities,stringify_all_granularities(ss_info->available_granularities));
#ifdef PAPI_DEF_MPX_NS

#else
    fprintf(output, "%-30s: %d\n","Multiplex timer sig",ss_info->multiplex_timer_sig); 
    fprintf(output, "%-30s: %d\n","Multiplex timer num",ss_info->multiplex_timer_num);
#endif
    fprintf(output, "%-30s: %d\n","Hardware intr sig",ss_info->hardware_intr_sig);  
    fprintf(output, "%-30s: %s\n","Hardware intr",ss_info->hardware_intr==0 ? "No" : "Yes");
    fprintf(output, "%-30s: %s\n","Precise intr",ss_info->precise_intr==0 ? "No" : "Yes"); 
    fprintf(output, "%-30s: %s\n","Posix1b timers",ss_info->posix1b_timers==0 ? "No" : "Yes");
    fprintf(output, "%-30s: %s\n","Kernel profile",ss_info->kernel_profile==0 ? "No" : "Yes");
    fprintf(output, "%-30s: %s\n","Kernel multiplex",ss_info->kernel_multiplex==0 ? "No" : "Yes");
    fprintf(output, "%-30s: %s\n","Fast counter read",ss_info->fast_counter_read==0 ? "No" : "Yes"); 
    fprintf(output, "%-30s: %s\n","Fast real timer",ss_info->fast_real_timer==0 ? "No" : "Yes");
    fprintf(output, "%-30s: %s\n","Fast virtual timer",ss_info->fast_virtual_timer==0 ? "No" : "Yes");
  }
  else {
    fprintf(output, "Error obtaining substrate information from PAPI\n"
            "PAPI_get_substrate_info returned NULL\n");
  }
  return; 
}

/* events is a comma-separated list of PAPI events */
static int test_papi_events(const char *events) {
  PAPIEX_DEBUG("Verifying %s for availability and compatibility\n", events);

  int retval = 0;
  static char *eventnames[PAPIEX_MAX_COUNTERS];
  static int eventcodes[PAPIEX_MAX_COUNTERS];

  memset(eventnames,0x0,sizeof(char *)*PAPIEX_MAX_COUNTERS);
  memset(eventcodes,0x0,sizeof(int)*PAPIEX_MAX_COUNTERS);
  char events2[PATH_MAX];
  strcpy(events2, events);
  int i = 0;
  char *tmp;
  if (strlen(events)) {
    tmp = strtok(events2, ",");
    do {
      int eventcode;
      retval = PAPI_event_name_to_code(tmp,&eventcode);
      if (retval != PAPI_OK) {
        if (!auto_stats) {
          PAPIEX_WARN("Could not map event to code for %s\n", tmp);
          PAPIEX_PAPI_ERROR("PAPI_event_name_to_code",retval);
        }
        return retval;
      }
      eventnames[i] = strdup(tmp);
      eventcodes[i] = eventcode;
      i++;
      if (i == PAPIEX_MAX_COUNTERS)
	PAPIEX_ERROR("Exceeded maximum number of events (%d).\n",PAPIEX_MAX_COUNTERS);
    } while ((tmp=strtok(NULL, ",")));

    int eventcnt = i;
    int eventset = PAPI_NULL;
    retval = PAPI_create_eventset(&eventset);
    if (retval != PAPI_OK) {
      if (!auto_stats)
        PAPIEX_PAPI_ERROR("PAPI_create_eventset",retval);
      return retval;
    }

    if (multiplex) {
#if (PAPI_VERSION_MAJOR(PAPI_VERSION) > 3)
      retval = PAPI_assign_eventset_component(eventset, 0);
      if (retval != PAPI_OK) {
        PAPIEX_PAPI_ERROR("PAPI_assign_eventset_component",retval);
        return retval;
      }
#endif
      PAPIEX_DEBUG("Enabling multiplexing in the PAPI library\n");
      retval = PAPI_multiplex_init();
      if (retval != PAPI_OK) {
        if (!auto_stats)
          PAPIEX_PAPI_ERROR("PAPI_multiplex_init",retval);
        return retval;
      }
      retval = PAPI_set_multiplex(eventset);
      if (retval != PAPI_OK) {
        if (!auto_stats)
          PAPIEX_PAPI_ERROR("PAPI_set_multiplex",retval);
        return retval;
      }
    }
    retval = PAPI_add_events(eventset, eventcodes, eventcnt);
    if (retval < PAPI_OK) {
      if (!auto_stats)
        PAPIEX_PAPI_ERROR("PAPI_add_events",retval);
      return retval;
    }
    if (retval != PAPI_OK) {
      if (!auto_stats) {
        char str[PAPI_MAX_STR_LEN];
        sprintf(str,"PAPI_add_events(%s)",eventnames[retval]);
        PAPIEX_PAPI_ERROR(str,PAPI_add_event(eventset,eventcodes[retval]));
      }
      return retval;
    }
    if (retval==PAPI_OK) 
      PAPIEX_DEBUG("Verified events for compatibility: %s\n", events);
  }
  return (retval);
}

/* events is a comma-separated list of PAPI events */
/* It CAN be modified to produce a set of compatible events */
static int make_compatible_eventset(char *events) {
  PAPIEX_DEBUG("Making a set of compatible events from:  %s\n", events);

  int retval = 0;
  static char *eventnames[PAPIEX_MAX_COUNTERS];
  static int eventcodes[PAPIEX_MAX_COUNTERS];

  memset(eventnames,0x0,sizeof(char *)*PAPIEX_MAX_COUNTERS);
  memset(eventcodes,0x0,sizeof(int)*PAPIEX_MAX_COUNTERS);
  char events2[PATH_MAX];
  char comp_events[PATH_MAX] = "";
  strcpy(events2, events);
  int i = 0;
  char *tmp;
  if (multiplex) {
    PAPIEX_DEBUG("Enabling multiplexing in the PAPI library\n");
    retval = PAPI_multiplex_init();
    if (retval != PAPI_OK)
      PAPIEX_PAPI_ERROR("PAPI_multiplex_init",retval);
  }

  if (strlen(events2)) {
    tmp = strtok(events2, ",");
    do {
      int eventcode;
      retval = PAPI_event_name_to_code(tmp,&eventcode);
      if (retval != PAPI_OK) {
        PAPIEX_DEBUG("Could not map event to code for %s. Skipping it\n", tmp);
        continue;
      }
      if (PAPI_query_event(eventcode) != PAPI_OK) {
        PAPIEX_DEBUG("Event %s cannot be counted on this arch, skipping it\n", tmp);
        continue;
      }

      eventnames[i] = strdup(tmp);
      eventcodes[i] = eventcode;
      i++;
      int eventcnt = i;
      int eventset = PAPI_NULL;
      retval = PAPI_create_eventset(&eventset);
      if (retval != PAPI_OK)
        PAPIEX_PAPI_ERROR("PAPI_create_eventset",retval);

      if (multiplex) {
#if (PAPI_VERSION_MAJOR(PAPI_VERSION) > 3)
        retval = PAPI_assign_eventset_component(eventset, 0);
        if (retval != PAPI_OK) {
          PAPIEX_PAPI_ERROR("PAPI_assign_eventset_component",retval);
          return retval;
        }
#endif
        retval = PAPI_set_multiplex(eventset);
        if (retval != PAPI_OK)
          PAPIEX_PAPI_ERROR("PAPI_set_multiplex",retval);
      }
      retval = PAPI_add_events(eventset, eventcodes, eventcnt);
      if (retval != PAPI_OK) {
        PAPIEX_DEBUG("Could not add event %s, so skipping it\n", tmp);
        i--;
      }
      else {
        append_option(comp_events, tmp);
      }
      PAPI_cleanup_eventset(eventset);
      PAPI_destroy_eventset(&eventset);
    } while ((tmp=strtok(NULL, ",")));

    strcpy(events, comp_events);
    if (retval==PAPI_OK)
      PAPIEX_DEBUG("Verified events for compatibility: %s\n", events);
  }
  return (retval);
}

void dump_shell_vars(char *optstr, char *outstr, char *preload_env, char *preload_val, char *ldpath_env, char *ldpath_val)
{
  char *basesh;
  char *sh = getenv ("SHELL");
  int not_csh = 0;
  
  if ((sh == NULL) || (strlen (sh) == 0))
    {
    bail:
      PAPIEX_ERROR ("Error: no valid SHELL environment variable\n");
      exit (1);
    }
  basesh = basename (sh);
  if ((basesh == NULL) || (strlen (basesh) == 0))
    goto bail;
  
  if ((strcmp (basesh, "bash") == 0) || (strcmp (basesh, "sh") == 0) ||
      (strcmp (basesh, "zsh") == 0) || (strcmp (basesh, "ksh") == 0) ||
      (strcmp (basesh, "ash") == 0))
    not_csh = 1;
  
  if (getenv(PAPIEX_DEFAULT_ARGS)) 
    printf ("%s %s%s\"%s\";\n", (not_csh ? "export" : "setenv"), PAPIEX_DEFAULT_ARGS,
	    (not_csh ? "=" : " "), getenv(PAPIEX_DEFAULT_ARGS));

  if (getenv(PAPIEX_ENV)) 
    printf ("%s %s%s%s;\n", (not_csh ? "export" : "setenv"), PAPIEX_ENV,
	    (not_csh ? "=" : " "), optstr);

  if (getenv(PAPIEX_OUTPUT_ENV))
    printf ("%s %s%s%s;\n", (not_csh ? "export" : "setenv"),
	    PAPIEX_OUTPUT_ENV, (not_csh ? "=" : " "), outstr);
  
  if (getenv("PAPIEX_DEBUG"))
    printf ("%s %s%s%s;\n", (not_csh ? "export" : "setenv"),
	    "PAPIEX_DEBUG", (not_csh ? "=" : " "), "1");

  if (getenv("MONITOR_DEBUG"))
    printf ("%s %s%s%s;\n", (not_csh ? "export" : "setenv"),
	    "MONITOR_DEBUG", (not_csh ? "=" : " "), "1");

  if (getenv("MONITOROPTIONS"))
    printf ("%s %s%s%s;\n", (not_csh ? "export" : "setenv"),
	    "MONITOROPTIONS", (not_csh ? "=" : " "), "1");
  
  if (getenv(preload_env))
    printf ("%s %s%s\"%s\";\n", (not_csh ? "export" : "setenv"),
	    preload_env, (not_csh ? "=" : " "), preload_val);

  if (getenv(ldpath_env))
    printf ("%s %s%s%s;\n", (not_csh ? "export" : "setenv"), ldpath_env,
	    (not_csh ? "=" : " "), getenv(ldpath_env));
}

int
main (int argc, char **argv)
{
  static char tmpstr[PATH_MAX] = "";
  static char tmpstr2[PATH_MAX] = "";
  static char outstr[PATH_MAX] = "";
  char papilib[PATH_MAX] = "";
  char monitorlib[PATH_MAX] = "";
  char preload_env[PATH_MAX] = "LD_PRELOAD";
  char preload_sep = ' ';
  char ldpath_env[PATH_MAX] = "LD_LIBRARY_PATH";
  char ldpath_sep = ':';
  char *cmd = NULL;
  char **cmd_argv = NULL;
  char *optstr = NULL;
  char new_ld_path[PATH_MAX];

  if (strcmp (basename (argv[0]), "papiex") == 0) {
    is_papiex = 1;
  }

  /* Add default arguments if any */
  int myargc = argc;
  char** myargv = argv;
  char* default_args = getenv(PAPIEX_DEFAULT_ARGS);
  if ((strcmp(basename(argv[0]), "papiex")==0) && default_args) {
    myargv = malloc(sizeof(char *)*256);
    if (myargv == NULL) {
      fprintf(stderr, "malloc failed. Quitting now!\n");
      exit(1);
    }
    myargv[0] = argv[0];
    myargc = 1;
    char *tmp =strtok(strdup(default_args), " ");
    do {
      myargv[myargc] = tmp;
      fprintf(stderr, "papiex: prepending option %s\n", tmp);
      myargc++;
    } while ((tmp=strtok(NULL," ")));
    int i;
    for (i=1;i<argc;i++)
      myargv[myargc++] = argv[i];
    myargv[myargc] = (char*) NULL;
  }
  optstr =
    parse_args (myargc, myargv, &cmd, &cmd_argv, preload_env, &preload_sep, ldpath_env, &ldpath_sep,
		outstr);
  if ((optstr == NULL) || (strlen (optstr) == 0))
    sprintf (tmpstr, "%s= ", PAPIEX_ENV);
  else
    sprintf (tmpstr, "%s=%s", PAPIEX_ENV, optstr);

#ifdef HAVE_MONITOR
  sprintf (monitorlib, "libmonitor.so");
#endif

  sprintf (papilib, "libpapi.so%c", preload_sep);

  /* papiex lib */
  if (strlen (tmpstr2))
    strncat (tmpstr2, &preload_sep, 1);
  strcat(tmpstr2, "libpapiex.so");

  /* monitor lib */
  if (strlen (tmpstr2))
    strncat (tmpstr2, &preload_sep, 1);
  strcat(tmpstr2, monitorlib);

  /* was there already something to be preloaded? */
  if (getenv(preload_env)) {
    if (strlen (tmpstr2))
      strncat (tmpstr2, &preload_sep, 1);
    strcat(tmpstr2, getenv(preload_env));
  }

  /* Do we need to modify LD_LIBRARY_PATH? */
  if (!no_ld_library_path) {
     new_ld_path[0] = '\0';
#ifdef PAPIEX_LD_LIBRARY_PATH
     sprintf(new_ld_path, "%s", PAPIEX_LD_LIBRARY_PATH);
     new_ld_paths_added = 1;
#endif
    
    /* if the environment variable PAPIEX_LD_LIBRARY_PATH is
     * is set, then that takes precedence,
     */
     char *papiex_ld_path_env = getenv(ENV_PAPIEX_LD_LIBPATH);
     if (papiex_ld_path_env != NULL) {
       sprintf(new_ld_path, "%s", papiex_ld_path_env);
       new_ld_paths_added = 1;
     }

     /* We prepend the path we just created to the LD_LIBRARY_PATH
      * if it's already set. Otherwise, we initialize it
      */
      if (strlen(new_ld_path)) {
        if (getenv(ldpath_env)) {
          strncat(new_ld_path, &ldpath_sep, 1);
          strcat(new_ld_path, getenv(ldpath_env));
        }
        setenv (ldpath_env, new_ld_path, 1);
      }
  } /* no_ld_library_path */

  if (strlen(tmpstr2))
    setenv (preload_env, tmpstr2, 1);
  if (is_papiex) {
    setenv (PAPIEX_ENV, optstr, 1);
    setenv (PAPIEX_OUTPUT_ENV, outstr, 1);
  }
  if (debug > 1) {
    setenv ("MONITOR_DEBUG","1",1);
    /* What the hell? -pjm */
    setenv ("SCPERFUTILS_DEBUG","1",1);
  }
  setenv ("MONITOR_OPTIONS","SIGINT",1);
  
  if ((debug >= 1) || (shell_dump))
    dump_shell_vars(optstr,outstr,preload_env,tmpstr2,ldpath_env,new_ld_path);

  if (shell_dump)
    exit(0);

  if (fork () == 0)
    {
      if (execvp (cmd, cmd_argv) == -1)
	{
          /* check if adding "." to PATH fixes this? */
          char path[PATH_MAX] = ".";
          if (getenv("PATH"))
            sprintf(path, "%s%c.", getenv("PATH"), ldpath_sep);
          setenv("PATH", path, 1);
          if (execvp (cmd, cmd_argv) == -1) {
	    PAPIEX_ERROR ("Error exec'ing %s: %s\n", cmd, strerror (errno));
	    exit (1);
          }
	}
      exit(0);
    }
  else
    {
      int status = 0;
      wait (&status);
      exit (WEXITSTATUS (status));
    }
}
