#ifndef PAPIEX_INTERNAL_H
#define PAPIEX_INTERNAL_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <memory.h>
#include <getopt.h>
#include <string.h>
#include <limits.h>
#include <dlfcn.h>
#include <time.h>
#include <errno.h>
#include <search.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>

#ifdef HAVE_MONITOR
#include "monitor.h"
#endif

#include "papiex.h"

#ifdef DEBUG
#define PAPIEX_DEBUG_THREADID (unsigned long)((_papiex_threaded ? PAPI_thread_id() : 0))
#define LIBPAPIEX_DEBUG(...) { if (_papiex_debug) { fprintf(stderr,"libpapiex debug: %lu,0x%lx,%s ",(unsigned long)getpid(),PAPIEX_DEBUG_THREADID,__func__); fprintf(stderr, __VA_ARGS__); } }
#define PAPIEX_DEBUG(...) { extern char *tool; if (debug) { fprintf(stderr,"%s debug: %s ",tool,__func__); fprintf(stderr, __VA_ARGS__); } }
#else
#define LIBPAPIEX_DEBUG(...) 
#define PAPIEX_DEBUG(...) 
#endif

#define LIBPAPIEX_WARN(...) { fprintf(stderr,"libpapiex warning: "); fprintf(stderr, __VA_ARGS__); fprintf(stderr,"\n"); }
#define LIBPAPIEX_ERROR(...) { fprintf(stderr,"libpapiex error: "); fprintf(stderr, __VA_ARGS__); fprintf(stderr,"\n"); monitor_real_exit(1); }
#define LIBPAPIEX_PAPI_ERROR(name,code) { LIBPAPIEX_ERROR("PAPI error from %s, %d, %s", name, code, PAPI_strerror(code)); }
#define LIBPAPIEX_PAPI_WARN(name,code) { LIBPAPIEX_WARN("PAPI error from %s, %d, %s", name, code, PAPI_strerror(code)); }

#define PAPIEX_WARN(...) { extern char *tool; fprintf(stderr,"%s warning: ",tool); fprintf(stderr, __VA_ARGS__); }
#define PAPIEX_ERROR(...) { extern char *tool; fprintf(stderr,"%s error: ",tool); fprintf(stderr, __VA_ARGS__); fprintf(stderr,"\n"); exit(1); }
#define PAPIEX_PAPI_ERROR(name,code) { PAPIEX_ERROR("PAPI error from %s, %d, %s\n", name, code, PAPI_strerror(code)); }
#define PAPIEX_PAPI_WARN(name,code) { PAPIEX_WARN("PAPI error from %s, %d, %s\n", name, code, PAPI_strerror(code)); }

#ifdef HAVE_PAPI
#include "papi.h"

#if (PAPI_VERSION_MAJOR(PAPI_VERSION) < 3)
#error "papiex only supports PAPI version 3 or greater."
#endif

#if (PAPI_VERSION_REVISION(PAPI_VERSION) < 3)
/* Old <= 3.0.2 definitions */
#ifndef PAPI_PRESET_MASK
#define PAPI_PRESET_MASK PRESET_MASK
#endif
#ifndef PAPI_NATIVE_MASK
#define PAPI_NATIVE_MASK NATIVE_MASK
#endif
#endif

#ifndef PAPI_TLS_USER_LEVEL1
#ifdef PAPI_USR1_TLS
#define PAPI_TLS_USER_LEVEL1 PAPI_USR1_TLS
#else
#error "No definition for PAPI_TLS_USER_LEVEL1 or PAPI_USR1_TLS!"
#endif
#endif

extern void _papiex_dump_event_info(FILE *out, int eventcode, int verbose);
extern void _papiex_dump_memory_info(FILE *out);

#endif

#define PAPIEX_DEFAULT_ARGS "PAPIEX_DEFAULT_ARGS"
#define PAPIEX_ENV "PAPIEX_OPTIONS"
#define PAPIEX_OUTPUT_ENV "PAPIEX_OUTPUT"
#define PAPIEX_LDLP "LD_LIBRARY_PATH"
#define PAPIEX_INIT_THREADS 64

#ifndef MAX_LABEL_SIZE
#define MAX_LABEL_SIZE 256
#endif

#define PAPIEX_MAX_COUNTERS 64

typedef struct {
  char label[MAX_LABEL_SIZE];
  float used;
  int depth;
#ifdef HAVE_PAPI
  long long tmp_real_usec;
  long long real_usec;
  long long tmp_virt_usec;
  long long virt_usec;
  long long tmp_real_cyc;
  long long real_cyc;
  long long virt_cyc;
  long long tmp_virt_cyc;
  long long tmp_counters[PAPIEX_MAX_COUNTERS];
  long long counters[PAPIEX_MAX_COUNTERS];
#endif
} papiex_caliper_data_t;

/* The structures below are used only for variance 
 * calculations where if double fields are not used,
 * overflows may occur 
 */
typedef struct {
  char label[MAX_LABEL_SIZE];
  float used;
  int depth;
#ifdef HAVE_PAPI
  double tmp_real_usec;
  double real_usec;
  double tmp_virt_usec;
  double virt_usec;
  double tmp_real_cyc;
  double real_cyc;
  double virt_cyc;
  double tmp_virt_cyc;
  double tmp_counters[PAPIEX_MAX_COUNTERS];
  double counters[PAPIEX_MAX_COUNTERS];
#endif
} papiex_double_caliper_data_t;

typedef struct {
  unsigned tid;
  int eventset;
  time_t stamp;    /* start time */
  time_t finish;   /* stop time */
  int max_caliper_entries; /* number of entries in allocated structure */
  int max_caliper_used; /* highest number of referenced entry */
  papiex_caliper_data_t data[PAPIEX_MAX_CALIPERS];
} papiex_perthread_data_t;

typedef struct {
  unsigned tid;
  int eventset;
  time_t stamp;    /* start time */
  time_t finish;   /* stop time */
  papiex_double_caliper_data_t data[PAPIEX_MAX_CALIPERS];
} papiex_double_perthread_data_t;

extern int _papiex_debug;
extern int _papiex_threaded;
extern void _papiex_dump_memory_info(FILE *output);

#define THR_SCOPE 1
#define PROC_SCOPE 2
#define JOB_SCOPE 3


#endif
