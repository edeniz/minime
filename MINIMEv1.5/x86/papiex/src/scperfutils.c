#ifndef scperfutils_c
#define scperfutils_c

#ifndef _GNU_SOURCE
#  define _GNU_SOURCE /* must define on Linux to get RTLD_NEXT from <dlfcn.h> */
#  define SELF_DEFINED__GNU_SOURCE
#endif

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <signal.h>
#include <unistd.h>      /* getpid() */
#include <linux/unistd.h>
#include <dlfcn.h>
#include <errno.h>

#ifdef HAVE_MPI
#include <mpi.h>
#endif

#define SCPERFUTILS_DEBUG(...)  do {				\
    if (scperfutils_opt_debug) {					\
	fprintf(stderr, "scperfutils debug>> %s,%lu: ", __func__, (long unsigned int)getpid()); \
	fprintf(stderr, __VA_ARGS__ );				\
    }								\
} while (0)

#define SCPERFUTILS_WARN(...)  do {					\
    fprintf(stderr, "scperfutils warning>> %s: ", __func__);	\
    fprintf(stderr, __VA_ARGS__ );				\
} while (0)

#define SCPERFUTILS_ERROR(...)  do {				\
    fprintf(stderr, "scperfutils error>> %s: ", __func__);		\
    fprintf(stderr, __VA_ARGS__ );				\
} while (0)

#ifdef HAVE_MPI
#define SCPERFUTILS_MPI_DEBUG(...)  do {				\
    if (scperfutils_opt_debug) {					\
	fprintf(stderr, "scperfutils mpi debug>> %s,%d: ", __func__, scperfutils_mpi_rank);	\
	fprintf(stderr, __VA_ARGS__ );				\
    }								\
} while (0)
#define SCPERFUTILS_MPI_ERROR(...)  do {				\
    fprintf(stderr, "scperfutils mpi error>> %s: ", __func__);		\
    fprintf(stderr, __VA_ARGS__ );				\
} while (0)
#endif
/**************************** Forward Declarations **************************/

#ifdef MAX
# undef MAX
#endif
#ifdef MIN
# undef MIN
#endif
#define MAX(a,b)	((a>=b)?a:b)
#define MIN(a,b)	((a<=b)?a:b)

static int scperfutils_opt_debug;
static int scperfutils_is_mpied;

#ifdef HAVE_MPI
static int scperfutils_mpi_rank;    /* Our global rank */
static int scperfutils_mpi_size;    /* Our global size */
typedef int (*__MPI_Comm_size_fptr_t) (MPI_Comm, int*);
typedef int (*__MPI_Comm_rank_fptr_t) (MPI_Comm, int*);
typedef int (*__MPI_Bcast_fptr_t) (void *, int, MPI_Datatype, int, MPI_Comm);
static __MPI_Comm_size_fptr_t _fptr_MPI_Comm_size = NULL;
static __MPI_Comm_rank_fptr_t _fptr_MPI_Comm_rank = NULL;
static __MPI_Bcast_fptr_t _fptr_MPI_Bcast = NULL;

#if 0
/* Default handlers. These should not get invoked */
static int _scperfutils_MPI_Comm_rank( MPI_Comm comm, int *rank ) {
    SCPERFUTILS_MPI_ERROR("MPI_Comm_rank default handler called. Your program is not built with MPI, yet you told me to use it!\n");
    exit(1);
}
static int _scperfutils_MPI_Comm_size( MPI_Comm comm, int *size ) {
    SCPERFUTILS_MPI_ERROR("MPI_Comm_size default handler called. Your program is not built with MPI, yet you told me to use it!\n");
    exit(1);
}
static int _scperfutils_MPI_Bcast( void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm ) {
    SCPERFUTILS_MPI_ERROR("MPI_Bcast default handler called. Your program is not built with MPI, yet you told me to use it!\n");
    exit(1);
}

/* Weak definitions */
int MPI_Comm_rank( MPI_Comm comm, int *rank )
    __attribute__ ((weak, alias ("_scperfutils_MPI_Comm_rank")));
int MPI_Comm_size( MPI_Comm comm, int *size )
    __attribute__ ((weak, alias ("_scperfutils_MPI_Comm_size")));
int MPI_Bcast( void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm )
    __attribute__ ((weak, alias ("_scperfutils_MPI_Bcast")));

#pragma weak MPI_Comm_rank
#pragma weak MPI_Comm_size
#pragma weak MPI_Bcast
#endif

#endif /* HAVE_MPI */


static int scperfutils_initialized = 0;
static int scperfutils_use_mpi = 0;
static char* scperfutils_suffix = ".txt";
static char base_output_path[PATH_MAX]="";
static char proc_output_path[PATH_MAX]="";
static char thread_output_path[PATH_MAX]="";

// This function checks the existence of path,
// if it doesn't exist, 0 is returned, else, the
// next available generation is returned
static int _scperfutils_get_next_gen(const char *path) {
  struct stat buf;
  char tmp_path[PATH_MAX];
  strcpy(tmp_path, path);
  int rc = stat(tmp_path, &buf);
  if ((rc<0) && (errno==ENOENT)) return 0;
  int inst = 0;
  do {
    inst++;
    strcpy(tmp_path, path);
    sprintf(tmp_path, "%s.%d", path, inst);
    rc = stat(tmp_path, &buf);
  } while (!((rc<0) && (errno==ENOENT)));
  return inst;
}


static void scperfutils_initialize(int _mpi_ok) {
  SCPERFUTILS_DEBUG("Initializing..");
  scperfutils_use_mpi = _mpi_ok;
  char *tmp;
  if ((tmp=getenv("SCPERFUTILS_DEBUG"))!= NULL)
    scperfutils_opt_debug = atoi(tmp);

  scperfutils_initialized = 1;
  if (!_mpi_ok) return;

#ifdef HAVE_MPI
  char* err;
  dlerror();  /* clear existing errors */
  dlsym(RTLD_NEXT, "MPI_Init");  /* Is MPI_Init defined? */
  if (dlerror() == NULL)
    scperfutils_is_mpied = 1;

  if (scperfutils_use_mpi && !scperfutils_is_mpied) 
    SCPERFUTILS_WARN("I don't think this program has MPI. Still following your directive and using MPI..\n");

  dlerror();  /* clear existing errors */
  _fptr_MPI_Comm_rank = (__MPI_Comm_rank_fptr_t)dlsym(RTLD_NEXT, "MPI_Comm_rank");
  if ((err = dlerror()) != NULL) {
    SCPERFUTILS_ERROR("dlsym(RTLD_NEXT,MPI_Comm_rank) failed. %s", err);
    exit(1);
  }
  dlerror();  /* clear existing errors */
  _fptr_MPI_Comm_size = (__MPI_Comm_size_fptr_t)dlsym(RTLD_NEXT, "MPI_Comm_size");
  if ((err = dlerror()) != NULL) {
    SCPERFUTILS_ERROR("dlsym(RTLD_NEXT,MPI_Comm_size) failed. %s", err);
    exit(1);
  }

  dlerror();  /* clear existing errors */
  _fptr_MPI_Bcast = (__MPI_Bcast_fptr_t)dlsym(RTLD_NEXT, "MPI_Bcast");
  if ((err = dlerror()) != NULL) {
    SCPERFUTILS_ERROR("dlsym(RTLD_NEXT,MPI_Bcast) failed. %s", err);
    exit(1);
  }
  /* The variables are used in MPI debug macros */
  if (scperfutils_is_mpied) {
    _fptr_MPI_Comm_rank(MPI_COMM_WORLD, &scperfutils_mpi_rank);
    _fptr_MPI_Comm_size(MPI_COMM_WORLD, &scperfutils_mpi_size);
  }
#endif /* HAVE_MPI */

 
  return;
}

/* This function should be called first, before any output from the program.
 * For MPI programs it MUST be called anytime AFTER MPI_Init and BEFORE MPI_Finalize.
 * All MPI tasks need to call this function, so preferably call this function as
 * part of an MPI_Init handler.
 *
 * The function can be called multiple times without harm. The last invocation counts.
 * 
 * There are two broad invocations, one in which the user_spec_file is NULL,
 * and the other in which the user specifies a file name.
 *
 * In the former case:
 *
 * The function will set base_output_path to: "<app>.<tool>.<size>.<host>.<pid>.<instance>
 * where, 
 *  <size>      : MPI comm size, for serial program it is 1.
 *  <instance>  : The first positive number so that the path does not collide.
 * 
 * If user_spec_path is set then that is merely copied into base_output_path,
 * and the above formatting of base_output_path is NOT done.
 *
 * If scperfutils_use_mpi is set, then MPI broadcast will be attempted to share the generated PATH.
 * Further, the job size will be automatically derived using scperfutils, and job_size will
 * be ignored.
 * If scperfutils_use_mpi is unset(zero), a job size may still be specfied using the job_size field.
 * 
 * RETURN VALUE: None (modifies base_output_path)
 */
extern void scperfutils_set_base_output_path(char* app, char *tool, char* prefix, char* user_spec_file, char* user_spec_suffix, int _mpi_ok, int job_size) {

  if (!scperfutils_initialized) scperfutils_initialize(_mpi_ok);

  SCPERFUTILS_DEBUG("Setting output paths\n");


  if (prefix && user_spec_file && strlen(prefix) && strlen(user_spec_file)) {
    SCPERFUTILS_ERROR("Both prefix and an output path cannot be specified\n");
    exit(1);
  }

  if (user_spec_suffix)
    scperfutils_suffix = strdup(user_spec_suffix);

  if (user_spec_file && strlen(user_spec_file)) {
#ifdef HAVE_MPI
    // for MPI programs we cannot have a single output file
    if (scperfutils_use_mpi) {
      SCPERFUTILS_ERROR("For MPI programs you cannot specify an output file.\n");
      exit(1);
    }
#endif
    /* If the user specified a path, that supersedes */
    SCPERFUTILS_DEBUG("User-specified output file: %s\n", user_spec_file);
    strncpy(base_output_path, user_spec_file, PATH_MAX);
    int gen = _scperfutils_get_next_gen(user_spec_file);
    
    if (gen) {
      sprintf(base_output_path, "%s.%d", user_spec_file, gen);
      SCPERFUTILS_WARN("User-specified file %s already exists, naming file %s\n", user_spec_file, base_output_path);
    }
  }
  else {
    int size = 1;
    if (job_size >0) size = job_size;

    int instance = 0;
    char hostname[256];
    gethostname(hostname, 256);
    char *host = hostname;
    if (strchr(hostname, '.'))
      host = strtok(hostname, ".");


    if (!prefix) prefix = "";

/*  Using a timestamp
    char timestamp[50] = "";
    struct timeval now;
    gettimeofday(&now, NULL);
    strftime(timestamp, sizeof(timestamp), "%Y%m%d-%H%M", localtime(&now.tv_sec));
 */

#ifdef HAVE_MPI
    if (scperfutils_use_mpi)
      size = scperfutils_mpi_size; 
#endif
    /* Figure out the instance number */
    do {
      instance++;
      snprintf(base_output_path, PATH_MAX, "%s%s.%s.%d.%s.%d.%d", prefix, app, tool, 
               size, host, (int)getpid(), instance);
    } while (access(base_output_path, F_OK)==0); 

  } 

  SCPERFUTILS_DEBUG("Tentative base output path is: %s\n", base_output_path);
  /* For non-MPI process path is the same as the base path */
  /* For MPI, this value will be overwritten below */
  strncpy(proc_output_path, base_output_path, PATH_MAX); 

#ifdef HAVE_MPI
  if (scperfutils_use_mpi) {
    /* share the root process's  base_output_path with the others */
    SCPERFUTILS_MPI_DEBUG("About to resolve base path using an MPI Bcast from root\n");
    _fptr_MPI_Bcast(base_output_path, PATH_MAX, MPI_CHAR, 0, MPI_COMM_WORLD);
    SCPERFUTILS_MPI_DEBUG("Received base_output_path from MPI_Bcast: %s\n", base_output_path);
    snprintf(proc_output_path, PATH_MAX, "%s/task_%d", base_output_path, scperfutils_mpi_rank);
  } /* scperfutils_use_mpi */
#endif /* HAVE_MPI */

  SCPERFUTILS_DEBUG("Process path set to: %s\n", proc_output_path);
  return;
}

extern char* scperfutils_get_base_output_path(void) { 
  if (!scperfutils_initialized) {
    SCPERFUTILS_ERROR("scperfutils_set_base_path() MUST be called before any scperfutils path API call\n");
    exit(1);
  }
  return base_output_path; 
}

extern char* scperfutils_get_proc_output_path(void) { 
  if (!scperfutils_initialized) {
    SCPERFUTILS_ERROR("scperfutils_set_base_path() MUST be called before any scperfutils path API call\n");
    exit(1);
  }
  return proc_output_path; 
}

extern char* scperfutils_get_thread_output_path(int thread_id) {
  if (!scperfutils_initialized) {
    SCPERFUTILS_ERROR("scperfutils_set_base_path() MUST be called before any scperfutils path API call\n");
    exit(1);
  }
  snprintf(thread_output_path, PATH_MAX, "%s/thread_%d%s", proc_output_path, thread_id, scperfutils_suffix);
  return thread_output_path;
}

/* This function can be called safely multiple times.
 * It creates the process path. 
 * For:
 *  unthreaded, non-mpi: No path created
 *    threaded, non-mpi: Process directory created (includes user-specified path as well)
 *  unthreaded,     mpi: Base directory created
 *    threaded,     mpi: Base->Process directory created
 */
extern void scperfutils_make_output_paths(int is_threaded) {
  if (!scperfutils_initialized) {
    SCPERFUTILS_ERROR("scperfutils_set_base_path() MUST be called before any scperfutils path API call\n");
    exit(1);
  }
#ifdef HAVE_MPI
  if (scperfutils_use_mpi) {
    SCPERFUTILS_MPI_DEBUG("This is an MPI program. Creating a base directory if it doesn't exist\n");
    if (mkdir(base_output_path, 0755)) {
      if (errno != EEXIST) {
        SCPERFUTILS_MPI_ERROR("Cannot created base directory: %s (errno=%d)\n", base_output_path, errno);
        exit(1);
      }
    }
    SCPERFUTILS_MPI_DEBUG("Created base output directory (or it existed): %s\n", base_output_path);
  }
#endif
  if (is_threaded) {
    SCPERFUTILS_DEBUG("This is a threaded program. Creating a process directory if it doesn't exist\n");
    if (mkdir(proc_output_path, 0755)) {
      if (errno != EEXIST) {
        SCPERFUTILS_ERROR("Cannot created process directory: %s (errno=%d)\n", proc_output_path, errno);
        exit(1);
      }
    }
    SCPERFUTILS_DEBUG("Created process output directory (or it existed): %s\n", proc_output_path);
  }
  return;
}

#endif /* scperfutils_c */
