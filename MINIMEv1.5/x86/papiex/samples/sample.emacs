[mucci@h04n05:~/papi-HEAD/tools/papiex]$ ./ia64-Linux/papiex -e PAPI_TOT_CYC -e PAPI_TOT_INS -- /usr/bin/emacs -nw
PapiEx Version:		0.98
Executable:		/usr/bin/emacs-21.2
Processor:		Itanium 2
Clockrate:		900.000000
Parent Process ID:	4905
Process ID:		4906
Hostname:		h04n05.pdc.kth.se
Options:		FILE,PAPI_TOT_CYC,PAPI_TOT_INS
Output:			sample.emacs-21.2.papiex.h04n05.pdc.kth.se.4906
Start:			Wed Feb  9 15:35:27 2005
Finish:			Wed Feb  9 15:35:28 2005
Domain:			User
Real usecs:		799450
Real cycles:		719477784
Proc usecs:		132736
Proc cycles:		119462400
PAPI_TOT_CYC:		108836016
PAPI_TOT_INS:		158004227

Event descriptions:
Event: PAPI_TOT_CYC
	Derived: No
	Short: Total cycles
	Long: Total cycles
Event: PAPI_TOT_INS
	Derived: Yes
	Short: Instr completed
	Long: Instructions completed
