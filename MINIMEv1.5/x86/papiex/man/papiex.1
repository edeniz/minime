.\" $Id: papiex.1,v 1.12 2010/07/28 16:27:14 mucci Exp $
.TH PAPIEX 1 "May, 2004"
.SH NAME
papiex \- transparently measure hardware performance events of an application with PAPI

.SH SYNOPSIS

\fBpapiex\fP [\fI-dhisVqnwlUKISarx\fP] [\fI-f output-dir\fP] [\fI-p prefix\fP] [\fI-o output-file\fP] [\fI-e papi-event\fP] [\fI-m[<interval>]\fP] [\fI-L papi-event\fP] [\fI--no-summary\fP] [\fI--no-scientific\fP] [\fI--no-ld-path\fP] [\fI--no-gather\fP] [--] command [args ...]

\fBpapiex\fP -- command args ...

\fBpapiex\fP command


.SH DESCRIPTION
\fBpapiex\fP is a PAPI-based program for measuring hardware performance events of an application using the command-line. It supports both PAPI preset events and native events. It supports multiple threads of execution as well, including pthreads and OpenMP threads. For MPI programs, \fBpapiex\fP can gather statistics across tasks. \fBpapiex\fP also measures the total time spent in I/O and MPI calls. 

The default settings are equivalent to typing:

\fBpapiex\fP -u -e PAPI_TOT_CYC -e PAPI_FP_INS

If your processor doesn't support counting of floating point operations (like the UltraSparc II and the original AMD Athlon) then PAPI_FP_INS or PAPI_TOT_INS is used instead (in order of availability).

\fBpapiex\fP honors the environment variable \fBPAPIEX_DEFAULT_ARGS\fP. The contents of this variable are appended to the command-line arguments every time \fBpapiex\fP is invoked. It is suggested that platform-wide arguments be set via this variable.

.SH OPTIONS

.TP
\fI--\fP 
This is the standard separator telling \fBpapiex\fP to terminate its option processing and pass the rest of the command line to the underlying shell. Use this if your application takes command line arguments. 

Example: \fBpapiex\fP -- ls -a

.TP
\fI-a\fP
Monitor useful events available on the architecture automatically. This implicitly enables multiplexing (see the \fI-m\fP flag) and memory monitoring (see the \fI-x\fP flag).

.TP
\fI-d\fP
Enable debugging output. Repeat this option for more verbosity. 

.TP
\fI-e <event>\fP 
Monitor the event as named. The event is a symbol as listed in the output from either the \fI-l\fP or \fI-L\fP flag. You may specify more than one event. If you specify more than the number of physical registers as listed with the \fI-i\fP flag, you must enable multiplexing with \fI-m\fP otherwise an error will be reported. 

.TP
\fI-f <output directory>\fP
All output of \fBpapiex\fP is created under the specified output directory. If the directory does not exist, it is created. By default, all output is placed under the working directory. 

.TP
\fI-h\fP 
Print the usage information.

.TP
\fI-i\fP
Print information about the host processor.

.TP
\fI-I\fP
Measure hardware counters in transient mode. This mode may not be supported on your processor. Some CPU's execute interrupt/TLB miss handlers in an entirely different privelege level. If your processor does not support this level, you will get an error when \fBpapiex\fP goes to set up the counters. See, also \fI-K\fP, \fI-U\fP and \fI-S\fP flags for other modes.

.TP
\fI-K\fP
Measure hardware counters in kernel mode. See, also \fI-I\fP, \fI-U\fP and \fI-S\fP flags for other modes.

.TP
\fI-l\fP
Print a list of the available PAPI presets and native events.

.TP
\fI-L event\fP
Print a full description of event.

.TP
\fI-m[<interval>]\fP
Enable counter multiplexing to measure more events than the number of physical counters available. The number of counters can be discovered with the \fI-i\fP flag. The interval is specified in Hz; it is optional and defaults to 10 (Hz).

.TP
\fI-n\fP
Do NOT create ANY output files. By default, in addition to writing to the terminal, \fBpapiex\fP creates files (and directories) containing its output. See, also \fI-w\fP.

.TP
\fI-o <output-file>\fP
By default \fBpapiex\fP dumps it output in a file/directory named <cmd>.papiex.<host>.<pid>.<instance>. The \fI-o\fP flag, instead causes the output to be sent to the user-supplied path. For multithreaded and MPI runs, its behavior can seem confusing, and you probably should use \fI-f\fP instead.

.TP
\fI-p <prefix>\fP
By default \fBpapiex\fP dumps it output in a file/directory named <cmd>.papiex.<host>.<pid>.<instance>. The \fI-p\fP flag causes <prefix> to be prepended to the output name. This is useful for MPI and multithreaded runs. For readibility, it is a good idea to have a separator, such as . (dot), at the end of your prefix. 

.TP
\fI-q\fP
Print information in a less verbose format. This is just the counter value followed by the counter name. The only additional information printed is the timing information and any thread identifiers. It is printed right justified with a width of 16 places. This option is currently not compatible with \fI-r\fP.

.TP
\fI-r\fP
Report resources used by the program, as reported by \fBgetrusage\fP(). Most of the time this doesn't work on Linux. This option is current not compatible with \fI-q\fP.

.TP
\fI-s\fP
This option simply dumps the environment variable/value pairs to stdout and then exits.

.TP
\fI-S\fP
Measure hardware counters in supervisor mode. See, also \fI-I\fP, \fI-U\fP and \fI-K\fP flags for other modes.

.TP
\fI-U\fP
Measure hardware counters in user mode. This is the default counting mode. See, also \fI-I\fP, \fI-K\fP and \fI-S\fP flags for other modes.

.TP
\fI-w\fP
Do NOT send output to the console. By default, in addition to writing files, \fBpapiex\fP emits output to the terminal. See also, \fI-n\fP.

.TP
\fI-x\fP
Report memory information for the process. Not all statistics will be available on all Linux kernel versions. Currently reported are peak virtual, peak resident, text, library, heap, stack, shared and locked memory. Numbers are in KB. This option is current not compatible with \fI-q\fP.

.TP
\fI-V\fP 
Print the version information of \fBpapiex\fP, the PAPI library and the PAPI header file \fBpapiex\fP was built against.

.TP
\fI--no-summary\fP
For MPI and multithreaded runs this prevents \fBpapiex\fP from printing a summary across threads/tasks.

.TP
\fI--no-scientific\fP
Disables printing output in scientific notation.

.TP
\fI--no-ld-path\fP
Do not modify the LD_LIBRARY_PATH environment variable under any circumstances.

.TP
\fI--no-gather\fP
Do not gather per-process data with MPI and output on the front-end. Instead, output data on each of the nodes.

.SH EXAMPLES
The simplest use of \fBpapiex\fP on a unithreaded, single process program, would be as:

\fBpapiex\fP /bin/ls

In the above case, the performance measurement of PAPI_TOT_CYC and PAPI_FP_OPS would be written to stderr.
To monitor specific events explicitly, one would do:

\fBpapex\fP -e PAPI_L1_DCM -e PAPI_L1_TCM /bin/ls

For multithreaded programs, you would simply invoke \fBpapiex\fP as above; the multiple threads are automatically handled. The output is written into a directory named <cmd>.papiex.<host>.<pid>.<instance>. In case you just want a high-level summary with no per-thread output files, you would do:

\fBpapiex\fP -n -e PAPI_FP_OPS my-threaded-prog

Observe the \fI-n\fP flag which instructs \fBpapiex\fP not to create any output files. In contrast, \fI-w\fP tells \fBpapiex\fP not to emit text to the console: only to files. Alternatively, you may want per-thread files, but not care for a summary across threads. To achieve this, do:

\fBpapiex\fP --no-summary my-threaded-app

Multiple task programs using MPI are automatically handled by \fBpapiex\fP, as in:

\fBmpirun\fP -np 4 \fBpapiex\fP -f /tmp ./pop

In the above example, the output data files are stored in /tmp/pop.papiex.<host>.<pid>.<instance>. Summary statistics are generated across tasks, which may be disabled using \fI--no-summary\fP. If you want no gathering of statistics by the front-end, and instead want per-task data emitted by each node, use \fI--no-gather\fP. The emitted files can be easily identified by the hostname embedded in the file name.

You can also give a prefix to the output path. For e.g.,

\fBpapiex\fP -f mystats. my-threaded-prog

The command above will create a directory ./mystats.my-threaded-prog.papiex.<host>.<pid>.<instance>. For multithreaded and/or MPI programs, \fBpapiex\fP creates per-thread/per-task and global statistics summaries across threads/tasks, which are stored this directory.

To facilitate ease of use, the \fI-a\fP flag is provided. This allows automatic monitoring of available interesting events. To enable multiple events to be monitored with limited counters, multiplexing (\fI-m\fP) is implicitly assumed. The flag also works for multihreaded and MPI programs. For any run more than a few minutes, it is \fBstrongly recommended\fP that you start your quest for understanding performance with \fBpapiex -a\fP. E.g.,

\fBpapiex\fP -a my-long-program

.SH NOTES

.SH SEE ALSO
\fB
fork(2),
PAPI(3),
getrusage(2),
ld.so(8)
\fP

.SH BUGS 
If you measure an application or process that makes use of the library preloading mechanism AND you disable the following of \fBfork\fP()'s with the \fI-n\fP flag, the child processes will most likely die a horrible death. 
.LP
Additional bugs should be reported to the OSPAT Mailing List at <ospat-devel@cs.utk.edu>. 

.SH AUTHORS
.B papiex
was written by Philip J. Mucci and Tushar Mohan

.SH COPYRIGHT
This software is \fBCOMPLETELY OPEN SOURCE\fP. If you incorporate any portion of this software, I would appreciate an acknowledgement in the appropriate places. Should you find PapiEx useful, please considering making a contribution in the form of hardware, software or plain old cash.


