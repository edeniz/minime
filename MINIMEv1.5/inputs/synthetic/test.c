/*
 * test.c
 *
 *  Created on: Dec 6, 2012
 *      Author: etem deniz
 */

#include <stdio.h>
#include <pthread.h>

#define NUM_THREADS 10

void *thread_func(void *param) {

	printf("Thread is running\n");

	return NULL;
}


int main(int argc, char **argv) {

	int i;
	pthread_t threads[NUM_THREADS];

	for (i = 0; i < NUM_THREADS; i++) {
		pthread_create(&threads[i], NULL, thread_func, NULL);
	}
	for (i = 0; i < NUM_THREADS; i++) {
		pthread_join(threads[i], NULL);
	}

	return 0;
}

