Copyright (c) 2011-2015, Etem Deniz <etem.deniz@boun.edu.tr>, 
Alper Sen <alper.sen@boun.edu.tr>, Bogazici University
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
(1) Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
(2) Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
(3) The names of the contributors may not be used to endorse or promote
products derived from this software without specific prior written
permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**********************************************************
#MINIME: Multicore Benchmark Synthesizer#
##About MINIME v1.5##

MINIME consists of three tools: MINIME-CPU, MINIME-SA, and MINIME-GPU.

MINIME-CPU is an automated tool that generates 
synthetic/standalone benchmarks in C using
MRAPI, MCAPI, or POSIX (Pthread). 
The CPU synthetic benchmark 
preserves the following performance characteristics:
Parallel Pattern (PP), Thread Communication (TC), 
Instructions per Cycle (IPC), Cache Miss Rate (CMR), 
Branch Misprediction Rate (BMR), 
Communication to Computation Ratio (CCR).
Our framework contains three main modules: 
benchmark characterizer, parallel pattern recognizer,
and synthetic benchmark synthesizer. The benchmark characterizer
obtains parallel benchmark characteristics by using
a dynamic binary instrumentation tool. The parallel pattern
recognizer decides the architectural parallel pattern from
the benchmark characteristics obtained. Finally, the benchmark synthesizer
generates a synthetic benchmark from the parallel pattern
and the benchmark characteristics obtained. 
Also, the specific MCA/Pthread
API is used during synthesis in order to generate 
benchmarks for heterogeneous embedded systems.

MINIME-SA is an automated tool similar to MINIME-CPU but
it uses user-defined input files instead of user given binary files. 

MINIME-GPU is an automated tool that generates 
synthetic benchmarks in C++ using OpenCL API. 
The GPU synthetic benchmark 
preserves the following performance characteristics:
instruction throughput, computation-to-memory access, 
dynamic memory instruction mix, 
memory efficiency, and compute unit occupancy.
Our framework contains two main modules: 
benchmark characterizer and synthetic benchmark synthesizer. 
The benchmark characterizer
obtains parallel benchmark characteristics by using
a simulation tool. The benchmark synthesizer
generates a synthetic benchmark from the 
benchmark characteristics obtained. 
Also, the specific OpenCL API is used during synthesis 
in order to generate benchmarks for CPU/GPU heterogeneous systems.

**Please cite the following papers if you are using our tool.**

E. Deniz, A. Sen, B. Kahne, J. Holt, 
MINIME: Pattern-Aware Multicore Benchmark Synthesizer, 
IEEE Transactions on Computers, 2014.

E. Deniz, A. Sen, 
MINIME-GPU: Multicore Benchmark Synthesizer for GPUs, 
ACM Transactions on Architecture and Code Optimization, 2015.

**The MINIME home page:**

  www.cmpe.boun.edu.tr/~deniz/minime.html

************************************************************
##Quickstart instructions for building and running MINIME:##

DynamoRio, Umbra, MCA APIs (only for synthetics in MCAPI/MRAPI),
PAPI, monitor, papiex, and Linux perf-tools 
are required for running the MINIME-CPU/MINIME-SA. 

Multi2Sim, AMD Catalyst driver, and AMD APP SDK with OpenCL 
are required for running the MINIME-GPU.

MINIME tool package includes these requirements, 
which are compatible and tested with the MINIME. 

--------------------------------------------------
The MINIME-CPU is tested on x86 machine with 6GB RAM running Ubuntu 11.10/12.04 
x86-64 with gcc version 4.6.3, PowerPC (Freescale p4080) machine with 4GB RAM
running Linux p4080ds 3.0.34-rt with gcc version 4.6.2,
and x86 VM running Linux debian 2.6.32-5-amd64 with gcc version 4.4.5.
DynamoRIO-Linux-4.1.2293 (custom build), umbra-linux-0.3 (this version is ported 
to DynamoRIO-Linux-3 by us), and MCAPI_MRAPI_2.0.3 (this version
is modified by us) are used during tests.

The MINIME-GPU is tested on x86 machine with 6GB RAM running Ubuntu 12.04 x86-64.
Multi2Sim 4.2 (this version is modified by us), AMD Catalyst 13.20 driver, 
AMD APP SDK v2.9 with OpenCL 1.2, and GCC version 4.6.3 
are used during tests.

--------------------------------------------------
##I. MINIME-CPU: Synthetic Benchmark Development for CPUs##

This part of the tool generates a synthetic benchmark 
from user given binary file.

###I-A. Installing and Running the MINIME on an x86 machine###

1. Change directory to x86/dynamorio and follow the 
instructions given below:
	- Install dependencies for Ubuntu Lucid. Adjust this command as appropriate for other distributions.
		* $ sudo apt-get install cmake-curses-gui cmake ia32-libs g++ g++-multilib doxygen transfig imagemagick ghostscript
	- Build and run
		* $ mkdir build && cd build
		* $ cmake ..
		* $ make -j
		* $ ./bin64/drrun echo hello world
		* hello world
	- Check README in x86/dynamorio directory for more details.

2. Change directory to x86/umbra and follow the 
instructions given below:
	- $ mkdir build && cd build
	- $ ccmake ../
		* First, press c to configure and then press e to exit help
		* Second, put /path/to/dynamorio/build/cmake at DynamoRIO_DIR
		* Lastly, press c for configuration and then press g to generate makefile and quit
	- $ make
	- $ /path/to/dynamorio/build/bin64/drrun -ops "-thread_private -max_bb_instrs 512 -no_finite_bb_cache -no_finite_trace_cache" -client /path/to/umbra/build/bin/libaio.so 0 "" ls
	- Check whether umbra.ls.*.proc.log file is generated,
	 if yes, then umbra is ready to use.
	- Check README in x86/umbra directory for more details.

3. Install Linux perf-tools. (If perf-tools exist skip this step.)
For example, run "sudo apt-get install linux-tools-common" for Ubuntu 
and run "sudo apt-get install linux-base linux-tools-common" for Debian.
Check that it works: $ perf stat true

4. Follow the following steps to install PAPI, monitor, and PapiEx.
	- Change directory to x86/papi-5.2.0/src and follow the 
instructions given below:
		* $ ./configure
		* $ make
		* $ make test
		* $ make install-all
		* Check INSTALL.txt in x86/papi-5.2.0 directory for more details.
	- Change directory to x86/monitor and follow the instructions given below:
		* $ make install
		* $ make quicktest
		* If monitor.h does not exist in /usr/local/include, perform copy operation as given below.
			* $ sudo cp ./src/monitor.h /usr/local/include
		* If libmonitor.so and libmonitor.so.1.0.1 do not exist in /usr/local/lib, perform copy operation as given below.
			* $ sudo cp ./x86_64/libmonitor* /usr/local/lib
		* Check INSTALL in x86/monitor directory for more details.
	- Change directory to x86/papiex and follow the 
instructions given below:
		* Install dependencies for Ubuntu Lucid. Adjust this command 
		as appropriate for other distributions.
			* $ sudo apt-get install gfortran
		* $ export LD_LIBRARY_PATH=/lib:/usr/lib:/usr/local/lib	
		* $ make PAPI_PREFIX="/usr/local" PAPI_LIB_PATH="/usr/local/lib" PAPI_INC_PATH="/usr/local/include"
		* $ make test
		* $ sudo make install
		* If libpapiex.so and libpapiex.so.1.1.0 do not exist in /usr/local/lib, perform copy operation as given below.
			* $ sudo cp ./x86_64/libpapiex* /usr/local/lib
		* Check INSTALL in x86/papiex directory for more details.

5. Test your installation with a simple program.
	- Build test.c in /path/to/MINIMEv1.5/inputs/synthetic directory.
	Simply, change directory to /path/to/MINIMEv1.5/inputs/synthetic 
	and run "gcc -o test test.c -lpthread".
	Change directory to /path/to/MINIMEv1.5/x86 directory.
	Run './minime.py -ep "path/to/MINIMEv1.5/inputs/synthetic/test"'.
	Check whether the synthetic benchmark (test_syn.c) is in 
	/path/to/MINIMEv1.5/x86/temp/posix/test_temp directory.
	For more details run './minime.py -h'. 
	You can see the list of the options that you can configure.

	- For generating a synthetic benchmark using MINIME,
	change directory to /path/to/MINIMEv1.5/x86 directory.
	Run './minime.py -ep "<executable_path>"' where <executable path> is your 
	binary file path and also includes arguments.
	The synthetic benchmark (C code file) is generated in 
	/path/to/MINIMEv1.5/x86/temp/posix/<executable_name>_temp directory where 
	posix is the type of the library that is set.

	- For measuring similarity between an executable and 
	an existing synthetic benchmark generated by MINIME,
	change directory to /path/to/MINIMEv1.5/x86 directory.
	The synthetic benchmark (C code file) should exist in 
	/path/to/MINIMEv1.5/x86/temp/posix/<executable_name>_temp directory where 
	posix is the type of the library that is set.
	Run './minime.py -co "<executable_path>"' where <executable path> is your 
	binary file path and also includes arguments.

###I-B. Installing and Running MINIME on a PowerPC machine###

1. Copy synthetic benchmarks generated on a x86 machine to a PowerPC machine.
The synthesized benchmarks are generated in 'temp' directory that is
in the same directory where minime.py is located.
Copy this directory from the x86 machine to the directory where 
mbdt_ppc.py script is located in the PowerPC machine.

2. Install Linux perf-tools. (If perf-tools exist skip this step.)
Check that it works: $ perf stat true

3. Test your installation with a simple program.
	- Build test.c in /path/to/MINIMEv1.5/inputs/synthetic directory.
	Simply, change directory to /path/to/MINIMEv1.5/inputs/synthetic 
	and run "gcc -o test test.c -lpthread".
	Change directory to /path/to/MINIMEv1.5/ppc directory.
	Run './minime.py -ep "path/to/MINIMEv1.5/inputs/synthetic/test"'.
	Check whether the synthetic benchmark (test_syn.c) is in 
	/path/to/MINIMEv1.5/ppc/temp/posix/test_temp directory.
	For more details run './minime.py -h'. 
	You can see the list of options that you can configure.

	- For generating a synthetic benchmark using MINIME,
	change directory to /path/to/MINIMEv1.5/ppc directory.
	Run './minime.py -ep "<executable_path>"' where <executable path> is your 
	binary file path and also includes arguments.
	The synthetic benchmark (C code file) is generated in 
	/path/to/MINIMEv1.5/ppc/temp/posix/<executable_name>_temp directory where 
	posix is the type of the library that is set.

	- For measuring similarity between an executable and 
	an existing synthetic benchmark generated by MINIME,
	change directory to /path/to/MINIMEv1.5/ppc directory.
	The synthetic benchmark (C code file) should exist in 
	/path/to/MINIMEv1.5/ppc/temp/posix/<executable_name>_temp directory where 
	posix is the type of the library that is set.
	Run './minime.py -co "<executable_path>"' where <executable path> is your 
	binary file path and also includes arguments.

--------------------------------------------------

##II. MINIME-SA: Stand-alone Benchmark Development for CPUs##

###II-A. Stand-alone Benchmark Generation###

This part of the tool generates a benchmark with user given properties.

1. Use makefile in x86/MBDT_Project/Debug or ppc/MBDT_Project/Debug directory for building 
MBDT project. Run "make all" to build the project.

2. Optional (If you will not develop benchmarks using MCA APIs, you can skip this step).
Use makefile in x86/MCA_Project/Debug or ppc/MCA_Project/Debug directory for building 
MCA APIs (MCAPI, MRAPI, MTAPI). Run "make all" to build the project. 

3. Run minime.py or mbdt_ppc.py with "-st sal" option 
for stand-alone benchmark generation.
(minime.py will be used for the rest of this section 
but similar operations work for mbdt_ppc.py)
Change directory to /path/to/MINIMEv1.5/x86 directory.
Run './minime.py -st sal -ep "<application_desc_file>"' where 
<application_desc_file> is the absolute file path that describes 
a multicore application. The synthetic benchmark (C code file) is generated in 
/path/to/MINIMEv1.5/x86/temp/posix/<binary_name>_temp directory where 
posix is the type of the library that is set.
For testing the stand-alone benchmark generation, run
'./minime.py -st sal -ep "/path/to/MINIMEv1.5/inputs/stand-alone/saInputGD" -bl posix'.
This command generates a multicore benchmark using POSIX (Pthread) library 
according to the properties given in saInputGD file.
A stand-alone input file should be formatted according to 
our Parallel Pattern Markup Language (PPML). 
The PPML defines the threads of a multicore application with the following attributes:
thread ID, parallel pattern, phase, creator thread ID, work size, 
shared data size, stage, loop count. For instance, 
"<thread_id: 2, par_pattern: GD, phase: 1, creator_id: 1, work_size: 10, 
shared_data_size: 100>" line states that ID of the thread is 2, 
parallel pattern is Geometric Decomposition, phase is 1, 
thread is created by the thread which's ID is 1 (this is main thread),
work (computation) size is 10, and shared data size is 100x4 bytes 
(where 4 is the size of integer). For the example given above, 
the synthetic benchmark named saInputGD_syn.c is generated in 
/path/to/MINIMEv1.5/x86/temp/posix/saInputGD_temp directory.

###II-B. Stand-alone Benchmark Generation with Similarity Metrics###

This part of the tool generates a benchmark with user given properties and
the benchmark has similar IPC, CMR, and BMR values.

1. Use makefile in x86/MBDT_Project/Debug or ppc/MBDT_Project/Debug directory for building 
Benchmark Synthesis project. Run "make all" to build the project.

2. Optional (If you will not develop benchmarks in MCA APIs, you can skip this step).
Use makefile in x86/MCA_Project/Debug or ppc/MCA_Project/Debug directory for building 
MCA APIs (MCAPI, MRAPI, MTAPI). Run "make all" to build the project. 

3. Run minime.py or mbdt_ppc.py with "-st swn" option 
for stand-alone benchmark generation with similarity metrics.
The similarity metrics are Instructions per Cycle (IPC), Cache Miss 
Rate (CMR), and Branch Miss Rate (BMR).
(minime.py will be used for the rest of this section 
but similar operations work for mbdt_ppc.py)
Change directory to /path/to/MINIMEv1.5/x86 directory.
Run "./minime.py <application_desc_file>" where 
<application_desc_file> is the absolute file path that describes 
multicore application. The synthetic benchmark (C code file) is generated in 
/path/to/MINIMEv1.5/x86/temp/posix/<binary_name>_temp  directory where 
posix is the type of the library that is set.
For testing the stand-alone benchmark generation, run
'./minime.py -st swm -ep "/path/to/MINIMEv1.5/inputs/stand-alone/saInputTP2" -bl posix'.
This command generates a multicore benchmark using POSIX (Pthread) library 
according to the properties given in saInputTP2 file.
A stand-alone input file should be formatted according to 
our Parallel Pattern Markup Language (PPML). 
The PPML defines an application with the following attributes:
application ID, ipc, cmr, and bmr.
"<application_id: 1, ipc: 1, cmr: 0.1, bmr: 0.05>" line states that
ID of the application is 1, IPC is 1, BMR is 10%, and CMR is 5%.
The PPML defines the threads of a multicore application with the following attributes:
thread ID, parallel pattern, phase, creator thread ID, work size, 
shared data size, stage, loop count. For instance, 
<thread_id: 2, par_pattern: TP, phase: 1, creator_id: 1, 
work_size: 10, local_data_size: 10>>" line states that ID of the thread is 2, 
parallel pattern is Task Parallel, phase is 1, 
thread is created by the thread which's ID is 1 (this is main thread),
work (computation) size is 10, and local data size is 10x4 bytes 
(where 4 is the size of integer). For the example given above, 
the synthetic benchmark named saInputTP2_syn.c is generated in 
/path/to/MINIMEv1.5/x86/temp/posix/saInputTP2_temp directory.

--------------------------------------------------

##III. MINIME-GPU: Synthetic Benchmark Development for GPUs##

This part of the tool generates a synthetic GPU benchmark 
from user given binary file.

1. Download and install AMD-APP-SDK-v2.9-lnx64.
	http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/
Be sure that AMDAPP directory (that as include, lib, and samples directories) 
is installed in "/opt/" directory.

2. Change directory to gpu and follow the instructions given below:
	- tar -xvzf multi2sim-4.2.tar.gz
	- Change directory to /gpu/multi2sim-4.2
	- Install dependencies for Ubuntu Precise Pangolin. Adjust this command as appropriate for other distributions.
		* $ sudo apt-get install llvm-3.1 flex bison freeglut3-dev libglew-dev libgtk-3-dev
	- Follow the "Basic Installation" steps in /gpu/multi2sim-4.2/INSTALL to complete 
		installation of Multi2Sim
	- Be sure that executables of Multi2Sim (m2s, m2c) are in "/usr/local/bin/" directory

3. Use makefile in gpu/MBDT_Project/Debug directory for building 
MBDT project. Run "make all" to build the project.

4. Test your installation with a simple program.
	- Change directory to /path/to/MINIMEv1.5/gpu directory.
	Run './minime.py -ep "path/to/MINIMEv1.5/inputs/gpu-synthetic/sample --load sample_Kernels.bin -q"'.
	Check whether the synthetic benchmark (sample_syn.cpp, sample_syn.hpp, and sample_syn_Kernels.cl) is in 
	/path/to/MINIMEv1.5/gpu/temp/opencl/sample_temp directory.
	For more details run './minime.py -h'. 
	You can see the list of the options that you can configure.

	- For generating a synthetic benchmark using MINIME,
	change directory to /path/to/MINIMEv1.5/gpu directory.
	Run './minime.py -ep "<executable_path>"' where <executable path> is your 
	binary file path and also includes arguments.
	The synthetic benchmark (host and kernel program files) is generated in 
	/path/to/MINIMEv1.5/gpu/temp/opencl/<executable_name>_temp directory where 
	opencl is the type of the library that is set.

--------------------------------------------------
##Limitations##

- MINIME runs on Linux 64-bit operating systems.
- (Characterization part of) MINIME can run on x86 machines (this
	is a limitation of Dynamorio).
- MINIME can characterize the applications that have no more than 
	32 threads (this is a limitation of Umbra).
- MINIME currently cannot generate benchmarks having 
	very low (for example, 0.01) or very high 
	(for example, 0.40) metric (CMR, BMR) values.

************************************************************
##Change log##

MINIME v1.5 fixed bugs and added features:
1. MINIME also supports generating synthetic GPU benchmarks
	in OpenCL.

MINIME v1.4 fixed bugs and added features:

1. MINIME also supports generating per-thread similar
	synthetic benchmarks on x86 systems.
2. MINIME uses PAPI, monitor, and papiex for per-thread 
	characterization.
3. MINIME uses new versions of Dynamorio (v4.1) and Umbra.
4. MINIME is tested and runs on Ubuntu 12.04.

MINIME v1.3 fixed bugs and added features:

1. Overall Similarity Score does not include 
	Thread Communication Score.

MINIME v1.2 fixed bugs and added features:

1. MINIME compares similarity between an 
	original binary and a synthetic binary.

MINIME v1.1 fixed bugs and added features:

1. Add single-threaded benchmark generation
2. Add generation of stand-alone benchmarks with user given 
	IPC, CMR, and BMR values.
3. Fix some path processing bugs.
4. Add command-line argument passing support.

************************************************************
##Getting help and reporting bugs:##

This is a test release. (Date: 3/1/15)
Please email etem.deniz@boun.edu.tr for any issues of using MINIME.

************************************************************
##Copyrights for the other tools we used:##

We've included this copyright since we're using DynamoRio for some stuff.

Copyright (c) 2010-2012 Google, Inc. licensed under the terms of the BSD.  All other rights reserved.
Copyright (c) 2000-2010 VMware, Inc. licensed under the terms of the BSD.  All other rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of VMware, Inc. nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL VMWARE, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

--------------------------------------------------

We've included this copyright since we're using Umbra for some stuff.

Copyright (c) 2010 Massachusetts Institute of Technology

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

--------------------------------------------------

We've included this copyright since we're using PAPI for some stuff.

                           Copyright (c) 2005 - 2010
                        Innovative Computing Laboratory
               Dept of Electrical Engineering & Computer Science
                            University of Tennessee, 
                                 Knoxville, TN. 
                              All Rights Reserved. 


Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the University of Tennessee nor the names of its 
      contributors may be used to endorse or promote products derived from this
      software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


This open source software license conforms to the BSD License template.

--------------------------------------------------

We've included this copyright since we're using monitor for some stuff.

		   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions. 

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version. 

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.

--------------------------------------------------

We've included this copyright since we're using papiex (PAPI Execute) for some stuff.

		   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions. 

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version. 

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.

************************************************************
