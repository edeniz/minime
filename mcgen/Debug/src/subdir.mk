################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/benchGen.c \
../src/benchPrep.c \
../src/dataPrep.c \
../src/genAlg.c \
../src/patRecog.c \
../src/threadOps.c \
../src/workGen.c 

OBJS += \
./src/benchGen.o \
./src/benchPrep.o \
./src/dataPrep.o \
./src/genAlg.o \
./src/patRecog.o \
./src/threadOps.o \
./src/workGen.o 

C_DEPS += \
./src/benchGen.d \
./src/benchPrep.d \
./src/dataPrep.d \
./src/genAlg.d \
./src/patRecog.d \
./src/threadOps.d \
./src/workGen.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"../include" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


