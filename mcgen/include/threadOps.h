/*
 * threadOps.h
 *
 *  Created on: Aug 11, 2014 
 *      Author: Alex Ford
 */

#ifndef THREADOPS_H_ 
#define THREADOPS_H_ 

#include "global.h"

void initThreads(FILE *fw, struct tThreadData *td, hwImplementationType implementation, memType mem_type, int numOfThreads, char* tab);
void finalizeThreads(FILE *fw, hwImplementationType implementation, char* tab);
int createThreads(FILE *fw, struct tThreadData *td, hwImplementationType implementation, memType mem_type, int taskId, int childTdIdx, char* tab);
void joinThreads(FILE *fw, struct tThreadData *joiningTd, hwImplementationType implementation, memType mem_type, int children, char* tab); 
void printCoreIdQuark(FILE *fw, struct tThreadData *td);
void printPortIdQuark(FILE *fw, struct tMcapiEndpoint *ep);

/******************************************** global function *************************************/

#endif /* THREADOPS_H_ */

