/*
 * workGen.h
 *
 *  Created on: May 20, 2014 
 *      Author: Alex Ford
 */

#ifndef WORKGEN_H_
#define WORKGEN_H_

#include "global.h"

/******************************************** global function *************************************/
void printWorkFunction(FILE *fw, workFuncType type);

unsigned doWorkFunction(struct tThreadData *td, workFuncType type, unsigned workload);

const char* getWorkFunctionStr(workFuncType type);
const char* getThreadAddrType (struct tThreadData *td);
boolean threadHasSharedAdressType (struct tThreadData *td);

#endif /* WORKGEN_H_ */
