Copyright (c) 2011-2014, Etem Deniz <etem.deniz@boun.edu.tr>, 
Alper Sen <alper.sen@boun.edu.tr>, Bogazici University
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
(1) Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
(2) Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
(3) The names of the contributors may not be used to endorse or promote
products derived from this software without specific prior written
permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************************************

#MBDT: Multicore Benchmark Development Tool#
#MCGEN: Multicore Generator#

MBDT/MCGEN generates a benchmark using user given properties.

************************************************************

##1. Installation:##
Use the makefile under /path/to/MBDT_Project/Debug directory for building MBDT. 
Run "make all" to build the project.

##2. Usage:##
Run '/path/to/MBDT_Project/Debug/MBDT_Project "<application_desc_file>"' where 
<application_desc_file> is the absolute file path that describe a multi-threaded application. 
The synthetic benchmark (C code file) is generated in the current working directory.
For testing stand-alone benchmark generation, simply run
"/path/to/MBDT_Project/Debug/MBDT_Project -a /path/to/MBDT_Project/inputs/saInputGD".
This command generates a multi-threaded benchmark using POSIX (Pthread) library 
according to properties given in saInputGD file.
The input file should be formatted according to our Parallel Pattern Markup Language (PPML). 
PPML defines threads with following attributes:
thread ID, parallel pattern, phase, creator thread ID, work size, shared data size, stage, loop count. 
For instance, "<thread_id: 2, par_pattern: GD, phase: 1, creator_id: 1, work_size: 10, 
shared_data_size: 100, recursion_count 15>" line states that ID of the thread is 2, 
parallel pattern is Geometric Decomposition, phase is 1, 
thread is created by the thread which's ID is 1 (this is main thread),
work (computation) size is 10, the recursive fibonacci index is 15, and shared data size is 100x4 bytes (where 4 is the size of integer). 

##3. Added Features - Alex Ford (alex.ford@freescale.com)##
The example below displays the added features for mbdt (renamed mcgen for freescale purposes)
"/path/to/MBDT_Project/Debug/MBDT_Project -a /path/to/MBDT_Project/inputs/saInputGD -b bare_metal -n quark -w".
 
-b bare_metal : This flag is added in order to build a C-file that does not have an operating system. Message passing is done between cores.
                'src/threadOps.c' has been abstracted for various bare_metal architectures to be defined in order to pass messages, as well as perform
                other thread specific operations. 'src/threadOps.c' also includes an OS-implementation
-n quark      : for bare_metal architectures, it's important to define the name of the architecture you are using, since the thread operations are unique to 
                each bare_metal architecture.
-w            : This is the 'workload checker enabled' flag. Workload checking is currently enabled for bare_metal architectures only. 

************************************************************

##Getting help and reporting bugs:##

This is a test release. (Date: 2/5/14)
Please email etem.deniz@boun.edu.tr for any issues of using MBDT.
