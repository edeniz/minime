/*
 * Copyright (c) 2011-2013, Etem Deniz <etem.deniz@boun.edu.tr>
 * Alper Sen <alper.sen@boun.edu.tr>, Bogazici University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * genAlg.c
 *
 *  Created on: Feb 10, 2012
 *      Author: Etem Deniz
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <limits.h>
#include "global.h"
#include "dataPrep.h"
#include "patRecog.h"
#include "benchPrep.h"
#include "benchGen.h"
#include "genAlg.h"

double dataSharingDisThreshold = 1;

paralPattern origPps[MAX_GROUP_NUMBER], candPps[MAX_GROUP_NUMBER];
int origPhaseCount, candPhaseCount;
int similarityThreshold = 80;
int indSimilarityThreshold = 0;
int similarities[MAX_THREADS][MAX_METRIC] = { { 0 } };
int metricCount = 0;
double mrapiDataSharingRatioInc = 0.2;
double mcapiDataSharingRatioInc = 0.002;
double posixDataSharingRatioInc = 0.4;
double dataSharingRatioInc = 0.02;
char *genAlgParFileName = "genAlgParams.txt";
char *resultFileName = "results.txt";
struct tMetric metrics[MAX_METRIC] = { { "PP", METRIC_HIGH }, { "IPC", METRIC_LOW },
		{ "CMR", METRIC_LOW }, { "BMR", METRIC_LOW }, { "CCR", METRIC_HIGH },
		{ "unknown", METRIC_UNKNOWN }, { "unknown", METRIC_UNKNOWN },
		{ "unknown", METRIC_UNKNOWN } };
struct tStepData oldStepData[MAX_THREADS][ITER_METRIC] = { { { 0, 0.0, 0, false, false } } }; /* ipc, cmr, bmp, ccr */
struct tStepData newStepData[MAX_THREADS][ITER_METRIC] = { { { 0, 0.0, 0, false, false } } }; /* ipc, cmr, bmp, ccr */
struct tStepData prevOldStepData[MAX_THREADS][ITER_METRIC] = { { { 0, 0.0, 0, false, false } } }; /* ipc, cmr, bmp, ccr */
int stepDataId = -1;
boolean isSimScoreMets[MAX_THREADS] = { false };

void saveParameters() {

	FILE *fw;
	int i = 0;

	fw = fopen(genAlgParFileName, "a");
	if (fw == NULL) {
		printf("ERROR! Cannot generate %s.\n", genAlgParFileName);
		return;
	}

	fprintf(fw, "dataSharingRatio: %.3f\n", dataSharingRatio);
	fprintf(fw, "dataPrivateRatio: %.3f\n", dataPrivateRatio);
	fprintf(fw, "dataReadonlyRatio: %.3f\n", dataReadonlyRatio);
	if (isPerThread == true) {
		for (i = 2; i <= maxTid; i++) {
			fprintf(fw, "IPC: %d %d %.3f %u %u\n", newStepData[i][0].id, i, newStepData[i][0].val, newStepData[i][0].count, newStepData[i][0].isInc);
			fprintf(fw, "CMR: %d %d %.3f %u %u\n", newStepData[i][1].id, i, newStepData[i][1].val, newStepData[i][1].count, newStepData[i][1].isInc);
			fprintf(fw, "BMR: %d %d %.3f %u %u\n", newStepData[i][2].id, i, newStepData[i][2].val, newStepData[i][2].count, newStepData[i][2].isInc);
			fprintf(fw, "CCR: %d %d %.3f %u %u\n", newStepData[i][3].id, i, newStepData[i][3].val, newStepData[i][3].count, newStepData[i][3].isInc);
			fprintf(fw, "SIM: %d %u\n", i, isSimScoreMets[i]);
		}
	} else {
		fprintf(fw, "IPC: %d %d %.3f %u %u\n", newStepData[i][0].id, i, newStepData[i][0].val, newStepData[i][0].count, newStepData[i][0].isInc);
		fprintf(fw, "CMR: %d %d %.3f %u %u\n", newStepData[i][1].id, i, newStepData[i][1].val, newStepData[i][1].count, newStepData[i][1].isInc);
		fprintf(fw, "BMR: %d %d %.3f %u %u\n", newStepData[i][2].id, i, newStepData[i][2].val, newStepData[i][2].count, newStepData[i][2].isInc);
		fprintf(fw, "CCR: %d %d %.3f %u %u\n", newStepData[i][3].id, i, newStepData[i][3].val, newStepData[i][3].count, newStepData[i][3].isInc);
	}

	fclose(fw);

	return;
}

double getParamDoubleValue(char *line) {

	double val;
	char *idBegin, cdata[STR_INT_MAX];

	while (*line != '\n') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cdata, idBegin, line -	 idBegin);
	cdata[line - idBegin] = '\0';

	val = atof(cdata);

	return val;
}

unsigned int getParamIntValue(char *line) {

	unsigned int val;
	char *idBegin, cdata[STR_INT_MAX];

	while (*line != '\n') line++;
	idBegin = line - 1;
	while (*idBegin != ' ') idBegin--;
	idBegin++;

	strncpy(cdata, idBegin, line -	 idBegin);
	cdata[line - idBegin] = '\0';

	val = atoll(cdata);

	return val;
}

void getStepData(char *line, struct tStepData prevOldStepData[MAX_THREADS][ITER_METRIC],
		struct tStepData oldStepData[MAX_THREADS][ITER_METRIC], int index) {

	int i = 0;
	struct tStepData localStepData;

	sscanf(line, "%d %d %lf %u %u", &localStepData.id, &i, &localStepData.val, &localStepData.count, &localStepData.isInc);
	if (localStepData.count != 0) {
		localStepData.isValid = true;
	} else {
		localStepData.isValid = false;
	}

	stepDataId = localStepData.id;
	// shifting
	memcpy(&prevOldStepData[i][index], &oldStepData[i][index], sizeof(struct tStepData));
	memcpy(&oldStepData[i][index], &localStepData, sizeof(struct tStepData));

	return;
}

void getSimData(char *line) {

	int i = 0;
	boolean isSimilar;

	sscanf(line, "%d %u", &i, &isSimilar);
	isSimScoreMets[i] = isSimilar;

	return;
}

void loadParameters() {

	char line[STR_LINE_MAX];
	FILE *fr;

	/* load values */
	fr = fopen(genAlgParFileName, "r+");
	if (fr != NULL) {
		while (!feof(fr)){
			fgets(line, STR_LINE_MAX, fr);
			if (strncmp(line, "dataSharingRatio", 16) == 0) {
				dataSharingRatio = getParamDoubleValue(line);
			} else if (strncmp(line, "dataPrivateRatio", 16) == 0) {
				dataPrivateRatio = getParamDoubleValue(line);
			} else if (strncmp(line, "dataReadonlyRatio", 17) == 0) {
				dataReadonlyRatio = getParamDoubleValue(line);
			} else if (strncmp(line, "IPC", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 0);
			} else if (strncmp(line, "CMR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 1);
			} else if (strncmp(line, "BMR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 2);
			} else if (strncmp(line, "CCR", 3) == 0) {
				getStepData(line + 5, prevOldStepData, oldStepData, 3);
			} else if (strncmp(line, "SIM", 3) == 0) {
				getSimData(line + 5);
			}
		}
		fclose(fr);
	} else {
		//printf("ERROR! Cannot open %s.\n", genAlgParFileName);
		return;
	}

	return;
}

int compareParallelPatterns() {

	int i, matchCount = 0;
	int sim = 0;

	if (origPhaseCount != candPhaseCount) {
		return sim;
	}

	for (i = 0; i < origPhaseCount; i++) {
		if (origPps[i] == candPps[i]) {
			matchCount++;
		}
	}

	sim = (matchCount / origPhaseCount) * SIMILARITY_MAX;

	return sim;
}

int compareComm() {

	int i, j, k;
	int numOfThreads, thresholdOrig, thresholdCand;
	struct tProcData *proc, *cand;
	struct tThreadData *origTd, *candTd;
	int sim = 0;
	int dd = 0, dn = 0, nd = 0, nn = 0, total = 0; /* d: dependent, n: not */
	boolean isOrigDep, isCandDep;

	for (i = 0; i < procCount; i++) {
		proc = procs[i];
		cand = cands[i];
		numOfThreads = proc->numOfThreads;

		for (j = 1; j < numOfThreads; j++) {
			origTd = proc->threads[j];
			candTd = cand->threads[j];
			if (origTd != NULL && candTd != NULL) {

				thresholdOrig = (int) (origTd->cacheInvalid * cahceMissRatio);
				thresholdCand = (int) (candTd->cacheInvalid * cahceMissRatio);
				for (k = 1; k <= numOfThreads; k++) {
					if (j != k) {
						if (origTd->groupId != -1 && proc->threads[k]->groupId != -1) {
							if (origTd->groupId != activeProc->threads[k]->groupId) {
								continue;
							}
						}
						//FIXME: origTd and candTd are in same group? Compare?!

						if (origTd->dependency[k] > thresholdOrig) {
							isOrigDep = true;
						} else {
							isOrigDep = false;
						}
						if (candTd->dependency[k] > thresholdCand) {
							isCandDep = true;
						} else {
							isCandDep = false;
						}

						if (isOrigDep && isCandDep) {
							dd++;
						} else if (isCandDep && !isCandDep) {
							dn++;
						} else if (!isOrigDep && isCandDep) {
							nd++;
						} else {
							nn++;
						}
						total++;
					}
				}

			}
		}

		if (numOfThreads == 1) {
			nn++;
		}

	}

	sim = (int) ((((double) (dd + nn)) / ((double) (dd + nn + dn + nd))) * SIMILARITY_MAX);

	return sim;
}

int compareDataSharing() {

	double dataSharing[MAX_DATA_SHR];
	double multiplier, dist;
	double max = MIN_DIST;
	int i, j, activeGroupId;
	int sim = 0, simCount = 0, totalCount = 0;

	dataSharingDisThreshold = scoreMax / 5; /* initialize */

	if (maxGroupId != maxCandGroupId) {
		return sim;
	}

	for (activeGroupId = 0; activeGroupId < maxGroupId; activeGroupId++) {

		dataSharing[0] = (double) activeProc->dataSharing[activeGroupId].readonly;
		dataSharing[1] = (double) activeProc->dataSharing[activeGroupId].migratory;
		dataSharing[2] = (double) activeProc->dataSharing[activeGroupId].prodCons;
		dataSharing[3] = (double) activeProc->dataSharing[activeGroupId].private;

		for (i = 0; i < MAX_DATA_SHR; i++) {
			if (dataSharing[i] > max) {
				max = dataSharing[i];
			}
		}

		multiplier = scoreMax / max;

		for (i = 0; i < MAX_DATA_SHR; i++) {
			dataSharing[i] *= multiplier;
		}

		for (j = 0; j < MAX_DATA_SHR; j++) {
			dist = dataSharing[j] - ppDataSharingPattern[j][origPps[activeGroupId]];
			if (dist > dataSharingDisThreshold) {
				simCount++;
			}
			totalCount++;
		}

	}

	return simCount / totalCount;
}

/*
 * error = (value - reference) / reference
 * if (error is larger than 1 then similarity score is 0)
 */

int compareIpc(int indexId) {

	int i = 0, itemCount = 0, sim = 0;
	double vals = 0, val;

	if (isPerThread == true) {
		val = fabs(cands[i]->threads[indexId]->ipcInfo.ipc - procs[i]->threads[indexId]->ipcInfo.ipc) / procs[i]->threads[indexId]->ipcInfo.ipc;
		if (val > 1) {
			val = 1;
		}
		vals += 1 - val;
		itemCount++;
	} else {
		val = fabs(cands[indexId]->ipcInfo.ipc - procs[indexId]->ipcInfo.ipc) / procs[indexId]->ipcInfo.ipc;
		if (val > 1) {
			val = 1;
		}
		vals += 1 - val;
		itemCount++;
	}

	sim = (vals / (double) itemCount) * SIMILARITY_MAX;

	return sim;
}

int compareCacheMisses(int indexId) {

	int i = 0, itemCount = 0, sim = 0;
	double vals = 0, val;

	if (isPerThread == true) {
		val = fabs(cands[i]->threads[indexId]->cacheInfo.cacheMissRatio - procs[i]->threads[indexId]->cacheInfo.cacheMissRatio) / procs[i]->threads[indexId]->cacheInfo.cacheMissRatio;
		if (val > 1) {
			val = 1;
		}
		vals += 1 - val;
		itemCount++;
	} else {
		val = fabs(cands[indexId]->cacheInfo.cacheMissRatio - procs[indexId]->cacheInfo.cacheMissRatio) / procs[indexId]->cacheInfo.cacheMissRatio;
		if (val > 1) {
			val = 1;
		}
		vals += 1 - val;
		itemCount++;
	}

	sim = (vals / (double) itemCount) * SIMILARITY_MAX;

	return sim;
}

int compareBranchMisses(int indexId) {

	int i = 0, itemCount = 0, sim = 0;
	double vals = 0, val;

	if (isPerThread == true) {
		val = fabs(cands[i]->threads[indexId]->branchInfo.branchMissRatio - procs[i]->threads[indexId]->branchInfo.branchMissRatio) / procs[i]->threads[indexId]->branchInfo.branchMissRatio;
		if (val > 1) {
			val = 1;
		}
		vals += 1 - val;
		itemCount++;
	} else {
		val = fabs(cands[indexId]->branchInfo.branchMissRatio - procs[indexId]->branchInfo.branchMissRatio) / procs[indexId]->branchInfo.branchMissRatio;
		if (val > 1) {
			val = 1;
		}
		vals += 1 - val;
		itemCount++;
	}

	sim = (vals / (double) itemCount) * SIMILARITY_MAX;

	return sim;

}

int compareCommToComp() {

	int i = 0, itemCount = 0, sim = 0;
	double vals = 0, val = 0;

	val = fabs(cands[i]->instructions.ccRatio - procs[i]->instructions.ccRatio) / procs[i]->instructions.ccRatio;
	if (val > 1) {
		val = 1;
	}
	vals += 1 - val;
	itemCount++;

	sim = (vals / (double) itemCount) * SIMILARITY_MAX;

	return sim;
}

int measureOverallSim(int appCount, char *apps[MAX_PROCS], int candCount, char *cands[MAX_PROCS], int indexId) {

	int sim, totalSim = 0;
	int i, activeMetricCount = 0;

	metricCount = 0;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		similarities[indexId][metricCount] = compareParallelPatterns();
		activeMetricCount++;
	}
	metricCount++;

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		similarities[index][metricCount] = compareComm();
//		activeMetricCount++;
//	}
//	metricCount++;

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		similarities[index][metricCount] = compareDataSharing();
//		activeMetricCount++;
//	}
//	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareIpc(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareCacheMisses(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareBranchMisses(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		similarities[indexId][metricCount] = compareCommToComp();
		activeMetricCount++;
	}
	metricCount++;

	for (i = 0; i < metricCount; i++) {
		totalSim += similarities[indexId][i];
	}

	sim = totalSim / activeMetricCount;

	return sim;
}

int measureOverallSimSa(int appCount, char *apps[MAX_PROCS], int candCount, char *cands[MAX_PROCS], int indexId) {

	int sim, totalSim = 0;
	int i, activeMetricCount = 0;

	metricCount = 0;

	metricCount++; /* pp */
	metricCount++; /* comm */

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareIpc(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareCacheMisses(indexId);
		activeMetricCount++;
	}
	metricCount++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		similarities[indexId][metricCount] = compareBranchMisses(indexId);
		activeMetricCount++;
	}
	metricCount++;

	metricCount++; /* comm to comp */

	for (i = 0; i < metricCount; i++) {
		totalSim += similarities[indexId][i];
	}

	sim = totalSim / activeMetricCount;
	return sim;
}

void setStepData(struct tStepData *newStepData, double val, unsigned int count, boolean isInc) {

	newStepData->id = stepDataId + 1;
	newStepData->val = val;
	newStepData->count = count;
	newStepData->isInc = isInc;

	return;
}

/*
 * numerator/denominator
 */
 
unsigned int calculateInitVal(long long unsigned int num, long long unsigned int den, double origRatio, double blockRatio) {

	unsigned int count;

#ifdef DEBUG_ON
	printf("[DEBUG] Calculating initial value\n");
#endif

	count = fabs(num - (origRatio * den)) / fabs(origRatio - blockRatio); // origRatio = (num + (count * blockRatio)) / (den + count)

	return count;

}

boolean calculateNewVal(int index, struct tStepData *pPrevOldStepData, struct tStepData *pOldStepData, double newCandVal, double origVal, unsigned int *newCount, boolean *newDir) {

	double diff;
	boolean retVal = true;
	int localCount;

#ifdef DEBUG_ON
	printf("[DEBUG] Calculating new value by using old one.\n");
#endif

	if (pOldStepData->id > 0) {
		if (pPrevOldStepData->count == pOldStepData->count) {
			retVal = false;
		}
	}

	if (retVal) {
		if (pOldStepData->val >= newCandVal) { /* from high to low */
			diff = pOldStepData->val - newCandVal;
			if (diff > 0) {
				localCount = (pOldStepData->count * (newCandVal - origVal)) / diff;
				if (localCount >= 0) { /* We have a value between oldVal and origVal */
					*newCount = pOldStepData->count + localCount; /* incremental learning */
					*newDir = pOldStepData->isInc; /* should be 'false' */
				} else { /* We have a value smaller than origVal */
					*newCount = pOldStepData->count + localCount; /* localCount is negative */
					*newDir = pOldStepData->isInc;
				}
			} else {
//				retVal = false;
				*newCount = pOldStepData->count * 2;
				*newDir = pOldStepData->isInc;
			}
		} else { /* from low to high */
			diff = newCandVal - pOldStepData->val;
			if (diff > 0) {
				localCount = (pOldStepData->count * (origVal - newCandVal)) / diff;
				if (localCount >= 0) { /* We have a value between oldVal and origVal */
					*newCount = pOldStepData->count + localCount; /* incremental learning */
					*newDir = pOldStepData->isInc; /* should be 'true' */
				} else { /* We have a value bigger than origVal */
					*newCount = pOldStepData->count + localCount; /* localCount is negative */
					*newDir = pOldStepData->isInc;
				}
			} else {
//				retVal = false;
				*newCount = pOldStepData->count * 2;
				*newDir = pOldStepData->isInc;
			}
		}
	}

	if (*newCount > MAX_LOOP_COUNT) {
		retVal = false;
	}

	return retVal;
}

int getIndexOfMinValue(int *arr, int arrSize) {

	int index = 0, minVal = INT_MAX;
	int i;

	for (i = 0; i < arrSize; i++) {
		if (arr[i] < minVal) {
			minVal = arr[i];
			index = i;
		}
	}

	return index;
}

void updateComm(int phaseIndex) {

	int i, targetIndex;
	double diff, maxDiff = 0;

	for (i = 0; i < MAX_COMM; i++) {
		diff = procs[0]->parPatternScores.tcSubScores[phaseIndex][i]
		        - cands[0]->parPatternScores.tcSubScores[phaseIndex][i];
		if (diff > maxDiff) {
			maxDiff = diff;
			targetIndex = i;
		}
	}

	//FIXME: do better operation for each
	if (targetIndex == 0) { /* none */
//		if (dataSharingRatio > dataSharingRatioInc) {
//			dataSharingRatio -= dataSharingRatioInc;
//		}
		dataSharingRatio += dataSharingRatioInc;
	} else if (targetIndex == 1) { /* few */
		dataSharingRatio += dataSharingRatioInc;
	} else { /* many */
		dataSharingRatio += dataSharingRatioInc;
	}

	return;
}

void updateDataSharing(int phaseIndex) {

	int i, targetIndex;
	double diff, maxDiff = 0;

	for (i = 0; i < MAX_DATA_SHR; i++) {
		diff = procs[0]->parPatternScores.dsSubScores[phaseIndex][i]
		        - cands[0]->parPatternScores.dsSubScores[phaseIndex][i];
		if (diff > maxDiff) {
			maxDiff = diff;
			targetIndex = i;
		}
	}

	//FIXME: do better operation for each
	if (targetIndex == 0) { /* read-only */
		dataReadonlyRatio += dataSharingRatioInc;
	} else if (targetIndex == 1) { /* migratory */
		dataSharingRatio += dataSharingRatioInc;
	} else if (targetIndex == 2) { /* prod/con */
		dataSharingRatio += dataSharingRatioInc;
	} else { /* private */
		dataPrivateRatio += dataSharingRatioInc;
	}

	return;
}

void updateGenThreading(int phaseIndex) {

	int i, targetIndex;
	double diff, maxDiff = 0;

	for (i = 0; i < MAX_GEN_THREADING; i++) {
		diff = procs[0]->parPatternScores.gtSubScores[phaseIndex][i]
		        - cands[0]->parPatternScores.gtSubScores[phaseIndex][i];
		if (diff > maxDiff) {
			maxDiff = diff;
			targetIndex = i;
		}
	}

	//FIXME: do better operation for each
	if (targetIndex == 0) { /* pc (unique) */
		/* this case should never occur */
	} else if (targetIndex == 1) { /* thread balanced */
		dataSharingRatio += dataSharingRatioInc;
	} else if (targetIndex == 2){ /* creator (same) */
		/* this case should never occur */
	} else if (targetIndex == 3) { /* lifetime */
		dataSharingRatio += dataSharingRatioInc;
	} else if (targetIndex == 4) { /* creation time */
		dataSharingRatio += dataSharingRatioInc;
	} else { /* exit time */
		dataSharingRatio += dataSharingRatioInc;
	}

	return;
}

void updateSyncCon(int phaseIndex) {

	//FIXME: not used currently so noop

	return;
}

//FIXME: Library causes wrong data sharing values
void updateParPattern() {

	int i, minScore = SIMILARITY_MAX, minIndex = 0;

	//FIXME: reset all other counts
//	ipcCount = 0;
//	cacheMissCount = 0;
//	branchMissCount = 0;
//	commToCompCount = 0;

	if (origPhaseCount != candPhaseCount) {
		//TODO: add/remove phases to/from candidate benchmark
	}

	for (i = 0; i < origPhaseCount; i++) {
		if (origPps[i] != candPps[i]) {
			/* find */
			if (weightDS > 0 && cands[0]->parPatternScores.dsScores[i][origPps[i]] < minScore) {
				minIndex = 0;
				minScore = cands[0]->parPatternScores.dsScores[i][origPps[i]];
			}
			if (weightTC > 0 && cands[0]->parPatternScores.commScores[i][origPps[i]] < minScore) {
				minIndex = 1;
				minScore = cands[0]->parPatternScores.commScores[i][origPps[i]];
			}
			if (weightGT > 0 && cands[0]->parPatternScores.genThreadingScores[i][origPps[i]] < minScore) {
				minIndex = 2;
				minScore = cands[0]->parPatternScores.genThreadingScores[i][origPps[i]];
			}
			if (weightSC > 0 && cands[0]->parPatternScores.syncConScores[i][origPps[i]] < minScore) {
				minIndex = 3;
				minScore = cands[0]->parPatternScores.syncConScores[i][origPps[i]];
			}

			/* update */
			if (minIndex == 0) { /* ds */
				updateDataSharing(i);
			} else if (minIndex == 1) { /* comm */
				updateComm(i);
			} else if (minIndex == 2) { /* genThreading */
				updateGenThreading(i);
			} else { /* syncCon */
				updateSyncCon(i);
			}
		}
	}

	return;
}

/*
 * Reciprocal throughput:
 * The throughput is the maximum number of instructions of the same kind that can be executed per clock cycle
 * ADD SUB r,r/i 1 1 x x x 1 0.33 --> 	IPC = 1/0.33 = 3
 * IDIV r32 9 9 20-27 11-18 --> IPC = 1/10 = 0.1
 * */
 
void updateIpc(struct tIpcInfo *pOrigIpcInfo, struct tIpcInfo *pCandIpcInfo, struct tStepData *pPrevOldStepData,
		struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateData *pUpdateData) {

	double ipcInc = 2.5;
	double ipcDec = 0.2;
	double origIpc;

	//FIXME: I need to save per process parameters and then use them in this kind of functions

	origIpc = pOrigIpcInfo->ipc;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewVal(0, pPrevOldStepData, pOldStepData, pCandIpcInfo->ipc, origIpc, &pUpdateData->ipcCount, &pUpdateData->isIpcInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->ipcCount = 0;
			pUpdateData->isIpcInc = true;
		}
	} else {
		if (pCandIpcInfo->ipc >= origIpc) { // decrement
			pUpdateData->ipcCount = calculateInitVal(pCandIpcInfo->instructions, pCandIpcInfo->cycles, origIpc, ipcDec);
			pUpdateData->ipcCount = pUpdateData->ipcCount / 600;
			pUpdateData->isIpcInc = false;
		} else {
			pUpdateData->ipcCount = calculateInitVal(pCandIpcInfo->instructions, pCandIpcInfo->cycles, origIpc, ipcInc);
			pUpdateData->ipcCount = pUpdateData->ipcCount / 400;
			pUpdateData->isIpcInc = true;
		}
		if (isPerThread == true) {
			pUpdateData->ipcCount /= 4;
		}
	}

	setStepData(pNewStepData, pCandIpcInfo->ipc, pUpdateData->ipcCount, pUpdateData->isIpcInc);

#ifdef DEBUG_ON
	printf("[DEBUG] IPC: new count = %u, is increment = %s\n", pUpdateData->ipcCount, pUpdateData->isIpcInc == true ? "true" : "false");
#endif


	return;
}

/*
 * int i;
 * int blockSize = 1024*1024*4; //4MB
 * int loopCount = 1000;
 *
 * int *block = (int*) malloc(blockSize * sizeof(int));
 * for (i = 0; i < loopCount; i++) {
 * 	block[rand() % blockSize] = 0;
 * }
 * free(block);
 *
 * loopCount = 100, miss rate = 10%.
 * loopCount = 1000, miss rate = 18%.
 * loopCount = 10000, miss rate = 24%.
 * loopCount = 100000, miss rate = 36%.
 * loopCount = 1000000, miss rate = 55%.
 * loopCount = 10000000, miss rate = 60%.
 *
 * 	int a, lc, i, j, x[256][256];
 *	lc = 1;
 *
 *	for (a = 0; a < lc; a++) {
 *		for (j = 0; j < 256; j++) {
 *			for (i = 0; i < 256; i++) {
 *				x[i][j] = 2 * x[i][j];
 *			}
 *		}
 *	}
 *
 * lc = 1, miss rate = 1.8%
 * lc = 10, miss rate = 0.7%
 * lc = 100, miss rate = 0.07%
 * lc = 1000, miss rate = 0.005%
 * */

void updateCmr(struct tCacheInfo *pOrigCacheInfo, struct tCacheInfo *pCandCacheInfo, struct tStepData *pPrevOldStepData,
		struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateData *pUpdateData) {

	double cmrInc = 0.5;
	double cmrDec = 0.002;
	double origCmr;

	origCmr = pOrigCacheInfo->cacheMissRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewVal(1, pPrevOldStepData, pOldStepData, pCandCacheInfo->cacheMissRatio, origCmr, &pUpdateData->cacheMissCount, &pUpdateData->isCacheMissInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->cacheMissCount = 0;
			pUpdateData->isCacheMissInc = true;
		}
	} else {
		if (pCandCacheInfo->cacheMissRatio >= origCmr) { // decrement
			pUpdateData->cacheMissCount = calculateInitVal(pCandCacheInfo->cacheMisses, pCandCacheInfo->cacheReferences, origCmr, cmrDec);
			pUpdateData->cacheMissCount = pUpdateData->cacheMissCount / 500;
			pUpdateData->isCacheMissInc = false;
		} else {
			pUpdateData->cacheMissCount = calculateInitVal(pCandCacheInfo->cacheMisses, pCandCacheInfo->cacheReferences, origCmr, cmrInc);
			pUpdateData->isCacheMissInc = true;
		}
		if (isPerThread == true) {
			pUpdateData->cacheMissCount /= 8;
		}
	}

	setStepData(pNewStepData, pCandCacheInfo->cacheMissRatio, pUpdateData->cacheMissCount, pUpdateData->isCacheMissInc);

#ifdef DEBUG_ON
	printf("[DEBUG] CMR: new count = %u, is increment = %s\n", pUpdateData->cacheMissCount, pUpdateData->isCacheMissInc == true ? "true" : "false");
#endif

	return;
}

/*
 * I can use an array of function pointers
 * in order to increase branch-miss
 * funcPointers[10];
 * for (i = 0; i < 10; i++) {
 * 		funcPointers[i];
 * 	}
 */

void updateBmr(struct tBranchInfo *pOrigBranchInfo, struct tBranchInfo *pCandBranchInfo, struct tStepData *pPrevOldStepData,
		struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateData *pUpdateData) {

	double bmpInc = 10;
	double bmpDec = 0.3;
	double origBmp;

	origBmp = pOrigBranchInfo->branchMissRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewVal(2, pPrevOldStepData, pOldStepData, pCandBranchInfo->branchMissRatio, origBmp, &pUpdateData->branchMissCount, &pUpdateData->isBranchMissInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->branchMissCount = 0;
			pUpdateData->isBranchMissInc = true;
		}
	} else {
		if (pCandBranchInfo->branchMissRatio >= origBmp) { // decrement
			pUpdateData->branchMissCount = calculateInitVal(pCandBranchInfo->branchMisses, pCandBranchInfo->branches, origBmp, bmpDec);
			pUpdateData->branchMissCount = pUpdateData->branchMissCount / 100;
			pUpdateData->isBranchMissInc = false;
		} else {
			pUpdateData->branchMissCount = calculateInitVal(pCandBranchInfo->branchMisses, pCandBranchInfo->branches, origBmp, bmpInc);
			pUpdateData->branchMissCount = pUpdateData->branchMissCount / 3;
			pUpdateData->isBranchMissInc = true;
		}
		if (isPerThread == true) {
			pUpdateData->branchMissCount /= 2;
		}
	}

	setStepData(pNewStepData, pCandBranchInfo->branchMissRatio, pUpdateData->branchMissCount, pUpdateData->isBranchMissInc);

#ifdef DEBUG_ON
	printf("[DEBUG] BMR: new count = %u, is increment = %s\n", pUpdateData->branchMissCount, pUpdateData->isBranchMissInc == true ? "true" : "false");
#endif

	return;
}

void updateCcr(struct tInstructions *pOrigInstructions, struct tInstructions *pCandInstructions, struct tStepData *pPrevOldStepData,
		struct tStepData *pOldStepData, struct tStepData *pNewStepData, struct tUpdateData *pUpdateData) {

	long long unsigned int commInstCount, compInstCount;
	double origCcr;

	origCcr = pOrigInstructions->ccRatio;

	if (pOldStepData->isValid == true) {
		pOldStepData->isValid = calculateNewVal(3, pPrevOldStepData, pOldStepData, pCandInstructions->ccRatio, origCcr, &pUpdateData->commToCompCount, &pUpdateData->isCommInc);
		if (pOldStepData->isValid == false) {
			pUpdateData->commToCompCount = 0;
			pUpdateData->isCommInc = true;
		}
	} else {
		commInstCount = pCandInstructions->loadCount + pCandInstructions->storeCount;
		compInstCount = pCandInstructions->instCount - commInstCount;

		if (pCandInstructions->ccRatio > origCcr) { // decrement
			/* add computation */
			pUpdateData->commToCompCount = ((double) commInstCount / origCcr) - compInstCount;
			pUpdateData->commToCompCount /= 20;
			pUpdateData->isCommInc = false;
		} else {
			/* add communication */
			pUpdateData->commToCompCount = (compInstCount * origCcr) - commInstCount;
			pUpdateData->commToCompCount = pUpdateData->commToCompCount / 15;
			pUpdateData->isCommInc = true;
		}
		if (isPerThread == true) {
			pUpdateData->commToCompCount /= 2;
		}
	}

	setStepData(pNewStepData, pCandInstructions->ccRatio, pUpdateData->commToCompCount, pUpdateData->isCommInc);

#ifdef DEBUG_ON
	printf("[DEBUG] CCR: new count = %u, is increment = %s\n", pUpdateData->commToCompCount, pUpdateData->isCommInc == true ? "true" : "false");
#endif

	return;
}

int findMinScore(int size, int indexId) {

	int minIndex = 0;
	int i;
	int min = similarityThreshold;

	for (i = 0; i < size; i++) {
		if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
			if (similarities[indexId][i] < min) {
				minIndex = i;
				min = similarities[indexId][i];
			}
		}
	}

	return minIndex;

}

void restoreOldStepData(int indexId) {

	if (isPerThread == true) {
		updateData[indexId].ipcCount = oldStepData[indexId][0].count;
		updateData[indexId].isIpcInc = oldStepData[indexId][0].isInc;
		setStepData(&newStepData[indexId][0], cands[0]->threads[indexId]->ipcInfo.ipc,
				updateData[indexId].ipcCount, updateData[indexId].isIpcInc);

		updateData[indexId].cacheMissCount = oldStepData[indexId][1].count;
		updateData[indexId].isCacheMissInc = oldStepData[indexId][1].isInc;
		setStepData(&newStepData[indexId][1], cands[0]->threads[indexId]->cacheInfo.cacheMissRatio,
				updateData[indexId].cacheMissCount, updateData[indexId].isCacheMissInc);

		updateData[indexId].branchMissCount = oldStepData[indexId][2].count;
		updateData[indexId].isBranchMissInc = oldStepData[indexId][2].isInc;
		setStepData(&newStepData[indexId][2], cands[0]->threads[indexId]->branchInfo.branchMissRatio,
				updateData[indexId].branchMissCount, updateData[indexId].isBranchMissInc);

		/* CCR not used with stand-alone; since findMinScore cannot find it because of its level, no problem*/
		updateData[indexId].commToCompCount = oldStepData[indexId][3].count;
		updateData[indexId].isCommInc = oldStepData[indexId][3].isInc;
		setStepData(&newStepData[indexId][3], cands[0]->instructions.ccRatio,
				updateData[indexId].commToCompCount, updateData[indexId].isCommInc);
	} else {
		updateData[indexId].ipcCount = oldStepData[indexId][0].count;
		updateData[indexId].isIpcInc = oldStepData[indexId][0].isInc;
		setStepData(&newStepData[indexId][0], cands[indexId]->ipcInfo.ipc,
				updateData[indexId].ipcCount, updateData[indexId].isIpcInc);

		updateData[indexId].cacheMissCount = oldStepData[indexId][1].count;
		updateData[indexId].isCacheMissInc = oldStepData[indexId][1].isInc;
		setStepData(&newStepData[indexId][1], cands[indexId]->cacheInfo.cacheMissRatio,
				updateData[indexId].cacheMissCount, updateData[indexId].isCacheMissInc);

		updateData[indexId].branchMissCount = oldStepData[indexId][2].count;
		updateData[indexId].isBranchMissInc = oldStepData[indexId][2].isInc;
		setStepData(&newStepData[indexId][2], cands[indexId]->branchInfo.branchMissRatio,
				updateData[indexId].branchMissCount, updateData[indexId].isBranchMissInc);

		/* CCR not used with stand-alone; since findMinScore cannot find it because of its level, no problem*/
		updateData[indexId].commToCompCount = oldStepData[indexId][3].count;
		updateData[indexId].isCommInc = oldStepData[indexId][3].isInc;
		setStepData(&newStepData[indexId][3], cands[indexId]->instructions.ccRatio,
				updateData[indexId].commToCompCount, updateData[indexId].isCommInc);
	}

	return;
}

void updateIpcWrapper(int indexId) {

	if (isPerThread == true) {
		updateIpc(&procs[0]->threads[indexId]->ipcInfo, &cands[0]->threads[indexId]->ipcInfo, &prevOldStepData[indexId][0],
				&oldStepData[indexId][0], &newStepData[indexId][0], &updateData[indexId]);
	} else {
		updateIpc(&procs[indexId]->ipcInfo, &cands[indexId]->ipcInfo, &prevOldStepData[indexId][0],
				&oldStepData[indexId][0], &newStepData[indexId][0], &updateData[indexId]);
	}

	return;
}

void updateCmrWrapper(int indexId) {

	if (isPerThread == true) {
		updateCmr(&procs[0]->threads[indexId]->cacheInfo, &cands[0]->threads[indexId]->cacheInfo, &prevOldStepData[indexId][1],
				&oldStepData[indexId][1], &newStepData[indexId][1], &updateData[indexId]);
	} else {
		updateCmr(&procs[indexId]->cacheInfo, &cands[indexId]->cacheInfo, &prevOldStepData[indexId][1],
				&oldStepData[indexId][1], &newStepData[indexId][1], &updateData[indexId]);
	}

	return;
}

void updateBmrWrapper(int indexId) {

	if (isPerThread == true) {
		updateBmr(&procs[0]->threads[indexId]->branchInfo, &cands[0]->threads[indexId]->branchInfo, &prevOldStepData[indexId][2],
				&oldStepData[indexId][2], &newStepData[indexId][2], &updateData[indexId]);
	} else {
		updateBmr(&procs[indexId]->branchInfo, &cands[indexId]->branchInfo, &prevOldStepData[indexId][2],
				&oldStepData[indexId][2], &newStepData[indexId][2], &updateData[indexId]);
	}

	return;
}

void updateCcrWrapper(int indexId) {

	static boolean isUpdateCcr = false;
	/* to prevent adding code blocks for multiple threads.
	 * note that ccr is process-based not thread-based */

	if (isPerThread == true) {
		if (isUpdateCcr == false) {
			updateCcr(&procs[0]->instructions, &cands[0]->instructions, &prevOldStepData[indexId][2],
					&oldStepData[indexId][3], &newStepData[indexId][3], &updateData[indexId]);
			isUpdateCcr = true;
		}
	} else {
		updateCcr(&procs[0]->instructions, &cands[0]->instructions,
				&prevOldStepData[indexId][3], &oldStepData[indexId][3], &newStepData[indexId][3], &updateData[indexId]);
	}

	return;
}

void updateCandBenchOne(int indexId) {

	int index;
	static boolean isUpdateParPattern = false;

	restoreOldStepData(indexId);

	index = findMinScore(metricCount, indexId);

#ifdef DEBUG_ON
	printf("[DEBUG] Minimum score index = %d, type = %s\n", index, metrics[index].name);
#endif

	switch (index) {
	case 0:
		if (isUpdateParPattern == false) {
			updateParPattern();
			isUpdateParPattern = true;
		}
		break;
	case 1:
		updateIpcWrapper(indexId);
		break;
	case 2:
		updateCmrWrapper(indexId);
		break;
	case 3:
		updateBmrWrapper(indexId);
		break;
	case 4:
		updateCcrWrapper(indexId);
		break;
	default:
		break;
	}

	return;
}

/* order --> ipc, ccr, cmr, bmp */
void updateCandBenchOrdered(int indexId) {

	int index = 0;
//	static boolean isUpdateParPattern = false;

	restoreOldStepData(indexId);

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < similarityThreshold) {
//			if (isUpdateParPattern == false) {
//				updateParPattern();
//				isUpdateParPattern = true;
//			}
//			return;
//		}
//	}
//	index++;
//
//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < similarityThreshold) {
//			updateComm(0);
//			return;
//		}
//	}
//	index++;
//
//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < SIMILARITY_THRESHOLD) {
//			updateDataSharing(0);
//			return;
//		}
//	}
//	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateIpcWrapper(indexId);
			return;
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateCmrWrapper(indexId);
			return;
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateBmrWrapper(indexId);
			return;
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateCcrWrapper(indexId);
			return;
		}
	}
	index++;

	return;
}

void updateCandBenchAll(int indexId) {

	int index = 0;
	static boolean isUpdateParPattern = false;

	restoreOldStepData(indexId);

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		if (similarities[indexId][index] < similarityThreshold) {
			if (isUpdateParPattern == false) {
				updateParPattern();
				isUpdateParPattern = true;
			}
		}
	}
	index++;

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < similarityThreshold) {
//			updateComm(0);
//		}
//	}
//	index++;

//	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//		if (similarities[indexId][index] < SIMILARITY_THRESHOLD) {
//			updateDataSharing(0);
//		}
//	}
//	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateIpcWrapper(indexId);
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateCmrWrapper(indexId);
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateBmrWrapper(indexId);
		}
	}
	index++;

	if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
		if (similarities[indexId][index] < similarityThreshold) {
			updateCcrWrapper(indexId);
		}
	}
	index++;

	return;
}

void manageSideEffects() {

	//TODO: do operations according to following knowledge

/*
 * 	Table: Side effects (+: inc, -: dec)
 *			IPC-inc	IPC-dec	CMR-inc	CMR-dec	BMR-inc	BMR-dec	CCR-inc	CCR-dec
 *	IPC		+		-
 *	CMR						+		-								+
 *	BMR										+		-
 *	CCR				+										+		-
 *
 */

	return;
}

/*
 * Program; Parallel Pattern(s); Ovl Sim; PP Sim; IPC Sim; CMR Sim; BMP Sim; CCR Sim
 * Program; oIPC; oCMR; oBMP; oCCR; sIPC; sCMR; sBMP; sCCR
 */

void writeSimScores(int similarityScores[MAX_THREADS]) {

	int i, index = 0, indexId = 0;
	FILE *fa;
	time_t now;

	struct tm *today;
	char date[32];

	fa = fopen(resultFileName, "a");
	if (fa == NULL) {
		printf("ERROR! Cannot generate %s.\n", resultFileName);
		return;
	}

	fprintf(fa, "%s;", procs[0]->name);

	for (i = 0; i < maxGroupId; i++) {
		if (i == maxGroupId - 1) {
			fprintf(fa, "%s;", getPatternText(origPps[i]));
		} else {
			fprintf(fa, "%s + ", getPatternText(origPps[i]));
		}
	}

	if (isPerThread == true) {
		for (i = 2; i <= maxTid; i++) {
			fprintf(fa, "Thread%d:", i);
			fprintf(fa, "%d;", similarityScores[indexId]); // Overall

			fprintf(fa, "%d;", similarities[indexId][index++]); // PP
//			fprintf(fa, "%d;", similarities[indexId][index++]);
//			fprintf(fa, "%d;", similarities[indexId][index++]);
			fprintf(fa, "%d;", similarities[indexId][index++]); // IPC
			fprintf(fa, "%d;", similarities[indexId][index++]); // CMR
			fprintf(fa, "%d;", similarities[indexId][index++]); // BMR
			fprintf(fa, "%d;", similarities[indexId][index++]); // CCR

			fprintf(fa, "%.3f;", procs[0]->threads[i]->ipcInfo.ipc);
			fprintf(fa, "%.3f;", procs[0]->threads[i]->cacheInfo.cacheMissRatio);
			fprintf(fa, "%.3f;", procs[0]->threads[i]->branchInfo.branchMissRatio);
			fprintf(fa, "%.3f;", procs[0]->instructions.ccRatio);

			fprintf(fa, "%.3f;", cands[0]->threads[i]->ipcInfo.ipc);
			fprintf(fa, "%.3f;", cands[0]->threads[i]->cacheInfo.cacheMissRatio);
			fprintf(fa, "%.3f;", cands[0]->threads[i]->branchInfo.branchMissRatio);
			fprintf(fa, "%.3f;", cands[0]->instructions.ccRatio);
		}
	} else {
		fprintf(fa, "%d;", similarityScores[indexId]); // Overall

		fprintf(fa, "%d;", similarities[indexId][index++]); //PP
//		fprintf(fa, "%d;", similarities[indexId][index++]);
//		fprintf(fa, "%d;", similarities[indexId][index++]);
		fprintf(fa, "%d;", similarities[indexId][index++]); // IPC
		fprintf(fa, "%d;", similarities[indexId][index++]); // CMR
		fprintf(fa, "%d;", similarities[indexId][index++]); // BMR
		fprintf(fa, "%d;", similarities[indexId][index++]); // CCR

		fprintf(fa, "%.3f;", procs[0]->ipcInfo.ipc);
		fprintf(fa, "%.3f;", procs[0]->cacheInfo.cacheMissRatio);
		fprintf(fa, "%.3f;", procs[0]->branchInfo.branchMissRatio);
		fprintf(fa, "%.3f;", procs[0]->instructions.ccRatio);

		fprintf(fa, "%.3f;", cands[0]->ipcInfo.ipc);
		fprintf(fa, "%.3f;", cands[0]->cacheInfo.cacheMissRatio);
		fprintf(fa, "%.3f;", cands[0]->branchInfo.branchMissRatio);
		fprintf(fa, "%.3f;", cands[0]->instructions.ccRatio);
	}

	time(&now);
	today = localtime(&now);
	strftime(date, 32, "%d.%m.%Y-%H:%M:%S", today);

	fprintf(fa, "%s", date);

	fprintf(fa, "\n");

	fclose(fa);

	return;
}

void printSimScores() {

	int index = 0, indexId = 0;

	printf(" Similarity Scores\n");

	if (isPerThread == true) {
		for (indexId = 2; indexId <= maxTid; indexId++) {
			index = 0;
			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
			printf("  Thread%d - Parallel Pattern(s): %d\n", indexId, similarities[indexId][index]);
			}
			index++;

//			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//				printf("  Thread%d - Thread Communication: %d\n", indexId, similarities[indexId][index]);
//			}
//			index++;
//
//			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//				printf("  Thread%d - Data Sharing: %d\n", indexId, similarities[indexId][index]);
//			}
//			index++;

			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
				printf("  Thread%d - IPC: %d (%.3f-%.3f)\n", indexId,
						similarities[indexId][index], procs[0]->threads[indexId]->ipcInfo.ipc, cands[0]->threads[indexId]->ipcInfo.ipc);
			}
			index++;

			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
				printf("  Thread%d - CMR: %d (%.1f%s-%.1f%s)\n", indexId, similarities[indexId][index],
						procs[0]->threads[indexId]->cacheInfo.cacheMissRatio * 100, "%", cands[0]->threads[indexId]->cacheInfo.cacheMissRatio * 100, "%");
			}
			index++;

			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
				printf("  Thread%d - BMR: %d (%.1f%s-%.1f%s)\n", indexId, similarities[indexId][index],
						procs[0]->threads[indexId]->branchInfo.branchMissRatio * 100, "%", cands[0]->threads[indexId]->branchInfo.branchMissRatio * 100, "%");
			}
			index++;

			if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
				printf("  Thread%d - CCR: %d (%.3f-%.3f)\n", indexId,
						similarities[indexId][index], procs[0]->instructions.ccRatio, cands[0]->instructions.ccRatio);
			}
			index++;
		}
	} else {
		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
			printf("  Parallel Pattern(s): %d\n", similarities[indexId][index]);
		}
		index++;

//		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//			printf("  Thread Communication: %d\n", similarities[indexId][index]);
//		}
//		index++;
//
//		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
//			printf("  Data Sharing: %d\n", similarities[indexId][index]);
//		}
//		index++;

		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
			printf("  IPC: %d (%.3f-%.3f)\n", similarities[indexId][index], procs[0]->ipcInfo.ipc, cands[0]->ipcInfo.ipc);
		}
		index++;

		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
			printf("  CMR: %d (%.1f%s-%.1f%s)\n", similarities[indexId][index],
					procs[0]->cacheInfo.cacheMissRatio * 100, "%", cands[0]->cacheInfo.cacheMissRatio * 100, "%");
		}
		index++;

		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_LOW) {
			printf("  BMR: %d (%.1f%s-%.1f%s)\n", similarities[indexId][index],
					procs[0]->branchInfo.branchMissRatio * 100, "%", cands[0]->branchInfo.branchMissRatio * 100, "%");
		}
		index++;

		if (simMetricLevel == METRIC_ALL || simMetricLevel == METRIC_HIGH) {
			printf("  CCR: %d (%.3f-%.3f)\n", similarities[indexId][index], procs[0]->instructions.ccRatio, cands[0]->instructions.ccRatio);
		}
		index++;
	}

	return;
}

void setGenAlgGlobalParams() {

	if (hasThreadMrapiExt) {
		dataSharingRatioInc = mrapiDataSharingRatioInc;
	}
	if (hasThreadMcapiExt) {
		dataSharingRatioInc = mcapiDataSharingRatioInc;
	}
	if (hasThreadMrapiExt && hasThreadMcapiExt) {
		dataSharingRatioInc = (mrapiDataSharingRatioInc + mcapiDataSharingRatioInc) / 2.0;
	}
	if (!hasThreadMrapiExt && !hasThreadMcapiExt) {
		dataSharingRatioInc = posixDataSharingRatioInc;
	}
}

void setOrigTargetPatterns(int phaseCount, paralPattern pps[MAX_GROUP_NUMBER], paralPattern targetPatterns[MAX_GROUP_NUMBER]) {

	int i;

	for (i = 0; i < phaseCount; i++) {
		if (targetPatterns[i] != PP_NA) {
			printf("INFO: Setting parallel pattern of phase %d from %s to %s.\n",
					i + 1, getPatternText(pps[i]), getPatternText(targetPatterns[i]));
			pps[i] = targetPatterns[i];
		}
	}
}

int geneticAlg(int appCount, char *appNames[MAX_PROCS * 2], int candCount, char *candNames[MAX_PROCS * 2],
		boolean isCandExists, paralPattern targetPatterns[MAX_GROUP_NUMBER], boolean isVerbose) {

	int similarityScores[MAX_THREADS], totalThreadScore, i, indexId = 0;
	int retVal = 2;
	boolean isSimScoreMet = true;
	int ret;

	assert(indSimilarityThreshold <= similarityThreshold);

	setGenAlgGlobalParams();

	if (candCount > 0) {
		if ((retVal = prepData(appCount, appNames)) == 0) {
			origPhaseCount = recogPattern(origPps, false, false);
			setOrigTargetPatterns(origPhaseCount, origPps, targetPatterns);
			setRunningType(false);
			ret = prepData(candCount, candNames);

			if (ret >= 0) {

#ifndef DEBUG_ON
				candPhaseCount = recogPattern(candPps, false, false);
#else
				candPhaseCount = recogPattern(candPps, true, false);
#endif
				setRunningType(true);

				if (isPerThread == true) {
					if (isCandExists) {
						loadParameters();
					}
					for (indexId = 2; indexId <= maxTid; indexId++) {
						if (isSimScoreMets[indexId] == true) {
							cands[0]->threads[indexId]->ipcInfo.ipc = oldStepData[indexId][0].val;
							cands[0]->threads[indexId]->cacheInfo.cacheMissRatio = oldStepData[indexId][1].val;
							cands[0]->threads[indexId]->branchInfo.branchMissRatio = oldStepData[indexId][2].val;
							cands[0]->instructions.ccRatio = oldStepData[indexId][3].val;
						}
						isSimScoreMets[indexId] = true;
						similarityScores[indexId] = measureOverallSim(appCount, appNames, candCount, candNames, indexId);
						if (similarityScores[indexId] >= similarityThreshold) {
							for (i = 0; i < metricCount; i++) {
								if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
									if (similarities[indexId][i] < indSimilarityThreshold) {
										isSimScoreMet = false;
										isSimScoreMets[indexId] = false;
										break;
									}
								}
							}
						} else {
							isSimScoreMet = false;
							isSimScoreMets[indexId] = false;
						}
					}
				} else {
					similarityScores[indexId] = measureOverallSim(appCount, appNames, candCount, candNames, indexId);
					if (similarityScores[indexId] >= similarityThreshold) {
						for (i = 0; i < metricCount; i++) {
							if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
								if (similarities[indexId][i] < indSimilarityThreshold) {
									isSimScoreMet = false;
									break;
								}
							}
						}
					} else {
						isSimScoreMet = false;
					}
				}

				if (isVerbose) {
					printSimScores();
				}

				if (isSimScoreMet) {
					printf("  Target similarity score is met.\n");
					writeSimScores(similarityScores);
					retVal = 3;
				} else {
					if (isGenSyn) {
						//FIXME: decide updateCandBench type according to parallel pattern or metric values
						if (isPerThread == true) {
							for (indexId = 2; indexId <= maxTid; indexId++) {
								if (isSimScoreMets[indexId] == false) {
//									updateCandBenchAll(indexId);
									updateCandBenchOne(indexId);
//									updateCandBenchOrdered(indexId);
								} else {
									restoreOldStepData(indexId);
								}
							}
						} else {
							if (isCandExists) {
								loadParameters();
							}
//							updateCandBenchAll(indexId);
							updateCandBenchOne(indexId);
//							updateCandBenchOrdered(indexId);
						}
						manageSideEffects();
						prepBenchmark(origPhaseCount, origPps, false);
						genBenchmark(origPhaseCount, origPps);
						saveParameters();
					}

#ifdef DEBUG_ON
					if (isPerThread == true) {
						for (i = 2; i <= maxTid; i++) {
							printf("[DEBUG] Thread%d, Block iteration counts: IPC=%u, CMR=%u, BMR=%u, CCR=%u\n",
									i, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count);
						}
					} else {
						i = 0;
						printf("[DEBUG] Process%d, Block iteration counts: IPC=%u, CMR=%u, BMR=%u, CCR=%u\n",
								i + 1, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count);
					}
#endif
					printf("  Not similar enough!\n");
				}

				if (isPerThread == true) {
					for (indexId = 2; indexId <= maxTid; indexId++) {
						printf("   Thread%d Score: %d/%d, Threshold: %d, Individual: %d (%s)\n",
								indexId, similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold,
								isSimScoreMets[indexId] == false ? "Not similar" : "Similar");
					}
					for (indexId = 2, totalThreadScore = 0; indexId <= maxTid; indexId++) {
						totalThreadScore += similarityScores[indexId];
					}
					printf("  Average Thread Score: %d/%d, Threshold: %d, Individual: %d\n",
							(int) (totalThreadScore / (maxTid - 1)), SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
				} else {
					printf(" Score: %d/%d, Threshold: %d, Individual: %d\n",
							similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
				}

			}
		}
	}

	return retVal;
}

int geneticAlgSa(int appCount, char *appNames[MAX_PROCS * 2], int candCount, char *candNames[MAX_PROCS * 2],
		boolean isCandExists, boolean isVerbose) {

	int similarityScores[MAX_THREADS], i, indexId = 0;
	int retVal = 2;
	boolean isSimScoreMet = true;
	paralPattern pps[MAX_GROUP_NUMBER] = { PP_NA };
	int phaseCount = 1;

	assert(indSimilarityThreshold <= similarityThreshold);

	setGenAlgGlobalParams();

	if (candCount > 0) {
		if ((retVal = prepDataSa(appCount, appNames)) == 0) {
			setRunningType(false);
			prepDataSa(candCount, candNames);
			setRunningType(true);
			maxGroupId = 1; /* can be done in pattern recognition */
			maxCandGroupId = 1; /* can be done in pattern recognition */

			if (isPerThread == true) {
				for (indexId = 2; indexId <= maxTid; indexId++) {
					similarityScores[indexId] = measureOverallSimSa(appCount, appNames, candCount, candNames, indexId);
					if (similarityScores[indexId] >= similarityThreshold) {
						for (i = 0; i < metricCount; i++) {
							if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
								if (similarities[indexId][i] < indSimilarityThreshold) {
									isSimScoreMet = false;
									break;
								}
							}
						}
					} else {
						isSimScoreMet = false;
					}
				}
			} else {
				similarityScores[indexId] = measureOverallSimSa(appCount, appNames, candCount, candNames, indexId);
				if (similarityScores[indexId] >= similarityThreshold) {
					for (i = 0; i < metricCount; i++) {
						if (simMetricLevel == METRIC_ALL || simMetricLevel == metrics[i].level) {
							if (similarities[indexId][i] < indSimilarityThreshold) {
								isSimScoreMet = false;
								break;
							}
						}
					}
				} else {
					isSimScoreMet = false;
				}
			}

			if (isVerbose) {
				printSimScores();
			}

			if (isSimScoreMet) {
				printf("  Target similarity score is met.");
				writeSimScores(similarityScores);
				retVal = 3;
			} else {
				if (isGenSyn) {
					if (isCandExists) {
						loadParameters();
					}
					//FIXME: decide updateCandBench type according to parallel pattern or metric values
					if (isPerThread == true) {
						for (indexId = 2; indexId <= maxTid; indexId++) {
//							updateCandBenchAll(indexId);
							updateCandBenchOne(indexId);
//							updateCandBenchOrdered(indexId);
						}
					} else {
//						updateCandBenchAll(indexId);
						updateCandBenchOne(indexId);
//						updateCandBenchOrdered(indexId);
					}
					manageSideEffects();
					prepBenchmarkSa(true);
					phaseCount = 1;
					pps[0] = PP_Hl;
					genBenchmarkSa(phaseCount, pps);
					saveParameters();
				}

#ifdef DEBUG_ON
				if (isPerThread == true) {
					for (i = 2; i <= maxTid; i++) {
						printf("[DEBUG] Block iteration counts of Thread%d: IPC=%u, CMR=%u, BMR=%u, CCR=%u\n",
								i, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count);
					}
				} else {
					i = 0;
					printf("[DEBUG] Block iteration counts of Process%d: IPC=%u, CMR=%u, BMR=%u, CCR=%u\n",
							i + 1, newStepData[i][0].count, newStepData[i][1].count, newStepData[i][2].count, newStepData[i][3].count);
				}
#endif

				printf("  Not similar enough!");
			}

			if (isPerThread == true) {
				for (indexId = 2; indexId <= maxTid; indexId++) {
					printf(" Thread %d Score: %d/%d, Threshold: %d, Individual: %d\n",
							indexId, similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
				}
			} else {
				printf(" Score: %d/%d, Threshold: %d, Individual: %d\n",
						similarityScores[indexId], SIMILARITY_MAX, similarityThreshold, indSimilarityThreshold);
			}

		}
	}

	return retVal;
}
