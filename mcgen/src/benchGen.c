/*
 * Copyright (c) 2011-2013, Etem Deniz <etem.deniz@boun.edu.tr>
 * Alper Sen <alper.sen@boun.edu.tr>, Bogazici University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * benchGen.c
 *
 *  Created on: Jan 4, 2012
 *      Author: Etem Deniz
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "benchGen.h"
#include "threadOps.h"
#include "workGen.h"

const char* copyright =
		"/*\n"
		" * Copyright (c) 2011-2013, Etem Deniz <etem.deniz@boun.edu.tr>, Bogazici University\n"
		" * All rights reserved.\n"
		" * \n"
		" * Redistribution and use in source and binary forms, with or without\n"
		" * modification, are permitted provided that the following conditions are\n"
		" * met:\n"
		" * \n"
		" * (1) Redistributions of source code must retain the above copyright\n"
		" * notice, this list of conditions and the following disclaimer.\n"
		" * \n"
		" * (2) Redistributions in binary form must reproduce the above copyright\n"
		" * notice, this list of conditions and the following disclaimer in the\n"
		" * documentation and/or other materials provided with the distribution.\n"
		" * \n"
		" * (3) The names of the contributors may not be used to endorse or promote\n"
		" * products derived from this software without specific prior written\n"
		" * permission.\n"
		" * \n"
		" * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS\n"
		" * IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED\n"
		" * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A\n"
		" * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER\n"
		" * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,\n"
		" * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,\n"
		" * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR\n"
		" * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF\n"
		" * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING\n"
		" * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS\n"
		" * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n"
		" * \n"
		" */\n"
		"\n";

const char* headerInclude =
		"#include <stdio.h>\n"
		"#include <stdlib.h>\n"
		"#include <string.h>\n"
		"#include <pthread.h>\n"
		"#include <unistd.h>\n";

const char* mrapiHeaderInclude =
		"#include \"mrapi.h\"\n"
		"#include \"mrapi_ext.h\"\n";

const char* mcapiHeaderInclude =
		"#include \"mca_config.h\"\n"
		"#include \"mcapi.h\"\n";

const char* posixHeaderInclude =
		"#include <semaphore.h>\n"
		"#include <errno.h>\n"
		"#include <fcntl.h>\n";

const char* mtapieaderInclude =
		"#include \"mtapi.h\"\n";

const char* quarkInclude =
		"#include \"../quark/common/quark/quark-runtime.h\"\n"
		"#include <stdlib.h>\n"
		"#include <stdio.h>\n"
		"#include <string.h>\n";

const char* mrapiDefines =
		"#ifndef MRAPI_DOMAIN\n"
		"#define MRAPI_DOMAIN 1\n"
		"#endif\n"
		"\n"
		"#define LOCK_LIMIT 1\n"
		"\n";

const char* mcapiDefines =
		"#ifndef MCAPI_DOMAIN\n"
		"#define MCAPI_DOMAIN 1\n"
		"#endif\n"
		"\n"
		"#define BUFF_SIZE 64\n"
		"#define MCAPI_TIMEOUT 1024\n"
		"#define MCAPI_ENDPOINT_INVALID (~0)\n"
		"\n";

const char* structures =
		"";

const char* mrapiGlobalVars =
		"char mrapiStatusBuff[MRAPI_MAX_STATUS_SIZE];\n"
		"\n";

const char* mcapiGlobalVars =
		"char mcapiStatusBuff[MCAPI_MAX_STATUS_SIZE];\n"
		"\n";

const char* wrongStr =
		"#define WRONG wrong(td->tid, status, __FILE__, __LINE__);\n"
		"void wrong(unsigned int tid, char *status, char *file, unsigned line) {\n"
		"\tprintf(stdout,\"WRONG: tid=%d, status=%s, file=%s, line=%u\\n\", tid, status, file, line);\n"
		"\tfflush(stdout);\n"
		"\texit(1);\n"
		"}\n"
		"\n";

const char* mrapiWrongStr =
		"#define MRAPI_WRONG mrapiWrong(td->tid, mrapiStatus, __FILE__, __LINE__);\n"
		"void mrapiWrong(mca_node_t tid, mrapi_status_t status, char *file, unsigned line) {\n"
		"\tprintf(stdout,\"WRONG: tid=%d, status=%s, file=%s, line=%u\\n\", tid, mrapi_display_status(status, mrapiStatusBuff, sizeof(mrapiStatusBuff)), file, line);\n"
		"\tfflush(stdout);\n"
		"\tmrapi_finalize(&status);\n"
		"\texit(1);\n"
		"}\n"
		"\n";

const char* mrapiCheckSuccessStr =
		"#define MRAPI_CHECK mrapiCheckSuccess(td->tid, mrapiStatus, __FILE__, __LINE__);\n"
		"void mrapiCheckSuccess(mca_node_t tid, mrapi_status_t status, char *file, unsigned line) {\n"
		"\tif (status != MRAPI_SUCCESS) {\n"
		"\t\tprintf(\"ERROR: tid=%d, status=%s, file=%s, line=%u\\n\", tid, mrapi_display_status(status, mrapiStatusBuff, sizeof(mrapiStatusBuff)), file, line);\n"
		"\t}\n"
		"}\n"
		"\n";

const char* mrapiFuncPars =
		"\tmrapi_parameters_t mrapiParms = 0;\n"
		"\tmrapi_info_t mrapiVersion;\n"
		"\tmrapi_status_t mrapiStatus;\n";

const char* mcapiWrongStr =
		"#define MCAPI_WRONG mcapiWrong(td->tid, mcapiStatus, __FILE__, __LINE__);\n"
		"void mcapiWrong(mca_node_t tid, mcapi_status_t status, char *file, unsigned line) {\n"
		"\tfprintf(stdout,\"WRONG: tid=%d, status=%s, file=%s, line=%u\\n\", tid, mcapi_display_status(status, mcapiStatusBuff, sizeof(mcapiStatusBuff)), file, line);\n"
		"\tfflush(stdout);\n"
		"\t/* mrapi_finalize(&mrapiStatus); // mcapi_finalize does */\n"
		"\tmcapi_finalize(&status);\n"
		"\texit(1);\n"
		"}\n"
		"\n";

const char* mcapiCheckSuccessStr =
		"#define MCAPI_CHECK mcapiCheckSuccess(td->tid, mcapiStatus, __FILE__, __LINE__);\n"
		"void mcapiCheckSuccess(mca_node_t tid, mcapi_status_t status, char *file, unsigned line) {\n"
		"\tif (status != MCAPI_SUCCESS) {\n"
		"\t\tprintf(\"ERROR: tid=%d, status=%s, file=%s, line=%u\\n\", tid, mcapi_display_status(status, mcapiStatusBuff, sizeof(mcapiStatusBuff)), file, line);\n"
		"\t}\n"
		"}\n"
		"\n";

const char* mcapiFuncPars =
		"\tmcapi_param_t mcapiParms = 0;\n"
		"\tmcapi_info_t mcapiVersion;\n"
		"\tmcapi_status_t mcapiStatus;\n";

const char* posixWrongStr =
		"#define POSIX_WRONG posixWrong(td->tid, retVal, __FILE__, __LINE__);\n"
		"void posixWrong(int tid, int retVal, char *file, unsigned line) {\n"
		"\tfprintf(stdout, \"WRONG: tid=%d, retval=%d, errno=%d, file=%s, line=%u\\n\", tid, retVal, errno, file, line);\n"
		"\tfflush(stdout);\n"
		"\texit(1);\n"
		"}\n"
		"\n";

const char* posixFuncPars =
		"int retVal = 0;\n";

const char* workloadFnName =
		"getWorkloadCalculation";

boolean isThreadStructData = true;
boolean isVariablesInSingleLine = true;
boolean isTlsVarArray = true;

int semLock(FILE *fw, struct tThreadData *td, int index, char *tab,  boolean hasNewLine);
int posixSemLock(FILE *fw, struct tThreadData *td, int index, char *tab,  boolean hasNewLine);

char *getOperationTypeText(workOpType type) {

	char *opType;

	switch (type) {
	case WORK_OP_NOOP:
		opType = "code block for computation";
		break;
	case WORK_OP_IPC_INC:
		opType = "code block for IPC increment";
		break;
	case WORK_OP_IPC_DEC:
		opType = "code block for IPC decrement";
		break;
	case WORK_OP_CACHE_HIT:
		opType = "code block for CMR decrement";
		break;
	case WORK_OP_CACHE_MISS:
		opType = "code block for CMR increment";
		break;
	case WORK_OP_BRNCH_HIT:
		opType = "code block for BMR decrement";
		break;
	case WORK_OP_BRNCH_MISS:
		opType = "code block for BMR increment";
		break;
	case WORK_OP_COMP_INC:
		opType = "code block for CCR decrement";
		break;
	case WORK_OP_COMM_INC:
		opType = "code block for CCR increment";
		break;
	case WORK_OP_FUNC:
		opType = "code block for work function";
		break;
	default:
		opType = "general";
		break;
	}

	return opType;

}

/***************************************************headers******************************************************/

void genHeader(FILE *fw, paralPattern pps[MAX_GROUP_NUMBER], boolean hasMrapiExt, boolean hasMcapiExt, boolean hasQuarkExt) {

	int i;

	fprintf(fw, "%s", copyright);
	fprintf(fw, "/*\n");
	fprintf(fw, " * Description: Synthetic benchmark of %s program.\n", activeProc->name);
	if (maxGroupId > 0) {
		fprintf(fw, " *              Parallel pattern of %s program is ", activeProc->name);
		for (i = 0; i < maxGroupId; i++) {
			if (i == maxGroupId - 1) {
				fprintf(fw, "%s.\n", getPatternText(pps[i]));
			} else {
				fprintf(fw, "%s", getPatternText(pps[i]));
				if (pps[i] != PP_Hl) {
					fprintf(fw, " + ");
				} else {
					break;
				}
			}
		}
	}
	fprintf(fw, " */\n\n");

	if (hasQuarkExt) {
		fprintf(fw, "%s", quarkInclude);
	} else {
		fprintf(fw, "%s", headerInclude);
	}
	if (hasMrapiExt) {
		fprintf(fw, "%s", mrapiHeaderInclude);
	}
	if (hasMcapiExt) {
		fprintf(fw, "%s", mcapiHeaderInclude);
	}
	if (!hasThreadMrapiExt && !hasThreadMcapiExt && !hasThreadBareMetalExt) {
		if (!hasMrapiExt && !hasMcapiExt && !hasQuarkExt) {
			fprintf(fw, "%s", posixHeaderInclude);
		}
	}
	fprintf(fw, "\n");

	return;
}

/***************************************************defines******************************************************/

void genDefines(FILE *fw, boolean hasMrapiExt, boolean hasMcapiExt) {

	if (hasMrapiExt) {
		fprintf(fw, "%s", mrapiDefines);
	}
	if (hasMcapiExt) {
		fprintf(fw, "%s", mcapiDefines);
	}

	return;
}

/***************************************************structures******************************************************/

void getMaxTlsCounts(int *lclCount, int *shmCount) {

	int i, numOfThreads;
	struct tThreadData *td;

	*lclCount = 0;
	*shmCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
		td = activeProc->threads[i];
		if (td->lsExt.hasExt) {
			if (td->lsExt.numOfLocalMems > *lclCount) {
				*lclCount = td->lsExt.numOfLocalMems;
			}
			if (td->lsExt.numOfSharedMems > *shmCount) {
                                *shmCount = td->lsExt.numOfSharedMems;
                        }
	
		}
	}

	return;
}

void getMaxMrapiCounts(int *mtxCount, int *semCount, int *shmCount, int *barCount) {

	int i, numOfThreads;
	struct tThreadData *td;

	*mtxCount = 0;
	*semCount = 0;
	*shmCount = 0;
	*barCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
		td = activeProc->threads[i];
		if (td->mrapiExt.hasExt) {
			if (td->mrapiExt.numOfMutexes > *mtxCount) {
				*mtxCount = td->mrapiExt.numOfMutexes;
			}
			if (td->mrapiExt.numOfSemaphores > *semCount) {
				*semCount = td->mrapiExt.numOfSemaphores;
			}
			if (td->mrapiExt.numOfSharedMems > *shmCount) {
				*shmCount = td->mrapiExt.numOfSharedMems;
			}
			if (td->mrapiExt.numOfBarriers > *barCount) {
				*barCount = td->mrapiExt.numOfBarriers;
			}
		}
	}

	return;
}

void getMaxMcapiCounts(int *epCount, int *pktChanCount, int *sclChanCount) {

	int i, numOfThreads;
	struct tThreadData *td;

	*epCount = 0;
	*pktChanCount = 0;
	*sclChanCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
		td = activeProc->threads[i];
		if (td->mcapiExt.hasExt) {
			if (td->mcapiExt.numOfEndpoints > *epCount) {
				*epCount = td->mcapiExt.numOfEndpoints;
			}
			if (td->mcapiExt.numOfPcktChans > *pktChanCount) {
				*pktChanCount = td->mcapiExt.numOfPcktChans;
			}
			if (td->mcapiExt.numOfScalChans > *sclChanCount) {
				*sclChanCount = td->mcapiExt.numOfScalChans;
			}
		}
	}

	return;
}

void genStructures(FILE *fw, boolean hasMrapiExt, boolean hasMcapiExt, boolean hasQuarkExt) {

	int localCount, sharedCount;
	int mtxCount, semCount, shmCount, barCount;
	int epCount, pktChanCount, sclChanCount;

	fprintf(fw, "%s", structures);

	if (isThreadStructData) {
		getMaxTlsCounts(&localCount, &sharedCount);
		getMaxMrapiCounts(&mtxCount, &semCount, &shmCount, &barCount);
		getMaxMcapiCounts(&epCount, &pktChanCount, &sclChanCount);
	} 

	fprintf(fw, "typedef struct {\n");

	if (isThreadStructData) {
		if (localCount > 0) {
			fprintf(fw, "\tint *localAddr[%d]; /* local mem address */\n", localCount);
		}
		if (sharedCount > 0) {
                        fprintf(fw, "\tint *shmAddr[%d]; /* shared mem address */\n", sharedCount);
                }
		if (mtxCount > 0) {
			fprintf(fw, "\tmrapi_mutex_hndl_t mutex[%d]; /* mrapi mutex handle */\n", mtxCount);
		}
		if (semCount > 0) {
			fprintf(fw, "\tmrapi_sem_hndl_t sem[%d]; /* mrapi sem handle */\n", semCount);
		}
		if (shmCount > 0) {
			fprintf(fw, "\tmrapi_shmem_hndl_t shmem[%d]; /* mrapi shmem handle */\n", shmCount);
		}
		if (shmCount > 0) {
			fprintf(fw, "\tchar *shmAddr[%d]; /* shared mem address */\n", shmCount);
		}
		if (epCount > 0) {
			fprintf(fw, "\tmcapi_endpoint_t ep[%d]; /* mcapi endpoint */\n", epCount);
		}
		if (pktChanCount > 0) {
			fprintf(fw, "\tmcapi_pktchan_send_hndl_t pktSendHandle[%d]; /* packet channel send handle */\n", pktChanCount);
			fprintf(fw, "\tmcapi_pktchan_recv_hndl_t pktRecvHandle[%d]; /* packet channel recv handle */\n", pktChanCount);
		}
		if (sclChanCount > 0) {
			fprintf(fw, "\tmcapi_sclchan_send_hndl_t sclSendHandle[%d]; /* scalar channel send handle */\n", sclChanCount);
			fprintf(fw, "\tmcapi_sclchan_recv_hndl_t sclRecvHandle[%d]; /* scalar channel recv handle */\n", sclChanCount);
		}
		if (hasWorkloadChecker) {
			fprintf(fw, "\tunsigned workload;\n");
		}
		if (hasQuarkExt) {
			fprintf(fw, "\tunsigned long *addr;\n");
		}
	}

	fprintf(fw, "\tunsigned int tid;\n");
	fprintf(fw, "} threadData;\n");
	fprintf(fw, "\n");

	return;
}

/***************************************************globals******************************************************/

void genGlobals(FILE *fw, boolean hasMrapiExt, boolean hasMcapiExt, boolean hasQuarkExt) {

	int i, j, numOfThreads;
	struct tThreadData *td;
	boolean isWritten;

	if (hasMrapiExt) {
		fprintf(fw, "%s", mrapiGlobalVars);
	}
	if (hasMcapiExt) {
		fprintf(fw, "%s", mcapiGlobalVars);
	}

	if (!hasThreadMrapiExt && !hasThreadMcapiExt) {
		if (!hasMrapiExt && !hasMcapiExt && !hasQuarkExt) {
			fprintf(fw, "%s", wrongStr);
		}
	}

	if (hasMrapiExt) {
		fprintf(fw, "%s", mrapiWrongStr);
		fprintf(fw, "%s", mrapiCheckSuccessStr);
		fprintf(fw, "%s", mrapiGlobalVars);
	}
	if (hasMcapiExt) {
		fprintf(fw, "%s", mcapiWrongStr);
		fprintf(fw, "%s", mcapiCheckSuccessStr);
		fprintf(fw, "%s", mcapiGlobalVars);
	}

	if (!hasThreadMrapiExt && !hasThreadMcapiExt) {
		if (!hasMrapiExt && !hasMcapiExt && !hasQuarkExt) {
			fprintf(fw, "%s", posixWrongStr);
		}
	}

	for (i = 0; i < activeProc->lsExt.numOfGlobMems; i++) {
		if (strcmp(activeProc->lsExt.globMems[i].name, "") == 0) {
			fprintf(fw, "int globAddr%d[%d];\n", i + 1, activeProc->lsExt.globMems[i].size);
		} else {
			fprintf(fw, "int %s[%d][%d];\n", activeProc->lsExt.globMems[i].name,
					activeProc->lsExt.globMems[i].size, activeProc->lsExt.globMems[i].size2);
		}
	}
	if (activeProc->lsExt.numOfGlobMems > 0) {
		fprintf(fw, "\n");
	}

	numOfThreads = activeProc->numOfThreads;

	isWritten = false;
	for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
		td = activeProc->threads[i];
		for (j = 0; j < td->posixExt.numOfMutexes; j++) {
			if (td->posixExt.mutexes[j].isOwner) {
				fprintf(fw, "pthread_mutex_t mutexRes%d = PTHREAD_MUTEX_INITIALIZER;\n", j + 1);
				fprintf(fw, "pthread_mutex_t *mutex%d = &mutexRes%d;\n", j + 1, j + 1);
				isWritten = true;
			}
		}
	}
	if (isWritten) {
		fprintf(fw, "\n");
	}

	isWritten = false;
	for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
		td = activeProc->threads[i];
		if (td->mrapiExt.hasExt) {
			for (j = 0; j < td->mrapiExt.numOfBarriers; j++) {
				if (td->mrapiExt.barriers[j].isOwner) {
					fprintf(fw, "volatile int barrier%d = 0;\n", td->mrapiExt.barriers[j].id);
					isWritten = true;
				}
			}
		}
	}
	if (isWritten) {
		fprintf(fw, "\n");
	}

	return;
}

/***************************************************globals******************************************************/

void genFuncSignatures(FILE *fw, boolean hasMrapiExt, boolean hasMcapiExt) {

	int i, numOfThreads, maxTaskId = -1;
	struct tThreadData *td;

	numOfThreads = activeProc->numOfThreads;
	for (i = MAIN_ID + 1; i < numOfThreads; i++) { /* exclude main */
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId > maxTaskId) {
			maxTaskId = td->mtapiExt.startRoutine.taskId;
		}
	}

	fprintf(fw, "/* Task signatures */\n");
	for (i = 0; i <= maxTaskId; i++) {
		fprintf(fw, "void *task%d(void *param);\n", i);
	}
	if (maxTaskId >= 0) {
		fprintf(fw, "\n");
	}

	return;
}

/***************************************************parameters******************************************************/

boolean genMtapiPars(FILE *fw, struct tThreadData *activeTd, char *tab) {

	int i, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean hasExt = false;

	if (activeTd->mtapiExt.numOfWorks > 0) {
		if (activeTd->id > 1) {
			fprintf(fw, "%sint workCount; /* work counter */\n", tab);
		} else {
			fprintf(fw, "%sregister unsigned int workCount; /* work counter */\n", tab);
//			fprintf(fw, "%sunsigned int workCount; /* work counter */\n", tab);
		}
	}

	numOfThreads = activeProc->numOfThreads;
	for (i = activeTd->id; i < numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mtapiExt.loopCount > 0) {
				fprintf(fw, "%s\tint loopCount; /* loop counter */\n", tab);
				hasExt = true;
				break;
			}
		}
	}

	return hasExt;
}

boolean getMaxTlsCountsInTask(int *lclCount, int *shmCount, int threadId, int taskId) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasExt = false;

	*lclCount = 0;
	*shmCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i < numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->lsExt.hasExt) {
				if (td->lsExt.numOfLocalMems > *lclCount) {
					*lclCount = td->lsExt.numOfLocalMems;
				}
				if (td->lsExt.numOfSharedMems > *shmCount) {
                                        *shmCount = td->lsExt.numOfSharedMems;
                                }
				hasExt = true;
			}
		}
	}

	return hasExt;
}

int getMaxTlsLocalSizeInTask(int threadId, int taskId, int tlsIndex) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean maxSize = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i < numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->lsExt.hasExt) {
				if (td->lsExt.numOfLocalMems > tlsIndex) {
					if (td->lsExt.localMems[tlsIndex].size > maxSize) {
						maxSize = td->lsExt.localMems[tlsIndex].size;
					}
				}
			}
		}
	}

	return maxSize;
}

int getMaxTlsShmSizeInTask(int threadId, int taskId, int tlsIndex) {

        int i, numOfThreads;
        struct tThreadData *td;
        boolean maxSize = 0;

        numOfThreads = activeProc->numOfThreads;
        for (i = threadId; i < numOfThreads; i++) {
                td = activeProc->threads[i];
                if (td->mtapiExt.startRoutine.taskId == taskId) {
                        if (td->lsExt.hasExt) {
                                if (td->lsExt.numOfSharedMems > tlsIndex) {
                                        if (td->lsExt.sharedMems[tlsIndex].size > maxSize) {
                                                maxSize = td->lsExt.sharedMems[tlsIndex].size;
                                        }
                                }
                        }
                }
        }

        return maxSize;
}

boolean hasGlobMemOps(struct tThreadData *activeTd) {

	int i, j, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean retVal = false;

	numOfThreads = activeProc->numOfThreads;
	for (i = activeTd->id; i < numOfThreads && retVal == false; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			for (j = 0; j < td->lsExt.numOfMemOps; j++) {
				if (td->lsExt.memOps[j].type == MEM_TYPE_GLOBAL) {
					retVal = true;
					break;
				}
			}
		}
	}

	return retVal;

}

boolean genTlsPars(FILE *fw, struct tThreadData *activeTd, char *tab) {

	int i;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	boolean isTlsFuncPars = false;
	int lclCountInTask, shmCountInTask, maxSize;

	getMaxTlsCountsInTask(&lclCountInTask, &shmCountInTask, activeTd->id, taskId);
	if (lclCountInTask > 0) {
		if (!isTlsVarArray) {
			fprintf(fw, "%schar *status;\n", tab);
		}
		fprintf(fw, "%sint localMemTemp; /* temp read/write data */\n", tab);
		fprintf(fw, "%sint localMemIt; /* temp read/write data */\n", tab);
		for (i = 0; i < lclCountInTask; i++) {
			maxSize = getMaxTlsLocalSizeInTask(activeTd->id, taskId, i);
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					if (isTlsVarArray) {
						fprintf(fw, ", localAddr%d[%d]", i + 1, maxSize);
					} else {
						fprintf(fw, ", *localAddr%d", i + 1);
					}
				} else { /* first */
					if (isTlsVarArray) {
						fprintf(fw, "%sint localAddr%d[%d]", tab, i + 1, maxSize);
					} else {
						fprintf(fw, "%sint *localAddr%d", tab, i + 1);
					}
				}
				if (i == lclCountInTask - 1) { /* last */
					fprintf(fw, "; /* thread local mem */\n");
				}
			} else {
				if (isTlsVarArray) {
					fprintf(fw, "%sint localAddr%d[%d]; /* thread local mem */\n", tab, i + 1, maxSize);
				} else {
					fprintf(fw, "%sint *localAddr%d; /* thread local mem */\n", tab, i + 1);
				}
			}
			isTlsFuncPars = true;
		}
	}
	if (shmCountInTask > 0) {
                if (!isTlsVarArray) {
                        fprintf(fw, "%schar *status;\n", tab);
                }
                fprintf(fw, "%sint shmMemTemp; /* temp read/write data */\n", tab);
                fprintf(fw, "%sint shmMemIt; /* temp read/write data */\n", tab);
		/* Shared Memory should be passed via pointers, not declared explicityly */
                for (i = 0; i < shmCountInTask && activeTd->tid == MAIN_ID; i++) { /* main creates the shared memory array */
                        maxSize = getMaxTlsShmSizeInTask(activeTd->id, taskId, i);
                        if (isVariablesInSingleLine) {
                                if (i != 0) { /* middle */
                                        if (isTlsVarArray) {
                                                fprintf(fw, ", shmAddr%d[%d]", i + 1, maxSize);
                                        } else {
                                               fprintf(fw, ", *shmAddr%d", i + 1);
                                       }
                               } else { /* first */
                                       if (isTlsVarArray) {
                                               fprintf(fw, "%sint shmAddr%d[%d]", tab, i + 1, maxSize);
                                        } else {
                                                fprintf(fw, "%sint *shmAddr%d", tab, i + 1);
                                        }
                                }
                                if (i == shmCountInTask - 1) { /* last */
                                        fprintf(fw, "; /* thread shared mem */\n");
                                }
                        } else {
                                if (isTlsVarArray) {
                                        fprintf(fw, "%sint shmAddr%d[%d]; /* thread shared mem */\n", tab, i + 1, maxSize);
                                } else {
                                        fprintf(fw, "%sint *shmAddr%d; /* thread shared mem */\n", tab, i + 1);
                                }
                        }
                        isTlsFuncPars = true;
                }
        }

	if (hasGlobMemOps(activeTd)) {
		fprintf(fw, "%sint globMemTemp;\n", tab);
		fprintf(fw, "%sint globMemIt;\n", tab);
	}

	return isTlsFuncPars;
}

boolean getMaxMrapiCountsInTask(int *mtxCount, int *semCount, int *shmCount, int *barCount, int threadId, int taskId) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasExt = false;

	*mtxCount = 0;
	*semCount = 0;
	*shmCount = 0;
	*barCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i < numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mrapiExt.hasExt) {
				if (td->mrapiExt.numOfMutexes > *mtxCount) {
					*mtxCount = td->mrapiExt.numOfMutexes;
				}
				if (td->mrapiExt.numOfSemaphores > *semCount) {
					*semCount = td->mrapiExt.numOfSemaphores;
				}
				if (td->mrapiExt.numOfSharedMems > *shmCount) {
					*shmCount = td->mrapiExt.numOfSharedMems;
				}
				if (td->mrapiExt.numOfBarriers > *barCount) {
					*barCount = td->mrapiExt.numOfBarriers;
				}
				hasExt = true;
			}
		}
	}

	return hasExt;
}

boolean genMrapiPars(FILE *fw, struct tThreadData *activeTd, char *tab) {

	int i;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	boolean hasExt;
	int mtxCount, semCount, shmCount, barCount;

	hasExt = getMaxMrapiCountsInTask(&mtxCount, &semCount, &shmCount, &barCount, activeTd->id, taskId);

	if (hasExt) {
		fprintf(fw, "%s", mrapiFuncPars);
		if (mtxCount > 0) {
			fprintf(fw, "%smrapi_key_t lockKey; /* mrapi mutex lock key */\n", tab);
			for (i = 0; i < mtxCount; i++) {
				if (isVariablesInSingleLine) {
					if (i != 0) { /* middle */
						fprintf(fw, ", mutex%d", i + 1);
					} else { /* first */
						fprintf(fw, "%smrapi_mutex_hndl_t mutex%d", tab, i + 1);
					}
					if (i == mtxCount - 1) { /* last */
						fprintf(fw, "; /* mrapi mutex handle */\n");
					}
				} else {
					fprintf(fw, "%smrapi_mutex_hndl_t mutex%d; /* mrapi mutex handle */\n", tab, i + 1);
				}
			}
		}
		for (i = 0; i < semCount; i++) {
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					fprintf(fw, ", sem%d", i + 1);
				} else { /* first */
					fprintf(fw, "%smrapi_sem_hndl_t sem%d", tab, i + 1);
				}
				if (i == semCount - 1) { /* last */
					fprintf(fw, "; /* mrapi sem handle */\n");
				}
			} else {
				fprintf(fw, "%smrapi_sem_hndl_t sem%d; /* mrapi sem handle */\n", tab, i + 1);
			}
		}
		if (shmCount > 0) {
			fprintf(fw, "%schar shmMemTemp; /* temp read/write data */\n", tab);
			fprintf(fw, "%sint shmMemIt; /* temp read/write data */\n", tab);
			for (i = 0; i < shmCount; i++) {
				if (isVariablesInSingleLine) {
					if (i != 0) { /* middle */
						fprintf(fw, ", shmem%d", i + 1);
					} else { /* first */
						fprintf(fw, "%smrapi_shmem_hndl_t shmem%d", tab, i + 1);
					}
					if (i == shmCount - 1) { /* last */
						fprintf(fw, "; /* mrapi shmem handle */\n");
					}
				} else {
					fprintf(fw, "%smrapi_shmem_hndl_t shmem%d; /* mrapi shmem handle */\n", tab, i + 1);
				}
			}
			for (i = 0; i < shmCount; i++) {
				if (isVariablesInSingleLine) {
					if (i != 0) { /* middle */
						fprintf(fw, ", *shmAddr%d", i + 1);
					} else { /* first */
						fprintf(fw, "%schar *shmAddr%d", tab, i + 1);
					}
					if (i == shmCount - 1) { /* last */
						fprintf(fw, "; /* shared mem */\n");
					}
				} else {
					fprintf(fw, "%schar *shmAddr%d; /* shared mem */\n", tab, i + 1);
				}
			}
		}
	}

	return hasExt;
}

boolean getMaxMcapiCountsInTask(int *epCount, int *pktChanCount, int *sclChanCount, int threadId, int taskId) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasExt = false;

	*epCount = 0;
	*pktChanCount = 0;
	*sclChanCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i < numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mcapiExt.hasExt) {
				if (td->mcapiExt.numOfEndpoints > *epCount) {
					*epCount = td->mcapiExt.numOfEndpoints;
				}
				if (td->mcapiExt.numOfPcktChans > *pktChanCount) {
					*pktChanCount = td->mcapiExt.numOfPcktChans;
				}
				if (td->mcapiExt.numOfScalChans > *sclChanCount) {
					*sclChanCount = td->mcapiExt.numOfScalChans;
				}
				hasExt = true;
			}
		}
	}

	return hasExt;
}

void getScalarTypeCountsInTask(int *scl8Count, int *scl16Count, int *scl32Count, int *scl64Count, int taskId) {

	int i, j, numOfThreads;
	struct tThreadData *td;

	*scl8Count = 0;
	*scl16Count = 0;
	*scl32Count = 0;
	*scl64Count = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mcapiExt.hasExt) {
				for (j = 0; j < td->mcapiExt.numOfScalChans; j++) {
					if (td->mcapiExt.scalChans[j]->size == 8) {
						(*scl8Count)++;
					}
					if (td->mcapiExt.scalChans[j]->size == 16) {
						(*scl16Count)++;
					}
					if (td->mcapiExt.scalChans[j]->size == 32) {
						(*scl32Count)++;
					}
					if (td->mcapiExt.scalChans[j]->size == 64) {
						(*scl64Count)++;
					}
				}
			}
		}
	}

	return;
}

boolean genMcapiPars(FILE *fw, struct tThreadData *activeTd, char *tab) {

	int i, t, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean hasExt;
	struct tMcapiChannel *pMcapiChan;
	int epCount, msgChanCount, pktChanCount, sclChanCount;
	int scl8Count, scl16Count, scl32Count, scl64Count;

	hasExt = getMaxMcapiCountsInTask(&epCount, &pktChanCount, &sclChanCount, activeTd->id, taskId);
	msgChanCount = epCount - (2 * (pktChanCount + sclChanCount)); /* there exists msg exchange endpoints */

	if (hasExt) {
		fprintf(fw, "%s", mcapiFuncPars);
		if (msgChanCount > 0) {
			fprintf(fw, "%schar *msg = NULL;\n", tab);
		}
		if (msgChanCount > 0 || pktChanCount > 0) {
			fprintf(fw, "%schar buffer[BUFF_SIZE];\n", tab);
		}
		if (sclChanCount > 0) {
			getScalarTypeCountsInTask(&scl8Count, &scl16Count, &scl32Count, &scl64Count, taskId);
			if (scl8Count > 0) {
				fprintf(fw, "%sunsigned char data8;\n", tab);
			}
			if (scl16Count > 0) {
				fprintf(fw, "%sunsigned short data16;\n", tab);
			}
			if (scl32Count > 0) {
				fprintf(fw, "%sunsigned int data32;\n", tab);
			}
			if (scl64Count > 0) {
				fprintf(fw, "%sunsigned long long data64;\n", tab);
			}
		}

		fprintf(fw, "%sint i;\n", tab);

		if (pktChanCount > 0 || sclChanCount > 0) {
			fprintf(fw, "%ssize_t size;\n", tab);
			fprintf(fw, "%smcapi_request_t mcapiRequest;\n", tab);
		} else if (epCount > 2 * (pktChanCount + sclChanCount)) {
			fprintf(fw, "%ssize_t size;\n", tab);
		}
		for (i = 0; i < epCount; i++) {
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					fprintf(fw, ", ep%d", i + 1);
				} else { /* first */
					fprintf(fw, "%smcapi_endpoint_t ep%d", tab, i + 1);
				}
				if (i == epCount - 1) { /* last */
					fprintf(fw, "; /* mcapi endpoint */\n");
				}
			} else {
				fprintf(fw, "%smcapi_endpoint_t ep%d; /* mcapi endpoint */\n", tab, i + 1);
			}
		}
	}

	numOfThreads = activeProc->numOfThreads;
	for (t = activeTd->id; t < numOfThreads; t++) {
		td = activeProc->threads[t];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mcapiExt.hasExt) {
				for (i = 0; i < td->mcapiExt.numOfPcktChans; i++) {
					pMcapiChan = td->mcapiExt.pcktChans[i];
					if (td == pMcapiChan->pSenderTd) {
						fprintf(fw, "%smcapi_pktchan_send_hndl_t pktSendHandle%d%d; /* psh = ep(%d)%d->ep(%d)%d */\n",
								tab, td->id, i + 1, pMcapiChan->pSenderEp->nodeId, pMcapiChan->pSenderEp->portId,
								pMcapiChan->pReceiverEp->nodeId, pMcapiChan->pReceiverEp->portId);
					} else {
						fprintf(fw, "%smcapi_pktchan_recv_hndl_t pktRecvHandle%d%d; /* prh = ep(%d)%d->ep(%d)%d */\n",
								tab, td->id, i + 1, pMcapiChan->pSenderEp->nodeId, pMcapiChan->pSenderEp->portId,
								pMcapiChan->pReceiverEp->nodeId, pMcapiChan->pReceiverEp->portId);
					}
				}
				for (i = 0; i < td->mcapiExt.numOfScalChans; i++) {
					pMcapiChan = td->mcapiExt.scalChans[i];
					if (td == pMcapiChan->pSenderTd) {
						fprintf(fw, "%smcapi_sclchan_send_hndl_t sclSendHandle%d%d; /* ssh = ep(%d)%d->ep(%d)%d */\n",
								tab, td->id, i + 1, pMcapiChan->pSenderEp->nodeId, pMcapiChan->pSenderEp->portId,
								pMcapiChan->pReceiverEp->nodeId, pMcapiChan->pReceiverEp->portId);
					} else {
						fprintf(fw, "%smcapi_sclchan_recv_hndl_t sclRecvHandle%d%d; /* srh = ep(%d)%d->ep(%d)%d */\n",
								tab, td->id, i + 1, pMcapiChan->pSenderEp->nodeId, pMcapiChan->pSenderEp->portId,
								pMcapiChan->pReceiverEp->nodeId, pMcapiChan->pReceiverEp->portId);
					}
				}
			}
		}
	}

	return hasExt;
}

void genQuarkPars(FILE *fw, struct tThreadData *activeTd, char *tab) {
    int i;

    if(activeTd->workCheck.isFinalChecker == true) {
        if(activeTd->standAloneExt.parPattern == PP_RD) {
            fprintf(fw, "%sint data[%d] = { ", tab, activeTd->standAloneExt.sharedDataSize);
            for (i = 0; i < activeTd->standAloneExt.sharedDataSize; i++) {
                fprintf(fw, "%d", activeTd->standAloneExt.data[i]);    
                if (i == activeTd->standAloneExt.sharedDataSize - 1) { //final
                    fprintf(fw, "};\n");        
                } else {
                    fprintf(fw, ", "); 
                }
            }
        }   
    }
    
    return;    
}

boolean getMaxPosixCountsInTask(int *semCount, int *mutexCount, int threadId, int taskId) {

	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasExt = false;

	*semCount = 0;
	*mutexCount = 0;

	numOfThreads = activeProc->numOfThreads;
	for (i = threadId; i < numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (!hasThreadMrapiExt && !hasThreadMcapiExt) {
				if (!td->mrapiExt.hasExt && !td->mcapiExt.hasExt) {
					if (td->posixExt.numOfSemaphores > *semCount) {
						*semCount = td->posixExt.numOfSemaphores;
					}
					if (td->posixExt.numOfMutexes > *mutexCount) {
						*mutexCount = td->posixExt.numOfMutexes;
					}
					hasExt = true;
				}
			}
		}
	}

	return hasExt;
}

boolean genPosixPars(FILE *fw, struct tThreadData *activeTd, char *tab) {

	int i, semCount, mutexCount;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	boolean hasExt = false;

	hasExt = getMaxPosixCountsInTask(&semCount, &mutexCount, activeTd->id, taskId);

	if (hasExt) {
		fprintf(fw, "%s%s", tab, posixFuncPars);
		for (i = 0; i < semCount; i++) {
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					fprintf(fw, ", *sem%d", i + 1);
				} else { /* first */
					fprintf(fw, "%ssem_t *sem%d", tab, i + 1);
				}
				if (i == semCount - 1) { /* last */
					fprintf(fw, "; /* posix sem */\n");
				}
			} else {
				fprintf(fw, "%ssem_t *sem%d; /* posix sem */\n", tab, i + 1);
			}
		}
		for (i = 0; i < mutexCount; i++) {
			if (isVariablesInSingleLine) {
				if (i != 0) { /* middle */
					fprintf(fw, ", mutex%d", i + 1);
				} else { /* first */
					fprintf(fw, "%spthread_mutex_t mutex%d", tab, i + 1);
				}
				if (i == semCount - 1) { /* last */
					fprintf(fw, "; /* posix mutex */\n");
				}
			} else {
				fprintf(fw, "%spthread_mutex_t mutex%d; /* posix mutex */\n", tab, i + 1);
			}
		}
	}

	return hasExt;
}

void genMxapiPars(FILE *fw, struct tThreadData *activeTd, char* tab) {

	genMtapiPars(fw, activeTd, tab);
	genTlsPars(fw, activeTd, tab);
	genMrapiPars(fw, activeTd, tab);
	genMcapiPars(fw, activeTd, tab);
	genPosixPars(fw, activeTd, tab);
	genQuarkPars(fw, activeTd, tab);

	fprintf(fw, "\n");

	return;
}

/***************************************************initialization******************************************************/

void initTls(FILE *fw, struct tThreadData *activeTd, char *tab) {

        int id;
	
	int childIndex = 0;

	int localCount, sharedCount;

        struct tThreadData *td;

        if (isThreadStructData) {
                getMaxTlsCounts(&localCount, &sharedCount);
        }

        for (id = MAIN_ID; id < activeProc->numOfThreads + MAIN_ID; id++) { /* include main */
		char* tDataStr = "maintData";

                td = activeProc->threads[id];

		if (activeTd->id == id && id == MAIN_ID) {
			tDataStr = &tDataStr[0]; /* String: "maintData" */
		} else {
			tDataStr = &tDataStr[4]; /* String : "tData" */
		}
		if (isThreadStructData) {
                        if (td->lsExt.hasExt) {
                                if (localCount > 0) {
                                        //if (activeTd->tid == MAIN_ID && td->tid == MAIN_ID) {
                                        //        fprintf(fw, "%s%s.localAddr[%d] = localAddr%d; /* local mem address */\n",
                                        //        tab, tDataStr, localCount - 1, localCount);
                                        //} else if (activeTd->dependency[id] > 0) {
                                        //      fprintf(fw, "%s%s[%d].localAddr[%d] = td->localAddr[%d]; /* local mem address */\n",
                                        //      tab, tDataStr, childIndex++, localCount - 1, localCount - 1);
                                        //}
                                }
                                if (sharedCount > 0) {
                                        if (td->standAloneExt.offset > 0) {
                                                if (activeTd->tid == MAIN_ID && td->tid == MAIN_ID) {
                                                        fprintf(fw, "%s%s.shmAddr[%d] = shmAddr%d + %d; /* shared mem address */\n",
                                                        tab, tDataStr, sharedCount - 1, sharedCount, td->standAloneExt.offset);
                                                } else if (activeTd->dependency[td->id] > 0) {
                                                        fprintf(fw, "%s%s[%d].shmAddr[%d] = td->shmAddr[%d] + %d; /* shared mem address */\n",
                                                        tab, tDataStr, childIndex++, sharedCount - 1, sharedCount - 1, td->standAloneExt.offset);
                                                }
                                        } else {
                                                if (activeTd->tid == MAIN_ID && td->tid == MAIN_ID) {
                                                        fprintf(fw, "%s%s.shmAddr[%d] = shmAddr%d; /* shared mem address */\n",
                                                        tab, tDataStr, sharedCount - 1, sharedCount);
                                                } else if (activeTd->dependency[td->id] > 0) {
                                                        fprintf(fw, "%s%s[%d].shmAddr[%d] = td->shmAddr[%d]; /* shared mem address */\n",
                                                        tab, tDataStr, childIndex++, sharedCount - 1, sharedCount - 1);
                                                }
                                        }
                                }
                        }
                }
	}
	fprintf(fw, "\n");	

        return;
}


void initQuark(FILE *fw, struct tThreadData *activeTd, char *tab) {

        int id;

        struct tThreadData *td;

	int childIndex = 0;

	for (id = MAIN_ID; id < activeProc->numOfThreads + MAIN_ID; id++) { /* include main */
                char* tDataStr = "maintData";

                td = activeProc->threads[id];

               	if (activeTd->tid == id && id == MAIN_ID) { 
                        tDataStr = &tDataStr[0]; /* String: "maintData" */
                } else {
                        tDataStr = &tDataStr[4]; /* String : "tData" */
                }

                if (isThreadStructData) {
                        if (td->bareMetalExt.hasExt) {
				if (activeTd->tid == MAIN_ID && hasWorkloadChecker) {
                               		if (activeTd->id == id) { 
                                        	fprintf(fw, "%s%s.workload = 0;\n", tab, tDataStr);
                                	} else if (activeTd->dependency[id] > 0) {
						fprintf(fw, "%s%s[%d].workload = 0;\n", tab, tDataStr, childIndex++);
					}
				}
                        }
                }
        }
	fprintf(fw, "\n");

        return;
}

void initMxapi(FILE *fw, struct tThreadData *activeTd, char *tab) {

	int t, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean hasMrapiExt = false, hasMcapiExt = false;

	numOfThreads = activeProc->numOfThreads;
	for (t = activeTd->id; t < numOfThreads; t++) {
		td = activeProc->threads[t];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mrapiExt.hasExt) {
				hasMrapiExt = true;
			}
			if (td->mcapiExt.hasExt) {
				hasMcapiExt = true;
			}
			if (hasMrapiExt && hasMrapiExt) {
				break;
			}
		}
	}

	/* Initialize child data structures */
       	initTls(fw, activeTd, tab);
       	initQuark(fw, activeTd, tab);
	

	if (hasMrapiExt) {
		fprintf(fw, "%smrapi_initialize(MRAPI_DOMAIN, td->tid, mrapiParms, &mrapiVersion, &mrapiStatus);\n", tab);
		fprintf(fw, "%sif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		fprintf(fw, "%s\n", tab);
	}

	if (hasMcapiExt) {
		fprintf(fw, "%smcapi_initialize(MCAPI_DOMAIN, td->tid, NULL, &mcapiParms, &mcapiVersion, &mcapiStatus);\n", tab);
		fprintf(fw, "%sif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\n", tab);
	}

	return;
}

/*************************************************** initialization **********************************************/



/***************************************************creation******************************************************/

int createTls(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (!isTlsVarArray) {
		if (activeTd->lsExt.hasExt) {
			for (i = 0; i < activeTd->lsExt.numOfLocalMems; i++) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* (create the local memory */\n", tab);
				fprintf(fw, "%s\tlocalAddr%d = (int *) malloc(sizeof(int) * %d);\n", tab, i + 1, activeTd->lsExt.localMems[i].size);
				//fprintf(fw, "%s\tif (localAddr%d == NULL) { status = \"ERR_NO_MEMORY\"; WRONG }\n", tab, i + 1);
				currIndex++;
			}
			for (i = 0; i < activeTd->lsExt.numOfSharedMems; i++) {
				if (currIndex > 0) {
                                        fprintf(fw, "%s\t\n", tab);
                                }
                                fprintf(fw, "%s\t/* (create the shared memory */\n", tab);
                                fprintf(fw, "%s\tshmAddr%d = (int *) malloc(sizeof(int) * %d);\n", tab, i + 1, activeTd->lsExt.localMems[i].size);
                                //fprintf(fw, "%s\tif (shmAddr%d == NULL) { status = \"ERR_NO_MEMORY\"; WRONG }\n", tab, i + 1);
                                currIndex++;
			}
		}
	}

	return currIndex;
}

int createMrapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (activeTd->mrapiExt.hasExt) {
		/* create semaphores */
		for (i = 0; i < activeTd->mrapiExt.numOfSemaphores; i++) {
			if (activeTd->mrapiExt.sems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* create the semaphore */\n", tab);
				fprintf(fw, "%s\tsem%d = mrapi_sem_create(0x%x, NULL /*attributes*/, LOCK_LIMIT, &mrapiStatus);\n",
						tab, i + 1, activeTd->mrapiExt.sems[i].key);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
			if (activeTd->mrapiExt.sems[i].isLockAfterCreate) {
				fprintf(fw, "%s\t\n", tab);
				semLock(fw, activeTd, i, tab, false);
			}
		}
		/* create mutexes */
		for (i = 0; i < activeTd->mrapiExt.numOfMutexes; i++) {
			if (activeTd->mrapiExt.mutexes[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* create the mutex */\n", tab);
				fprintf(fw, "%s\tmutex%d = mrapi_mutex_create(0x%x, NULL /*attributes*/, &mrapiStatus);\n",
						tab, i + 1, activeTd->mrapiExt.mutexes[i].key);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		/* create shared memory */
		for (i = 0; i < activeTd->mrapiExt.numOfSharedMems; i++) {
			if (activeTd->mrapiExt.sharedMems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* (create the shared memory and) attach to it */\n", tab);
				fprintf(fw, "%s\tshmem%d = mrapi_shmem_create(0x%x, %d /*size*/, NULL /* nodes list */, 0 /* nodes list size */, NULL /*attrs*/,"
						" 0 /*attrs size*/, &mrapiStatus);\n", tab, i + 1, activeTd->mrapiExt.sharedMems[i].key, activeTd->mrapiExt.sharedMems[i].size);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				fprintf(fw, "%s\t\n", tab);
				fprintf(fw, "%s\tshmAddr%d = mrapi_shmem_attach(shmem%d, &mrapiStatus);\n",
						tab, i + 1, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int getMrapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (activeTd->mrapiExt.hasExt) {
		/* get semaphores */
		for (i = 0; i < activeTd->mrapiExt.numOfSemaphores; i++) {
			if (!activeTd->mrapiExt.sems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* get the semaphore */\n", tab);
				fprintf(fw, "%s\tdo {\n", tab);
				fprintf(fw, "%s\t\tsem%d = mrapi_sem_get(0x%x, &mrapiStatus);\n",
						tab, i + 1, activeTd->mrapiExt.sems[i].key);
				fprintf(fw, "%s\t\tif (mrapiStatus == MRAPI_ERR_SEM_ID_INVALID) sched_yield();\n", tab);
				fprintf(fw, "%s\t} while (mrapiStatus == MRAPI_ERR_SEM_ID_INVALID);\n", tab);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		/* get mutexes */
		for (i = 0; i < activeTd->mrapiExt.numOfMutexes; i++) {
			if (!activeTd->mrapiExt.mutexes[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* get the mutex */\n", tab);
				fprintf(fw, "%s\tdo {\n", tab);
				fprintf(fw, "%s\t\tmutex%d = mrapi_mutex_get(0x%x, &mrapiStatus);\n",
						tab, i + 1, activeTd->mrapiExt.mutexes[i].key);
				fprintf(fw, "%s\t\tif (mrapiStatus == MRAPI_ERR_MUTEX_ID_INVALID) sched_yield();\n", tab);
				fprintf(fw, "%s\t} while (mrapiStatus == MRAPI_ERR_MUTEX_ID_INVALID);\n", tab);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		/* get shared memory */
		for (i = 0; i < activeTd->mrapiExt.numOfSharedMems; i++) {
			if (!activeTd->mrapiExt.sharedMems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* (get the shared memory and) attach to it */\n", tab);
				fprintf(fw, "%s\tdo {\n", tab);
				fprintf(fw, "%s\t\tshmem%d = mrapi_shmem_get(0x%x, &mrapiStatus);\n", tab, i + 1, activeTd->mrapiExt.sharedMems[i].key);
				fprintf(fw, "%s\t\tif (mrapiStatus == MRAPI_ERR_SHM_ID_INVALID) sched_yield();\n", tab);
				fprintf(fw, "%s\t} while (mrapiStatus == MRAPI_ERR_SHM_ID_INVALID);\n", tab);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				fprintf(fw, "%s\t\n", tab);
				fprintf(fw, "%s\tshmAddr%d = mrapi_shmem_attach(shmem%d, &mrapiStatus);\n",
						tab, i + 1, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

void openChan(FILE *fw, struct tThreadData *td, struct tMcapiChannel *pMcapiChan, int i, char *tab) {

	fprintf(fw, "%s\t\n", tab);
	fprintf(fw, "%s\t/* connect the channel */\n", tab);
	fprintf(fw, "%s\tdo {\n", tab);
	if (pMcapiChan->type == EP_PACKET) {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\tmcapi_pktchan_connect_i(ep%d, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, pMcapiChan->pSenderEp->portId, pMcapiChan->pSenderGetEp->printPortId);
		} else {
			fprintf(fw, "%s\t\tmcapi_pktchan_connect_i(ep%d, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, pMcapiChan->pReceiverGetEp->printPortId, pMcapiChan->pReceiverEp->portId);
		}
	} else {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_sclchan_connect_i(ep%d, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, pMcapiChan->pSenderEp->portId, pMcapiChan->pSenderGetEp->printPortId);
		} else {
			fprintf(fw, "%s\t\tmcapi_sclchan_connect_i(ep%d, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, pMcapiChan->pReceiverGetEp->printPortId, pMcapiChan->pReceiverEp->portId);
		}
	}
	fprintf(fw, "%s\t//retry if all request handles are in-use\n", tab);
	fprintf(fw, "%s\t} while (mcapiStatus == MCAPI_ERR_REQUEST_LIMIT);\n", tab);
	fprintf(fw, "%s\tmcapi_wait(&mcapiRequest, &size, MCA_INFINITE, &mcapiStatus);\n", tab);
	if (td == pMcapiChan->pSenderTd) {
		//fprintf(fw, "%s\tif (mcapiStatus != MCAPI_ERR_CHAN_CONNECTED && mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t/* If the status is MCAPI_ERR_ENDP_INVALID, we assume that there is problem with deletion */\n", tab);
				fprintf(fw, "%s\tif ((mcapiStatus != MCAPI_ERR_CHAN_CONNECTED && mcapiStatus != MCAPI_SUCCESS) && mcapiStatus != MCAPI_ERR_ENDP_INVALID) { MCAPI_WRONG }\n", tab);
	} else {
		fprintf(fw, "%s\t/* If the status is MCAPI_ERR_ENDP_INVALID, we assume that sender is done and sender endpoint is deleted */\n", tab);
		fprintf(fw, "%s\tif ((mcapiStatus != MCAPI_ERR_CHAN_CONNECTED && mcapiStatus != MCAPI_SUCCESS) && mcapiStatus != MCAPI_ERR_ENDP_INVALID) { MCAPI_WRONG }\n", tab);
	}

	fprintf(fw, "%s\t\n", tab);
	fprintf(fw, "%s\t/* open the endpoint handles */\n", tab);
	fprintf(fw, "%s\tdo {\n", tab);
	if (pMcapiChan->type == EP_PACKET) {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_pktchan_send_open_i(&pktSendHandle%d%d /*send_handle*/, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1, pMcapiChan->pSenderEp->portId);
		} else {
			fprintf(fw, "%s\t\tmcapi_pktchan_recv_open_i(&pktRecvHandle%d%d /*recv_handle*/, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1, pMcapiChan->pReceiverEp->portId);
		}
	} else {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_sclchan_send_open_i(&sclSendHandle%d%d /*send_handle*/, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1, pMcapiChan->pSenderEp->portId);
		} else {
			fprintf(fw, "%s\t\tmcapi_sclchan_recv_open_i(&sclRecvHandle%d%d /*recv_handle*/, ep%d, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1, pMcapiChan->pReceiverEp->portId);
		}
	}
	fprintf(fw, "%s\t//retry if all request handles are in-use\n", tab);
	fprintf(fw, "%s\t} while (mcapiStatus == MCAPI_ERR_REQUEST_LIMIT);\n", tab);
	fprintf(fw, "%s\tmcapi_wait(&mcapiRequest, &size, MCA_INFINITE, &mcapiStatus);\n", tab);
	fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);

	return;
}

int createMcapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;
	struct tMcapiEndpoint *mcapiEndpoint;

	if (activeTd->mcapiExt.hasExt) {
		/* endpoint create */
		for (i = 0; i < activeTd->mcapiExt.numOfEndpoints; i++) {
			mcapiEndpoint = &activeTd->mcapiExt.endpoints[i];
			if (mcapiEndpoint->isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* create an endpoint */\n", tab);
				fprintf(fw, "%s\tep%d = mcapi_endpoint_create(%d, &mcapiStatus);\n",
						tab, i + 1, mcapiEndpoint->portId);
				fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int getMcapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;
	struct tMcapiEndpoint *mcapiEndpoint;
	struct tMcapiChannel *pMcapiChan;

	if (activeTd->mcapiExt.hasExt) {
		/* endpoint get */
		for (i = 0; i < activeTd->mcapiExt.numOfEndpoints; i++) {
			mcapiEndpoint = &activeTd->mcapiExt.endpoints[i];
			if (!mcapiEndpoint->isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				if (mcapiEndpoint->type == EP_MESSAGE || mcapiEndpoint->isSender) {
					fprintf(fw, "%s\t/* get the endpoint */\n", tab);
					fprintf(fw, "%s\tep%d = mcapi_endpoint_get(MCAPI_DOMAIN, %d, %d, MCA_INFINITE, &mcapiStatus);\n",
							tab, i + 1, mcapiEndpoint->nodeId, mcapiEndpoint->portId);
					fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
				} else {
					fprintf(fw, "%s\t/* get the endpoint if exists*/\n", tab);
					fprintf(fw, "%s\tep%d = MCAPI_ENDPOINT_INVALID;\n", tab, i + 1);
					fprintf(fw, "%s\tmcapi_endpoint_get_i(MCAPI_DOMAIN, %d, %d, &ep%d, &mcapiRequest, &mcapiStatus);\n",
							tab, mcapiEndpoint->nodeId, mcapiEndpoint->portId, i + 1);
					fprintf(fw, "%s\tmcapi_wait(&mcapiRequest, &size, MCAPI_TIMEOUT, &mcapiStatus);\n", tab);
				}
				currIndex++;
			}
		}
		for (i = 0; i < activeTd->mcapiExt.numOfPcktChans; i++) {
			pMcapiChan = activeTd->mcapiExt.pcktChans[i];
			openChan(fw, activeTd, pMcapiChan, i, tab);
		}
		for (i = 0; i < activeTd->mcapiExt.numOfScalChans; i++) {
			pMcapiChan = activeTd->mcapiExt.scalChans[i];
			openChan(fw, activeTd, pMcapiChan, i, tab);
		}
	}

	return currIndex;
}

int createQuark(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {
    return 0;
}

int getQuark(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {
    return 0;
}

int createPosix(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (!activeTd->mrapiExt.hasExt && !activeTd->mcapiExt.hasExt) {
		/* create semaphores */
		for (i = 0; i < activeTd->posixExt.numOfSemaphores; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			fprintf(fw, "%s\t/* create the semaphore */\n", tab);
			if (!activeTd->posixExt.sems[i].isLockAfterCreate) {
				fprintf(fw, "%s\tsem%d = sem_open(\"sem%x\", O_CREAT, 0644, 1);\n", tab, i + 1, activeTd->posixExt.sems[i].key);
			} else {
				fprintf(fw, "%s\tsem%d = sem_open(\"sem%x\", O_CREAT, 0644, 0);\n", tab, i + 1, activeTd->posixExt.sems[i].key);
			}
			fprintf(fw, "%s\tif (sem%d == SEM_FAILED) { POSIX_WRONG }\n", tab, i + 1);
			currIndex++;
//			if (activeTd->posixExt.sems[i].isLockAfterCreate) {
//				fprintf(fw, "%s\t\n", tab);
//				posixSemLock(fw, activeTd, i, tab, false);
//			}
		}

	}

	return currIndex;
}

int createMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	fprintf(fw, "%s/* Create data structures */\n", tab);
	currIndex = createTls(fw, activeTd, tab, currIndex);
	currIndex = createMrapi(fw, activeTd, tab, currIndex);
	currIndex = createMcapi(fw, activeTd, tab, currIndex);
	currIndex = createQuark(fw, activeTd, tab, currIndex);
	currIndex = createPosix(fw, activeTd, tab, currIndex);

	return currIndex;
}

int getMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	currIndex = getMrapi(fw, activeTd, tab, currIndex);
	currIndex = getMcapi(fw, activeTd, tab, currIndex);
	currIndex = getQuark(fw, activeTd, tab, currIndex);

	return currIndex;
}

int createAndGetMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	currIndex = createTls(fw, activeTd, tab, currIndex);
	currIndex = createMrapi(fw, activeTd, tab, currIndex);
	currIndex = getMrapi(fw, activeTd, tab, currIndex);
	currIndex = createMcapi(fw, activeTd, tab, currIndex);
	currIndex = getMcapi(fw, activeTd, tab, currIndex);
	currIndex = createPosix(fw, activeTd, tab, currIndex);
	currIndex = createQuark(fw, activeTd, tab, currIndex);
        currIndex = getQuark(fw, activeTd, tab, currIndex);

	return currIndex;
}

/***************************************************operations******************************************************/

const char* getMemInitCode(memInitType initType) {
    const char* ZERO_STR = "0";

    const char* RAND_STR = "rand()";

    const char* ITER_STR = "shmMemIt";

    const char* DATA_STR = "data[shmMemIt]";

    if (initType == MEM_INIT_ZERO) {
        return ZERO_STR;
    } else if (initType == MEM_INIT_RAND) {
        return RAND_STR;
    } else if (initType == MEM_INIT_ITER) {
        return ITER_STR;
    } else if (initType == MEM_INIT_DATA) {
        return DATA_STR;
    }

    return NULL;
}


int globMemRead(FILE *fw, int index, int itStartIndex, int numOfIterations, char *tab) {

	if (index < activeProc->lsExt.numOfGlobMems) {
		if (numOfIterations <= activeProc->lsExt.globMems[index].size) {
			/* lock global mem */
			fprintf(fw, "%s\tfor (globMemIt = %d; globMemIt < %d; globMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\t\tglobMemTemp = globAddr%d[globMemIt]; /* read global mem */\n", tab, index + 1);
			fprintf(fw, "%s\t\t/* use globMemTemp */\n", tab);
			fprintf(fw, "%s\t}\n", tab);
		} else {
			printf("ERROR! Process %d, global mem (index = %d) size (%d) is smaller than %d!\n",
					activeProc->id, index, activeProc->lsExt.globMems[index].size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Process %d, no such global mem (index = %d)!\n", activeProc->id, index);
		return -1;
	}

	return 0;
}

int globMemWrite(FILE *fw, int index, int itStartIndex, int numOfIterations, char *tab) {

	if (index < activeProc->lsExt.numOfGlobMems) {
		if (numOfIterations <= activeProc->lsExt.globMems[index].size) {
			/* lock global mem */
			fprintf(fw, "%s\tfor (globMemIt = %d; globMemIt < %d; globMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\t\tglobMemTemp = globMemIt; /* assign globMemTemp */\n", tab);
			fprintf(fw, "%s\t\tglobAddr%d[globMemIt] = globMemTemp; /* write global mem */\n", tab, index + 1);
			fprintf(fw, "%s\t}\n", tab);
			/* unlock global mem */
		} else {
			printf("ERROR! Process %d, global mem (index = %d) size (%d) is smaller than %d!\n",
					activeProc->id, index, activeProc->lsExt.globMems[index].size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Process %d, no such global mem (index = %d)!\n", activeProc->id, index);
		return -1;
	}

	return 0;
}

int localMemRead(FILE *fw, struct tThreadData *td, int index, int itStartIndex, int numOfIterations, char *tab) {

	if (index < td->lsExt.numOfLocalMems) {
		if (itStartIndex + numOfIterations <= td->lsExt.localMems[index].size) {
			fprintf(fw, "%sfor (localMemIt = %d; localMemIt < %d; localMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\tlocalMemTemp = localAddr%d[localMemIt]; /* read local mem */\n", tab, index + 1);
			fprintf(fw, "%s\t/* use localMemTemp */\n", tab);
			fprintf(fw, "%s}\n", tab);
		} else {
			printf("ERROR! Thread %d, local mem (index = %d) size (%d) is smaller than %d!\n",
					td->id, index, td->lsExt.localMems[index].size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Thread %d, no such local mem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int localMemWrite(FILE *fw, struct tThreadData *td, int index, int itStartIndex, int numOfIterations, char *tab) {

	struct tLocalMem *pLclMem;	

	if (index < td->lsExt.numOfLocalMems) {
		pLclMem = &td->lsExt.localMems[index];
		if (itStartIndex + numOfIterations <= pLclMem->size) {
			fprintf(fw, "%sfor (localMemIt = %d; localMemIt < %d; localMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			if (pLclMem->initType == MEM_INIT_NOOP) {
				fprintf(fw, "%s\t/* Skipping Local Memory Initialization */\n", tab);
			} else if (pLclMem->initType == MEM_INIT_COPY) { 
				fprintf(fw, "%s\t/* Copy Previous Memory Initialization */\n", tab);
				fprintf(fw, "%s\tlocalMemTemp = shmAddr%d[localMemIt]; /* assign localMemTemp */\n", tab, index + 1);	
				fprintf(fw, "%s\tlocalAddr%d[localMemIt] = localMemTemp; /* write local mem */\n", tab, index + 1);
			} else {
                                fprintf(fw, "%s\tlocalMemTemp = %s; /* assign localMemTemp */\n", tab, getMemInitCode(pLclMem->initType));
                                fprintf(fw, "%s\tlocalAddr%d[localMemIt] = localMemTemp; /* write local mem */\n", tab, index + 1);
                        } 
			fprintf(fw, "%s}\n", tab);
		} else {
			printf("ERROR! Thread %d, local mem (index = %d) size (%d) is smaller than %d!\n",
					td->id, index, td->lsExt.localMems[index].size, itStartIndex + numOfIterations);
			return -2;
		}
	} else {
		printf("ERROR! Thread %d, no such local mem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;


}

int mutexLock(FILE *fw, struct tThreadData *td, int index, char *tab, boolean hasNewLine) {

	if (index < td->mrapiExt.numOfMutexes) {
		fprintf(fw, "%s\tmrapi_mutex_lock(mutex%d, &lockKey, MRAPI_TIMEOUT_INFINITE /*timeout*/, &mrapiStatus);\n", tab, index + 1);
		fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such mutex (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int mutexUnlock(FILE *fw, struct tThreadData *td, int index, char *tab, boolean hasNewLine) {

	if (index < td->mrapiExt.numOfMutexes) {
		fprintf(fw, "%s\tmrapi_mutex_unlock(mutex%d, &lockKey, &mrapiStatus);\n", tab, index + 1);
		fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such mutex (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int semLock(FILE *fw, struct tThreadData *td, int index, char *tab,  boolean hasNewLine) {

	if (index < td->mrapiExt.numOfSemaphores) {
		fprintf(fw, "%s\tmrapi_sem_lock(sem%d, MRAPI_TIMEOUT_INFINITE /*timeout*/, &mrapiStatus);\n", tab, index + 1);
		fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such sem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int semUnlock(FILE *fw, struct tThreadData *td, int index, boolean isWaitTillLocked, boolean hasNewLine,  char *tab) {

	if (index < td->mrapiExt.numOfSemaphores) {
		fprintf(fw, "%s\tdo {\n", tab);
		fprintf(fw, "%s\t\tmrapi_sem_unlock(sem%d, &mrapiStatus);\n", tab, index + 1);
		fprintf(fw, "%s\t} while (mrapiStatus == MRAPI_ERR_SEM_NOTLOCKED);\n", tab);
		fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such sem (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int barrierWait(FILE *fw, struct tThreadData *td, int index, char *tab) {

	struct tBarrier *pBarrier;

	if (index < td->mrapiExt.numOfBarriers) {
		pBarrier = &td->mrapiExt.barriers[index];
		fprintf(fw, "%s\tmrapi_barrier_wait(mutex%d, &barrier%d, %d /* num of threads */);\n",
				tab, pBarrier->pMutex->id, pBarrier->id, pBarrier->numOfThreads);
	} else {
		printf("ERROR! Thread %d, no such barrier (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int shmMemRead(FILE *fw, struct tThreadData *td, int index, int itStartIndex, int numOfIterations, char *tab) {

	struct tSharedMem *pShmMem;

	if (index < td->mrapiExt.numOfSharedMems) {
		pShmMem = &td->mrapiExt.sharedMems[index];
		if (itStartIndex + numOfIterations <= pShmMem->size) {
			mutexLock(fw, td, pShmMem->pMutex->id - 1, tab, true);
			fprintf(fw, "%sfor (shmMemIt = %d; shmMemIt < %d; shmMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\tshmMemTemp = td->shmAddr[%d][shmMemIt]; /* read shared mem */\n", tab, index);
			fprintf(fw, "%s\t/* use shmMemTemp */\n", tab);
			fprintf(fw, "%s}\n", tab);
			fprintf(fw, "%s\n", tab);
			mutexUnlock(fw, td, pShmMem->pMutex->id - 1, tab, false);
		} else {
			printf("ERROR! Thread %d, shared mem (index = %d) size (%d) is smaller than %d!\n",
					td->id, index, pShmMem->size, itStartIndex + numOfIterations);
			return -2;
		}
	} else if (index < td->lsExt.numOfSharedMems) {
		pShmMem = &td->lsExt.sharedMems[index];
                if (numOfIterations <= pShmMem->size) {
                        fprintf(fw, "%sfor (shmMemIt = %d; shmMemIt < %d; shmMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
                        fprintf(fw, "%s\tshmMemTemp = td->shmAddr[%d][shmMemIt]; /* read shared mem */\n", tab, index);
                        fprintf(fw, "%s\t/* use shmMemTemp */\n", tab);
                        fprintf(fw, "%s}\n", tab);
                        fprintf(fw, "%s\n", tab);
                } else {
                        printf("ERROR! Thread %d, shared mem (index = %d) size (%d) is smaller than %d!\n",
                                        td->id, index, pShmMem->size, itStartIndex + numOfIterations);
                        return -2;
                }	
	} else {
		printf("ERROR! Thread %d, no such shared mem (index = %d), numSharedMems = %d!\n", td->tid, index, td->lsExt.numOfSharedMems);
		return -1;
	}

	return 0;
}

int shmMemWrite(FILE *fw, struct tThreadData *td, int index, int itStartIndex, int numOfIterations, char *tab) {

	struct tSharedMem *pShmMem;

	if (index < td->mrapiExt.numOfSharedMems) {
		pShmMem = &td->mrapiExt.sharedMems[index];
		if (itStartIndex + numOfIterations <= pShmMem->size) {
			mutexLock(fw, td, pShmMem->pMutex->id - 1, tab, true);
			fprintf(fw, "%sfor (shmMemIt = %d; shmMemIt < %d; shmMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			fprintf(fw, "%s\tshmMemTemp = shmMemIt; /* assign shmMemTemp */\n", tab);
			fprintf(fw, "%s\tshmAddr%d[shmMemIt] = shmMemTemp; /* write shared mem */\n", tab, index + 1);
			fprintf(fw, "%s}\n", tab);
			fprintf(fw, "%s\n", tab);
			mutexUnlock(fw, td, pShmMem->pMutex->id - 1, tab, false);
		} else {
			printf("ERROR! Thread %d, shared mem (index = %d) size (%d) is smaller than %d!\n",
					td->id, index, pShmMem->size, itStartIndex + numOfIterations);
			return -2;
		}
	} else if (index < td->lsExt.numOfSharedMems) {
		pShmMem = &td->lsExt.sharedMems[index];
                if (numOfIterations <= pShmMem->size) {
                        fprintf(fw, "%sfor (shmMemIt = %d; shmMemIt < %d; shmMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			if (pShmMem->initType != MEM_INIT_NOOP) {
                        	fprintf(fw, "%s\tshmMemTemp = %s; /* assign shmMemTemp */\n", tab, getMemInitCode(pShmMem->initType));
                        	fprintf(fw, "%s\tshmAddr1[shmMemIt] = shmMemTemp; /* write shared mem */\n", tab);
			} else {
				fprintf(fw, "%s\t/* Skipping Shared Memory Initialization */\n", tab);
			}
                        fprintf(fw, "%s}\n", tab);
                        fprintf(fw, "%s\n", tab);
                } else {
                        printf("ERROR! Thread %d, shared mem (index = %d) size (%d) is smaller than %d!\n",
                                        td->id, index, pShmMem->size, itStartIndex + numOfIterations);
                        return -2;
                }
	} else {
		printf("ERROR! Thread %d, no such shared mem (index = %d)!\n", td->id, index);
		return -1;
	}

	if (index < td->bareMetalExt.numOfSharedMems) {
		pShmMem = &td->bareMetalExt.sharedMems[index];
                if (numOfIterations <= pShmMem->size) {
                        fprintf(fw, "%sfor (shmMemIt = %d; shmMemIt < %d; shmMemIt++) {\n", tab, itStartIndex, itStartIndex + numOfIterations);
			if (pShmMem->initType != MEM_INIT_NOOP) {
                                fprintf(fw, "%s\tshmMemTemp = %s; /* assign shmMemTemp */\n", tab, getMemInitCode(pShmMem->initType));	
                       		fprintf(fw, "%s\tshmAddr%d[shmMemIt] = shmMemTemp; /* write shared mem */\n", tab, index + 1); 
			} else {
                                fprintf(fw, "%s\t/* Skipping Shared Memory Initialization */\n", tab);
                        }
                        fprintf(fw, "%s}\n", tab);
                        fprintf(fw, "%s\n", tab);
                } else {
                        printf("ERROR! Thread %d, shared mem (index = %d) size (%d) is smaller than %d!\n",
                                        td->id, index, pShmMem->size, itStartIndex + numOfIterations);
                        return -2;
                }	
	}

	return 0;
}

int msgSendMcapi(FILE *fw, struct tThreadData *td, int sendIndex, int recvIndex, int count, char *tab) {

	if (sendIndex < td->mcapiExt.numOfEndpoints && recvIndex < td->mcapiExt.numOfEndpoints) {
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tmsg = \"mcapi_msg\";\n", tab);
		fprintf(fw, "%s\t\tsize = strlen(msg);\n", tab);
		fprintf(fw, "%s\t\tdo {\n", tab);
		fprintf(fw, "%s\t\t\tmcapi_msg_send(ep%d, ep%d, msg, size, 1 /*priority*/, &mcapiStatus);\n", tab, sendIndex + 1, recvIndex + 1);
		fprintf(fw, "%s\t\t} while (mcapiStatus == MCAPI_ERR_MEM_LIMIT);\n", tab);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such endpoint(s) (sendIndex = %d, recvIndex = %d)!\n", td->tid, sendIndex, recvIndex);
		return -1;
	}

	return 0;
}

int msgRecvMcapi(FILE *fw, struct tThreadData *td, int recvIndex, int count, char *tab) {

	if (recvIndex < td->mcapiExt.numOfEndpoints) {
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tmcapi_msg_recv(ep%d, buffer, BUFF_SIZE, &size, &mcapiStatus);\n", tab, recvIndex + 1);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t\t/* use buffer */\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such endpoint (recvIndex = %d)!\n", td->tid, recvIndex);
		return -1;
	}

	return 0;
}

int pktSendMcapi(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	//struct tMcapiChannel *pChannel;

	if (index < td->mcapiExt.numOfPcktChans) {
		//pChannel = td->mcapiExt.pcktChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tsprintf(buffer, \"mcapi_packet_%s\", i);\n", tab, "%d");
		fprintf(fw, "%s\t\tsize = strlen(buffer);\n", tab);
		fprintf(fw, "%s\t\tdo {\n", tab);
		fprintf(fw, "%s\t\t\tmcapi_pktchan_send(pktSendHandle%d%d, buffer, size, &mcapiStatus);\n", tab, td->tid, index + 1);
		fprintf(fw, "%s\t\t} while (mcapiStatus == MCAPI_ERR_MEM_LIMIT);\n", tab);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

int pktRecvMcapi(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	//struct tMcapiChannel *pChannel;

	if (index < td->mcapiExt.numOfPcktChans) {
		//pChannel = td->mcapiExt.pcktChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tmcapi_pktchan_recv(pktRecvHandle%d%d, (void **) ((void*) &buffer), &size, &mcapiStatus);\n",
				tab, td->tid, index + 1);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t\t/* use buffer */\n", tab);
		fprintf(fw, "%s\t\tmcapi_pktchan_release((void *) buffer, &mcapiStatus);\n", tab);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

int sclSendMcapi(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	struct tMcapiChannel *pChannel;

	if (index < td->mcapiExt.numOfScalChans) {
		pChannel = td->mcapiExt.scalChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		if (pChannel->size == 8) {
			fprintf(fw, "%s\tdata8 = (unsigned char) i;\n", tab);
			fprintf(fw, "%s\t\tdo {\n", tab);
			fprintf(fw, "%s\t\t\tmcapi_sclchan_send_uint8(sclSendHandle%d%d, data8, &mcapiStatus);\n", tab, td->id, index + 1);
		} else if (pChannel->size == 16) {
			fprintf(fw, "%s\t\tdata16 = (unsigned short) i;\n", tab);
			fprintf(fw, "%s\t\tdo {\n", tab);
			fprintf(fw, "%s\t\t\tmcapi_sclchan_send_uint16(sclSendHandle%d%d, data16, &mcapiStatus);\n", tab, td->id, index + 1);
		} else if (pChannel->size == 32) {
			fprintf(fw, "%s\t\tdata32 = (unsigned int) i;\n", tab);
			fprintf(fw, "%s\t\tdo {\n", tab);
			fprintf(fw, "%s\t\t\tmcapi_sclchan_send_uint32(sclSendHandle%d%d, data32, &mcapiStatus);\n", tab, td->id, index + 1);
		} else {
			fprintf(fw, "%s\t\tdata64 = (unsigned long long) i;\n", tab);
			fprintf(fw, "%s\t\tdo {\n", tab);
			fprintf(fw, "%s\t\t\tmcapi_sclchan_send_uint64(sclSendHandle%d%d, data64, &mcapiStatus);\n", tab, td->id, index + 1);
		}
		fprintf(fw, "%s\t\t} while (mcapiStatus == MCAPI_ERR_MEM_LIMIT);\n", tab);
		fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

int sclRecvMcapi(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	struct tMcapiChannel *pChannel;

	if (index < td->mcapiExt.numOfScalChans) {
		pChannel = td->mcapiExt.scalChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		if (pChannel->size == 8) {
			fprintf(fw, "%s\t\tdata8 = mcapi_sclchan_recv_uint8(sclRecvHandle%d%d, &mcapiStatus);\n", tab, td->id, index + 1);
			fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
			fprintf(fw, "%s\t\t/* use data8 */\n", tab);
		} else if (pChannel->size == 16) {
			fprintf(fw, "%s\t\tdata16 = mcapi_sclchan_recv_uint16(sclRecvHandle%d%d, &mcapiStatus);\n", tab, td->id, index + 1);
			fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
			fprintf(fw, "%s\t\t/* use data16 */\n", tab);
		} else if (pChannel->size == 32) {
			fprintf(fw, "%s\t\tdata32 = mcapi_sclchan_recv_uint32(sclRecvHandle%d%d, &mcapiStatus);\n", tab, td->id, index + 1);
			fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
			fprintf(fw, "%s\t\t/* use data32 */\n", tab);
		} else {
			fprintf(fw, "%s\t\tdata64 = mcapi_sclchan_recv_uint64(sclRecvHandle%d%d, &mcapiStatus);\n", tab, td->id, index + 1);
			fprintf(fw, "%s\t\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
			fprintf(fw, "%s\t\t/* use data64 */\n", tab);
		}
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->id, index);
		return -1;
	}

	return 0;
}

void printEpData(struct tMcapiEndpoint *ep) {

        printf("Endpoint Data:\n");
        printf("\tint nodeId:   %d\n", ep->nodeId);
        printf("\tint portId:   %d\n", ep->portId);
        printf("\tint printNodeId:      %d\n", ep->printNodeId);
        printf("\tint printPortId:      %d\n", ep->printPortId);
        printf("\tendpointType type:    ");
        if (ep->type == EP_MESSAGE) {
                printf("EP_MESSAGE\n");
        } else if (ep->type == EP_PACKET) {
                printf("EP_PACKET\n");
        } else if (ep->type == EP_SCALAR) {
                printf("EP_SCALAR\n");
        }
        printf("\tboolean isSender: %d\n", ep->isSender);
        printf("\tboolean isOwner:  %d\n", ep->isOwner);

        return;
}

void printMsgOp(struct tMsgOp *msgop) {

        printf("Message Operation Data:\n");
        printf("\tint resId:            %d\n", msgop->resId);
        printf("\tint resId2:           %d\n", msgop->resId2);
        printf("\tint numOfIterations:  %d\n", msgop->numOfIterations);
        printf("\tint time:             %d\n", msgop->time);
        printf("\tmsgOpType opType:     ");
        if (msgop->opType == MSG_OP_SEND) {
                printf("MSG_OP_SEND\n");
        } else if (msgop->opType == MSG_OP_RECV) {
                printf("MSG_OP_RECV\n");
        }
        printf("\tmsgType type:         ");
        if (msgop->type == MSG_TYPE_MSG) {
                printf("MSG_TYPE_MSG\n");
        } else if (msgop->type == MSG_TYPE_PKT) {
                printf("MSG_TYPE_PKT\n");
        } else if (msgop->type == MSG_TYPE_SCL) {
                printf("MSG_TYPE_SCL\n");
        }

        return;
}

int msgSendQuark(FILE *fw, struct tThreadData *td, int sendIndex, int recvIndex, int count, char *tab) {

	struct tMcapiEndpoint *ep = &td->bareMetalExt.endpoints[recvIndex];
    struct tThreadData *recvTd = activeProc->threads[ep->nodeId];
	if (sendIndex < td->bareMetalExt.numOfEndpoints && recvIndex < td->bareMetalExt.numOfEndpoints) {
		if ((td->workCheck.workAck == MEM_TYPE_SHARED)) {
			fprintf(fw, "%ssendmsg(", tab);
            printCoreIdQuark(fw, recvTd);
            fprintf(fw, ", ");
            printPortIdQuark(fw, ep);
            fprintf(fw, ", retVal);\n");	
		} else {
            fprintf(fw, "%ssendmsg(", tab);
            printCoreIdQuark(fw, activeProc->threads[ep->nodeId]);
            fprintf(fw, ", ");
            printPortIdQuark(fw, ep);
            fprintf(fw, ", localAddr%d[0]);\n", td->lsExt.numOfLocalMems);
		}
	} else {
		printf("ERROR! Thread %d, no such endpoint(s) (sendIndex = %d, recvIndex = %d)!\n", td->tid, sendIndex, recvIndex);
		return -1;
	}
	return 0;
}

int msgRecvQuark(FILE *fw, struct tThreadData *recvTd, int recvIndex, int count, char *tab) {

	struct tMcapiEndpoint *ep = &recvTd->bareMetalExt.endpoints[recvIndex];

	/* Iterate through Memory Operation to determine where to save Recv Message */

	if (recvIndex < recvTd->bareMetalExt.numOfEndpoints) {
		/* Check if pattern uses shared or local memory to determine if data needs to be captured */
		if (recvTd->lsExt.numOfSharedMems > 0) {	
			/* Since Global Memory is shared between all threads, we only want to recv a valid bit */
			fprintf(fw, "%srecvmsg(td->addr, ", tab);
            printPortIdQuark(fw, ep);
            fprintf(fw, ");\n");
		} else if (recvTd->lsExt.numOfLocalMems > 0) {
			/* Local Memory must receive computed data from a thread */
			if (recvTd->tid == MAIN_ID && hasWorkloadChecker) {
				fprintf(fw, "%std->workload += recvmsg(td->addr, ", tab);
			} else {
				if (recvTd->mtapiExt.loopCount > 0) {
					fprintf(fw, "%slocalAddr%d[loopCount] += recvmsg(td->addr, ", tab, recvTd->lsExt.numOfLocalMems);
				} else {
					fprintf(fw, "%slocalAddr%d[0] += recvmsg(td->addr, ", tab, recvTd->lsExt.numOfLocalMems);
				}
			}
            printPortIdQuark(fw, ep);
            fprintf(fw, ");\n");
		} else {
			printf("ERROR! Thread %d, only has %d memory operations\n", recvTd->tid, recvTd->lsExt.numOfMemOps);
		}
	} else {
		printf("ERROR! Thread %d, no such endpoint (recvIndex = %d)!\n", recvTd->tid, recvIndex);
		printf("ERROR! Thread %d, only has %d endpoints\n", recvTd->tid, recvTd->bareMetalExt.numOfEndpoints);
		return -1;
	}
	return 0;
}

int msgSend(FILE *fw, struct tThreadData *td, int sendIndex, int recvIndex, int count, char *tab) {

	if (td->bareMetalExt.hasExt) {
                if (activeProc->quarkExt.hasExt) {
                        return msgSendQuark(fw, td, sendIndex, recvIndex, count, tab);
                } else {
                        printf("ERROR: Unknown Bare Metal Architecture\n");
                        exit(0);
                }
        } else if (td->mcapiExt.hasExt) {
                return msgSendMcapi(fw, td, sendIndex, recvIndex, count, tab); 
        } else {
                printf("ERROR: Extension Type not known for msgRecv\n");
                exit(0);
        }

        return 0;

}

int msgRecv(FILE *fw, struct tThreadData *recvTd, int recvIndex, int count, char *tab) {

	if (recvTd->bareMetalExt.hasExt) {
                if (activeProc->quarkExt.hasExt) {
                        return msgRecvQuark(fw, recvTd, recvIndex, count, tab);
                } else {
                        printf("ERROR: Unknown Bare Metal Architecture\n");
                        exit(0);
                }
        } else if (recvTd->mcapiExt.hasExt) {
                return msgRecvMcapi(fw, recvTd, recvIndex, count, tab);
        } else {
                printf("ERROR: Extension Type not known for sclRecv\n");
                exit(0);
        }

        return 0;
}

int pktSendQuark(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	//struct tMcapiChannel *pChannel;

        if (index < td->bareMetalExt.numOfPcktChans) {
                //pChannel = td->bareMetalExt.pcktChans[index];
                fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
                fprintf(fw, "%s\t\tsprintf(buffer, \"quark_packet_%s\", i);\n", tab, "%d");
                fprintf(fw, "%s\t\tsize = strlen(buffer);\n", tab);
                fprintf(fw, "%s\t\tdo {\n", tab);
                fprintf(fw, "%s\t\t\tquark_pktchan_send(pktSendHandle%d%d, buffer, size, &mcapiStatus);\n", tab, td->tid, index + 1);
                fprintf(fw, "%s\t\t} while (quarkStatus == MCAPI_ERR_MEM_LIMIT);\n", tab);
                fprintf(fw, "%s\t\tif (quarkStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
                fprintf(fw, "%s\t}\n", tab);
        } else {
                printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->tid, index);
                return -1;
        }

        return 0;
	
}

int pktRecvQuark(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	//struct tMcapiChannel *pChannel;

	if (index < td->bareMetalExt.numOfPcktChans) {
		//pChannel = td->bareMetalExt.pcktChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		fprintf(fw, "%s\t\tquark_pktchan_recv(pktRecvHandle%d%d, (void **) ((void*) &buffer), &size, &quarkStatus);\n",
				tab, td->tid, index + 1);
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

int pktSend(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	if (td->bareMetalExt.hasExt) {
                if (activeProc->quarkExt.hasExt) {
                        return pktSendQuark(fw, td, index, count, tab);
                } else {
                        printf("ERROR: Unknown Bare Metal Architecture\n");
                        exit(0);
                }
        } else if (td->mcapiExt.hasExt) {
                return pktSendMcapi(fw, td, index, count, tab);
        } else {
                printf("ERROR: Extension Type not known for sclRecv\n");
                exit(0);
        }

        return 0;	
}

int pktRecv(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

        if (td->bareMetalExt.hasExt) {
                if (activeProc->quarkExt.hasExt) {
                        return pktRecvQuark(fw, td, index, count, tab);
                } else {
                        printf("ERROR: Unknown Bare Metal Architecture\n");
                        exit(0);
                }
        } else if (td->mcapiExt.hasExt) {
                return pktRecvMcapi(fw, td, index, count, tab);
        } else {
                printf("ERROR: Extension Type not known for sclRecv\n");
                exit(0);
        }

        return 0;
}


int sclSendQuark(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	struct tMcapiChannel *pChannel;

	if (index < td->bareMetalExt.numOfScalChans) {
		pChannel = td->bareMetalExt.scalChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		if (pChannel->size == 8) {
			fprintf(fw, "%s\tdata8 = (unsigned char) i;\n", tab);
			fprintf(fw, "%s\t\t\tquark_sclchan_send_uint8(sclSendHandle%d%d, data8, &quarkStatus);\n", tab, td->tid, index + 1);
		} else if (pChannel->size == 16) {
			fprintf(fw, "%s\t\tdata16 = (unsigned short) i;\n", tab);
			fprintf(fw, "%s\t\t\tquark_sclchan_send_uint16(sclSendHandle%d%d, data16, &quarkStatus);\n", tab, td->tid, index + 1);
		} else if (pChannel->size == 32) {
			fprintf(fw, "%s\t\tdata32 = (unsigned int) i;\n", tab);
			fprintf(fw, "%s\t\t\tquark_sclchan_send_uint32(sclSendHandle%d%d, data32, &quarkStatus);\n", tab, td->tid, index + 1);
		} else {
			fprintf(fw, "%s\t\tdata64 = (unsigned long long) i;\n", tab);
			fprintf(fw, "%s\t\t\tquark_sclchan_send_uint64(sclSendHandle%d%d, data64, &quarkStatus);\n", tab, td->tid, index + 1);
		}
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

int sclRecvQuark(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

	struct tMcapiChannel *pChannel;

	if (index < td->bareMetalExt.numOfScalChans) {
		pChannel = td->bareMetalExt.scalChans[index];
		fprintf(fw, "%s\tfor (i = 0; i < %d; i++) {\n", tab, count);
		if (pChannel->size == 8) {
			fprintf(fw, "%s\t\tdata8 = quark_sclchan_recv_uint8(sclRecvHandle%d%d, &quarkStatus);\n", tab, td->tid, index + 1);
			fprintf(fw, "%s\t\t/* use data8 */\n", tab);
		} else if (pChannel->size == 16) {
			fprintf(fw, "%s\t\tdata16 = quark_sclchan_recv_uint16(sclRecvHandle%d%d, &quarkStatus);\n", tab, td->tid, index + 1);
			fprintf(fw, "%s\t\t/* use data16 */\n", tab);
		} else if (pChannel->size == 32) {
			fprintf(fw, "%s\t\tdata32 = quark_sclchan_recv_uint32(sclRecvHandle%d%d, &quarkStatus);\n", tab, td->tid, index + 1);
			fprintf(fw, "%s\t\t/* use data32 */\n", tab);
		} else {
			fprintf(fw, "%s\t\tdata64 = quark_sclchan_recv_uint64(sclRecvHandle%d%d, &quarkStatus);\n", tab, td->tid, index + 1);
			fprintf(fw, "%s\t\t/* use data64 */\n", tab);
		}
		fprintf(fw, "%s\t}\n", tab);
	} else {
		printf("ERROR! Thread %d, no such pkt channel (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

int sclRecv(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

        if (td->bareMetalExt.hasExt) {
                if (activeProc->quarkExt.hasExt) {
                        return sclRecvQuark(fw, td, index, count, tab);
                } else {
                        printf("ERROR: Unknown Bare Metal Architecture\n");
                        exit(0);
                }
        } else if (td->mcapiExt.hasExt) {
                return sclRecvMcapi(fw, td, index, count, tab); 
        } else {
                printf("ERROR: Extension Type not known for sclRecv\n");
                exit(0);
        }

        return 0;
}

int sclSend(FILE *fw, struct tThreadData *td, int index, int count, char *tab) {

        if (td->bareMetalExt.hasExt) {
                if (activeProc->quarkExt.hasExt) {
                        return sclSendQuark(fw, td, index, count, tab);
                } else {
                        printf("ERROR: Unknown Bare Metal Architecture\n");
                        exit(0);
                }
        } else if (td->mcapiExt.hasExt) {
                return sclSendMcapi(fw, td, index, count, tab);
        } else {
                printf("ERROR: Extension Type not known for sclSend\n");
                exit(0);
        }

        return 0;


}

int posixMutexLock(FILE *fw, struct tThreadData *td, int index, char *tab, boolean hasNewLine) {

	if (index < td->posixExt.numOfMutexes) {
		fprintf(fw, "%s\tretVal = pthread_mutex_lock(mutex%d);\n", tab, index + 1);
		fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such posix mutex (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

int posixMutexUnlock(FILE *fw, struct tThreadData *td, int index, char *tab, boolean hasNewLine) {

	if (index < td->posixExt.numOfMutexes) {
		fprintf(fw, "%s\tpthread_mutex_unlock(mutex%d);\n", tab, index + 1);
		fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such posix mutex (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

int posixSemLock(FILE *fw, struct tThreadData *td, int index, char *tab,  boolean hasNewLine) {

	if (index < td->posixExt.numOfSemaphores) {
		fprintf(fw, "%s\tretVal = sem_wait(sem%d);\n", tab, index + 1);
		fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such posix sem (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

int posixSemUnlock(FILE *fw, struct tThreadData *td, int index, boolean isWaitTillLocked, boolean hasNewLine,  char *tab) {

	if (index < td->posixExt.numOfSemaphores) {
		fprintf(fw, "%s\tretVal = sem_post(sem%d);\n", tab, index + 1);
		fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
		if (hasNewLine) {
			fprintf(fw, "%s\t\n", tab);
		}
	} else {
		printf("ERROR! Thread %d, no such posix sem (index = %d)!\n", td->tid, index);
		return -1;
	}

	return 0;
}

/********************** buildWorkloadResults **********
Description:


Input:
int tIdx			: Thread Index 
unsigned *pWorkload		: Array of Unique Workload Paths

Output:
int result			: Returns Workload Result (based on dependency tree) starting from Thread Index 'tIdx'

Assumptions:

*****************************************************/
int buildWorkloadResults(struct tThreadData *td, int tIdx, unsigned *pWorkload) {

    int i;
    struct tThreadData *itTd = activeProc->threads[tIdx];
    unsigned result = 0;

    for (i = MAIN_ID; i < activeProc->numOfThreads + MAIN_ID; i++) { /* Only get the results which were passed based on dependencies */
        if (itTd->dependency[i] && i != itTd->id) {
            if (td->standAloneExt.parPattern == PP_RD) {
                result = result | buildWorkloadResults(td, i, pWorkload);
            } else {
                result += buildWorkloadResults(td, i, pWorkload);
            }
        }
    }

    if (td->standAloneExt.parPattern == PP_RD) {
        result = result | itTd->workCheck.workOut;
    } else {
        result += itTd->workCheck.workOut;
    }

    #ifdef WORKLOAD_DEBUG_ON
	printf	("WORKLOAD_DEBUG: Thread %d received a commulative result of %d and added it's workload %d for a total %d\n", 
		itTd->tid, result - itTd->workCheck.workOut, itTd->workCheck.workOut, result);
    #endif

	if (td->id == itTd->id) {
		*pWorkload = result;
		pWorkload++;
	}

        return result;
}

/* ******************* getNumOfPaths ******************************
Description: 
Find the number of unique paths from the calling thread 'td' to any endpoint which has no dependencies
The paths are based on the dependency trees found within 'struct tThreadData'

Inputs:
struct tThreadData *td 	: The Calling Thread. We determine the number of Unique paths starting at 'this' thread 
int *totalDependencies 	: Pointer to an Array of Total Dependencies
			  Example: totalDependencies[x] = 3 means a thread with id 'x' depends on 3 unique threads

Outputs:
int numOfPaths		: returns the Number of Unique Paths Starting from the Calling thread 'td'			

Assumptions:
If Thread 1 has a dependency on Thread 2 (ie receives a workload message from thread 2)
then td->dependency[itTd->id] where td->tid = 1 and ItTd->tid = 2
***************************************************************** */
int getNumOfPaths(struct tThreadData *td, int *totalDependencies) {
        int x;
        struct tThreadData *itTd;
        int numOfPaths = 0;
        for (x = MAIN_ID; x < activeProc->numOfThreads + MAIN_ID; x++) {
                itTd = activeProc->threads[x];
                if (td->dependency[itTd->id]) {
                        if(totalDependencies[itTd->id] == 0 && itTd->creator != td->id) { /* itTd is an endpoint / end of the path */
                                numOfPaths++;
                        } else {
				printf("Getting num of paths for thread %d\n", itTd->tid);
                                numOfPaths += getNumOfPaths(itTd, totalDependencies);
                        }
                }
        }

        if (numOfPaths == 0) {
                return 1;
        } else {
                return numOfPaths;
        }
}

/* ********************** calcWorkload ************************
Inputs:
struct tThreadData *td : The workload from 'this' threads dependency tree 
unsigned **pWorkload   : A pointer to an array of possible workloads based on the dependency tree

Outputs:
return (int)	       : Returns to size of the workload array 

Assumptions	       : All workloads are of 'unisnged' type		
*************************************************************** */ 
int calcWorkload(struct tThreadData *td, unsigned **pWorkload) {

	int x, y;
	int size = 0;
	int totalDependencies[MAX_THREADS];

	struct tThreadData *itTd;

	/* Allocate Memory for all Expected Workload Paths */
	for (x = MAIN_ID; x < activeProc->numOfThreads + MAIN_ID; x++) {
		itTd = activeProc->threads[x];
		for (y = MAIN_ID + 1; y < activeProc->numOfThreads + MAIN_ID; y++) {
			if(itTd->dependency[y] && x != y) {
				totalDependencies[x]++; 
			}	
		}
	}

	/* FIXME: Only Qorking with one unique path right now
	size = getNumOfPaths(td, totalDependencies); 
	printf("getNumOfPaths = %d\n", size);
	*/ 
	
	size = 1;
	*pWorkload = malloc(size * sizeof(unsigned));
	buildWorkloadResults(td, td->id, *pWorkload);

	return size; 
		
} 

void assignWorkload(FILE *fw, struct tThreadData *td, char* tab) {

        int i;

        for (i = 0; i < td->lsExt.numOfSharedMems; i++) {
                if (td->standAloneExt.parPattern == PP_DaC) {
                        fprintf(fw, "%std->workload = strcmp((char*) td->shmAddr[%d], (char*) MergeSort(localAddr%d, %d));\n",
                        tab, i, i + 1, td->lsExt.sharedMems[i].size);
                } else if (td->standAloneExt.parPattern == PP_RD) {
                        fprintf(fw, "%std->workload = (td->workload ==  Search(data, %d, %d));\n",
                        tab, td->lsExt.sharedMems[i].size, td->standAloneExt.queue);         
                } else {
                        fprintf(fw, "%std->workload = %s(td->shmAddr[%d], %d);\n", tab, workloadFnName, i, td->lsExt.sharedMems[i].size);
                }
        }

        for (i=0; i < td->lsExt.numOfLocalMems; i++) {
            /** do nothing for localMems */
        }

        return;
}

int doMtapiOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j, k, count1, count2;
	struct tWorkOp *pWorkOp;
	workOpType type;
	char* tStr;
	char mainThreadStr[] = "td->";
	char otherThreadStr[] = "td->";
	int tid = td->tid;

	if (td->tid == MAIN_ID) {
		tStr = mainThreadStr;
	} else {
		tStr = otherThreadStr;
	}	

	for (i = 0; i < td->mtapiExt.numOfWorks; i++) {
		pWorkOp = &td->mtapiExt.workOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pWorkOp->time == j) {
				if (pWorkOp->numOfIterations > 0) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					type = pWorkOp->type;
					fprintf(fw, "%s/*\n", tab);
					fprintf(fw, "%s * ==========================================================\n", tab);
					fprintf(fw, "%s * ====================== DO WORK-%d =========================\n", tab, i + 1);
					fprintf(fw, "%s * Description: %s\n", tab, getOperationTypeText(type));
					fprintf(fw, "%s * ==========================================================\n", tab);
					fprintf(fw, "%s */\n", tab);

					// variable decleration
					if (type == WORK_OP_IPC_INC) {
						if (codeLevel == 1) {
							fprintf(fw, "%sint i1_%d = 288, i2_%d = 3, ires_%d = 0;\n", tab, tid, tid, tid);
						}
					} else if (type == WORK_OP_IPC_DEC) {
						if (codeLevel == 1) {
							fprintf(fw, "%sdouble d1_%d = 288, d2_%d = 3, dres_%d = 0;\n", tab, tid, tid, tid);
						}
					} else if (type == WORK_OP_CACHE_HIT) {
						fprintf(fw, "%sint i_%d, j_%d;\n", tab, tid, tid);
					} else if (type == WORK_OP_CACHE_MISS) {
						fprintf(fw, "%sregister int wi_%d;\n", tab, tid);
						fprintf(fw, "%sint *wa_%d, wsize_%d = (1024 * 1024) * 4;\n", tab, tid, tid);
						fprintf(fw, "%swa_%d = (int *) malloc(wsize_%d * sizeof(int));\n", tab, tid, tid);
					} else if (type == WORK_OP_BRNCH_HIT) {
						if (codeLevel == 1) {
							fprintf(fw, "%sint b1_%d = 288, b2_%d = 3, bres_%d = 0;\n", tab, tid, tid, tid);
						}
					} else if (type == WORK_OP_BRNCH_MISS) {
						if (codeLevel == 1) {
							fprintf(fw, "%sint b1_%d = 288, b2_%d = 3, bres_%d = 0;\n", tab, tid, tid, tid);
						}
						fprintf(fw, "%sunsigned int randNum_%d, r3_%d, r4_%d, r8_%d;\n", tab, tid, tid, tid, tid);
					} else if (type == WORK_OP_COMM_INC) {
						fprintf(fw, "%sint c1_%d = 288, c2_%d = 3, cres_%d = 0;\n", tab, tid, tid, tid);
					} else if (type == WORK_OP_COMP_INC) {
						fprintf(fw, "%sint c1_%d = 288, c2_%d = 3, cres_%d = 0;\n", tab, tid, tid, tid);
					} 


					if (type != WORK_OP_FUNC) {
						// code blocks
				        	fprintf(fw, "%sfor (workCount = 0; workCount < %u; workCount++) {\n", tab, pWorkOp->numOfIterations);
				        	fprintf(fw, "%s\t/* do something */\n", tab);
					}
	
					if (type == WORK_OP_IPC_INC) {
						if (codeLevel == 1) {
							count1 = 40;
							count2 = 0;
						} else {
							count1 = 0; //(int) ((double) (pWorkOp->type1Percent * 40) / 100);
							count2 = 40; //40 - count1;
						}
						for (k = 0; k < count1; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\tires_%d = i1_%d + i2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t__asm__ (\"add $0x1,%seax\");\n", tab, "%");
							}
						}
						for (k = 0; k < count2; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\tires_%d = i1_%d;\n", tab, tid, tid);
							} else {
								fprintf(fw, "%s\t__asm__ (\"mov $0x1,%seax\");\n", tab, "%");
							}
						}
					} else if (type == WORK_OP_IPC_DEC) {
						if (codeLevel == 0) {
							fprintf(fw, "%s\t__asm__ (\"movl $0x5,%seax\");\n", tab, "%");
							fprintf(fw, "%s\t__asm__ (\"movl %seax,%sedx\");\n", tab, "%", "%");
							fprintf(fw, "%s\t__asm__ (\"sar $0x1f,%sedx\");\n", tab, "%");
						}
						for (k = 0; k < 50; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\tdres_%d = d1_%d / d2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t__asm__ (\"idivl %seax\");\n", tab, "%");
							}
						}
					} else if (type == WORK_OP_CACHE_HIT) {
						fprintf(fw, "%s\tfor (i_%d = 0; i_%d < 256; i_%d += 1) {\n", tab, tid, tid, tid);
						fprintf(fw, "%s\t\tfor (j_%d = 0; j_%d < 256; j_%d += 1) {\n", tab, tid, tid, tid);
						fprintf(fw, "%s\t\t\twa[i_%d][j_%d] = 2 * wa[i_%d][j_%d];\n", tab, tid, tid, tid, tid);
						fprintf(fw, "%s\t\t}\n", tab);
						fprintf(fw, "%s\t}\n", tab);
					} else if (type == WORK_OP_CACHE_MISS) {
						fprintf(fw, "%s\twa_%d[rand() %s wsize_%d] = 0;\n", tab, tid, "%", tid);
					} else if (type == WORK_OP_BRNCH_HIT) {
						fprintf(fw, "%s\tif (workCount >= 0) {\n", tab);
						for (k = 0; k < 2; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\t\tbres_%d = b1_%d + b2_%d + (b1_%d / b2_%d);\n", tab, tid, tid, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t\t\t__asm__ (\"idivl %seax\");\n", tab, "%");
							}
						}
						fprintf(fw, "%s\t}\n", tab);
					} else if (type == WORK_OP_BRNCH_MISS) {
						fprintf(fw, "%s\trandNum_%d = rand();\n", tab, tid);
						fprintf(fw, "%s\tr3_%d = randNum_%d %s 3;\n", tab, tid, tid, "%");
						for (k = 0; k < 2; k++) {
							fprintf(fw, "%s\tif (r3_%d == %d) {\n", tab, tid, k);
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\tbres_%d = b1_%d + b2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
							}
							fprintf(fw, "%s\t}\n", tab);
						}
						fprintf(fw, "%s\tr4_%d = randNum_%d %s 4;\n", tab, tid, tid, "%");
						for (k = 0; k < 2; k++) {
							fprintf(fw, "%s\tif (r4_%d == %d) {\n", tab, tid, k);
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\tbres_%d = b1_%d + b2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
							}
							fprintf(fw, "%s\t}\n", tab);
						}
						fprintf(fw, "%s\tr8_%d = randNum_%d %s 8;\n", tab, tid, tid, "%");
						for (k = 0; k < 4; k++) {
							fprintf(fw, "%s\tif (r8_%d == %d) {\n", tab, tid, k);
							if (codeLevel == 1) {
								fprintf(fw, "%s\t\tbres_%d = b1_%d + b2_%d;\n", tab, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
							}
							fprintf(fw, "%s\t}\n", tab);
						}
					} else if (type == WORK_OP_COMP_INC) {
						if (codeLevel == 0) {
							fprintf(fw, "%s\t__asm__ (\"movl $0x5,%seax\");\n", tab, "%");
							fprintf(fw, "%s\t__asm__ (\"movl %seax,%sedx\");\n", tab, "%", "%");
							fprintf(fw, "%s\t__asm__ (\"sar $0x1f,%sedx\");\n", tab, "%");
						}
						for (k = 0; k < 2; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\tcres_%d = ((2 * c1_%d) + (c2_%d / 2)) / ((3 * c1_%d) + (c2_%d / 3));\n",
										tab, tid, tid, tid, tid, tid);
							} else {
								fprintf(fw, "%s\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t__asm__ (\"add %sedx,%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t__asm__ (\"idivl %seax\");\n", tab, "%");
							}
						}
					} else if (type == WORK_OP_COMM_INC) {
						for (k = 0; k < 5; k++) {
							if (codeLevel == 1) {
								fprintf(fw, "%s\tc1_%d = c2_%d;\n", tab, tid, tid);
								fprintf(fw, "%s\tcres_%d = c1_%d;\n", tab, tid, tid);
								fprintf(fw, "%s\tc2_%d = c1_%d;\n", tab, tid, tid);
								fprintf(fw, "%s\tcres_%d = c2_%d;\n", tab, tid, tid);
							} else {
								fprintf(fw, "%s\t__asm__ (\"mov -0x10(%srbp),%seax\");\n", tab, "%", "%");
								fprintf(fw, "%s\t__asm__ (\"mov -0x14(%srbp),%sedx\");\n", tab, "%", "%");
								fprintf(fw, "%s\t__asm__ (\"mov %seax,-0x18(%srbp)\");\n", tab, "%", "%");
							}
						}
					} else if (type == WORK_OP_FUNC) {
						if (pWorkOp->workFunc.type == FUNC_SORT) {
							fprintf(fw, "%s%s(%s%sAddr[%d], %s%sAddr[%d] + %d, %s%sAddr[%d] + %d, localAddr%d); /* FUNC_SORT */\n", 
							tab, getWorkFunctionStr(pWorkOp->workFunc.type), 
							tStr, getThreadAddrType(td), td->lsExt.numOfSharedMems - 1,
							tStr, getThreadAddrType(td), td->lsExt.numOfSharedMems - 1, pWorkOp->numOfIterations / 2,
							tStr, getThreadAddrType(td), td->lsExt.numOfSharedMems - 1, pWorkOp->numOfIterations,
							td->lsExt.numOfLocalMems);
                        } else if (pWorkOp->workFunc.type == FUNC_SEARCH) {
                            fprintf(fw, "%sretVal = retVal | %s(%s%sAddr[%d], %d, %d); /* FUNC_SEARCH */\n",
                            tab, getWorkFunctionStr(pWorkOp->workFunc.type),    
                            tStr, getThreadAddrType(td), td->lsExt.numOfSharedMems - 1,
                            pWorkOp->numOfIterations,
                            activeProc->threads[MAIN_ID]->standAloneExt.queue);
						} else if (td->mtapiExt.loopCount > 0) {
							if (strcmp(getThreadAddrType(td), "shm") == 0) {
                                                                fprintf(fw, "%s%s%sAddr[loopCount][%d] += ",
                                                                tab, tStr, getThreadAddrType(td), td->lsExt.memOps[i].startIndex);
                                                        } else if (strcmp(getThreadAddrType(td), "local") == 0) {
                                                                fprintf(fw, "%s%sAddr%d[%d] += ",
                                                                tab, getThreadAddrType(td), td->lsExt.numOfLocalMems, td->lsExt.memOps[i].startIndex);
                                                        } else {
                                                                fprintf(fw, "%s%s%sAddr[loopCount] += ",
                                                                tab, tStr, getThreadAddrType(td));
                                                        }	
						} else {
							if (strcmp(getThreadAddrType(td), "shm") == 0) {
								fprintf(fw, "%s%s%sAddr[%d][%d] += ", 
								tab, tStr, getThreadAddrType(td), td->lsExt.numOfSharedMems - 1, td->lsExt.memOps[i].startIndex);
							} else if (strcmp(getThreadAddrType(td), "local") == 0) {
								fprintf(fw, "%s%sAddr%d[%d] += ", 
								tab, getThreadAddrType(td), td->lsExt.numOfLocalMems, td->lsExt.memOps[i].startIndex);
							} else {
								fprintf(fw, "%s%s%sAddr[%d] += ", 
								tab, tStr, getThreadAddrType(td), td->lsExt.memOps[i].startIndex);
							}
						}
						if (pWorkOp->workFunc.type == FUNC_NOOP) {
							fprintf(fw, "%s; /* FUNC_NOOP */\n", getWorkFunctionStr(pWorkOp->workFunc.type));
						} else if (pWorkOp->workFunc.type == FUNC_RECURSIVE) {
							fprintf(fw, "%s(%d); /* FUNC_RECURSIVE */\n", getWorkFunctionStr(pWorkOp->workFunc.type), pWorkOp->workFunc.workload);
						} else if (pWorkOp->workFunc.type == FUNC_TID) {
                                                        fprintf(fw, "%s; /* FUNC_TID */\n", getWorkFunctionStr(pWorkOp->workFunc.type));
						}
					}

					if (type != WORK_OP_FUNC) {
						fprintf(fw, "%s}\n", tab);
					}

					if (type == WORK_OP_CACHE_MISS) {
					    fprintf(fw, "%sfree(wa_%d);\n", tab, tid);
					} 
					currIndex++;
				}
			}
		}
	}
	return currIndex;
}

int doTlsOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j;
	struct tMemOp *pMemOp;

	for (i = 0; i < td->lsExt.numOfMemOps; i++) {
		pMemOp = &td->lsExt.memOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pMemOp->time == j) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}

				if (pMemOp->type == MEM_TYPE_LOCAL) {
					if (pMemOp->opType == MEM_OP_READ) {
						localMemRead(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					} else if (pMemOp->opType == MEM_OP_WRITE) {
						localMemWrite(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					}
				} else if (pMemOp->type == MEM_TYPE_GLOBAL) {
					if (pMemOp->opType == MEM_OP_READ) {
						if (pMemOp->semIndex != -1) { /* lock the semaphore */
							posixSemLock(fw, td, pMemOp->semIndex, tab, true);
#ifdef PRINT_CTRL_FLOW_IN_SYN
							fprintf(fw, "\t\t\tprintf(\"Thread %d, lock sem %d\\n\");\n", td->tid, pMemOp->semIndex);
#endif
						}
						globMemRead(fw, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					} else if (pMemOp->opType == MEM_OP_WRITE) {
						globMemWrite(fw, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
						if (pMemOp->semIndex != -1) { /* unlock the semaphore */
							fprintf(fw, "%s\t\n", tab);
							posixSemUnlock(fw, td, pMemOp->semIndex, true, false, tab);
#ifdef PRINT_CTRL_FLOW_IN_SYN
							fprintf(fw, "\t\t\tprintf(\"Thread %d, unlock sem %d\\n\");\n", td->tid, pMemOp->semIndex);
#endif
						}
					}
				} else if (pMemOp->type == MEM_TYPE_SHARED) {
					if (pMemOp->opType == MEM_OP_READ) {
						shmMemRead(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					} else if (pMemOp->opType == MEM_OP_WRITE) {
						shmMemWrite(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					}
				}
				currIndex++;
			}
		}
	}

	return currIndex;
}

int doMrapiOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j;
	struct tMemOp *pMemOp;
	struct tSyncOp *pSyncOp;

	for (i = 0; i < td->mrapiExt.numOfMemOps; i++) {
		pMemOp = &td->mrapiExt.memOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pMemOp->time == j) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				if (pMemOp->opType == MEM_OP_READ) {
					if (pMemOp->semIndex != -1) { /* lock the semaphore */
						semLock(fw, td, pMemOp->semIndex, tab, true);
					}
					shmMemRead(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
				} else if (pMemOp->opType == MEM_OP_WRITE) {
					shmMemWrite(fw, td, pMemOp->resId, pMemOp->startIndex, pMemOp->numOfIterations, tab);
					if (pMemOp->semIndex != -1) { /* unlock the semaphore */
						fprintf(fw, "%s\t\n", tab);
						semUnlock(fw, td, pMemOp->semIndex, true, false, tab);
					}
				}
				currIndex++;
			}
		}
	}

	for (i = 0; i < td->mrapiExt.numOfSyncOps; i++) {
		pSyncOp = &td->mrapiExt.syncOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pSyncOp->time == j) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				if (pSyncOp->type == SYNC_TYPE_BAR) {
					barrierWait(fw, td, pSyncOp->resId, tab);
				} else if (pSyncOp->type == SYNC_TYPE_MTX) {
					mutexLock(fw, td, pSyncOp->resId, tab, false);
				} else if (pSyncOp->type == SYNC_TYPE_SEM) {
					semLock(fw, td, pSyncOp->resId, tab, false);
				}
				currIndex++;
			}
		}
	}

	return currIndex;
}

int doMcapiOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j;
	struct tMsgOp *pMsgOp;

	for (i = 0; i < td->mcapiExt.numOfMsgOps; i++) {
		pMsgOp = &td->mcapiExt.msgOps[i];
		for (j = minTime; j <= maxTime; j++) {
			if (pMsgOp->time == j) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				if (pMsgOp->type == MSG_TYPE_MSG) {
					if (pMsgOp->opType == MSG_OP_SEND) {
						msgSend(fw, td, pMsgOp->resId, pMsgOp->resId2, pMsgOp->numOfIterations, tab);
					} else if (pMsgOp->opType == MSG_OP_RECV) {
						msgRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					}
				} else if (pMsgOp->type == MSG_TYPE_PKT) {
					if (pMsgOp->opType == MSG_OP_SEND) {
						pktSend(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					} else if (pMsgOp->opType == MSG_OP_RECV) {
						pktRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					}
				} else if (pMsgOp->type == MSG_TYPE_SCL) {
					if (pMsgOp->opType == MSG_OP_SEND) {
						sclSend(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					} else if (pMsgOp->opType == MSG_OP_RECV) {
						sclRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					}
				}
				currIndex++;
			}
		}
	}

	return currIndex;
}

int doQuarkOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

    	int i, j;
        struct tMsgOp *pMsgOp;
	struct tMemOp *pMemOp;

	/* set local Data to Thread Data */	
	for (i = 0; i < td->lsExt.numOfMemOps; i++) {
                pMemOp = &td->lsExt.memOps[i];
                for (j = minTime; j <= maxTime; j++) {
                        if (pMemOp->time == j && td->workCheck.time == j) {
                                if (currIndex > 0) {
                                        fprintf(fw, "%s\t\n", tab);
                                }

                                if (pMemOp->type == MEM_TYPE_LOCAL) {
                                        if (pMemOp->opType == MEM_OP_READ) {
                                                
                                        } else if (pMemOp->opType == MEM_OP_WRITE) {
						if (hasWorkloadChecker) fprintf(fw, "%s\ttd->workload = ", tab);
						else fprintf(fw, "%s\t", tab);
                                              	fprintf(fw, "%s(td->%sAddr[%d] + %d, %d);\n\n",
                                                        workloadFnName, getThreadAddrType(td), 0, pMemOp->startIndex, td->mtapiExt.numOfWorks); 
                                        }
                                } else if (pMemOp->type == MEM_TYPE_GLOBAL) {
                                        if (pMemOp->opType == MEM_OP_READ) {
                                                
                                        } else if (pMemOp->opType == MEM_OP_WRITE) {
						if (hasWorkloadChecker) fprintf(fw, "%s\ttd->workload = ", tab);
                                                else fprintf(fw, "%s\t", tab);
                                		fprintf(fw, "%s(td->%sAddr[%d] + %d, %d);\n\n", 
							workloadFnName, getThreadAddrType(td), 0, pMemOp->startIndex, td->mtapiExt.numOfWorks);
                                        }
                                } else if (pMemOp->type == MEM_TYPE_SHARED) {
					if (pMemOp->opType == MEM_OP_READ) {

                                        } else if (pMemOp->opType == MEM_OP_WRITE) {
						if (hasWorkloadChecker) fprintf(fw, "%s\ttd->workload = ", tab);
                                                else fprintf(fw, "%s\t", tab);
						fprintf(fw, "%s(td->%sAddr[%d] + %d, %d);\n\n",
                                                        workloadFnName, getThreadAddrType(td), 0, pMemOp->startIndex, td->mtapiExt.numOfWorks);	
                                        }
				}
                        }
                }
	}

	/* do send and receive operations */
        for (i = 0; i < td->bareMetalExt.numOfMsgOps; i++) {
                pMsgOp = &td->bareMetalExt.msgOps[i];
                for (j = minTime; j <= maxTime; j++) {
                        if (pMsgOp->time == j) {
                                if (currIndex > 0) {
                                        fprintf(fw, "%s\t\n", tab);
                                }
                                if (pMsgOp->type == MSG_TYPE_MSG) {
                                        if (pMsgOp->opType == MSG_OP_SEND) {
                                                msgSend(fw, td, pMsgOp->resId, pMsgOp->resId2, pMsgOp->numOfIterations, tab);
                                        } else if (pMsgOp->opType == MSG_OP_RECV) {
                                                msgRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
					}
                                } else if (pMsgOp->type == MSG_TYPE_PKT) {
                                        if (pMsgOp->opType == MSG_OP_SEND) {
                                                pktSend(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
                                        } else if (pMsgOp->opType == MSG_OP_RECV) {
                                                pktRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
                                        }
                                } else if (pMsgOp->type == MSG_TYPE_SCL) {
                                        if (pMsgOp->opType == MSG_OP_SEND) {
                                                sclSend(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
                                        } else if (pMsgOp->opType == MSG_OP_RECV) {
                                                sclRecv(fw, td, pMsgOp->resId, pMsgOp->numOfIterations, tab);
                                        }
                                }
                                currIndex++;
                        }
                }
        }
        return currIndex;  
}

int doWorkCheckOps (FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {

	int i, j;
        struct tWorkCheck *pWorkCheckOp;
	unsigned *expectedWorkload;
	unsigned workloadSize = 0;

	for (i = minTime; i <= maxTime; i++) {
		pWorkCheckOp = &td->workCheck;
		if (pWorkCheckOp->time == i) {
			if (pWorkCheckOp->isFinalChecker) {
				assignWorkload(fw, td, tab);
				workloadSize = calcWorkload(td, &expectedWorkload);
				fprintf(fw, "\n%s/* Check Final Results */\n", tab);
				for (j = 0; j < workloadSize; j++) {	
					fprintf(fw, "%sif (td->workload == %d) {\n", tab, expectedWorkload[j]);
                                	fprintf(fw, "%s\treturn 0xDEADBEEF;\n", tab);
					fprintf(fw, "%s} else ", tab);	
				}
        		        fprintf(fw, "{\n");
        		        fprintf(fw, "%s\treturn 0x0DEFACED;\n", tab);
        		        fprintf(fw, "%s}\n", tab);		
			}

			currIndex++; 
		}
	}

	return currIndex;
}

int doOps(FILE *fw, struct tThreadData *td, int minTime, int maxTime, char *tab, int currIndex) {
	if (td->bareMetalExt.hasExt) {
                currIndex = doQuarkOps(fw, td, minTime, maxTime, tab, currIndex);
        }
	if (td->lsExt.hasExt) {
		currIndex = doTlsOps(fw, td, minTime, maxTime, tab, currIndex);
	}

	if (td->mrapiExt.hasExt) {
		currIndex = doMrapiOps(fw, td, minTime, maxTime, tab, currIndex);
	}

	if (td->mcapiExt.hasExt) {
		currIndex = doMcapiOps(fw, td, minTime, maxTime, tab, currIndex);
	}
	currIndex = doMtapiOps(fw, td, minTime, maxTime, tab, currIndex);

	currIndex = doWorkCheckOps(fw, td, minTime, maxTime, tab, currIndex);

	return currIndex;
}

/***************************************************deletion******************************************************/

int deleteTls(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (!isTlsVarArray) {
		if (activeTd->lsExt.hasExt) {
			for (i = 0; i < activeTd->lsExt.numOfLocalMems; i++) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* free the local memory */\n", tab);
				fprintf(fw, "%s\tfree(localAddr%d);\n", tab, i + 1);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int releaseMrapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (activeTd->mrapiExt.hasExt) {
		for (i = 0; i < activeTd->mrapiExt.numOfSemaphores; i++) {
			if (!activeTd->mrapiExt.sems[i].isOwner) {
				if (activeTd->mrapiExt.sems[i].isLocked) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					fprintf(fw, "%s\t/* unlock the semaphore so the owner can lock for deletion */\n", tab);
					fprintf(fw, "%s\tmrapi_sem_unlock(sem%d, &mrapiStatus);\n", tab, i + 1);
					fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
					currIndex++;
				}
			}
		}
		for (i = 0; i < activeTd->mrapiExt.numOfMutexes; i++) {
			if (!activeTd->mrapiExt.mutexes[i].isOwner) {
				if (activeTd->mrapiExt.mutexes[i].isLocked) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					fprintf(fw, "%s\t/* unlock the mutex so the owner can lock for deletion */\n", tab);
					fprintf(fw, "%s\tmrapi_mutex_unlock(mutex%d, &lockKey, &mrapiStatus);\n", tab, i + 1);
					fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
					currIndex++;
				}
			}
		}
	}

	return currIndex;
}

int deleteMrapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (activeTd->mrapiExt.hasExt) {
		for (i = 0; i < activeTd->mrapiExt.numOfSemaphores; i++) {
			if (activeTd->mrapiExt.sems[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* lock the semaphore */\n", tab);
				fprintf(fw, "%s\tmrapi_sem_lock(sem%d, MRAPI_TIMEOUT_INFINITE /*timeout*/, &mrapiStatus);\n", tab, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				fprintf(fw, "%s\t\n", tab);
				fprintf(fw, "%s\t/* delete the semaphore */\n", tab);
				fprintf(fw, "%s\tmrapi_sem_delete(sem%d, &mrapiStatus);\n", tab, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		for (i = 0; i < activeTd->mrapiExt.numOfMutexes; i++) {
			if (activeTd->mrapiExt.mutexes[i].isOwner) {
				if (!activeTd->mrapiExt.mutexes[i].isLocked) {
					if (currIndex > 0) {
						fprintf(fw, "%s\t\n", tab);
					}
					fprintf(fw, "%s\t/* lock the mutex */\n", tab);
					fprintf(fw, "%s\tmrapi_mutex_lock(mutex%d, &lockKey, MRAPI_TIMEOUT_INFINITE /*timeout*/, &mrapiStatus);\n", tab, i + 1);
					fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* delete the mutex */\n", tab);
				fprintf(fw, "%s\tmrapi_mutex_delete(mutex%d, &mrapiStatus);\n", tab, i + 1);
				fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
		for (i = 0; i < activeTd->mrapiExt.numOfSharedMems; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			fprintf(fw, "%s\t/* detach from the shared memory */\n", tab);
			fprintf(fw, "%s\tmrapi_shmem_detach(shmem%d, &mrapiStatus);\n", tab, i + 1);
			fprintf(fw, "%s\tif (mrapiStatus != MRAPI_SUCCESS) { MRAPI_WRONG }\n", tab);
			currIndex++;
		}
	}

	return currIndex;
}

void closeChan(FILE *fw, struct tThreadData *td, struct tMcapiChannel *pMcapiChan, int i, char *tab) {

	fprintf(fw, "%s\t/* close the endpoint handles */\n", tab);
	fprintf(fw, "%s\tdo {\n", tab);
	if (pMcapiChan->type == EP_PACKET) {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_pktchan_send_close_i(pktSendHandle%d%d /*send_handle*/, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1);
		} else {
			fprintf(fw, "%s\t\tmcapi_pktchan_recv_close_i(pktRecvHandle%d%d /*recv_handle*/, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1);
		}
	} else {
		if (td == pMcapiChan->pSenderTd) {
			fprintf(fw, "%s\t\tmcapi_sclchan_send_close_i(sclSendHandle%d%d /*send_handle*/, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1);
		} else {
			fprintf(fw, "%s\t\tmcapi_sclchan_recv_close_i(sclRecvHandle%d%d /*recv_handle*/, &mcapiRequest, &mcapiStatus);\n",
					tab, td->id, i + 1);
		}
	}
	fprintf(fw, "%s\t//retry if all request handles are in-use\n", tab);
	fprintf(fw, "%s\t} while (mcapiStatus == MCAPI_ERR_REQUEST_LIMIT);\n", tab);
	fprintf(fw, "%s\tmcapi_wait(&mcapiRequest, &size, MCA_INFINITE, &mcapiStatus);\n", tab);
	fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);

	return;
}

int deleteMcapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;
	struct tMcapiChannel *pMcapiChan;

	if (activeTd->mcapiExt.hasExt) {
		for (i = 0; i < activeTd->mcapiExt.numOfPcktChans; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			pMcapiChan = activeTd->mcapiExt.pcktChans[i];
			closeChan(fw, activeTd, pMcapiChan, i, tab);
			currIndex++;
		}
		for (i = 0; i < activeTd->mcapiExt.numOfScalChans; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			pMcapiChan = activeTd->mcapiExt.scalChans[i];
			closeChan(fw, activeTd, pMcapiChan, i, tab);
			currIndex++;
		}
		for (i = 0; i < activeTd->mcapiExt.numOfEndpoints; i++) {
			if (activeTd->mcapiExt.endpoints[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* delete the endpoint we've created */\n", tab);
				fprintf(fw, "%s\tmcapi_endpoint_delete(ep%d, &mcapiStatus);\n", tab, i + 1);
				fprintf(fw, "%s\tif (mcapiStatus != MCAPI_SUCCESS) { MCAPI_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int deleteQuark(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {
    return 0;
}

int deletePosix(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	int i;

	if (!activeTd->mrapiExt.hasExt && !activeTd->mcapiExt.hasExt) {
		for (i = 0; i < activeTd->posixExt.numOfSemaphores; i++) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
//FIXME: when the owner closes the sem, it is removed. Is this correct behavior of POSIX standard?
//			if (activeTd->posixExt.sems[i].isOwner) {
//				fprintf(fw, "%s\t/* unlink the semaphore */\n", tab);
//				fprintf(fw, "%s\tretVal = sem_unlink(\"sem%x\");\n", tab, activeTd->posixExt.sems[i].key);
//				fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
//				fprintf(fw, "%s\t\n", tab);
//			}
			fprintf(fw, "%s\t/* remove reference of the semaphore */\n", tab);
			fprintf(fw, "%s\tretVal = sem_close(sem%d);\n", tab, i + 1);
			fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
			currIndex++;
		}
		for (i = 0; i < activeTd->posixExt.numOfMutexes; i++) {
			if (activeTd->posixExt.mutexes[i].isOwner) {
				if (currIndex > 0) {
					fprintf(fw, "%s\t\n", tab);
				}
				fprintf(fw, "%s\t/* destroy the mutex */\n", tab);
				fprintf(fw, "%s\tretVal = pthread_mutex_destroy(mutex%d);\n", tab, i + 1);
				fprintf(fw, "%s\tif (retVal != 0) { POSIX_WRONG }\n", tab);
				currIndex++;
			}
		}
	}

	return currIndex;
}

int releaseMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	currIndex = releaseMrapi(fw, activeTd, tab, currIndex);

	return currIndex;
}

int deleteMxapi(FILE *fw, struct tThreadData *activeTd, char *tab, int currIndex) {

	currIndex = deleteTls(fw, activeTd, tab, currIndex);
	currIndex = deleteMrapi(fw, activeTd, tab, currIndex);
	currIndex = deleteMcapi(fw, activeTd, tab, currIndex);
	currIndex = deleteQuark(fw, activeTd, tab, currIndex);
	currIndex = deletePosix(fw, activeTd, tab, currIndex);

	return currIndex;
}

/***************************************************finalize******************************************************/

void finalizeMxApi(FILE *fw, struct tThreadData *activeTd) {

	int t, numOfThreads;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	boolean hasMrapiExt = false, hasMcapiExt = false;

	numOfThreads = activeProc->numOfThreads;
	for (t = activeTd->id; t < numOfThreads; t++) {
		td = activeProc->threads[t];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			if (td->mrapiExt.hasExt) {
				hasMrapiExt = true;
			}
			if (td->mcapiExt.hasExt) {
				hasMcapiExt = true;
			}
			if (hasMrapiExt && hasMrapiExt) {
				break;
			}
		}
	}

	if (hasMrapiExt && hasMcapiExt) {
		fprintf(fw, "\t\n");
		fprintf(fw, "\t/*\n");
		fprintf(fw, "\t* mrapi_finalize(&mrapiStatus); // mcapi_finalize does\n");
		fprintf(fw, "\t* MRAPI_CHECK\n");
		fprintf(fw, "\t* */\n");
	}

	if (hasMcapiExt) {
		fprintf(fw, "\t\n");
		fprintf(fw, "\tmcapi_finalize(&mcapiStatus);\n");
		fprintf(fw, "\tMCAPI_CHECK\n");
		fprintf(fw, "\t\n");
	}

	if (hasMrapiExt && !hasMcapiExt) {
		fprintf(fw, "\t\n");
		fprintf(fw, "\tmrapi_finalize(&mrapiStatus);\n");
		fprintf(fw, "\tMRAPI_CHECK\n");
		fprintf(fw, "\t\n");
	}

	return;
}

/***************************************************thread operations******************************************************/

void getTab(int tabCount, char *tab) {

	int i;

	for (i = 0; i < tabCount && i < MAX_INDENT - 1; i++) {
		tab[i] = '\t';
	}
	tab[tabCount] = '\0';

	return;
}

void incTab(int incCount, char *tab) {

        int i, j;
	i = 0;

	while (tab[i] != '\0') {
		i++;
	}
	for (j = i; j < incCount + i && j < MAX_INDENT - 1; j++) {
		tab[j] = '\t';	
	}

        tab[j] = '\0';

        return;
}

void decTab(int decCount, char *tab) {

        int i, j;
        i = 0;

        while (tab[i] != '\0') {
                i++;
        }
        for (j = i - 1; j >= i - decCount && j >= 0; j--) {
                tab[j] = '\0';
        }

        return;
}

void sendGlobalAddrPtr(FILE *fw, struct tThreadData *launchTd, struct tThreadData *depTd, char *tab) {
	int i;
	struct tThreadData* itTd;
	int numOfThreads = activeProc->numOfThreads;

	if (launchTd->tid == depTd->tid) {
        	fprintf(fw, "%s\tcase %d:\n", tab, launchTd->tid);
		for (i = MAIN_ID + 1; i < numOfThreads + MAIN_ID; i++) { /* except main */
                        itTd = activeProc->threads[i];
                        if (depTd->dependency[i] && threadHasSharedAdressType(itTd)) {
                                fprintf(fw, "%s\t\tsendmsg(%d / get_num_contexts(), "
                                "(%d+1) %% get_num_contexts(), "
                             	"(unsigned) td->%sAddr + %d * sizeof(unsigned));\n",
                               	tab, itTd->tid, itTd->tid, getThreadAddrType(itTd), itTd->standAloneExt.offset);
                        }
                }
		fprintf(fw, "%s\t\tbreak;\n", tab);	
	}
	return;
}

void launchThread(FILE *fw, struct tThreadData *launchTd, struct tThreadData *depTd, int taskId, char *tab, boolean *threadDone) {
	int i;
	struct tThreadData* itTd;
	int numOfThreads = activeProc->numOfThreads;

	if(threadDone[depTd->id]) {
		return;
	} else {
		threadDone[depTd->id] = true;
	}

	for (i = MAIN_ID; i < numOfThreads + MAIN_ID; i++) { /* include main */
        	if (depTd->dependency[i] && depTd->id != i) {
			itTd = activeProc->threads[i];
			if(threadDone[itTd->id]) {
				continue;
			}	
			fprintf(fw, "%s\tcase %d:\n", tab, itTd->tid);

			if (itTd->creator == launchTd->id) {
				taskId = itTd->mtapiExt.startRoutine.taskId;
			}
               	        if (launchTd->tid == MAIN_ID) {
               	                fprintf(fw, "%s\t\ttask%d((void*) &tData);\n",
               	                tab, taskId);
               	        } else {
               	                fprintf(fw, "%s\t\ttask%d((void*) td);\n",
               	                tab, taskId);
               	        }
			fprintf(fw, "%s\t\tbreak;\n", tab);
			launchThread(fw, launchTd, itTd, taskId, tab, threadDone);
		}
	}
	return;
}
     
void doSwitchStatementOps(FILE *fw, struct tThreadData *launchTd, char *tab) {

	boolean threadDone[MAX_THREADS];
        memset (threadDone, false, MAX_THREADS * sizeof(boolean));
       	fprintf(fw, "%s\tswitch (td->tid) {\n", tab);
	sendGlobalAddrPtr(fw, launchTd, launchTd, tab);
	launchThread(fw, launchTd, launchTd, MAIN_ID, tab, threadDone);
	fprintf(fw, "%s\t}\n\n", tab);

	return;
}
   
int getMaxGroupId(struct tThreadData *td) {

	int i, numOfThreads;
	int maxGrpId = 0;
	struct tThreadData *itTd;

	if (!isStandalone) {
		maxGrpId = maxGroupId;
	} else {
		numOfThreads = activeProc->numOfThreads;
		for (i = MAIN_ID + 1; i < numOfThreads + MAIN_ID; i++) { /* except main */
			itTd = activeProc->threads[i];
			if (itTd->creator == td->id) {
				if (itTd->standAloneExt.phase > maxGrpId) {
					maxGrpId = itTd->standAloneExt.phase;
				}
			}
		}
	}

	return maxGrpId;

}

int getCreateJoin(FILE *fw, struct tThreadData *activeTd, int numOfThreads, char *tab, int currIndex) {

	int i, j, k, p, t;
	struct tThreadData *td, *tdIt;
	int numTasksInGroup;
	boolean isNextGroup, isNextThread;
	int createdPCs[MAX_THREADS];
	int numOfCreatedPCs = 0;
	int childTdIdx = 0;

	//static const int THREADS_PER_CORE = 4;	/* added by aford */

	maxGroupId = getMaxGroupId(activeTd);

	for (p = 0, t = 0, isNextGroup = false; p < maxGroupId; p++, isNextGroup = false) {

		if (p == 0) {
			currIndex = createMxapi(fw, activeTd, tab, currIndex);
		}

		currIndex = doOps(fw, activeTd, p * MAX_WORK_PER_PHASE, p * MAX_WORK_PER_PHASE, tab, currIndex); /* initial */

		/* clear information of the previous phase */
		for (k = 0; k < numOfCreatedPCs; k++) {
			createdPCs[k] = 0;
		}
		numOfCreatedPCs = 0;

		if (currIndex > 0) {
			fprintf(fw, "%s\t\n", tab);
		}

		fprintf(fw, "%s/* Create and run all the threads of phase %d */\n", tab, p + 1);
		for (i = MAIN_ID; i < numOfThreads + MAIN_ID; i++) { /* include main */
			td = activeProc->threads[i];
			if (td->creator == activeTd->id && td->groupId == p) {
				for (k = 0, isNextThread = false; k < numOfCreatedPCs; k++) {
					if (td->pc == createdPCs[k]) {
						isNextThread = true;
						break;
					}
				}
				if (isNextThread) {
					continue;
				}
				for (j = i + 1, numTasksInGroup = 1; j < numOfThreads + MAIN_ID; j++) {
					tdIt = activeProc->threads[j];
					if ((tdIt->creator == activeTd->id && tdIt->groupId == p) && tdIt->mtapiExt.startRoutine.taskId == td->mtapiExt.startRoutine.taskId) {
						numTasksInGroup++;
					}
				}
				if (numTasksInGroup > 1) {
					if (!activeTd->bareMetalExt.hasExt) {
						createdPCs[numOfCreatedPCs++] = td->pc;
						fprintf(fw, "%s\tfor(n = 0, t = 0; n < %d; n++, t++) {\n", tab, numTasksInGroup);
	                        childTdIdx = createThreads(fw, td, activeProc->hwType, activeTd->workCheck.workAck, 
							td->mtapiExt.startRoutine.taskId, childTdIdx, tab);
	                        fprintf(fw, "%s\t}\n\n", tab);	
					} else {
                        createdPCs[numOfCreatedPCs++] = td->pc;
                        for(j = MAIN_ID + 1; j < numOfThreads + MAIN_ID; j++) {
                            tdIt = activeProc->threads[j];
                            if ((tdIt->creator == activeTd->id && tdIt->groupId == p) && tdIt->mtapiExt.startRoutine.taskId == td->mtapiExt.startRoutine.taskId) { 
						        childTdIdx = createThreads(fw, tdIt, activeProc->hwType, activeTd->workCheck.workAck,
                                tdIt->mtapiExt.startRoutine.taskId, childTdIdx, tab);
                            }
                        }
					}
					if (numTasksInGroup == activeTd->mtapiExt.numOfChildren[p]) {
						isNextGroup = true;
						currIndex++;
						break;
					}
				} else {
					if (currIndex > 0) {
						childTdIdx = createThreads(fw, td, activeProc->hwType, activeTd->workCheck.workAck,
                                                        td->mtapiExt.startRoutine.taskId, childTdIdx, tab);	
					} else {
						childTdIdx = createThreads(fw, td, activeProc->hwType, activeTd->workCheck.workAck,
                                                        td->mtapiExt.startRoutine.taskId, childTdIdx, tab);	
					}
					t++;
				}
			}
			if (isNextGroup) {
				break;
			}
			currIndex++;
		}

		if (p == 0) {
			currIndex = getMxapi(fw, activeTd, tab, currIndex);
		}

		if (activeTd->mtapiExt.loopCount > 0) {
			if (currIndex > 0) {
				fprintf(fw, "%s\t\n", tab);
			}
			fprintf(fw, "%s\tfor (loopCount = 0; loopCount < %d; loopCount++) {\n", tab, activeTd->mtapiExt.loopCount);
			incTab(1, tab);
		}
		
		currIndex = doOps(fw, activeTd, (p * MAX_WORK_PER_PHASE) + 1, (p * MAX_WORK_PER_PHASE) + 1, tab, currIndex); /* internal - init */
		currIndex = doOps(fw, activeTd, (p * MAX_WORK_PER_PHASE) + 2, (p * MAX_WORK_PER_PHASE) + 2, tab, currIndex); /* internal - final */

		if (activeTd->mtapiExt.loopCount > 0) {
			fprintf(fw, "%s\t\n", tab);
			decTab(1, tab);
			fprintf(fw, "%s\t}\n", tab);
		}

		fprintf(fw, "%s\t\n", tab);

		/* join */
		fprintf(fw, "%s\t/* Wait for all threads of phase %d*/\n", tab, p + 1);
		joinThreads(fw, activeTd, activeProc->hwType, activeTd->workCheck.workAck, 
			    activeTd->mtapiExt.numOfChildren[p], tab);	

	        currIndex = doOps(fw, activeTd, (p * MAX_WORK_PER_PHASE) + 3, (p * MAX_WORK_PER_PHASE) + 3, tab, currIndex); /* final */
		currIndex = doOps(fw, activeTd, (p * MAX_WORK_PER_PHASE) + 4, (p * MAX_WORK_PER_PHASE) + 4, tab, currIndex); /* workChecker */
	}

	return currIndex;
}

int getMaxChildren(struct tThreadData *activeTd) {

	int i;
	int maxChildren = 0;

	maxGroupId = getMaxGroupId(activeTd);

	for (i = 0; i < maxGroupId; i++) {
		if (activeTd->mtapiExt.numOfChildren[i] > maxChildren) {
			maxChildren = activeTd->mtapiExt.numOfChildren[i];
		}
	}

	return maxChildren;
}

int getTotalChildren(struct tThreadData *activeTd) {

	int i;
	int totalChildren = 0;

	maxGroupId = getMaxGroupId(activeTd);

	for (i = 0; i < maxGroupId; i++) {
		totalChildren += activeTd->mtapiExt.numOfChildren[i];
	}

	return totalChildren;
}

void prepMemPointers(FILE *fw, struct tThreadData *td) {

	int i = 0, j;
	char tab = '\0';

	if (td->bareMetalExt.hasExt) {
		for (i = 0; i < td->bareMetalExt.numOfMsgOps; i++) {
			if (td->bareMetalExt.msgOps[i].opType == MSG_OP_RECV) {
					/* assign memory for each WRITE_MEM Operation */
					for (j = 0; j < td->lsExt.numOfMemOps; j++) {
						if (td->lsExt.memOps[j].opType == MEM_OP_WRITE &&
						    td->lsExt.memOps[j].type == MEM_TYPE_SHARED) {
							break;
						}
					}	
					if (j < td->lsExt.numOfMemOps) {
						/* assign memory for first WRITE_SHARED_MEM Operation */
						fprintf(fw, "\n\t/* Initialize Quark Msg Passing Variables */\n");
						if (td->bareMetalExt.msgOps[i].numOfIterations > 1) {
	                                                fprintf(fw, "\tint i;\n");
	                                        }	
						if (td->tid == MAIN_ID) {
							fprintf(fw, "\n\tif (td->tid == 0) {\n");
							fprintf(fw, "\t\ttd->%sAddr = ", getThreadAddrType(td));
                                                	fprintf(fw, "malloc(%d * sizeof(unsigned));\n", td->lsExt.memOps[j].numOfIterations);
							fprintf(fw, "\t\tmemset (td->%sAddr, 0, %d * sizeof(unsigned));\n",
                                                	        getThreadAddrType(td), td->lsExt.memOps[j].numOfIterations);
							incTab(1, &tab);
							doOps(fw, td, -1, -1, &tab, 0); /* prep mem pointers */
							decTab(1, &tab);
							fprintf(fw, "\t} else {\n");
                                               		fprintf(fw, "\t\ttd->%sAddr = (int *) recvmsg(td->addr, (td->tid+1) %% get_num_contexts());\n",
                                                		getThreadAddrType(td));
                                               		fprintf(fw, "\t}\n");
                                               		fprintf(fw, "\n");
						}
						break; 	
					}
			}
		}
	}
}

void genTask(FILE *fw, struct tThreadData *activeTd, int numOfThreads, char* tab) {

	int maxChildren, totalChildren;
	int i, t, currThread, threadCountInTask = activeTd->mtapiExt.startRoutine.threadCount;
	int taskId = activeTd->mtapiExt.startRoutine.taskId;
	struct tThreadData *td;
	int currIndex = 0;

	maxChildren = getMaxChildren(activeTd);
	totalChildren = getTotalChildren(activeTd);
	
	/* variables */
	fprintf(fw, "%s/* initialize variables */\n", tab);
	if (totalChildren > 0) {
		fprintf(fw, "%sthreadData tData[%d];\n", tab, totalChildren);
	}

	if (maxChildren > 0) {
		if(!activeTd->bareMetalExt.hasExt) {
		    fprintf(fw, "%spthread_t threads[%d];\n", tab, maxChildren);
		}
		fprintf(fw, "%sint rc, t, n;\n", tab);
	}

	if (activeTd->id > 0) {
		fprintf(fw, "%sthreadData* td = (threadData*) param;\n", tab);
		
	}

	genMxapiPars(fw, activeTd, tab);

	/* functions */
	if (totalChildren > 0) {
		for (i = MAIN_ID + 1, t = 0; i < numOfThreads; i++) { /* except main */
			if (activeTd->bareMetalExt.hasExt && activeTd->tid == MAIN_ID) {
				fprintf(fw, "%std->tid = get_num_contexts() * get_core_id() + get_thread_id();\n", tab);
				break;
			} else if (activeProc->threads[i]->creator == activeTd->id && !activeTd->bareMetalExt.hasExt) { 
				fprintf(fw, "%std->tid = %d;\n", tab, activeProc->threads[i]->tid);
				t++;
			}	
		}
		fprintf(fw, "%s\n", tab);
	} else if (activeTd->bareMetalExt.hasExt) {
        fprintf(fw, "%std->tid = get_num_contexts() * get_core_id() + get_thread_id();\n", tab);
    }

	initMxapi(fw, activeTd, tab);

	prepMemPointers(fw, activeTd);

	for (t = activeTd->id, currThread = 0; t < numOfThreads + MAIN_ID; t++, currIndex = 0) {
		td = activeProc->threads[t];
		if (td->mtapiExt.startRoutine.taskId == taskId) {
			currThread++;
			if (!activeProc->mtapiExt.hasUniqueThreads[td->mtapiExt.startRoutine.taskId] && threadCountInTask > 1) {
				fprintf(fw, "%sif (td->tid == %d) {\n", tab, td->tid);

	#ifdef PRINT_CTRL_FLOW_IN_SYN
				fprintf(fw, "%s\tprintf(\"Thread %d started\\n\");\n", tab, td->tid);
	#endif
				incTab(1, tab);
			}

			if (totalChildren == 0) {
				currIndex = createAndGetMxapi(fw, td, tab, currIndex);
				currIndex = doOps(fw, td, 0, 0, tab, currIndex); /* initial */
				if (td->mtapiExt.loopCount > 0) {
					if (currIndex > 0) {
						fprintf(fw, "%s\n", tab);
					}
					fprintf(fw, "%sfor (loopCount = 0; loopCount < %d; loopCount++) {\n", tab, td->mtapiExt.loopCount);
					incTab(1, tab);
					currIndex++;
				}
				currIndex = doOps(fw, td, 1, 1, tab, currIndex); /* internal - init */
				currIndex = doOps(fw, td, 2, 2, tab, currIndex); /* internal - final */
				if (td->mtapiExt.loopCount > 0) {
					fprintf(fw, "%s\n", tab);
					decTab(1, tab);
					fprintf(fw, "%s}\n", tab);
				}
				currIndex = doOps(fw, td, 3, 3, tab, currIndex); /* final */
                        	/* Send task information */
			} else {
				/* create child threads if any exists */
				currIndex = getCreateJoin(fw, td, numOfThreads, tab, currIndex);
			}

			currIndex = releaseMxapi(fw, td, tab, currIndex);

			currIndex = deleteMxapi(fw, td, tab, currIndex);

#ifdef PRINT_CTRL_FLOW_IN_SYN
			fprintf(fw, "%sprintf(\"Thread %d exited\\n\");\n", tab, td->tid);
#endif


			if (!activeProc->mtapiExt.hasUniqueThreads[td->mtapiExt.startRoutine.taskId]) {
				if (threadCountInTask > 1) {
					fprintf(fw, "%s}\n", tab);
				}
				if (currThread < threadCountInTask) {
					fprintf(fw, "%s\n", tab);
				}
			} else {
				break; /* generating the code for the first process is enough */
			}
		}
	}

	finalizeMxApi(fw, activeTd);

	return;
}

void genDepTdArr(int* depTd, int dependency[MAX_THREADS]) {

}

void genMainThread(FILE *fw, struct tThreadData *mainTd, int numOfThreads) {

	char tab[MAX_INDENT];
	getTab(0, tab);	

	fprintf(fw, "%sint main(int argc, char **argv) {\n", tab);
	incTab(1, tab);
	/* variables */
        if ((mainTd->mrapiExt.hasExt || mainTd->mcapiExt.hasExt) || mainTd->lsExt.hasExt || mainTd->bareMetalExt.hasExt) {	
		fprintf(fw, "%sthreadData maintData;\n", tab);
		fprintf(fw, "%sthreadData *td = &maintData;\n", tab);
	}

	initThreads(fw, mainTd, activeProc->hwType, mainTd->workCheck.workAck, numOfThreads, tab);

	/* functions */
	genTask(fw, mainTd, numOfThreads, tab);

	decTab(1, tab);
	finalizeThreads(fw, activeProc->hwType, tab);

	if (!mainTd->mrapiExt.hasExt && !mainTd->mcapiExt.hasExt && !mainTd->bareMetalExt.hasExt) {
		fprintf(fw, "\t\n");
	}

	fprintf(fw, "%s}\n", tab);

	return;
}

void genThread(FILE *fw, struct tThreadData *td, int numOfThreads) {
	int taskId = td->mtapiExt.startRoutine.taskId;
        char tab[MAX_INDENT];
        getTab(1, tab);

	if (taskId < MAX_THREADS && activeProc->mtapiExt.isStartRoutine[taskId] != SR_CREATED) {
		fprintf(fw, "void *task%d(void *param) {\n", taskId);
		genTask(fw, td, numOfThreads, tab);

		if (!td->mrapiExt.hasExt && !td->mcapiExt.hasExt) {
			fprintf(fw, "\t\n");
		}

		fprintf(fw, "\treturn NULL;\n");
		fprintf(fw, "}\n");
		fprintf(fw, "\n");

		activeProc->mtapiExt.isStartRoutine[taskId] = SR_CREATED; /* this start routine is create */
	}

	return;
}

void genMergeSortCheckerFunction(FILE *fw) {

	// Function Comments
	fprintf (fw,"/**\n * Merge()\n * Merge two sorted arrays, A with a  integers and\n * B with b integers, into a sorted array C.\n **/\n");
	fprintf (fw, "void Merge (int *A, int a, int *B, int b, int *C) {\n");
	fprintf (fw, "\tint i, j, k;\n");
	fprintf (fw, "\ti = 0;\n");
	fprintf (fw, "\tj = 0;\n");
	fprintf (fw, "\tk = 0;\n");	
        fprintf (fw, "\twhile (i < a && j < b) {\n");
        fprintf (fw, "\t\tif (A[i] <= B[j]) {\n");
        fprintf (fw, "\t\t\t/* copy A[i] to C[k] and move the pointer i and k forward */\n");
        fprintf (fw, "\t\t\tC[k] = A[i];\n");
        fprintf (fw, "\t\t\ti++;\n");
        fprintf (fw, "\t\t\tk++;\n");
        fprintf (fw, "\t\t} else {\n");
        fprintf (fw, "\t\t\t/* copy B[j] to C[k] and move the pointer j and k forward */\n");
        fprintf (fw, "\t\t\tC[k] = B[j];\n");
        fprintf (fw, "\t\t\tj++;\n");
        fprintf (fw, "\t\t\tk++;\n");
        fprintf (fw, "\t\t}\n");
	fprintf (fw, "\t}\n");
	fprintf (fw, "\t/* move the remaining elements in A into C */\n");
	fprintf (fw, "\twhile (i < a) {\n");
	fprintf (fw, "\t\tC[k]= A[i];\n");
	fprintf (fw, "\t\ti++;\n");
	fprintf (fw, "\t\tk++;\n");
	fprintf (fw, "\t}\n");
	fprintf (fw, "\t/* move the remaining elements in B into C */\n");
        fprintf (fw, "\twhile (j < b)  {\n");
        fprintf (fw, "\t\tC[k]= B[j];\n");
        fprintf (fw, "\t\tj++;\n");
        fprintf (fw, "\t\tk++;\n");
        fprintf (fw, "\t}\n");
	fprintf(fw, "}\n\n");

	fprintf(fw, "/**\n * MergeSort()\n * Sort array A with n integers, using merge-sort algorithm.\n**/\n");
        fprintf(fw, "int* MergeSort(int *A, int n) {\n");
        fprintf(fw, "\tint i, *A1, *A2, n1, n2;\n");
        fprintf(fw, "\tif (n < 2)\n");
        fprintf(fw, "\t\treturn A;   /* the array is sorted when n=1.*/\n\n");
        fprintf(fw, "\t/* divide A into two array A1 and A2 */\n");
        fprintf(fw, "\tn1 = n / 2;   /* the number of elements in A1 */\n");
        fprintf(fw, "\tn2 = n - n1;  /* the number of elements in A2 */\n");
        fprintf(fw, "\tA1 = (int*)malloc(sizeof(int) * n1);\n");
        fprintf(fw, "\tA2 = (int*)malloc(sizeof(int) * n2);\n\n");
        fprintf(fw, "\t/* move the first n/2 elements to A1 */\n");
        fprintf(fw, "\tfor (i =0; i < n1; i++) {\n");
        fprintf(fw, "\t\tA1[i] = A[i];\n");
        fprintf(fw, "\t}\n");
        fprintf(fw, "\t/* move the rest to A2 */\n");
        fprintf(fw, "\tfor (i = 0; i < n2; i++) {\n");
        fprintf(fw, "\t\tA2[i] = A[i+n1];\n");
        fprintf(fw, "\t}\n");
        fprintf(fw, "\t/* recursive call */\n");
        fprintf(fw, "\tMergeSort(A1, n1);\n");
        fprintf(fw, "\tMergeSort(A2, n2);\n\n");
        fprintf(fw, "\t/* conquer */\n");
        fprintf(fw, "\tMerge(A1, n1, A2, n2, A);\n");
        fprintf(fw, "\tfree(A1);\n");
        fprintf(fw, "\tfree(A2);\n");
        fprintf(fw, "\treturn A;\n");
        fprintf(fw, "}\n\n");
}

void genBinarySearchCheckerFunction(FILE *fw) {
  /**
   * Search()
   * Search array 'a' of size with N integers, using binary-search algorithm.
   */
    fprintf(fw, "int Search(int *a, int N, int q) {\n");
    fprintf(fw, "\tif (N==0) {\n");
    fprintf(fw, "\t\treturn 0;\n");
    fprintf(fw, "\t} else if (a[N/2] == q) {\n");
    fprintf(fw, "\t\treturn 1;\n");
    fprintf(fw, "\t} else {\n");
    fprintf(fw, "\t\tint x,y;\n");
    fprintf(fw, "\t\tx = Search(a, N/2, q);\n");
    fprintf(fw, "\t\ty = Search(a + (N/2) + 1, N/2, q);\n");
    fprintf(fw, "\t\treturn (x | y);\n");
    fprintf(fw, "\t}\n");
    fprintf(fw, "\treturn 0;   // This case will never be hit\n");
    fprintf(fw, "}\n");

    return;
}

void genWorkloadCheckerFunction(FILE *fw) {

	const char *p_workld = 	"result_t";
	const char *tCnt = 	"numOfIterations";

	/* return variable declaration */
	const char *returnVal =  "total";
	const char *returnType = "unsigned"; 

	fprintf(fw, "%s %s(int *%s, int %s) {\n", returnType, workloadFnName, p_workld, tCnt);
	/* variable decalrations */
	fprintf(fw, "\t%s %s = 0;\n", returnType, returnVal);
	fprintf(fw, "\tint i;\n");
	/* method */
	fprintf(fw, "\n");
	fprintf(fw, "\tfor (i = 0; i < %s; i++) {\n", tCnt);
	fprintf(fw, "\t\t%s += %s[i];\n", returnVal, p_workld);
	fprintf(fw, "\t}\n");
	fprintf(fw, "\treturn %s;\n", returnVal);
	fprintf(fw, "}\n");
	fprintf(fw, "\n");
	
        return;
}

void genWorkFunctions(FILE *fw) {
	
	int i, j;
        int numOfThreads;
        struct tThreadData *td;
	struct tWorkOp *pWorkOp;
        workOpType type;

	//enum workFuncType fn;
	boolean funcIsPrinted[NumberOfTypes];

	/* initialize */
	for (i = 0; i < NumberOfTypes; i++) {
		funcIsPrinted[i] = false;
	}

        numOfThreads = activeProc->numOfThreads;

        for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
                td = activeProc->threads[i];
               	for (j = 0; j < td->mtapiExt.numOfWorks; j++) {
	                pWorkOp = &td->mtapiExt.workOps[j]; 
			type = pWorkOp->type;
			if (type == WORK_OP_FUNC) {
				if (!funcIsPrinted[pWorkOp->workFunc.type]) {
					printWorkFunction(fw, pWorkOp->workFunc.type);
					funcIsPrinted[pWorkOp->workFunc.type] = true;
				} else {
				} 
			} 
		}
        }
}

void genFunctions(FILE *fw) {
	struct tThreadData *td = activeProc->threads[MAIN_ID];

	if (td->standAloneExt.parPattern == PP_DaC) {
		genMergeSortCheckerFunction(fw);
    } else if (td->standAloneExt.parPattern == PP_RD) {
        genBinarySearchCheckerFunction(fw);    
	} else {
		genWorkloadCheckerFunction(fw);
	}
	genWorkFunctions(fw);

	return;
}

void genThreads(FILE *fw) {

	int i;
	int numOfThreads;
	struct tThreadData *td;

	numOfThreads = activeProc->numOfThreads;

	for (i = MAIN_ID + 1; i < numOfThreads + MAIN_ID; i++) { /* except main */
		td = activeProc->threads[i];
		genThread(fw, td, numOfThreads);
	}
	genMainThread(fw, activeProc->threads[0], numOfThreads);

	return;
}

/***************************************************main******************************************************/

int genBenchmark(int phaseCount, paralPattern pp[MAX_GROUP_NUMBER]) {

	FILE *fw;
	char *temp, name[MAX_PATH] = { '\0' };
	int i, j, k, numOfThreads;
	struct tThreadData *td;
	boolean hasMrapiExt = false, hasMcapiExt = false, hasQuarkExt = false;

	activeProc = procs[0];
	temp = getBaseName(activeProc->name);
	strcpy(name, temp);
	strcat(name, "_syn.c");

//	fw = stdout;
	fw = fopen(name, "w");
	if (fw == NULL) {
		printf("ERROR! Cannot generate %s.\n", name);
		return -1;
	}

	numOfThreads = activeProc->numOfThreads;
	for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
		td = activeProc->threads[i];
		if (td->mrapiExt.hasExt) {
			hasMrapiExt = true;
		}
		if (td->mcapiExt.hasExt) {
			hasMcapiExt = true;
		}
		if (td->bareMetalExt.hasExt) {
			/* Define Architecture for Bare Metal Implementation */
			if (hasProcQuarkArchExt) {
				hasQuarkExt = true;
			}	
		}
		if (hasMrapiExt && hasMrapiExt) {
			break;
		}
	}

	genHeader(fw, pp, hasMrapiExt, hasMcapiExt, hasQuarkExt);
	genDefines(fw, hasMrapiExt, hasMcapiExt);
	genStructures(fw, hasMrapiExt, hasMcapiExt, hasQuarkExt);
	genGlobals(fw, hasMrapiExt, hasMcapiExt, hasQuarkExt);
	genFuncSignatures(fw, hasMrapiExt, hasMcapiExt);
	genFunctions(fw);
	genThreads(fw);

	fclose(fw);

	/* Print Debug Data */
        for ( i = 0; i < procCount; i++ ) {
            activeProc = procs[i];
            for (j = MAIN_ID; j < activeProc->numOfThreads; j++) { /* include main */
                td = activeProc->threads[j];
                printf("\nPrinting Endpoint Data for Thread Id %d\n", td->tid);
                for (k = 0; k < td->bareMetalExt.numOfEndpoints; k++) {
                        printEpData(&td->bareMetalExt.endpoints[k]);
                }
            }
        }

	return 0;
}

int genBenchmarkSa(int phaseCount, paralPattern pp[MAX_GROUP_NUMBER]) {

	FILE *fw;
	char *temp, name[MAX_PATH] = { '\0' };
	int i, numOfThreads;
	struct tThreadData *td;
	boolean hasMrapiExt = false, hasMcapiExt = false, hasQuarkExt = false;

	activeProc = procs[0];
	temp = getBaseName(activeProc->name);
	strcpy(name, temp);
	strcat(name, "_syn.c");

//	fw = stdout;
	fw = fopen(name, "w");
	if (fw == NULL) {
		printf("ERROR! Cannot generate %s.\n", name);
		return -1;
	}

	numOfThreads = activeProc->numOfThreads;
	for (i = MAIN_ID; i < numOfThreads; i++) { /* include main */
		td = activeProc->threads[i];
		if (td->mrapiExt.hasExt) {
			hasMrapiExt = true;
		}
		if (td->mcapiExt.hasExt) {
			hasMcapiExt = true;
		}
		if (td->bareMetalExt.hasExt) {
			hasQuarkExt = true;
		}
		if (hasMrapiExt && hasMcapiExt) {
			break;
		}
	}
	genHeader(fw, pp, hasMrapiExt, hasMcapiExt, hasQuarkExt);
	genDefines(fw, hasMrapiExt, hasMcapiExt);
	genStructures(fw, hasMrapiExt, hasMcapiExt, hasQuarkExt);
	genGlobals(fw, hasMrapiExt, hasMcapiExt, hasQuarkExt);
	genFuncSignatures(fw, hasMrapiExt, hasMcapiExt);
	genFunctions(fw);
	genThreads(fw);

	fclose(fw);

	return 0;
}
