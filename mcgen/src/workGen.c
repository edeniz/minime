/*
 * workGen.c
 *
 *  Created on: May 20, 2014
 *      Author: Alex Ford
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "workGen.h"

unsigned Fibonacci(int num);

void MergeSort(int* start, int* end);
void Merge(int* start, int* mid, int* end);

const char* recursiveFunction =
                "unsigned fibonacci(int n) {\n"
		"\tif (n < 2) {\n"
		"\t\treturn (unsigned) n;\n"
		"\t} else {\n"
		"\t\treturn fibonacci(n-1) + fibonacci(n-2);\n"
		"\t}\n"
		"}\n";

const char* sortFunction = 
	"void merge(int* start, int* mid, int* end, int* tmp_a) {\n"
    "\tint *first_s1 = start, *last_s1 = mid, *first_s2 = mid, *last_s2 = end;\n"
    "\tint *tmp;\n"
	"\tfor (tmp = tmp_a; (first_s1 < last_s1) && (first_s2 < last_s2); ++tmp) {\n"
    "\t\tif (*first_s1 < *first_s2) {\n"
	"\t\t\t*tmp = *first_s1;\n"
	"\t\t\t++first_s1;\n"
	"\t\t} else {\n"
    "\t\t\t*tmp = *first_s2;\n"
    "\t\t\t++first_s2;\n"
    "\t\t}\n"
    "\t}\n"
    "\twhile (first_s1 < last_s1) {\n"
    "\t\t*tmp++ = *(first_s1++);\n"
    "\t}\n"
    "\twhile (first_s2 < last_s2 ) {\n"
    "\t\t*tmp++ = *(first_s2++);\n"
    "\t}\n"
    "\ttmp = tmp_a;\n"
    "\twhile (start < end) {\n"
    "\t\t*start++ = *tmp++;\n"
    "\t}\n"
    "\treturn;\n"
    "}\n";

const char* searchFunction = 
    "int search(int *a, int N, int q) {\n"
    "\tif (N==0) {\n"
    "\t\treturn 0;\n"
    "\t} else if (a[N/2] == q) {\n"
    "\t\treturn 1;\n"
    "\t}\n"
    "\treturn 0;\n"
    "}\n";

const char* MERGESORT_STR =
		"merge";

const char* RECURSIVE_STR = 
		"fibonacci";

const char* THREAD_ID_STR = 
		"get_num_contexts() * get_core_id() + get_thread_id()";

const char* BINARYSEARCH_STR = 
        "search";

const char* NOOP_STR = "0";

void printWorkFunction(FILE *fw, workFuncType type) {

	if (type == FUNC_NOOP) {
		/* do nothing */
	} else if (type == FUNC_TID) {
		/* do nothing */
	} else if (type == FUNC_RECURSIVE) {
		fprintf(fw, "%s\n", recursiveFunction);	
	} else if (type == FUNC_SORT) {
		fprintf(fw, "%s\n", sortFunction);
	} else if (type == FUNC_SEARCH) {
        fprintf(fw, "%s\n", searchFunction);
    }
	return;
}

unsigned doWorkFunction(struct tThreadData *td, workFuncType type, unsigned workload) {
	if (type == FUNC_NOOP) {
                /* do nothing */
		return 0;
        } else if (type == FUNC_TID) {
            return (unsigned) td->tid; 
        } else if (type == FUNC_RECURSIVE) {
            return Fibonacci(workload); 
        } else if (type == FUNC_SORT) {
		    return 0; // We want to match with strcmp which returns 0
	    } else if (type == FUNC_SEARCH) {
            return 1; // We want to make sure workloads are equal (recursive found? == Main Td found?);
        }	
        return -1;
}

const char* getWorkFunctionStr(workFuncType type) {
	if (type == FUNC_NOOP) {
                /* do nothing */
                return NOOP_STR;
        } else if (type == FUNC_TID) {
                return THREAD_ID_STR; 
        } else if (type == FUNC_RECURSIVE) {
                return RECURSIVE_STR; 
        } else if (type == FUNC_SORT) {
		    return MERGESORT_STR;
	    } else if (type == FUNC_SEARCH) {
            return BINARYSEARCH_STR;
        }    
        return NULL;	
}

const char* getThreadAddrType (struct tThreadData *td) {

	int i;
	struct tMemOp *pMemOp;

	const char *shared = "shm";
	const char *global = "global";
	const char *local = "local";
	const char *null = "";


	if (td->lsExt.numOfSharedMems > 0) {
        return shared;
    } else if (td->lsExt.numOfLocalMems > 0) {
        return local;
    }	
	else if (td->lsExt.hasExt) {
		if (td->lsExt.numOfSharedMems > 0) {
			return shared;
		} else if (td->lsExt.numOfLocalMems > 0) {
			return local;
		} else {
			printf("DEBUG: numOfSharedMems is %d for thread %d\n", td->lsExt.numOfSharedMems, td->tid);
			printf("DEBUG: numOfLocalMems is %d for thread %d\n", td->lsExt.numOfLocalMems, td->tid);
		}
		for (i = 0; i < td->lsExt.numOfMemOps; i++) {
			pMemOp = &td->lsExt.memOps[i];
			if (pMemOp->type == MEM_TYPE_SHARED) {
                return shared;	
			} else if (pMemOp->type == MEM_TYPE_LOCAL) {
				return local;		
			} else if (pMemOp->type == MEM_TYPE_GLOBAL) {
				return global;
			} 
		}               	 
    } else {
		printf("DEBUG: Thread %d has no lsExt\n", td->tid);
	} 
	
	return null;	
}

boolean threadHasSharedAdressType (struct tThreadData *td) {

	if (td->lsExt.numOfSharedMems > 0) {
                return true;
    } 
	printf("DEBUG: thread %d has no shared memory\n", td->tid);
       	return false;
}

/******************************************** FUNC_RECURSIVE **************************************/
unsigned Fibonacci (int n) {
	if (n < 2) {
		return (unsigned) n;
	} else {
		return Fibonacci(n-1) + Fibonacci(n-2);
	}
}

/******************************************** FUNC_TID **************************************/

/******************************************** FUNC_SORT *************************************/
void MergeSort(int* start, int* end) {
	if (end - start > 1) { // one element is already sorted
		// find the middle
		int* mid = start + (end - start) / 2;
		
		// sort left side
		MergeSort(start, mid);
		// sort right side
		MergeSort(mid, end);
		// merge two halves
		Merge(start, mid, end);
	}
}

void Merge(int* start, int* mid, int* end) {
	int *tmp_a = malloc((end - start) * sizeof(int));
	int* first_s1 = start, *last_s1 = mid, *first_s2 = mid, *last_s2 = end;
	int *tmp;

	for (tmp = tmp_a; (first_s1 < last_s1) && (first_s2 < last_s2); ++tmp) {
		if (*first_s1 < *first_s2) {
			*tmp = *first_s1;
			++first_s1;
		} else {
			*tmp = *first_s2;
			++first_s2;
		}
	}
	while (first_s1 < last_s1) {
		*tmp++ = *(first_s1++);
	}
	while (first_s2 < last_s2 ) {
		*tmp++ = *(first_s2++);
	}
	tmp = tmp_a;
	while (start < end) {
		*start++ = *tmp++;
	}
	free(tmp_a);
}

/******************************************** FUNC_SEARCH *************************************/
/**
 * Search()
 * Search array 'a' of size with N integers, using Binary-Search Algorithm.
 */
int Search(int *a, int N, int q) {
    if (N==0) {
        return 0;
    } else if (a[N/2] == q) {
        return 1;
    } else {
        int x,y;
        x = Search(a, N/2, q);
        y = Search(a + (N/2) + 1, N/2, q);
        return (x | y);
    }
    return 0;   // This case will never be hit
}
