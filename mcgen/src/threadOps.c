/*
 * threadOps.c 
 *
 *  Created on: Aug 11, 2014
 *      Author: Alex Ford
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "benchGen.h"
#include "threadOps.h"

void initThreads(FILE *fw, struct tThreadData *td, hwImplementationType implementation, memType mem_type, int numOfThreads, char *tab) {
	int i;

#ifdef DEBUG_ON
        fprintf(fw, "%s/* ============================ Initilize Threads =============================== */\n", tab);
#endif

	if (implementation == OS) {
	    /* do nothing */
	} else if (implementation == BARE_METAL) {
	    fprintf(fw, "%sstart_all_contexts(get_thread_tid());                            /* thread 0 starts all contexts */\n", tab);
	    fprintf(fw, "%std->tid = get_num_contexts() * get_core_id() + get_thread_id();  /* init thread id */\n", tab);
	    fprintf(fw, "%std->workload = 0;						/* init workload */\n", tab);
	    fprintf(fw, "%sswitch (td->tid) {\n", tab);
	    for (i = 0; i < numOfThreads; i++) {
		    if (i == td->id) {
			    continue;
		    }
		    fprintf(fw, "%scase %d: {\n", tab, activeProc->threads[i]->tid);
		    fprintf(fw, "%s\tready_for_work((%d %% get_num_contexts()) + 1);\n", tab, activeProc->threads[i]->tid);
		    fprintf(fw, "%s} break;\n", tab);
	    }
	    fprintf(fw, "%scase 0: {\n", tab);
	    if (mem_type == MEM_TYPE_SHARED) {

	    } else if (mem_type == MEM_TYPE_LOCAL) {
		
	    }
	    incTab(1, tab);
	} else {
	    printf("ERROR: Unknown Hardware Implementation\n");
	}

	return;
}

void finalizeThreads(FILE *fw, hwImplementationType implementation, char* tab) {

	decTab(1, tab);

#ifdef DEBUG_ON
	fprintf(fw, "%s\t/* ============================ Finalize Threads =============================== */\n", tab);
#endif

	if (implementation == OS) {
            /* do nothing */
        } else if (implementation == BARE_METAL) {
		fprintf(fw, "%s\t} break;\n", tab);
		fprintf(fw, "%s\tdefault: {\n", tab);
		fprintf(fw, "%s\t} break;\n", tab);
		fprintf(fw, "%s\t}\n", tab);
		fprintf(fw, "%s\treturn 0;\n", tab);
        } else {
            printf("ERROR: Unknown Hardware Implementation\n");
        }

        return;	
}

void printCoreIdQuark(FILE *fw, struct tThreadData *td) {
        fprintf(fw, "%d / get_num_contexts()", td->tid);
}

void printPortIdQuark(FILE *fw, struct tMcapiEndpoint *ep) {
        fprintf(fw, "%d * get_num_contexts() + %d + get_num_contexts()", ep->nodeId, ep->portId);
}

int createThreads(FILE *fw, struct tThreadData *td, hwImplementationType implementation, memType mem_type, int taskId, int childTdIdx, char* tab) {

#ifdef DEBUG_ON
        fprintf(fw, "%s/* ============================ Create Threads =============================== */\n", tab);
#endif


	if (implementation == OS) {
            fprintf(fw, "%src = pthread_create(&threads[t], NULL, task%d, (void *) &tData);\n",
                    tab, taskId);
            fprintf(fw, "%sif (rc) {\n", tab);
            fprintf(fw, "%s\tfprintf(stdout,\"ERROR; return code from pthread_create() is %s\\n\", rc);\n", tab, "%d");
            fprintf(fw, "%s\texit(-1);\n", tab);
            fprintf(fw, "%s}\n", tab);
        } else if (implementation == BARE_METAL) {
	    /* Bare Metal Operations are done through message passing */
            if (mem_type == MEM_TYPE_SHARED) {
			
            } else if (mem_type == MEM_TYPE_LOCAL) {
		
            } else if (mem_type == MEM_TYPE_GLOBAL) {
		
	        }
	        fprintf(fw, "%srun_on_core(%d / get_num_contexts(), // core id\n"
                        "%s\t(%d %% get_num_contexts()) + 1, // port id\n"
                        "%s\t(work_t) task%d, // task id\n"
                        "%s\t((void*) &tData[%d]));\n\n",
                        tab, td->tid, tab, td->tid, tab, taskId, tab, childTdIdx++);	 
        } else {
            printf("ERROR: Unknown Hardware Implementation\n");
	    exit(1);
        }
	return childTdIdx;
}

void joinThreads(FILE *fw, struct tThreadData *joiningTd, hwImplementationType implementation, memType mem_type, int children, char* tab) {
	int i;

#ifdef DEBUG_ON
        fprintf(fw, "%s/* ============================ Join Threads =============================== */\n", tab);
#endif

        if (implementation == OS) {
		fprintf(fw, "%sfor (t = 0; t < %d; t++) {\n", tab, children);
        	fprintf(fw, "%s\tpthread_join(threads[t], NULL);\n", tab); 		/* OS Implementation */
		fprintf(fw, "%s}\n", tab);        
        } else if (implementation == BARE_METAL) {
            /* Bare Metal Operations are done through message passing */
	        if (joiningTd->tid == MAIN_ID) {	
	    	    for (i = 0; i < joiningTd->bareMetalExt.numOfEndpoints; i++) {
			        if (joiningTd->bareMetalExt.endpoints[i].isOwner && !joiningTd->bareMetalExt.endpoints[i].isSender) {
                        if (joiningTd->standAloneExt.parPattern == PP_RD) {
                            fprintf(fw, "%std->workload = td->workload | recvmsg(td->addr, ", tab);   /* Bare Metal - Local Data */
				        } else if (mem_type == MEM_TYPE_SHARED) {
                            fprintf(fw, "\t//DEBUG: Joining Thread Failed for RD Pattern\n");
                	        fprintf(fw, "%srecvmsg(td->addr, ", tab);                   /* Bare Metal - Shared Data */
                	    } else if (mem_type == MEM_TYPE_LOCAL) {
                		    fprintf(fw, "%std->workload += recvmsg(td->addr, ", tab);   /* Bare Metal - Local Data */
                	    } else if (mem_type == MEM_TYPE_GLOBAL) {
                		    fprintf(fw, "%srecvmsg(td->addr, \n", tab);                 /* Bare Metal - Global Data */
                	    } else {
                		        printf("ERROR: Unknown Memory Type\n");
                		        exit(1);
                		}
                        printPortIdQuark(fw, &joiningTd->bareMetalExt.endpoints[i]);
                        fprintf(fw, ");\n");	
			        } 
	    	    }
	        }	
        } else {
            printf("ERROR: Unknown Hardware Implementation\n");
	    exit(1);
        }
        return;
}
