/*
 * Copyright (c) 2011-2013, Etem Deniz <etem.deniz@boun.edu.tr>
 * Alper Sen <alper.sen@boun.edu.tr>, Bogazici University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * (1) Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * (3) The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * patRecog.c
 *
 *  Created on: Dec 5, 2011
 *      Author: Etem Deniz
 */

 /*
 *							<< MULTI PARALLEL PATTERN DETECTION >>
 *
 * Finding Concurrency Design Space
 * 	Decomposition: Data Decomposition, Task Decomposition
 * 	Dependency Analysis: Group Tasks, Order Tasks, Data Sharing
 *
 *  ========================================================================================
 * |												--> Regular?	Pipeline				|
 * |			--> Organize By Flow of Data												|
 * |		  /									--> Irregular?	Event-Based Coordination	|
 * |		 /																				|
 * |		/								--> Linear?		Task Parallel					|
 * | Start	--> Organize By Tasks		<													|
 * |		\								-->	Recursive	Divide and Conquer				|
 * |		 \																				|
 * |		  \									--> Linear?		Geometric Decomposition		|
 * |			--> Organize By Data														|
 * |												--> Recursive?	Recursive Data			|
 *  ========================================================================================
 *
 *									Algorithm Structure Design Space
 *
 * A- Data Sharing
 *
 *  ====================================================================================================
 * | 			| Task 			| Divide and 	| Geometric		| Recursive	| Pipeline	| Event-based	|
 * | 			| Parallelism	| Conquer		| Decomposition	| Data		| 			| Coordination	|
 * |====================================================================================================|
 * | Read-Only	| ***			| 				| *				| ***		| 			| 				|
 * |----------------------------------------------------------------------------------------------------|
 * | Migratory	| **			| *** 			| **			| 			| ****		| ****			|
 * |----------------------------------------------------------------------------------------------------|
 * | Prod/Cons	| 				| **			| ****			| 			| 			| 				|
 * |----------------------------------------------------------------------------------------------------|
 * | Private	| ****			| *				| *				| ****		| *			| *				|
 *  ====================================================================================================
 *
 *  								Parallel Pattern Sharing Behaviors
 *
 * If (num_of_reads == 1 && num_of_writes == 1) then Private,
 * else if (num_of_writes == 0)  the Read-Only,
 * else if (num_of_reads > num_of_writes) Prod/Cons,
 * else Migratory.
 *
 * A-1) For example, in a pipeline pattern, data migrates between stages, 
 * 	as opposed to an embarrassingly parallel problem where 
 * 	data largely remains private to each thread.
 * A-2) The geometric decomposition pattern is typically characterized by sharing between threads, 
 * 	particularly threads with neighboring data.
 * A-3) Private data is common to patterns without much sharing, such as task parallel and recursive data.
 * A-4) Migratory data is common to pipeline and event based coordination. In these patterns, 
 * 	threads are pinned to various stages in a process and data migrates between threads. 
 * A-5) Producer / consumer sharing is common in patterns with widely shared data such as geometric decomposition.
 *
 *
 * B- Thread Communication
 *
 *  ====================================================================================================
 * | 			| Task 			| Divide and 	| Geometric		| Recursive	| Pipeline	| Event-based	|
 * | 			| Parallelism	| Conquer		| Decomposition	| Data		| 			| Coordination	|
 * |====================================================================================================|
 * | None		| ****			| ***			| 				| 			| *			| 				|
 * |----------------------------------------------------------------------------------------------------|
 * | Few		| *				| **	 		| *				| *			| ****		| ****			|
 * |----------------------------------------------------------------------------------------------------|
 * | Many		| 				| *				| ****			| ****		| 			| 				|
 *  ====================================================================================================
 *
 *  								Parallel Pattern Communication Behaviors
 *
 * B-1) The pipeline pattern is performed by taking a flow of data through tasks
 *  and splitting it into pipeline stages.
 * B-2) The style of freeform pipeline is sometimes referred to as event-based coordination.
 * B-3) Patterns with high degrees of inter-thread communication such as geometric decomposition 
 * 	would benefit more from intelligent thread placement such as 
 * 	placing high communication threads on the higher bandwidth nodes.
 * B-4) When the problem consists of a group of independent tasks or task groups to be run in parallel,
 * 	the parallel pattern employed is task parallelism. Task parallelism has limited to no sharing,
 * 	and a embarrassingly parallel problems are a large subclass of this group.
 *
 *
 * C- General Threading
 *
 *  ====================================================================================================================
 * | 							| Task 			| Divide and 	| Geometric		| Recursive	| Pipeline	| Event-based	|
 * | 							| Parallelism	| Conquer		| Decomposition	| Data		| 			| Coordination	|
 * |====================================================================================================================|
 * | PC (same)					| ***			| **			| ****			| ****		| 			| 				|
 * |--------------------------------------------------------------------------------------------------------------------|
 * | Thread balanced			| **			| *				| ****			| **		| **		| ***			|
 * |--------------------------------------------------------------------------------------------------------------------|
 * | Creator (same)				| ****			| ** 			| ****			| ***		| ***		| *				|
 * |--------------------------------------------------------------------------------------------------------------------|
 * | Lifetime of threads (same)	| **			| **	 		| ****			| *			| ***		| **			|
 * |--------------------------------------------------------------------------------------------------------------------|
 * | Creation times of threads	| ***			| *	 			| ****			| **		| ***		| **			|
 * |--------------------------------------------------------------------------------------------------------------------|
 * | Exit times of threads		| **			| 	 			| ***			| 			| **		| *				|
 *  ====================================================================================================================
 *
 *  								Parallel Pattern Thread Behaviors
 *
 * C-1) When there is a problem task that naturally subdivides into several smaller tasks
 * 	that can be done in parallel, then the divide and conquer pattern is applied. 
 * 	Divide and Conquer splits tasks until a work threshold is met and
 * 	the subtask works on a subset of the data serially.
 * C-2) A recursive data approach would create a thread for every node in the graph
 * 	and perform a graph-climbing algorithm independently for each node.
 * C-3) Patterns such as divide and conquer or recursive data have unique thread behaviors that
 * 	provide a signature pattern. Divide and conquer is characterized by a rising number of threads 
 * 	during the divide phase, some period of leveling for computation, followed by a declining phase
 * 	as the results are joined.
 * C-4) Recursive data, however, begins with many threads and slowly ramps down as
 * 	the different threads finish execution.
 * C-5) In addition to thread spawn/exit behavior, thread imbalance is unique between patterns.
 * 	For example, geometric decomposition is typically SIMD or SPMD programming 
 * 	that is more balanced than a recursive data pattern. To measure the imbalance, 
 * 	the number of dynamic instructions per thread is measured to calculate 
 * 	the average number of instructions per thread. If most of the threads have 
 * 	dynamic instruction counts within one standard deviation of the mean, 
 * 	the workload is considered balanced. Otherwise it is considered unbalanced.
 * C-6) We also investigated the PC (Program Counter) uniqueness between threads.
 * 	This test looks at whether a SPMD or SIMD style programming method was used. 
 * 	The PCs accessed by each thread are counted, and if most PCs are unique 
 * 	this suggests a non-SPMD style pattern such as pipeline. This is measured by 
 * 	measuring for each PC accessed how many threads used that PC. 
 * 	Patterns such as pipeline will have unique PCs for each thread, 
 * 	whereas geometric decomposition will share PCs with most threads.
 *
 *
 * D- Synchronization/Concurrency
 * 
 *  ========================================================================================================
 * | 				| Task 			| Divide and 	| Geometric		| Recursive	| Pipeline	| Event-based	|
 * | 				| Parallelism	| Conquer		| Decomposition	| Data		| 			| Coordination	|
 * |========================================================================================================|
 * | #locks			| **			| **			| ****			| ****		| ****		| ****			|
 * |--------------------------------------------------------------------------------------------------------|
 * | #conditions	| *				| 		 		| **			| **		| ***		| ***			|
 * |--------------------------------------------------------------------------------------------------------|
 * | #barriers		| *				| 				| *				| *			| 			| 				|
 *  ========================================================================================================
 *
 *									Parallel Pattern Synchronization/Concurrency Behaviors
 *
 *	(_: x == 0;	 *: x <= 2; 	**: 2 < x <= 4; 	***: 4 < x <= 6; 	****: 6 < x)
 *
 * D-1) The number of the locks, conditions, barriers.
 * D-2) The types of access (reader versus writer), and relation to other critical sections.
 *
 * */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include "global.h"
#include "patRecog.h"

paralPattern paralPatterns[MAX_PAR_PAT] = {
		PP_TP, PP_DaC, PP_GD, PP_RD, PP_Pl, PP_EbC
};

double ppDataSharingPattern[MAX_DATA_SHR][MAX_PAR_PAT] = {
		{3, 0, 1, 3, 0, 0},
		{2, 3, 2, 0, 4, 4},
		{0, 2, 4, 0, 0, 0},
		{4, 1, 1, 4, 1, 1}
};

double ppCommPattern[MAX_COMM][MAX_PAR_PAT] = {
		{4, 3, 0, 0, 1, 0},
		{1, 2, 1, 1, 4, 4},
		{0, 1, 4, 4, 0, 0}
};

double ppGenThreadingPattern[MAX_GEN_THREADING][MAX_PAR_PAT] = {
		{3, 2, 4, 4, 0, 0},
		{2, 1, 4, 2, 2, 3},
		{4, 2, 4, 3, 3, 1},
		{2, 2, 4, 1, 3, 2},
		{3, 1, 4, 2, 3, 2},
		{2, 0, 3, 0, 2, 1}
};

double ppSyncConPattern[MAX_COMM][MAX_PAR_PAT] = {
		{2, 2, 4, 4, 4, 4},
		{0, 0, 1, 1, 3, 3},
		{0, 0, 1, 1, 0, 0}
};

/* min score = 0; max score = 100 */
int dsScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
int commScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
int genThreadingScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
int syncConScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];
int totalScores[MAX_GROUP_NUMBER][MAX_PAR_PAT];

double maxDistDS;
double maxDistTC;
double maxDistGT;
double maxDistSC;

double scoreMultDS;
double scoreMultTC;
double scoreMultGT;
double scoreMultSC;

double weightDS = 1;
double weightTC = 1;
double weightGT = 1;
double weightSC = 0; //FIXME: equals to what?

double cahceMissRatio = 0.1;
double cahceMissRatioInterval = 0.01;
double dsLowerBoundRatio = 0.1;
double tcLowerBoundRatio = 0.1;
double stdDevMult = 2;
double cvUpperBound = 0.2;

int scoreMax = 4;
int activeGroupId;

char *getPatternText(paralPattern pp) {

	char *pattern;

	switch (pp) {
	case PP_TP:
		pattern = "Task Parallel";
		break;
	case PP_DaC:
		pattern = "Divide and Conquer";
		break;
	case PP_GD:
		pattern = "Geometric Decomposition";
		break;
	case PP_RD:
		pattern = "Recursive Data";
		break;
	case PP_Pl:
		pattern = "Pipeline";
		break;
	case PP_EbC:
		pattern = "Event-based Coordination";
		break;
	case PP_Hl:
		pattern = "Hierarchical";
		break;
	case PP_ST:
		pattern = "Single Thread";
		break;
	default:
		pattern = "Unknown";
		break;
	}

	return pattern;
}

void initGlobalValues() {

	maxDistDS = sqrt(pow(scoreMax, 2) * MAX_DATA_SHR);
	maxDistTC = sqrt(pow(scoreMax, 2) * MAX_COMM);
	maxDistGT = sqrt(pow(scoreMax, 2) * MAX_GEN_THREADING);
	maxDistSC = sqrt(pow(scoreMax, 2) * MAX_SYNC_CON);

	scoreMultDS = 100/maxDistDS;
	scoreMultTC = 100/maxDistTC;
	scoreMultGT = 100/maxDistGT;
	scoreMultSC = 100/maxDistSC;

	memset(dsScores, 0, sizeof(dsScores));
	memset(commScores, 0, sizeof(commScores));
	memset(genThreadingScores, 0, sizeof(genThreadingScores));
	memset(syncConScores, 0, sizeof(syncConScores));

	memset(totalScores, 0, sizeof(totalScores));

	return;
}

void addScores(int lhs[MAX_PAR_PAT], int rhs[MAX_PAR_PAT], double weight) {

	int i = 0;

	for (i = 0; i < MAX_PAR_PAT; i++) {
		lhs[i] += (weight * rhs[i]);
	}

	return;
}

paralPattern getBestFit(int scores[MAX_PAR_PAT], int dsScores[MAX_PAR_PAT], int commScores[MAX_PAR_PAT],
		int genThreadingScores[MAX_PAR_PAT], int syncConScores[MAX_PAR_PAT]) {

	int i, bestFitIndex = 0, bestFitValue = 0;
	int old, new;

	for (i = 0; i < MAX_PAR_PAT; i++) {
		if (scores[i] > bestFitValue) {
			bestFitValue = scores[i];
			bestFitIndex = i;
		} else if (scores[i] == bestFitValue) { /* majority voting when have same scores */
				if (dsScores[bestFitIndex] > dsScores[i]) old++;
				else if (dsScores[bestFitIndex] < dsScores[i]) new++;
				if (commScores[bestFitIndex] > commScores[i]) old++;
				else if (commScores[bestFitIndex] < commScores[i]) new++;
				if (genThreadingScores[bestFitIndex] > genThreadingScores[i]) old++;
				else if (genThreadingScores[bestFitIndex] < genThreadingScores[i]) new++;
				if (syncConScores[bestFitIndex] > syncConScores[i]) old++;
				else if (syncConScores[bestFitIndex] < syncConScores[i]) new++;
				if (new > old) {
					bestFitValue = scores[i];
					bestFitIndex = i;
				}
		}
	}

	//FIXME: I can also do majority voting.

	return paralPatterns[bestFitIndex];
}

int getTimeInNanosec(struct tTimeData *time) {

	int retTime;

	retTime = time->nanosec;
	retTime += time->second * 1000;
	retTime += time->minute * (1000 * 60);
	retTime += time->hour * ((1000 * 60) * 60);

	return retTime;
}

int getLifeTime(struct tTimeData *cTime, struct tTimeData *eTime) {

	int lifeTime, creationTime, exitTime;

	creationTime = getTimeInNanosec(cTime);
	exitTime = getTimeInNanosec(eTime);

	if (exitTime >= creationTime) {
		lifeTime = exitTime - creationTime;
	} else {
		printf("ERROR! exit time of a thread is earlier than creation time of the thread.\n");
		exit(-5);
	}

	return lifeTime;
}

int getNormElemCount(int data[MAX_THREADS], int totalData, int numOfElements) {

	double mean, stdDev, lowerBound, upperBound;
	double total;
	int count = 0;
	int i;

	assert(numOfElements > 0);

	//FIXME: First, I can remove outliers

	mean = (int) (totalData / numOfElements);
	
	for (i = 0, total = 0; i < numOfElements; i++) {
		total += pow((data[i] - mean), 2);
	}

	stdDev = (int) sqrt(total / numOfElements);

	/* The coefficient of variation (CV) is defined as the ratio of the standard deviation to the mean */
	if (stdDev / mean < cvUpperBound) {
		lowerBound = mean - (stdDevMult * stdDev);
		upperBound = mean + (stdDevMult * stdDev);
		for (i = 0; i < numOfElements; i++) {
			if (data[i] > lowerBound && data[i] < upperBound) {
				count++;
			}
		}
	}

	return count;
}

void findDataSharingPattern() {

	double dataSharing[MAX_DATA_SHR];
	double multiplier, total, dist;
	double max = MIN_DIST;
	double dsDistances[MAX_PAR_PAT];
	int i, j;

	dataSharing[0] = (double) activeProc->dataSharing[activeGroupId].readonly;
	dataSharing[1] = (double) activeProc->dataSharing[activeGroupId].migratory;
	dataSharing[2] = (double) activeProc->dataSharing[activeGroupId].prodCons;
	dataSharing[3] = (double) activeProc->dataSharing[activeGroupId].private;

	for (i = 0; i < MAX_DATA_SHR; i++) {
		if (dataSharing[i] > max) {
			max = dataSharing[i];
		}
	}

	if (max > 0) {
		multiplier = scoreMax / max;

		for (i = 0; i < MAX_DATA_SHR; i++) {
			dataSharing[i] *= multiplier;
		}

		for (i = 0; i < MAX_PAR_PAT; i++) {
			total = 0;
			for (j = 0; j < MAX_DATA_SHR; j++) {
				dist = dataSharing[j] - ppDataSharingPattern[j][i];
				total += pow(dist, 2);
			}
			dsDistances[i] = sqrt(total);
			dsScores[activeGroupId][i] = (maxDistDS - dsDistances[i]) * scoreMultDS;
		}
	} else {
		for (i = 0; i < MAX_PAR_PAT; i++) {
			dsScores[activeGroupId][i] = 0; /* no information, so assign zero */
		}
	}

	memcpy(&activeProc->parPatternScores.dsSubScores[activeGroupId], &dataSharing, MAX_DATA_SHR * sizeof(double));

#ifdef DEBUG_ON
	printf("[DEBUG] %s, DS behavior stars: %.2f, %.2f, %.2f, %.2f\n", activeProc->name,
			dataSharing[0], dataSharing[1], dataSharing[2], dataSharing[3]);

	printf("[DEBUG] %s, DS characteristics: %d, %d, %d, %d\n", activeProc->name,
			activeProc->dataSharing[activeGroupId].readonly,
			activeProc->dataSharing[activeGroupId].migratory,
			activeProc->dataSharing[activeGroupId].prodCons,
			activeProc->dataSharing[activeGroupId].private);
#endif

	return;
}

void findCommPattern() {

	double comm[MAX_COMM] = { 0 };
	double total, dist;
	double commDistances[MAX_PAR_PAT];
	struct tThreadData *td;
	int numOfDeps = 0, numOfCreatorDeps = 0, numOfCreatorDepsOn = 0, numOfDepsExceptMainDeps;
	int valueOfDeps = 0;
	int numOfCycDeps = 0, pc = 0;
	int numOfThreads, numOfThreadsInGroup = 0;
	int totalCache, threshold;
	int dependsFrom[MAX_THREADS] = { 0 }, dependsTo[MAX_THREADS] = { 0 };
	int i, j, k;
	boolean isDone = false, hasUniquePC = true;

	numOfThreads = activeProc->numOfThreads;

	for (i = 1; i <= numOfThreads; i++) {
		// check PC uniqueness
		td = activeProc->threads[i];
		if (pc != 0 && hasUniquePC == true && td->pc != pc) {
			hasUniquePC = false;
		} else if (i > 1) {
			pc = td->pc;
		}

		//FIXME: threshold should not depend on cacheInvalid
		threshold = (int) (td->cacheInvalid * cahceMissRatio);
		if (td->groupId == activeGroupId || td->groupId == -1) {
			numOfThreadsInGroup++;
		} else {
			continue;
		}
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId && td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold) {
					numOfDeps++;
					valueOfDeps += td->dependency[j];
					if (i == activeProc->threads[j]->creator) {
						numOfCreatorDepsOn++;
					}
					if (j == td->creator) {
						numOfCreatorDeps++;
					} else {
						dependsTo[i] = 1;
						dependsFrom[j] = 1;
					}
					if (activeProc->threads[j]->dependency[i] > (int) (activeProc->threads[j]->cacheInvalid * cahceMissRatio)) {
						numOfCycDeps++;
					}
				}
			}
		}
	}

	numOfDepsExceptMainDeps = numOfDeps - numOfCreatorDeps;
	//FIXME: totalCache should not include the main thread cache
	totalCache = activeProc->dataSharing[activeGroupId].total;
	numOfThreadsInGroup--; /* except main */

	/* check whether a pipeline dependency exists for pipeline pattern */
	if (numOfThreads > 2 && numOfDepsExceptMainDeps == (numOfThreadsInGroup - 1) + numOfCreatorDepsOn) {
		for (i = 1, j = 0, k = 0; i <= numOfThreads; i++) {
			if (dependsTo[i] == 1) {
				j++;
			}
			if (dependsFrom[i] == 1) {
				k++;
			}
		}
		if (j == numOfDepsExceptMainDeps && k == j) { /* pipeline */
			comm[0] = ppCommPattern[0][PP_Pl]; // none
			comm[1] = ppCommPattern[1][PP_Pl]; // few
			comm[2] = ppCommPattern[2][PP_Pl]; // many
			isDone = true;
		}
	}

	if (isDone == false) {
		//if has no numOfCycDeps with not unique PCs then Pl
		if (numOfCycDeps == 0 && hasUniquePC == false && numOfThreads > 2 &&
				activeProc->dataSharing[activeGroupId].migratory + activeProc->dataSharing[activeGroupId].prodCons > 0) {
			comm[0] = ppCommPattern[0][PP_Pl]; // none
			comm[1] = ppCommPattern[1][PP_Pl]; // few
			comm[2] = ppCommPattern[2][PP_Pl]; // many
			isDone = true;
		}
	}

	if (isDone == false) {
		if (numOfDepsExceptMainDeps + 1 <= numOfThreadsInGroup * dsLowerBoundRatio) {
			// TP or DaC
			comm[0] = scoreMax - (int) ((double) numOfDepsExceptMainDeps / (double) numOfThreadsInGroup); // none
			comm[1] = scoreMax - comm[0]; // few
			comm[2] = 0; // many
		} else {
			if (valueOfDeps < totalCache * dsLowerBoundRatio + 1) {
				if (numOfDepsExceptMainDeps > numOfThreadsInGroup) {
					numOfDepsExceptMainDeps = numOfThreadsInGroup;
				}
				// Pl or EbC
				comm[1] = ((double) numOfDepsExceptMainDeps / (double) numOfThreadsInGroup) * scoreMax; // few
				comm[0] = scoreMax - comm[1]; // none
			}

			//FIXME: Fix here for recursive data
			if (totalCache > valueOfDeps * dsLowerBoundRatio + 1) {
				if (valueOfDeps > totalCache) {
					valueOfDeps = totalCache;
				}
				// GD or RD
				comm[2] = ((double) valueOfDeps / (double) totalCache) * scoreMax; // many
			} else {
				// Pl or EbC
				comm[1] = ((double) totalCache / (double) valueOfDeps) * scoreMax; // few
				comm[0] = scoreMax - comm[1]; // none
				comm[2] = 0; // many
			}
		}
	}

	for (i = 0; i < MAX_PAR_PAT; i++) {
		total = 0;
		for (j = 0; j < MAX_COMM; j++) {
			dist = comm[j] - ppCommPattern[j][i];
			total += pow(dist, 2);
		}
		commDistances[i] = sqrt(total);
		commScores[activeGroupId][i] = (maxDistTC - commDistances[i]) * scoreMultTC;
	}

	memcpy(&activeProc->parPatternScores.tcSubScores[activeGroupId], &comm, MAX_COMM * sizeof(double));

#ifdef DEBUG_ON
	printf("[DEBUG] %s, TC behavior stars: %.2f, %.2f, %.2f\n", activeProc->name,
			comm[0], comm[1], comm[2]);

	printf("[DEBUG] %s, TC characteristics: %d, %d, %d, %d\n", activeProc->name,
			numOfDepsExceptMainDeps, numOfThreadsInGroup, valueOfDeps, totalCache);
#endif

	return;
}

void findGenThreadingPattern() {

	double gt[MAX_GEN_THREADING] = { 0 };
	double total, dist;
	double gtDistances[MAX_PAR_PAT];
	struct tThreadData *td;
	int data[MAX_THREADS] = { 0 }, totalData, dataCount;
	int numOfThreads;
	int numOfTasks = 0, numOfCreators = 0, balanceCount;
	int lifeTimeCount, creationTimeCount, exitTimeCount;
	int i, j;

	numOfThreads = activeProc->numOfThreads;

	/* PC (same) */
	for (i = 2, dataCount = 0; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->groupId == activeGroupId) {
			dataCount++;
			for (j = 0; j < numOfTasks; j++) {
				if (td->pc == data[j]) {
					break;
				}
			}
			if (j == numOfTasks) {
				data[numOfTasks] = td->pc;
				numOfTasks++;
			}
		}
	}
	// 1: same, 0: unique
	if (dataCount > numOfTasks) {
		gt[0] = ((1.0 / (dataCount - 1)) * (dataCount - numOfTasks)) * scoreMax;
	} else {
		gt[0] = scoreMax;
	}

	/* Thread balanced */
	for (i = 2, dataCount = 0, totalData = 0; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->groupId == activeGroupId) {
			data[dataCount] = td->instCount;
			totalData += data[dataCount];
			dataCount++;
		}
	}
	// 1: balanced, 0: not balanced
	balanceCount = getNormElemCount(data, totalData, dataCount);
	gt[1] = ((double) balanceCount / (double) dataCount) * scoreMax;

	/* Creator (same) */
	for (i = 2, dataCount = 0; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->groupId == activeGroupId) {
			dataCount++;
			for (j = 0; j < numOfCreators; j++) {
				if (td->creator == data[j]) {
					break;
				}
			}
			if (j == numOfCreators) {
				data[numOfCreators] = td->creator;
				numOfCreators++;
			}
		}
	}
	// 1: same, 0: different
	if (dataCount > numOfCreators) {
		gt[2] = ((1.0 / (dataCount - 1)) * (dataCount - numOfCreators)) * scoreMax;
	} else {
		gt[2] = scoreMax;
	}

	/* Life time of threads (same) */
	for (i = 2, dataCount = 0, totalData = 0; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->groupId == activeGroupId) {
			data[dataCount] = getLifeTime(&td->creationTime, &td->exitTime);
			totalData += data[dataCount];
			dataCount++;
		}
	}
	// 1: same, 0: different
	lifeTimeCount = getNormElemCount(data, totalData, dataCount);
	gt[3] = ((double) lifeTimeCount / (double) dataCount) * scoreMax;

	/* Creation times of threads */
	for (i = 2, dataCount = 0, totalData = 0; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->groupId == activeGroupId) {
			data[dataCount] = getTimeInNanosec(&td->creationTime);
			totalData += data[dataCount];
			dataCount++;
		}
	}
	// 1: same, 0: different
	creationTimeCount = getNormElemCount(data, totalData, dataCount);
	gt[4] = ((double) creationTimeCount / (double) dataCount) * scoreMax;

	/* Exit times of threads */
	for (i = 2, dataCount = 0, totalData = 0; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		if (td->groupId == activeGroupId) {
			data[dataCount] = getTimeInNanosec(&td->exitTime);
			totalData += data[dataCount];
			dataCount++;
		}
	}
	// 1: same, 0: different
	exitTimeCount = getNormElemCount(data, totalData, dataCount);
	gt[5] = ((double) exitTimeCount / (double) dataCount) * scoreMax;

	for (i = 0; i < MAX_PAR_PAT; i++) {
		total = 0;
		for (j = 0; j < MAX_GEN_THREADING; j++) {
			dist = gt[j] - ppGenThreadingPattern[j][i];
			total += pow(dist, 2);
		}
		gtDistances[i] = sqrt(total);
		genThreadingScores[activeGroupId][i] = (maxDistGT - gtDistances[i]) * scoreMultGT;
	}

	memcpy(&activeProc->parPatternScores.gtSubScores[activeGroupId], &gt, MAX_GEN_THREADING * sizeof(double));

#ifdef DEBUG_ON
	printf("[DEBUG] %s, GT behavior stars: %.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n", activeProc->name,
			gt[0], gt[1], gt[2], gt[3], gt[4], gt[5]);

	// 1: different, 0: same
	printf("[DEBUG] %s, GT characteristics: %.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n", activeProc->name,
			gt[0] / scoreMax, gt[1] / scoreMax, gt[2] / scoreMax,
			gt[3] / scoreMax, gt[4] / scoreMax, gt[5] / scoreMax);

#endif

	return;

}

void findSyncConPattern() {

	double syncCon[MAX_SYNC_CON] = { 0 };
	double total, dist;
	double syncConDistances[MAX_PAR_PAT];
	int data, val;
	int i, j;

	data = activeProc->syncData[activeGroupId].locks;
	val = (int) ((data + (data % 2)) / 2);
	syncCon[0] = val > scoreMax ? scoreMax : val;
	data = activeProc->syncData[activeGroupId].conditions;
	val = (int) ((data + (data % 2)) / 2);
	syncCon[1] = val > scoreMax ? scoreMax : val;
	data = activeProc->syncData[activeGroupId].barriers;
	val = (int) ((data + (data % 2)) / 2);
	syncCon[2] = val > scoreMax ? scoreMax : val;

	for (i = 0; i < MAX_PAR_PAT; i++) {
		total = 0;
		for (j = 0; j < MAX_SYNC_CON; j++) {
			dist = syncCon[j] - ppSyncConPattern[j][i];
			total += pow(dist, 2);
		}
		syncConDistances[i] = sqrt(total);
		syncConScores[activeGroupId][i] = (maxDistSC - syncConDistances[i]) * scoreMultSC;
	}

	memcpy(&activeProc->parPatternScores.scSubScores[activeGroupId], &syncCon, MAX_SYNC_CON * sizeof(double));

#ifdef DEBUG_ON
	printf("[DEBUG] %s, SC behavior stars: %.2f, %.2f, %.2f\n", activeProc->name,
			syncCon[0], syncCon[1], syncCon[2]);

	printf("[DEBUG] %s, SC characteristics: %d, %d, %d\n", activeProc->name,
			activeProc->syncData[activeGroupId].locks,
			activeProc->syncData[activeGroupId].conditions,
			activeProc->syncData[activeGroupId].barriers);
#endif

	return;
}

void printScores(paralPattern bestFits[MAX_GROUP_NUMBER]) {

	int i, j;

	printf("  Application: %s\n", activeProc->name);

	for (i = 0; i < maxGroupId; i++) {
		if (maxGroupId > 1) {
			printf("   >Phase %d:\n", i + 1);
		}
		printf("    Parallel Pattern: DS + TC + GT + SC = Total\n");
		for (j = 0; j < MAX_PAR_PAT; j++) {
			printf("    %s: %d + %d + %d + %d = %d",
					getPatternText(paralPatterns[j]), (int) (weightDS * dsScores[i][j]),
					(int) (weightTC * commScores[i][j]), (int) (weightGT * genThreadingScores[i][j]),
					(int) (weightSC * syncConScores[i][j]), (int) (totalScores[i][j]));
			if (paralPatterns[j] != bestFits[i]) {
				printf("\n");
			} else {
				printf("*\n");
			}
		}
	}

	printf("  Parallel Pattern(s): ");
	for (i = 0; i < maxGroupId; i++) {
		if (i == maxGroupId - 1) {
			printf("%s\n", getPatternText(bestFits[i]));
		} else {
			printf("%s + ", getPatternText(bestFits[i]));
		}
	}

	return;
}

int printGraph(paralPattern pps[MAX_GROUP_NUMBER]) {

	int i, j;
	struct tThreadData *td;
	int numOfThreads, threshold, edgeCount = 0;
	char output[1024], temp[32];
	FILE *fw;
	double cahceMissRatios[MAX_GROUP_NUMBER] = { cahceMissRatio };
	double totalCahceMissRatio = 0;

	for (i = 0; i < maxGroupId; i++) {
		if (pps[i] == PP_TP || pps[i] == PP_DaC) {
			cahceMissRatios[i] = cahceMissRatio + cahceMissRatioInterval;
		} else {
			cahceMissRatios[i] = cahceMissRatio - cahceMissRatioInterval;
		}
		totalCahceMissRatio += cahceMissRatios[i];
	}

	cahceMissRatio = totalCahceMissRatio / maxGroupId;
	/* Update the value since I also use this value in benchmark preparation */

	numOfThreads = activeProc->numOfThreads;

	output[0] = '\0';
	for (i = 1; i <= numOfThreads; i++) {
		td = activeProc->threads[i];
		threshold = (int) ((td->cacheMiss + td->cacheInvalid) * cahceMissRatio);
		for (j = 1; j <= numOfThreads; j++) {
			if (i != j) {
				if (td->groupId != -1 && activeProc->threads[j]->groupId != -1) {
					if (td->groupId != activeProc->threads[j]->groupId && td->groupId == -1) {
						continue;
					}
				}
				if (td->dependency[j] > threshold || j == 1) {
					sprintf(temp, "%d %d\n", j - 1, i - 1);
					strcat(output, temp);
					edgeCount++;
				}
			}
		}
	}

	fw = fopen("graph.txt", "w+");
	if (fw != NULL) {
		fprintf(fw, "%d\n%d\n%s", numOfThreads, edgeCount, output);
		fclose(fw);
	} else {
		return -1;
	}

	return 0;
}

//FIXME: I can use weights while calculating the total score.
/*
 * If the prediction is correct, then I can save what I've learned.
 * I can teach my Pattern Recognizer. This means tuning the parameters to their best.
 */

int recogPattern(paralPattern pps[MAX_GROUP_NUMBER], boolean isVerboseOpen, boolean isPrintGraph) {

	int i;

	initGlobalValues();

	activeProc = procs[0];

	for (i = 0; i < maxGroupId; i++) {

		activeGroupId = i;

		if (activeProc->numOfThreads > 1) {

			findDataSharingPattern();
			addScores(totalScores[i], dsScores[i], weightDS);
			memcpy(&activeProc->parPatternScores.dsScores[i], &dsScores[i], MAX_PAR_PAT * sizeof(int));

			findCommPattern();
			addScores(totalScores[i], commScores[i], weightTC);
			memcpy(&activeProc->parPatternScores.commScores[i], &commScores[i], MAX_PAR_PAT * sizeof(int));

			findGenThreadingPattern();
			addScores(totalScores[i], genThreadingScores[i], weightGT);
			memcpy(&activeProc->parPatternScores.genThreadingScores[i], &genThreadingScores[i], MAX_PAR_PAT * sizeof(int));

			findSyncConPattern();
			addScores(totalScores[i], syncConScores[i], weightSC);
			memcpy(&activeProc->parPatternScores.syncConScores[i], &syncConScores[i], MAX_PAR_PAT * sizeof(int));

			memcpy(&activeProc->parPatternScores.totalScores[i], &totalScores[i], MAX_PAR_PAT * sizeof(int));

			pps[i] = getBestFit(totalScores[i], dsScores[i], commScores[i], genThreadingScores[i], syncConScores[i]);

		} else {

			pps[i] = PP_ST; /* single thread */

		}

	}

	if (isVerboseOpen) {
		printScores(pps);
	}

	if (isPrintGraph) {
		printGraph(pps);
	}

	return maxGroupId;
}
