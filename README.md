#MINIME: Multicore Benchmark Synthesizer#

MINIME consists of three tools: MINIME-CPU, MINIME-SA, and MINIME-GPU.

MINIME-CPU is an automated tool that generates 
synthetic/standalone benchmarks in C using
MRAPI, MCAPI, or POSIX (Pthread). 
The CPU synthetic benchmark 
preserves the following performance characteristics:
Parallel Pattern (PP), Thread Communication (TC), 
Instructions per Cycle (IPC), Cache Miss Rate (CMR), 
Branch Misprediction Rate (BMR), 
Communication to Computation Ratio (CCR).
Our framework contains three main modules: 
benchmark characterizer, parallel pattern recognizer,
and synthetic benchmark synthesizer. The benchmark characterizer
obtains parallel benchmark characteristics by using
a dynamic binary instrumentation tool. The parallel pattern
recognizer decides the architectural parallel pattern from
the benchmark characteristics obtained. Finally, the benchmark synthesizer
generates a synthetic benchmark from the parallel pattern
and the benchmark characteristics obtained. 
Also, the specific MCA/Pthread
API is used during synthesis in order to generate 
benchmarks for heterogeneous embedded systems.

MINIME-SA is an automated tool similar to MINIME-CPU but
it uses user-defined input files instead of user given binary files. 

MINIME-GPU is an automated tool that generates 
synthetic benchmarks in C++ using OpenCL API. 
The GPU synthetic benchmark 
preserves the following performance characteristics:
instruction throughput, computation-to-memory access, 
dynamic memory instruction mix, 
memory efficiency, and compute unit occupancy.
Our framework contains two main modules: 
benchmark characterizer and synthetic benchmark synthesizer. 
The benchmark characterizer
obtains parallel benchmark characteristics by using
a simulation tool. The benchmark synthesizer
generates a synthetic benchmark from the 
benchmark characteristics obtained. 
Also, the specific OpenCL API is used during synthesis 
in order to generate benchmarks for CPU/GPU heterogeneous systems.

**Please cite the following papers if you are using our tool.**

E. Deniz, A. Sen, B. Kahne, J. Holt, 
MINIME: Pattern-Aware Multicore Benchmark Synthesizer, 
IEEE Transactions on Computers, 2014.

E. Deniz, A. Sen, 
MINIME-GPU: Multicore Benchmark Synthesizer for GPUs, 
ACM Transactions on Architecture and Code Optimization, 2015.

**The MINIME home page:**

  www.cmpe.boun.edu.tr/~deniz/minime.html